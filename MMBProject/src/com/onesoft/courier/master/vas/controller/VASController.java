package com.onesoft.courier.master.vas.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.OutwardBean;
import com.onesoft.courier.booking.bean.OutwardForwardedTableBean;
import com.onesoft.courier.master.vas.bean.VAS_Bean;
import com.onesoft.courier.master.vas.bean.VAS_Table_Bean;
import com.onesoft.courier.sql.queries.MasterSQL_VAS_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class VASController implements Initializable{

	
	private ObservableList<VAS_Table_Bean> tabledata_VAS=FXCollections.observableArrayList();
	
	@FXML
	private ImageView imgViewSearchIcon_VAS;
	
	@FXML
	private TextField txtVasCode;
	
	@FXML
	private TextField txtVasName;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnUpdate;
	

	@FXML
	private TableView<VAS_Table_Bean> tableVAS;

	@FXML
	private TableColumn<VAS_Table_Bean, Integer> vas_tabColumn_slno;

	@FXML
	private TableColumn<VAS_Table_Bean, String> vas_tabColumn_VAS_Code;

	@FXML
	private TableColumn<VAS_Table_Bean, String> vas_tabColumn_VAS_Name;
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		if (n.getId().equals("txtVasCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				check_VASDetails();
				txtVasName.requestFocus();
			}
		}

		else if (n.getId().equals("txtVasName")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(btnUpdate.isDisable()==true)
				{
					btnSave.requestFocus();
				}
				else
				{
					btnUpdate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
		
		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
	}

	
// ==================================================================================	
	
	public void setValidation() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		//double test=Double.valueOf(txtWeightUpto.getText());
		
		if (txtVasCode.getText() == null || txtVasCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a VAS Code");
			alert.showAndWait();
			txtVasCode.requestFocus();
		}
		
		else if (txtVasName.getText() == null || txtVasName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a VAS Name");
			alert.showAndWait();
			txtVasName.requestFocus();
		}
		else
		{
			if(btnUpdate.isDisable()==true)
			{
				saveNewVAS();
			}
			else
			{
				updateVAS();
			}
		}
		
	}

	
// *************************************************************************

	public void check_VASDetails() throws SQLException
	{
		VAS_Bean vBean=new VAS_Bean();
		vBean.setVasCode(txtVasCode.getText());
		ResultSet rs = null;

		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		boolean isDataExist=false;
		
		try 
		{
			sql = MasterSQL_VAS_Utils.VAS_DATA_RETRIEVE_SQL;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,vBean.getVasCode());

			System.out.println("PS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				isDataExist=true;
				txtVasName.setText(rs.getString("vas_name"));
			}
			
			if(isDataExist==true)
			{
				btnUpdate.setDisable(false);
				btnSave.setDisable(true);
				isDataExist=false;
			}

			else
			{
				btnUpdate.setDisable(true);
				btnSave.setDisable(false);
			}
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}

	
// *************************************************************************	
	
	public void saveNewVAS() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		VAS_Bean vBean=new VAS_Bean();
		
		vBean.setVasCode(txtVasCode.getText());
		vBean.setVasName(txtVasName.getText());
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_VAS_Utils.VAS_INSERT_SQL;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, vBean.getVasCode());
			preparedStmt.setString(2, vBean.getVasName());
			
			preparedStmt.executeUpdate();
			
			loadVASTable();
			
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// *************************************************************************	
	
	public void updateVAS() throws SQLException
	{
		VAS_Bean vBean=new VAS_Bean();
		
		vBean.setVasCode(txtVasCode.getText());
		vBean.setVasName(txtVasName.getText());
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_VAS_Utils.VAS_UPDATE_SQL;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, vBean.getVasName());
			preparedStmt.setString(2, vBean.getVasCode());	
			
			preparedStmt.executeUpdate();
			
			loadVASTable();
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// *************************************************************************

	public void loadVASTable() throws SQLException 
	{
		tabledata_VAS.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		VAS_Bean vBean = new VAS_Bean();

		int slno = 1;

		try 
		{
			stmt = con.createStatement();
			sql = MasterSQL_VAS_Utils.VAS_LOAD_TABLE_SQL;
		
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) 
			{
				vBean.setSlno(slno);
				vBean.setVasCode(rs.getString("vas_code"));
				vBean.setVasName(rs.getString("vas_name"));

				tabledata_VAS.add(new VAS_Table_Bean(vBean.getSlno(), vBean.getVasCode(), vBean.getVasName(),0));

				slno++;
			}
			tableVAS.setItems(tabledata_VAS);
			reset();
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
// *************************************************************************	
	
	public void reset()
	{
		txtVasCode.clear();
		txtVasName.clear();
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
		txtVasCode.requestFocus();
	}
	
// *************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		imgViewSearchIcon_VAS.setImage(new Image("/icon/searchicon_blue.png"));
		
		vas_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,Integer>("slno"));
		vas_tabColumn_VAS_Code.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,String>("vasCode"));
		vas_tabColumn_VAS_Name.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,String>("vasName"));
		
		
		btnUpdate.setDisable(true);
		
		try {
			loadVASTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
