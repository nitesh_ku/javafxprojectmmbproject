package com.onesoft.courier.master.vas.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.controller.DataEntryController;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.master.vas.bean.ClientVASBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_VAS_Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ClientVASController implements Initializable{

	private ObservableList<String> comboBoxPcs_KG_Items=FXCollections.observableArrayList("per pcs","per kg");
	private ObservableList<String> comboBoxFreeFloorItems=FXCollections.observableArrayList("1","2","3","4","5","6","7","8","9","10");
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	private Set<String> serviceItems=new HashSet<>();
	
	private List<String> list_clients=new ArrayList<>();
	

	@FXML
	private Button btnSave;

	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset;
	
	
// **********************************************************	
	
	@FXML
	private CheckBox checkBoxFuelOnFOV;
	
	@FXML
	private CheckBox checkBoxFuelExcludingFOV;
	
	@FXML
	private ComboBox<String> comboBoxHFD_Type_Pcs_N_Kg;
	
	@FXML
	private ComboBox<String> comboBoxFreeFloor;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	@FXML
	private TextField txtClient;
	
	@FXML
	private TextField txtCOD_Min;
	
	@FXML
	private TextField txtCOD_Percentage;
	
	@FXML
	private TextField txtOverSizeShpmnt_Min;
	
	@FXML
	private TextField txtOverSizeShpmnt_Kg;
	
	@FXML
	private TextField txtOverSizeShpmnt_KG_Limit;
	
	@FXML
	private TextField txtOverSizeShpmnt_CM_Limit;
	
	@FXML
	private TextField txtInsurance_Carrier_Min;
	
	@FXML
	private TextField txtInsurance_Carrier_Percentage;
	
	@FXML
	private TextField txtInsurance_Owner_Min;
	
	@FXML
	private TextField txtInsurance_Owner_Percentage;
	
	@FXML
	private TextField txtHoldAtOffice_Min;
	
	@FXML
	private TextField txtHoldAtOffice_Kg;

	@FXML
	private TextField txtDocketCharge_Min;
	
	@FXML
	private TextField txtHFD_Charges_Min;
	
	@FXML
	private TextField txtHFD_Charges_Pcs_kg;
	
	@FXML
	private TextField txtFuelSurcharge;
	
	@FXML
	private TextField txtDeliveryOnInvoice;
	
	@FXML
	private TextField txtCriticalSrvc_Rate;
	
	@FXML
	private TextField txtCriticalSrvc_Additional;
	
	@FXML
	private TextField txtCriticalSrvc_Slab;
	
	@FXML
	private TextField txtTDD_Rate;
	
	@FXML
	private TextField txtTDD_Additional;
	
	@FXML
	private TextField txtTDD_Slab;
	
	@FXML
	private TextField txtODA_Min;
	
	@FXML
	private TextField txtODA_Kg;
	
	@FXML
	private TextField txtOPA_Min;
	
	@FXML
	private TextField txtOPA_Kg;
	
	@FXML
	private TextField txtReversePickup_Min;
	
	@FXML
	private TextField txtReversePickup_Slab;
	
	@FXML
	private TextField txtAdvancementFee_Min;
	
	@FXML
	private TextField txtAdvancementFee_Percentage;
	
	@FXML
	private TextField txtGreenTax;
	
	@FXML
	private TextField txtToPay;
	
	@FXML
	private TextField txtAddressCorrectionCharges;

	
// =============================================================================================

	public void loadClients() throws SQLException 
	{
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_clients.add(bean.getClientName() + " | " + bean.getClientCode());

		}
		TextFields.bindAutoCompletion(txtClient, list_clients);

	}
	
// =============================================================================================	
	
/*	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	*/
	
	
// ******************************************************************************

	public void loadService() throws SQLException 
	{
		new LoadServiceGroups().loadServicesWithName();

		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME) 
		{
			serviceItems.add(bean.getServiceName()+" | "+bean.getServiceCode());
		}
		
		for(String service: serviceItems)
		{
			comboBoxServiceItems.add(service);
		}
			
		
		comboBoxService.setItems(comboBoxServiceItems);

	}	
	
// =============================================================================================
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		
		if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxService.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getDataViaClientAndService();
				txtCOD_Min.requestFocus();
			}
		}

		else if (n.getId().equals("checkBoxFuelExcludingFOV")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				checkBoxFuelOnFOV.requestFocus();
			}
		}
		
		else if (n.getId().equals("checkBoxFuelOnFOV")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCOD_Min.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtCOD_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCOD_Percentage.requestFocus();
			}
		}

		else if (n.getId().equals("txtCOD_Percentage")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtODA_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtODA_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtODA_Kg.requestFocus();
			}
		}

		else if (n.getId().equals("txtODA_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtFuelSurcharge.requestFocus();
				//txtOverSizeShpmnt_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtOverSizeShpmnt_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOverSizeShpmnt_Kg.requestFocus();
			}
		}

		else if (n.getId().equals("txtOverSizeShpmnt_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOverSizeShpmnt_KG_Limit.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtOverSizeShpmnt_KG_Limit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOverSizeShpmnt_CM_Limit.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtOPA_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOPA_Kg.requestFocus();
			}
		}

		else if (n.getId().equals("txtOPA_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInsurance_Carrier_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtInsurance_Carrier_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInsurance_Carrier_Percentage.requestFocus();
			}
		}

		else if (n.getId().equals("txtInsurance_Carrier_Percentage")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInsurance_Owner_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtInsurance_Owner_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInsurance_Owner_Percentage.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtInsurance_Owner_Percentage")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtHoldAtOffice_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtHoldAtOffice_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtHoldAtOffice_Kg.requestFocus();
			}
		}

		else if (n.getId().equals("txtHoldAtOffice_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtReversePickup_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtReversePickup_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtReversePickup_Slab.requestFocus();
			}
		}

		else if (n.getId().equals("txtReversePickup_Slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDocketCharge_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtDocketCharge_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtGreenTax.requestFocus();
			}
		}

		else if (n.getId().equals("txtGreenTax")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAdvancementFee_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtAdvancementFee_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAdvancementFee_Percentage.requestFocus();
			}
		}

		else if (n.getId().equals("txtAdvancementFee_Percentage")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtHFD_Charges_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtHFD_Charges_Min")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtHFD_Charges_Pcs_kg.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtHFD_Charges_Pcs_kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxHFD_Type_Pcs_N_Kg.requestFocus();
			}
		}
		
		else if (n.getId().equals("comboBoxHFD_Type_Pcs_N_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxFreeFloor.requestFocus();
			}
		}
	/*	else if (n.getId().equals("txtHFD_Charges_PerPcs")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtGreenTax.requestFocus();
			}
		}*/

		else if (n.getId().equals("comboBoxFreeFloor")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtToPay.requestFocus();
			}
		}
		
		
		else if (n.getId().equals("txtToPay")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAddressCorrectionCharges.requestFocus();
			}
		}

		else if (n.getId().equals("txtAddressCorrectionCharges")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtTDD_Rate.requestFocus();
				//txtFuelSurcharge.requestFocus();
			}
		}

		else if (n.getId().equals("txtFuelSurcharge")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDeliveryOnInvoice.requestFocus();
			}
		}

		else if (n.getId().equals("txtDeliveryOnInvoice")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOPA_Min.requestFocus();
			}
		}

		else if (n.getId().equals("txtTDD_Rate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtTDD_Additional.requestFocus();
			}
		}

		else if (n.getId().equals("txtTDD_Additional")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtTDD_Slab.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtTDD_Slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCriticalSrvc_Rate.requestFocus();
			}
		}

		else if (n.getId().equals("txtCriticalSrvc_Rate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCriticalSrvc_Additional.requestFocus();
			}
		}

		else if (n.getId().equals("txtCriticalSrvc_Additional")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCriticalSrvc_Slab.requestFocus();
			}
		}

		else if (n.getId().equals("txtCriticalSrvc_Slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				txtOverSizeShpmnt_Min.requestFocus();
				
			}
		}
		
		
		else if (n.getId().equals("txtOverSizeShpmnt_CM_Limit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(btnUpdate.isDisable()==true)
				{
					btnSave.requestFocus();
				}
				else
				{
					btnUpdate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				System.out.println("Okk...........");
				setValidation();
			}
		}
		
		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				System.out.println("Okk...........");
				setValidation();
			}
		}
	}

	
// =============================================================================================	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		//double test=Double.valueOf(txtWeightUpto.getText());
		
		if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Client");
			alert.showAndWait();
			txtClient.requestFocus();
		}
		
		else if (comboBoxService.getValue() == null || comboBoxService.getValue().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Service");
			alert.showAndWait();
			comboBoxService.requestFocus();
		}
		
		
		
		else if (!txtCOD_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("COD min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtCOD_Min.requestFocus();
		}
		
		else if (!txtCOD_Percentage.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("COD % Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtCOD_Percentage.requestFocus();
		}
		
		else if (!txtODA_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("ODA min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtODA_Min.requestFocus();
		}
		
		else if (!txtODA_Kg.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("ODA kg Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtODA_Kg.requestFocus();
		}
		
		else if (!txtOverSizeShpmnt_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Over Size Shipment min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOverSizeShpmnt_Min.requestFocus();
		}
		
		else if (!txtOverSizeShpmnt_KG_Limit.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Over Size Shipment KG Limit Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOverSizeShpmnt_KG_Limit.requestFocus();
		}
		
		else if (!txtOverSizeShpmnt_CM_Limit.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Over Size Shipment CM Limit Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOverSizeShpmnt_CM_Limit.requestFocus();
		}
		
		else if (!txtOverSizeShpmnt_Kg.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Over Size Shipment kg Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOverSizeShpmnt_Kg.requestFocus();
		}
		
		else if (!txtOPA_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("OPA Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOPA_Min.requestFocus();
		}
		
		else if (!txtOPA_Kg.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("OPA Kg Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtOPA_Kg.requestFocus();
		}
		
		else if (!txtInsurance_Carrier_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Insurance Carrier Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtInsurance_Carrier_Min.requestFocus();
		}
		
		else if (!txtInsurance_Carrier_Percentage.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Insurance Carrier % Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtInsurance_Carrier_Percentage.requestFocus();
		}
		
		else if (!txtInsurance_Owner_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Insurance Owner Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtInsurance_Owner_Min.requestFocus();
		}
		
		else if (!txtInsurance_Owner_Percentage.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Insurance Owner % Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtInsurance_Owner_Percentage.requestFocus();
		}
		
		else if (!txtHoldAtOffice_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Hold At Office Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtHoldAtOffice_Min.requestFocus();
		}
		
		else if (!txtHoldAtOffice_Kg.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Hold At Office Kg Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtHoldAtOffice_Kg.requestFocus();
		}
		
		else if (!txtReversePickup_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Reverse Pickup Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtReversePickup_Min.requestFocus();
		}
		
		else if (!txtDocketCharge_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Docket Charge Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtDocketCharge_Min.requestFocus();
		}
		
		/*else if (!txtDocketCharge_Percentage.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Docket Charge % Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtDocketCharge_Percentage.requestFocus();
		}*/
		
		else if (!txtAdvancementFee_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Advancement Fee Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtAdvancementFee_Min.requestFocus();
		}
		
		else if (!txtAdvancementFee_Percentage.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Advancement Fee % Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtAdvancementFee_Percentage.requestFocus();
		}
		
		else if (!txtHFD_Charges_Min.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("HFD Charges Min Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtHFD_Charges_Min.requestFocus();
		}
		
		else if (!txtHFD_Charges_Pcs_kg.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("HFD Charges PerPcs Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtHFD_Charges_Pcs_kg.requestFocus();
		}
		
		else if (!txtGreenTax.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Green Tax Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtGreenTax.requestFocus();
		}
		
		else if (!txtFuelSurcharge.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Fuel Surcharge Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtFuelSurcharge.requestFocus();
		}
		
		else if (!txtDeliveryOnInvoice.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Delivery On Invoice Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtDeliveryOnInvoice.requestFocus();
		}
		
		else if (!txtToPay.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("To Pay Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtToPay.requestFocus();
		}
		
		else if (!txtAddressCorrectionCharges.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Address Correction Charges Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtAddressCorrectionCharges.requestFocus();
		}
		
		else if (!txtCriticalSrvc_Rate.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Critical Serivice Rate Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtCriticalSrvc_Rate.requestFocus();
		}
		
		else if (!txtCriticalSrvc_Additional.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Critical Service Additional Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtCriticalSrvc_Additional.requestFocus();
		}
		
		else if (!txtCriticalSrvc_Slab.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Critical Service Slab Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtCriticalSrvc_Slab.requestFocus();
		}
		
		else if (!txtTDD_Rate.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("TDD Rate Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtTDD_Rate.requestFocus();
		}
		
		else if (!txtTDD_Additional.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("TDD Additional Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtTDD_Additional.requestFocus();
		}
		
		else if (!txtTDD_Slab.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("TDD Slab Textfield:\nplease enter only numeric values!!");
			alert.showAndWait();
			txtTDD_Slab.requestFocus();
		}
		
		else
		{
			setInitialValueZeroInTextFields();
			System.out.println("Validation Ok");
			
			if(btnUpdate.isDisable()==true)
			{
				saveClientVASDetails();
			}
			else
			{
				updateClientVAS_viaClientAndNetwork();
			}
		}
	}	
		

// =============================================================================================	

	public void setInitialValueZeroInTextFields()
	{
		if(txtCOD_Min.getText().equals(""))
		{
			txtCOD_Min.setText("0");
		}
		
		if(txtCOD_Percentage.getText().equals(""))
		{
			txtCOD_Percentage.setText("0");
		}
		
		if(txtOverSizeShpmnt_Min.getText().equals(""))
		{
			txtOverSizeShpmnt_Min.setText("0");
		}
		
		if(txtOverSizeShpmnt_Kg.getText().equals(""))
		{
			txtOverSizeShpmnt_Kg.setText("0");
		}
		
		if(txtOverSizeShpmnt_KG_Limit.getText().equals(""))
		{
			txtOverSizeShpmnt_KG_Limit.setText("0");
		}
		
		if(txtOverSizeShpmnt_CM_Limit.getText().equals(""))
		{
			txtOverSizeShpmnt_CM_Limit.setText("120");
		}
		
		if(txtInsurance_Carrier_Min.getText().equals(""))
		{
			txtInsurance_Carrier_Min.setText("0");
		}
		
		if(txtInsurance_Carrier_Percentage.getText().equals(""))
		{
			txtInsurance_Carrier_Percentage.setText("0");
		}
		
		if(txtInsurance_Owner_Min.getText().equals(""))
		{
			txtInsurance_Owner_Min.setText("0");
		}
		
		if(txtInsurance_Owner_Percentage.getText().equals(""))
		{
			txtInsurance_Owner_Percentage.setText("0");
		}
		
		if(txtHoldAtOffice_Min.getText().equals(""))
		{
			txtHoldAtOffice_Min.setText("0");
		}
		
		if(txtHoldAtOffice_Kg.getText().equals(""))
		{
			txtHoldAtOffice_Kg.setText("0");
		}
		
		if(txtDocketCharge_Min.getText().equals(""))
		{
			txtDocketCharge_Min.setText("0");
		}
		
	/*	if(txtDocketCharge_Percentage.getText().equals(""))
		{
			txtDocketCharge_Percentage.setText("0");
		}*/
		
		if(txtHFD_Charges_Min.getText().equals(""))
		{
			txtHFD_Charges_Min.setText("0");
		}
		
		if(txtHFD_Charges_Pcs_kg.getText().equals(""))
		{
			txtHFD_Charges_Pcs_kg.setText("0");
		}
		
		if(txtFuelSurcharge.getText().equals(""))
		{
			txtFuelSurcharge.setText("0");
		}
		
		if(txtDeliveryOnInvoice.getText().equals(""))
		{
			txtDeliveryOnInvoice.setText("0");
		}
		
		if(txtCriticalSrvc_Rate.getText().equals(""))
		{
			txtCriticalSrvc_Rate.setText("0");
		}
		
		if(txtCriticalSrvc_Additional.getText().equals(""))
		{
			txtCriticalSrvc_Additional.setText("0");
		}
		
		if(txtCriticalSrvc_Slab.getText().equals(""))
		{
			txtCriticalSrvc_Slab.setText("0");
		}
		
		if(txtTDD_Rate.getText().equals(""))
		{
			txtTDD_Rate.setText("0");
		}
		
		if(txtTDD_Additional.getText().equals(""))
		{
			txtTDD_Additional.setText("0");
		}
		
		if(txtTDD_Slab.getText().equals(""))
		{
			txtTDD_Slab.setText("0");
		}
		
		if(txtODA_Min.getText().equals(""))
		{
			txtODA_Min.setText("0");
		}
		
		if(txtODA_Kg.getText().equals(""))
		{
			txtODA_Kg.setText("0");
		}
		
		if(txtOPA_Min.getText().equals(""))
		{
			txtOPA_Min.setText("0");
		}
		
		if(txtOPA_Kg.getText().equals(""))
		{
			txtOPA_Kg.setText("0");
		}
		
		if(txtReversePickup_Min.getText().equals(""))
		{
			txtReversePickup_Min.setText("0");
		}
		
		if(txtReversePickup_Slab.getText().equals(""))
		{
			txtReversePickup_Slab.setText("0");
		}
		
		if(txtAdvancementFee_Min.getText().equals(""))
		{
			txtAdvancementFee_Min.setText("0");
		}
		
		if(txtAdvancementFee_Percentage.getText().equals(""))
		{
			txtAdvancementFee_Percentage.setText("0");
		}
		
		if(txtGreenTax.getText().equals(""))
		{
			txtGreenTax.setText("0");
		}
		
		if(txtToPay.getText().equals(""))
		{
			txtToPay.setText("0");
		}
		
		if(txtAddressCorrectionCharges.getText().equals(""))
		{
			txtAddressCorrectionCharges.setText("0");
		}
	}

// =============================================================================================	

	public void saveClientVASDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] serviceCode=comboBoxService.getValue().replaceAll("\\s+","").split("\\|");
		
		ClientVASBean cvBean=new ClientVASBean();
		
		cvBean.setClient(clientCode[1]);
		cvBean.setNetwork(serviceCode[1]);
		
		if(checkBoxFuelOnFOV.isSelected()==true)
		{
			cvBean.setFov_Status("T");	
		}
		else
		{
			cvBean.setFov_Status("F");	
		}
		
		if(checkBoxFuelExcludingFOV.isSelected()==true)
		{
			cvBean.setFov_Fuel_Excluding("T");	
		}
		else
		{
			cvBean.setFov_Fuel_Excluding("F");	
		}
		
		cvBean.setCod_Min(Double.valueOf(txtCOD_Min.getText()));
		cvBean.setCod_Percentage(Double.valueOf(txtCOD_Percentage.getText()));
		cvBean.setOda_Min(Double.valueOf(txtODA_Min.getText()));
		cvBean.setOda_Kg(Double.valueOf(txtODA_Kg.getText()));
		cvBean.setOverSizeShpmnt_Min(Double.valueOf(txtOverSizeShpmnt_Min.getText()));
		cvBean.setOverSizeShpmnt_Kg(Double.valueOf(txtOverSizeShpmnt_Kg.getText()));
		cvBean.setOverSizeShpmnt_kg_Limit(Double.valueOf(txtOverSizeShpmnt_KG_Limit.getText()));
		cvBean.setOverSizeShpmnt_cm_Limit(Double.valueOf(txtOverSizeShpmnt_CM_Limit.getText()));
		cvBean.setOpa_Min(Double.valueOf(txtOPA_Min.getText()));
		cvBean.setOpa_Kg(Double.valueOf(txtOPA_Kg.getText()));
		cvBean.setInsurance_Carrier_Min(Double.valueOf(txtInsurance_Carrier_Min.getText()));
		cvBean.setInsurance_Carrier_Percentage(Double.valueOf(txtInsurance_Carrier_Percentage.getText()));
		cvBean.setInsurance_Owner_Min(Double.valueOf(txtInsurance_Owner_Min.getText()));
		cvBean.setInsurance_Owner_Percentage(Double.valueOf(txtInsurance_Owner_Percentage.getText()));
		cvBean.setHoldAtOffice_Min(Double.valueOf(txtHoldAtOffice_Min.getText()));
		cvBean.setHoldAtOffice_Kg(Double.valueOf(txtHoldAtOffice_Kg.getText()));
		cvBean.setReversePickup_Min(Double.valueOf(txtReversePickup_Min.getText()));
		cvBean.setReversePickup_slab(Double.valueOf(txtReversePickup_Slab.getText()));
		cvBean.setDocketCharge_Min(Double.valueOf(txtDocketCharge_Min.getText()));
		cvBean.setAdvancementFee_Min(Double.valueOf(txtAdvancementFee_Min.getText()));
		cvBean.setAdvancementFee_Percentage(Double.valueOf(txtAdvancementFee_Percentage.getText()));
		cvBean.setHdf_Charges_Min(Double.valueOf(txtHFD_Charges_Min.getText()));
		cvBean.setHdf_Charges_PerPcs(Double.valueOf(txtHFD_Charges_Pcs_kg.getText()));
		cvBean.setGreenTax(Double.valueOf(txtGreenTax.getText()));
		cvBean.setFuelSurcharge(Double.valueOf(txtFuelSurcharge.getText()));
		cvBean.setDeliveryOnInvoice(Double.valueOf(txtDeliveryOnInvoice.getText()));
		cvBean.setToPay(Double.valueOf(txtToPay.getText()));
		cvBean.setAddressCorrectionCharges(Double.valueOf(txtAddressCorrectionCharges.getText()));
		cvBean.setCriticalSrvc_Rate(Double.valueOf(txtCriticalSrvc_Rate.getText()));
		cvBean.setCriticalSrvc_Additional(Double.valueOf(txtCriticalSrvc_Additional.getText()));
		cvBean.setCriticalSrvc_Slab(Double.valueOf(txtCriticalSrvc_Slab.getText()));
		cvBean.settDD_Rate(Double.valueOf(txtTDD_Rate.getText()));
		cvBean.settDD_Additional(Double.valueOf(txtTDD_Additional.getText()));
		cvBean.settDD_Slab(Double.valueOf(txtTDD_Slab.getText()));
		cvBean.setHfd_type_pcs_kg(comboBoxHFD_Type_Pcs_N_Kg.getValue());
		cvBean.setHfd_free_flr(Integer.valueOf(comboBoxFreeFloor.getValue()));
		

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		try 
		{			
			/*System.out.println("Network Code: "+cvBean.getNetworkCode());
			System.out.println("Service Code: "+cvBean.getServiceCode());
			System.out.println("Mapping Type: "+cvBean.getMappingType());
			System.out.println("From Zone/City: "+cvBean.getFromZoneOrCityCode());
			System.out.println("To Zone/City: "+cvBean.getToZoneOrCityCode());
			System.out.println("Slab Type: "+cvBean.getSlabType());
			System.out.println("Weight Up To: "+cvBean.getWeightUpto());
			System.out.println("Slab: "+cvBean.getSlab());
			System.out.println("Dox: "+cvBean.getDox());
			System.out.println("Non Dox: "+cvBean.getNonDox());*/

			String query = MasterSQL_Client_VAS_Utils.CLIENT_VAS_INSERT_SQL_SAVE_CLIENT;
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, cvBean.getClient());
			preparedStmt.setString(2, cvBean.getNetwork());
			preparedStmt.setDouble(3, cvBean.getCod_Min());
			preparedStmt.setDouble(4, cvBean.getCod_Percentage());
			preparedStmt.setDouble(5, cvBean.getInsurance_Owner_Min());
			preparedStmt.setDouble(6, cvBean.getInsurance_Owner_Percentage());
			preparedStmt.setDouble(7, cvBean.getInsurance_Carrier_Min());
			preparedStmt.setDouble(8, cvBean.getInsurance_Carrier_Percentage());
			preparedStmt.setDouble(9, cvBean.getOverSizeShpmnt_Min());
			preparedStmt.setDouble(10, cvBean.getOverSizeShpmnt_Kg());
			preparedStmt.setDouble(11, cvBean.getDocketCharge_Min());
			preparedStmt.setDouble(12, cvBean.getOda_Min());
			preparedStmt.setDouble(13, cvBean.getOda_Kg());
			preparedStmt.setDouble(14, cvBean.getOpa_Min());
			preparedStmt.setDouble(15, cvBean.getOpa_Kg());
			preparedStmt.setDouble(16, cvBean.getAdvancementFee_Min());
			preparedStmt.setDouble(17, cvBean.getAdvancementFee_Percentage());
			preparedStmt.setDouble(18, cvBean.getDeliveryOnInvoice());
			preparedStmt.setDouble(19, cvBean.getFuelSurcharge());
			preparedStmt.setDouble(20, cvBean.getHdf_Charges_Min());
			preparedStmt.setDouble(21, cvBean.getHdf_Charges_PerPcs());
			preparedStmt.setDouble(22, cvBean.getToPay());
			preparedStmt.setDouble(23, cvBean.getAddressCorrectionCharges());
			preparedStmt.setDouble(24, cvBean.getHoldAtOffice_Min());
			preparedStmt.setDouble(25, cvBean.getHoldAtOffice_Kg());
			preparedStmt.setDouble(26, cvBean.getGreenTax());
			preparedStmt.setDouble(27, cvBean.getReversePickup_Min());
			preparedStmt.setDouble(28, cvBean.getReversePickup_slab());
			preparedStmt.setDouble(29, cvBean.gettDD_Rate());
			preparedStmt.setDouble(30, cvBean.gettDD_Additional());
			preparedStmt.setDouble(31, cvBean.gettDD_Slab());
			preparedStmt.setDouble(32, cvBean.getCriticalSrvc_Rate());
			preparedStmt.setDouble(33, cvBean.getCriticalSrvc_Additional());
			preparedStmt.setDouble(34, cvBean.getCriticalSrvc_Slab());
			preparedStmt.setString(35, MasterSQL_Client_VAS_Utils.CLIENT_VAS_APPLICATION_ID_FORALL);
			preparedStmt.setString(36, MasterSQL_Client_VAS_Utils.CLIENT_VAS_LAST_MODIFIED_USER_FORALL);
			preparedStmt.setString(37, cvBean.getHfd_type_pcs_kg());
			preparedStmt.setInt(38, cvBean.getHfd_free_flr());
			preparedStmt.setDouble(39, cvBean.getOverSizeShpmnt_kg_Limit());
			preparedStmt.setDouble(40, cvBean.getOverSizeShpmnt_cm_Limit());
			preparedStmt.setString(41, cvBean.getFov_Status());
			preparedStmt.setString(42, cvBean.getFov_Fuel_Excluding());
			

			int status=preparedStmt.executeUpdate();
			
			reset();
			
			if(status==1)
			{
				reset();
				DataEntryController.isClientVAS_Updated=true;	
				alert.setContentText("Client VAS detials successfully Saved...!");
				alert.showAndWait();
				if(DataEntryController.clientVAS_CallFromDataEntry==false)
				{
					DataEntryController.clientVAS_CallFromDataEntry=true;
					Main.hyperLinkStage.hide();
					
					
				}
				
			
			}
			else
			{
				alert.setContentText("Client VAS detials not Saved...!\nPlease Check");
				alert.showAndWait();
			}
			
			
			
			
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// =============================================================================================	
	
	public void getDataViaClientAndService() throws SQLException
	{
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] serviceCode=comboBoxService.getValue().replaceAll("\\s+","").split("\\|");

		ClientVASBean cvBean=new ClientVASBean();
		
		cvBean.setClient(clientCode[1]);
		cvBean.setNetwork(serviceCode[1]);
		
		ResultSet rs = null;
		
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		boolean isRowAvailable=false;

		try 
		{
			
			sql = MasterSQL_Client_VAS_Utils.CLIENT_VAS_DATA_RETRIEVE_SQL;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,cvBean.getClient());
			preparedStmt.setString(2,cvBean.getNetwork());
			
			System.out.println("PS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
				isRowAvailable=true;
				
				cvBean.setCod_Min(rs.getDouble("cod_min"));
				cvBean.setCod_Percentage(rs.getDouble("cod_per"));
				cvBean.setOda_Min(rs.getDouble("oda_min"));
				cvBean.setOda_Kg(rs.getDouble("oda_perkg"));
				cvBean.setOverSizeShpmnt_Min(rs.getDouble("oss_min"));
				cvBean.setOverSizeShpmnt_Kg(rs.getDouble("oss_perkg"));
				cvBean.setOverSizeShpmnt_kg_Limit(rs.getDouble("oss_kg_limit"));
				cvBean.setOverSizeShpmnt_cm_Limit(rs.getDouble("oss_cm_limit"));
				cvBean.setOpa_Min(rs.getDouble("opa_min"));
				cvBean.setOpa_Kg(rs.getDouble("opa_perkg"));
				cvBean.setInsurance_Carrier_Min(rs.getDouble("insur_carr_min"));
				cvBean.setInsurance_Carrier_Percentage(rs.getDouble("insur_carr_per"));
				cvBean.setInsurance_Owner_Min(rs.getDouble("insur_own_min"));
				cvBean.setInsurance_Owner_Percentage(rs.getDouble("insur_own_per"));
				cvBean.setHoldAtOffice_Min(rs.getDouble("hold_at_office_min"));
				cvBean.setHoldAtOffice_Kg(rs.getDouble("hold_at_office_perkg"));
				cvBean.setReversePickup_Min(rs.getDouble("reverse_pickup_min"));
				cvBean.setReversePickup_slab(rs.getDouble("reverse_pickup_slab"));
				cvBean.setDocketCharge_Min(rs.getDouble("docket_chrgs"));
				cvBean.setAdvancementFee_Min(rs.getDouble("adv_fee_min"));
				cvBean.setAdvancementFee_Percentage(rs.getDouble("adv_fee_per"));
				cvBean.setHdf_Charges_Min(rs.getDouble("hfd_min"));
				cvBean.setHdf_Charges_PerPcs(rs.getDouble("hfd_per_pcs_kg"));
				cvBean.setGreenTax(rs.getDouble("green_tax"));
				cvBean.setFuelSurcharge(rs.getDouble("fuel_rate"));
				cvBean.setDeliveryOnInvoice(rs.getDouble("delivery_on_invoice"));
				cvBean.setToPay(rs.getDouble("topay"));
				cvBean.setAddressCorrectionCharges(rs.getDouble("addres_correction_chrgs"));
				cvBean.setCriticalSrvc_Rate(rs.getDouble("critical_min"));
				cvBean.setCriticalSrvc_Additional(rs.getDouble("critical_add"));
				cvBean.setCriticalSrvc_Slab(rs.getDouble("critical_slab"));
				cvBean.settDD_Rate(rs.getDouble("tdd_min"));
				cvBean.settDD_Additional(rs.getDouble("tdd_add"));
				cvBean.settDD_Slab(rs.getDouble("tdd_slab"));
				cvBean.setHfd_type_pcs_kg(rs.getString("hfd_type"));
				cvBean.setHfd_free_flr(rs.getInt("hfd_free_floor"));
				cvBean.setFov_Status(rs.getString("fov"));
				cvBean.setFov_Fuel_Excluding(rs.getString("fuel_excluding_fov"));
				
				
			}
			
			
			if(isRowAvailable==true)
			{
				btnUpdate.setDisable(false);
				btnSave.setDisable(true);
				
				isRowAvailable=false;
				txtCOD_Min.setText(String.valueOf(cvBean.getCod_Min()));
				txtCOD_Percentage.setText(String.valueOf(cvBean.getCod_Percentage()));
				txtOverSizeShpmnt_Min.setText(String.valueOf(cvBean.getOverSizeShpmnt_Min()));
				txtOverSizeShpmnt_Kg.setText(String.valueOf(cvBean.getOverSizeShpmnt_Kg()));
				txtOverSizeShpmnt_KG_Limit.setText(String.valueOf(cvBean.getOverSizeShpmnt_kg_Limit()));
				txtOverSizeShpmnt_CM_Limit.setText(String.valueOf(cvBean.getOverSizeShpmnt_cm_Limit()));
				txtInsurance_Carrier_Min.setText(String.valueOf(cvBean.getInsurance_Carrier_Min()));
				txtInsurance_Carrier_Percentage.setText(String.valueOf(cvBean.getInsurance_Carrier_Percentage()));
				txtInsurance_Owner_Min.setText(String.valueOf(cvBean.getInsurance_Owner_Min()));
				txtInsurance_Owner_Percentage.setText(String.valueOf(cvBean.getInsurance_Owner_Percentage()));
				txtHoldAtOffice_Min.setText(String.valueOf(cvBean.getHoldAtOffice_Min()));
				txtHoldAtOffice_Kg.setText(String.valueOf(cvBean.getHoldAtOffice_Kg()));
				txtDocketCharge_Min.setText(String.valueOf(cvBean.getDocketCharge_Min()));
				txtHFD_Charges_Min.setText(String.valueOf(cvBean.getHdf_Charges_Min()));
				txtHFD_Charges_Pcs_kg.setText(String.valueOf(cvBean.getHdf_Charges_PerPcs()));
				txtFuelSurcharge.setText(String.valueOf(cvBean.getFuelSurcharge()));
				txtDeliveryOnInvoice.setText(String.valueOf(cvBean.getDeliveryOnInvoice()));
				txtCriticalSrvc_Rate.setText(String.valueOf(cvBean.getCriticalSrvc_Rate()));
				txtCriticalSrvc_Additional.setText(String.valueOf(cvBean.getCriticalSrvc_Additional()));
				txtCriticalSrvc_Slab.setText(String.valueOf(cvBean.getCriticalSrvc_Slab()));
				txtTDD_Rate.setText(String.valueOf(cvBean.gettDD_Rate()));
				txtTDD_Additional.setText(String.valueOf(cvBean.gettDD_Additional()));
				txtTDD_Slab.setText(String.valueOf(cvBean.gettDD_Slab()));
				txtODA_Min.setText(String.valueOf(cvBean.getOda_Min()));
				txtODA_Kg.setText(String.valueOf(cvBean.getOda_Kg()));
				txtOPA_Min.setText(String.valueOf(cvBean.getOpa_Min()));
				txtOPA_Kg.setText(String.valueOf(cvBean.getOpa_Kg()));
				txtReversePickup_Min.setText(String.valueOf(cvBean.getReversePickup_Min()));
				txtReversePickup_Slab.setText(String.valueOf(cvBean.getReversePickup_slab()));
				txtAdvancementFee_Min.setText(String.valueOf(cvBean.getAdvancementFee_Min()));
				txtAdvancementFee_Percentage.setText(String.valueOf(cvBean.getAdvancementFee_Percentage()));
				txtGreenTax.setText(String.valueOf(cvBean.getGreenTax()));
				txtToPay.setText(String.valueOf(cvBean.getToPay()));
				txtAddressCorrectionCharges.setText(String.valueOf(cvBean.getAddressCorrectionCharges()));
				comboBoxHFD_Type_Pcs_N_Kg.setValue(cvBean.getHfd_type_pcs_kg());
				comboBoxFreeFloor.setValue(String.valueOf(cvBean.getHfd_free_flr()));
				
				if(cvBean.getFov_Status()!=null)
				{
					if(cvBean.getFov_Status().equals("T"))
					{
						checkBoxFuelOnFOV.setSelected(true);
					}
					else
					{
						checkBoxFuelOnFOV.setSelected(false);
					}
				}
				
				if(cvBean.getFov_Fuel_Excluding()!=null)
				{
					if(cvBean.getFov_Fuel_Excluding().equals("T"))
					{
						checkBoxFuelExcludingFOV.setSelected(true);
					}
					else
					{
						checkBoxFuelExcludingFOV.setSelected(false);
					}
				}
				
				
			}
			else
			{
				btnUpdate.setDisable(true);
				btnSave.setDisable(false);
				checkBoxFuelOnFOV.setSelected(false);
				checkBoxFuelExcludingFOV.setSelected(false);
				
				txtCOD_Min.setText("0");
				txtCOD_Percentage.setText("0");
				txtOverSizeShpmnt_Min.setText("0");
				txtOverSizeShpmnt_Kg.setText("0");
				txtOverSizeShpmnt_KG_Limit.setText("0");
				txtOverSizeShpmnt_CM_Limit.setText("120");
				txtInsurance_Carrier_Min.setText("0");
				txtInsurance_Carrier_Percentage.setText("0");
				txtInsurance_Owner_Min.setText("0");
				txtInsurance_Owner_Percentage.setText("0");
				txtHoldAtOffice_Min.setText("0");
				txtHoldAtOffice_Kg.setText("0");
				txtDocketCharge_Min.setText("0");
				txtHFD_Charges_Min.setText("0");
				txtHFD_Charges_Pcs_kg.setText("0");
				txtFuelSurcharge.setText("0");
				txtDeliveryOnInvoice.setText("0");
				txtCriticalSrvc_Rate.setText("0");
				txtCriticalSrvc_Additional.setText("0");
				txtCriticalSrvc_Slab.setText("0");
				txtTDD_Rate.setText("0");
				txtTDD_Additional.setText("0");
				txtTDD_Slab.setText("0");
				txtODA_Min.setText("0");
				txtODA_Kg.setText("0");
				txtOPA_Min.setText("0");
				txtOPA_Kg.setText("0");
				txtReversePickup_Min.setText("0");
				txtReversePickup_Slab.setText("0");
				txtAdvancementFee_Min.setText("0");
				txtAdvancementFee_Percentage.setText("0");
				txtGreenTax.setText("0");
				txtToPay.setText("0");
				txtAddressCorrectionCharges.setText("0");
				comboBoxFreeFloor.setValue("3");
				comboBoxHFD_Type_Pcs_N_Kg.setValue("per pcs");
				
			}
			
		}
		
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}

// =============================================================================================	

	public void updateClientVAS_viaClientAndNetwork() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		alert.setTitle("Alert");

		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxService.getValue().replaceAll("\\s+","").split("\\|");
		
		ClientVASBean cvBean=new ClientVASBean();
		
		cvBean.setClient(clientCode[1]);
		cvBean.setNetwork(networkCode[1]);
		
		if(checkBoxFuelOnFOV.isSelected()==true)
		{
			cvBean.setFov_Status("T");	
		}
		else
		{
			cvBean.setFov_Status("F");	
		}
		
		if(checkBoxFuelExcludingFOV.isSelected()==true)
		{
			cvBean.setFov_Fuel_Excluding("T");	
		}
		else
		{
			cvBean.setFov_Fuel_Excluding("F");	
		}
		
		cvBean.setCod_Min(Double.valueOf(txtCOD_Min.getText()));
		cvBean.setCod_Percentage(Double.valueOf(txtCOD_Percentage.getText()));
		cvBean.setOda_Min(Double.valueOf(txtODA_Min.getText()));
		cvBean.setOda_Kg(Double.valueOf(txtODA_Kg.getText()));
		cvBean.setOverSizeShpmnt_Min(Double.valueOf(txtOverSizeShpmnt_Min.getText()));
		cvBean.setOverSizeShpmnt_Kg(Double.valueOf(txtOverSizeShpmnt_Kg.getText()));
		cvBean.setOverSizeShpmnt_kg_Limit(Double.valueOf(txtOverSizeShpmnt_KG_Limit.getText()));
		cvBean.setOverSizeShpmnt_cm_Limit(Double.valueOf(txtOverSizeShpmnt_CM_Limit.getText()));
		cvBean.setOpa_Min(Double.valueOf(txtOPA_Min.getText()));
		cvBean.setOpa_Kg(Double.valueOf(txtOPA_Kg.getText()));
		cvBean.setInsurance_Carrier_Min(Double.valueOf(txtInsurance_Carrier_Min.getText()));
		cvBean.setInsurance_Carrier_Percentage(Double.valueOf(txtInsurance_Carrier_Percentage.getText()));
		cvBean.setInsurance_Owner_Min(Double.valueOf(txtInsurance_Owner_Min.getText()));
		cvBean.setInsurance_Owner_Percentage(Double.valueOf(txtInsurance_Owner_Percentage.getText()));
		cvBean.setHoldAtOffice_Min(Double.valueOf(txtHoldAtOffice_Min.getText()));
		cvBean.setHoldAtOffice_Kg(Double.valueOf(txtHoldAtOffice_Kg.getText()));
		cvBean.setReversePickup_Min(Double.valueOf(txtReversePickup_Min.getText()));
		cvBean.setReversePickup_slab(Double.valueOf(txtReversePickup_Slab.getText()));
		cvBean.setDocketCharge_Min(Double.valueOf(txtDocketCharge_Min.getText()));
		cvBean.setAdvancementFee_Min(Double.valueOf(txtAdvancementFee_Min.getText()));
		cvBean.setAdvancementFee_Percentage(Double.valueOf(txtAdvancementFee_Percentage.getText()));
		cvBean.setHdf_Charges_Min(Double.valueOf(txtHFD_Charges_Min.getText()));
		cvBean.setHdf_Charges_PerPcs(Double.valueOf(txtHFD_Charges_Pcs_kg.getText()));
		cvBean.setGreenTax(Double.valueOf(txtGreenTax.getText()));
		cvBean.setFuelSurcharge(Double.valueOf(txtFuelSurcharge.getText()));
		cvBean.setDeliveryOnInvoice(Double.valueOf(txtDeliveryOnInvoice.getText()));
		cvBean.setToPay(Double.valueOf(txtToPay.getText()));
		cvBean.setAddressCorrectionCharges(Double.valueOf(txtAddressCorrectionCharges.getText()));
		cvBean.setCriticalSrvc_Rate(Double.valueOf(txtCriticalSrvc_Rate.getText()));
		cvBean.setCriticalSrvc_Additional(Double.valueOf(txtCriticalSrvc_Additional.getText()));
		cvBean.setCriticalSrvc_Slab(Double.valueOf(txtCriticalSrvc_Slab.getText()));
		cvBean.settDD_Rate(Double.valueOf(txtTDD_Rate.getText()));
		cvBean.settDD_Additional(Double.valueOf(txtTDD_Additional.getText()));
		cvBean.settDD_Slab(Double.valueOf(txtTDD_Slab.getText()));
		cvBean.setHfd_type_pcs_kg(comboBoxHFD_Type_Pcs_N_Kg.getValue());
		cvBean.setHfd_free_flr(Integer.valueOf(comboBoxFreeFloor.getValue()));
		

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		try 
		{			
			String query = MasterSQL_Client_VAS_Utils.CLIENT_VAS_UPDATE_SQL;
			preparedStmt = con.prepareStatement(query);
			
			
			preparedStmt.setDouble(1, cvBean.getCod_Min());
			preparedStmt.setDouble(2, cvBean.getCod_Percentage());
			preparedStmt.setDouble(3, cvBean.getInsurance_Owner_Min());
			preparedStmt.setDouble(4, cvBean.getInsurance_Owner_Percentage());
			preparedStmt.setDouble(5, cvBean.getInsurance_Carrier_Min());
			preparedStmt.setDouble(6, cvBean.getInsurance_Carrier_Percentage());
			preparedStmt.setDouble(7, cvBean.getOverSizeShpmnt_Min());
			preparedStmt.setDouble(8, cvBean.getOverSizeShpmnt_Kg());
			preparedStmt.setDouble(9, cvBean.getDocketCharge_Min());
			preparedStmt.setDouble(10, cvBean.getOda_Min());
			preparedStmt.setDouble(11, cvBean.getOda_Kg());
			preparedStmt.setDouble(12, cvBean.getOpa_Min());
			preparedStmt.setDouble(13, cvBean.getOpa_Kg());
			preparedStmt.setDouble(14, cvBean.getAdvancementFee_Min());
			preparedStmt.setDouble(15, cvBean.getAdvancementFee_Percentage());
			preparedStmt.setDouble(16, cvBean.getDeliveryOnInvoice());
			preparedStmt.setDouble(17, cvBean.getFuelSurcharge());
			preparedStmt.setDouble(18, cvBean.getHdf_Charges_Min());
			preparedStmt.setDouble(19, cvBean.getHdf_Charges_PerPcs());
			preparedStmt.setDouble(20, cvBean.getToPay());
			preparedStmt.setDouble(21, cvBean.getAddressCorrectionCharges());
			preparedStmt.setDouble(22, cvBean.getHoldAtOffice_Min());
			preparedStmt.setDouble(23, cvBean.getHoldAtOffice_Kg());
			preparedStmt.setDouble(24, cvBean.getGreenTax());
			preparedStmt.setDouble(25, cvBean.getReversePickup_Min());
			preparedStmt.setDouble(26, cvBean.getReversePickup_slab());
			preparedStmt.setDouble(27, cvBean.gettDD_Rate());
			preparedStmt.setDouble(28, cvBean.gettDD_Additional());
			preparedStmt.setDouble(29, cvBean.gettDD_Slab());
			preparedStmt.setDouble(30, cvBean.getCriticalSrvc_Rate());
			preparedStmt.setDouble(31, cvBean.getCriticalSrvc_Additional());
			preparedStmt.setDouble(32, cvBean.getCriticalSrvc_Slab());
			preparedStmt.setString(33, cvBean.getHfd_type_pcs_kg());
			preparedStmt.setInt(34, cvBean.getHfd_free_flr());
			preparedStmt.setDouble(35, cvBean.getOverSizeShpmnt_kg_Limit());
			preparedStmt.setDouble(36, cvBean.getOverSizeShpmnt_cm_Limit());
			preparedStmt.setString(37, cvBean.getFov_Status());
			preparedStmt.setString(38, cvBean.getFov_Fuel_Excluding());
			preparedStmt.setString(39, cvBean.getClient());
			preparedStmt.setString(40, cvBean.getNetwork());
			

			int status=preparedStmt.executeUpdate();
		
			if(status==1)
			{
				reset();
				txtClient.requestFocus();
				DataEntryController.isClientVAS_Updated=true;
				alert.setContentText("Client VAS detials successfully Updated...!");
				alert.showAndWait();
				
				if(DataEntryController.clientVAS_CallFromDataEntry==false)
				{
					DataEntryController.clientVAS_CallFromDataEntry=true;
					System.err.println("Stage? >>>>>>>>>>>>>>>>>>>>>>");
					Main.hyperLinkStage.hide();
				}
				
				
			}
			else
			{
				alert.setContentText("Client VAS detials not Updated...!\nPlease Check");
				alert.showAndWait();
			}
			
			
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// *************************************************************************	
	
	public void reset()
	{
		/*txtClient.clear();
		comboBoxNetwork.getSelectionModel().clearSelection();*/
		txtCOD_Min.setText("0");
		txtCOD_Percentage.setText("0");
		txtOverSizeShpmnt_Min.setText("0");
		txtOverSizeShpmnt_Kg.setText("0");
		txtOverSizeShpmnt_KG_Limit.setText("0");
		txtOverSizeShpmnt_CM_Limit.setText("120");
		txtInsurance_Carrier_Min.setText("0");
		txtInsurance_Carrier_Percentage.setText("0");
		txtInsurance_Owner_Min.setText("0");
		txtInsurance_Owner_Percentage.setText("0");
		txtHoldAtOffice_Min.setText("0");
		txtHoldAtOffice_Kg.setText("0");
		txtDocketCharge_Min.setText("0");
		txtHFD_Charges_Min.setText("0");
		txtHFD_Charges_Pcs_kg.setText("0");
		txtFuelSurcharge.setText("0");
		txtDeliveryOnInvoice.setText("0");
		txtCriticalSrvc_Rate.setText("0");
		txtCriticalSrvc_Additional.setText("0");
		txtCriticalSrvc_Slab.setText("0");
		txtTDD_Rate.setText("0");
		txtTDD_Additional.setText("0");
		txtTDD_Slab.setText("0");
		txtODA_Min.setText("0");
		txtODA_Kg.setText("0");
		txtOPA_Min.setText("0");
		txtOPA_Kg.setText("0");
		txtReversePickup_Min.setText("0");
		txtReversePickup_Slab.setText("0");
		txtAdvancementFee_Min.setText("0");
		txtAdvancementFee_Percentage.setText("0");
		txtGreenTax.setText("0");
		txtToPay.setText("0");
		txtAddressCorrectionCharges.setText("0");
		comboBoxFreeFloor.setValue("3");
		comboBoxHFD_Type_Pcs_N_Kg.setValue("per pcs");
	}
	
// =============================================================================================		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		setInitialValueZeroInTextFields();
		
		btnUpdate.setDisable(true);
		
		txtOverSizeShpmnt_CM_Limit.setText("120");
		
		comboBoxHFD_Type_Pcs_N_Kg.setValue("per pcs");
		comboBoxFreeFloor.setValue("3");
		comboBoxFreeFloor.setItems(comboBoxFreeFloorItems);
		comboBoxHFD_Type_Pcs_N_Kg.setItems(comboBoxPcs_KG_Items);
		
		try {
			loadClients();
			loadService();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
