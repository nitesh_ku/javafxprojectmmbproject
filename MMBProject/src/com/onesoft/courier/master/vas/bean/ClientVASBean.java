package com.onesoft.courier.master.vas.bean;

public class ClientVASBean {
	
	
	private String service;
	private String network;
	private String client;
	private String hfd_type_pcs_kg;
	private String fov_Status;
	private String fov_Fuel_Excluding;
	private int hfd_free_flr; 
	private double cod_Min;
	private double cod_Percentage;
	private double overSizeShpmnt_Min;
	private double overSizeShpmnt_Kg;
	private double overSizeShpmnt_kg_Limit;
	private double overSizeShpmnt_cm_Limit;
	private double insurance_Carrier_Min;
	private double insurance_Carrier_Percentage;
	private double insurance_Owner_Min;
	private double insurance_Owner_Percentage;
	private double holdAtOffice_Min;
	private double holdAtOffice_Kg;
	private double docketCharge_Min;
	private double fuelSurcharge;
	private double hdf_Charges_PerPcs;
	private double hdf_Charges_Min;
	
	private double deliveryOnInvoice;
	private double criticalSrvc_Rate;
	private double criticalSrvc_Additional;
	private double criticalSrvc_Slab;
	private double tDD_Rate;
	private double tDD_Additional;
	private double tDD_Slab;
	private double oda_Min;
	private double oda_Kg;
	private double opa_Min;
	private double opa_Kg;
	private double reversePickup_Min;
	private double reversePickup_slab;
	private double advancementFee_Min;
	private double advancementFee_Percentage;
	private double greenTax;
	private double toPay;
	private double addressCorrectionCharges;

	
	
	
	public double getOverSizeShpmnt_kg_Limit() {
		return overSizeShpmnt_kg_Limit;
	}
	public void setOverSizeShpmnt_kg_Limit(double overSizeShpmnt_kg_Limit) {
		this.overSizeShpmnt_kg_Limit = overSizeShpmnt_kg_Limit;
	}
	public double getOverSizeShpmnt_cm_Limit() {
		return overSizeShpmnt_cm_Limit;
	}
	public void setOverSizeShpmnt_cm_Limit(double overSizeShpmnt_cm_Limit) {
		this.overSizeShpmnt_cm_Limit = overSizeShpmnt_cm_Limit;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public double getCod_Min() {
		return cod_Min;
	}
	public void setCod_Min(double cod_Min) {
		this.cod_Min = cod_Min;
	}
	public double getCod_Percentage() {
		return cod_Percentage;
	}
	public void setCod_Percentage(double cod_Percentage) {
		this.cod_Percentage = cod_Percentage;
	}
	public double getOverSizeShpmnt_Min() {
		return overSizeShpmnt_Min;
	}
	public void setOverSizeShpmnt_Min(double overSizeShpmnt_Min) {
		this.overSizeShpmnt_Min = overSizeShpmnt_Min;
	}
	public double getOverSizeShpmnt_Kg() {
		return overSizeShpmnt_Kg;
	}
	public void setOverSizeShpmnt_Kg(double overSizeShpmnt_Kg) {
		this.overSizeShpmnt_Kg = overSizeShpmnt_Kg;
	}
	public double getInsurance_Carrier_Min() {
		return insurance_Carrier_Min;
	}
	public void setInsurance_Carrier_Min(double insurance_Carrier_Min) {
		this.insurance_Carrier_Min = insurance_Carrier_Min;
	}
	public double getInsurance_Carrier_Percentage() {
		return insurance_Carrier_Percentage;
	}
	public void setInsurance_Carrier_Percentage(double insurance_Carrier_Percentage) {
		this.insurance_Carrier_Percentage = insurance_Carrier_Percentage;
	}
	public double getInsurance_Owner_Min() {
		return insurance_Owner_Min;
	}
	public void setInsurance_Owner_Min(double insurance_Owner_Min) {
		this.insurance_Owner_Min = insurance_Owner_Min;
	}
	public double getInsurance_Owner_Percentage() {
		return insurance_Owner_Percentage;
	}
	public void setInsurance_Owner_Percentage(double insurance_Owner_Percentage) {
		this.insurance_Owner_Percentage = insurance_Owner_Percentage;
	}
	public double getHoldAtOffice_Min() {
		return holdAtOffice_Min;
	}
	public void setHoldAtOffice_Min(double holdAtOffice_Min) {
		this.holdAtOffice_Min = holdAtOffice_Min;
	}
	public double getHoldAtOffice_Kg() {
		return holdAtOffice_Kg;
	}
	public void setHoldAtOffice_Kg(double holdAtOffice_Kg) {
		this.holdAtOffice_Kg = holdAtOffice_Kg;
	}
	public double getDocketCharge_Min() {
		return docketCharge_Min;
	}
	public void setDocketCharge_Min(double docketCharge_Min) {
		this.docketCharge_Min = docketCharge_Min;
	}
	
	public double getFuelSurcharge() {
		return fuelSurcharge;
	}
	public void setFuelSurcharge(double fuelSurcharge) {
		this.fuelSurcharge = fuelSurcharge;
	}
	public double getHdf_Charges_PerPcs() {
		return hdf_Charges_PerPcs;
	}
	public void setHdf_Charges_PerPcs(double hdf_Charges_PerPcs) {
		this.hdf_Charges_PerPcs = hdf_Charges_PerPcs;
	}
	public double getHdf_Charges_Min() {
		return hdf_Charges_Min;
	}
	public void setHdf_Charges_Min(double hdf_Charges_Min) {
		this.hdf_Charges_Min = hdf_Charges_Min;
	}
	public double getDeliveryOnInvoice() {
		return deliveryOnInvoice;
	}
	public void setDeliveryOnInvoice(double deliveryOnInvoice) {
		this.deliveryOnInvoice = deliveryOnInvoice;
	}
	public double getCriticalSrvc_Rate() {
		return criticalSrvc_Rate;
	}
	public void setCriticalSrvc_Rate(double criticalSrvc_Rate) {
		this.criticalSrvc_Rate = criticalSrvc_Rate;
	}
	public double getCriticalSrvc_Additional() {
		return criticalSrvc_Additional;
	}
	public void setCriticalSrvc_Additional(double criticalSrvc_Additional) {
		this.criticalSrvc_Additional = criticalSrvc_Additional;
	}
	public double getCriticalSrvc_Slab() {
		return criticalSrvc_Slab;
	}
	public void setCriticalSrvc_Slab(double criticalSrvc_Slab) {
		this.criticalSrvc_Slab = criticalSrvc_Slab;
	}
	public double gettDD_Rate() {
		return tDD_Rate;
	}
	public void settDD_Rate(double tDD_Rate) {
		this.tDD_Rate = tDD_Rate;
	}
	public double gettDD_Additional() {
		return tDD_Additional;
	}
	public void settDD_Additional(double tDD_Additional) {
		this.tDD_Additional = tDD_Additional;
	}
	public double gettDD_Slab() {
		return tDD_Slab;
	}
	public void settDD_Slab(double tDD_Slab) {
		this.tDD_Slab = tDD_Slab;
	}
	public double getOda_Min() {
		return oda_Min;
	}
	public void setOda_Min(double oda_Min) {
		this.oda_Min = oda_Min;
	}
	public double getOda_Kg() {
		return oda_Kg;
	}
	public void setOda_Kg(double oda_Kg) {
		this.oda_Kg = oda_Kg;
	}
	public double getOpa_Min() {
		return opa_Min;
	}
	public void setOpa_Min(double opa_Min) {
		this.opa_Min = opa_Min;
	}
	public double getOpa_Kg() {
		return opa_Kg;
	}
	public void setOpa_Kg(double opa_Kg) {
		this.opa_Kg = opa_Kg;
	}
	public double getReversePickup_Min() {
		return reversePickup_Min;
	}
	public void setReversePickup_Min(double reversePickup_Min) {
		this.reversePickup_Min = reversePickup_Min;
	}

	public double getAdvancementFee_Min() {
		return advancementFee_Min;
	}
	public void setAdvancementFee_Min(double advancementFee_Min) {
		this.advancementFee_Min = advancementFee_Min;
	}
	public double getAdvancementFee_Percentage() {
		return advancementFee_Percentage;
	}
	public void setAdvancementFee_Percentage(double advancementFee_Percentage) {
		this.advancementFee_Percentage = advancementFee_Percentage;
	}
	public double getGreenTax() {
		return greenTax;
	}
	public void setGreenTax(double greenTax) {
		this.greenTax = greenTax;
	}
	public double getToPay() {
		return toPay;
	}
	public void setToPay(double toPay) {
		this.toPay = toPay;
	}
	public double getAddressCorrectionCharges() {
		return addressCorrectionCharges;
	}
	public void setAddressCorrectionCharges(double addressCorrectionCharges) {
		this.addressCorrectionCharges = addressCorrectionCharges;
	}
	public double getReversePickup_slab() {
		return reversePickup_slab;
	}
	public void setReversePickup_slab(double reversePickup_slab) {
		this.reversePickup_slab = reversePickup_slab;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getHfd_type_pcs_kg() {
		return hfd_type_pcs_kg;
	}
	public void setHfd_type_pcs_kg(String hfd_type_pcs_kg) {
		this.hfd_type_pcs_kg = hfd_type_pcs_kg;
	}
	public int getHfd_free_flr() {
		return hfd_free_flr;
	}
	public void setHfd_free_flr(int hfd_free_flr) {
		this.hfd_free_flr = hfd_free_flr;
	}
	public String getFov_Status() {
		return fov_Status;
	}
	public void setFov_Status(String fov_Status) {
		this.fov_Status = fov_Status;
	}
	public String getFov_Fuel_Excluding() {
		return fov_Fuel_Excluding;
	}
	public void setFov_Fuel_Excluding(String fov_Fuel_Excluding) {
		this.fov_Fuel_Excluding = fov_Fuel_Excluding;
	}

	
}
