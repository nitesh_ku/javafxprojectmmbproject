package com.onesoft.courier.master.vas.bean;

public class VAS_Bean {
	
	private int slno;
	private String vasCode;
	private String vasName;
	
	
	
	public String getVasCode() {
		return vasCode;
	}
	public void setVasCode(String vasCode) {
		this.vasCode = vasCode;
	}
	public String getVasName() {
		return vasName;
	}
	public void setVasName(String vasName) {
		this.vasName = vasName;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}

}
