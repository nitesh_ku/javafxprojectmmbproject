package com.onesoft.courier.master.vas.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class VAS_Table_Bean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty vasCode;
	private SimpleStringProperty vasName;
	private SimpleDoubleProperty vasAmount;
	
	
	public VAS_Table_Bean(int slno,String vascode,String vasname, double vasamount)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.vasCode=new SimpleStringProperty(vascode);
		this.vasName=new SimpleStringProperty(vasname);
		this.vasAmount=new SimpleDoubleProperty(vasamount);
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getVasCode() {
		return vasCode.get();
	}
	public void setVasCode(SimpleStringProperty vasCode) {
		this.vasCode = vasCode;
	}
	public String getVasName() {
		return vasName.get();
	}
	public void setVasName(SimpleStringProperty vasName) {
		this.vasName = vasName;
	}

	public double getVasAmount() {
		return vasAmount.get();
	}

	public void setVasAmount(SimpleDoubleProperty vasAmount) {
		this.vasAmount = vasAmount;
	}
	

}
