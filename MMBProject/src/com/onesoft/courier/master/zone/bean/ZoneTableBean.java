package com.onesoft.courier.master.zone.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ZoneTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty zoneCode;
	private SimpleStringProperty zoneName;
	
	
	public ZoneTableBean(int slno, String code, String name)
	{
		
		this.slno=new SimpleIntegerProperty(slno);
		this.zoneCode=new SimpleStringProperty(code);
		this.zoneName=new SimpleStringProperty(name);
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getZoneCode() {
		return zoneCode.get();
	}
	public void setZoneCode(SimpleStringProperty zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getZoneName() {
		return zoneName.get();
	}
	public void setZoneName(SimpleStringProperty zoneName) {
		this.zoneName = zoneName;
	}
	
	
	

}
