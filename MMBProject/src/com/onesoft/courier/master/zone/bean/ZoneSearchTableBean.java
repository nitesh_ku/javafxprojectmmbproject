package com.onesoft.courier.master.zone.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ZoneSearchTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty pincode;
	private SimpleStringProperty cityName;
	private SimpleStringProperty stateCode;
	private SimpleStringProperty zonecode;
	
	public ZoneSearchTableBean(int slno, String pincode, String cityname, String statecode, String zonecode)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.pincode=new SimpleStringProperty(pincode);
		this.cityName=new SimpleStringProperty(cityname);
		this.stateCode=new SimpleStringProperty(statecode);
		this.zonecode=new SimpleStringProperty(zonecode);
		
	}
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getPincode() {
		return pincode.get();
	}
	public void setPincode(SimpleStringProperty pincode) {
		this.pincode = pincode;
	}
	public String getCityName() {
		return cityName.get();
	}
	public void setCityName(SimpleStringProperty cityName) {
		this.cityName = cityName;
	}
	public String getStateCode() {
		return stateCode.get();
	}
	public void setStateCode(SimpleStringProperty stateCode) {
		this.stateCode = stateCode;
	}
	public String getZonecode() {
		return zonecode.get();
	}
	public void setZonecode(SimpleStringProperty zonecode) {
		this.zonecode = zonecode;
	}
	
	
	
	

}
