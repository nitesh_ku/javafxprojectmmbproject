package com.onesoft.courier.master.zone.controller;


import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.CheckListView;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.OutwardBean;
import com.onesoft.courier.booking.bean.OutwardForwardedTableBean;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.common.bean.LoadZoneBean;
import com.onesoft.courier.forwarder.bean.AddOnExpensesBean;
import com.onesoft.courier.forwarder.bean.AddOn_AvailableExpensesTableBean;
import com.onesoft.courier.forwarder.bean.AddOn_SelectedExpensesTableBean;
import com.onesoft.courier.forwarder.controller.ForwarderDetailsController;
import com.onesoft.courier.master.location.bean.AreaBean;
import com.onesoft.courier.master.location.bean.CityBean;
import com.onesoft.courier.master.zone.bean.ZoneBean;
import com.onesoft.courier.master.zone.bean.ZoneMappingBean;
import com.onesoft.courier.master.zone.bean.ZoneSearchTableBean;
import com.onesoft.courier.master.zone.bean.ZoneTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Location_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_Zone_Utils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

public class ZoneController implements Initializable {
	
	private ObservableList<ZoneSearchTableBean> tabledata_UnMapped_ZoneItems=FXCollections.observableArrayList();
	private ObservableList<ZoneTableBean> tabledata_ZoneItems=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxZoneItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxZoneItems_SearchTab=FXCollections.observableArrayList();
	
	
	private ObservableList<String> checkListView_ServicesItems=FXCollections.observableArrayList();
	
	private ObservableList<String> listViewStateItems=FXCollections.observableArrayList();
	private ObservableList<String> listViewCityOrCountryItems=FXCollections.observableArrayList();
	private ObservableList<String> listViewSelectedCityItems=FXCollections.observableArrayList();
	
	
	private ObservableList<String> listViewMappedStates_Item_SearchTab=FXCollections.observableArrayList();
	private ObservableList<String> listViewMappedCities_Item_SearchTab=FXCollections.observableArrayList();
	private ObservableList<String> listViewPincodeWith_Service_Item_SearchTab=FXCollections.observableArrayList();
	
	public Set<String> setList_States=new HashSet<>();
	public Set<String> setList_Cities=new HashSet<>();
	public Set<String> setList_SelectedCities=new HashSet<>();
	public Set<String> setList_Services=new HashSet<>();
	
	
	
	
	public  List<CityBean> list_loadCity=new ArrayList<>();
	
	public int listView_cities_index=0;
	
	@FXML
	private Pagination pagination_UnMappted_CityTable;
	
	@FXML
	private CheckListView<String> checkListView_services;
	
	@FXML
	private Tab tabZoneMapping;
	
	@FXML
	private Tab tabSearch;
	
	@FXML
	private TextField txtZoneCode;
	
	@FXML
	private TextField txtZoneName;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset_ZoneMappingTab;
	
	@FXML
	private Button btnUpdate_SearchTab;
	
	@FXML
	private RadioButton rdBtnDomesticState;
	
	@FXML
	private RadioButton rdBtnDomesticCity;
	
	@FXML
	private RadioButton rdBtnInternational;
	
	@FXML
	private ComboBox<String> comboBoxZone;
	
	@FXML
	private ListView<String> listViewZoneStates;
	
	@FXML
	private ListView<String> listViewZoneCitiesAndStates;
	
	@FXML
	private ListView<String> listViewSelectedZoneCities;
	
	@FXML
	private Button btnInCities;
	
	@FXML
	private Button btnOutCities;

// =========================================================================	

	@FXML
	private TableView<ZoneTableBean> tableZone;
	
	@FXML
	private TableColumn<ZoneTableBean, Integer> tabCol_slno;
	
	@FXML
	private TableColumn<ZoneTableBean, String> tabCol_ZoneCode;
	
	@FXML
	private TableColumn<ZoneTableBean, String> tabCol_ZoneName;
	
	
// =========================================================================	

	@FXML
	private TableView<ZoneSearchTableBean> tableUnMappedCities;

	@FXML
	private TableColumn<ZoneSearchTableBean, Integer> tabCol_unMap_slno;

	@FXML
	private TableColumn<ZoneSearchTableBean, String> tabCol_unMap_ZoneCode;

	@FXML
	private TableColumn<ZoneSearchTableBean, String> tabCol_unMap_CityName;	
	
	@FXML
	private TableColumn<ZoneSearchTableBean, String> tabCol_unMap_StateCode;

	@FXML
	private TableColumn<ZoneSearchTableBean, String> tabCol_unMap_Pincode;	
 
// =========================================================================	
	
	@FXML
	private ComboBox<String> comboBoxZone_SearchTab;
	
	@FXML
	private ListView<String> listViewMappedStates_SearchTab;
	
	@FXML
	private ListView<String> listViewMappedCities_SearchTab;
	
	@FXML
	private ListView<String> listViewPincodeAndServices_SearchTab;
	
	
	
// ******************************************************************************

	public void loadServicesWithName() throws SQLException 
	{
		new LoadServiceGroups().loadServicesWithName();

		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME) 
		{
			setList_Services.add(bean.getServiceName()+" | "+bean.getServiceCode());

		}
		
		for(String services: setList_Services)
		{
			checkListView_ServicesItems.add(services);
		}

	}	

// ******************************************************************************

	public void loadZone() throws SQLException 
	{
		new LoadZone().loadZoneWithName();

		for (LoadZoneBean bean : LoadZone.SET_LOAD_ZONE_WITH_NAME) 
		{
			comboBoxZoneItems.add(bean.getZoneName()+" | "+bean.getZoneCode());
			comboBoxZoneItems_SearchTab.add(bean.getZoneName()+" | "+bean.getZoneCode());
		}
	
		comboBoxZone.setItems(comboBoxZoneItems);
		comboBoxZone_SearchTab.setItems(comboBoxZoneItems_SearchTab);
			
	}	
	
	
// =========================================================================
	
	public void saveZone() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		ZoneBean zBean=new ZoneBean();
		LoadZoneBean lzBean=new LoadZoneBean();
		zBean.setZoneCode(txtZoneCode.getText().toUpperCase());
		zBean.setZoneName(txtZoneName.getText());
		
		lzBean.setZoneCode(txtZoneCode.getText().toUpperCase());
		lzBean.setZoneName(txtZoneName.getText());
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_Zone_Utils.INSERT_SQL_ZONE;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, zBean.getZoneCode());
			preparedStmt.setString(2, zBean.getZoneName());
			preparedStmt.setString(3, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(4, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);
			
			LoadZone.SET_LOAD_ZONE_WITH_NAME.add(lzBean);
			
			comboBoxZoneItems.add(zBean.getZoneName()+" | "+zBean.getZoneCode());
			
			int status=preparedStmt.executeUpdate();
			
			if(status==1)
			{
				reset();
				//loadAreaTable();
				
				loadZoneTable();
				btnSave.setDisable(false);
				btnUpdate.setDisable(true);
				alert.setContentText("Zone Details Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Zone Details not saved \nPlease try again...!");
				alert.showAndWait();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	
	
// ******************************************************************************
	
	public void getZoneDetails() throws SQLException 
	{
		ResultSet rs = null;
		// Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		ZoneBean zBean=new ZoneBean();
		zBean.setZoneCode(txtZoneCode.getText().toUpperCase());

		try {

			sql = MasterSQL_Zone_Utils.DATA_RETRIEVE_SQL_ZONE;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, zBean.getZoneCode());

			rs = preparedStmt.executeQuery();

			if (!rs.next() == true) 
			{
				
			}
			else 
			{
				do {

					txtZoneCode.setEditable(false);
					btnSave.setDisable(true);
					btnUpdate.setDisable(false);

					zBean.setZoneCode(rs.getString("code"));
					zBean.setZoneName(rs.getString("name"));

					txtZoneName.setText(zBean.getZoneName());
					
				} while (rs.next());
			}

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	


// ******************************************************************************

	public void updateZoneDetails() throws SQLException {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		ZoneBean zBean=new ZoneBean();
		
		zBean.setZoneCode(txtZoneCode.getText().toUpperCase());
		zBean.setZoneName(txtZoneName.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_Zone_Utils.UPDATE_SQL_ZONE;
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, zBean.getZoneName());
			preparedStmt.setString(2, zBean.getZoneCode());
			
			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				reset();
			//	loadAreaTable();
				alert.setContentText("Zone Details Successfully Updated...");
				alert.showAndWait();

			} else {
				btnSave.setDisable(true);
				btnUpdate.setDisable(false);
				alert.setContentText("Zone Details not updated \nPlease try again...!");
				alert.showAndWait();
			}

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
// ******************************************************************************
	
	public void loadZoneTable() throws SQLException 
	{
		tabledata_ZoneItems.clear();
		
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int slno = 1;

		try 
		{
			stmt = con.createStatement();
			sql = MasterSQL_Zone_Utils.LOADTABLE_SQL_ZONE;
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) 
			{
				ZoneBean zBean=new ZoneBean();

				zBean.setZoneCode(rs.getString("code"));
				zBean.setZoneName(rs.getString("name"));
				
				tabledata_ZoneItems.add(new ZoneTableBean(slno, zBean.getZoneCode(), zBean.getZoneName()));
				slno++;
			}

			tableZone.setItems(tabledata_ZoneItems);

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		
		if (n.getId().equals("txtZoneCode")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				// checkAreaCodeExistanceAndLoad();
				txtZoneName.requestFocus();
			}
		} 
		else if (n.getId().equals("txtZoneName")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				if (txtZoneCode.isEditable() == true) 
				{
					btnSave.requestFocus();
				} 
				else 
				{
					btnUpdate.requestFocus();
				}
			}
		}
	
		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				setValidation();
				txtZoneCode.requestFocus();
				//System.out.println("Save running...");

			}
		}

		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
				txtZoneCode.requestFocus();
				//System.out.println("Update running...");

			}
		}

	}
	
	
// *************** Method to set Validation *******************

	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
			
		if (txtZoneCode.getText() == null || txtZoneCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtZoneCode.requestFocus();
		} 
		
		else if (txtZoneName.getText() == null || txtZoneName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtZoneName.requestFocus();
		}
		
		else 
		{
			if(btnSave.isDisable()==true)
			{
				System.err.println("Update");
				updateZoneDetails();
			}
			else
			{
				System.err.println("Save");
				saveZone();
			}
		}
	}	

// =========================================================================	

	public void reset()
	{
		txtZoneCode.clear();
		txtZoneName.clear();
		
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
		
		txtZoneCode.setEditable(true);
	}
	
	
// =========================================================================	

	public void loadStatesInListView() throws SQLException
	{
		new LoadState().loadStateFromPincodeMaster();
		
		for (LoadCityBean bean : LoadState.SET_LOAD_STATEWITHNAME_FROM_PINCODEMASTER) 
		{
			if(bean.getCountryCode().equals("IN"))
			{
			setList_States.add(bean.getStateName()+" | "+bean.getStateCode());
			}
		}
		
		System.out.println("State list size: "+setList_States.size());
		
		if(rdBtnDomesticCity.isSelected()==true)
		{
			for(String state: setList_States)
			{
				listViewStateItems.add(state);
			}
			
			Collections.sort(listViewStateItems);
			listViewZoneStates.setItems(listViewStateItems);
		}
		else if(rdBtnDomesticState.isSelected()==true)
		{
			listViewZoneCitiesAndStates.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			for(String state: setList_States)
			{
				listViewCityOrCountryItems.add(state);
			}
			
			Collections.sort(listViewCityOrCountryItems);
			listViewZoneCitiesAndStates.setItems(listViewCityOrCountryItems);
			
		}
		
		
		
	}
	
	

			
	
	
// ******************************************************************************

	public void loadCityInCityListView(String statecode) throws SQLException
	{
		listViewZoneCitiesAndStates.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		listViewCityOrCountryItems.clear();
		
		
		new LoadCity().loadCityWithNameFromPinCodeMaster();
		
		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
		{
			if(bean.getStateCode().equals(statecode))
			{
				listViewCityOrCountryItems.add(bean.getCityName()+" | "+bean.getStateCode());
			}
		}
		
		Collections.sort(listViewCityOrCountryItems);
		listViewZoneCitiesAndStates.setItems(listViewCityOrCountryItems);
	}		
	
	
	public void clickAvailableRow() throws Exception 
	{
		
		listViewZoneCitiesAndStates.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		        	int index=0;
		        //	cell.getIndex()
		            String item = cell.getItem();
		            String[] citycode=item.replaceAll("\\s+","").split("\\|");
		            
		            listView_cities_index=cell.getIndex();
		         
		         
		         
		         /*SetList_SelectedCities.add(item);
		         listViewSelectedZoneCities.setItems(value);
		         listViewCityOrCountryItems.remove(index);*/
		         

		        }
		    });
		    return cell ;
		});
	}	
	
// =========================================================================		

	public void loadSelectedCitiesListView() throws SQLException
	{
		ObservableList<Integer> selectedIndices = listViewZoneCitiesAndStates.getSelectionModel().getSelectedIndices();
		
		for(Integer i:selectedIndices)
		{
			setList_SelectedCities.add(listViewCityOrCountryItems.get(i));
			listViewSelectedCityItems.add(listViewCityOrCountryItems.get(i));
		}
		
		Collections.sort(listViewSelectedCityItems);
		listViewSelectedZoneCities.setItems(listViewSelectedCityItems);
		
		
		
		for(String selectedCityName:setList_SelectedCities)
		{
			for(String cityName:listViewCityOrCountryItems)
			{
				if(selectedCityName.equals(cityName))
				{
					int index=listViewCityOrCountryItems.indexOf(cityName);
					listViewCityOrCountryItems.remove(index);
					break;
				}
			}
		}
		
		listViewZoneCitiesAndStates.getSelectionModel().clearSelection();
		
		if(setList_SelectedCities.size()>0)
		{
			checkCityExistanceAgainstZone(setList_SelectedCities);
			//saveZoneMappingDataToDB(SetList_SelectedCities);
		}
		
		setList_SelectedCities.clear();
		
	}
	

// ========================================================================	
	
	public void checkCityExistanceAgainstZone(Set<String> setCityItems) throws SQLException
	{
		String[] zoneCode=comboBoxZone.getValue().replaceAll("\\s+","").split("\\|");
		ObservableList<String> checked_Services=checkListView_services.getCheckModel().getCheckedItems();
		
		ZoneMappingBean zmBean=new ZoneMappingBean();
		zmBean.setZoneCode(zoneCode[1]);
	
		String serviceToSave=null;
		String serviceFromDB=null;
		String[] srvcArrayFromDB=null;
		String[] srvcArrayFromGUI=null;
		String[] serviceCodeArray=null;
		
		
		if(rdBtnDomesticState.isSelected()==true)
		{
			zmBean.setType(rdBtnDomesticState.getText());
		}
		else
		{
			zmBean.setType(rdBtnInternational.getText());
		}
		
		for(String cityOrState:setCityItems)
		{
			String[] cityNameOrStateCode=cityOrState.replaceAll("\\s+","").split("\\|");
			serviceFromDB=checkedServicesMapWithCityOrState(cityNameOrStateCode, zmBean.getZoneCode());
			System.out.println("Service From DB >> "+serviceFromDB);
			
			if(serviceFromDB!=null && serviceFromDB.length()>0)
			{
				if(checked_Services.size()>0)
				{
				srvcArrayFromDB=serviceFromDB.replaceAll("\\s+","").split("\\,");
				for(String checkedservices:checked_Services)
				{
					serviceCodeArray=checkedservices.replaceAll("\\s+","").split("\\|");
					serviceToSave=serviceToSave+","+serviceCodeArray[1];
					System.out.println("Service to Save: "+serviceToSave);
				}
			
				srvcArrayFromGUI=serviceToSave.substring(5).replaceAll("\\s+","").split("\\,");
				for(String sc:srvcArrayFromGUI)
				{
					if(!Arrays.asList(srvcArrayFromDB).contains(sc))
					{
					serviceFromDB=serviceFromDB+","+sc;
					}
				}
				
				zmBean.setServices(serviceFromDB);
				}
				else
				{
					zmBean.setServices("");	
				}
			}
			else
			{
				if(checked_Services.size()>0)
				{
				for(String checkedservices:checked_Services)
				{
					serviceCodeArray=checkedservices.replaceAll("\\s+","").split("\\|");
					serviceToSave=serviceToSave+","+serviceCodeArray[1];
					System.out.println("Service to Save: "+serviceToSave);
				}
				
				if(serviceToSave.contains(","))
				{
					zmBean.setServices(serviceToSave.substring(5));
					System.out.println(zmBean.getServices());
				}
				else
				{
					zmBean.setServices(serviceToSave);
					System.out.println(zmBean.getServices());
				}
				}
				else
				{
					zmBean.setServices("");	
				}
			}
			
			System.out.println("New Service with existing service: "+serviceFromDB);
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
	
			PreparedStatement preparedStmt=null;
			ResultSet rs=null;
	
			try
			{	
				
				if(rdBtnDomesticState.isSelected()==true)
				{
					System.out.println("Zone :"+zmBean.getZoneCode()+" | State Code: "+cityNameOrStateCode[1]);
					String query = MasterSQL_Zone_Utils.UPDATE_SQL_ZONE_MAPPING_VIA_STATECODE;
					preparedStmt = con.prepareStatement(query);
					
					preparedStmt.setString(1, zmBean.getZoneCode());
					preparedStmt.setString(2, zmBean.getServices());
					preparedStmt.setString(3, cityNameOrStateCode[1]);
					
					preparedStmt.executeUpdate();
					serviceToSave=null;
					serviceCodeArray=null;
					//checked_Services.clear();
				}
				else if(rdBtnDomesticCity.isSelected()==true)
				{
					System.out.println("Zone :"+zmBean.getZoneCode()+" | City Name: "+cityNameOrStateCode[0]);
					String query = MasterSQL_Zone_Utils.UPDATE_SQL_ZONE_MAPPING_VIA_CITYNAME;
					preparedStmt = con.prepareStatement(query);
					
					preparedStmt.setString(1, zmBean.getZoneCode());
					preparedStmt.setString(2, zmBean.getServices());
					preparedStmt.setString(3, cityNameOrStateCode[0]);
					
					preparedStmt.executeUpdate();
					
					serviceToSave=null;
					serviceCodeArray=null;
				//	checked_Services.clear();
				}
					
				
				/*if(!rs.next()==true)
				{
					saveZoneMappingDataToDB(zmBean.getZoneCode(), zmBean.getStateCode(), cityNameOrStateCode[1], zmBean.getType());
				}*/
				
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}	
			
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
		}
	}
	
// =========================================================================
	
	public String checkedServicesMapWithCityOrState(String[] cityOrState,String zone_code) throws SQLException
	{
		
		ResultSet rs = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;
		
		String servicesFromDB=null;

		try 
		{
			if(rdBtnDomesticState.isSelected()==true)
			{
			
			sql = MasterSQL_Zone_Utils.CHECK_EXISTING_SERVICE_MAPPING_WITH_STATECODE;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,zone_code);
			preparedStmt.setString(2,cityOrState[1]);
			
			System.out.println("PS Sql 1: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
				servicesFromDB=rs.getString("service_code");
			}
			
			}
			else if(rdBtnDomesticCity.isSelected()==true)
			{
				sql = MasterSQL_Zone_Utils.CHECK_EXISTING_SERVICE_MAPPING_WITH_CITYNAME;

				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,zone_code);
				preparedStmt.setString(2,cityOrState[0]);
				
				System.out.println("PS Sql 2: "+ preparedStmt);
				rs = preparedStmt.executeQuery();
				
				while (rs.next()) 
				{
					servicesFromDB=rs.getString("service_code");
				}
			}
			
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		return servicesFromDB;
		
		
	}
	
// =========================================================================
	
	public void saveZoneMappingDataToDB(String zone,String state,String city,String type) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_Zone_Utils.INSERT_SQL_ZONE_MAPPING;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, city);
			preparedStmt.setString(2, state);
			preparedStmt.setString(3, zone);
			preparedStmt.setString(4, type);
			preparedStmt.setString(5, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(6, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);
		
			preparedStmt.executeUpdate();
			
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// =========================================================================

	public void removeItemsFromSelectedCitiesListView() 
	{
		ObservableList<Integer> selectedIndices = listViewSelectedZoneCities.getSelectionModel().getSelectedIndices();

		for (int i : selectedIndices) 
		{
			setList_Cities.add(listViewSelectedCityItems.get(i));
			listViewCityOrCountryItems.add(listViewSelectedCityItems.get(i));
		}
		
		listViewZoneCitiesAndStates.setItems(listViewCityOrCountryItems);

		for (String selectedCityName : setList_Cities) 
		{
			for (String cityName : listViewSelectedCityItems)
			{
				if (selectedCityName.equals(cityName)) 
				{
					int index = listViewSelectedCityItems.indexOf(cityName);
					listViewSelectedCityItems.remove(index);
					break;
				}
			}
		}
		
		listViewSelectedZoneCities.getSelectionModel().clearSelection();
		
		if(setList_SelectedCities.size()>0)
		{
			removeSelectedCitiesFromDB(setList_SelectedCities);
			//saveZoneMappingDataToDB(SetList_SelectedCities);
		}
		
		setList_Cities.clear();
	}	
		

// =========================================================================	

	public void removeSelectedCitiesFromDB(Set<String> setCityName)
	{
		
	}
	
// =========================================================================	

	public void loadCountryinListView() throws SQLException
	{
		new LoadCountry().loadCountryWithName();

		for (LoadCityBean bean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			if(!bean.getCountryCode().equals("IN"))
			{
				listViewCityOrCountryItems.add(bean.getCountryName() + " | " + bean.getCountryCode());
			}
		}
		listViewZoneCitiesAndStates.setItems(listViewCityOrCountryItems);
			//TextFields.bindAutoCompletion(txtCountry, list_Country);
			
		}
	
	
// =========================================================================

	public void selectDomesticOrInternational() throws SQLException
	{
		if(rdBtnDomesticCity.isSelected()==true)
		{
			comboBoxZoneItems.clear();
			
			listViewCityOrCountryItems.clear();
			listViewSelectedCityItems.clear();
			
			comboBoxZone.setDisable(false);
			btnInCities.setDisable(false);
			btnOutCities.setDisable(false);
			
			listViewZoneStates.setDisable(false);
			listViewSelectedZoneCities.setDisable(false);
			for (LoadZoneBean bean : LoadZone.SET_LOAD_ZONE_WITH_NAME) 
			{
				comboBoxZoneItems.add(bean.getZoneName()+" | "+bean.getZoneCode());
			}
			
			if(LoadZone.SET_LOAD_ZONE_WITH_NAME.size()>0)
			{
				comboBoxZone.setValue(comboBoxZoneItems.get(0));
			}
			
			try {
				
				loadStatesInListView();
				clickAvailableRow();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		else if(rdBtnDomesticState.isSelected()==true)
		{
			comboBoxZoneItems.clear();
			listViewCityOrCountryItems.clear();
			listViewStateItems.clear();
			listViewSelectedCityItems.clear();
			
			comboBoxZone.setDisable(false);
			btnInCities.setDisable(false);
			btnOutCities.setDisable(false);
			
			listViewZoneStates.setDisable(true);
			listViewSelectedZoneCities.setDisable(false);
			
			for (LoadZoneBean bean : LoadZone.SET_LOAD_ZONE_WITH_NAME) 
			{
				comboBoxZoneItems.add(bean.getZoneName()+" | "+bean.getZoneCode());
			}
		
			if(LoadZone.SET_LOAD_ZONE_WITH_NAME.size()>0)
			{
				comboBoxZone.setValue(comboBoxZoneItems.get(0));
			}
			
			comboBoxZone.setValue(comboBoxZoneItems.get(0));
			comboBoxZone.setItems(comboBoxZoneItems);
			
			try {
				
				loadStatesInListView();
				clickAvailableRow();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		else
		{
			comboBoxZoneItems.clear();
			listViewCityOrCountryItems.clear();
			listViewSelectedCityItems.clear();
			
			btnInCities.setDisable(true);
			btnOutCities.setDisable(true);
			
			loadCountryinListView();
			
			comboBoxZone.setDisable(true);
			listViewZoneStates.setDisable(true);
			listViewSelectedZoneCities.setDisable(true);
			
			comboBoxZone.getSelectionModel().clearSelection();
			listViewStateItems.clear();
			
		}
	}
	
	public void tabZoneMappingSelected() throws SQLException
	{
		rdBtnDomesticState.setSelected(true);
		checkListView_services.getCheckModel().clearChecks();
		selectDomesticOrInternational();
	}
	
// =========================================================================	

	public void clearSelectedCitiesListView()
	{
		listViewSelectedCityItems.clear();
		setList_SelectedCities.clear();
		
		//checkListView_services.getCheckModel().clearChecks();
		/*ObservableList<String> checked_Services=checkListView_services.getCheckModel().getCheckedItems();
		
		for(String checked:checked_Services)
		{
			System.out.println(checked);
		}*/
	}
	
	
	

	
	
//***************************************************************************************************************************	
//						Tab >> Search Code Block Start
//***************************************************************************************************************************
	
	public void getMappedStatesViaZone() throws SQLException
	{
		if(comboBoxZone_SearchTab.getValue()!=null)
		{
			listViewMappedStates_Item_SearchTab.clear();
		listViewMappedCities_Item_SearchTab.clear();
		listViewPincodeWith_Service_Item_SearchTab.clear();
		
		
		String[] zoneCode=null;
		
		
			zoneCode=comboBoxZone_SearchTab.getValue().replaceAll("\\s+","").split("\\|");
		
			
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;

		int slno = 1;

		try 
		{
			sql = MasterSQL_Zone_Utils.DATA_RETRIEVE_SQL_STATES_VIA_ZONE;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,zoneCode[1]);
			
			System.out.println("Zone Search Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				ZoneBean zBean=new ZoneBean();

				zBean.setZoneCode(rs.getString("pm_state_code"));
				zBean.setZoneName(rs.getString("sm_state_name"));
				listViewMappedStates_Item_SearchTab.add(zBean.getZoneName()+" | "+zBean.getZoneCode());
				
			}
			listViewMappedStates_SearchTab.setItems(listViewMappedStates_Item_SearchTab);
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
		}
	}
		
	
	
// =========================================================================
	
	public void getMappedCitiesViaZoneAndStates(String stateCode) throws SQLException
	{
		listViewMappedCities_Item_SearchTab.clear();
		
		String[] zoneCode=comboBoxZone_SearchTab.getValue().replaceAll("\\s+","").split("\\|");
			
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;

		int slno = 1;

		try 
		{
			
			sql = MasterSQL_Zone_Utils.DATA_RETRIEVE_SQL_CITIES_VIA_ZONE_AND_STATES;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,zoneCode[1]);
			preparedStmt.setString(2,stateCode);
			
			System.out.println("Zone Search Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				ZoneBean zBean=new ZoneBean();
				zBean.setCity_name(rs.getString("city_name"));
				listViewMappedCities_Item_SearchTab.add(zBean.getCity_name());
			}

			listViewMappedCities_SearchTab.setItems(listViewMappedCities_Item_SearchTab);

		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// ========================================================================

	public void getPincodeViaCity(String cityName) throws SQLException
	{
		listViewPincodeWith_Service_Item_SearchTab.clear();
		
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;

		try 
		{
			sql = MasterSQL_Zone_Utils.DATA_RETRIEVE_SQL_PINCODE_AND_SERVICE_VIA_CITY;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,cityName);
		//	preparedStmt.setString(2,stateCode);
			
		//	System.out.println("Zone Search Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				if(rs.getString("service_code")==null || rs.getString("service_code").isEmpty())
					
				{
					listViewPincodeWith_Service_Item_SearchTab.add(rs.getString("pincode"));
				}
				else
				{
					listViewPincodeWith_Service_Item_SearchTab.add(rs.getString("pincode")+" | "+rs.getString("service_code"));	
				}
				
			}

			listViewPincodeAndServices_SearchTab.setItems(listViewPincodeWith_Service_Item_SearchTab);
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
// ========================================================================
	
	/*public void loadUnMapppedZoneTable() throws SQLException 
	{
		tabledata_ZoneItems.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int slno = 1;

		try 
		{
			stmt = con.createStatement();
			sql = MasterSQL_Zone_Utils.LOADTABLE_SQL_UNMAPPED_ZONE;
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) 
			{
				ZoneBean zBean=new ZoneBean();

				zBean.setSlno(slno);
				zBean.setPincode(rs.getString("pincode"));
				zBean.setCity_name(rs.getString("city_name"));
				zBean.setStateCode(rs.getString("state_code"));
				zBean.setZoneCode(rs.getString("zone_code"));
				

				tabledata_UnMapped_ZoneItems.add(new ZoneSearchTableBean(slno, zBean.getPincode(), zBean.getCity_name(), zBean.getStateCode(), zBean.getZoneCode()));
				slno++;
			}

			tableUnMappedCities.setItems(tabledata_UnMapped_ZoneItems);
			
			pagination_UnMappted_CityTable.setPageCount((tabledata_UnMapped_ZoneItems.size() / rowsPerPage() + 1));
			pagination_UnMappted_CityTable.setCurrentPageIndex(0);
			pagination_UnMappted_CityTable.setPageFactory((Integer pageIndex) -> createPage_UnMappedCityTable(pageIndex));

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}*/
	
	
	public void loadUnMapppedZoneTable() throws SQLException
	{
		tabledata_ZoneItems.clear();
		
		int slno=1;
		
		new LoadPincodeForAll().loadPincode();
		for(LoadPincodeBean pinBean: LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pinBean.getZone_code()==null)
			{
				tabledata_UnMapped_ZoneItems.add(new ZoneSearchTableBean(slno, pinBean.getPincode(), pinBean.getCity_name(),
						pinBean.getService_code(), pinBean.getZone_code()));
				slno++;
			}
		}
		
		tableUnMappedCities.setItems(tabledata_UnMapped_ZoneItems);
		
		pagination_UnMappted_CityTable.setPageCount((tabledata_UnMapped_ZoneItems.size() / rowsPerPage() + 1));
		pagination_UnMappted_CityTable.setCurrentPageIndex(0);
		pagination_UnMappted_CityTable.setPageFactory((Integer pageIndex) -> createPage_UnMappedCityTable(pageIndex));
	}


// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 50;
	}

	public GridPane createPage_UnMappedCityTable(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_UnMapped_ZoneItems.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_UnMapped_ZoneItems.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableUnMappedCities.setItems(FXCollections.observableArrayList(tabledata_UnMapped_ZoneItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableUnMappedCities.setItems(FXCollections.observableArrayList(tabledata_UnMapped_ZoneItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

// ========================================================================
	
	public void resetSearchTab()
	{
		listViewMappedCities_Item_SearchTab.clear();
		listViewMappedStates_Item_SearchTab.clear();
		listViewPincodeWith_Service_Item_SearchTab.clear();
		comboBoxZone_SearchTab.getSelectionModel().clearSelection();
	}
	
// ========================================================================
	
	public void resetZoneMappingTab() throws SQLException
	{
		rdBtnDomesticState.setSelected(true);
		if(LoadZone.SET_LOAD_ZONE_WITH_NAME.size()>0)
		{
			comboBoxZone.setValue(comboBoxZoneItems.get(0));
		}
		listViewZoneStates.setDisable(true);
		listViewStateItems.clear();
		loadStatesInListView();
		
		checkListView_services.getCheckModel().clearChecks();
		listViewSelectedCityItems.clear();
		listViewCityOrCountryItems.clear();
		loadStatesInListView();
		
		
	}
	
// ========================================================================
	
	public void resetZoneTab()
	{
		
	}
	
// ========================================================================		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		try 
		{
			loadZone();
			loadZoneTable();
			loadUnMapppedZoneTable();
			loadStatesInListView();
			clickAvailableRow();
			loadServicesWithName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	checkListView_services.getSelectionModel().getSelectedIndices();
		
		btnUpdate.setDisable(true);
		listViewZoneStates.setDisable(true);
		
		//listViewPincodeAndServices_SearchTab.setItems(listViewMappedStates_Item_SearchTab);
		
		if(LoadZone.SET_LOAD_ZONE_WITH_NAME.size()>0)
		{
			comboBoxZone.setValue(comboBoxZoneItems.get(0));
		}
		
		tabCol_slno.setCellValueFactory(new PropertyValueFactory<ZoneTableBean,Integer>("slno"));
		tabCol_ZoneCode.setCellValueFactory(new PropertyValueFactory<ZoneTableBean,String>("zoneCode"));
		tabCol_ZoneName.setCellValueFactory(new PropertyValueFactory<ZoneTableBean,String>("zoneName"));
		
		
		tabCol_unMap_slno.setCellValueFactory(new PropertyValueFactory<ZoneSearchTableBean,Integer>("slno"));
		tabCol_unMap_Pincode.setCellValueFactory(new PropertyValueFactory<ZoneSearchTableBean,String>("pincode"));
		tabCol_unMap_CityName.setCellValueFactory(new PropertyValueFactory<ZoneSearchTableBean,String>("cityName"));
		tabCol_unMap_StateCode.setCellValueFactory(new PropertyValueFactory<ZoneSearchTableBean,String>("stateCode"));
		tabCol_unMap_ZoneCode.setCellValueFactory(new PropertyValueFactory<ZoneSearchTableBean,String>("zonecode"));
		
		
		listViewSelectedZoneCities.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		//checkListView_services.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		checkListView_services.setItems(checkListView_ServicesItems);
		
		
		listViewZoneStates.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		            String[] statecode=item.replaceAll("\\s+","").split("\\|");
		            
		            try {
		            	
						loadCityInCityListView(statecode[1]);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});
		
		
		listViewMappedStates_SearchTab.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		            String[] statecode=item.replaceAll("\\s+","").split("\\|");
		            
		            try 
		            {
		            	listViewPincodeWith_Service_Item_SearchTab.clear();
						getMappedCitiesViaZoneAndStates(statecode[1]);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});
		
		
		listViewMappedCities_SearchTab.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		            //String[] statecode=item.replaceAll("\\s+","").split("\\|");
		            
		            try {
						getPincodeViaCity(item);
						System.out.println("City Name: "+item);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});
		
		
	 
		
		
	}
	
}
