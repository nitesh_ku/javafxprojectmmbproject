package com.onesoft.courier.master.addclient.controller;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.CheckListView;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadUser;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadUserBean;
import com.onesoft.courier.invoice.bean.InvoiceBean;
import com.onesoft.courier.master.addclient.bean.AddClientBean;
import com.onesoft.courier.sql.queries.MasterSQL_Zone_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AddClientController implements Initializable{

	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxUserItems=FXCollections.observableArrayList();
	
	private ObservableList<String> listView_AvailableClientsItems=FXCollections.observableArrayList();
	private ObservableList<String> listView_AddedClientsItems=FXCollections.observableArrayList();
	
	private Set<String> set_AddedClients=new HashSet<>();
	private Set<String> set_RemovedClients=new HashSet<>();
	private Set<String> set_AvailableClients=new HashSet<>();
	//private Set<String> set_LoadAvailableClients=new HashSet<>();
	
	@FXML
	private Button btnAddClient;
	
	@FXML
	private Button btnRemoveClient;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxUser;
	
	@FXML
	private ListView<String> listView_AvailableClients;
	
	@FXML
	private ListView<String> listView_AddedClients;
	
	
// ====================================================================================================================

	public void loadBranch() throws SQLException
	{	

		new	LoadBranch().loadBranchWithName();

		System.out.println("Network Compare Contorller >> value from LoadBranch Class: >>>>>>>> ");

		//System.out.println("Network Contorller >> Load from Global set object");
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			comboBoxBranchItems.add(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);

	}
	
// ====================================================================================================================

	public void loadUsers() throws SQLException
	{	
		comboBoxUserItems.clear();

		new	LoadUser().loadUserWithID();

		String[] branchcode=null;
		branchcode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(LoadUserBean userBean:LoadUser.SET_LOAD_USER)
		{
			if(branchcode[0].equals(userBean.getBranchCode()))
			{
			comboBoxUserItems.add(userBean.getUserName()+ " | "+userBean.getUserid());
			}
		}
		comboBoxUser.setItems(comboBoxUserItems);

	}	

	
// =============================================================================================

	public void loadClients() throws SQLException 
	{
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			//checkListView_AvailableClientsItems.add(bean.getClientCode() + " | " + bean.getClientName());
			set_AvailableClients.add(bean.getClientCode());

		}
		//checkListView_AvailableClients.setItems(checkListView_AvailableClientsItems);
		//TextFields.bindAutoCompletion(txtClient, list_clients);
	}

// ====================================================================================================================	

	public void AddClientToUserListView() throws SQLException
	{
		
		ObservableList<String> checked_Clients=listView_AvailableClients.getSelectionModel().getSelectedItems();
		
		String clientWithComma=null;
		
		System.out.println("Added Clients :");
		for(String clients:checked_Clients)
		{
			set_AddedClients.add(clients);
			String[] clientCode=null;
			clientCode=clients.replaceAll("\\s+","").split("\\|");
			clientWithComma=clientWithComma+","+clientCode[0];
			//list_clientCodes
			
		}
		
		
		//checkListView_AddedClientsItems.clear();
		for(String addedclients: set_AddedClients)
		{
			if(!listView_AddedClientsItems.contains(addedclients))
			{
				listView_AddedClientsItems.add(addedclients);
			}
			
			listView_AvailableClientsItems.remove(addedclients);
		}
		listView_AddedClients.setItems(listView_AddedClientsItems);
		
		
		
		
		mapClientWithUserInDB();
		
		listView_AvailableClients.getSelectionModel().clearSelection();
		set_AddedClients.clear();
		
		//System.out.println(clientWithComma.substring(5));
		
		/*if(checked_Clients.size()>0)
		{
			mapClientWithUserInDB(clientWithComma.substring(5));
		}*/
		
	}
	
	public void mapClientWithUserInDB() throws SQLException
	{
		String clientWithComma=null;
		String[] clientCode=null;
		
		for(String clients:listView_AddedClientsItems)
		{
			clientCode=clients.replaceAll("\\s+","").split("\\|");
			clientWithComma=clientWithComma+","+clientCode[0];
			//list_clientCodes
		}
		
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		
		String[] userCode=comboBoxUser.getValue().replaceAll("\\s+","").split("\\|");
		String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
		
			
			String query = "update logindetail set clientcode=? where userid=? and branchcode=?";
			//System.out.println("from ============= "+awbno+" "+inbean.getInvoice_status());	
			//System.out.println(inbean.);
			preparedStmt = con.prepareStatement(query);
			
			if(clientWithComma!=null)
			{
				preparedStmt.setString(1,clientWithComma.substring(5));
				preparedStmt.setString(2,userCode[0]);
				preparedStmt.setString(3,branchCode[0]);
			}
			else
			{
				preparedStmt.setString(1,null);
				preparedStmt.setString(2,userCode[0]);
				preparedStmt.setString(3,branchCode[0]);
			}
			preparedStmt.executeUpdate();
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	
// ====================================================================================================================	

	File mainFileDown = new File("src/icon/down_arrow.png");
	File mainFileUP = new File("src/icon/up_arrow.png");
	
	Image imageUp = new Image(mainFileUP.toURI().toString());
	Image imageDown = new Image(mainFileDown.toURI().toString());
	
	public void setAddRemoveGraphics()
	{
		File hoverFileUp = new File("src/icon/Hover_up.png");
		File hoverFileDown = new File("src/icon/Hover_down.png");
		Image hoverImageDown = new Image(hoverFileDown.toURI().toString());
		Image hoverImageUp = new Image(hoverFileUp.toURI().toString());
		
		btnAddClient.setGraphic(new ImageView(hoverImageUp));
		btnRemoveClient.setGraphic(new ImageView(hoverImageDown));
		
	}
	
	public void addClientIcon()
	{
		btnAddClient.setGraphic(new ImageView(imageUp));
	}
	
	public void removeClientIcon()
	{
		btnRemoveClient.setGraphic(new ImageView(imageDown));
		
	}
	
// ====================================================================================================================		
	
	public void loadAddedClientsViaBranchAndUser() throws SQLException
	{
		boolean isClientCodedExist=false;
		
		if(comboBoxBranch.getValue()!=null && comboBoxUser.getValue()!=null)
		{
			String[] userCode=comboBoxUser.getValue().replaceAll("\\s+","").split("\\|");
			String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
			
			ResultSet rs = null;
			String sql = null;
			DBconnection dbcon = new DBconnection();
			Connection con = dbcon.ConnectDB();
	
			PreparedStatement preparedStmt=null;
			
			AddClientBean addBean=new AddClientBean();
			String[] addedClients=null;
			
			try
			{
				sql = "select userid,username,branchcode,clientcode from logindetail where branchcode=? and userid=?";
		
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,branchCode[0]);
				preparedStmt.setString(2,userCode[0]);
				
				System.out.println("PS Sql 2: "+ preparedStmt);
				rs = preparedStmt.executeQuery();
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						addBean.setBranchcode(rs.getString("branchcode"));
						addBean.setUserid(rs.getString("userid"));
						addBean.setUsername(rs.getString("username"));
						addBean.setClientcode(rs.getString("clientcode"));
						
						
						
						if(addBean.getClientcode()!=null)
						{
							isClientCodedExist=true;
							addedClients=addBean.getClientcode().replaceAll("\\s+","").split("\\,");
						}
						else
						{
							isClientCodedExist=false;
							System.out.println("Added clients not found...");
						}
						
						
					}
					while (rs.next());
				}
	
				//set_AddedClients.clear();
				//set_AvailableClients.clear();
				listView_AddedClientsItems.clear();
				listView_AvailableClientsItems.clear();
				
				if(isClientCodedExist==true)
				{
				
					if(addedClients!=null)
					{
						for(String client: addedClients)
						{
							for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
							{
								if(clBean.getClientCode().equals(client))
								{
									listView_AddedClientsItems.add(client+" | "+clBean.getClientName());
								}
							}
						}
						
						for(String avclient: addedClients)
						{
							if(set_AvailableClients.contains(avclient))
							{
								set_AvailableClients.remove(avclient);
							}
						}
					}
					
					
					for(String cl:set_AvailableClients)
					{	
						for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
						{
							if(clBean.getClientCode().equals(cl))
							{
								listView_AvailableClientsItems.add(cl+" | "+clBean.getClientName());
							}
						}
					}
					
					listView_AvailableClients.setItems(listView_AvailableClientsItems);
					listView_AddedClients.setItems(listView_AddedClientsItems);
					
					loadClients();
					isClientCodedExist=false;
				}
				else
				{
					
					for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
					{
						listView_AvailableClientsItems.add(clBean.getClientCode()+" | "+clBean.getClientName());
					}
					
					listView_AvailableClients.setItems(listView_AvailableClientsItems);
					//listView_AddedClients.setItems(listView_AddedClientsItems);
				}
			
				
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, rs, con);
			}
		}
	}
	
	
// ====================================================================================================================		

	public void removeClientsFromAddedList() throws SQLException
	{
		ObservableList<String> removed_Clients=listView_AddedClients.getSelectionModel().getSelectedItems();
		
		//String clientWithComma=null;
		
		System.out.println("Removed Clients :");
		for(String clients:removed_Clients)
		{
			set_RemovedClients.add(clients);
			clients.replaceAll("\\s+","").split("\\|");
			
		}
		
		
		//checkListView_AddedClientsItems.clear();
		for(String removedclients: set_RemovedClients)
		{
			if(!listView_AvailableClientsItems.contains(removedclients))
			{
				listView_AvailableClientsItems.add(removedclients);
			}
			
			listView_AddedClientsItems.remove(removedclients);
		}
		listView_AvailableClients.setItems(listView_AvailableClientsItems);
		
		
		mapClientWithUserInDB();
		
		listView_AddedClients.getSelectionModel().clearSelection();
		set_RemovedClients.clear();
	}
	
// ====================================================================================================================	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		listView_AddedClients.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		listView_AvailableClients.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		try {
			loadBranch();
			loadClients();
			//loadUsers();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setAddRemoveGraphics();
	}

	
}
