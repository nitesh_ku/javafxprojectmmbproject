package com.onesoft.courier.master.employee.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class EmployeeRegistrationTableBean {
	
	private SimpleIntegerProperty slno;

	private SimpleStringProperty phone;
	private SimpleStringProperty website;
	private SimpleStringProperty email;
	private SimpleStringProperty address;
	private SimpleStringProperty city;
	private SimpleStringProperty state;
	private SimpleStringProperty designation;
	private SimpleStringProperty doj;
	private SimpleStringProperty dob;
	private SimpleStringProperty password;
	private SimpleStringProperty loginType;
	private SimpleStringProperty employeeCode;
	private SimpleStringProperty employeeName;
	private SimpleStringProperty status;
	
	
	
	
	public EmployeeRegistrationTableBean(int slno, String empCode, String empName, String phone, String email ,
			String address,String city,String designation, String doj, String dob,String status)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.employeeCode=new SimpleStringProperty(empCode);
		this.employeeName=new SimpleStringProperty(empName);
		this.phone=new SimpleStringProperty(phone);
		this.email=new SimpleStringProperty(email);
		this.address=new SimpleStringProperty(address);
		this.city=new SimpleStringProperty(city);
		this.designation=new SimpleStringProperty(designation);
		this.dob=new SimpleStringProperty(dob);
		this.doj=new SimpleStringProperty(doj);
		this.status=new SimpleStringProperty(status);
		
	}
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}

	public String getPhone() {
		return phone.get();
	}
	public void setPhone(SimpleStringProperty phone) {
		this.phone = phone;
	}
	public String getWebsite() {
		return website.get();
	}
	public void setWebsite(SimpleStringProperty website) {
		this.website = website;
	}
	public String getEmail() {
		return email.get();
	}
	public void setEmail(SimpleStringProperty email) {
		this.email = email;
	}
	public String getAddress() {
		return address.get();
	}
	public void setAddress(SimpleStringProperty address) {
		this.address = address;
	}
	public String getCity() {
		return city.get();
	}
	public void setCity(SimpleStringProperty city) {
		this.city = city;
	}
	public String getState() {
		return state.get();
	}
	public void setState(SimpleStringProperty state) {
		this.state = state;
	}
	public String getDesignation() {
		return designation.get();
	}
	public void setDesignation(SimpleStringProperty designation) {
		this.designation = designation;
	}
	public String getDoj() {
		return doj.get();
	}
	public void setDoj(SimpleStringProperty doj) {
		this.doj = doj;
	}
	public String getDob() {
		return dob.get();
	}
	public void setDob(SimpleStringProperty dob) {
		this.dob = dob;
	}
	public String getPassword() {
		return password.get();
	}
	public void setPassword(SimpleStringProperty password) {
		this.password = password;
	}
	public String getLoginType() {
		return loginType.get();
	}
	public void setLoginType(SimpleStringProperty loginType) {
		this.loginType = loginType;
	}
	public String getEmployeeCode() {
		return employeeCode.get();
	}
	public void setEmployeeCode(SimpleStringProperty employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getEmployeeName() {
		return employeeName.get();
	}
	public void setEmployeeName(SimpleStringProperty employeeName) {
		this.employeeName = employeeName;
	}


	public String getStatus() {
		return status.get();
	}


	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}


}
