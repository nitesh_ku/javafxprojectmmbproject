package com.onesoft.courier.master.employee.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.master.employee.bean.EmployeeRegistrationBean;
import com.onesoft.courier.master.employee.bean.EmployeeRegistrationTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Address_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_Employee_Registration_Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class EmployeeRegistrationDetailsController implements Initializable {
	
	private ObservableList<EmployeeRegistrationTableBean> tabledata_employee_registration=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxStateItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxCountryItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxStatusItems=FXCollections.observableArrayList(MasterSQL_Employee_Registration_Utils.ACTIVE,MasterSQL_Employee_Registration_Utils.IN_ACTIVE);
	private ObservableList<String> comboBoxLoginTypeItems=FXCollections.observableArrayList(MasterSQL_Employee_Registration_Utils.ACTIVE,MasterSQL_Employee_Registration_Utils.IN_ACTIVE);
	
	private List<String> list_City=new ArrayList<>();
	private List<String> list_State=new ArrayList<>();
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public int addressCodeForEmployeeRegistration=0;
	
	public int getAddressCodeforUpdate=0;
	
	
	@FXML
	private ImageView imageView_SearchIcon;
	
	@FXML
	private TextField txtEmployeeCode;

	@FXML
	private TextField txtEmployeeName;
	
	@FXML
	private TextField txtPhone;
	
	@FXML
	private TextField txtEmail;
	
	@FXML
	private TextField txtAddress;
	
	@FXML
	private TextField txtCity;
	
	@FXML
	private TextField txtDesignation;
	
	@FXML
	private TextField txtPassword;
	
	@FXML
	private DatePicker dpkDOB;
	
	@FXML
	private DatePicker dpkDOJ;
	
	@FXML
	private ComboBox<String> comboBoxState;
	
	@FXML
	private ComboBox<String> comboBoxCountry;
	
	@FXML
	private ComboBox<String> comboBoxStatus;
	
	@FXML
	private ComboBox<String> comboBoxLoginType;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset;
	
	
// ===============================================	

	@FXML
	private TableView<EmployeeRegistrationTableBean> tableEmployeeRegistration;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, Integer> tableCol_Slno;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_EmpCode;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_EmpName;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_EmpPhone;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_DOB;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_DOJ;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_Designation;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_Status;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, String> tableCol_Email;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, Double> tableCol_Address;

	@FXML
	private TableColumn<EmployeeRegistrationTableBean, Double> tableCol_City;

	
	
// *******************************************************************************

	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName()+" | "+bean.getCityCode());
		}
		//comboBoxCity.setItems(comboBoxCityItems);
		TextFields.bindAutoCompletion(txtCity, list_City);

	}	

// *******************************************************************************

	public void loadState() throws SQLException 
	{
		new LoadState().loadStateWithName();
	
		for (LoadCityBean bean : LoadState.SET_LOAD_STATEWITHNAME) 
		{
			comboBoxStateItems.add(bean.getStateCode());
		}
		comboBoxState.setItems(comboBoxStateItems);
		//TextFields.bindAutoCompletion(txtState, list_State);
	}		
	
	
// *******************************************************************************
	
	public void loadCountry() throws SQLException 
	{
		new LoadCountry().loadCountryWithName();

		for (LoadCityBean bean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			comboBoxCountryItems.add(bean.getCountryCode());
		}
		 comboBoxCountry.setItems(comboBoxCountryItems);
		//TextFields.bindAutoCompletion(txtCountry, list_Country);
	}	
	
	
// *******************************************************************************
	
	public void saveEmployeeDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		saveAddress();
			
		EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
		
		empBean.setEmployeeCode(txtEmployeeCode.getText());
		empBean.setEmployeeName(txtEmployeeName.getText());
		empBean.setPhone(txtPhone.getText());
		empBean.setEmail(txtEmail.getText());
		empBean.setDesignation(txtDesignation.getText());
		empBean.setPassword(txtPassword.getText());
		
		LocalDate localDOB=dpkDOB.getValue();
		Date dob=Date.valueOf(localDOB);
		LocalDate localDOJ=dpkDOJ.getValue();
		Date doj=Date.valueOf(localDOJ);
				
		if(comboBoxStatus.getValue().equals(MasterSQL_Employee_Registration_Utils.ACTIVE))
		{
			empBean.setStatus("A");
			
		}
		else
		{
			empBean.setStatus("I");
		}
		
		if(comboBoxLoginType.getValue().equals(MasterSQL_Employee_Registration_Utils.ACTIVE))
		{
			empBean.setLoginType("T");
			
		}
		else
		{
			empBean.setLoginType("F");
		}
		
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			String query = MasterSQL_Employee_Registration_Utils.INSERT_SQL_EMPLOYEE_REGISTRATION_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, empBean.getEmployeeCode());
			preparedStmt.setString(2, empBean.getEmployeeName());
			preparedStmt.setString(3, empBean.getPhone());
			preparedStmt.setDate(4, dob);
			preparedStmt.setDate(5, doj);
			preparedStmt.setString(6, empBean.getEmail());
			preparedStmt.setInt(7, addressCodeForEmployeeRegistration);
			preparedStmt.setString(8, empBean.getDesignation());
			preparedStmt.setString(9, empBean.getStatus());
			preparedStmt.setString(10, empBean.getLoginType());
			preparedStmt.setString(11, empBean.getPassword());
			preparedStmt.setString(12, MasterSQL_Employee_Registration_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(13, MasterSQL_Employee_Registration_Utils.LAST_MODIFIED_USER_FORALL);
			
			

			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				reset();
				alert.setContentText("Employee Registration details successfully saved...");
				alert.showAndWait();
			} else {
				alert.setContentText("Employee Registration details is not saved \nPlease try again...!");
				alert.showAndWait();
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	

// *******************************************************************************	

	public int getAddressCode() throws SQLException
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
	
		int addressCode = 0;

		try {

			stmt = con.createStatement();
			sql = MasterSQL_Address_Utils.GET_SQL_ADDRESS_CODE;
			rs = stmt.executeQuery(sql);

			while (rs.next())
			{		
				addressCode=rs.getInt("addressid");
			}
			
			addressCode++;
			addressCodeForEmployeeRegistration=addressCode;
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
		return addressCode;
	}
	
	
// *******************************************************************************
	
	
	public void saveAddress() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		

		EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
		
		if(!txtCity.getText().equals(""))
		{
			String[] city=txtCity.getText().replaceAll("\\s+","").split("\\|");
			empBean.setCity(city[1]);
		}
		
		empBean.setAddressId(getAddressCode());
		empBean.setPhone(txtPhone.getText());
		empBean.setAddress(txtAddress.getText());
		empBean.setState(comboBoxState.getValue());
		empBean.setCountry(comboBoxCountry.getValue());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			String query = MasterSQL_Address_Utils.INSERT_SQL_ADDRESS_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setInt(1, empBean.getAddressId());
			preparedStmt.setString(2, empBean.getPhone());
			preparedStmt.setString(3, empBean.getAddress());
			preparedStmt.setString(4, empBean.getCity());
			preparedStmt.setString(5, empBean.getState());
			preparedStmt.setString(6, empBean.getCountry());
			
			preparedStmt.executeUpdate();

			
						
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
		
	}		
	
	

// *******************************************************************************	

	public void getEmployeeRegistrationDetails() throws SQLException
	{
		
		EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
		
		empBean.setEmployeeCode(txtEmployeeCode.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = MasterSQL_Employee_Registration_Utils.DATA_RETRIEVE_EMPLOYEE_REGISTRATION_DETAILS;
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, empBean.getEmployeeCode());
			
			System.out.println("SQL Query: " + preparedStmt);
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				txtEmployeeCode.setEditable(true);

			} 
			else 
			{
				txtEmployeeCode.setEditable(false);
				btnUpdate.setDisable(false);
				btnSave.setDisable(true);
				
				do 
				{
					empBean.setEmployeeCode(rs.getString("empcode"));
					empBean.setEmployeeName(rs.getString("empname"));
					empBean.setPhone(rs.getString("empphone"));
					empBean.setDob(date.format(rs.getDate("empdob")));
					empBean.setDoj(date.format(rs.getDate("empdoj")));
					empBean.setEmail(rs.getString("empemail"));
					empBean.setAddressId(rs.getInt("empaddresscode"));
					empBean.setAddress(rs.getString("empaddress"));
					empBean.setCity(rs.getString("empcity"));
					empBean.setState(rs.getString("empstate"));
					empBean.setCountry(rs.getString("empcountry"));
					empBean.setDesignation(rs.getString("empdesignation"));
					empBean.setStatus(rs.getString("empstatus"));
					empBean.setLoginType(rs.getString("empusertype"));
					empBean.setPassword(rs.getString("emppassword"));
					

				} while (rs.next());

				
				getAddressCodeforUpdate=empBean.getAddressId();
				
				txtEmployeeCode.setText(empBean.getEmployeeCode());
				txtEmployeeName.setText(empBean.getEmployeeName());
				txtPhone.setText(empBean.getPhone());
				dpkDOB.setValue(LocalDate.parse(empBean.getDob(), localdateformatter));
				dpkDOJ.setValue(LocalDate.parse(empBean.getDoj(), localdateformatter));
				txtEmail.setText(empBean.getEmail());
				txtAddress.setText(empBean.getAddress());
				
				System.out.println("City:  >>> "+empBean.getCity());
				
				if(empBean.getCity() !=null)
				{
					for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME)
					{
						if(empBean.getCity().equals(cityBean.getCityCode()))
						{
							txtCity.setText(cityBean.getCityName()+" | "+cityBean.getCityCode());
							break;
						}
					}
				}
				
				comboBoxState.setValue(empBean.getState());
				comboBoxCountry.setValue(empBean.getCountry());
				txtDesignation.setText(empBean.getDesignation());
				
				if(empBean.getStatus().equals("A"))
				{
					comboBoxStatus.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
				}
				else
				{
					comboBoxStatus.setValue(MasterSQL_Employee_Registration_Utils.IN_ACTIVE);
				}


				if(empBean.getLoginType().equals("T"))
				{
					comboBoxLoginType.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
				}
				else
				{
					comboBoxLoginType.setValue(MasterSQL_Employee_Registration_Utils.IN_ACTIVE);
				}
				
				txtPassword.setText(empBean.getPassword());
				
			
			}

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}

	}
	
	
// *******************************************************************************	

	public void updateEmployeeRegistrationDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		//saveAddress();
		updateAddress();
		
		EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
		
		empBean.setEmployeeCode(txtEmployeeCode.getText());
		empBean.setEmployeeName(txtEmployeeName.getText());
		empBean.setPhone(txtPhone.getText());
		empBean.setEmail(txtEmail.getText());
		empBean.setDesignation(txtDesignation.getText());
		empBean.setPassword(txtPassword.getText());
		
		LocalDate localDOB=dpkDOB.getValue();
		LocalDate localDOJ=dpkDOJ.getValue();
		Date dob=Date.valueOf(localDOB);
		Date doj=Date.valueOf(localDOJ);
		
		
		if(comboBoxStatus.getValue().equals(MasterSQL_Employee_Registration_Utils.ACTIVE))
		{
			empBean.setStatus("A");
			
		}
		else
		{
			empBean.setStatus("I");
		}
		
		if(comboBoxLoginType.getValue().equals(MasterSQL_Employee_Registration_Utils.ACTIVE))
		{
			empBean.setLoginType("T");
			
		}
		else
		{
			empBean.setLoginType("F");
		}
		
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			String query = MasterSQL_Employee_Registration_Utils.UPDATE_SQL_EMPLOYEE_REGISTRATION_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, empBean.getEmployeeName());
			preparedStmt.setString(2, empBean.getPhone());
			preparedStmt.setDate(3, dob);
			preparedStmt.setDate(4, doj);
			preparedStmt.setString(5, empBean.getEmail());
			preparedStmt.setString(6, empBean.getDesignation());
			preparedStmt.setString(7, empBean.getStatus());
			preparedStmt.setString(8, empBean.getLoginType());
			preparedStmt.setString(9, empBean.getPassword());
			preparedStmt.setString(10, empBean.getEmployeeCode());
			
			

			int status = preparedStmt.executeUpdate();
			
			System.out.println("executeUPdate value: "+status);

			if (status == 1) 
			{
				
				reset();
				btnUpdate.setDisable(true);
				btnSave.setDisable(false);
				alert.setContentText("Employee Registration details successfully Updated...");
				alert.showAndWait();
			} 
			else if (status == 2) 
			{
				
				reset();
				btnUpdate.setDisable(true);
				btnSave.setDisable(false);
				alert.setContentText("Employee Registration details successfully Updated...");
				alert.showAndWait();
			} 
			else 
			{
				alert.setContentText("Employee Registration details is not Updated \nPlease try again...!");
				alert.showAndWait();
			}
			
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
		
	}
	
	
// *******************************************************************************
	

	public void updateAddress() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		String[] city=txtCity.getText().replaceAll("\\s+","").split("\\|");

		EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
		
		
		empBean.setAddressId(getAddressCodeforUpdate);
		empBean.setCity(city[1]);
		empBean.setPhone(txtPhone.getText());
		empBean.setAddress(txtAddress.getText());
		empBean.setState(comboBoxState.getValue());
		empBean.setCountry(comboBoxCountry.getValue());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			String query = MasterSQL_Address_Utils.UPDATE_SQL_EMP_ADDRESS_DETAILS;
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, empBean.getPhone());
			preparedStmt.setString(2, empBean.getAddress());
			preparedStmt.setString(3, empBean.getCity());
			preparedStmt.setString(4, empBean.getState());
			preparedStmt.setString(5, empBean.getCountry());
			preparedStmt.setInt(6, empBean.getAddressId());
			
			preparedStmt.executeUpdate();

			
						
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	
	
// ========================================================================================		
	
	public void loadEmployeeRegistrationTable() throws SQLException {
		tabledata_employee_registration.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		

		int slno = 1;
		try {

			stmt = con.createStatement();
			sql = MasterSQL_Employee_Registration_Utils.LOAD_TABLE_EMPLOYEE_REGISTRATION_DETAILS;
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				
				EmployeeRegistrationBean empBean=new EmployeeRegistrationBean();
				
				empBean.setEmployeeCode(rs.getString("empcode"));
				empBean.setEmployeeName(rs.getString("empname"));
				empBean.setPhone(rs.getString("empphone"));
				empBean.setDob(date.format(rs.getDate("empdob")));
				empBean.setDoj(date.format(rs.getDate("empdoj")));
				empBean.setDesignation(rs.getString("empdesignation"));
				empBean.setStatus(rs.getString("empstatus"));
				empBean.setEmail(rs.getString("empemail"));
				empBean.setAddress(rs.getString("empaddress"));
				
				
			/*	if(rs.getString("empcity") !=null)
				{
					for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME)
					{
						if(rs.getString("empcity").equals(cityBean.getCityCode()))
						{
							empBean.setCity(cityBean.getCityName());
							break;
						}
					}
				}
				*/
			
				
				empBean.setCity(rs.getString("empcity"));
				

				tabledata_employee_registration.add(new EmployeeRegistrationTableBean(slno, empBean.getEmployeeCode(), empBean.getEmployeeName(),
						empBean.getPhone(), empBean.getEmail(), empBean.getAddress(), empBean.getCity(), empBean.getDesignation(),
						empBean.getDoj(), empBean.getDob(), empBean.getStatus()));

				slno++;
			}

			tableEmployeeRegistration.setItems(tabledata_employee_registration);

			

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}

	
// *************************************************************************************

	public void setStateAndCountry()
	{
		if (txtCity.getText() == null || txtCity.getText().isEmpty()) 
		{
		
		}
		else
		{
			String[] citycode=txtCity.getText().replaceAll("\\s+","").split("\\|");
			
			for(LoadCityBean stateBean: LoadCity.SET_LOAD_CITYWITHNAME)
			{
				if(citycode[1].equals(stateBean.getCityCode()))
				{
					comboBoxState.setValue(stateBean.getStateCode());
					break;
				}
			}
			
			
			for(LoadCityBean countryBean: LoadState.SET_LOAD_STATEWITHNAME)
			{
				if(comboBoxState.getValue().equals(countryBean.getStateCode()))
				{
					comboBoxCountry.setValue(countryBean.getCountryCode());
					break;
				}
			}

				
		}
		
	}
	
	
// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();
		
		
		if(n.getId().equals("txtEmployeeCode"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtEmployeeName.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtEmployeeName"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtPhone.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtPhone"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkDOB.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkDOB"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkDOJ.requestFocus();
			}
			
		}
		
		else if(n.getId().equals("dpkDOJ"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				txtEmail.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtEmail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAddress.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAddress"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtCity.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtCity"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				setStateAndCountry();
				comboBoxState.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxState"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxCountry.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxCountry"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				txtDesignation.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtDesignation"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxStatus.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxStatus"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxLoginType.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxLoginType"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtPassword.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtPassword"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtEmployeeCode.isEditable()==true)
				{
					btnSave.requestFocus();
				}
				else
				{
					btnUpdate.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("btnSave"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				System.out.println("Button save");
				setValidation();
				txtEmployeeCode.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnUpdate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				System.out.println("Button Update");
				setValidation();
				txtEmployeeCode.requestFocus();
			}
		}
		
	}	
	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if (txtEmployeeCode.getText() == null || txtEmployeeCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtEmployeeCode.requestFocus();
		} 
		
		else if (txtEmployeeName.getText() == null || txtEmployeeName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtEmployeeName.requestFocus();
		}
		
		else if(dpkDOB.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("DOB field is Empty!");
			alert.showAndWait();
			dpkDOB.requestFocus();
		}
		
		else if(dpkDOJ.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("DOJ field is Empty!");
			alert.showAndWait();
			dpkDOJ.requestFocus();
		}
		
		else 
		{
			if(txtEmployeeCode.isEditable()==true)
			{
				saveEmployeeDetails();
			}
			else
			{
				updateEmployeeRegistrationDetails();
			}
			
		}
	}	
	

// *******************************************************************************	

	public void reset()
	{
		txtEmployeeCode.setEditable(true);
		
		txtEmployeeCode.clear();
		txtEmployeeName.clear();
		txtPhone.clear();
		txtEmail.clear();
		txtAddress.clear();
		txtCity.clear();
		txtDesignation.clear();
		txtPassword.clear();
		
		comboBoxCountry.getSelectionModel().clearSelection();
		comboBoxState.getSelectionModel().clearSelection();
		
		dpkDOB.getEditor().clear();
		dpkDOJ.getEditor().clear();
		
		comboBoxStatus.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
		comboBoxLoginType.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
		
	}
	
	
	
	
// *******************************************************************************	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		btnUpdate.setDisable(true);
		
		comboBoxStatus.setItems(comboBoxStatusItems);
		comboBoxLoginType.setItems(comboBoxLoginTypeItems);
		comboBoxStatus.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
		comboBoxLoginType.setValue(MasterSQL_Employee_Registration_Utils.ACTIVE);
		
		imageView_SearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		tableCol_Slno.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,Integer>("slno"));
		tableCol_EmpCode.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("employeeCode"));
		tableCol_EmpName.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("employeeName"));
		tableCol_EmpPhone.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("phone"));
		tableCol_DOB.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("dob"));
		tableCol_DOJ.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("doj"));
		tableCol_Designation.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("designation"));
		tableCol_Status.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("status"));
		tableCol_Email.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,String>("email"));
		tableCol_Address.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,Double>("address"));
		tableCol_City.setCellValueFactory(new PropertyValueFactory<EmployeeRegistrationTableBean,Double>("city"));
		
		
		
		try {
			loadCity();
			loadState();
			loadCountry();
			loadEmployeeRegistrationTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
