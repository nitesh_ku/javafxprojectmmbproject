package com.onesoft.courier.master.pincode.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.master.pincode.bean.UploadPincodeBean;
import com.onesoft.courier.master.pincode.bean.UploadPincodeTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_UploadPincode_Excel_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;

public class UploadPincodeExcelController implements Initializable{
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	
	private List<UploadPincodeBean> list_PincodeRecords=new ArrayList<>();
	private List<UploadPincodeBean> list_ZoneMappingRecords=new ArrayList<>();
	private Set<String> set_UnsavedPincodeWhileInsert_AND_Update=new HashSet();
	private Set<String> set_UnsavedZoneMappingInsert_AND_Update=new HashSet();
	
	private ObservableList<UploadPincodeTableBean> tabledata_Pincode=FXCollections.observableArrayList();
	
	private List<String> list_Pincode_from_DB=new ArrayList<>();
	private List<UploadPincodeBean> list_zoneMapping=new ArrayList<>();
	//private List<UploadPincodeBean> list_Pincode_from_DB_ForZoneMappingAndLoadTable=new ArrayList<>();
	
	boolean oda_opa_browse_btn_clicked=false;
	boolean zone_browse_btn_clicked=false;
	
	boolean isExcelFileIsCorrect=false;
	
	@FXML
	private ImageView imgViewSearchIcon_PincodeData;
	
	@FXML
	private Pagination pagination_Pincode;
	
	@FXML
	private CheckBox checkBoxShowAll;
	
	@FXML
	private TextField txt_ODA_OPA_BrowseExcel;
	
	@FXML
	private Button btn_ODA_OPA_Browse;
	
	@FXML
	private Button btn_ODA_OPA_Upload;
	
	@FXML
	private Button btn_ODA_OPA_Reset;
	
	@FXML
	private RadioButton rdBtn_ODA_OPA_Add;
	
	@FXML
	private RadioButton rdBtn_ODA_OPA_Update;	
	
// ******************************************************************************	
	
	@FXML
	private TextField txt_Zone_BrowseExcel;
	
	@FXML
	private Button btn_Zone_Browse;
	
	@FXML
	private Button btn_Zone_Upload;
	
	@FXML
	private Button btn_Zone_Reset;
	
	/*@FXML
	private RadioButton rdBtn_Zone_Add;
	
	@FXML
	private RadioButton rdBtn_Zone_Update;	*/
	
	
// ***************************************

	@FXML
	private TableView<UploadPincodeTableBean> tableUploadPincode;

	@FXML
	private TableColumn<UploadPincodeTableBean, Integer> upload_tabCol_slno;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_Pincode;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_CityName;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_StateCode;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_Oda_Opa;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_InterService;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_DomesticService;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_CodSrvice;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_ZoneCode;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_ZoneAir;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_ZoneSurface;

	@FXML
	private TableColumn<UploadPincodeTableBean, String> upload_tabCol_SerciceCode;


// ******************************************************************************

	public void btn_ODA_OPA_Browse_Clicked()
	{
		oda_opa_browse_btn_clicked=true;
		zone_browse_btn_clicked=false;
		
		fileAttachment();
		txt_Zone_BrowseExcel.clear();
	}


	public void btn_ZONE_Browse_Clicked()
	{
		oda_opa_browse_btn_clicked=false;
		zone_browse_btn_clicked=true;
		
		fileAttachment();
		txt_ODA_OPA_BrowseExcel.clear();
	}

	
// ******************************************************************************
	
		public void fileAttachment()
		{
			FileChooser fc = new FileChooser();
			long a=0;
			long size=0;
				//--------- Set validation for text and image files ------------
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
			
	    	selectedFile = fc.showOpenDialog(null);
	    	
	    	if(selectedFile != null)
	    	{
	    		a=5*1024*1024;
	    		size=selectedFile.length();	
	    		
	    		
	    		if(oda_opa_browse_btn_clicked==true)
	    		{
		    	   	if(size<a)
		    	   	{
		    	   		txt_ODA_OPA_BrowseExcel.getText();
		    	   		txt_ODA_OPA_BrowseExcel.setText(selectedFile.getAbsolutePath());
		    	   		btn_Zone_Upload.setDisable(true);
		    	   		btn_ODA_OPA_Upload.setDisable(false);
		    	   		btn_ODA_OPA_Upload.requestFocus();
		    	   	}
		    	   	else
		    	   	{
		    	   		txt_ODA_OPA_BrowseExcel.clear();
		    	   	}
	    		}
	    		
	    		else if(zone_browse_btn_clicked==true)
	    		{
	    			if(size<a)
		    	   	{
		    	   		txt_Zone_BrowseExcel.getText();
		    	   		txt_Zone_BrowseExcel.setText(selectedFile.getAbsolutePath());
		    	   		btn_ODA_OPA_Upload.setDisable(true);
		    	   		btn_Zone_Upload.setDisable(false);
		    	   		btn_Zone_Upload.requestFocus();
		    	   	}
		    	   	else
		    	   	{
		    	   		txt_Zone_BrowseExcel.clear();
		    	   	}
	    		}
	    	}
	    	else
	    	{
	    		size=0;
	    	}
		}	
		
		
// ******************************************************************************	

	public void progressBarTest() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Processing.......");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);
		
		taskUpdateStage.setScene(new Scene(updatePane));
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				save();
			
				int max = 1000;
				for (int i = 1; i <= max; i++) 
				{
					// Thread.sleep(100);
					/*if (BirtReportExportCon.FLAG == true) {
	                      break;
	                }*/
				}
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() 
		{
			public void handle(WorkerStateEvent t) 
			{
				taskUpdateStage.close();
				
				btn_ODA_OPA_Upload.setDisable(true);
    	   		btn_Zone_Upload.setDisable(true);
    	   		
    	   		
    	   		if(isExcelFileIsCorrect==false)
    	   		{
    	   		Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Import Alert");
				//alert.setTitle("Empty Field Validation");
				alert.setContentText("Please select correct Excel format file");
				alert.showAndWait();
				isExcelFileIsCorrect=true;
    	   		}
    	   		else
    	   		{
    	   			Alert alert = new Alert(AlertType.INFORMATION);
    				alert.setTitle("Upload Alert");
    				//alert.setTitle("Empty Field Validation");
    				alert.setContentText("Pincode ODA OPA file successfully uploaded...");
    				alert.showAndWait();
    				isExcelFileIsCorrect=true;
    	   		}

		
					/*	if (list_ZoneWiseClientRate.size()==update_insert_count) {
						Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Task Complete");
					//alert.setTitle("Empty Field Validation");
					alert.setContentText("Rates successfully uploaded...");
					alert.showAndWait();
					}*/
			}
		});

		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();
	}

	
// ******************************************************************************

	public void save() throws IOException
	{

		if(oda_opa_browse_btn_clicked==true && btn_Zone_Upload.isDisable()==true)
		{
			importPincodeDataExcel(selectedFile);
			txt_ODA_OPA_BrowseExcel.clear();
			oda_opa_browse_btn_clicked=false;
			zone_browse_btn_clicked=false;
		}

		if(zone_browse_btn_clicked==true && btn_ODA_OPA_Upload.isDisable()==true)
		{
			importZoneMappingDataExcel(selectedFile);
			txt_Zone_BrowseExcel.clear();
			oda_opa_browse_btn_clicked=false;
			zone_browse_btn_clicked=false;
		}

	}
			
// ******************************************************************************	

	public void importPincodeDataExcel(File selectedFile) throws IOException
	{
		list_PincodeRecords.clear();

		try
		{
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			//System.out.println("File Name: "+fileName);

			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

				HSSFRow row;
				if(Sheet.getLastRowNum()!=0)
				{
					
					int maxCell=0;
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						maxCell=  row.getLastCellNum();
						System.out.println("Total Columns >> "+maxCell);
						break;
						
					}
					
					if(maxCell==7)
					{
						isExcelFileIsCorrect=true;
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						Iterator<Cell> cellIterator = row.cellIterator();

						UploadPincodeBean upBean=new UploadPincodeBean();
						
						upBean.setPincode(String.valueOf((int)row.getCell(0).getNumericCellValue()));
						upBean.setCity_name(row.getCell(1).toString());
						upBean.setState_code(row.getCell(2).toString());
						upBean.setOda_Opa_Regular(row.getCell(3).toString());
						upBean.setInternationa_Service(row.getCell(4).toString());
						upBean.setDomestic_service(row.getCell(5).toString());
						upBean.setCod_service(row.getCell(6).toString());
						
						
						list_PincodeRecords.add(upBean);

					}
					}
					 else
					 {
						 isExcelFileIsCorrect=false;
						 /*Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Import Alert");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Please select correct Excel format file");
							alert.showAndWait();
							break;*/
					 }

				}
			}

			
			/*for(UploadPincodeBean bean: list_PincodeRecords)
			{
				System.out.println(bean.getPincode()+" | "+bean.getCity_name()+" | "+bean.getState_code()+" | "+bean.getInternationa_Service()+" | "+bean.getDomestic_service()+" | "+bean.getCod_service());
			}*/
			
		//	System.out.println("List size: "+list_PincodeRecords.size());
			
			if(rdBtn_ODA_OPA_Add.isSelected()==true)
			{
				System.out.println("Add running...");
				savePincodeData();
			}
			else
			{
				UpdatePincodeData();
			}

			
			fileInputStream.close();
		}

		catch(final Exception e)
		{
			e.printStackTrace();
		}

	}	
	
	
// ******************************************************************************	

	public void importZoneMappingDataExcel(File selectedFile) throws IOException
	{
		list_ZoneMappingRecords.clear();

		try
		{
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			System.out.println("File Name: "+fileName);

			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				
				
				
				//int colCount=sheetName.getrow(1).getLastCellNum();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

				HSSFRow row;
				if(Sheet.getLastRowNum()!=0)
				{
					int maxCell=0;
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						maxCell=  row.getLastCellNum();
						System.out.println("Total Columns >> "+maxCell);
						break;
						
					}
					
					if(maxCell==7)
					{
						isExcelFileIsCorrect=true;
						for(int k=1;k<=Sheet.getLastRowNum();k++)
						{
							row=(HSSFRow) Sheet.getRow(k);
							 
							Iterator<Cell> cellIterator = row.cellIterator();
	
							UploadPincodeBean upBean=new UploadPincodeBean();
	
	//						System.out.println("Pincode >> "+row.getCell(0).getNumericCellValue());
							upBean.setPincode(String.valueOf((int)row.getCell(0).getNumericCellValue()));
							upBean.setZone_air(row.getCell(1).toString());
							upBean.setZone_surface(row.getCell(3).toString());
							upBean.setService_code(String.valueOf(row.getCell(6)));
	
							list_ZoneMappingRecords.add(upBean);
							
						}
					 }
					 else
					 {
						 isExcelFileIsCorrect=false;
						 /*Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Import Alert");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Please select correct Excel format file");
							alert.showAndWait();
							break;*/
					 }
				}
			}

			System.out.println("List size: "+list_ZoneMappingRecords.size());

			Update_MapZoneWithPincode();
			fileInputStream.close();
		}

		catch(final Exception e)
		{
			e.printStackTrace();
		}

	}	

	
// **********************************************************************************	
		
	/*public boolean getPincodeFromDB() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		boolean isPincodeTableBlank=false;

		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String sql = MasterSQL_UploadPincode_Excel_Utils.GET_PINCODE_FROM_DB;
			preparedStmt = con.prepareStatement(sql);
			//preparedStmt.setString(1,pincode);

			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				isPincodeTableBlank=true;
			} 
			else 
			{
				do{
					list_Pincode_from_DB.add(rs.getString("pincode"));
					
				}while(rs.next());
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		return isPincodeTableBlank;
	}	*/
	
	
	public boolean getPincodeFromDB() throws SQLException
	{
		boolean isPincodeTableBlank=false;
		new LoadPincodeForAll().loadPincode();
		
		if(LoadPincodeForAll.list_Load_Pincode_from_Common.size()>0)
		{
			isPincodeTableBlank=false;
			for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
			{
				list_Pincode_from_DB.add(pinBean.getPincode());
			}
		}
		else
		{
			isPincodeTableBlank=true;
		}

		return isPincodeTableBlank;
	}	
	
	
	
// **********************************************************************************	
			
	public boolean loadPincodeForZoneMapping() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		boolean isPincodeTableBlank=false;

		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String sql = MasterSQL_UploadPincode_Excel_Utils.GET_PINCODE_FROM_DB;
			preparedStmt = con.prepareStatement(sql);
			//preparedStmt.setString(1,pincode);

			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				isPincodeTableBlank=true;
			} 
			else 
			{
				do
				{
					UploadPincodeBean upBean=new UploadPincodeBean();

					upBean.setPincode(rs.getString("pincode"));
					upBean.setZone_air(rs.getString("zone_air"));
					upBean.setZone_surface(rs.getString("zone_surface"));

					list_zoneMapping.add(upBean);
					
				}
				while(rs.next());
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		return isPincodeTableBlank;
	}


// **********************************************************************************	

	public void savePincodeData() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		boolean isPincodeTableBlank=false;
		
		PreparedStatement preparedStmt = null;
		long startTime=System.currentTimeMillis();
		isPincodeTableBlank=getPincodeFromDB();
		
		
		boolean isBulkDataAvailable=false;
		int count=0;
		if(list_PincodeRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		try 
		{
			if(isPincodeTableBlank==true)
			{	
				String query = MasterSQL_UploadPincode_Excel_Utils.INSERT_SQL_UPLOAD_PINCODE;
				preparedStmt = con.prepareStatement(query);
				con.setAutoCommit(false);
	
				for(UploadPincodeBean upBean:list_PincodeRecords)
				{
					preparedStmt.setString(1, upBean.getPincode());
					preparedStmt.setString(2, upBean.getCity_name());
					preparedStmt.setString(3, upBean.getState_code());
					preparedStmt.setString(4,upBean.getOda_Opa_Regular());
					preparedStmt.setString(5,upBean.getInternationa_Service());
					preparedStmt.setString(6, upBean.getDomestic_service());
					preparedStmt.setString(7, upBean.getCod_service());
					preparedStmt.addBatch();
					
					count++;
					if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}
				}
			
				int[] pincodeCount=preparedStmt.executeBatch();
				System.out.println(" Pincode count if table is empty>>> "+pincodeCount.length);
				con.commit();
				
			}
			else
			{	
				String query = MasterSQL_UploadPincode_Excel_Utils.INSERT_SQL_UPLOAD_PINCODE;
				preparedStmt = con.prepareStatement(query);
				con.setAutoCommit(false);
				
				for(UploadPincodeBean upBean:list_PincodeRecords)
				{
					if(!list_Pincode_from_DB.contains(upBean.getPincode()))
					{
						preparedStmt.setString(1, upBean.getPincode());
						preparedStmt.setString(2, upBean.getCity_name());
						preparedStmt.setString(3, upBean.getState_code());
						preparedStmt.setString(4,upBean.getOda_Opa_Regular());
						preparedStmt.setString(5,upBean.getInternationa_Service());
						preparedStmt.setString(6, upBean.getDomestic_service());
						preparedStmt.setString(7, upBean.getCod_service());
						preparedStmt.addBatch();
						
						count++;
						if(isBulkDataAvailable==true)
						{
							new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
						}
					}
					else
					{
						set_UnsavedPincodeWhileInsert_AND_Update.add(upBean.getPincode());
					}
				}
				
				
				int[] pincodeCount=preparedStmt.executeBatch();
				System.out.println(" Pincode count if Table is not empty >>> "+pincodeCount.length);
				System.out.println(" Un saved list size: >> "+set_UnsavedPincodeWhileInsert_AND_Update.size());
				con.commit(); 
				
			}
	
			long endTime=System.currentTimeMillis();
			long duration=endTime-startTime;
			System.out.println(duration+" ms");
		
			list_Pincode_from_DB.clear();
			list_PincodeRecords.clear();
			
		
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
		
	
	

// **********************************************************************************	
	
	/*public boolean checkPincodeForInsert(String pincode) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		boolean isPincodeExist=true;

		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String sql = MasterSQL_UploadPincode_Excel_Utils.CHECK_PINCODE_FOR_INSERT;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,pincode);

			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				isPincodeExist=false;
			} 
			else 
			{
				isPincodeExist=true;	
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		return isPincodeExist;
	}	*/
	
// **********************************************************************************	
	
	/*public void savePincodeData() throws SQLException
	{

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		boolean isPincodeExist=false;
		

		PreparedStatement preparedStmt = null;

		try 
		{
			int count=1;
			long startTime=System.currentTimeMillis();
			for(UploadPincodeBean upBean:list_PincodeRecords)
			{
				
				isPincodeExist=checkPincodeForInsert(upBean.getPincode());
				//isAwbNoExist_InConsignorDetails=checkAwbNoForInsertInConsignorDetails(upBean.getMasterAwbNo());

					System.out.println("pincode status >> "+count+" >> "+isPincodeExist);
					if(isPincodeExist==false)
					{

						String query = MasterSQL_UploadPincode_Excel_Utils.INSERT_SQL_UPLOAD_PINCODE;

						preparedStmt = con.prepareStatement(query);

						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
						//String date = "16-Aug-2016";
						LocalDate localDate = LocalDate.parse(upBean.getBookingDate(), formatter);
						// System.out.println(localDate);  //defa

						preparedStmt.setString(1, upBean.getPincode());
						preparedStmt.setString(2, upBean.getCity_name());
						preparedStmt.setString(3, upBean.getState_code());
						preparedStmt.setString(4,upBean.getOda_Opa_Regular());
						preparedStmt.setString(5,upBean.getInternationa_Service());
						preparedStmt.setString(6, upBean.getDomestic_service());
						preparedStmt.setString(7, upBean.getCod_service());
						
						preparedStmt.executeUpdate();
						 
						System.out.println("1 row affected...!");

					}
					else
					{
						set_UnsavedPincodeWhileInsert_AND_Update.add(upBean.getPincode());
						//set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(upBean.getMasterAwbNo());
						isPincodeExist=false;
					}
					
			}

			long endTime=System.currentTimeMillis();
			long duration=endTime-startTime;
			System.out.println(duration+" ms");
			//updateTime.setText(String.valueOf(duration)+" ms");
		   // pst.getUpdateCount();
			//System.out.println(	pst.getUpdateCount());

		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
	*/
	
	
// **********************************************************************************	
	
	public void UpdatePincodeData() throws SQLException
	{

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		
		boolean isBulkDataAvailable=false;
		int batchCount=0;
		if(list_PincodeRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		try 
		{
			int count=0;
			int updatedCount=0;
			long startTime=System.currentTimeMillis();
			

			String query = MasterSQL_UploadPincode_Excel_Utils.UPDATE_SQL_PINCODE_DATA;
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);	
			
			for(UploadPincodeBean upBean:list_PincodeRecords)
			{
				preparedStmt.setString(1, upBean.getCity_name());
				preparedStmt.setString(2, upBean.getState_code());
				preparedStmt.setString(3,upBean.getOda_Opa_Regular());
				preparedStmt.setString(4,upBean.getInternationa_Service());
				preparedStmt.setString(5, upBean.getDomestic_service());
				preparedStmt.setString(6, upBean.getCod_service());
				preparedStmt.setString(7, upBean.getPincode());
				
				preparedStmt.addBatch();
				
				batchCount++;
				if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, batchCount);
				}
				
			}
			
			int[] pincodeUpdateCount=preparedStmt.executeBatch();
			System.out.println(" Pincode update count  >>> "+pincodeUpdateCount.length);
			con.commit(); 
				
				//System.out.println("Query >> "+preparedStmt);

//			int pincode_update_statue=preparedStmt.executeUpdate();

				//count=count+pincode_update_statue;
				//System.out.println("1 row affected...!");
				
		/*		if(pincode_update_statue==0)
				{
					updatedCount++;
					System.out.println("not updated >>>>>>>>>>>>>> "+pincode_update_statue);
					
					set_UnsavedPincodeWhileInsert_AND_Update.add(upBean.getPincode());
				}
				else
				{
					System.out.println("updated >>>>>>>>>>>>>> "+pincode_update_statue);
					//updatedCount=updatedCount+pincode_update_statue;
				}*/
				

			

			System.out.println("Total row affected >> "+count);
			System.out.println("un updated >> "+set_UnsavedPincodeWhileInsert_AND_Update.size());
			
			long endTime=System.currentTimeMillis();
			long duration=endTime-startTime;
			System.out.println(duration+" ms");
			
			list_Pincode_from_DB.clear();
			list_PincodeRecords.clear();
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}	
	
	
// **********************************************************************************	

	public void Update_MapZoneWithPincode() throws SQLException
	{
		//loadPincodeForZoneMapping();
		
		set_UnsavedZoneMappingInsert_AND_Update.clear();
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int batchCount=0;
		
		if(list_PincodeRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		try 
		{
			int count=0;
			//int newPincodeCounter=0;
			//int updatedCount=0;
			int pincode_update_statue=0;
			long startTime=System.currentTimeMillis();
			//int index=0;
			
			int inDB=1;
			
			
			//String updatedPincode=null;
			
			String query = MasterSQL_UploadPincode_Excel_Utils.UPDATE_SQL_MAP_ZONE_WITH_PINCODE;
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(UploadPincodeBean upBean:list_ZoneMappingRecords)
			{			
				if(!upBean.getZone_air().equals("null"))
				{
					preparedStmt.setString(1, upBean.getZone_air());
				}
				else
				{
					preparedStmt.setString(1, null);
				}
				
				if(!upBean.getZone_surface().equals("null"))
				{
					preparedStmt.setString(2, upBean.getZone_surface());
				}
				else
				{
					preparedStmt.setString(2, null);
				}
				
				if(!upBean.getService_code().equals("null"))
				{
					preparedStmt.setString(3, upBean.getService_code());
				}
				else
				{
					preparedStmt.setString(3, null);
				}
				preparedStmt.setString(4, upBean.getPincode());
				preparedStmt.addBatch();
				
				batchCount++;
				if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, batchCount);
				}
		
			}
			

			int[] pincodeUpdateCount=preparedStmt.executeBatch();
			System.out.println(" Pincode Zone Mapping count  >>> "+pincodeUpdateCount.length);
			con.commit(); 
			
			System.out.println("No Change: "+set_UnsavedZoneMappingInsert_AND_Update.size());
			
			
			list_zoneMapping.clear();
			
			
			long endTime=System.currentTimeMillis();
			long duration=endTime-startTime;
			System.out.println(duration+" ms");

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}	

	
// ****************************************************************	
	
	public void selectInsertOrUpdate() throws SQLException
	{
		if(rdBtn_ODA_OPA_Add.isSelected()==true)
		{
			btn_ODA_OPA_Upload.setText("Upload");
		}
		else
		{
			btn_ODA_OPA_Upload.setText("Update");
		}
		
		set_UnsavedPincodeWhileInsert_AND_Update.clear();
		txt_ODA_OPA_BrowseExcel.clear();

	}

// ****************************************************************


	public void resetOPAODA() throws SQLException
	{
		rdBtn_ODA_OPA_Add.setSelected(true);
		selectInsertOrUpdate();
		list_Pincode_from_DB.clear();
		list_PincodeRecords.clear();
		btn_ODA_OPA_Browse.requestFocus();
		btn_ODA_OPA_Upload.setDisable(true);
	}
	

	public void resetZone() throws SQLException
	{
		list_ZoneMappingRecords.clear();
		set_UnsavedZoneMappingInsert_AND_Update.clear();
		txt_Zone_BrowseExcel.clear();
		btn_Zone_Browse.requestFocus();
		btn_Zone_Upload.setDisable(true);
		
	}
	
// ****************************************************************

	/*public void loadPincodeForTableAndZoneMapping() throws SQLException
	{
		tabledata_Pincode.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		

		int slno = 1;
		try 
		{
			
			stmt = con.createStatement();
			sql = MasterSQL_UploadPincode_Excel_Utils.LOAD_TABLE_PINCODE_DATA_SHOW_ALL;
	
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				
				UploadPincodeBean upBean = new UploadPincodeBean();

				upBean.setSlno(slno);
				upBean.setPincode(rs.getString("pincode"));
				upBean.setCity_name(rs.getString("city_name"));
				upBean.setService_code(rs.getString("state_code"));
				upBean.setOda_Opa_Regular(rs.getString("oda_opa"));
				upBean.setInternationa_Service(rs.getString("international_service"));
				upBean.setDomestic_service(rs.getString("domestic_service"));
				upBean.setCod_service(rs.getString("cod_serviceble"));
				upBean.setZone_code(rs.getString("zone_code"));
				upBean.setZone_air(rs.getString("zone_air"));
				upBean.setZone_surface(rs.getString("zone_surface"));
				upBean.setService_code(rs.getString("service_code"));
				
				list_Pincode_from_DB_ForZoneMappingAndLoadTable.add(upBean);
				
		
			//	tabledata_Pincode.add(new UploadPincodeTableBean(slno, upBean.getPincode(), upBean.getCity_name(), upBean.getState_code(), 
			//			upBean.getOda_Opa_Regular(), upBean.getInternationa_Service(), upBean.getDomestic_service(), upBean.getCod_service(), upBean.getZone_code(), upBean.getZone_air(), upBean.getZone_surface(), upBean.getService_code()));

				slno++;
			}

		//	tableUploadPincode.setItems(tabledata_Pincode);
			
		//	pagination_Pincode.setPageCount((tabledata_Pincode.size() / rowsPerPage() + 1));
		//	pagination_Pincode.setCurrentPageIndex(0);
		//	pagination_Pincode.setPageFactory((Integer pageIndex) -> createPage_UnForwarded(pageIndex));
			

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}*/
	

// ****************************************************************	
	
	public void loadPincodeTableFromList() throws SQLException
	{
		tabledata_Pincode.clear();
		int count=1;
		
		new LoadPincodeForAll().loadPincode();
		
		if(checkBoxShowAll.isSelected()==true)
		{
			for(LoadPincodeBean upBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
			{
				
				tabledata_Pincode.add(new UploadPincodeTableBean(upBean.getSlno(), upBean.getPincode(), upBean.getCity_name(), upBean.getState_code(), 
						upBean.getOda_Opa_Regular(), upBean.getInternationa_Service(), upBean.getDomestic_service(), upBean.getCod_service(), upBean.getZone_code(), upBean.getZone_air(), upBean.getZone_surface(), upBean.getService_code()));
				
			}
		}
		else
		{
			for(LoadPincodeBean upBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
			{
				if(count<=200)
				{
				tabledata_Pincode.add(new UploadPincodeTableBean(upBean.getSlno(), upBean.getPincode(), upBean.getCity_name(), upBean.getState_code(), 
						upBean.getOda_Opa_Regular(), upBean.getInternationa_Service(), upBean.getDomestic_service(), upBean.getCod_service(), upBean.getZone_code(), upBean.getZone_air(), upBean.getZone_surface(), upBean.getService_code()));
				}
				else
				{
					break;
				}
				count++;
			}
		}
		
		
		
		tableUploadPincode.setItems(tabledata_Pincode);
		
		pagination_Pincode.setPageCount((tabledata_Pincode.size() / rowsPerPage() + 1));
		pagination_Pincode.setCurrentPageIndex(0);
		pagination_Pincode.setPageFactory((Integer pageIndex) -> createPage_UnForwarded(pageIndex));
	}
	
// ============ Code for paginatation =====================================================

		public int itemsPerPage() {
			return 1;
		}

		public int rowsPerPage() {
			return 50;
		}

		public GridPane createPage_UnForwarded(int pageIndex) {
			int lastIndex = 0;

			GridPane pane = new GridPane();
			int displace = tabledata_Pincode.size() % rowsPerPage();

			if (displace >= 0) {
				lastIndex = tabledata_Pincode.size() / rowsPerPage();
			}

			int page = pageIndex * itemsPerPage();
			for (int i = page; i < page + itemsPerPage(); i++) {
				if (lastIndex == pageIndex) {
					tableUploadPincode.setItems(FXCollections.observableArrayList(tabledata_Pincode
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
				} else {
					tableUploadPincode.setItems(FXCollections.observableArrayList(tabledata_Pincode
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
				}
			}
			return pane;

		}	
	
	
// ****************************************************************	

	public void showAllPincodes() throws SQLException
	{
		loadPincodeTableFromList();
	}
	

// ****************************************************************
	
	public void setValidation()
	{
		/*if(btn_Zone_Upload.getOnMouseClicked())
		{
			System.out.println("Click Zone button");
		}
		
		if(btn_ODA_OPA_Upload.isPressed()==true)
		{
			System.out.println("Click OPA ODA button");
		}*/
	}
	
	
// ****************************************************************	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imgViewSearchIcon_PincodeData.setImage(new Image("/icon/searchicon_blue.png"));
		
		btn_ODA_OPA_Upload.setDisable(true);
		btn_Zone_Upload.setDisable(true);
		txt_ODA_OPA_BrowseExcel.setEditable(false);
		txt_Zone_BrowseExcel.setEditable(false);
		
		upload_tabCol_slno.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,Integer>("slno"));
		upload_tabCol_Pincode.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("pincode"));
		upload_tabCol_CityName.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("city_name"));
		upload_tabCol_StateCode.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("state_code"));
		upload_tabCol_Oda_Opa.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("oda_Opa_Regular"));
		upload_tabCol_InterService.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("internationa_Service"));
		upload_tabCol_DomesticService.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("domestic_service"));
		upload_tabCol_CodSrvice.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("cod_service"));
		upload_tabCol_ZoneCode.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("zone_code"));
		upload_tabCol_ZoneAir.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("zone_air"));
		upload_tabCol_ZoneSurface.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("zone_surface"));
		upload_tabCol_SerciceCode.setCellValueFactory(new PropertyValueFactory<UploadPincodeTableBean,String>("service_code"));
		
		
		
		try {
			loadPincodeTableFromList();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
