package com.onesoft.courier.master.pincode.bean;


import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class UploadPincodeTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty pincode;
	private SimpleStringProperty city_name;
	private SimpleStringProperty state_code;
	private SimpleStringProperty oda_Opa_Regular;
	private SimpleStringProperty internationa_Service;
	private SimpleStringProperty domestic_service;
	private SimpleStringProperty cod_service;
	private SimpleStringProperty zone_code;
	private SimpleStringProperty zone_air;
	private SimpleStringProperty zone_surface;
	private SimpleStringProperty service_code;
	
	public UploadPincodeTableBean(int slno, String pincode , String city_name , String state_code , String oda_Opa_Regular ,String internationa_Service,
			String domestic_service , String cod_service, String zone_code, String zone_air, String zone_surface, String service_code)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.pincode=new SimpleStringProperty(pincode);
		this.city_name=new SimpleStringProperty(city_name);
		this.state_code=new SimpleStringProperty(state_code);
		this.oda_Opa_Regular=new SimpleStringProperty(oda_Opa_Regular);
		this.internationa_Service=new SimpleStringProperty(internationa_Service);
		this.domestic_service=new SimpleStringProperty(domestic_service);
		this.cod_service=new SimpleStringProperty(cod_service);
		this.zone_code=new SimpleStringProperty(zone_code);
		this.zone_air=new SimpleStringProperty(zone_air);
		this.zone_surface=new SimpleStringProperty(zone_surface);
		this.service_code=new SimpleStringProperty(service_code);
		
	}
	
	
	public int getSlno() {
		return slno.get();
	}


	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}


	public String getPincode() {
		return pincode.get();
	}
	public void setPincode(SimpleStringProperty pincode) {
		this.pincode = pincode;
	}
	public String getCity_name() {
		return city_name.get();
	}
	public void setCity_name(SimpleStringProperty city_name) {
		this.city_name = city_name;
	}
	public String getState_code() {
		return state_code.get();
	}
	public void setState_code(SimpleStringProperty state_code) {
		this.state_code = state_code;
	}
	public String getOda_Opa_Regular() {
		return oda_Opa_Regular.get();
	}
	public void setOda_Opa_Regular(SimpleStringProperty oda_Opa_Regular) {
		this.oda_Opa_Regular = oda_Opa_Regular;
	}
	public String getInternationa_Service() {
		return internationa_Service.get();
	}
	public void setInternationa_Service(SimpleStringProperty internationa_Service) {
		this.internationa_Service = internationa_Service;
	}
	public String getDomestic_service() {
		return domestic_service.get();
	}
	public void setDomestic_service(SimpleStringProperty domestic_service) {
		this.domestic_service = domestic_service;
	}
	public String getCod_service() {
		return cod_service.get();
	}
	public void setCod_service(SimpleStringProperty cod_service) {
		this.cod_service = cod_service;
	}
	public String getZone_code() {
		return zone_code.get();
	}
	public void setZone_code(SimpleStringProperty zone_code) {
		this.zone_code = zone_code;
	}
	public String getZone_air() {
		return zone_air.get();
	}
	public void setZone_air(SimpleStringProperty zone_air) {
		this.zone_air = zone_air;
	}
	public String getZone_surface() {
		return zone_surface.get();
	}
	public void setZone_surface(SimpleStringProperty zone_surface) {
		this.zone_surface = zone_surface;
	}
	public String getService_code() {
		return service_code.get();
	}
	public void setService_code(SimpleStringProperty service_code) {
		this.service_code = service_code;
	}
	
}
