package com.onesoft.courier.master.pincode.bean;

public class UploadPincodeBean {
	
	private int slno;
	private String pincode;
	private String city_name;
	private String state_code;
	private String oda_Opa_Regular;
	private String internationa_Service;
	private String domestic_service;
	private String cod_service;
	private String zone_code;
	private String zone_air;
	private String zone_surface;
	private String service_code;
	
	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getOda_Opa_Regular() {
		return oda_Opa_Regular;
	}
	public void setOda_Opa_Regular(String oda_Opa_Regular) {
		this.oda_Opa_Regular = oda_Opa_Regular;
	}
	public String getInternationa_Service() {
		return internationa_Service;
	}
	public void setInternationa_Service(String internationa_Service) {
		this.internationa_Service = internationa_Service;
	}
	public String getDomestic_service() {
		return domestic_service;
	}
	public void setDomestic_service(String domestic_service) {
		this.domestic_service = domestic_service;
	}
	public String getCod_service() {
		return cod_service;
	}
	public void setCod_service(String cod_service) {
		this.cod_service = cod_service;
	}
	public String getZone_code() {
		return zone_code;
	}
	public void setZone_code(String zone_code) {
		this.zone_code = zone_code;
	}
	public String getZone_air() {
		return zone_air;
	}
	public void setZone_air(String zone_air) {
		this.zone_air = zone_air;
	}
	public String getZone_surface() {
		return zone_surface;
	}
	public void setZone_surface(String zone_surface) {
		this.zone_surface = zone_surface;
	}
	public String getService_code() {
		return service_code;
	}
	public void setService_code(String service_code) {
		this.service_code = service_code;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	
	
}
