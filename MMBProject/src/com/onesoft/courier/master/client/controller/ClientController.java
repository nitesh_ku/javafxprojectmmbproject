package com.onesoft.courier.master.client.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.SaveAddress;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.master.client.bean.ClientBean;
import com.onesoft.courier.master.client.bean.ClientTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ClientController implements Initializable {
	
	private List<String> list_City=new ArrayList<>();
	private ObservableList<String> comboBoxStateItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxCountryItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxStatusItems=FXCollections.observableArrayList(MasterSQL_Client_Utils.ACTIVE,MasterSQL_Client_Utils.IN_ACTIVE);
	private ObservableList<String> comboBoxAirDimensionFormulaItems=FXCollections.observableArrayList(MasterSQL_Client_Utils.CFT,MasterSQL_Client_Utils.DIVISION);
	private ObservableList<String> comboBoxSurfaceDimensionFormulaItems=FXCollections.observableArrayList(MasterSQL_Client_Utils.CFT,MasterSQL_Client_Utils.DIVISION);
	
	private ObservableList<ClientTableBean> tabledata_ClientDetails=FXCollections.observableArrayList();
	
	public int getAddressCodeforUpdate=0;
	
	@FXML
	private TextField txtClientCode;
	
	@FXML
	private TextField txtClientName;
	
	@FXML
	private TextField txtContactPerson;
	
	@FXML
	private TextField txtAddress;
	
	@FXML
	private TextField txtContactNumber;
	
	/*@FXML
	private TextField txtFuelRate;
	
	@FXML
	private TextField txtInsuranceCarrier;
	
	@FXML
	private TextField txtInsuranceCarrier_Percentage;
	
	@FXML
	private TextField txtInsuranceOwner;
	
	@FXML
	private TextField txtInsuranceOwner_Percentage;
	
	@FXML
	private TextField txtCOD_Flat;
	
	@FXML
	private TextField txtCOD_Percentage;
	
	@FXML
	private TextField txtFOD_Charges;*/
	
	@FXML
	private TextField txtGSTIN;
	
	@FXML
	private TextField txtPassword;
	
	@FXML
	private TextField txtDiscount_Percentage;
	
	@FXML
	private TextField txtMinimumBillingAmt;
	
	@FXML
	private TextField txtEmailId;
	
	@FXML
	private TextField txtCity;
	
	@FXML
	private TextField txtPincode;
	
	@FXML
	private TextField txtAir;
	
	@FXML
	private TextField txtSurface;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxState;
	
	@FXML
	private ComboBox<String> comboBoxCountry;
	
	@FXML
	private ComboBox<String> comboBoxStatus;
	
	@FXML
	private ComboBox<String> comboBox_Air_DimensionFormula;
	
	@FXML
	private ComboBox<String> comboBox_Surface_DimensionFormula;
	
	@FXML
	private CheckBox chkBoxCash;
	
	@FXML
	private CheckBox chkBoxGST;
	
	@FXML
	private CheckBox chkBoxReverse;
	
	@FXML
	private CheckBox chkBoxFuelOnVAS;
	
	@FXML
	private CheckBox chkBoxAutoEmail;

	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset;
	
// =============================================================================================
	
	@FXML
	private TableView<ClientTableBean> tableClientDetails;

	@FXML
	private TableColumn<ClientTableBean, Integer> tableCol_slno;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_ClientCode;
	
	@FXML
	private TableColumn<ClientTableBean, String> tableCol_ClientName;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_ContactPerson;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_Branch;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_Address;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_City;

	@FXML
	private TableColumn<ClientTableBean, String> tableCol_Status;

	@FXML
	private TableColumn<ClientTableBean, Double> tableCol_FuelRate;


// *******************************************************************************

/*	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName() + " | " + bean.getCityCode());
		}
		// comboBoxCity.setItems(comboBoxCityItems);
		TextFields.bindAutoCompletion(txtCity, list_City);

	}*/

// *******************************************************************************

	public void loadState() throws SQLException {
		new LoadState().loadStateWithName();

		for (LoadCityBean bean : LoadState.SET_LOAD_STATEWITHNAME) 
		{
			comboBoxStateItems.add(bean.getStateCode());
		}
		comboBoxState.setItems(comboBoxStateItems);
		// TextFields.bindAutoCompletion(txtState, list_State);
	}

// *******************************************************************************

	public void loadCountry() throws SQLException {
		new LoadCountry().loadCountryWithName();

		for (LoadCityBean bean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			comboBoxCountryItems.add(bean.getCountryCode());
		}
		comboBoxCountry.setItems(comboBoxCountryItems);
		// TextFields.bindAutoCompletion(txtCountry, list_Country);
	}
	
	
// =============================================================================================
	
	
	public void loadBranchWithName() throws SQLException
	{
		new LoadBranch().loadBranchWithName();
			
		for(LoadBranchBean bean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			comboBoxBranchItems.add(bean.getBranchName()+" | "+bean.getBranchCode());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
	}	
	
	
// ******************************************************************************

	public void loadCityFromPincode() throws SQLException 
	{
		new LoadCity().loadCityWithNameFromPinCodeMaster();;

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
		{
			list_City.add(bean.getCityName());
		
		}

		//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		//TextFields.bindAutoCompletion(txtOrigin, list_Origin);
		TextFields.bindAutoCompletion(txtCity, list_City);

	}	
	
	public void setCityFromPcode() throws SQLException 
	{

		if (!txtPincode.getText().equals("")) 
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);

			boolean checkZipcodeStatus = false;
			LoadCityWithZipcodeBean destinationBean = new LoadCityWithZipcodeBean();

			new LoadCity().loadZipcodeCityDetails();
			
			for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
			{
				//System.out.println("Pincode: "+bean.getZipcode());
				
				if (Long.valueOf(txtPincode.getText()) == bean.getZipcode())
				{
					checkZipcodeStatus = true;
					destinationBean.setCityName(bean.getCityName());
					break;
				} 
				else
				{
					checkZipcodeStatus = false;
				}
			}

			if (checkZipcodeStatus == true) 
			{
				txtCity.setText(destinationBean.getCityName());
				txtCity.requestFocus();
			}
			else 
			{
				txtCity.clear();
				alert.setTitle("Zipcode City Alert");
				alert.setContentText("Please enter correct zipcode");
				alert.showAndWait();
				txtPincode.requestFocus();
			}
		}
		else 
		{
			txtCity.requestFocus();
		}
	}
	
// *************************************************************************************

	public void setStateAndCountry() 
	{
		if (txtCity.getText() == null || txtCity.getText().isEmpty()) 
		{

		}
		else 
		{
			//String[] citycode = txtCity.getText().replaceAll("\\s+", "").split("\\|");

			for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
			{
				if (txtCity.getText().equals(bean.getCityName())) 
				{
					comboBoxState.setValue(bean.getStateCode());
					break;
				}
			}
			
			comboBoxCountry.setValue("IN");

			/*for (LoadCityBean countryBean : LoadState.SET_LOAD_STATEWITHNAME)
			{
				if (comboBoxState.getValue().equals(countryBean.getStateCode())) 
				{
					comboBoxCountry.setValue(countryBean.getCountryCode());
					break;
				}
			}*/

		}

	}

// *************************************************************************************	

	public void saveClient() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		String[] branch=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		
		ClientBean clBean=new ClientBean();
		
		clBean.setClientCode(txtClientCode.getText());
		clBean.setClientName(txtClientName.getText());
		clBean.setBranch(branch[1]);
		clBean.setContactPerson(txtContactPerson.getText());
		clBean.setContactNumber(txtContactNumber.getText());
		clBean.setAddress(txtAddress.getText());
		clBean.setCity(txtCity.getText());
		clBean.setState(comboBoxState.getValue());
		clBean.setCountry(comboBoxCountry.getValue());
	/*	clBean.setFuelRate(Double.valueOf(txtFuelRate.getText()));
		clBean.setInsuranceCarrier(Double.valueOf(txtInsuranceCarrier.getText()));
		clBean.setInsuranceCarrier_Percentage(Double.valueOf(txtInsuranceCarrier_Percentage.getText()));
		clBean.setInsuranceOwner(Double.valueOf(txtInsuranceOwner.getText()));
		clBean.setInsuranceOwner_Percentage(Double.valueOf(txtInsuranceOwner_Percentage.getText()));
		clBean.setCOD_Flat(Double.valueOf(txtCOD_Flat.getText()));
		clBean.setCOD_Percentage(Double.valueOf(txtCOD_Percentage.getText()));
		clBean.setFOD_Charges(Double.valueOf(txtFOD_Charges.getText()));*/
		clBean.setGSTIN(txtGSTIN.getText());
		clBean.setPassword(txtPassword.getText());
		clBean.setAir(Double.valueOf(txtAir.getText()));
		clBean.setAir_dimensionFormula(comboBox_Air_DimensionFormula.getValue());
		
		clBean.setSurface(Double.valueOf(txtSurface.getText()));
		clBean.setSurface_dimensionFormula(comboBox_Surface_DimensionFormula.getValue());
		
		clBean.setDiscount_Percentage(Double.valueOf(txtDiscount_Percentage.getText()));
		clBean.setMinimumBillingAmt(Double.valueOf(txtMinimumBillingAmt.getText()));
		clBean.setStatus(comboBoxStatus.getValue());
		clBean.setEmailId(txtEmailId.getText());
		clBean.setPincode(txtPincode.getText());
		
		if(chkBoxGST.isSelected()==true)
		{
			clBean.setGST("T");
		}
		else
		{
			clBean.setGST("F");
		}
		
		if(chkBoxReverse.isSelected()==true)
		{
			clBean.setReverse("T");
		}
		else
		{
			clBean.setReverse("F");
		}
		
		if(chkBoxFuelOnVAS.isSelected()==true)
		{
			clBean.setFuelOnVAS("T");
		}
		else
		{
			clBean.setFuelOnVAS("F");
		}
		
		if(chkBoxAutoEmail.isSelected()==true)
		{
			clBean.setAutoEmail("T");
		}
		else
		{
			clBean.setAutoEmail("F");
		}
		
		if(chkBoxCash.isSelected()==true)
		{
			clBean.setCash("T");
		}
		else
		{
			clBean.setCash("F");
		}
		
		if(comboBoxStatus.getValue().equals(MasterSQL_Client_Utils.ACTIVE))
		{
			clBean.setStatus("A");
			
		}
		else
		{
			clBean.setStatus("I");
		}
		
		/*if(comboBox_Air_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
		{
			clBean.setAir_dimensionFormula(MasterSQL_Client_Utils.CFT);
			
		}
		else
		{
			clBean.setAir_dimensionFormula(MasterSQL_Client_Utils.DIVISION);
		}*/
		
		/*SaveAddress saveAddress=new SaveAddress();
		saveAddress.saveAddress(clBean.getCity(), clBean.getContactNumber(), clBean.getAddress(), clBean.getState(), clBean.getCountry());*/
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		LoadClientBean lcBean=new LoadClientBean();
		
		lcBean.setClientCode(clBean.getClientCode());
		lcBean.setClientName(clBean.getClientName());
		lcBean.setBranchcode(clBean.getBranch());
		
		try 
		{
			
			String query = MasterSQL_Client_Utils.INSERT_SQL_SAVE_CLIENT;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, clBean.getClientCode());
			preparedStmt.setString(2, clBean.getClientName());
			preparedStmt.setString(3, clBean.getBranch());
			preparedStmt.setString(4, clBean.getContactPerson());
			preparedStmt.setString(5, clBean.getAddress());
			preparedStmt.setDouble(6, clBean.getFuelRate());
			preparedStmt.setString(7, clBean.getGST());
			preparedStmt.setString(8, clBean.getReverse());
			preparedStmt.setString(9, clBean.getFuelOnVAS());
			preparedStmt.setDouble(10, clBean.getInsuranceCarrier());
			preparedStmt.setDouble(11, clBean.getInsuranceCarrier_Percentage());
			preparedStmt.setDouble(12, clBean.getInsuranceOwner());
			preparedStmt.setDouble(13, clBean.getInsuranceOwner_Percentage());
			preparedStmt.setDouble(14, clBean.getCOD_Flat());
			preparedStmt.setDouble(15, clBean.getCOD_Percentage());
			preparedStmt.setDouble(16, clBean.getFOD_Charges());
			preparedStmt.setString(17, clBean.getGSTIN());
			preparedStmt.setString(18, clBean.getPassword());
			preparedStmt.setDouble(19, clBean.getAir());
			preparedStmt.setDouble(20, clBean.getSurface());
			preparedStmt.setString(21, clBean.getAir_dimensionFormula());
			preparedStmt.setDouble(22, clBean.getDiscount_Percentage());
			preparedStmt.setDouble(23, clBean.getMinimumBillingAmt());
			preparedStmt.setString(24, clBean.getStatus());
			preparedStmt.setString(25, clBean.getAutoEmail());
			preparedStmt.setString(26, clBean.getEmailId());
			preparedStmt.setString(27, clBean.getPincode());
			preparedStmt.setString(28, clBean.getCity());
			preparedStmt.setString(29, MasterSQL_Client_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(30, MasterSQL_Client_Utils.LAST_MODIFIED_USER_FORALL);
			preparedStmt.setString(31, clBean.getCash());
			preparedStmt.setString(32, clBean.getSurface_dimensionFormula());
			preparedStmt.setString(33, clBean.getContactNumber());
		
			int status = preparedStmt.executeUpdate();
			
			
		/*	LoadClientBean loadClientBean=new LoadClientBean();
			loadClientBean.setClientName(clBean.getClientName());
			loadClientBean.setClientCode(clBean.getClientCode());
			loadClientBean.setBranchcode(clBean.getBranch());
			loadClientBean.setClient_city(clBean.getCity());
			loadClientBean.setAir_dim(Double.valueOf(txtAir.getText()));
			loadClientBean.setAir_dim_formula(comboBox_Air_DimensionFormula.getValue());
			loadClientBean.setSurface_dim(Double.valueOf(txtSurface.getText()));
			loadClientBean.setSurface_dim_formula(comboBox_Surface_DimensionFormula.getValue());
			
			LoadClients.SET_LOAD_CLIENTWITHNAME.add(loadClientBean);*/

			if (status == 1) 
			{
				reset();
				alert.setContentText("Client successfully Added...");
				alert.showAndWait();
				loadClientDetailsTable();
				
				//LoadClients.SET_LOAD_CLIENTWITHNAME.add(lcBean);
			
				
				
			}
			else 
			{
				alert.setContentText("Client Details is not saved \nPlease try again...!");
				alert.showAndWait();
			}
		
			LoadClients.SET_LOAD_CLIENTWITHNAME.clear();
			
			new LoadClients().loadClientWithName();
			showOldClients();
			
		} 
		
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	

// *******************************************************************************	
	
	public void getClientDetails() throws SQLException
	{
		ClientBean clBean=new ClientBean();
		
		clBean.setClientCode(txtClientCode.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = MasterSQL_Client_Utils.DATA_RETRIEVE_SQL_CLIENT_DETAILS;
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, clBean.getClientCode());
			
			System.out.println("SQL Query: " + preparedStmt);
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				txtClientCode.setEditable(true);
			} 
			else 
			{
				txtClientCode.setEditable(false);
				btnUpdate.setDisable(false);
				btnSave.setDisable(true);
				
				do 
				{
					clBean.setClientCode(rs.getString("code"));
					clBean.setClientName(rs.getString("name"));
					clBean.setBranch(rs.getString("clientmaster2branch"));
					clBean.setContactPerson(rs.getString("contactperson"));
					clBean.setFuelRate(rs.getDouble("fuelrate"));
					clBean.setInsuranceCarrier(rs.getDouble("carrierinsurancefixed"));
					clBean.setInsuranceCarrier_Percentage(rs.getDouble("carrierinsurancepercentage"));
					clBean.setInsuranceOwner(rs.getDouble("ownerinsurancefixed"));
					clBean.setInsuranceOwner_Percentage(rs.getDouble("ownerinsurancepercentage"));
					clBean.setCOD_Flat(rs.getDouble("codchargesfixed"));
					clBean.setCOD_Percentage(rs.getDouble("codchargespercentage"));
					clBean.setFOD_Charges(rs.getDouble("fodcharges"));
					clBean.setGSTIN(rs.getString("gstin_number"));
					clBean.setPassword(rs.getString("password"));
					clBean.setAir(rs.getDouble("dim_air"));
					clBean.setSurface(rs.getDouble("dim_surface"));
					clBean.setAir_dimensionFormula(rs.getString("air_dim_formula"));
					clBean.setSurface_dimensionFormula(rs.getString("surface_dim_formula"));
					clBean.setDiscount_Percentage(rs.getDouble("discountrate"));
					clBean.setMinimumBillingAmt(rs.getDouble("minimumbillingamount"));
					clBean.setStatus(rs.getString("status"));
					clBean.setEmailId(rs.getString("emailid"));
					clBean.setGST(rs.getString("gst"));
					clBean.setReverse(rs.getString("reverse"));
					clBean.setFuelOnVAS(rs.getString("fuelonvas"));
					clBean.setAutoEmail(rs.getString("autoemail"));
					clBean.setCash(rs.getString("client_type"));
					clBean.setContactNumber(rs.getString("phone_no"));
					//clBean.setAddressid(rs.getInt("addressid"));
					clBean.setAddress(rs.getString("address"));
					clBean.setCity(rs.getString("city"));
					clBean.setPincode(rs.getString("pincode"));
					clBean.setAddress(rs.getString("address"));
					/*clBean.setState(rs.getString("state"));
					clBean.setCountry(rs.getString("country"));*/
		
				} while (rs.next());
				
				
				getAddressCodeforUpdate=clBean.getAddressid();
				txtClientCode.setText(clBean.getClientCode());
				txtClientName.setText(clBean.getClientName());
				txtContactPerson.setText(clBean.getContactPerson());
				txtContactNumber.setText(clBean.getContactNumber());
				txtAddress.setText(clBean.getAddress());
				//comboBoxState.setValue(clBean.getState());
				//comboBoxCountry.setValue(clBean.getCountry());
			/*	txtFuelRate.setText(String.valueOf(clBean.getFuelRate()));
				txtInsuranceCarrier.setText(String.valueOf(clBean.getInsuranceCarrier()));
				txtInsuranceCarrier_Percentage.setText(String.valueOf(clBean.getInsuranceCarrier_Percentage()));
				txtInsuranceOwner.setText(String.valueOf(clBean.getInsuranceOwner()));
				txtInsuranceOwner_Percentage.setText(String.valueOf(clBean.getInsuranceOwner_Percentage()));
				txtCOD_Flat.setText(String.valueOf(clBean.getCOD_Flat()));
				txtCOD_Percentage.setText(String.valueOf(clBean.getCOD_Percentage()));
				txtFOD_Charges.setText(String.valueOf(clBean.getFOD_Charges()));*/	
				txtGSTIN.setText(clBean.getGSTIN());
				txtPassword.setText(clBean.getPassword());
				txtAir.setText(String.valueOf(clBean.getAir()));
				txtSurface.setText(String.valueOf(clBean.getSurface()));
				comboBox_Air_DimensionFormula.setValue(clBean.getAir_dimensionFormula());
				comboBox_Surface_DimensionFormula.setValue(clBean.getSurface_dimensionFormula());
				txtDiscount_Percentage.setText(String.valueOf(clBean.getDiscount_Percentage()));
				txtMinimumBillingAmt.setText(String.valueOf(clBean.getMinimumBillingAmt()));
			//	comboBoxStatus.setValue(clBean.getStatus());
				txtEmailId.setText(clBean.getEmailId());
				//txtCity.setText(clBean.getCity());
				txtPincode.setText(clBean.getPincode());
				setCityFromPcode();
				setStateAndCountry();
				//txtAddress.setText(clBean.getAddress());
				
				
				if(clBean.getBranch() !=null)
				{
					for(String branch: comboBoxBranchItems)
					{
						String[] branchCode=branch.replaceAll("\\s+","").split("\\|");
						
						if(clBean.getBranch().equals(branchCode[1]))
						{
							comboBoxBranch.setValue(branch);
							break;
						}
					}
				}
				
				if(clBean.getCity() !=null)
				{
					for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME)
					{
						if(clBean.getCity().equals(cityBean.getCityCode()))
						{
							txtCity.setText(cityBean.getCityName()+" | "+cityBean.getCityCode());
							break;
						}
					}
				}
				
				if(clBean.getGST().equals("T"))
				{
					chkBoxGST.setSelected(true);
				}
				else
				{
					chkBoxGST.setSelected(false);
				}
				
				if(clBean.getReverse().equals("T"))
				{
					chkBoxReverse.setSelected(true);
				}
				else
				{
					chkBoxReverse.setSelected(false);
				}
				
				if(clBean.getFuelOnVAS().equals("T"))
				{
					chkBoxFuelOnVAS.setSelected(true);
				}
				else
				{
					chkBoxFuelOnVAS.setSelected(false);
				}
				
				if(clBean.getAutoEmail().equals("T"))
				{
					chkBoxAutoEmail.setSelected(true);
				}
				else
				{
					chkBoxAutoEmail.setSelected(false);
				}
				
				if(clBean.getCash().equals("T"))
				{
					chkBoxCash.setSelected(true);
				}
				else
				{
					chkBoxCash.setSelected(false);
				}
				
				if(clBean.getStatus().equals("A"))
				{
					comboBoxStatus.setValue(MasterSQL_Client_Utils.ACTIVE);
				}
				else
				{
					comboBoxStatus.setValue(MasterSQL_Client_Utils.IN_ACTIVE);
				}
			}
		} 

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	
// *******************************************************************************	

	public void updateClientDetails() throws SQLException 
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		String[] branch=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		
		ClientBean clBean=new ClientBean();
		
		clBean.setClientCode(txtClientCode.getText());
		clBean.setClientName(txtClientName.getText());
		clBean.setBranch(branch[1]);
		clBean.setContactPerson(txtContactPerson.getText());
		clBean.setContactNumber(txtContactNumber.getText());
		clBean.setAddressid(getAddressCodeforUpdate);
		clBean.setAddress(txtAddress.getText());
		clBean.setPincode(txtPincode.getText());
		clBean.setCity(txtCity.getText());
		clBean.setState(comboBoxState.getValue());
		clBean.setCountry(comboBoxCountry.getValue());
		/*clBean.setFuelRate(Double.valueOf(txtFuelRate.getText()));
		clBean.setInsuranceCarrier(Double.valueOf(txtInsuranceCarrier.getText()));
		clBean.setInsuranceCarrier_Percentage(Double.valueOf(txtInsuranceCarrier_Percentage.getText()));
		clBean.setInsuranceOwner(Double.valueOf(txtInsuranceOwner.getText()));
		clBean.setInsuranceOwner_Percentage(Double.valueOf(txtInsuranceOwner_Percentage.getText()));
		clBean.setCOD_Flat(Double.valueOf(txtCOD_Flat.getText()));
		clBean.setCOD_Percentage(Double.valueOf(txtCOD_Percentage.getText()));
		clBean.setFOD_Charges(Double.valueOf(txtFOD_Charges.getText()));*/	
		clBean.setGSTIN(txtGSTIN.getText());
		clBean.setPassword(txtPassword.getText());
		clBean.setAir(Double.valueOf(txtAir.getText()));
		clBean.setAir_dimensionFormula(comboBox_Air_DimensionFormula.getValue());
		
		clBean.setSurface(Double.valueOf(txtSurface.getText()));
		clBean.setSurface_dimensionFormula(comboBox_Surface_DimensionFormula.getValue());
		
		clBean.setDiscount_Percentage(Double.valueOf(txtDiscount_Percentage.getText()));
		clBean.setMinimumBillingAmt(Double.valueOf(txtMinimumBillingAmt.getText()));
		clBean.setStatus(comboBoxStatus.getValue());
		clBean.setEmailId(txtEmailId.getText());
		
		if(chkBoxGST.isSelected()==true)
		{
			clBean.setGST("T");
		}
		else
		{
			clBean.setGST("F");
		}
		
		if(chkBoxReverse.isSelected()==true)
		{
			clBean.setReverse("T");
		}
		else
		{
			clBean.setReverse("F");
		}
		
		if(chkBoxFuelOnVAS.isSelected()==true)
		{
			clBean.setFuelOnVAS("T");
		}
		else
		{
			clBean.setFuelOnVAS("F");
		}
		
		if(chkBoxAutoEmail.isSelected()==true)
		{
			clBean.setAutoEmail("T");
		}
		else
		{
			clBean.setAutoEmail("F");
		}
		
		if(chkBoxCash.isSelected()==true)
		{
			clBean.setCash("T");
		}
		else
		{
			clBean.setCash("F");
		}
		
		if(comboBoxStatus.getValue().equals(MasterSQL_Client_Utils.ACTIVE))
		{
			clBean.setStatus("A");
			
		}
		else
		{
			clBean.setStatus("I");
		}
		
	/*	if(comboBox_Air_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
		{
			clBean.setAir_dimensionFormula(MasterSQL_Client_Utils.CFT);
			
		}
		else
		{
			clBean.setAir_dimensionFormula(MasterSQL_Client_Utils.DIVISION);
		}*/
		
		/*SaveAddress saveAddress=new SaveAddress();
		saveAddress.updateAddress(clBean.getAddressid(), clBean.getCity(), clBean.getContactNumber(), clBean.getAddress(), clBean.getState(), clBean.getCountry());*/

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{

			System.out.println("FR: "+clBean.getFuelRate());
			System.out.println("flat: "+clBean.getCOD_Flat());
			System.out.println("fod: "+clBean.getFOD_Charges());
			System.out.println("cod: "+clBean.getCOD_Percentage());
			
			
			String query = MasterSQL_Client_Utils.UPDATE_SQL_SAVE_CLIENT;
			preparedStmt = con.prepareStatement(query);

			//preparedStmt.setString(1, clBean.getClientCode());
			preparedStmt.setString(1, clBean.getClientName());
			preparedStmt.setString(2, clBean.getBranch());
			preparedStmt.setString(3, clBean.getContactPerson());
			preparedStmt.setString(4,clBean.getAddress());
			preparedStmt.setDouble(5, clBean.getFuelRate());
			preparedStmt.setString(6, clBean.getGST());
			preparedStmt.setString(7, clBean.getReverse());
			preparedStmt.setString(8, clBean.getFuelOnVAS());
			preparedStmt.setDouble(9, clBean.getInsuranceCarrier());
			preparedStmt.setDouble(10, clBean.getInsuranceCarrier_Percentage());
			preparedStmt.setDouble(11, clBean.getInsuranceOwner());
			preparedStmt.setDouble(12, clBean.getInsuranceOwner_Percentage());
			preparedStmt.setDouble(13, clBean.getCOD_Flat());
			preparedStmt.setDouble(14, clBean.getCOD_Percentage());
			preparedStmt.setDouble(15, clBean.getFOD_Charges());
			preparedStmt.setString(16, clBean.getGSTIN());
			preparedStmt.setString(17, clBean.getPassword());
			preparedStmt.setDouble(18, clBean.getAir());
			preparedStmt.setDouble(19, clBean.getSurface());
			preparedStmt.setString(20, clBean.getAir_dimensionFormula());
			preparedStmt.setDouble(21, clBean.getDiscount_Percentage());
			preparedStmt.setDouble(22, clBean.getMinimumBillingAmt());
			preparedStmt.setString(23, clBean.getStatus());
			preparedStmt.setString(24, clBean.getAutoEmail());
			preparedStmt.setString(25, clBean.getEmailId());
			preparedStmt.setString(26, clBean.getCash());
			preparedStmt.setString(27, clBean.getCity());
			preparedStmt.setString(28, clBean.getPincode());
			preparedStmt.setString(29, clBean.getSurface_dimensionFormula());
			preparedStmt.setString(30, clBean.getContactNumber());
			preparedStmt.setString(31, clBean.getClientCode());
			

			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				reset();
				btnUpdate.setDisable(true);
				btnSave.setDisable(false);
				alert.setContentText("Client details successfully updated...");
				alert.showAndWait();
				loadClientDetailsTable();
			} 
			else 
			{
				alert.setContentText("Client details is not updated \nPlease try again...!");
				alert.showAndWait();
			}

		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	

// ========================================================================================		
	
	public void loadClientDetailsTable() throws SQLException 
	{
		tabledata_ClientDetails.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
			
		int slno = 1;
		try 
		{
			stmt = con.createStatement();
			sql = MasterSQL_Client_Utils.LOAD_TABLE_SQL_CLIENT_DETAILS;
			System.out.println("Client Table SQL :: "+sql);
			rs = stmt.executeQuery(sql);

			/*public static String LOAD_TABLE_SQL_CLIENT_DETAILS="select code,name,client_type,clientmaster2branch,contactperson,fuelrate,"
					+ "status,address,city from clientmaster where client_type!='ToPay' order by clientmasterid DESC";*/
			
			while (rs.next()) {
					
			ClientBean clBean=new ClientBean();
			
			clBean.setClientCode(rs.getString("code"));
			clBean.setClientName(rs.getString("name"));
			clBean.setBranch(rs.getString("clientmaster2branch"));
			clBean.setContactPerson(rs.getString("contactperson"));
			clBean.setFuelRate(rs.getDouble("fuelrate"));
			clBean.setStatus(rs.getString("status"));
			clBean.setAddress(rs.getString("address"));
			clBean.setCity(rs.getString("city"));
					
			tabledata_ClientDetails.add(new ClientTableBean(slno, clBean.getClientCode(), clBean.getClientName(), clBean.getContactPerson(), clBean.getBranch(),
					clBean.getStatus(), clBean.getFuelRate(), clBean.getCity(), clBean.getAddress()));
				slno++;
			}
			tableClientDetails.setItems(tabledata_ClientDetails);

			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
// *************************************************************************************
	
		@FXML
		public void useEnterAsTabKey(KeyEvent e) throws SQLException
		{
			Node n=(Node) e.getSource();
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);
			
			if(n.getId().equals("chkBoxCash"))						
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtClientCode.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtClientCode"))						
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtClientName.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtClientName"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					comboBoxBranch.requestFocus();
				}
			}
			
			else if(n.getId().equals("comboBoxBranch"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtContactPerson.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtContactPerson"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtContactNumber.requestFocus();
				}
				
			}
			
			else if(n.getId().equals("txtContactNumber"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					
					txtAddress.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtAddress"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtPincode.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtPincode"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtCity.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtCity"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					//setStateAndCountry();
					comboBoxState.requestFocus();
				}
			}
			
			else if(n.getId().equals("comboBoxState"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					comboBoxCountry.requestFocus();
				}
			}
			
			else if(n.getId().equals("comboBoxCountry"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					chkBoxGST.requestFocus();
				}
			}
			
			else if(n.getId().equals("chkBoxGST"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					
					chkBoxReverse.requestFocus();
				}
			}
			
			else if(n.getId().equals("chkBoxReverse"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					chkBoxFuelOnVAS.requestFocus();
				}
			}
			
			else if(n.getId().equals("chkBoxFuelOnVAS"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtGSTIN.requestFocus();
				}
			}
			
			
			
			else if(n.getId().equals("txtGSTIN"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtPassword.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtPassword"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtAir.requestFocus();
				}
			}
			
			
			else if(n.getId().equals("txtAir"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					comboBox_Air_DimensionFormula.requestFocus();
				}
			}
			
			else if(n.getId().equals("comboBox_Air_DimensionFormula"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					
					if(comboBox_Air_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
					{
						if(Double.valueOf(txtAir.getText())>10)
						{
							alert.setTitle("Incorrect Value");
							alert.setContentText("Air value should be not more than 10, In case of Division");
							alert.showAndWait();
							txtAir.requestFocus();
							txtAir.setText("10");
						}
						else
						{
							txtSurface.requestFocus();	
						}
					}
					else
					{
						txtSurface.requestFocus();	
					}
				}
			}
			
			else if(n.getId().equals("txtSurface"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					comboBox_Surface_DimensionFormula.requestFocus();
				}
			}
			
			else if(n.getId().equals("comboBox_Surface_DimensionFormula"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					if(comboBox_Surface_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
					{
						if(Double.valueOf(txtSurface.getText())>10)
						{
							alert.setTitle("Empty Field Validation");
							alert.setContentText("Surface value should be not more than 10, In case of Division");
							alert.showAndWait();
							txtSurface.requestFocus();
							txtSurface.setText("10");
						}
						else
						{
							txtDiscount_Percentage.requestFocus();
						}
					}
					else
					{
						txtDiscount_Percentage.requestFocus();
					}
				}
			}
			
		
			
			else if(n.getId().equals("txtDiscount_Percentage"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtMinimumBillingAmt.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtMinimumBillingAmt"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					
					comboBoxStatus.requestFocus();
				}
			}
			
			
			else if(n.getId().equals("comboBoxStatus"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					chkBoxAutoEmail.requestFocus();
				}
			}
			
			else if(n.getId().equals("chkBoxAutoEmail"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					txtEmailId.requestFocus();
				}
			}
			
			else if(n.getId().equals("txtEmailId"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					if(txtClientCode.isEditable()==true)
					{
						btnSave.requestFocus();
					}
					else
					{
						btnUpdate.requestFocus();
					}
				}
			}
			
			else if(n.getId().equals("btnSave"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					System.out.println("Button save");
					setValidation();
					txtClientCode.requestFocus();
				}
			}
			
			else if(n.getId().equals("btnUpdate"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					System.out.println("Button Update");
					setValidation();
					txtClientCode.requestFocus();
				}
			}
			
		}	
		
// *******************************************************************************	
		
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
			
		if (txtClientCode.getText() == null || txtClientCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtClientCode.requestFocus();
		} 
		
		else if (txtClientName.getText() == null || txtClientName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtClientName.requestFocus();
		}
		
		else if(txtAddress.getText() == null || txtAddress.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Address field is Empty!");
			alert.showAndWait();
			txtAddress.requestFocus();
		}
		
		else if(txtCity.getText() == null || txtCity.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("City field is Empty!");
			alert.showAndWait();
			txtCity.requestFocus();
		}
		
		else if(comboBoxState.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("State field is Empty!");
			alert.showAndWait();
			comboBoxState.requestFocus();
		}
		
		else if(comboBoxCountry.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Country field is Empty!");
			alert.showAndWait();
			comboBoxCountry.requestFocus();
		}
		/*else if(comboBox_Air_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.DIVISION))
		{
			if(Integer.valueOf(txtAir.getText())>10)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Air value should be not more than 10, In case of Division");
				alert.showAndWait();
				txtAir.requestFocus();
				txtAir.setText("10");
			}
		}
		
		else if(comboBox_Surface_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.DIVISION))
		{
			if(Integer.valueOf(txtSurface.getText())>10)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Surface value should be not more than 10, In case of Division");
				alert.showAndWait();
				txtSurface.requestFocus();
				txtSurface.setText("10");
			}
		}*/
		
		else 
		{
			if(txtClientCode.isEditable()==true)
			{
				saveClient();
			}
			else
			{
				updateClientDetails();
			}
			
		}
					
	}
	
// *************************************************************************************

	
	public void reset()
	{
		
		txtClientCode.clear();
		txtClientName.clear();
		txtContactPerson.clear();
		txtContactNumber.clear();
		txtAddress.clear();
		txtCity.clear();
		/*txtFuelRate.setText("0");
		txtInsuranceCarrier.setText("0");
		txtInsuranceCarrier_Percentage.setText("0");
		txtInsuranceOwner.setText("0");
		txtInsuranceOwner_Percentage.setText("0");
		txtCOD_Flat.setText("0");
		txtCOD_Percentage.setText("0");
		txtFOD_Charges.setText("0");*/
		txtGSTIN.clear();
		txtPassword.clear();
		txtAir.setText("0");
		txtSurface.setText("0");
		txtDiscount_Percentage.setText("0");
		txtEmailId.clear();
		txtMinimumBillingAmt.setText("0");
		
		chkBoxAutoEmail.setSelected(false);
		chkBoxCash.setSelected(false);
		chkBoxFuelOnVAS.setSelected(false);
		chkBoxGST.setSelected(false);
		chkBoxReverse.setSelected(false);
		
		comboBoxBranch.setValue(comboBoxBranchItems.get(0));
		comboBoxState.getSelectionModel().clearSelection();
		comboBoxCountry.getSelectionModel().clearSelection();
		comboBox_Air_DimensionFormula.setValue(MasterSQL_Client_Utils.CFT);
		comboBoxStatus.setValue(MasterSQL_Client_Utils.ACTIVE);
		
		txtClientCode.setEditable(true);
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
		
	}

// *************************************************************************************
	
	public void showOldClients() throws SQLException
	{
		new LoadClients().loadClientWithName();
		
		System.out.println("Before new Client Added...");
		for(LoadClientBean clBean: LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			System.out.println(clBean.getClientName()+" | "+clBean.getClientCode()+" | "+clBean.getBranchcode()+" | "+clBean.getClient_city()+" | "+clBean.getAir_dim()+" | "+clBean.getAir_dim_formula()+" | "+clBean.getSurface_dim()+" | "+clBean.getSurface_dim_formula());
		}
	}
	
	
// *************************************************************************************	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		try {
			//loadCity();
			showOldClients();
			loadCityFromPincode();
			loadState();
			loadBranchWithName();
			loadCountry();
			//loadOrigin_and_Destination();
			loadClientDetailsTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		btnUpdate.setDisable(true);
		
		comboBoxStatus.setValue(MasterSQL_Client_Utils.ACTIVE);
		comboBoxStatus.setItems(comboBoxStatusItems);
		
		comboBox_Air_DimensionFormula.setValue(MasterSQL_Client_Utils.DIVISION);
		comboBox_Air_DimensionFormula.setItems(comboBoxAirDimensionFormulaItems);
		
		comboBox_Surface_DimensionFormula.setValue(MasterSQL_Client_Utils.DIVISION);
		comboBox_Surface_DimensionFormula.setItems(comboBoxAirDimensionFormulaItems);
		
		if(LoadBranch.SET_LOADBRANCHWITHNAME.size()>0)
		{
			comboBoxBranch.setValue(comboBoxBranchItems.get(0));
		}
		
		
		tableCol_slno.setCellValueFactory(new PropertyValueFactory<ClientTableBean,Integer>("slno"));
		tableCol_ClientCode.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("clientCode"));
		tableCol_ClientName.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("clientName"));
		tableCol_ContactPerson.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("contactPerson"));
		tableCol_Branch.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("branch"));
		tableCol_Address.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("address"));
		tableCol_City.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("city"));
		tableCol_Status.setCellValueFactory(new PropertyValueFactory<ClientTableBean,String>("status"));
		tableCol_FuelRate.setCellValueFactory(new PropertyValueFactory<ClientTableBean,Double>("fuelRate"));
		
		
		
	}
	

	
	

}
