package com.onesoft.courier.master.client.bean;


public class ClientBean {
	
	
	
	private String clientCode;
	private String clientName;
	private String contactPerson;
	private String branch;
	private String status;
	private double fuelRate;
	private String pincode;
	private String city;
	private String address;
	
	private int addressid;
	private String contactNumber;
	private double insuranceCarrier;
	private double insuranceCarrier_Percentage;
	private double insuranceOwner;
	private double insuranceOwner_Percentage;
	private double cod_Flat;
	private double cod_Percentage;
	private double fod_Charges;
	private String gst;
	private String gstin;
	private String password;
	private double discount_Percentage;
	private double minimumBillingAmt;
	private String emailId;
	private double air;
	private double surface;
	private String state;
	private String country;
	private String air_dimensionFormula;
	private String surface_dimensionFormula;
	private String autoEmail;
	private String reverse;
	private String fuelOnVAS;
	private String cash;
	
	
	
	public String getReverse() {
		return reverse;
	}
	public void setReverse(String reverse) {
		this.reverse = reverse;
	}
	public String getFuelOnVAS() {
		return fuelOnVAS;
	}
	public void setFuelOnVAS(String fuelOnVAS) {
		this.fuelOnVAS = fuelOnVAS;
	}
	public String getCash() {
		return cash;
	}
	public void setCash(String cash) {
		this.cash = cash;
	}
	public String getAutoEmail() {
		return autoEmail;
	}
	public void setAutoEmail(String autoEmail) {
		this.autoEmail = autoEmail;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public double getFuelRate() {
		return fuelRate;
	}
	public void setFuelRate(double fuelRate) {
		this.fuelRate = fuelRate;
	}
	public double getInsuranceCarrier() {
		return insuranceCarrier;
	}
	public void setInsuranceCarrier(double insuranceCarrier) {
		this.insuranceCarrier = insuranceCarrier;
	}
	public double getInsuranceCarrier_Percentage() {
		return insuranceCarrier_Percentage;
	}
	public void setInsuranceCarrier_Percentage(double insuranceCarrier_Percentage) {
		this.insuranceCarrier_Percentage = insuranceCarrier_Percentage;
	}
	public double getInsuranceOwner() {
		return insuranceOwner;
	}
	public void setInsuranceOwner(double insuranceOwner) {
		this.insuranceOwner = insuranceOwner;
	}
	public double getInsuranceOwner_Percentage() {
		return insuranceOwner_Percentage;
	}
	public void setInsuranceOwner_Percentage(double insuranceOwner_Percentage) {
		this.insuranceOwner_Percentage = insuranceOwner_Percentage;
	}
	public double getCOD_Flat() {
		return cod_Flat;
	}
	public void setCOD_Flat(double cOD_Flat) {
		this.cod_Flat = cOD_Flat;
	}
	public double getCOD_Percentage() {
		return cod_Percentage;
	}
	public void setCOD_Percentage(double cOD_Percentage) {
		this.cod_Percentage = cOD_Percentage;
	}
	public double getFOD_Charges() {
		return fod_Charges;
	}
	public void setFOD_Charges(double fOD_Charges) {
		this.fod_Charges = fOD_Charges;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public double getDiscount_Percentage() {
		return discount_Percentage;
	}
	public void setDiscount_Percentage(double discount_Percentage) {
		this.discount_Percentage = discount_Percentage;
	}
	public double getMinimumBillingAmt() {
		return minimumBillingAmt;
	}
	public void setMinimumBillingAmt(double minimumBillingAmt) {
		this.minimumBillingAmt = minimumBillingAmt;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public double getAir() {
		return air;
	}
	public void setAir(double air) {
		this.air = air;
	}
	public double getSurface() {
		return surface;
	}
	public void setSurface(double surface) {
		this.surface = surface;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getGSTIN() {
		return gstin;
	}
	public void setGSTIN(String gSTIN) {
		this.gstin = gSTIN;
	}
	public String getGST() {
		return gst;
	}
	public void setGST(String gST) {
		this.gst = gST;
	}
	public int getAddressid() {
		return addressid;
	}
	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getAir_dimensionFormula() {
		return air_dimensionFormula;
	}
	public void setAir_dimensionFormula(String air_dimensionFormula) {
		this.air_dimensionFormula = air_dimensionFormula;
	}
	public String getSurface_dimensionFormula() {
		return surface_dimensionFormula;
	}
	public void setSurface_dimensionFormula(String surface_dimensionFormula) {
		this.surface_dimensionFormula = surface_dimensionFormula;
	}
	
	
	
	

}
