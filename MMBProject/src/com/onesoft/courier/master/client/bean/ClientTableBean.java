package com.onesoft.courier.master.client.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ClientTableBean {

	private SimpleIntegerProperty slno;
	private SimpleStringProperty clientCode;
	private SimpleStringProperty clientName;
	private SimpleStringProperty contactPerson;
	private SimpleStringProperty branch;
	private SimpleStringProperty status;
	private SimpleDoubleProperty fuelRate;
	private SimpleStringProperty city;
	private SimpleStringProperty address;
	
	
	public ClientTableBean(int slno,String code, String name, String person, String branch, String status,
			double fuel, String city, String address)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.clientCode=new SimpleStringProperty(code); 
		this.clientName=new SimpleStringProperty(name);
		this.contactPerson=new SimpleStringProperty(person);
		this.branch=new SimpleStringProperty(branch);
		this.status=new SimpleStringProperty(status);
		this.fuelRate=new SimpleDoubleProperty(fuel);
		this.city=new SimpleStringProperty(city);
		this.address=new SimpleStringProperty(address);
	}
	
	public String getClientCode() {
		return clientCode.get();
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public String getClientName() {
		return clientName.get();
	}
	public void setClientName(SimpleStringProperty clientName) {
		this.clientName = clientName;
	}
	public String getContactPerson() {
		return contactPerson.get();
	}
	public void setContactPerson(SimpleStringProperty contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getBranch() {
		return branch.get();
	}
	public void setBranch(SimpleStringProperty branch) {
		this.branch = branch;
	}
	public String getStatus() {
		return status.get();
	}
	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}
	public double getFuelRate() {
		return fuelRate.doubleValue();
	}
	public void setFuelRate(SimpleDoubleProperty fuelRate) {
		this.fuelRate = fuelRate;
	}
	public String getCity() {
		return city.get();
	}
	public void setCity(SimpleStringProperty city) {
		this.city = city;
	}
	public String getAddress() {
		return address.get();
	}
	public void setAddress(SimpleStringProperty address) {
		this.address = address;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	
}
