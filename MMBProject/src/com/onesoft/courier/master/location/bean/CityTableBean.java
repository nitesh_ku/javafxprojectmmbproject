package com.onesoft.courier.master.location.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class CityTableBean {

	private SimpleIntegerProperty slno;
	private SimpleStringProperty stateCode;
	private SimpleStringProperty cityName;
	private SimpleStringProperty cityCode;
	
	
	public CityTableBean(int slno,String cityCode, String cityName, String stateCode)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.cityCode=new SimpleStringProperty(cityCode);
		this.cityName=new SimpleStringProperty(cityName);
		this.setStateCode(new SimpleStringProperty(stateCode));
		
	}
	
	public String getCityName() {
		return cityName.get();
	}
	public void setCityName(SimpleStringProperty cityName) {
		this.cityName = cityName;
	}
	public String getCityCode() {
		return cityCode.get();
	}
	public void setCityCode(SimpleStringProperty cityCode) {
		this.cityCode = cityCode;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getStateCode() {
		return stateCode.get();
	}
	public void setStateCode(SimpleStringProperty stateCode) {
		this.stateCode = stateCode;
	}
	
	
	
	
	
}
