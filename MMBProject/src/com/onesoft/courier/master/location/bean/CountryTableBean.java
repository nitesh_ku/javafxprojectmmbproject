package com.onesoft.courier.master.location.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class CountryTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty countryCode;
	private SimpleStringProperty countryName;
	
	
	public CountryTableBean(int slno, String countryCode,String countryName)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.countryCode=new SimpleStringProperty(countryCode);
		this.countryName=new SimpleStringProperty(countryName);
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getCountryCode() {
		return countryCode.get();
	}
	public void setCountryCode(SimpleStringProperty countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName.get();
	}
	public void setCountryName(SimpleStringProperty countryName) {
		this.countryName = countryName;
	}

}
