package com.onesoft.courier.master.location.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class StateTableBean {

	private SimpleIntegerProperty slno;
	private SimpleStringProperty stateCode;
	private SimpleStringProperty stateName;
	private SimpleStringProperty countryCode;
	
	
	public StateTableBean(int slno,String stateCode, String stateName, String countryCode)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.stateCode=new SimpleStringProperty(stateCode);
		this.stateName=new SimpleStringProperty(stateName);
		this.countryCode= new SimpleStringProperty(countryCode);
		
	}
	

	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getStateCode() {
		return stateCode.get();
	}
	public void setStateCode(SimpleStringProperty stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName.get();
	}
	public void setStateName(SimpleStringProperty stateName) {
		this.stateName = stateName;
	}
	public String getCountryCode() {
		return countryCode.get();
	}
	public void setCountryCode(SimpleStringProperty countryCode) {
		this.countryCode = countryCode;
	}
	
	
	
	
	
}
