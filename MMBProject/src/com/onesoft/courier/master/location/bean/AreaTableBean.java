package com.onesoft.courier.master.location.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AreaTableBean {

	private SimpleIntegerProperty slno;
	private SimpleStringProperty areaCode;
	private SimpleStringProperty areaName;
	private SimpleStringProperty pincode;
	private SimpleStringProperty cityName;
	private SimpleStringProperty cityCode;
	
	
	public AreaTableBean(int slno,String areaCode, String areaName, String pinCode, String cityCode)
	{
		this.areaCode=new SimpleStringProperty(areaCode);
		this.areaName=new SimpleStringProperty(areaName);
		this.pincode=new SimpleStringProperty(pinCode);
		this.cityCode=new SimpleStringProperty(cityCode);
		this.slno=new SimpleIntegerProperty(slno);
	}
	
	
	public String getAreaCode() {
		return areaCode.get();
	}
	public void setAreaCode(SimpleStringProperty areaCode) {
		this.areaCode = areaCode;
	}
	public String getAreaName() {
		return areaName.get();
	}
	public void setAreaName(SimpleStringProperty areaName) {
		this.areaName = areaName;
	}
	public String getPincode() {
		return pincode.get();
	}
	public void setPincode(SimpleStringProperty pincode) {
		this.pincode = pincode;
	}
	public String getCityName() {
		return cityName.get();
	}
	public void setCityName(SimpleStringProperty cityName) {
		this.cityName = cityName;
	}
	public String getCityCode() {
		return cityCode.get();
	}
	public void setCityCode(SimpleStringProperty cityCode) {
		this.cityCode = cityCode;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	
	
	
	
}
