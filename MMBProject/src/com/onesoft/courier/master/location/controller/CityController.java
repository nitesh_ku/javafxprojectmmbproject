package com.onesoft.courier.master.location.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.master.location.bean.CityTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Location_Utils;
import com.onesoft.courier.master.location.bean.AreaBean;
import com.onesoft.courier.master.location.bean.AreaTableBean;
import com.onesoft.courier.master.location.bean.CityBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class CityController implements Initializable{
	
	private ObservableList<CityTableBean> tabledata_CityDetailsItems=FXCollections.observableArrayList();
	private List<String> list_State=new ArrayList<>();
	
	@FXML
	private Pagination pagination_CityDetails;
	
	@FXML
	private ImageView imageViewSearchIcon;
	
	@FXML
	private TextField txtCode;
	
	@FXML
	private TextField txtCityName;
	
	@FXML
	private TextField txtState;
	

	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	boolean checkState=false;
	
	
	@FXML
	private TableView<CityTableBean> tableCity;
	
	@FXML
	private TableColumn<CityTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<CityTableBean, String> tabColumn_CityCode;
	
	@FXML
	private TableColumn<CityTableBean, String> tabColumn_CityName;
	
	@FXML
	private TableColumn<CityTableBean, String> tabColumn_StateCode;


	
	
// ******************************************************************************

	public void loadState() throws SQLException 
	{
		new LoadState().loadStateWithName();
	
		for (LoadCityBean bean : LoadState.SET_LOAD_STATEWITHNAME) 
		{
			list_State.add(bean.getStateName()+" | "+bean.getStateCode());
		}
		//comboBoxCity.setItems(comboBoxCityItems);
		TextFields.bindAutoCompletion(txtState, list_State);
	}		
	
	
	
// ******************************************************************************
	
	public void checkCityCodeExistanceAndLoad() throws SQLException
	{
		ResultSet rs = null;
		//Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		CityBean ctBean=new CityBean();
		ctBean.setCityCode(txtCode.getText());

		try 
		{
			sql = MasterSQL_Location_Utils.DATA_RETRIEVE_SQL_CITY_DETAILS;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, ctBean.getCityCode());
			
			rs = preparedStmt.executeQuery();
		
			if(!rs.next()==true)
			{
				btnSave.setDisable(false);
				btnUpdate.setDisable(true);
			}
			else
			{
				do{
					
					txtCode.setEditable(false);
					btnSave.setDisable(true);
					btnUpdate.setDisable(false);
									
					ctBean.setCityName(rs.getString("cityname"));
					ctBean.setCityCode(rs.getString("citycode"));
					ctBean.setStateCode(rs.getString("statename")+" | "+rs.getString("statecode"));
					txtCode.setText(ctBean.getCityCode());
					txtCityName.setText(ctBean.getCityName());
					txtState.setText(ctBean.getStateCode());
					
				}
				while (rs.next());
			}
				
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}	
	
// ******************************************************************************	

	public void saveCityDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		String[] state=txtState.getText().replaceAll("\\s+","").split("\\|");
			
		CityBean ctBean=new CityBean();
		
		
		ctBean.setCityCode(txtCode.getText());
		ctBean.setCityName(txtCityName.getText());
		ctBean.setStateCode(state[1]);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_Location_Utils.INSERT_SQL_CITY_DETAILS;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, ctBean.getStateCode());
			preparedStmt.setString(2, ctBean.getCityCode());
			preparedStmt.setString(3, ctBean.getCityName());
			preparedStmt.setString(4, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(5, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);
			
			
			int status=preparedStmt.executeUpdate();
			
			if(status==1)
			{
				reset();
				loadCityTable();
				alert.setContentText("City Details Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("City Details not saved \nPlease try again...!");
				alert.showAndWait();
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}

	
// ******************************************************************************	

	public void updateCityDetails() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		String[] state = txtState.getText().replaceAll("\\s+", "").split("\\|");

		CityBean ctBean=new CityBean();

		ctBean.setCityCode(txtCode.getText());
		ctBean.setCityName(txtCityName.getText());
		ctBean.setStateCode(state[1]);
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = MasterSQL_Location_Utils.UPDATE_SQL_CITY_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, ctBean.getStateCode());
			preparedStmt.setString(2, ctBean.getCityName());
			preparedStmt.setString(3, ctBean.getCityCode());
			
			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				reset();
				loadCityTable();
				alert.setContentText("City Details Successfully Updated...");
				alert.showAndWait();

			} 
			else 
			{
				btnSave.setDisable(true);
				btnUpdate.setDisable(false);
				alert.setContentText("City Details not updated \nPlease try again...!");
				alert.showAndWait();
			}

		} 
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
				
		if (n.getId().equals("txtCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				txtCityName.requestFocus();
			}
		} else if (n.getId().equals("txtCityName")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtState.requestFocus();
			}
		}

		else if (n.getId().equals("txtState")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				if (txtCode.isEditable() == true)
				{
					btnSave.requestFocus();
				} 
				else 
				{
					btnUpdate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				setValidation();

			}
		}

		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();

			}
		}

	}
	
	
	// *************** Method to set Validation *******************

	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if (txtCode.getText() == null || txtCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtCode.requestFocus();
		}

		else if (txtCityName.getText() == null || txtCityName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a City Name");
			alert.showAndWait();
			txtCityName.requestFocus();
		}

		else if (txtState.getText() == null || txtState.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a State");
			alert.showAndWait();
			txtState.requestFocus();
		}

		else 
		{
			checkStateExistance();

			if (checkState == true) 
			{
				if (btnSave.isDisable() == true)
				{
					System.out.println("Update City");
					updateCityDetails();
					reset();
					txtCode.requestFocus();
				} 
				else 
				{
					
					System.out.println("Save City");
					saveCityDetails();
					reset();
					txtCode.requestFocus();
				}
			} 
			else 
			{
				alert.setTitle("Incorrect State name");
				alert.setContentText("Please enter correct State name...!!");
				alert.showAndWait();
				txtState.requestFocus();
				checkState = false;
			}

		}
	}		
	
	
// ******************************************************************************	
	
	public void checkStateExistance()
	{
		for(LoadCityBean cityBean: LoadState.SET_LOAD_STATEWITHNAME)
		{
			String stateWithCode=cityBean.getStateName()+" | "+cityBean.getStateCode();
			if(stateWithCode.equals(txtState.getText()))
			{
				checkState=true;
				break;
			}
			else
			{
				checkState=false;
			}
			
		}
	}	
	
	
// ******************************************************************************

		public void reset()
		{
			txtCode.setEditable(true);
			
			txtCode.clear();
			txtCityName.clear();
			txtState.clear();
			btnSave.setDisable(false);
			btnUpdate.setDisable(true);
			
		}
	
		
// ******************************************************************************
		
	public void loadCityTable() throws SQLException
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int slno = 1;

		try 
		{

			stmt = con.createStatement();

			sql = MasterSQL_Location_Utils.LOADTABLE_SQL_CITY_DETAILS;

			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) 
			{
				CityBean ctBean=new CityBean();

				
				ctBean.setCityCode(rs.getString("city_code"));
				ctBean.setCityName(rs.getString("city_name"));
				ctBean.setStateCode(rs.getString("state_code"));

				tabledata_CityDetailsItems.add(new CityTableBean(slno, ctBean.getCityCode(), ctBean.getCityName(), ctBean.getStateCode()));
				slno++;

			}

			tableCity.setItems(tabledata_CityDetailsItems);
			
			
			pagination_CityDetails.setPageCount((tabledata_CityDetailsItems.size() / rowsPerPage() + 1));
			pagination_CityDetails.setCurrentPageIndex(0);
			pagination_CityDetails.setPageFactory((Integer pageIndex) -> createPage_ForwarderDetails(pageIndex));

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
		
	
// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 50;
	}

	public GridPane createPage_ForwarderDetails(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_CityDetailsItems.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_CityDetailsItems.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableCity.setItems(FXCollections.observableArrayList(tabledata_CityDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableCity.setItems(FXCollections.observableArrayList(tabledata_CityDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

// ******************************************************************************	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		btnUpdate.setDisable(true);
		imageViewSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<CityTableBean,Integer>("slno"));
		tabColumn_CityCode.setCellValueFactory(new PropertyValueFactory<CityTableBean,String>("cityCode"));
		tabColumn_CityName.setCellValueFactory(new PropertyValueFactory<CityTableBean,String>("cityName"));
		tabColumn_StateCode.setCellValueFactory(new PropertyValueFactory<CityTableBean,String>("stateCode"));
		
		
		
		try {
			loadState();
			loadCityTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
