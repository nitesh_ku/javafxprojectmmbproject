package com.onesoft.courier.master.location.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.master.location.bean.CountryBean;
import com.onesoft.courier.master.location.bean.CountryTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Location_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class CountryController implements Initializable{
	
	private ObservableList<CountryTableBean> tabledata_CountryDetailsItems=FXCollections.observableArrayList();
	
	@FXML
	private Pagination pagination_CountryDetails;
	
	@FXML
	private ImageView imageViewSearchIcon;
	
	@FXML
	private TextField txtCode;
	
	@FXML
	private TextField txtName;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	
	@FXML
	private TableView<CountryTableBean> tableCountry;
	
	@FXML
	private TableColumn<CountryTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<CountryTableBean, String> tabColumn_CountryCode;
	
	@FXML
	private TableColumn<CountryTableBean, String> tabColumn_CountryName;

	

// ******************************************************************************

	public void saveCountryDetails() throws SQLException 
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		CountryBean crtyBean=new CountryBean();

		crtyBean.setCountryCode(txtCode.getText());
		crtyBean.setCountryName(txtName.getText());
		

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_Location_Utils.INSERT_SQL_COUNTRY_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, crtyBean.getCountryCode());
			preparedStmt.setString(2, crtyBean.getCountryName());
			preparedStmt.setString(3, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(4, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);

			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				//reset();
				loadCountryTable();
				alert.setContentText("Country Details Successfully saved...");
				alert.showAndWait();
			}
			else 
			{
				alert.setContentText("Country Details not saved \nPlease try again...!");
				alert.showAndWait();
			}
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
	
	
// ******************************************************************************

	public void checkCountryCodeExistanceAndLoad() throws SQLException 
	{
		ResultSet rs = null;
		// Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		CountryBean crtyBean=new CountryBean();
		crtyBean.setCountryCode(txtCode.getText());

		try 
		{
			sql = MasterSQL_Location_Utils.DATA_RETRIEVE_SQL_COUNTRY_DETAILS;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, crtyBean.getCountryCode());

			rs = preparedStmt.executeQuery();

			if (!rs.next() == true) 
			{
				btnSave.setDisable(false);
				btnUpdate.setDisable(true);
			}
			else 
			{
				do {

					txtCode.setEditable(false);
					btnSave.setDisable(true);
					btnUpdate.setDisable(false);

					crtyBean.setCountryCode(rs.getString("country_code"));
					crtyBean.setCountryName(rs.getString("country_name"));
					txtCode.setText(crtyBean.getCountryCode());
					txtName.setText(crtyBean.getCountryName());
					

				} while (rs.next());
			}

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}	
	
	
	
// ******************************************************************************	

	public void updateCountryDetails() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		CountryBean crtyBean=new CountryBean();

		crtyBean.setCountryCode(txtCode.getText());
		crtyBean.setCountryName(txtName.getText());
		

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = MasterSQL_Location_Utils.UPDATE_SQL_COUNTRY_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, crtyBean.getCountryName());
			preparedStmt.setString(2, crtyBean.getCountryCode());
			
			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				reset();
				loadCountryTable();
				alert.setContentText("Country Details Successfully Updated...");
				alert.showAndWait();

			} 
			else 
			{
				btnSave.setDisable(true);
				btnUpdate.setDisable(false);
				alert.setContentText("Country Details not updated \nPlease try again...!");
				alert.showAndWait();
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
// ******************************************************************************
	
	public void loadCountryTable() throws SQLException {
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int slno = 1;

		try {

			stmt = con.createStatement();

			sql = MasterSQL_Location_Utils.LOADTABLE_SQL_COUNTRY_DETAILS;

			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				CountryBean crtyBean = new CountryBean();

				crtyBean.setCountryCode(rs.getString("country_code"));
				crtyBean.setCountryName(rs.getString("country_name"));

				tabledata_CountryDetailsItems
						.add(new CountryTableBean(slno, crtyBean.getCountryCode(), crtyBean.getCountryName()));
				slno++;

			}

			tableCountry.setItems(tabledata_CountryDetailsItems);

			pagination_CountryDetails.setPageCount((tabledata_CountryDetailsItems.size() / rowsPerPage() + 1));
			pagination_CountryDetails.setCurrentPageIndex(0);
			pagination_CountryDetails.setPageFactory((Integer pageIndex) -> createPage_ForwarderDetails(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
			
	// ============ Code for paginatation =====================================================

		public int itemsPerPage() {
			return 1;
		}

		public int rowsPerPage() {
			return 20;
		}

		public GridPane createPage_ForwarderDetails(int pageIndex) {
			int lastIndex = 0;

			GridPane pane = new GridPane();
			int displace = tabledata_CountryDetailsItems.size() % rowsPerPage();

			if (displace >= 0) {
				lastIndex = tabledata_CountryDetailsItems.size() / rowsPerPage();
			}

			int page = pageIndex * itemsPerPage();
			for (int i = page; i < page + itemsPerPage(); i++) {
				if (lastIndex == pageIndex) {
					tableCountry.setItems(FXCollections.observableArrayList(tabledata_CountryDetailsItems
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
				} else {
					tableCountry.setItems(FXCollections.observableArrayList(tabledata_CountryDetailsItems
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
				}
			}
			return pane;

		}	
		
		
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException 
	{
		Node n = (Node) e.getSource();
		
		if (n.getId().equals("txtCode")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				txtName.requestFocus();
			}
		} 
	
		else if (n.getId().equals("txtName")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				if (txtCode.isEditable() == true) 
				{
					btnSave.requestFocus();
				} 
				else 
				{
					btnUpdate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("btnSave")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				setValidation();
			}
		}

		else if (n.getId().equals("btnUpdate")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				setValidation();
			}
		}
	}
		
	// *************** Method to set Validation *******************

		public void setValidation() throws SQLException {

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);

			if (txtCode.getText() == null || txtCode.getText().isEmpty()) {
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Code");
				alert.showAndWait();
				txtCode.requestFocus();
			}

			else if (txtName.getText() == null || txtName.getText().isEmpty()) {
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Name");
				alert.showAndWait();
				txtName.requestFocus();
			}

			else 
			{
				if (btnSave.isDisable() == true) 
				{
					//System.out.println("Update Country");
					updateCountryDetails();
					
					reset();
					txtCode.requestFocus();
				}
				else 
				{
						//System.out.println("Save Country");
					saveCountryDetails();
					reset();
					txtCode.requestFocus();
				}
			

			}
		}		
		
	
// ******************************************************************************

	public void reset()
	{
		txtCode.setEditable(true);
			
		txtName.clear();
		txtCode.clear();
		
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
					
	}	
			
	
// ******************************************************************************

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		btnUpdate.setDisable(true);
		
		imageViewSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<CountryTableBean,Integer>("slno"));
		tabColumn_CountryName.setCellValueFactory(new PropertyValueFactory<CountryTableBean,String>("countryName"));
		tabColumn_CountryCode.setCellValueFactory(new PropertyValueFactory<CountryTableBean,String>("countryCode"));
		
		try {
			loadCountryTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
