package com.onesoft.courier.master.location.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.master.location.bean.AreaBean;
import com.onesoft.courier.master.location.bean.AreaTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Location_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class AreaController implements Initializable {

	private ObservableList<AreaTableBean> tabledata_AreaDetailsItems=FXCollections.observableArrayList();
	
	private List<String> list_City=new ArrayList<>();
	ObservableList<String> 	filteredList;
	

	String filter="";
	
	@FXML
	private Pagination pagination_AreaDetails;
	
	@FXML
	private ImageView imageViewSearchIcon;
	
	@FXML
	private TextField txtCode;
	
	@FXML
	private TextField txtName;
	
	@FXML
	private TextField txtPinCode;
	
	@FXML
	private TextField txtCity;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	boolean checkCity=false;
	
	
	@FXML
	private TableView<AreaTableBean> tableArea;
	
	@FXML
	private TableColumn<AreaTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<AreaTableBean, String> tabColumn_AreaCode;
	
	@FXML
	private TableColumn<AreaTableBean, String> tabColumn_AreaName;
	
	@FXML
	private TableColumn<AreaTableBean, String> tabColumn_Pincode;

	@FXML
	private TableColumn<AreaTableBean, String> tabColumn_CityCode;
	
	
// ******************************************************************************

	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName()+" | "+bean.getCityCode());
		}
		//comboBoxCity.setItems(comboBoxCityItems);
		TextFields.bindAutoCompletion(txtCity, list_City);

	}	
	

// ******************************************************************************
	
	public void checkAreaCodeExistanceAndLoad() throws SQLException
	{
		ResultSet rs = null;
		//Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		AreaBean aBean=new AreaBean();
		aBean.setAreaCode(txtCode.getText());

		try {

			sql = MasterSQL_Location_Utils.DATA_RETRIEVE_SQL_AREA_DETAILS;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, aBean.getAreaCode());
			
			rs = preparedStmt.executeQuery();
		
			
			if(!rs.next()==true)
			{
				btnSave.setDisable(false);
				btnUpdate.setDisable(true);
			}
			else
			{
				do{
					
					txtCode.setEditable(false);
					btnSave.setDisable(true);
					btnUpdate.setDisable(false);
									
					aBean.setCityName(rs.getString("cityname"));
					aBean.setCityCode(rs.getString("citycode"));
					aBean.setAreaName(rs.getString("areaname"));
					aBean.setPincode(rs.getString("zipcode"));
					
					txtName.setText(aBean.getAreaName());
					txtCity.setText(aBean.getCityName()+" | "+aBean.getCityCode());
					txtPinCode.setText(aBean.getPincode());
					
				}while (rs.next());
			}
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	
	
// ******************************************************************************	

	public void saveAreaDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		String[] city=txtCity.getText().replaceAll("\\s+","").split("\\|");
		
		AreaBean aBean=new AreaBean();
		
		aBean.setAreaCode(txtCode.getText());
		aBean.setAreaName(txtName.getText());
		aBean.setCityName(city[0]);
		aBean.setCityCode(city[1]);
		aBean.setPincode(txtPinCode.getText());
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_Location_Utils.INSERT_SQL_AREA_DETAILS;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, aBean.getAreaCode());
			preparedStmt.setString(2, aBean.getAreaName());
			preparedStmt.setString(3, aBean.getPincode());
			preparedStmt.setString(4, aBean.getCityCode());
			preparedStmt.setString(5, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(6, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);
			
			
			int status=preparedStmt.executeUpdate();
			
			if(status==1)
			{
				reset();
				loadAreaTable();
				alert.setContentText("Area Details Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Area Details not saved \nPlease try again...!");
				alert.showAndWait();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	

// ******************************************************************************	

	public void updateAreaDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		String[] city=txtCity.getText().replaceAll("\\s+","").split("\\|");
	
		AreaBean aBean=new AreaBean();
	
		aBean.setAreaCode(txtCode.getText());
		aBean.setAreaName(txtName.getText());
		aBean.setCityName(city[0]);
		aBean.setCityCode(city[1]);
		aBean.setPincode(txtPinCode.getText());
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = MasterSQL_Location_Utils.UPDATE_SQL_AREA_DETAILS;
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1, aBean.getAreaCode());
			preparedStmt.setString(1, aBean.getAreaName());
			preparedStmt.setString(2, aBean.getPincode());
			preparedStmt.setString(3, aBean.getCityCode());
			preparedStmt.setString(4, aBean.getAreaCode());
			
			
			int status=preparedStmt.executeUpdate();
			
			if(status==1)
			{
				reset();
				loadAreaTable();
				alert.setContentText("Area Details Successfully Updated...");
				alert.showAndWait();
				
			}
			else
			{
				btnSave.setDisable(true);
				btnUpdate.setDisable(false);
				alert.setContentText("Area Details not updated \nPlease try again...!");
				alert.showAndWait();
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	


// ******************************************************************************

	public void reset()
	{
		txtCode.setEditable(true);
		txtCity.clear();
		txtCode.clear();
		txtName.clear();
		txtPinCode.clear();
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
		
	}

// ******************************************************************************	
	
	public void checkCityExistance()
	{
		for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME)
		{
			String cityWithCode=cityBean.getCityName()+" | "+cityBean.getCityCode();
			if(cityWithCode.equals(txtCity.getText()))
			{
				checkCity=true;
				break;
			}
			else
			{
				checkCity=false;
			}
			
		}
	}
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
			
		
		if (n.getId().equals("txtCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				//checkAreaCodeExistanceAndLoad();
				txtName.requestFocus();
			}
		}
		else if (n.getId().equals("txtName")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtPinCode.requestFocus();
			}
		}

		else if (n.getId().equals("txtPinCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCity.requestFocus();
			}
		}

		else if (n.getId().equals("txtCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
					if(txtCode.isEditable()==true)
					{
						btnSave.requestFocus();
					}
					else
					{
						btnUpdate.requestFocus();
					}
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				setValidation();
			
			}
		}
		
		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
				
			}
		}

	}
			

// *************** Method to set Validation *******************

	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if (txtCode.getText() == null || txtCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtCode.requestFocus();
		} 
		
		else if (txtName.getText() == null || txtName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtName.requestFocus();
		}
		
		else if(!txtPinCode.getText().matches("[0-9]*"))
		{
	    	alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only Numbers");
			alert.showAndWait();
			txtPinCode.requestFocus();
		}
		
		else if (txtPinCode.getText() == null || txtPinCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a PinCode");
			alert.showAndWait();
			txtPinCode.requestFocus();
		}
		
		else if (txtCity.getText() == null || txtCity.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a City");
			alert.showAndWait();
			txtCity.requestFocus();
		}
		
		else 
		{
			checkCityExistance();
			
			if(checkCity==true)
			{
				if(btnSave.isDisable()==true)
				{
					updateAreaDetails();
					//System.err.println("Update");
					reset();
					txtCode.requestFocus();
				}
				else
				{
					saveAreaDetails();
					//System.err.println("Save");
					reset();
					txtCode.requestFocus();
				}
			}
			else
			{
				alert.setTitle("Incorrect city name");
				alert.setContentText("Please enter correct city name...!!");
				alert.showAndWait();
				txtCity.requestFocus();
				checkCity=false;
			}
			
		}
	}	
		
	
// ******************************************************************************
	
	public void loadAreaTable() throws SQLException
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		
		int slno=1;

		try {

			stmt = con.createStatement();
			
			sql = MasterSQL_Location_Utils.LOADTABLE_SQL_AREA_DETAILS;
			

			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next())
			{		
				AreaBean aBean=new AreaBean();
				
				aBean.setAreaCode(rs.getString("code"));
				aBean.setCityCode(rs.getString("city_code"));
				aBean.setAreaName(rs.getString("area_name"));
				aBean.setPincode(rs.getString("zip_code"));
				
				tabledata_AreaDetailsItems.add(new AreaTableBean(slno, aBean.getAreaCode(), aBean.getAreaName(),
								aBean.getPincode(), aBean.getCityCode()));
				slno++;
				
			}
			
			tableArea.setItems(tabledata_AreaDetailsItems);
			
			pagination_AreaDetails.setPageCount((tabledata_AreaDetailsItems.size() / rowsPerPage() + 1));
			pagination_AreaDetails.setCurrentPageIndex(0);
			pagination_AreaDetails.setPageFactory((Integer pageIndex) -> createPage_ForwarderDetails(pageIndex));
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
	// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 20;
	}

	public GridPane createPage_ForwarderDetails(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_AreaDetailsItems.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_AreaDetailsItems.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableArea.setItems(FXCollections.observableArrayList(tabledata_AreaDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableArea.setItems(FXCollections.observableArrayList(tabledata_AreaDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

// ******************************************************************************
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imageViewSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		btnUpdate.setDisable(true);

		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<AreaTableBean,Integer>("slno"));
		tabColumn_CityCode.setCellValueFactory(new PropertyValueFactory<AreaTableBean,String>("cityCode"));
		tabColumn_Pincode.setCellValueFactory(new PropertyValueFactory<AreaTableBean,String>("pincode"));
		tabColumn_AreaName.setCellValueFactory(new PropertyValueFactory<AreaTableBean,String>("areaName"));
		tabColumn_AreaCode.setCellValueFactory(new PropertyValueFactory<AreaTableBean,String>("areaCode"));
		
		
		
		try {
			loadCity();
			loadAreaTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
