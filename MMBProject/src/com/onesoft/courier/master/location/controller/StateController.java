package com.onesoft.courier.master.location.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.master.location.bean.CityBean;
import com.onesoft.courier.master.location.bean.CityTableBean;
import com.onesoft.courier.master.location.bean.StateBean;
import com.onesoft.courier.master.location.bean.StateTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Location_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class StateController implements Initializable {
	
	private ObservableList<StateTableBean> tabledata_StateDetailsItems=FXCollections.observableArrayList();
	private List<String> list_Country=new ArrayList<>();
	
	@FXML
	private Pagination pagination_StateDetails;
	
	@FXML
	private ImageView imageViewSearchIcon;
	
	@FXML
	private TextField txtStateCode;
	
	@FXML
	private TextField txtStateName;
	
	@FXML
	private TextField txtCountry;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnUpdate;
	
	boolean checkCountry=false;

	@FXML
	private TableView<StateTableBean> tableState;
	
	@FXML
	private TableColumn<StateTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<StateTableBean, String> tabColumn_StateCode;
	
	@FXML
	private TableColumn<StateTableBean, String> tabColumn_StateName;
	
	@FXML
	private TableColumn<StateTableBean, String> tabColumn_CountryCode;


	
// ******************************************************************************

	public void saveStateDetails() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		String[] country = txtCountry.getText().replaceAll("\\s+", "").split("\\|");

		StateBean stBean=new StateBean();

		stBean.setStateCode(txtStateCode.getText());
		stBean.setStateName(txtStateName.getText());
		stBean.setCountry(country[1]);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = MasterSQL_Location_Utils.INSERT_SQL_STATE_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, stBean.getStateCode());
			preparedStmt.setString(2, stBean.getCountry());
			preparedStmt.setString(3, stBean.getStateName());
			preparedStmt.setString(4, MasterSQL_Location_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(5, MasterSQL_Location_Utils.LAST_MODIFIED_USER_FORALL);

			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				reset();
				loadStateTable();
				alert.setContentText("State Details Successfully saved...");
				alert.showAndWait();
			} 
			else 
			{
				alert.setContentText("State Details not saved \nPlease try again...!");
				alert.showAndWait();
			}
		} 
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}	
	
	
// ******************************************************************************

	public void checkStateCodeExistanceAndLoad() throws SQLException 
	{
		ResultSet rs = null;
		// Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		StateBean stBean=new StateBean();
		stBean.setStateCode(txtStateCode.getText());

		try {
			sql = MasterSQL_Location_Utils.DATA_RETRIEVE_SQL_STATE_DETAILS;
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, stBean.getStateCode());

			rs = preparedStmt.executeQuery();

			if (!rs.next() == true) 
			{
				btnSave.setDisable(false);
				btnUpdate.setDisable(true);
			}
			else 
			{
				do {

					txtStateCode.setEditable(false);
					btnSave.setDisable(true);
					btnUpdate.setDisable(false);

					stBean.setStateCode(rs.getString("stcode"));
					stBean.setStateName(rs.getString("stname"));
					stBean.setCountry(rs.getString("countryname") + " | " + rs.getString("countrycode"));
					txtStateCode.setText(stBean.getStateCode());
					txtStateName.setText(stBean.getStateName());
					txtCountry.setText(stBean.getCountry());

				} while (rs.next());
			}

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}	
	
	
	
// ******************************************************************************	

	public void updateStateDetails() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		String[] country = txtCountry.getText().replaceAll("\\s+", "").split("\\|");

		StateBean stBean=new StateBean();

		stBean.setStateCode(txtStateCode.getText());
		stBean.setStateName(txtStateName.getText());
		stBean.setCountry(country[1]);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_Location_Utils.UPDATE_SQL_STATE_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, stBean.getStateName());
			preparedStmt.setString(2, stBean.getCountry());
			preparedStmt.setString(3, stBean.getStateCode());

			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				reset();
				loadStateTable();
				alert.setContentText("State Details Successfully Updated...");
				alert.showAndWait();

			}
			else 
			{
				btnSave.setDisable(true);
				btnUpdate.setDisable(false);
				alert.setContentText("State Details not updated \nPlease try again...!");
				alert.showAndWait();
			}
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
	
// ******************************************************************************
			
	public void loadStateTable() throws SQLException 
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int slno = 1;

		try 
		{

			stmt = con.createStatement();

			sql = MasterSQL_Location_Utils.LOADTABLE_SQL_STATE_DETAILS;

			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) 
			{
				StateBean stBean=new StateBean();

				stBean.setStateCode(rs.getString("state_code"));
				stBean.setStateName(rs.getString("state_name"));
				stBean.setCountry(rs.getString("country_code"));

				tabledata_StateDetailsItems.add(new StateTableBean(slno, stBean.getStateCode(), stBean.getStateName(), stBean.getCountry()));
				slno++;

			}

			tableState.setItems(tabledata_StateDetailsItems);

			pagination_StateDetails.setPageCount((tabledata_StateDetailsItems.size() / rowsPerPage() + 1));
			pagination_StateDetails.setCurrentPageIndex(0);
			pagination_StateDetails.setPageFactory((Integer pageIndex) -> createPage_ForwarderDetails(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
		
// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 20;
	}

	public GridPane createPage_ForwarderDetails(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_StateDetailsItems.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_StateDetailsItems.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableState.setItems(FXCollections.observableArrayList(tabledata_StateDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableState.setItems(FXCollections.observableArrayList(tabledata_StateDetailsItems
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("txtStateCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				txtStateName.requestFocus();
			}
		} else if (n.getId().equals("txtStateName")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCountry.requestFocus();
			}
		}

		else if (n.getId().equals("txtCountry")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				if (txtStateCode.isEditable() == true) {
					btnSave.requestFocus();
				} else {
					btnUpdate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				setValidation();

			}
		}

		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();

			}
		}

	}
		
// *************** Method to set Validation *******************

	public void setValidation() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if (txtStateCode.getText() == null || txtStateCode.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtStateCode.requestFocus();
		}

		else if (txtStateName.getText() == null || txtStateName.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtStateName.requestFocus();
		}

		else if (txtCountry.getText() == null || txtCountry.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Country");
			alert.showAndWait();
			txtCountry.requestFocus();
		}

		else {
			checkStateExistance();

			if (checkCountry == true) {
				if (btnSave.isDisable() == true) {
					//System.out.println("Update Country");
					updateStateDetails();
					
					reset();
					txtStateCode.requestFocus();
				} else {

					//System.out.println("Save Country");
					saveStateDetails();
					reset();
					txtStateCode.requestFocus();
				}
			} else {
				alert.setTitle("Incorrect Country name");
				alert.setContentText("Please enter correct Country name...!!");
				alert.showAndWait();
				txtCountry.requestFocus();
				checkCountry = false;
			}

		}
	}
			
// ******************************************************************************

	public void checkStateExistance() 
	{
		for (LoadCityBean cityBean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			String countryWithCode = cityBean.getCountryName() + " | " + cityBean.getCountryCode();
			if (countryWithCode.equals(txtCountry.getText())) 
			{
				checkCountry = true;
				break;
			}
			else 
			{
				checkCountry = false;
			}

		}
	}
	
	
// ******************************************************************************

	public void reset()
	{
		txtStateCode.setEditable(true);
			
		txtStateCode.clear();
		txtStateName.clear();
		txtCountry.clear();
		btnSave.setDisable(false);
		btnUpdate.setDisable(true);
				
	}		
	
// ******************************************************************************

	public void loadCountry() throws SQLException 
	{
		new LoadCountry().loadCountryWithName();

		for (LoadCityBean bean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			list_Country.add(bean.getCountryName() + " | " + bean.getCountryCode());
		}
		// comboBoxCity.setItems(comboBoxCityItems);
		TextFields.bindAutoCompletion(txtCountry, list_Country);
	}	
	
// ******************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		btnUpdate.setDisable(true);
		imageViewSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		

		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<StateTableBean,Integer>("slno"));
		tabColumn_StateCode.setCellValueFactory(new PropertyValueFactory<StateTableBean,String>("stateCode"));
		tabColumn_StateName.setCellValueFactory(new PropertyValueFactory<StateTableBean,String>("stateName"));
		tabColumn_CountryCode.setCellValueFactory(new PropertyValueFactory<StateTableBean,String>("countryCode"));
		
		
		try {
			loadCountry();
			loadStateTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
