package com.onesoft.courier.administration.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class DeliveryStatusTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty forwarderNumber;
	private SimpleStringProperty Network;
	private SimpleStringProperty deliveryStatus;
	private SimpleStringProperty deliveryTime;
	private SimpleStringProperty deliveryDate;
	private SimpleStringProperty receivedBy;
	private SimpleStringProperty branch;
	private SimpleStringProperty client;

	
	public DeliveryStatusTableBean(int slno, String awbno,String status, String date, String time, String receivedBy,String client,String network,String branch)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbNo=new SimpleStringProperty(awbno);
		this.deliveryStatus=new SimpleStringProperty(status);
		this.deliveryDate=new SimpleStringProperty(date);
		this.deliveryTime=new SimpleStringProperty(time);
		this.receivedBy=new SimpleStringProperty(receivedBy);
		this.Network=new SimpleStringProperty(network);
		this.client=new SimpleStringProperty(client);
		this.branch=new SimpleStringProperty(branch);
		
				
	}
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getAwbNo() {
		return awbNo.get();
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public String getForwarderNumber() {
		return forwarderNumber.get();
	}
	public void setForwarderNumber(SimpleStringProperty forwarderNumber) {
		this.forwarderNumber = forwarderNumber;
	}
	public String getNetwork() {
		return Network.get();
	}
	public void setNetwork(SimpleStringProperty network) {
		Network = network;
	}
	public String getDeliveryStatus() {
		return deliveryStatus.get();
	}
	public void setDeliveryStatus(SimpleStringProperty deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getDeliveryTime() {
		return deliveryTime.get();
	}
	public void setDeliveryTime(SimpleStringProperty deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getDeliveryDate() {
		return deliveryDate.get();
	}
	public void setDeliveryDate(SimpleStringProperty deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getReceivedBy() {
		return receivedBy.get();
	}
	public void setReceivedBy(SimpleStringProperty receivedBy) {
		this.receivedBy = receivedBy;
	}
	public String getClient() {
		return client.get();
	}
	public void setClient(SimpleStringProperty client) {
		this.client = client;
	}
	public String getBranch() {
		return branch.get();
	}
	public void setBranch(SimpleStringProperty branch) {
		this.branch = branch;
	}

}
