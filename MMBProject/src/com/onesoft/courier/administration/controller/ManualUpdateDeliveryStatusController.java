package com.onesoft.courier.administration.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.administration.bean.DeliveryStatusBean;
import com.onesoft.courier.administration.bean.DeliveryStatusTableBean;
import com.onesoft.courier.administration.bean.DeliveryStatusTableBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import jfxtras.scene.control.LocalDateTimeTextField;


public class ManualUpdateDeliveryStatusController implements Initializable{
	
	/*List<DeliveryStatusBean> DeliveryStatusUtils.list_awbNoWithNetwork=new ArrayList<>();
	List<DeliveryStatusBean> DeliveryStatusUtils.list_DeliveredAwbNo=new ArrayList<>();*/
	
	private ObservableList<DeliveryStatusTableBean> tableData_DeliveredAwbno=FXCollections.observableArrayList();
	private ObservableList<DeliveryStatusTableBean> tableData_Un_DeliveredAwbno=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxDeliveryStatusItems=FXCollections.observableArrayList(DeliveryStatusUtils.UN_DELIVERED,DeliveryStatusUtils.RETURN,DeliveryStatusUtils.DELIVERED);
	
	@FXML
	private Pagination pagination_Delivered;
	
	@FXML
	private Pagination pagination_UnDelivered;
	
	@FXML
	private ImageView imgSearchIcon_Delivered;
	
	@FXML
	private ImageView imgSearchIcon_UnDelivered;
	
	@FXML
	private TextField txtAwbNo;
	
	@FXML
	private TextField txtBranch;
	
	@FXML
	private TextField txtNetwork;
	
	@FXML
	private TextField txtClient;
	
//	@FXML
//	private TextField txtDeliveryStatus;
	
	@FXML
	private TextField txtReceivedBy;
	
	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExportToExcel_Delivered;
	
	@FXML
	private Button btnExportToExcel_UnDelivered;
	
	@FXML
	private ComboBox<String> comboBoxDeliveryStatus;
	
	@FXML
	private LocalDateTimeTextField localDateTime_DeliveryDateTime;
	
	
//================================================================

	@FXML
	private TableView<DeliveryStatusTableBean> TableDeliveredAwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, Integer> delvrd_Col_slno;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_AwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_ReceivedBy;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_Date;

	/*@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_Time;*/
		
		
//================================================================

	@FXML
	private TableView<DeliveryStatusTableBean> TableUnDeliveredAwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, Integer> unDelvrd_Col_slno;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_AwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Client;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Branch;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Network;	
	
	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_DeliveryStatus;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Date;
	
	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_ReceivedBy;


// ***********************************************************************************
	
	public void timePicker()
	{
		System.out.println("Local Date Time Picker value :: "+localDateTime_DeliveryDateTime.getText());
	}
	
	
// ***********************************************************************************	
	
	public void getAwbDeliveryStatus() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;
		
		DeliveryStatusBean dsBean=new DeliveryStatusBean();
		dsBean.setAwbNo(txtAwbNo.getText());

		try 
		{
				
			String sql = "SELECT air_way_bill_number, master_awbno, mps_type, dailybookingtransaction2branch,"
					+ "dailybookingtransaction2client,dailybookingtransaction2network, delivery_status,receive_detail,"
					+ "delivery_date FROM dailybookingtransaction where master_awbno=? and mps_type='T'";

			preparedStmt = con.prepareStatement(sql);
			
			preparedStmt.setString(1,dsBean.getAwbNo());

			System.out.println(sql);
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				System.out.println("NO DATA FOUND");
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle("Alert");
				alert.setContentText("Data Not Found Or Already Delivered...");
				alert.showAndWait();
				
				txtBranch.clear();
				txtClient.clear();
				txtNetwork.clear();
				comboBoxDeliveryStatus.setValue(DeliveryStatusUtils.UN_DELIVERED);
				txtReceivedBy.clear();
				txtAwbNo.requestFocus();
				
			} 
			else 
			{
				do 
				{
					dsBean.setBranch(rs.getString("dailybookingtransaction2branch"));
					dsBean.setClient(rs.getString("dailybookingtransaction2client"));
					dsBean.setNetwork(rs.getString("dailybookingtransaction2network"));
					dsBean.setDeliveryStatus(rs.getString("delivery_status"));
					dsBean.setReceivedBy(rs.getString("receive_detail"));
					dsBean.setDeliveryDate(rs.getString("delivery_date"));
				} 
				while (rs.next());
				
				txtBranch.setText(dsBean.getBranch());
				txtClient.setText(dsBean.getClient());
				txtNetwork.setText(dsBean.getNetwork());
				if(dsBean.getDeliveryStatus()==null)
				{
					comboBoxDeliveryStatus.setValue(DeliveryStatusUtils.UN_DELIVERED);
				}
				else
				{
					comboBoxDeliveryStatus.setValue(dsBean.getDeliveryStatus());	
				}
				
				txtReceivedBy.setText(dsBean.getReceivedBy());
			
				if(dsBean.getDeliveryDate()!=null)
				localDateTime_DeliveryDateTime.setText(dsBean.getDeliveryDate());
			}
			
			setUpdateButtonEnableDisable();
			
			//setReceivedby();
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ***********************************************************************************

	public void updateDeliveryStatusToDB() throws SQLException
	{		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		/*LocalDate local_DelDate=dpkDeliveryDate.getValue();
		Date delDate=Date.valueOf(local_DelDate);*/
		
		DeliveryStatusBean dsBean=new DeliveryStatusBean();
		
		dsBean.setDeliveryStatus(comboBoxDeliveryStatus.getValue());
		dsBean.setDeliveryDate(localDateTime_DeliveryDateTime.getText());
		dsBean.setReceivedBy(txtReceivedBy.getText());
		dsBean.setAwbNo(txtAwbNo.getText());
		dsBean.setBranch(txtBranch.getText());
		dsBean.setNetwork(txtNetwork.getText());
		dsBean.setClient(txtClient.getText());
		
		try 
		{
			String query = "update dailybookingtransaction SET delivery_status=?,delivery_date=?,receive_detail=? where air_way_bill_number=?";
				
			preparedStmt = con.prepareStatement(query);
				
			preparedStmt.setString(1, dsBean.getDeliveryStatus());
			preparedStmt.setString(2, dsBean.getDeliveryDate());
			preparedStmt.setString(3, dsBean.getReceivedBy());
			preparedStmt.setString(4, dsBean.getAwbNo());
			
				
			int updateStatus=preparedStmt.executeUpdate();
			
			
			if(updateStatus==1)
			{
				int checkAwb=0;
				for(DeliveryStatusBean dlBean: DeliveryStatusUtils.list_DeliveredAwbNo)
				{	
					if(dsBean.getAwbNo().equals(dlBean.getAwbNo()))
					{
						DeliveryStatusUtils.list_DeliveredAwbNo.remove(checkAwb);
						break;
					}
					checkAwb++;
				}
				
				checkAwb=0;
				
				if(dsBean.getDeliveryStatus().equals(DeliveryStatusUtils.DELIVERED))
				{
					DeliveryStatusUtils.list_DeliveredAwbNo.add(dsBean);
				}
				//DeliveryStatusUtils.list_DeliveredAwbNo.add(dsBean);
				
				
				for(DeliveryStatusBean dlBean: DeliveryStatusUtils.list_awbNoWithNetwork)
				{	
					if(dsBean.getAwbNo().equals(dlBean.getAwbNo()))
					{
						DeliveryStatusUtils.list_awbNoWithNetwork.remove(checkAwb);
						break;
					}
					checkAwb++;
				}
					
				if(!dsBean.getDeliveryStatus().equals(DeliveryStatusUtils.DELIVERED))
				{
					DeliveryStatusUtils.list_awbNoWithNetwork.add(dsBean);
				}
				
				loadDeliveredTable();
				loadUnDeliveredTable();
				
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle("Alert");
				alert.setContentText("Delivery Status Successfully Updated");
				alert.showAndWait();
				
			}
			else
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle("Alert");
				alert.setContentText("Delivery Status Is Not Updated...!!\nPlease Check");
				alert.showAndWait();
			}
			
			
		
		} 
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
	
// ***************************************************************************************	

	public void loadUnDeliveredData() throws SQLException
	{	
		if(DeliveryStatusUtils.list_awbNoWithNetwork.isEmpty()==true)
		{
			DeliveryStatusUtils.list_awbNoWithNetwork.clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
	
			Statement st=null;
			ResultSet rs=null;
			PreparedStatement preparedStmt=null;
			String sql=null;
	
			try
			{	
				sql = "select air_way_bill_number,dailybookingtransaction2network,dailybookingtransaction2client,"
						+ "dailybookingtransaction2branch,delivery_status,receive_detail,delivery_date from "
						+ "dailybookingtransaction where delivery_status!='Delivered' or delivery_status is null and mps_type='T' "
						+ "and booking_date > current_date - interval '3 day' order by dailybookingtransactionid DESC";
				
				
				System.out.println(sql);
				st=con.createStatement();
				rs=st.executeQuery(sql);
	
				if(!rs.next())
				{
	
				}
				else
				{
					do
					{
						DeliveryStatusBean dsBean=new DeliveryStatusBean();
	
						dsBean.setAwbNo(rs.getString("air_way_bill_number"));
						//dsBean.setForwarderNumber(rs.getString("forwarder_number"));
						dsBean.setNetwork(rs.getString("dailybookingtransaction2network"));
						dsBean.setClient(rs.getString("dailybookingtransaction2client"));
						dsBean.setBranch(rs.getString("dailybookingtransaction2branch"));
						dsBean.setDeliveryStatus(rs.getString("delivery_status"));
						dsBean.setDeliveryDate(rs.getString("delivery_date"));
						dsBean.setReceivedBy(rs.getString("receive_detail"));
	
							DeliveryStatusUtils.list_awbNoWithNetwork.add(dsBean);
	
					}while(rs.next());
				}
	
				System.out.println("List Size :: "+DeliveryStatusUtils.list_awbNoWithNetwork.size());
				
			}
	
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
	}	
	

//************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
	
		
		if (n.getId().equals("txtAwbNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxDeliveryStatus.requestFocus();
				
			}
		}
		
		else if (n.getId().equals("comboBoxDeliveryStatus")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				//setReceivedby();
				localDateTime_DeliveryDateTime.requestFocus();
				
			}
		}


		else if (n.getId().equals("localDateTime_DeliveryDateTime")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtReceivedBy.requestFocus();
			}
		}

		else if (n.getId().equals("txtReceivedBy")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnUpdate.requestFocus();
			}
		}
		
		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				System.out.println("Ok.................");
				
				setValidation();
			}
		}

		
	}		


// *************** Method to set Validation *******************

	public void setValidation() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if (txtAwbNo.getText() == null || txtAwbNo.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Awb No.");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		} 

		else if (txtBranch.getText() == null || txtBranch.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Branch");
			alert.showAndWait();
			txtBranch.requestFocus();
		}
		
		else if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Client");
			alert.showAndWait();
			txtClient.requestFocus();
		}
		
		else if (txtNetwork.getText() == null || txtNetwork.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Network");
			alert.showAndWait();
			txtNetwork.requestFocus();
		}

		else 
		{
			updateDeliveryStatusToDB();

		}
	}		

	
	
// ***************************************************************************************
	
	public void loadDeliveredTable()
	{
		tableData_DeliveredAwbno.clear();
		int slno=1;
		for(DeliveryStatusBean dsBean:DeliveryStatusUtils.list_DeliveredAwbNo)
		{
			tableData_DeliveredAwbno.add(new DeliveryStatusTableBean(slno, dsBean.getAwbNo(),null, dsBean.getDeliveryDate(),null, dsBean.getReceivedBy(),null,null,null));
			slno++;
		}
		TableDeliveredAwbNo.setItems(tableData_DeliveredAwbno);
		
		if(tableData_DeliveredAwbno.isEmpty()==false)
		{
			pagination_Delivered.setVisible(true);
			btnExportToExcel_Delivered.setVisible(true);
			
			pagination_Delivered.setPageCount((tableData_DeliveredAwbno.size() / rowsPerPage() + 1));
			pagination_Delivered.setCurrentPageIndex(0);
			pagination_Delivered.setPageFactory((Integer pageIndex) -> createPage_DeliveredAwb(pageIndex));
		}
		else
		{
			pagination_Delivered.setVisible(false);
			btnExportToExcel_Delivered.setVisible(false);
		}
		

	}	
	
	
// ***************************************************************************************

	public void loadUnDeliveredTable()
	{
		tableData_Un_DeliveredAwbno.clear();
		int slno=1;
		
		if(DeliveryStatusUtils.list_DeliveredAwbNo.isEmpty()==false)
		{
			for(DeliveryStatusBean delvrdBean:DeliveryStatusUtils.list_DeliveredAwbNo)
			{	
				for(DeliveryStatusBean dsBean:DeliveryStatusUtils.list_awbNoWithNetwork)
				{
					if(!delvrdBean.getAwbNo().equals(dsBean.getAwbNo()))
					{
						tableData_Un_DeliveredAwbno.add(new DeliveryStatusTableBean(slno, dsBean.getAwbNo(),dsBean.getDeliveryStatus(), dsBean.getDeliveryDate(),null, dsBean.getReceivedBy(),dsBean.getClient(),dsBean.getNetwork(),dsBean.getBranch()));
						slno++;
					}
				}
			}
		}
		else
		{
			for(DeliveryStatusBean dsBean:DeliveryStatusUtils.list_awbNoWithNetwork)
			{
				tableData_Un_DeliveredAwbno.add(new DeliveryStatusTableBean(slno, dsBean.getAwbNo(),dsBean.getDeliveryStatus(), dsBean.getDeliveryDate(),null, dsBean.getReceivedBy(),dsBean.getClient(),dsBean.getNetwork(),dsBean.getBranch()));
				slno++;
			}
		}
		TableUnDeliveredAwbNo.setItems(tableData_Un_DeliveredAwbno);
		
		
		if(tableData_Un_DeliveredAwbno.isEmpty()==false)
		{
			pagination_UnDelivered.setVisible(true);
			btnExportToExcel_UnDelivered.setVisible(true);
			
			pagination_UnDelivered.setPageCount((tableData_Un_DeliveredAwbno.size() / rowsPerPage() + 1));
			pagination_UnDelivered.setCurrentPageIndex(0);
			pagination_UnDelivered.setPageFactory((Integer pageIndex) -> createPage_UnDeliveredAwb(pageIndex));
		}
		else
		{
			pagination_UnDelivered.setVisible(false);
			btnExportToExcel_UnDelivered.setVisible(false);
		}
			
	}	
	
	
	// ============ Code for paginatation =====================================================

		public int itemsPerPage() {
			return 1;
		}

		public int rowsPerPage() {
			return 10;
		}

		public GridPane createPage_UnDeliveredAwb(int pageIndex) {
			int lastIndex = 0;

			GridPane pane = new GridPane();
			int displace = tableData_Un_DeliveredAwbno.size() % rowsPerPage();

			if (displace >= 0) {
				lastIndex = tableData_Un_DeliveredAwbno.size() / rowsPerPage();
			}

			int page = pageIndex * itemsPerPage();
			for (int i = page; i < page + itemsPerPage(); i++) {
				if (lastIndex == pageIndex) {
					TableUnDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_Un_DeliveredAwbno
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
				} else {
					TableUnDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_Un_DeliveredAwbno
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
				}
			}
			return pane;

		}

		public GridPane createPage_DeliveredAwb(int pageIndex) {
			int lastIndex = 0;

			GridPane pane = new GridPane();
			int displace = tableData_DeliveredAwbno.size() % rowsPerPage();

			if (displace >= 0) {
				lastIndex = tableData_DeliveredAwbno.size() / rowsPerPage();
			}

			int page = pageIndex * itemsPerPage();
			for (int i = page; i < page + itemsPerPage(); i++) {
				if (lastIndex == pageIndex) {
					TableDeliveredAwbNo.setItems(FXCollections.observableArrayList(
							tableData_DeliveredAwbno.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
				} else {
					TableDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_DeliveredAwbno
							.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
				}
			}
			return pane;

		}		
	
// ***********************************************************************************
	
	public void setUpdateButtonEnableDisable()
	{
		if(!txtAwbNo.getText().equals("") && !txtBranch.getText().equals("") && !txtClient.getText().equals("") && !txtNetwork.getText().equals(""))
		{
			btnUpdate.setDisable(false);
		}
		else
		{
			btnUpdate.setDisable(true);
			
		}
	}

// ***********************************************************************************	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		imgSearchIcon_Delivered.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIcon_UnDelivered.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtBranch.setEditable(false);
		txtClient.setEditable(false);
		txtNetwork.setEditable(false);
		btnUpdate.setDisable(true);
		
		//txtReceivedBy.setDisable(true);
		//localDateTime_DeliveryDateTime.setDisable(true);
		
		comboBoxDeliveryStatus.setValue(DeliveryStatusUtils.UN_DELIVERED);
		comboBoxDeliveryStatus.setItems(comboBoxDeliveryStatusItems);
		
		delvrd_Col_slno.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,Integer>("slno"));
		delvrd_Col_AwbNo.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("awbNo"));
		delvrd_Col_Date.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryDate"));
		//delvrd_Col_Time.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryTime"));
		delvrd_Col_ReceivedBy.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("receivedBy"));
		
		unDelvrd_Col_slno.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,Integer>("slno"));
		unDelvrd_Col_AwbNo.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("awbNo"));
		unDelvrd_Col_Client.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("client"));
		unDelvrd_Col_Branch.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("branch"));
		unDelvrd_Col_Network.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("Network"));
		unDelvrd_Col_Date.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryDate"));
		unDelvrd_Col_DeliveryStatus.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryStatus"));
		unDelvrd_Col_ReceivedBy.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("receivedBy"));
		
		
		try {
			loadUnDeliveredData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loadDeliveredTable();
		loadUnDeliveredTable();
		
	}

}
