package com.onesoft.courier.administration.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.administration.bean.DeliveryStatusBean;
import com.onesoft.courier.administration.bean.DeliveryStatusTableBean;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.fedex.tracking.TrackWebServiceClient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AutoUpdateDeliveryStatusController implements Initializable{
	
	Task<Void> task;
	List<DeliveryStatusBean> list_awbNoWithNetwork=new ArrayList<>();
	List<String> list_awbNo=new ArrayList<>();
	
	List<DeliveryStatusBean> list_DeliveredAwbNo=new ArrayList<>();
	
	
	private ObservableList<DeliveryStatusTableBean> tableData_DeliveredAwbno=FXCollections.observableArrayList();
	private ObservableList<DeliveryStatusTableBean> tableData_Un_DeliveredAwbno=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	
	public static String fedex_deliveryStatus=null;
	public static String fedex_deliveryDate=null;
	public static String fedex_deliveryTime=null;
	public static String fedex_received_by=null;
	
	public static String bluedart_deliveryStatus=null;
	public static String bluedart_deliveryDate=null;
	public static String bluedart_deliveryTime=null;
	public static String bluedart_received_by=null;
	
	@FXML
	private ImageView imgSearchIcon_Delivered;
	
	@FXML
	private ImageView imgSearchIcon_UnDelivered;
	
	@FXML
	private Pagination pagination_Delivered;
	
	@FXML
	private Pagination pagination_UnDelivered;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private DatePicker dpkFrom;
	
	@FXML
	private DatePicker dpkTo;
	
	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExportToExcel_Delivered;
	
	@FXML
	private Button btnExportToExcel_UnDelivered;
	
//================================================================

	@FXML
	private TableView<DeliveryStatusTableBean> TableDeliveredAwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, Integer> delvrd_Col_slno;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_AwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_ReceivedBy;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_Date;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> delvrd_Col_Time;
	
	
//================================================================

	@FXML
	private TableView<DeliveryStatusTableBean> TableUnDeliveredAwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, Integer> unDelvrd_Col_slno;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_AwbNo;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Client;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Branch;

	@FXML
	private TableColumn<DeliveryStatusTableBean, String> unDelvrd_Col_Network;	


// ***************************************************************************************		
	
	public void loadBranch() throws SQLException
	{	
		comboBoxBranchItems.clear();
		new	LoadBranch().loadBranchWithName();
		
		System.out.println("Network Compare Contorller >> value from LoadBranch Class: >>>>>>>> ");
		comboBoxBranch.setValue("All Branch");
		comboBoxBranchItems.add("All Branch");
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			System.out.println("Company from branch: >> "+branchBean.getCmpnyCode());
			
			comboBoxBranchItems.add(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
		
	}	

// ***************************************************************************************	
	
	public void loadClientsViaBranch() throws SQLException
	{
		comboBoxClientItems.clear();
		
		new LoadClients().loadClientWithName();
		comboBoxClient.setValue("All Client");
		comboBoxClientItems.add("All Client");
		
		if(comboBoxBranch.getValue().equals("All Branch"))
		{
			for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
			{
				comboBoxClientItems.add(clBean.getClientCode()+" | "+clBean.getClientName());
			}
		}
		else
		{
			String[] branchcode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
			
			for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
			{
				if(branchcode[0].equals(clBean.getBranchcode()))
				{
					comboBoxClientItems.add(clBean.getClientCode()+" | "+clBean.getClientName());
				}
			}
		}
		
		comboBoxClient.setItems(comboBoxClientItems);
		System.out.println("Client Count :: "+comboBoxClientItems.size());
		
	}	

// ******************************************************************************
	
	public void loadNetworks() throws SQLException
	{
		comboBoxNetworkItems.clear();
		comboBoxNetworkItems.add("All Network");
		comboBoxNetwork.setValue("All Network");
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkCode()+" | "+bean.getNetworkName());
		}
		comboBoxNetwork.setItems(comboBoxNetworkItems);

		//TextFields.bindAutoCompletion(txtNetwork, list_Networks);
	}		
	

// *************** Method the move Cursor using Entry Key *******************

		@FXML
		public void useEnterAsTabKey(KeyEvent e) throws SQLException {
			Node n = (Node) e.getSource();

			
			
			if (n.getId().equals("dpkFrom")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					dpkTo.requestFocus();
				}
			}

			else if (n.getId().equals("dpkTo")) {
				if (e.getCode().equals(KeyCode.ENTER)) {

					comboBoxBranch.requestFocus();
				}
			}

			else if (n.getId().equals("comboBoxBranch")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					comboBoxClient.requestFocus();
				}
			}
			
			else if (n.getId().equals("comboBoxClient")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					comboBoxNetwork.requestFocus();
				}
			}


			else if (n.getId().equals("comboBoxNetwork")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					btnUpdate.requestFocus();
				}
			}

			else if (n.getId().equals("btnUpdate")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					
					setValidation();
				}
			}

			
		}	
	
// ***************************************************************************************		
		
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if(dpkFrom.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select a From Date");
			alert.showAndWait();
			dpkFrom.requestFocus();
		}
		
		else if(dpkTo.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select a To Date");
			alert.showAndWait();
			dpkTo.requestFocus();
		}

		else
		{
			//testSaveOutward();
			autoUpdateConfirmation();
			//saveOutwardDetails();
		}
	}	
		
// ***************************************************************************************	
		
	public String generateQueryFromFilters()
	{
		String sql="Where";
		String branchsql="";
		String clientsql="";
		String networkSql="";
		
		
		LocalDate from=dpkFrom.getValue();
		LocalDate to=dpkTo.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		//setFilterForTable(stdate, edate);

		String dateRange=" booking_date between '"+ stdate+"' AND '"+edate+"' and mps_type='T'";


		if(!comboBoxBranch.getValue().equals("All Branch"))
		{
			String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
			branchsql=" dailybookingtransaction2branch='"+branchCode[0]+"' and";
		}
		
		if(!comboBoxClient.getValue().equals("All Client"))
		{
			String[] clientCode=comboBoxClient.getValue().replaceAll("\\s+","").split("\\|");
			clientsql=" dailybookingtransaction2client='"+clientCode[0]+"' and";
		}
		
		if(!comboBoxNetwork.getValue().equals("All Network"))
		{
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			networkSql=" dailybookingtransaction2Network='"+networkCode[0]+"' and";
			
		}
		
		sql=sql+""+branchsql+""+clientsql+""+networkSql+""+dateRange;

		System.out.println(" Filter sql: ====>> "+sql);
		return sql;

	}
	
	
// ***************************************************************************************	
	
	public void autoUpdateConfirmation() throws SQLException
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Auto Update Confirmation");
		alert.setHeaderText("This process could take several minutes!!\nDo you want to continue...");
		//alert.setContentText("Do you want to continue...?");

		ButtonType buttonTypeYes = new ButtonType("Yes");
		ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes)
		{

			System.out.println("Start >>> ");
			progressBarForShowDetails();
			//deleteInvoice();
		}
		else if(result.get() == buttonTypeNo)
		{
			System.out.println("Close >>> ");
		}
	}	
	
	public void progressBarForShowDetails() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Please wait while we process your data...");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 16));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);

		taskUpdateStage.setScene(new Scene(updatePane));
		
		taskUpdateStage.initStyle(StageStyle.UNDECORATED);
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				//getDataBeforeReCalculation();
				getAwbNoBetweenRange();
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			public void handle(WorkerStateEvent t) {
				taskUpdateStage.close();
				
				System.out.println("Delivered Table size :: "+tableData_DeliveredAwbno.size());
				System.out.println("Un Delivered Table size :: "+tableData_Un_DeliveredAwbno.size());
				
				if(tableData_DeliveredAwbno.isEmpty()==false)
				{
					pagination_Delivered.setVisible(true);
					btnExportToExcel_Delivered.setVisible(true);
					
					pagination_Delivered.setPageCount((tableData_DeliveredAwbno.size() / rowsPerPage() + 1));
					pagination_Delivered.setCurrentPageIndex(0);
					pagination_Delivered.setPageFactory((Integer pageIndex) -> createPage_DeliveredAwb(pageIndex));
				}
				else
				{
					pagination_Delivered.setVisible(false);
					btnExportToExcel_Delivered.setVisible(false);
				}
				
				
				if(tableData_Un_DeliveredAwbno.isEmpty()==false)
				{
					pagination_UnDelivered.setVisible(true);
					btnExportToExcel_UnDelivered.setVisible(true);
					
					pagination_UnDelivered.setPageCount((tableData_Un_DeliveredAwbno.size() / rowsPerPage() + 1));
					pagination_UnDelivered.setCurrentPageIndex(0);
					pagination_UnDelivered.setPageFactory((Integer pageIndex) -> createPage_UnDeliveredAwb(pageIndex));	
				}
				else
				{
					pagination_UnDelivered.setVisible(false);
					btnExportToExcel_UnDelivered.setVisible(false);
				}
				
				list_awbNo.clear();
				list_awbNoWithNetwork.clear();
				list_DeliveredAwbNo.clear();
				
				System.out.println("Deliver list size :: "+list_DeliveredAwbNo.size());
				
				for(DeliveryStatusBean dsBean: list_DeliveredAwbNo)
				{
				System.out.println("FEDEX >>>>>> Awb :: "+dsBean.getAwbNo()+" | Delivery Status :: "+dsBean.getDeliveryStatus()+" | Received By :: "+dsBean.getReceivedBy()+" | Date :: "+dsBean.getDeliveryDate()+" | Time :: "+dsBean.getDeliveryTime());
				}
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Auto Update");
				alert.setHeaderText(null);
				alert.setContentText("Auto Update Successfully Completed...");
				alert.showAndWait();
				
				dpkFrom.requestFocus();
				
			}
		});

		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();

	}
	

// ***************************************************************************************	
	
	public void showDate() throws SQLException
	{
		System.out.println("Current Date :: "+LocalDate.now());
		System.out.println("Last date :: "+LocalDate.now().minusDays(29));
		//System.out.println("Current Date:: "+currentDate);
		getAwbNoBetweenRange();
	}

// ***************************************************************************************	
	
	public void getAwbNoBetweenRange() throws SQLException
	{
		
		list_awbNoWithNetwork.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		String sql=null;
		 		 
		try
		{	
			sql = "select air_way_bill_number,forwarder_number,dailybookingtransaction2network,dailybookingtransaction2client,dailybookingtransaction2branch from dailybookingtransaction "+generateQueryFromFilters();
			System.out.println(sql);
			st=con.createStatement();
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					DeliveryStatusBean dsBean=new DeliveryStatusBean();
					
					dsBean.setAwbNo(rs.getString("air_way_bill_number"));
					dsBean.setForwarderNumber(rs.getString("forwarder_number"));
					dsBean.setNetwork(rs.getString("dailybookingtransaction2network"));
					dsBean.setClient(rs.getString("dailybookingtransaction2client"));
					dsBean.setBranch(rs.getString("dailybookingtransaction2branch"));
					
					
					if(dsBean.getNetwork()!=null)
					list_awbNoWithNetwork.add(dsBean);
					
				}while(rs.next());
			}
			
			System.out.println("List Size :: "+list_awbNoWithNetwork.size());
			getDeliveryStatus();
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ***************************************************************************************	
	
	public void getDeliveryStatus() throws Exception
	{
		for(DeliveryStatusBean dsBean:list_awbNoWithNetwork)
	    {
	    	if(dsBean.getNetwork().equals("BlueDart") )
	    	{
	    		try
	    		{
					DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
					DocumentBuilder db = dbf.newDocumentBuilder();
					Document doc = db.parse("http://www.bluedart.com/servlet/RoutingServlet?handler=tnt&action="+
							"custawbquery&awb=awb&numbers="+dsBean.getAwbNo()+"&format=xml&verno=1.3&scan=1&loginid=DL269474"+
							"&lickey=2815e6b9abf144407368467144ded2a6&verno=1.3&scan=1");
					//System.out.println("BLUEDARTBLUEDARTBLUEDARTBLUEDARTBLUEDARTBLUEDARTBLUEDARTBLUEDART");
					NodeList Scan = doc.getElementsByTagName("Scan");
					NodeList ScanTime = doc.getElementsByTagName("ScanTime");
					NodeList ScanDate = doc.getElementsByTagName("ScanDate");
					NodeList ScannedLocation = doc.getElementsByTagName("ScannedLocation");
					NodeList Service = doc.getElementsByTagName("Service");
					NodeList Destination = doc.getElementsByTagName("Destination");
					NodeList ReceivedBy = doc.getElementsByTagName("ReceivedBy");
					NodeList status  = doc.getElementsByTagName("Status");
					NodeList statusType  = doc.getElementsByTagName("StatusType");
					NodeList statusDate  = doc.getElementsByTagName("StatusDate");
					NodeList statusTime  = doc.getElementsByTagName("StatusTime");
					
					String deliveryStatus=status.item(0).getFirstChild().getNodeValue();
					
					System.err.println("AWB :: "+dsBean.getAwbNo()+" :: "+deliveryStatus+"< :: "+statusType.item(0).getFirstChild().getNodeValue());
					
					if(deliveryStatus.trim().equals("SHIPMENT DELIVERED"))
					{
						String deliveryData=statusDate.item(0).getFirstChild().getNodeValue().toString();
						String deliveryTime=statusTime.item(0).getFirstChild().getNodeValue().toString();
						String receivedby=ReceivedBy.item(0).getFirstChild().getNodeValue().toString();
					
						System.out.println("BLUEDART >>>>>> Awb :: "+dsBean.getAwbNo().trim()+" | Delivery Status :: "+deliveryStatus.trim()+" | Received By :: "+receivedby.trim()+" | Date :: "+deliveryData.trim()+" | Time :: "+deliveryTime.trim());
						
						DeliveryStatusBean delBean=new DeliveryStatusBean();
						delBean.setAwbNo(dsBean.getAwbNo().trim());
						delBean.setDeliveryStatus("Delivered");
						delBean.setDeliveryDate(deliveryData);
						delBean.setDeliveryTime(deliveryTime);
						delBean.setReceivedBy(receivedby);
						list_DeliveredAwbNo.add(delBean);
						list_awbNo.add(dsBean.getAwbNo().trim());
					}
				} 
	    		catch(Exception e)
	    		{
					e.printStackTrace();
					break;
				}
			}
			else if(dsBean.getNetwork().equals("FedEx"))
			{ 
				TrackWebServiceClient trackservice = new TrackWebServiceClient();
				trackservice.track(dsBean.getAwbNo());
				
				if(fedex_deliveryStatus!=null)
				{
					System.out.println("FEDEX >>>>>> Awb :: "+dsBean.getAwbNo()+" | Delivery Status :: "+fedex_deliveryStatus+" | Received By :: "+fedex_received_by+" | Date :: "+fedex_deliveryDate+" | Time :: "+fedex_deliveryTime);
					DeliveryStatusBean delBean=new DeliveryStatusBean();
					delBean.setAwbNo(dsBean.getAwbNo());
					delBean.setDeliveryStatus(fedex_deliveryStatus);
					delBean.setDeliveryDate(fedex_deliveryDate);
					delBean.setDeliveryTime(fedex_deliveryTime);
					delBean.setReceivedBy(fedex_received_by);
					list_DeliveredAwbNo.add(delBean);
					list_awbNo.add(dsBean.getAwbNo().trim());
				}
				
				
				fedex_deliveryStatus=null;
				fedex_deliveryDate=null;
				fedex_deliveryTime=null;
				fedex_received_by=null;
			}
	    }
		
		
		updateDeliveryStatusToDB();
	}
	
	
// ***************************************************************************************
	
	public void updateDeliveryStatusToDB() throws SQLException
	{
			
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int batchCount=0;
		
		if(list_DeliveredAwbNo.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}

		try 
		{
			int	count=0;
			String query = "update dailybookingtransaction SET delivery_status=?,delivery_date=?,receive_detail=? where air_way_bill_number=?";
				
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);

			for(DeliveryStatusBean dsBean: list_DeliveredAwbNo)
			{
				count++;
				preparedStmt.setString(1, dsBean.getDeliveryStatus().trim());
				preparedStmt.setString(2, dsBean.getDeliveryDate().trim()+", "+dsBean.getDeliveryTime().trim());
				preparedStmt.setString(3, dsBean.getReceivedBy().trim());
				preparedStmt.setString(4, dsBean.getAwbNo());
				
				preparedStmt.addBatch();
				
				batchCount++;
				if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, batchCount);
				}
			}

			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows updated in DailyBookingTransaction Table: "+ result.length);
			con.commit();
			
			loadDeliveredTable();
			loadUnDeliveredTable();

		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
		
	}
	
	
// ***************************************************************************************
	
	public void loadDeliveredTable()
	{
		tableData_DeliveredAwbno.clear();
		int slno=1;
		for(DeliveryStatusBean dsBean:list_DeliveredAwbNo)
		{
			tableData_DeliveredAwbno.add(new DeliveryStatusTableBean(slno, dsBean.getAwbNo(),null, dsBean.getDeliveryDate(), dsBean.getDeliveryTime(), dsBean.getReceivedBy(),null,null,null));
			slno++;
		}
		TableDeliveredAwbNo.setItems(tableData_DeliveredAwbno);
		
	}
	
// ***************************************************************************************

	public void loadUnDeliveredTable()
	{
		tableData_Un_DeliveredAwbno.clear();
		int slno=1;
		for(DeliveryStatusBean dsBean:list_awbNoWithNetwork)
		{
			if(!list_awbNo.contains(dsBean.getAwbNo()))
			{
				tableData_Un_DeliveredAwbno.add(new DeliveryStatusTableBean(slno, dsBean.getAwbNo(),null, null, null, null,dsBean.getClient(),dsBean.getNetwork(),dsBean.getBranch()));
				slno++;
			}
		}
		TableUnDeliveredAwbNo.setItems(tableData_Un_DeliveredAwbno);
		
		
		
		
	}
	
// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 10;
	}

	public GridPane createPage_UnDeliveredAwb(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tableData_Un_DeliveredAwbno.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tableData_Un_DeliveredAwbno.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				TableUnDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_Un_DeliveredAwbno
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				TableUnDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_Un_DeliveredAwbno
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

	public GridPane createPage_DeliveredAwb(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tableData_DeliveredAwbno.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tableData_DeliveredAwbno.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				TableDeliveredAwbNo.setItems(FXCollections.observableArrayList(
						tableData_DeliveredAwbno.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				TableDeliveredAwbNo.setItems(FXCollections.observableArrayList(tableData_DeliveredAwbno
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}	


// ***************************************************************************************
	
	public void reset()
	{
		dpkFrom.getEditor().clear();
		dpkTo.getEditor().clear();
		comboBoxBranch.setValue("All Branch");
		comboBoxClient.setValue("All Client");
		comboBoxNetwork.setValue("All Network");
		
	}
	
	
// ***************************************************************************************
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imgSearchIcon_Delivered.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIcon_UnDelivered.setImage(new Image("/icon/searchicon_blue.png"));
		
		pagination_Delivered.setVisible(false);
		pagination_UnDelivered.setVisible(false);
		btnExportToExcel_Delivered.setVisible(false);
		btnExportToExcel_UnDelivered.setVisible(false);
		
		delvrd_Col_slno.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,Integer>("slno"));
		delvrd_Col_AwbNo.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("awbNo"));
		delvrd_Col_Date.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryDate"));
		delvrd_Col_Time.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("deliveryTime"));
		delvrd_Col_ReceivedBy.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("receivedBy"));
		
		unDelvrd_Col_slno.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,Integer>("slno"));
		unDelvrd_Col_AwbNo.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("awbNo"));
		unDelvrd_Col_Client.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("client"));
		unDelvrd_Col_Branch.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("branch"));
		unDelvrd_Col_Network.setCellValueFactory(new PropertyValueFactory<DeliveryStatusTableBean,String>("Network"));
		
		
		try {
			loadBranch();
			loadClientsViaBranch();
			loadNetworks();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
