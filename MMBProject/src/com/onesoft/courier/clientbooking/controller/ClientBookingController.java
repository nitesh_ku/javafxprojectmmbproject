package com.onesoft.courier.clientbooking.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.clientbooking.bean.ClientBookingBean;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class ClientBookingController implements Initializable {
	
	
	@FXML
	private TextField txt_Consignor_Phone;
	
	@FXML
	private TextField txt_Consignor_Name;

	@FXML
	private TextField txt_Consignor_Company;
	
	@FXML
	private TextField txt_Consignor_Address;
	
	@FXML
	private TextField txt_Consignor_ContactPerson;
	
	@FXML
	private TextField txt_Consignor_Zipcode;
	
	@FXML
	private TextField txt_Consignor_City;
	
// ****************************************************************	
	
	@FXML
	private TextField txt_Consignee_Phone;
	
	@FXML
	private TextField txt_Consignee_Name;
	
	@FXML
	private TextField txt_Consignee_Company;
	
	@FXML
	private TextField txt_Consignee_Address;
	
	@FXML
	private TextField txt_Consignee_ContactPerson;
	
	@FXML
	private TextField txt_Consignee_Zipcode;
	
	@FXML
	private TextField txt_Consignee_City;
	
	@FXML
	private TextField txt_Consignee_Email;
	
	@FXML
	private TextField txt_Consignee_Remarks;
	
// ****************************************************************

	@FXML
	private DatePicker dpkShippingDate;
	
	@FXML
	private TextField txtService;

	@FXML
	private TextField txtPackaging;

	@FXML
	private TextField txtPCS;

	@FXML
	private TextField txtToatalWeight;
		
	
// ****************************************************************

	@FXML
	private ComboBox<String> combBox_Consignor_State;

	@FXML
	private ComboBox<String> combBox_Consignee_State;

	@FXML
	private ComboBox<String> combBox_Consignor_Country;

	@FXML
	private ComboBox<String> combBox_Consignee_Country;
	
// ****************************************************************

	@FXML
	private Button btnSubmit;

	@FXML
	private Button btnReset;
	

// ****************************************************************

	@FXML
	public void showConsignorDetails() throws SQLException 
	{
		if (!txt_Consignor_Phone.getText().equals("")) 
		{

			System.out.println("From Show Consignor Details...");

			DBconnection dbcon = new DBconnection();
			Connection con = dbcon.ConnectDB();

			Statement st = null;
			ResultSet rs = null;

			ClientBookingBean clBean = new ClientBookingBean();
			PreparedStatement preparedStmt = null;

			clBean.setConsigNOR_Phone(txt_Consignor_Phone.getText());
			clBean.setConsignor_Consignee_Type("cnor");

			try 
			{
				String sql = "select ccd.name,ccd.address,ccd.contact_person,ccd.zipcode,ccd.city,ccd.state,ccd.country,ccd.email,ad.address_line from consignor_consignee_details as ccd , address as ad where cast(ccd.address as Integer)=ad.addressid and phone_no=? and type=?";

				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1, clBean.getConsigNOR_Phone());
				preparedStmt.setString(2, clBean.getConsignor_Consignee_Type());
				System.out.println("Sql: " + preparedStmt);
				rs = preparedStmt.executeQuery();

				if (!rs.next()) 
				{
					txt_Consignor_Name.requestFocus();
				} 
				else 
				{
					do 
					{
						clBean.setConsigNOR_Name(rs.getString("name"));
						clBean.setConsigNOR_Address(rs.getString("address_line"));
						clBean.setConsigNOR_ContactPerson(rs.getString("contact_person"));
						clBean.setConsigNOR_Zipcode(rs.getInt("zipcode"));
						clBean.setConsigNOR_City(rs.getString("city"));
						clBean.setConsigNOR_State(rs.getString("state"));
						clBean.setConsigNOR_Country(rs.getString("country"));
						
					}
					while (rs.next());

					txt_Consignor_Name.setText(clBean.getConsigNOR_Name());
					txt_Consignor_Address.setText(clBean.getConsigNOR_Address());
					txt_Consignor_ContactPerson.setText(clBean.getConsigNOR_ContactPerson());
					txt_Consignor_Zipcode.setText(String.valueOf(clBean.getConsigNOR_Zipcode()));
					txt_Consignor_City.setText(clBean.getConsigNOR_City());
					combBox_Consignor_State.setValue(clBean.getConsigNOR_State());
					combBox_Consignor_Country.setValue(clBean.getConsigNOR_Country());
					
				}

			} 
			catch (Exception e) 
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally 
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
	}	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
