package com.onesoft.courier.clientbooking.bean;

public class ClientBookingBean {
	
	private int slno;
	private String awbNo;
	private String bookingDate;
	private String origin;
	private String destination;
	private String network;
	private String service;
	
	private String consigNOR_Phone;
	private String consigNOR_Name;
	private String consigNOR_Address;
	private String consigNOR_ContactPerson;
	private int consigNOR_Zipcode;
	private String consigNOR_City;
	private String consigNOR_State;
	private String consigNOR_Country;
	private String consigNOR_Email;
	
	private String consigNEE_Phone;
	private String consigNEE_Name;
	private String consigNEE_Address;
	private String consigNEE_ContactPerson;
	private int consigNEE_Zipcode;
	private String consigNEE_City;
	private String consigNEE_State;
	private String consigNEE_Country;
	private String consigNEE_Email;
	private String consigNEE_Remarks;
	
	private String type;
	private int pcs;
	private double actualWeight;
	private double volumeWeight;
	private double billWeight;
	private String insurance;
	private String invoiceNo;
	private double InvoiceAmount;
	private double InsuranceAmount;
	private double InsuranceRate;
	private String consignor_Consignee_Type;
	
	
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}

	public String getConsigNOR_Name() {
		return consigNOR_Name;
	}
	public void setConsigNOR_Name(String consigNOR_Name) {
		this.consigNOR_Name = consigNOR_Name;
	}
	public String getConsigNOR_Address() {
		return consigNOR_Address;
	}
	public void setConsigNOR_Address(String consigNOR_Address) {
		this.consigNOR_Address = consigNOR_Address;
	}
	public String getConsigNOR_ContactPerson() {
		return consigNOR_ContactPerson;
	}
	public void setConsigNOR_ContactPerson(String consigNOR_ContactPerson) {
		this.consigNOR_ContactPerson = consigNOR_ContactPerson;
	}
	public int getConsigNOR_Zipcode() {
		return consigNOR_Zipcode;
	}
	public void setConsigNOR_Zipcode(int consigNOR_Zipcode) {
		this.consigNOR_Zipcode = consigNOR_Zipcode;
	}
	public String getConsigNOR_City() {
		return consigNOR_City;
	}
	public void setConsigNOR_City(String consigNOR_City) {
		this.consigNOR_City = consigNOR_City;
	}
	public String getConsigNOR_State() {
		return consigNOR_State;
	}
	public void setConsigNOR_State(String consigNOR_State) {
		this.consigNOR_State = consigNOR_State;
	}
	public String getConsigNOR_Country() {
		return consigNOR_Country;
	}
	public void setConsigNOR_Country(String consigNOR_Country) {
		this.consigNOR_Country = consigNOR_Country;
	}
	public String getConsigNOR_Email() {
		return consigNOR_Email;
	}
	public void setConsigNOR_Email(String consigNOR_Email) {
		this.consigNOR_Email = consigNOR_Email;
	}

	public String getConsigNEE_Name() {
		return consigNEE_Name;
	}
	public void setConsigNEE_Name(String consigNEE_Name) {
		this.consigNEE_Name = consigNEE_Name;
	}
	public String getConsigNEE_Address() {
		return consigNEE_Address;
	}
	public void setConsigNEE_Address(String consigNEE_Address) {
		this.consigNEE_Address = consigNEE_Address;
	}
	public String getConsigNEE_ContactPerson() {
		return consigNEE_ContactPerson;
	}
	public void setConsigNEE_ContactPerson(String consigNEE_ContactPerson) {
		this.consigNEE_ContactPerson = consigNEE_ContactPerson;
	}
	public int getConsigNEE_Zipcode() {
		return consigNEE_Zipcode;
	}
	public void setConsigNEE_Zipcode(int consigNEE_Zipcode) {
		this.consigNEE_Zipcode = consigNEE_Zipcode;
	}
	public String getConsigNEE_City() {
		return consigNEE_City;
	}
	public void setConsigNEE_City(String consigNEE_City) {
		this.consigNEE_City = consigNEE_City;
	}
	public String getConsigNEE_State() {
		return consigNEE_State;
	}
	public void setConsigNEE_State(String consigNEE_State) {
		this.consigNEE_State = consigNEE_State;
	}
	public String getConsigNEE_Country() {
		return consigNEE_Country;
	}
	public void setConsigNEE_Country(String consigNEE_Country) {
		this.consigNEE_Country = consigNEE_Country;
	}
	public String getConsigNEE_Email() {
		return consigNEE_Email;
	}
	public void setConsigNEE_Email(String consigNEE_Email) {
		this.consigNEE_Email = consigNEE_Email;
	}
	public String getConsigNEE_Remarks() {
		return consigNEE_Remarks;
	}
	public void setConsigNEE_Remarks(String consigNEE_Remarks) {
		this.consigNEE_Remarks = consigNEE_Remarks;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}
	public double getVolumeWeight() {
		return volumeWeight;
	}
	public void setVolumeWeight(double volumeWeight) {
		this.volumeWeight = volumeWeight;
	}
	public double getBillWeight() {
		return billWeight;
	}
	public void setBillWeight(double billWeight) {
		this.billWeight = billWeight;
	}
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public double getInvoiceAmount() {
		return InvoiceAmount;
	}
	public void setInvoiceAmount(double invoiceAmount) {
		InvoiceAmount = invoiceAmount;
	}
	public double getInsuranceAmount() {
		return InsuranceAmount;
	}
	public void setInsuranceAmount(double insuranceAmount) {
		InsuranceAmount = insuranceAmount;
	}
	public String getConsignor_Consignee_Type() {
		return consignor_Consignee_Type;
	}
	public void setConsignor_Consignee_Type(String consignor_Consignee_Type) {
		this.consignor_Consignee_Type = consignor_Consignee_Type;
	}
	public String getConsigNOR_Phone() {
		return consigNOR_Phone;
	}
	public void setConsigNOR_Phone(String consigNOR_Phone) {
		this.consigNOR_Phone = consigNOR_Phone;
	}
	public String getConsigNEE_Phone() {
		return consigNEE_Phone;
	}
	public void setConsigNEE_Phone(String consigNEE_Phone) {
		this.consigNEE_Phone = consigNEE_Phone;
	}
	public double getInsuranceRate() {
		return InsuranceRate;
	}
	public void setInsuranceRate(double insuranceRate) {
		InsuranceRate = insuranceRate;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	
	

}
