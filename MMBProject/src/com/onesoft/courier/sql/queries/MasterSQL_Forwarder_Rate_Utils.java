package com.onesoft.courier.sql.queries;

public class MasterSQL_Forwarder_Rate_Utils {


	
	public static String FORWARDER_SLAB_TYPE_BASIC="Basic";
	public static String FORWARDER_SLAB_TYPE_ADDITIONAL="Additional";
	public static String FORWARDER_SLAB_TYPE_KG="Kgs";
	
	public static String FORWARDER_MAPPING_TYPE_ZONE="zone";
	public static String FORWARDER_MAPPING_TYPE_CITY="city";
	
	public static String FORWARDER_APPLICATION_ID_FORALL="MMB";
	public static String FORWARDER_LAST_MODIFIED_USER_FORALL="1";
	
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_FORWARDERRATE_DETAILS="insert into forwarderratemaster(forwarderratemaster2vendorcode,"
									+ "forwarderratemaster2groupcode,mappingtype,sourcezone,targetzone,slabtype,weightupto,slab,dox,nondox,"
									+ "create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
											
	public static String CHECK_BASIC_RATE_ENTRY_IN_DB_SQL_FORWARDERRATE="select forwarderratemaster2vendorcode,forwarderratemaster2groupcode,"
									+ "mappingtype,sourcezone,targetzone,slabtype from forwarderratemaster where forwarderratemaster2vendorcode=? "
									+ "and forwarderratemaster2groupcode=? and sourcezone=? and targetzone=? and slabtype=? limit 1";
	
	/*public static String DATA_RETRIEVE_SQL_ZONE="select code,name from zonetype where code=?";
	
	public static String UPDATE_SQL_ZONE="update zonetype SET name=?, lastmodifieddate=CURRENT_TIMESTAMP where code=?";*/
	
	public static String LOADTABLE_SQL_FORWARDERRATE="select forwarderratemasterid,forwarderratemaster2vendorcode,slabtype,weightupto,slab,"
						+ "dox,nondox from forwarderratemaster where forwarderratemaster2vendorcode=? order by forwarderratemasterid DESC";
	
	
	
}
