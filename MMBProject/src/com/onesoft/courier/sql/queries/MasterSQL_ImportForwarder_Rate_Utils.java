package com.onesoft.courier.sql.queries;

public class MasterSQL_ImportForwarder_Rate_Utils {


	
	public static String SLAB_TYPE_BASIC="Basic";
	public static String SLAB_TYPE_ADDITIONAL="Additional";
	public static String SLAB_TYPE_KG="Kgs";
	
	public static String MAPPING_TYPE_ZONE="zone";
	public static String MAPPING_TYPE_CITY="city";
	
	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_FORWARDERRATE_DETAILS="insert into forwarderratemaster(clientratemaster2client,clientratemaster2vendorcode,"
									+ "clientratemaster2servicecode,mappingtype,sourcezone,targetzone,slabtype,weightupto,slab,dox,nondox,"
									+ "create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";

	
	public static String INSERT_SQL_FORWARDERRATE_ZONE_DETAILS="insert into forwarderrate_zone(forwarder,network,service,zone_code,"
						+ "da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,dn,do1,dp,dq,dr,ds,dt,rate_type) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static String INSERT_SQL_FORWARDERRATE_ZONE_WITH_ADD_KG="insert into forwarderrate_zone(forwarder,network,service,zone_code,"
						+ "da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,dn,do1,dp,dq,dr,ds,dt,rate_type) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static String UPDATE_SQL_FORWARDERRATE_ZONE_DETAILS="update forwarderrate_zone set da=?,db=?,dc=?,dd=?,de=?,df=?,"
							+ "dg=?,dh=?,di=?,dj=?,dk=?,dl=?,dm=?,dn=?,do1=?,dp=?,dq=?,dr=?,ds=?,dt=? where  forwarder=? and network=? and service=? and zone_code=?";

	public static String UPDATE_SQL_FORWARDERRATE_ZONE_WITH_ADD_KG="update forwarderrate_zone set da=?,db=?,dc=?,dd=?,de=?,df=?,"
							+ "dg=?,dh=?,di=?,dj=?,dk=?,dl=?,dm=?,dn=?,do1=?,dp=?,dq=?,dr=?,ds=?,dt=? where  forwarder=? and network=? and service=? and rate_type=?";
		
	public static String INSERT_SQL_FORWARDERRATE_SLAB_DETAILS="insert into forwarderrate_slab(forwarderrate2forwarder,forwarderrate2network,forwarderrate2service,basic_range,"
						+ "add_slab,add_range,kg,create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	//public static String INSERT_SQL_CLIENTRATE_SLAB_DETAILS="insert into clientrate_slab(clientrate2client,clientrate2network,clientrate2service,create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	//public static String UPDATE_SQL_CLIENTRATE_SLAB_DETAILS="update clientrate_slab set basic_range=?,add_slab=?,add_range=?,kg=?,"
						//		+ "lastmodifieddate=CURRENT_TIMESTAMP where clientrate2client=? and clientrate2network=? and clientrate2service=?";
	
	
	public static String UPDATE_SQL_FORWARDERRATE_SLAB_DETAILS="update forwarderrate_slab set basic_range=?,add_slab=?,add_range=?,kg=?,"
			+ "lastmodifieddate=CURRENT_TIMESTAMP where forwarderrate2forwarder=? and forwarderrate2network=? and forwarderrate2service=?";
	
	/*public static String DATA_RETRIEVE_SQL_ZONE="select code,name from zonetype where code=?";
	
	public static String UPDATE_SQL_ZONE="update zonetype SET name=?, lastmodifieddate=CURRENT_TIMESTAMP where code=?";*/
	
	public static String LOADTABLE_SQL_FORWARDERRATE="select zone_code,da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm from "
						+ "forwarderrate_zone where forwarder=? and network=? and service=?";
	
	
	
	public static String LOADData_SQL_FORWARDERSLAB="select forwarderrate2forwarder,forwarderrate2network,forwarderrate2service,basic_range,add_slab,add_range,kg from "
			+ "forwarderrate_slab where forwarderrate2forwarder=? and forwarderrate2network=? and forwarderrate2service=?";
	
	
	public static String CHECK_SLAB_DETAILS_SQL_FORWARDERRATE="select forwarderrate2forwarder,forwarderrate2network,forwarderrate2service from forwarderrate_slab where forwarderrate2forwarder=? and forwarderrate2network=? and forwarderrate2service=?";
	
	public static String CHECK_ZONE_DETAILS_SQL_FORWARDERRATE="select forwarder,network,service,zone_code,rate_type from forwarderrate_zone where forwarder=? and network=? and service=? and zone_code=?";
	
	public static String CHECK_ZONE_DETAILS_SQL_FORWARDERRATE_WITH_ADD_KG="select forwarder,network,service,zone_code,rate_type from forwarderrate_zone where forwarder=? and network=? and service=? and rate_type=?";
	
	public static String DELETE_ADD_KG_FROM_RATEZONE="delete from forwarderrate_zone where forwarder=? and network=? and service=? and rate_type=?";
	
}
