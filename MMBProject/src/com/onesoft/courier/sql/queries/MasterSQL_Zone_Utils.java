package com.onesoft.courier.sql.queries;

public class MasterSQL_Zone_Utils {


	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_ZONE="insert into zonetype(code,name,create_date,lastmodifieddate,applicationid,last_modified_user)"
											+ " values(?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	public static String DATA_RETRIEVE_SQL_ZONE="select code,name from zonetype where code=?";
	
	public static String UPDATE_SQL_ZONE="update zonetype SET name=?, lastmodifieddate=CURRENT_TIMESTAMP where code=?";
	
	public static String LOADTABLE_SQL_ZONE="select code,name from zonetype order by zonetypeid DESC";
	
	
// ============= SQL queries for Zone Mapping Tab =========================================
	
	public static String LOAD_STATES_SQL_FOR_MAPPING="select sm.state_code,sm.state_name from  zonedetail as zd, "
								+ "state_master as sm where zd.zone2state=sm.state_code and zd.zonedetail2zonetype=?";
	
	public static String LOAD_CITIES_FOR_LISTVIEW="select city_code, city_name, state_code from city_master where state_code=?";
	
	public static String INSERT_SQL_ZONE_MAPPING="insert into zonedetail(zone2city,zone2state,zonedetail2zonetype,"
								+ "behaviour,create_date,lastmodifieddate,applicationid,last_modified_user) "
								+ "values(?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	public static String IS_CITY_EXIST_AGAINST_ZONE_IN_DB="select zone2city,zonedetail2zonetype from zonedetail where zonedetail2zonetype=? and zone2city=?";
	
	
	public static String UPDATE_SQL_ZONE_MAPPING_VIA_CITYNAME="update pincode_master set zone_code=?,service_code=? where city_name=?";

	public static String UPDATE_SQL_ZONE_MAPPING_VIA_STATECODE="update pincode_master set zone_code=?,service_code=? where state_code=?";

	public static String CHECK_EXISTING_SERVICE_MAPPING_WITH_CITYNAME="select service_code from pincode_master where zone_code=? and city_name=? limit 1";
	
	public static String CHECK_EXISTING_SERVICE_MAPPING_WITH_STATECODE="select service_code from pincode_master where zone_code=? and state_code=? limit 1";
	
// ============= SQL queries for Search Tab =========================================
	
	public static String DATA_RETRIEVE_SQL_STATES_VIA_ZONE="select pm.state_code as pm_state_code,sm.state_name as sm_state_name from state_master as sm, pincode_master as pm "
								+ "where sm.state_code=pm.state_code and pm.zone_code=? group by pm.state_Code,sm.state_name";
	
	public static String DATA_RETRIEVE_SQL_CITIES_VIA_ZONE_AND_STATES="select city_name,state_code from pincode_master "
								+ "where zone_code=? and state_code=? group by city_name,state_code";
	
	public static String LOADTABLE_SQL_UNMAPPED_ZONE="select pincode,city_name,state_code,zone_code from pincode_master where zone_code IS NULL order by pincode";
	
	public static String DATA_RETRIEVE_SQL_PINCODE_AND_SERVICE_VIA_CITY="select pincode,service_code from pincode_master where city_name=?";
	
}
