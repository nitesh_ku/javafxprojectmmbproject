package com.onesoft.courier.sql.queries;

public class MasterSQL_ToPay_Utils {
	
	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	
	public static String ACTIVE="Active";
	public static String IN_ACTIVE="In-Active";
	
	public static String CFT="CFT";
	public static String DIVISION="Division";
	
	public static String INSERT_SQL_SAVE_TOPAY="insert into clientmaster(code,name,clientmaster2branch,address,"
			+ "gstin_number,emailid,pincode,city,create_date,lastmodifieddate,applicationid,last_modified_user,client_type,phone_no)"
			+ "values(?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?)";
	
	public static String GET_TOPAY_DATA_SQL="select name,clientmaster2branch,address,gstin_number,emailid,pincode,city,client_type,phone_no "
			+ "from clientmaster where client_type='ToPay'";
	
	public static String UPDATE_TOPAY_DATA_SQL="update clientmaster set clientmaster2branch=?,phone_no=?,address=?,pincode=?,city=?,"
			+ "gstin_number=?,emailid=?,lastmodifieddate=CURRENT_TIMESTAMP where name=? and client_type=?";
	
	
	public static String DATA_RETRIEVE_SQL_CLIENT_DETAILS="select code,name,clientmaster2branch,contactperson,fuelrate,gst,"
			+ "reverse,fuelonvas,carrierinsurancefixed,carrierinsurancepercentage,ownerinsurancefixed,"
			+ "ownerinsurancepercentage,codchargesfixed,codchargespercentage,fodcharges,gstin_number,"
			+ "password,dim_air,dim_surface,air_dim_formula,surface_dim_formula,discountrate,minimumbillingamount,status,"
			+ "autoemail,emailid,client_type,phone_no,city,pincode,address from clientmaster where code=?";
	
	
	public static String UPDATE_SQL_SAVE_CLIENT="update clientmaster SET name=?,clientmaster2branch=?,contactperson=?,address=?,"
			+ "fuelrate=?,gst=?,reverse=?,fuelonvas=?,carrierinsurancefixed=?,carrierinsurancepercentage=?,ownerinsurancefixed=?,"
			+ "ownerinsurancepercentage=?,codchargesfixed=?,codchargespercentage=?,fodcharges=?,gstin_number=?,password=?,dim_air=?,"
			+ "dim_surface=?,air_dim_formula=?,discountrate=?,minimumbillingamount=?,status=?,autoemail=?,emailid=?,"
			+ "lastmodifieddate=CURRENT_TIMESTAMP,client_type=?,city=?, pincode=?,surface_dim_formula=?,phone_no=? where code=?";

	public static String LOAD_TABLE_SQL_CLIENT_DETAILS="select cm.code,cm.name,cm.clientmaster2branch,cm.contactperson,cm.fuelrate,cm.status,"
			+ "add.address_line,add.city from clientmaster as cm, address as add where cm.clientmaster2address=add.addressid order by clientmasterid DESC";
	
	

}
