package com.onesoft.courier.sql.queries;

public class MasterSQL_UploadPincode_Excel_Utils {


	
	public static String FORWARDER_SLAB_TYPE_BASIC="Basic";
	public static String FORWARDER_SLAB_TYPE_ADDITIONAL="Additional";
	public static String FORWARDER_SLAB_TYPE_KG="Kgs";
	
	public static String FORWARDER_MAPPING_TYPE_ZONE="zone";
	public static String FORWARDER_MAPPING_TYPE_CITY="city";
	
	public static String FORWARDER_APPLICATION_ID_FORALL="MMB";
	public static String FORWARDER_LAST_MODIFIED_USER_FORALL="1";
	
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_UPLOAD_PINCODE="insert into pincode_master (pincode,city_name,state_code,oda_opa,"
								+ "international_service,domestic_service,cod_serviceble) values(?,?,?,?,?,?,?)";
											
	public static String GET_PINCODE_FROM_DB="select pincode,zone_air,zone_surface from pincode_master";
	
	public static String CHECK_PINCODE_FOR_INSERT="select pincode from pincode_master where pincode=?";
	
	
	public static String UPDATE_SQL_PINCODE_DATA="update pincode_master set city_name=?,state_code=?,oda_opa=?,"
								+ "international_service=?,domestic_service=?,cod_serviceble=? where pincode=?";
	
	public static String UPDATE_SQL_MAP_ZONE_WITH_PINCODE="update pincode_master set zone_air=?,zone_surface=?,service_code=? where pincode=?";

	public static String LOAD_TABLE_PINCODE_DATA="select pincode,city_name,state_code,oda_opa,international_service,domestic_service,cod_serviceble,zone_code,"
			+ "service_code,zone_air,zone_surface from pincode_master order by pincode limit 200";

	/*public static String LOAD_TABLE_PINCODE_DATA_SHOW_ALL="select pincode,city_name,state_code,oda_opa,international_service,domestic_service,cod_serviceble,zone_code,"
			+ "service_code,zone_air,zone_surface,state_gstin_code from pincode_master order by pincode";*/
	
	public static String LOAD_TABLE_PINCODE_DATA_SHOW_ALL="select * from pincode_master order by pincode";
	
	
	
	
}
