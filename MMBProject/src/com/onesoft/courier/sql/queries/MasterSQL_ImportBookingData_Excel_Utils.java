package com.onesoft.courier.sql.queries;

public class MasterSQL_ImportBookingData_Excel_Utils {


	
	public static String FORWARDER_SLAB_TYPE_BASIC="Basic";
	public static String FORWARDER_SLAB_TYPE_ADDITIONAL="Additional";
	public static String FORWARDER_SLAB_TYPE_KG="Kgs";
	
	public static String FORWARDER_MAPPING_TYPE_ZONE="zone";
	public static String FORWARDER_MAPPING_TYPE_CITY="city";
	
	public static String FORWARDER_APPLICATION_ID_FORALL="MMB";
	public static String FORWARDER_LAST_MODIFIED_USER_FORALL="1";
	
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_BOOKING_DATA="INSERT INTO dailybookingtransaction(air_way_bill_number, master_awbno,forwarder_number, mps, mps_type,"
								+ "booking_date, dailybookingtransaction2client, origin, dailybookingtransaction2city, zipcode,"
								+ "dailybookingtransaction2network, dailybookingtransaction2service, dox_nondox, packets, dimension, actual_weight,"
								+ "billing_weight, insurance_value, travel_status, create_date, lastmodifieddate, applicationid, last_modified_user,"
								+ "forwarder_account,dailybookingtransaction2branch,spot_rate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?,?)";
											
	public static String INSERT_SQL_CONSIGNEE_CONSIGNOR_DETAILS="insert into consignor_consignee_details(awb_no,cnee_name,indate,create_date,lastmodifieddate)"
								+ " values(?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
	
	public static String CHECK_AWBNO_FOR_INSERT="select master_awbno from dailybookingtransaction where master_awbno=?";
	
	public static String CHECK_AWBNO_FOR_INSERT_IN_CONSIGNORDETAIL="select awb_no from consignor_consignee_details where awb_no=?";
	
	
	public static String UPDATE_SQL_BOOKING_DATA="update dailybookingtransaction set forwarder_number=?,"
								+ "booking_date=?, dailybookingtransaction2client=?, origin=?, dailybookingtransaction2city=?, zipcode=?,"
								+ "dailybookingtransaction2network=?, dailybookingtransaction2service=?, dox_nondox=?, packets=?, dimension=?, actual_weight=?,"
								+ "billing_weight=?, insurance_value=?, lastmodifieddate=CURRENT_DATE,"
								+ "forwarder_account=?,dailybookingtransaction2branch=? where master_awbno=? and invoice_number IS NULL";
	
	
	public static String UPDATE_SQL_CONSIGNEE_CONSIGNOR_DETAILS= "update consignor_consignee_details set cnee_name=?,lastmodifieddate=CURRENT_TIMESTAMP where awb_no=?";
	
	
	public static String LOAD_TABLE_SAVED_BOOKING_DATA="select master_awbno,dailybookingtransaction2service,dailybookingtransaction2network,forwarder_number,booking_date,forwarder_account "
					+ "from dailybookingtransaction order by dailybookingtransactionid";

	
	
	
	
}
