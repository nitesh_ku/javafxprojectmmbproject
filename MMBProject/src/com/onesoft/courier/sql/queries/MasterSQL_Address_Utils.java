package com.onesoft.courier.sql.queries;

public class MasterSQL_Address_Utils {
	
	public static String UPDATE_SQL_EMP_ADDRESS_DETAILS="update address set phone=?, address_line=?, city=?, state=?,"
			+ "country=?,lastmodifieddate=CURRENT_TIMESTAMP where addressid=?";

	public static String INSERT_SQL_ADDRESS_DETAILS="insert into address(addressid,phone,address_line,city,state,country,create_date,lastmodifieddate) "
			+ "values(?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";

	public static String GET_SQL_ADDRESS_CODE="select addressid from address order by addressid DESC limit 1";


}
