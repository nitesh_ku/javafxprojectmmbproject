package com.onesoft.courier.sql.queries;

public class MasterSQL_DataEntry_Utils {
	
	public static String CLIENT_VAS_APPLICATION_ID_FORALL="MMB";
	public static String CLIENT_VAS_LAST_MODIFIED_USER_FORALL="1";
	
	public static String CLIENT_VAS_INSERT_SQL_SAVE_CLIENT="";
	
	
	public static String CLIENTVAS_IN_DATAENTRY_RETRIEVE_SQL="select cod_min,cod_per,insur_own_min,insur_own_per,insur_carr_min,"
						+ "insur_carr_per,oss_min,oss_perkg,docket_chrgs,oda_min,oda_perkg,opa_min,opa_perkg,adv_fee_min,adv_fee_per,"
						+ "delivery_on_invoice,fuel_rate,hfd_min,hfd_per_pcs_kg,hfd_type,hfd_free_floor,topay,addres_correction_chrgs,"
						+ "hold_at_office_min,hold_at_office_perkg,green_tax,reverse_pickup_min,reverse_pickup_slab,tdd_min,tdd_add,"
						+ "tdd_slab,critical_min,critical_add,critical_slab,oss_kg_limit,oss_cm_limit,fov,fuel_excluding_fov from client_vas where client_cd=? and service_cd=?";
	
	
	public static String CLIENT_VAS_UPDATE_SQL="";

	public static String LOAD_TABLE_SQL_CLIENT_DETAILS="";
	
	

}
