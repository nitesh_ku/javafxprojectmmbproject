package com.onesoft.courier.sql.queries;

public class MasterSQL_Employee_Registration_Utils {


	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	public static String ACTIVE="Active";
	public static String IN_ACTIVE="In-Active";
	
			
	
	public static String INSERT_SQL_EMPLOYEE_REGISTRATION_DETAILS="insert into empdetail(code, full_name,emp_phone,emp_dob,emp_doj,email,"
													+ "empdetail2address,empdetail2designation,status,user_type,"
													+ "pwd,create_date,lastmodifieddate,applicationid,last_modified_user) "
													+ "values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	
	
		
	public static String DATA_RETRIEVE_EMPLOYEE_REGISTRATION_DETAILS="select emp.code as empcode,emp.full_name as empname,emp.emp_phone as empphone,emp.emp_dob as empdob,emp.emp_doj as empdoj,"
													+ "emp.email as empemail,emp.empdetail2designation as empdesignation,emp.status as empstatus,emp.user_type as empusertype,emp.pwd as emppassword,add.address_line as empaddress,"
													+ "add.addressid empaddresscode,add.city as empcity,add.state as empstate,add.country as empcountry from empdetail as emp, address as add "
													+ "where emp.empdetail2address=add.addressid and emp.code=?";
	
	public static String UPDATE_SQL_EMPLOYEE_REGISTRATION_DETAILS="update empdetail set full_name=?, emp_phone=?, emp_dob=?, emp_doj=? ,email=? ,"
													+ "empdetail2designation=?, status=?, user_type=?,"
													+ "pwd=? ,lastmodifieddate= CURRENT_TIMESTAMP where code=?";
	
	
	public static String LOAD_TABLE_EMPLOYEE_REGISTRATION_DETAILS="select emp.code as empcode,emp.full_name as empname,emp.emp_phone as empphone,emp.emp_dob as empdob,emp.emp_doj as empdoj,"
													+ "emp.email as empemail,emp.empdetail2designation as empdesignation,emp.status as empstatus,add.address_line as empaddress,"
													+ "add.addressid empaddresscode,add.city as empcity from empdetail as emp, address as add "
													+ "where emp.empdetail2address=add.addressid order by emp.empdetailid DESC";
	
	
	
	
}
