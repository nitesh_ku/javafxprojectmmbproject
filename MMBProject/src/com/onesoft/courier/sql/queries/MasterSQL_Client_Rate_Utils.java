package com.onesoft.courier.sql.queries;

public class MasterSQL_Client_Rate_Utils {


	
	public static String SLAB_TYPE_BASIC="Basic";
	public static String SLAB_TYPE_ADDITIONAL="Additional";
	public static String SLAB_TYPE_KG="Kgs";
	
	public static String MAPPING_TYPE_ZONE="zone";
	public static String MAPPING_TYPE_CITY="city";
	
	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	
	
	
// ============= SQL queries for Zone Tab =========================================	

	
	public static String INSERT_SQL_CLIENTRATE_DETAILS="insert into clientratemaster(clientratemaster2client,clientratemaster2vendorcode,"
									+ "clientratemaster2servicecode,mappingtype,sourcezone,targetzone,slabtype,weightupto,slab,dox,nondox,"
									+ "create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";

	
	public static String INSERT_SQL_CLIENTRATE_ZONE_DETAILS="insert into clientrate_zone(client,network,service,zone_code,"
						+ "da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,dn,do1,dp,dq,dr,ds,dt,rate_type,additional_reverse,kg_reverse) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static String INSERT_SQL_CLIENTRATE_ZONE_WITH_ADD_KG="insert into clientrate_zone(client,network,service,zone_code,"
						+ "da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,dn,do1,dp,dq,dr,ds,dt,rate_type,additional_reverse,kg_reverse) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static String UPDATE_SQL_CLIENTRATE_ZONE_DETAILS="update clientrate_zone set da=?,db=?,dc=?,dd=?,de=?,df=?,"
							+ "dg=?,dh=?,di=?,dj=?,dk=?,dl=?,dm=?,dn=?,do1=?,dp=?,dq=?,dr=?,ds=?,dt=?,additional_reverse=?,kg_reverse=? where  client=? and network=? and service=? and zone_code=?";

	public static String UPDATE_SQL_CLIENTRATE_ZONE_WITH_ADD_KG="update clientrate_zone set da=?,db=?,dc=?,dd=?,de=?,df=?,"
							+ "dg=?,dh=?,di=?,dj=?,dk=?,dl=?,dm=?,dn=?,do1=?,dp=?,dq=?,dr=?,ds=?,dt=?,additional_reverse=?,kg_reverse=? where  client=? and network=? and service=? and rate_type=?";
		
	public static String INSERT_SQL_CLIENTRATE_SLAB_DETAILS="insert into clientrate_slab(clientrate2client,clientrate2network,clientrate2service,basic_range,"
						+ "add_slab,add_range,kg,create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	//public static String INSERT_SQL_CLIENTRATE_SLAB_DETAILS="insert into clientrate_slab(clientrate2client,clientrate2network,clientrate2service,create_date,lastmodifieddate,applicationid,last_modified_user) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	//public static String UPDATE_SQL_CLIENTRATE_SLAB_DETAILS="update clientrate_slab set basic_range=?,add_slab=?,add_range=?,kg=?,"
						//		+ "lastmodifieddate=CURRENT_TIMESTAMP where clientrate2client=? and clientrate2network=? and clientrate2service=?";
	
	
	public static String UPDATE_SQL_CLIENTRATE_SLAB_DETAILS="update clientrate_slab set basic_range=?,add_slab=?,add_range=?,kg=?,"
			+ "lastmodifieddate=CURRENT_TIMESTAMP where clientrate2client=? and clientrate2network=? and clientrate2service=?";
	
	/*public static String DATA_RETRIEVE_SQL_ZONE="select code,name from zonetype where code=?";
	
	public static String UPDATE_SQL_ZONE="update zonetype SET name=?, lastmodifieddate=CURRENT_TIMESTAMP where code=?";*/
	
	public static String LOADTABLE_SQL_CLIENTRATE="select zone_code,da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,dn,do1,dp,"
			+ "dq,dr,ds,dt,additional_reverse,kg_reverse from clientrate_zone where client=? and network=? and service=?";
	
	
	
	public static String LOADData_SQL_CLIENTSLAB="select clientrate2client,clientrate2network,clientrate2service,basic_range,add_slab,add_range,kg from "
			+ "clientrate_slab where clientrate2client=? and clientrate2network=? and clientrate2service=?";
	
	
	public static String CHECK_SLAB_DETAILS_SQL_CLIENTRATE="select clientrate2client,clientrate2network,clientrate2service from clientrate_slab where clientrate2client=? and clientrate2network=? and clientrate2service=?";
	
	public static String CHECK_ZONE_DETAILS_SQL_CLIENTRATE="select client,network,service,zone_code,rate_type from clientrate_zone where client=? and network=? and service=? and zone_code=?";
	
	public static String CHECK_ZONE_DETAILS_SQL_CLIENTRATE_WITH_ADD_KG="select client,network,service,zone_code,rate_type from clientrate_zone where client=? and network=? and service=? and rate_type=?";
	
	public static String DELETE_ADD_KG_FROM_RATEZONE="delete from clientrate_zone where client=? and network=? and service=? and rate_type=?";
	public static String UPDATE_REVERSE_ADD_KG_FROM_RATEZONE="UPDATE clientrate_zone set additional_reverse=null, kg_reverse=null where client=? and network=? and service=? and rate_type=?";
	
	
	public static String DELETE_EXTRA_ZONE_RATES="delete from  clientrate_zone where client=? and network=? and service=? and zone_code=?";
	
}
