package com.onesoft.courier.sql.queries;

public class MasterSQL_VAS_Utils {
	
	
	public static String VAS_INSERT_SQL="insert into vas_master(vas_code,vas_name) values(?,?)";
	
	public static String VAS_DATA_RETRIEVE_SQL="select vas_name from vas_master where vas_code=?";
	
	public static String VAS_UPDATE_SQL="update vas_master set vas_name=? where vas_code=?";

	public static String VAS_LOAD_TABLE_SQL="select vas_code,vas_name from vas_master order by slno";
	
	

}
