package com.onesoft.courier.sql.queries;

public class MasterSQL_Location_Utils {


	public static String APPLICATION_ID_FORALL="MMB";
	public static String LAST_MODIFIED_USER_FORALL="1";
	
	
// ============= SQL queries for Area Form =========================================	

	
	public static String INSERT_SQL_AREA_DETAILS="insert into area(code,area_name,zip_code,city_code,create_date,lastmodifieddate,"
														+ "applicationid,last_modified_user) values(?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	public static String DATA_RETRIEVE_SQL_AREA_DETAILS="select ar.code as areacode,ar.area_name as areaname,ar.zip_code as zipcode,"
														+ "ar.city_code as citycode,ctyms.city_name as cityname from area as ar,"
														+ "city_master as ctyms where ar.city_code=ctyms.city_code and ar.code=?";
	
	public static String UPDATE_SQL_AREA_DETAILS="update area SET area_name=?, zip_code=?, city_code=?, lastmodifieddate=CURRENT_TIMESTAMP "
														+ "where code=?";
	
	public static String LOADTABLE_SQL_AREA_DETAILS="select code,area_name,zip_code,city_code from area order by areaid DESC";
	
// ============= SQL queries for City Form =========================================		
	
	public static String INSERT_SQL_CITY_DETAILS="insert into city_master(state_code,city_code,city_name,create_date,lastmodifieddate,"
														+ "applicationid,last_modified_user) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
	
	public static String DATA_RETRIEVE_SQL_CITY_DETAILS="select ctyms.state_code statecode,ctyms.city_code as citycode,ctyms.city_name as cityname,"
														+ "stms.state_name as statename from city_master as ctyms,state_master as stms "
														+ "where ctyms.state_code=stms.state_code and city_code=?";
	
	public static String UPDATE_SQL_CITY_DETAILS="update city_master SET state_code=?, city_name=?, lastmodifieddate=CURRENT_TIMESTAMP where city_code=?";
	
	public static String LOADTABLE_SQL_CITY_DETAILS="select state_code, city_name, city_code from city_master order by create_date";
	
// ============= SQL queries for State Form =========================================		
	
	public static String INSERT_SQL_STATE_DETAILS="insert into state_master(state_code,country_code,state_name,create_date,lastmodifieddate,"
														+ "applicationid,last_modified_user) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
		
	
	public static String DATA_RETRIEVE_SQL_STATE_DETAILS="select stms.state_code as stcode, stms.state_name as stname, stms.country_code as countrycode,"
														+ "coms.country_name as countryname  from state_master as stms , country_master as coms"
														+ " where stms.country_code=coms.country_code and stms.state_code=?";
	
	public static String UPDATE_SQL_STATE_DETAILS="update state_master SET state_name=?, country_code=?, lastmodifieddate=CURRENT_TIMESTAMP where state_code=?";
	
	public static String LOADTABLE_SQL_STATE_DETAILS="select state_code, state_name, country_code from state_master order by sno DESC";


// ============= SQL queries for State Form =========================================		
		
	public static String INSERT_SQL_COUNTRY_DETAILS="insert into country_master(country_code,country_name,create_date,lastmodifieddate,"
													+ "applicationid,last_modified_user) values(?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
			
	public static String DATA_RETRIEVE_SQL_COUNTRY_DETAILS="select country_code, country_name from country_master where country_code=?";
	
	public static String UPDATE_SQL_COUNTRY_DETAILS="update country_master SET country_name=?, lastmodifieddate=CURRENT_TIMESTAMP  where country_code=?";
	
	public static String LOADTABLE_SQL_COUNTRY_DETAILS="select country_code, country_name from country_master order by create_date ASC";
	

// ============= SQL queries for Employee Registration Form =========================================			
	
	public static String INSERT_SQL_EMPLOYEE_REGISTRATION_DETAILS="insert into empdetail(code, full_name,emp_phone,emp_dob,emp_doj,email,"
													+ "address,city,state,country,empdetail2designation,status,user_type,pwd,create_date,lastmodifieddate) "
													+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";
	
	
	
}
