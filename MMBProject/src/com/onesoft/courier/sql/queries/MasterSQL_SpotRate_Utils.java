package com.onesoft.courier.sql.queries;

public class MasterSQL_SpotRate_Utils {
	
	public static String INSERT_SQL_SPOTRATE = "insert into spot_rate(spotrate_client,create_date,spotrate_forwarder,spotrate_network,spotrate_service,spotrate_type,"
			+ "spotrate_destination,from_weight,to_weight,sale_rate,purchase_rate,expiry_date,approved_by,calculation_type,"
			+ "fuel_chk,tax_chk,spotrate_remark,lastmodifieddate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";
	
	public static String LOAD_TABLE_SQL_SPOTRATE ="select spotrate_client, create_date, spotrate_forwarder, spotrate_network, spotrate_service, spotrate_destination,"
			+ " sale_rate, purchase_rate, from_weight, to_weight, expiry_date, approved_by from spot_rate order by slno desc";
	
	public static String VALIDIATE_SQL_SPOTRATE ="select spotrate_client,create_date,spotrate_forwarder,spotrate_network,spotrate_service,spotrate_type,"
			+ "spotrate_destination,from_weight,to_weight,sale_rate,purchase_rate,expiry_date,approved_by,calculation_type,"
			+ "fuel_chk,tax_chk,spotrate_remark,lastmodifieddate from spot_rate where spotrate_client=? and create_date=?"
			+ " and spotrate_forwarder=? and spotrate_network=? and spotrate_service=?"; 
	
	public static String UPDATE_SQL_SPOTRATE = "update spot_rate set spotrate_type=?,spotrate_destination=?,from_weight=?,to_weight=?,sale_rate=?,purchase_rate=?,"
			+ "expiry_date=?,approved_by=?,calculation_type=?,fuel_chk=?,tax_chk=?,spotrate_remark=?,lastmodifieddate=CURRENT_TIMESTAMP"
			+ " where spotrate_client=? and create_date=? and spotrate_forwarder=? and spotrate_network=? and spotrate_service=?";

}
