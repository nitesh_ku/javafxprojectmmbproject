package com.onesoft.courier.fedex.tracking;

public class FromToBean {
	
	private String city;
	private String zipcode;
	private String StateOrProvince;
	private String CountryCode;
	
	private String dateTime;
	private String timeSet;
	private String eventDescrition;
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public String getStateOrProvince() {
		return StateOrProvince;
	}
	public void setStateOrProvince(String stateOrProvince) {
		StateOrProvince = stateOrProvince;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getEventDescrition() {
		return eventDescrition;
	}
	public void setEventDescrition(String eventDescrition) {
		this.eventDescrition = eventDescrition;
	}
	public String getTimeSet() {
		return timeSet;
	}
	public void setTimeSet(String timeSet) {
		this.timeSet = timeSet;
	}
	
	
}
