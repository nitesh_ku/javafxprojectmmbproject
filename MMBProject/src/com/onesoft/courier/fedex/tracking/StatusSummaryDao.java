package com.onesoft.courier.fedex.tracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;




public class StatusSummaryDao {
	
	public void insertBusinessTypeData(ArrayList<StatusSummaryBean> listBean) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement ps=null;
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
			
			try
			{
				st=con.createStatement();
				for(StatusSummaryBean bean : listBean){
				sql="INSERT INTO status_summary(awbno, tracking_number, service, shipper_reference, branchcode, branch_receiving_person_id, event_no, indate, address, address_type, city, state, country, pincode, outdate, delivery_status, delivery_date, delivery_day, type, reason, received_by, remarks, receiving_date_time, create_date) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";
					
				ps = con.prepareStatement(sql);
					
				ps.addBatch();
				}
				ps.executeBatch();
				
				System.out.println("Business Type successfully Saved...!!");
				
			}
			
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			
			finally
			{
				dbcon.disconnect(ps, st, rs, con);
			//	showClientDetail();
			}
	}

}
