package com.onesoft.courier.fedex.tracking;

import java.sql.Date;

public class StatusSummaryBean {
	
	  private String trackingNumber;
	  private String serviceType;
	  private String serviceDescription;
	  private String shipperReference;
	  private int eventNo;
	  private String carrierCode;
	 private String addressType;
	  private Date deliveryDate;
	  private String deliveryDay;
	  private String type;
	  private String awbno;
	  private String branchCode;
	  private String branchReceivingPersonId;
	  private Date indate;
	  private String address;
	  private String city;
	  private String state;
	  private String country;
	  private int pincode ;
	  private int outdate;
	  private String deliveryStatus;
	  private String reason;
	  private String receivedBy;
	  private String remarks;
	  private String receiving_date_time;
	  
	  
	  
	  
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public String getShipperReference() {
		return shipperReference;
	}
	public void setShipperReference(String shipperReference) {
		this.shipperReference = shipperReference;
	}
	public int getEventNo() {
		return eventNo;
	}
	public void setEventNo(int eventNo) {
		this.eventNo = eventNo;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryDay() {
		return deliveryDay;
	}
	public void setDeliveryDay(String deliveryDay) {
		this.deliveryDay = deliveryDay;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAwbno() {
		return awbno;
	}
	public void setAwbno(String awbno) {
		this.awbno = awbno;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchReceivingPersonId() {
		return branchReceivingPersonId;
	}
	public void setBranchReceivingPersonId(String branchReceivingPersonId) {
		this.branchReceivingPersonId = branchReceivingPersonId;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getOutdate() {
		return outdate;
	}
	public void setOutdate(int outdate) {
		this.outdate = outdate;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getReceiving_date_time() {
		return receiving_date_time;
	}
	public void setReceiving_date_time(String receiving_date_time) {
		this.receiving_date_time = receiving_date_time;
	}
	  
}
