package com.onesoft.courier.fedex.tracking;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBconnection {
	
	public Connection ConnectDB()
	{
	Connection con=null;
	
	/*Properties prop = new Properties();
	InputStream input = null;*/
	
	try{
	/*	input = new FileInputStream("./config.properties");
		prop.load(input);*/
		Class.forName("org.postgresql.Driver");
		
		//con=DriverManager.getConnection("jdbc:postgresql://34.199.78.166:5432/clientrecorddb","client","onesoft@321");
		System.out.println("jdbc:postgresql://"+System.getProperty("jnlp.machine")+":"+System.getProperty("jnlp.port")+"/"+System.getProperty("jnlp.database")+System.getProperty("jnlp.dbuser")+System.getProperty("jnlp.dbpassword"));
		con=DriverManager.getConnection("jdbc:postgresql://"+System.getProperty("jnlp.machine")+":"+System.getProperty("jnlp.port")+"/"+System.getProperty("jnlp.database"),System.getProperty("jnlp.dbuser"),System.getProperty("jnlp.dbpassword"));
		//con=DriverManager.getConnection("jdbc:postgresql://61.246.6.193:5432/onesoftclient","mmb","dtdc1");
		
		System.out.println("Database successfully connected...!!");
		
		return con;
		
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	return null;

}
	
	public void disconnect(PreparedStatement psmt,Statement st, ResultSet rs, Connection con) throws SQLException
	{
		if(psmt != null)
			psmt.close();
		if(st != null)
			st.close();
		if(rs != null)
			rs.close();
		if(con != null)
			con.close();
		
		System.out.println("connection closed successfully");
	}
	
}

