/**
 * TrackPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.onesoft.courier.fedex.stub;

public interface TrackPortType extends java.rmi.Remote {
    public com.onesoft.courier.fedex.stub.SignatureProofOfDeliveryLetterReply retrieveSignatureProofOfDeliveryLetter(com.onesoft.courier.fedex.stub.SignatureProofOfDeliveryLetterRequest signatureProofOfDeliveryLetterRequest) throws java.rmi.RemoteException;
    public com.onesoft.courier.fedex.stub.TrackReply track(com.onesoft.courier.fedex.stub.TrackRequest trackRequest) throws java.rmi.RemoteException;
    public com.onesoft.courier.fedex.stub.SignatureProofOfDeliveryFaxReply sendSignatureProofOfDeliveryFax(com.onesoft.courier.fedex.stub.SignatureProofOfDeliveryFaxRequest signatureProofOfDeliveryFaxRequest) throws java.rmi.RemoteException;
    public com.onesoft.courier.fedex.stub.SendNotificationsReply sendNotifications(com.onesoft.courier.fedex.stub.SendNotificationsRequest sendNotificationsRequest) throws java.rmi.RemoteException;
}
