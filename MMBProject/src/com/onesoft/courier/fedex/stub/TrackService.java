/**
 * TrackService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.onesoft.courier.fedex.stub;

public interface TrackService extends javax.xml.rpc.Service {
    public java.lang.String getTrackServicePortAddress();

    public com.onesoft.courier.fedex.stub.TrackPortType getTrackServicePort() throws javax.xml.rpc.ServiceException;

    public com.onesoft.courier.fedex.stub.TrackPortType getTrackServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
