package com.onesoft.courier.home.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import com.onesoft.courier.administration.controller.AutoUpdateDeliveryStatusController;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.LoadGST_Rates;
import com.onesoft.courier.login.controller.LoginSessionTracking;
import com.onesoft.courier.main.Main;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

public class HomeController implements Initializable {
	
	
	//public static String loggedInUSER=null;
	//public static String userIdAndUserName=null;
	//public static String userBranch=null;
	
	private static final DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	
	
	@FXML
	private Label labUserIdAndUserName;
	
	@FXML
	private Label labDateTime;
	
	@FXML
	private MenuItem mItemInward;
	
	@FXML
	private MenuItem mItemOutward;
	
	@FXML
	private MenuItem mItemDataEntry;
	
	@FXML
	private MenuItem mItemClientBooking;
	
	@FXML
	private MenuItem mItemCloudInward;
	
	@FXML
	private MenuItem mItemForwarderDetails;
	
	@FXML
	private MenuItem mItemSearch;
	
	@FXML
	private MenuItem mItemArea;
	
	@FXML
	private MenuItem mItemCity;
	
	@FXML
	private MenuItem mItemState;
	
	@FXML
	private MenuItem mItemCountry;
	
	@FXML
	private MenuItem mItemEmployeeRegistration;
	
	@FXML
	private MenuItem mItemZone;
	
	@FXML
	private MenuItem mItemClient;
	
	@FXML
	private MenuItem mItemClientRate;
	
	@FXML
	private MenuItem mItemForwarderRate; 
	
	@FXML
	private MenuItem mItemImportClientRateExcel;
	
	@FXML
	private MenuItem mItemImportForwarderRateExcel;
	
	@FXML
	private MenuItem mItemNewImportClientRateExcel;
	
	@FXML
	private MenuItem mItemClientVAS;
	
	@FXML
	private MenuItem mItemVAS;
	
	@FXML
	private MenuItem mItemUploadPincode;
	
	@FXML
	private MenuItem mItemImportBookingData;
	
	@FXML
	private MenuItem mItemUpdateBookingData;
	
	@FXML
	private MenuItem mItemGenerateInvoice;
	
	@FXML
	private MenuItem mItemToPayInvoice;
	
	@FXML
	private MenuItem mItemGenerateInvoicePerforma;
	
	@FXML
	private MenuItem mItemInvoiceDeleteReprint;
	
	@FXML
	private MenuItem mItemComplaintRegister;
	
	@FXML
	private MenuItem mItemAddClient;
	
	@FXML
	private MenuItem mItemAutoCalculation;
	
	@FXML
	private MenuItem mItemSpotRate;
	
	@FXML
	private MenuItem mItemClientVsZoneReport;
	
	@FXML
	private MenuItem mItemInvoicePayment;
	
	@FXML
	private MenuItem mItemAutoUpdateDeliveryStatus;

	@FXML
	private MenuItem mItemManualUpdateDeliveryStatus;
	
	@FXML
	private MenuItem mItemManualPaymentReceiving;
	
	@FXML
	private MenuItem mItemManifest;
	
	@FXML
	private MenuItem mItemGoogleMapView;

	@FXML
	private Button btnLogout;
	
	LoginSessionTracking ls=new LoginSessionTracking();
	
	
	Main m=new Main();
	
	@FXML
	private void goInward() throws IOException
	{
		m.showInward();
	}
	
	@FXML
	private void goOutward() throws IOException
	{
		m.showOutward();
	}

	@FXML
	private void goDataEntry() throws IOException
	{
		m.showDataEntry();
	}
	
	@FXML
	private void goUpdateBookingData() throws IOException
	{
		m.showUpdateBookingData();
	}
	
	@FXML
	private void goImportBookingData() throws IOException
	{
		m.showImportBookingData();
	}
	
	@FXML
	private void goClientBooking() throws IOException
	{
		m.showClientBooking();
	}
	
	@FXML
	private void goCloudInward() throws IOException
	{
		m.showCloudInward();;
	}
	
	@FXML
	private void goForwarderDetails() throws IOException
	{
		m.showForwarderDetails();
	}

	@FXML
	private void goSearchPage() throws IOException
	{
		m.showSearchPage();
	}
	
	@FXML
	private void goAreaPage() throws IOException
	{
		m.showArea();
	}
	
	@FXML
	private void goCityPage() throws IOException
	{
		m.showCity();
	}
	
	@FXML
	private void goStatePage() throws IOException
	{
		m.showState();
	}
	
	@FXML
	private void goCountryPage() throws IOException
	{
		m.showCountry();
	}
	
	@FXML
	private void goEmployeeRegistrationPage() throws IOException
	{
		m.showEmployeeRegistrationDetails();
	}
	
	@FXML
	private void goZone() throws IOException
	{
		m.showZone();
	}
	
	@FXML
	private void goClient() throws IOException
	{
		m.showClient();
	}
	
	@FXML
	private void goClientRate() throws IOException
	{
		m.showClientRate();
	}
	
	@FXML
	private void goForwarderRate() throws IOException
	{
		m.showForwarderRate();
	}
	
	@FXML
	private void goImportClientRateExcel() throws IOException
	{
		m.showImportClientRateExcel();
	}
	
	@FXML
	private void goImportForwarderRateExcel() throws IOException
	{
		m.showImportForwarderRateExcel();
	}
	
	@FXML
	private void goNewImportClientRateExcel() throws IOException
	{
		m.showNewImportClientRateExcel();
	}
	
	@FXML
	private void goClientVAS() throws IOException
	{
		m.showClientVAS();
	}
	
	@FXML
	private void goVAS() throws IOException
	{
		m.showVAS();
	}
	
	@FXML
	private void goUploadPincode() throws IOException
	{
		m.showUploadPincodeExcel();
	}
	
	@FXML
	private void goGenerateInvoice() throws IOException
	{
		m.showGenerateInvoice();
	}
	
	@FXML
	private void goToPayInvoice() throws IOException
	{
		m.showToPayInvoice();
	}
	
	@FXML
	private void goGenerateInvoicePerforma() throws IOException
	{
		m.showGenerateInvoicePerforma();
	}
	
	@FXML
	private void goInvoiceDeleteReprint() throws IOException
	{
		m.showInvoiceDeleteReprint();
	}
	
	@FXML
	private void goComplaintRegister() throws IOException
	{
		m.showComplaintRegister();
	}
	
	@FXML
	private void goAddClient() throws IOException
	{
		m.showAddClient();
	}
	
	@FXML
	private void goAutoCalculation() throws IOException
	{
		m.showAutoCalculation();
	}
	
	@FXML
	private void goSpotRate() throws IOException
	{
		m.showSpotRate();
	}
	
	@FXML
	private void goClientVsZoneReport() throws IOException
	{
		m.showClientVsZoneReport();
	}
	
	@FXML
	private void goStackBarChart() throws IOException
	{
		//m.showStackedBarChart();
	}
	
	@FXML
	private void goAutoUpdateDeliveryStatus() throws IOException, SQLException
	{
		//new AutoUpdateDeliveryStatusController().autoUpdateConfirmation();
		m.showAutoUpdateDeliveryStatus();
	}
	
	@FXML
	private void goManualUpdateDeliveryStatus() throws IOException, SQLException
	{
		m.showManualUpdateDeliveryStatus();
	}
	
	@FXML
	private void goPaymentReceiving() throws IOException, SQLException
	{
		m.showPaymentReceiving();
	}
	
	@FXML
	private void goInvoicePaymentGraph() throws IOException, SQLException
	{
		m.showInvoicePaymentReport();
	}
	
	@FXML
	private void goManifestForm() throws IOException, SQLException
	{
		m.showManifestForm();
	}
	
	
	@FXML
	private void goGoogleMap() throws IOException, SQLException
	{
		m.showGoogleMap();
		
	
	}
	
	@FXML
	private void goLogout() throws IOException, ParseException, SQLException
	{	
		// ------------------- method to store logout detail ------------------------
		//ls.logoutDetail();
		
		Main.primaryStage.close();
		CommonVariable.USER_BRANCH_CITY=null;
		CommonVariable.USER_BRANCH_PINCODE=null;
		CommonVariable.USER_BRANCHCODE=null;
		CommonVariable.USER_IP_ADDRESS=null;
		CommonVariable.USER_HOST_NAME=null;
		//LoadOrigin.ORIGIN=null;
		Main.loginpage();
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		/*
		try {
			new LoadGST_Rates().loadLatestGST_onLoad();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		Timer timer = new Timer();
	    timer.scheduleAtFixedRate(new TimerTask() {
	        @Override
	        public void run() {
	            Platform.runLater(new Runnable() {
	                @Override
	                public void run() {
	                    labDateTime.setText(sdf.format(new Date()));
	                }
	            });
	        }
	    }, 0, 1000);
		
		//labDateTime.setText(sdf.format(date));
	    System.out.println("From home Controller >>> "+CommonVariable.LOGGED_IN_USER);
		labUserIdAndUserName.setText(CommonVariable.LOGGED_IN_USER);
		
		double num=5;
		double slab=1.2;
		double floor=Math.floor(num/slab);
		
		
		
		
		System.out.println("divider: "+num/slab);
		System.out.println("remainder: "+num%slab);
		System.out.println("floor: "+Math.floor(num/slab));
		
		if(floor!=(num/slab))
		{
			System.out.println("Decimal value 1 increment >> "+(floor+1));
		}
		else
		{
			System.out.println("integer value >> "+floor);
		}
		
	}

}
