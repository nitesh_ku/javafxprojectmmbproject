package com.onesoft.courier.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidator {

	
	
    public static String formatDate1(String value) {
        String fDate = null;
      //  System.out.println("INSIDE DATE METHOD 1::"+value);
        
        SimpleDateFormat original = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat target = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date;
        try {
            date = original.parse(value);
           // System.out.println("PARSING 1 ::"+date);
            fDate = target.format(date);
           // System.out.println("DONE 1 ::"+fDate);
        } catch (ParseException ex) {
        	//System.err.println("Incorrect Date format "+value);
            //  ex.printStackTrace();
        }
        return fDate;
    }
     
      public static String formatDate3(String value) {
        //  System.out.println("INSIDE DATE METHOD 3::"+value);
        String fDate = null;
        SimpleDateFormat original = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat target = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date;
        try {            
            date = original.parse(value);
            //System.out.println("PARSING 3 ::"+date);
            fDate = target.format(date);
          //  System.out.println("DONE 3 ::"+fDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return fDate;
    }

	
}
