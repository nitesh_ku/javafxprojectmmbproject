package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.ToPayBean;
import com.onesoft.courier.sql.queries.MasterSQL_ToPay_Utils;

public class LoadToPayData {

	public static List<ToPayBean> LIST_GETTOPAYDATA=new ArrayList<>();
	
	public void getToPayDataFromDB() throws SQLException
	{
		if(LIST_GETTOPAYDATA.isEmpty()==true)
		{
			System.out.println("*************************** DB connected for ToPay Details ********************");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			 		 
			try
			{	
				st=con.createStatement();
				String sql=MasterSQL_ToPay_Utils.GET_TOPAY_DATA_SQL;
				rs=st.executeQuery(sql);
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						ToPayBean tpBean=new ToPayBean();
						
						tpBean.setName(rs.getString("name"));
						tpBean.setBranchCode(rs.getString("clientmaster2branch"));
						tpBean.setPhone(rs.getString("phone_no"));
						tpBean.setAddress(rs.getString("address"));
						tpBean.setPincode(rs.getString("pincode"));
						tpBean.setCity(rs.getString("city"));
						tpBean.setCountry("IN");
						tpBean.setGstin(rs.getString("gstin_number"));
						tpBean.setEmail(rs.getString("emailid"));
						tpBean.setClientType(rs.getString("client_type"));
						
						LIST_GETTOPAYDATA.add(tpBean);
						
					}while(rs.next());
				}
				
				for(ToPayBean bean:LIST_GETTOPAYDATA)
				{
					System.err.println("Name :: "+bean.getName()+" | Branch :: "+bean.getBranchCode()+" | Ph :: "+bean.getPhone()+" | Address :: "+bean.getAddress()+
								" | Pin :: "+bean.getPincode()+" | City :: "+bean.getCity()+" | GSTIN :: "+bean.getGstin()+" | Email :: "+bean.getEmail()+" | Type :: "+bean.getClientType());
				}
			}
			
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("*************************** DB not ******** connected for ToPay Details ********************");
		}
	}
	
}
