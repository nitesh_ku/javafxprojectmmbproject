package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadZoneBean;



public class LoadZone {

	public static  final Set<String> SET_LOAD_ZONE_FOR_ALL=new HashSet<>();
	public static  final Set<LoadZoneBean> SET_LOAD_ZONE_WITH_NAME=new HashSet<>();
	
	public void loadZone() throws SQLException
	{
		
		if(SET_LOAD_ZONE_FOR_ALL.size()==0)
		{
			
			System.out.println("First tym get value from DB in Zone combobox...");
			System.out.println("Connection open... from LoadZone");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		//Set<String> setZone = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select zonedetail2zonetype from zonedetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				SET_LOAD_ZONE_FOR_ALL.add(rs.getString("zonedetail2zonetype"));
			//	setZone.add(rs.getString(1));
			}
		
			/*for(String zone:setZone)
			{
				comboBoxZoneItems.add(zone);
			}
		comboBoxZone.setItems(comboBoxZoneItems);	
			*/
		
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			System.out.println("Connection Closed... from LoadZone");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Now value get from Set Object in LoadZone class...");
		}
	}
	
	
	
	public void loadZoneWithName() throws SQLException
	{
		if(SET_LOAD_ZONE_WITH_NAME.size()==0)
		{
			
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code,name from zonetype";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				LoadZoneBean zBean=new LoadZoneBean();
				
				zBean.setZoneCode(rs.getString("code"));
				zBean.setZoneName(rs.getString("name"));
				
				SET_LOAD_ZONE_WITH_NAME.add(zBean);
			}
		
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			System.out.println("Connection Closed... from LoadZone");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Now value get from Set Object in LoadZone class...");
		}
	}
}
