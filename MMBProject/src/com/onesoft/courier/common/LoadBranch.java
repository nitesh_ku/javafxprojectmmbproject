package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadBranchBean;

public class LoadBranch {
	
	
	public static String check;//="Test";
	
	public static Set<String> SETLOADBRANCHFORALL=new HashSet<>();
	public static List<LoadBranchBean> SET_LOADBRANCHWITHNAME=new ArrayList<>();
	//public static List<String> SETLOADBRANCHFORALL=new ArrayList<>();
	
	
//	public static Set<String> setBranch=new HashSet<>();
	
	/*public void loadOrigin() throws SQLException
	{
		
		check="Test";
		
		if(SETLOADBRANCHFORALL.size()==0)
		{
		
		//if(setBranch.size()==0)
		//{
			System.out.println("Load Branch if Running... "+SETLOADBRANCHFORALL.size());
			System.out.println("Value getting from Set Value Using DB connection");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select code from brndetail";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					//setBranch.add(rs.getString("code"));
					SETLOADBRANCHFORALL.add(rs.getString("code"));
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");
			
		}
	}*/
	
	
	public void updateBranch() throws SQLException
	{
		System.out.println("Branch Update running...");	
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select code from brndetail";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					//setBranch.add(rs.getString("code"));
					SETLOADBRANCHFORALL.add(rs.getString("code"));
				}
				
				for(String value: SETLOADBRANCHFORALL)
				{
					System.out.println("Value from update method: .... "+value);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		
	}
	
	
	public void loadBranchWithName() throws SQLException
	{
		
		if(SET_LOADBRANCHWITHNAME.size()==0)
		{
		
	/*	if(setBranch.size()==0)
		{*/	
			System.out.println("Load Branch if Running... "+SETLOADBRANCHFORALL.size());
			System.out.println("Value getting from Set Value Using DB connection");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select code,brn_name,brndetail2cmpdetail,branch_city,branch_pincode from brndetail";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadBranchBean bean=new LoadBranchBean();
					//setBranch.add(rs.getString("code"));
					bean.setBranchCode(rs.getString("code"));
					bean.setBranchName(rs.getString("brn_name"));
					bean.setBranchCity(rs.getString("branch_city"));
					bean.setCmpnyCode(rs.getString("brndetail2cmpdetail"));
					bean.setBranchPincode(rs.getString("branch_pincode"));
					
					SET_LOADBRANCHWITHNAME.add(bean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			/*System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");*/
			
		}
		
	}
}
