package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadCityBean;

public class LoadCountry {
	
	public static  final List<LoadCityBean> SET_LOAD_COUNTRYWITHNAME=new ArrayList<>();
	
	public void loadCountryWithName() throws SQLException
	{
		if(SET_LOAD_COUNTRYWITHNAME.size()==0)
		{
			System.out.println("Country with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select country_name,country_code from country_master";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCityBean loadClientBean=new LoadCityBean();
					loadClientBean.setCountryName(rs.getString("country_name"));
					loadClientBean.setCountryCode(rs.getString("country_code"));
					
					SET_LOAD_COUNTRYWITHNAME.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Country with name load by list...");
			
		}
	}

}
