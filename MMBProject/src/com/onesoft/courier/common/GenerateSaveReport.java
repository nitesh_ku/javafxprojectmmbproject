package com.onesoft.courier.common;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import com.onesoft.courier.DB.DBconnection;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class GenerateSaveReport {

	JasperPrint jasperPrint;
	String path;
	String reportSource;
	String postgressQuery;
	
	Map<String, Object> parameter;
	
	public void exportReport(String finalQuery, String checkBoxValue, String src, Map<String, Object> parameters) throws IOException, JRException, SQLException {
		postgressQuery = finalQuery;
		reportSource = src;
		parameter = parameters;
		if (checkBoxValue.equals("true")) {
			selectDirectory();
		} else {
			generateReport();
		}
	}

	public void generateReport() throws JRException, SQLException {
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		System.out.println("Connection for Report");

		try {
			JasperDesign jasperDesign = JRXmlLoader.load(reportSource);
			JRDesignQuery newQuery = new JRDesignQuery();
			newQuery.setText(postgressQuery);
			jasperDesign.setQuery(newQuery);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, con);
			
			JasperViewer.viewReport(jasperPrint, false);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbcon.disconnect(null, null, null, con);
			System.out.println("Connection closed");
		}
	}

	public void exportXLS() throws JRException, SQLException {
		generateReport();
		/*JRCsvExporter exportercsv = new JRCsvExporter();
		exportercsv.setExporterInput(new SimpleExporterInput(jasperPrint));
		exportercsv.setExporterOutput(new SimpleWriterExporterOutput(path));
		SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
		configuration.setOverrideHints(false);
		exportercsv.setConfiguration(configuration);
		exportercsv.exportReport();*/
		
		JRXlsExporter exporter = new JRXlsExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(path));
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setWhitePageBackground(false);
		configuration.setDetectCellType(true);
		configuration.setRemoveEmptySpaceBetweenRows(true);
		configuration.setRemoveEmptySpaceBetweenColumns(true);
		configuration.setCollapseRowSpan(false);
		exporter.setConfiguration(configuration);
		exporter.exportReport();
	}

	public void selectDirectory() throws IOException, JRException, SQLException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fileChooser.showSaveDialog(null);
		if (file != null) {
			System.out.println("IF");
			path = file.getCanonicalPath();
			exportXLS();
		} else {
			System.out.println("ELSE");
		}
	}

}
