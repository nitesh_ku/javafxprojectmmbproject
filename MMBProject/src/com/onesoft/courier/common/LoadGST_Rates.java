package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadGST_RateBean;

public class LoadGST_Rates {
	
	public static List<LoadGST_RateBean> LIST_GST_RATE_SLAB=new ArrayList<>();
	public static List<LoadGST_RateBean> LIST_LATEST_GST_RATE_SLAB=new ArrayList<>();
	public static Date GSTDATE=null;
	
	public static Date gst_Date_Temp=null;
	public static double cgst_rate_Temp=0;
	public static double sgst_rate_Temp=0;
	public static double igst_rate_Temp=0;
	
	public static String cgst_Name=null;
	public static String sgst_Name=null;
	public static String igst_Name=null;
	
	public static Date gst_Date_Fix=null;
	public static double cgst_rate_fix=0;
	public static double sgst_rate_fix=0;
	public static double igst_rate_fix=0;
	
	
	
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	public void loadLatestGST_onLoad() throws SQLException
	{
		LIST_LATEST_GST_RATE_SLAB.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
				 
		try
		{		
			st=con.createStatement();
			String sql="select effective_date,taxname1,taxrate1,taxname2,taxrate2,taxname3,taxrate3 from expensestaxtype order by effective_date DESC limit 1";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				LoadGST_RateBean gstbean=new LoadGST_RateBean();
				gstbean.setGstEffectiveDate(rs.getDate("effective_date"));
				gstbean.setTax1_cgst(rs.getString("taxname1"));
				gstbean.setTax2_sgst(rs.getString("taxname2"));
				gstbean.setTax3_igst(rs.getString("taxname3"));
				
				gstbean.setTax1_cgst_rate(rs.getDouble("taxrate1"));
				gstbean.setTax2_sgst_rate(rs.getDouble("taxrate2"));
				gstbean.setTax3_igst_rate(rs.getDouble("taxrate3"));
				
				LIST_LATEST_GST_RATE_SLAB.add(gstbean);
			}
			
			for(LoadGST_RateBean gstBean: LoadGST_Rates.LIST_LATEST_GST_RATE_SLAB)
			{
				cgst_Name=gstBean.getTax1_cgst();
				sgst_Name=gstBean.getTax2_sgst();
				igst_Name=gstBean.getTax3_igst();
				
				gst_Date_Fix=gstBean.getGstEffectiveDate();
				cgst_rate_fix=gstBean.getTax1_cgst_rate();
				sgst_rate_fix=gstBean.getTax2_sgst_rate();
				igst_rate_fix=gstBean.getTax3_igst_rate();		
				//System.out.println("Current GST Fix date >> "+gst_Date_Fix);
				System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_fix+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_fix+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_fix);
			}
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	public void loadGST_OnChangeDate(Date effectiveDate) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
				 
		try
		{		
			st=con.createStatement();
			String sql="select effective_date,taxname1,taxrate1,taxname2,taxrate2,taxname3,taxrate3 from expensestaxtype "
						+ "where effective_date<='"+effectiveDate+"' order by effective_date DESC limit 1";
			rs=st.executeQuery(sql);
			
			
			
			while(rs.next())
			{
				LIST_GST_RATE_SLAB.clear();
				
				LoadGST_RateBean gstbean=new LoadGST_RateBean();

				gstbean.setGstEffectiveDate(rs.getDate("effective_date"));
				gstbean.setTax1_cgst(rs.getString("taxname1"));
				gstbean.setTax2_sgst(rs.getString("taxname2"));
				gstbean.setTax3_igst(rs.getString("taxname3"));

				gstbean.setTax1_cgst_rate(rs.getDouble("taxrate1"));
				gstbean.setTax2_sgst_rate(rs.getDouble("taxrate2"));
				gstbean.setTax3_igst_rate(rs.getDouble("taxrate3"));

				LIST_GST_RATE_SLAB.add(gstbean);
			}
			
		//	System.out.println("IF Temp date is null >>>>>>>>>>>>>>>");
			
			if(LIST_GST_RATE_SLAB.size()>0)
			{
				for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
				{
					gst_Date_Temp=gstBean.getGstEffectiveDate();
					cgst_rate_Temp=gstBean.getTax1_cgst_rate();
					sgst_rate_Temp=gstBean.getTax2_sgst_rate();
					igst_rate_Temp=gstBean.getTax3_igst_rate();
				//	System.out.println("If GST TEMP if date >> "+gst_Date_Temp);
					System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_Temp+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_Temp+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_Temp);
				}
			}
			else
			{
				System.out.println("CGST: "+LoadGST_Rates.cgst_rate_fix+" | SGST: "+LoadGST_Rates.sgst_rate_fix+" | IGST: "+LoadGST_Rates.igst_rate_fix);
			}
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	/*
	public void loadGST_OnChangeDate(Date effectiveDate) throws SQLException
	{	
		//int status=date1.compareTo(date2);
		if(LIST_GST_RATE_SLAB.size()==0)
		{
		if(gst_Date_Temp==null)
		{
			
				DBconnection dbcon=new DBconnection();
				Connection con=dbcon.ConnectDB();
				Statement st=null;
				ResultSet rs=null;
						 
				try
				{		
					st=con.createStatement();
					String sql="select effective_date,taxname1,taxrate1,taxname2,taxrate2,taxname3,taxrate3 from expensestaxtype where effective_date='"+effectiveDate+"'";
					rs=st.executeQuery(sql);
					
					
					
					while(rs.next())
					{
						LIST_GST_RATE_SLAB.clear();
						
						LoadGST_RateBean gstbean=new LoadGST_RateBean();

						gstbean.setGstEffectiveDate(rs.getDate("effective_date"));
						gstbean.setTax1_cgst(rs.getString("taxname1"));
						gstbean.setTax2_sgst(rs.getString("taxname2"));
						gstbean.setTax3_igst(rs.getString("taxname3"));

						gstbean.setTax1_cgst_rate(rs.getDouble("taxrate1"));
						gstbean.setTax2_sgst_rate(rs.getDouble("taxrate2"));
						gstbean.setTax3_igst_rate(rs.getDouble("taxrate3"));

						LIST_GST_RATE_SLAB.add(gstbean);
					}
					
				//	System.out.println("IF Temp date is null >>>>>>>>>>>>>>>");
					
					if(LIST_GST_RATE_SLAB.size()>0)
					{
						for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
						{
							gst_Date_Temp=gstBean.getGstEffectiveDate();
							cgst_rate_Temp=gstBean.getTax1_cgst_rate();
							sgst_rate_Temp=gstBean.getTax2_sgst_rate();
							igst_rate_Temp=gstBean.getTax3_igst_rate();
						//	System.out.println("If GST TEMP if date >> "+gst_Date_Temp);
							System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_Temp+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_Temp+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_Temp);
						}
					}
					else
					{
						for(LoadGST_RateBean gstBean: LoadGST_Rates.LIST_LATEST_GST_RATE_SLAB)
						{
							LoadGST_Rates.gst_Date_Fix=gstBean.getGstEffectiveDate();
							LoadGST_Rates.cgst_rate_fix=gstBean.getTax1_cgst_rate();
							LoadGST_Rates.sgst_rate_fix=gstBean.getTax2_sgst_rate();
							LoadGST_Rates.igst_rate_fix=gstBean.getTax3_igst_rate();		
							//System.out.println("Current GST Fix date >> "+LoadGST_Rates.gst_Date_Fix);
							System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_fix+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_fix+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_fix);
						}
						
						System.out.println("GST Temp else date >> "+gst_Date_Temp);
						System.out.println("CGST: "+LoadGST_Rates.cgst_rate_Temp+" | SGST: "+LoadGST_Rates.sgst_rate_Temp+" | IGST: "+LoadGST_Rates.igst_rate_Temp);
					}
				
					
					for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
					{
						System.out.println(gstBean.getTax1_cgst()+" >> "+gstBean.getTax1_cgst_rate()+" | "+gstBean.getTax2_sgst()+" >> "+gstBean.getTax2_sgst_rate()+" | "+gstBean.getTax3_igst()+" >> "+gstBean.getTax3_igst_rate());
					}
					
					//gst_Date_Temp=effectiveDate;
				}
			
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}	
				
				finally
				{
					dbcon.disconnect(null, st, rs, con);
				}
			
		}
		else
		{
		if(effectiveDate.before(gst_Date_Temp))
		{
			
		
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select effective_date,taxname1,taxrate1,taxname2,taxrate2,taxname3,taxrate3 from expensestaxtype where effective_date='"+effectiveDate+"'";
				rs=st.executeQuery(sql);
				
				System.out.println("SQL : "+sql);
				
				while(rs.next())
				{
					LIST_GST_RATE_SLAB.clear();
					
					LoadGST_RateBean gstbean=new LoadGST_RateBean();

					gstbean.setGstEffectiveDate(rs.getDate("effective_date"));
					gstbean.setTax1_cgst(rs.getString("taxname1"));
					gstbean.setTax2_sgst(rs.getString("taxname2"));
					gstbean.setTax3_igst(rs.getString("taxname3"));

					gstbean.setTax1_cgst_rate(rs.getDouble("taxrate1"));
					gstbean.setTax2_sgst_rate(rs.getDouble("taxrate2"));
					gstbean.setTax3_igst_rate(rs.getDouble("taxrate3"));

					LIST_GST_RATE_SLAB.add(gstbean);
				}
				
				
				
				//System.out.println("Else Temp date is null >>>>>>>>>>>>>>>");
				
				if(LIST_GST_RATE_SLAB.size()>0)
				{
					for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
					{
						gst_Date_Temp=gstBean.getGstEffectiveDate();
						cgst_rate_Temp=gstBean.getTax1_cgst_rate();
						sgst_rate_Temp=gstBean.getTax2_sgst_rate();
						igst_rate_Temp=gstBean.getTax3_igst_rate();
						//System.out.println("Else GST Temp Else date >> "+gst_Date_Temp);
						System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_Temp+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_Temp+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_Temp);
					}
				}
				else
				{
					//System.out.println("GST TEMP date >> "+gst_Date_Temp);
					System.out.println("CGST: "+LoadGST_Rates.cgst_rate_Temp+" | SGST: "+LoadGST_Rates.sgst_rate_Temp+" | IGST: "+LoadGST_Rates.igst_rate_Temp);
				}
			
				
				for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
				{
					System.out.println(gstBean.getTax1_cgst()+" >> "+gstBean.getTax1_cgst_rate()+" | "+gstBean.getTax2_sgst()+" >> "+gstBean.getTax2_sgst_rate()+" | "+gstBean.getTax3_igst()+" >> "+gstBean.getTax3_igst_rate());
				}
				
				//gst_Date_Temp=effectiveDate;
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else //if(effectiveDate==gst_Date_Temp)
		{
			//System.err.println("Same PPPPP");
			if(LIST_GST_RATE_SLAB.size()>0)
			{
				for(LoadGST_RateBean gstBean:LIST_GST_RATE_SLAB)
				{
					gst_Date_Temp=gstBean.getGstEffectiveDate();
					cgst_rate_Temp=gstBean.getTax1_cgst_rate();
					sgst_rate_Temp=gstBean.getTax2_sgst_rate();
					igst_rate_Temp=gstBean.getTax3_igst_rate();
				//	System.out.println("Else GST Temp Else date >> "+gst_Date_Temp);
					System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_Temp+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_Temp+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_Temp);
				}
			}
			else
			{
				//System.out.println("GST TEMP date >> <<<<<"+gst_Date_Temp);
				System.out.println("CGST: "+LoadGST_Rates.cgst_rate_Temp+" | SGST: "+LoadGST_Rates.sgst_rate_Temp+" | IGST: "+LoadGST_Rates.igst_rate_Temp);
			}
		}
		
}*/
			/*}
		else
		{
			System.out.println("\n");
			
			System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");
			
		}*/
		
	//}

}
