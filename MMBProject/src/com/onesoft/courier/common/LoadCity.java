package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;

public class LoadCity {


public static  final List<LoadCityBean> SET_LOAD_CITYWITHNAME=new ArrayList<>();
public static  final List<LoadCityBean> SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER=new ArrayList<>();

public static  final List<LoadCityWithZipcodeBean> LIST_LOAD_CITY_WITH_ZIPCODE=new ArrayList<>();
	
	
	public void loadCityWithName() throws SQLException
	{
		if(SET_LOAD_CITYWITHNAME.size()==0)
		{
			System.out.println("Client with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select city_code, city_name, state_code from city_master";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCityBean loadClientBean=new LoadCityBean();
					loadClientBean.setCityName(rs.getString("city_name"));
					loadClientBean.setCityCode(rs.getString("city_code"));
					loadClientBean.setStateCode(rs.getString("state_code"));
					
					SET_LOAD_CITYWITHNAME.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Client with name load by list...");
			
		}
	}
	
	
	public void loadCityWithNameFromPinCodeMaster() throws SQLException
	{
		if(SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER.size()==0)
		{
			System.out.println("Client with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select city_name,state_code from pincode_master group by city_name,state_code";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCityBean loadClientBean=new LoadCityBean();
					loadClientBean.setCityName(rs.getString("city_name"));
					//loadClientBean.setCityCode(rs.getString("city_code"));
					loadClientBean.setStateCode(rs.getString("state_code"));
					
					SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Client with name load by list...");
			
		}
	}
	
	
	
	public void loadZipcodeCityDetails() throws SQLException
	 {
		if(LIST_LOAD_CITY_WITH_ZIPCODE.size()==0)
		{		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
			
				 
		try
		{		
			st=con.createStatement();
			String sql="select pincode,city_name,state_code from pincode_master order by sno";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				LoadCityWithZipcodeBean bean=new LoadCityWithZipcodeBean();
				bean.setZipcode(rs.getLong("pincode"));
				bean.setCityName(rs.getString("city_name"));
				bean.setState_code(rs.getString("state_code"));
				LIST_LOAD_CITY_WITH_ZIPCODE.add(bean);
				
			}

			/*for(ZipcodeCityBean zip:zipcodeList)
			{
				System.out.println("Pincode: "+zip.getPincode()+", City Name: "+zip.getCity_name());
			}*/
			
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			
			
		}
		}
		else
		{
			System.out.println("Now get the values from variable of set for CIITY NAME with Zipcode =========");
		}
		
		
	}	
	
	
}
