package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadEmployeeBean;

public class LoadEmployee {
	
public static  final Set<LoadEmployeeBean> SET_LOAD_EMPLOYEE=new HashSet<>();
	
public void loadEmployeeWithName() throws SQLException
{
	if(SET_LOAD_EMPLOYEE.size()==0)
	{
		System.out.println("Employee load by DB...");
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
				 
		try
		{		
			st=con.createStatement();
			String sql="select code,full_name from empdetail";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				LoadEmployeeBean bean=new LoadEmployeeBean();
				//setBranch.add(rs.getString("code"));
				bean.setEmployeeCode(rs.getString("code"));
				bean.setEmployeeName(rs.getString("full_name"));
				
				SET_LOAD_EMPLOYEE.add(bean);
			}
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	else
	{
		System.out.println("\n");
		System.out.println("Employee load by list...");
		
	}
	
}
	

}
