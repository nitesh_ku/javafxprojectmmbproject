package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;

public class LoadServiceGroups {

	public static  final Set<String> SET_LOAD_SERVICES_FOR_ALL=new HashSet<>();
	public static  final List<LoadServiceWithNetworkBean> LIST_LOAD_SERVICES_WITH_NETWORK=new ArrayList<>();
	public static  final List<LoadServiceWithNetworkBean> LIST_LOAD_SERVICES_WITH_NAME=new ArrayList<>();
	public static  final List<LoadServiceWithNetworkBean> LIST_LOAD_SERVICES_FROM_SERVICETYPE=new ArrayList<>();
	
	public void loadServiceGroup() throws SQLException
	{
		
		if(SET_LOAD_SERVICES_FOR_ALL.size()==0)
		{
			System.out.println("Connection Open from LoadServiceGroup Class");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select sgtp.code as code from servicetype as stp, servicegrouptype as sgtp where stp.service2group=sgtp.code GROUP BY sgtp.code";
			rs=st.executeQuery(sql);
			//comboBoxServiceGroup.setValue(FilterUtils.ALLSERVICEGROUP);
			//comboBoxNetworkItems.add("All Networks");
			while(rs.next())
			{
				SET_LOAD_SERVICES_FOR_ALL.add(rs.getString("code"));
				//comboBoxServiceGroupItems.add(rs.getString("code"));
			//	set.add(rs.getString(2));
			}
	
			/*for(String s:set)
			{
				//comboBoxNetworkItems.add(s);
			}
		*/
			//comboBoxServiceGroup.setItems(comboBoxServiceGroupItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			System.out.println("Connection Close from LoadServiceGroup Class");
		}
		
		}
		else
		{
			System.out.println("Direct all Set...  LoadServiceGroup");
		}
	}
	
	
	
// ================== Services from servicedetail table with Network ====================	
	
	public void loadServicesWithNetwork() throws SQLException
	{
		if(LIST_LOAD_SERVICES_WITH_NETWORK.size()==0)
		{
			System.out.println("Connection Open from LoadServiceGroup Class");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select service_code, vendor_code from servicedetails";
			rs=st.executeQuery(sql);
			while(rs.next())
			{
				LoadServiceWithNetworkBean serviceNameBean=new LoadServiceWithNetworkBean();
				serviceNameBean.setServiceCode(rs.getString("service_code"));
				serviceNameBean.setNetworkCode(rs.getString("vendor_code"));
				
				LIST_LOAD_SERVICES_WITH_NETWORK.add(serviceNameBean);
			}
	
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			System.out.println("Connection Close from LoadServiceGroup Class");
		}
		
		}
		else
		{
			System.out.println("Direct all Set...  LoadServiceGroup");
		}
	}
	
	

// ================== Services from servicedetail table with Network ====================	
		
	public void loadServicesWithName() throws SQLException
	{
		if(LIST_LOAD_SERVICES_WITH_NAME.size()==0)
		{
			System.out.println("Connection Open from LoadServiceGroup Class");
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();

			Statement st=null;
			ResultSet rs=null;

			try
			{	
				st=con.createStatement();
				String sql = "select sd.service_code as code,sd.vendor_code as network,st.name as name from servicedetails as sd,"
						+ " servicetype as st where sd.service_code=st.code and sd.vendor_code !='' "
						+ "group by sd.service_code,st.name,sd.vendor_code";
				rs=st.executeQuery(sql);
				while(rs.next())
				{
					LoadServiceWithNetworkBean serviceNameBean=new LoadServiceWithNetworkBean();
					serviceNameBean.setServiceCode(rs.getString("code"));
					serviceNameBean.setServiceName(rs.getString("name"));
					serviceNameBean.setNetworkCode(rs.getString("network"));

					LIST_LOAD_SERVICES_WITH_NAME.add(serviceNameBean);
				}

			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
				System.out.println("Connection Close from LoadServiceGroup Class");
			}

		}
		else
		{
			System.out.println("Direct all Set...  LoadServiceGroup");
		}
	}	
	
	
	
// ================== Services from servicedetail table with Network ====================	
	
	public void loadServicesFromServiceType() throws SQLException
	{
		if(LIST_LOAD_SERVICES_FROM_SERVICETYPE.size()==0)
		{
			System.out.println("Connection Open from LoadServiceGroup Class");
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();

			Statement st=null;
			ResultSet rs=null;

			try
			{	
				st=con.createStatement();
				String sql = "select code, name, service_mode from servicetype";
				rs=st.executeQuery(sql);
				while(rs.next())
				{
					LoadServiceWithNetworkBean serviceCode_NameBean=new LoadServiceWithNetworkBean();
					serviceCode_NameBean.setServiceCode(rs.getString("code"));
					serviceCode_NameBean.setServiceName(rs.getString("name"));
					serviceCode_NameBean.setServiceMode(rs.getString("service_mode"));
					
					LIST_LOAD_SERVICES_FROM_SERVICETYPE.add(serviceCode_NameBean);
				}

			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
				System.out.println("Connection Close from LoadServiceGroup Class");
			}

		}
		else
		{
			System.out.println("Direct all Set...  LoadServiceGroup");
		}
	}	
	
}
