package com.onesoft.courier.common;

public class GraphFilterUtils {

	public static String STACKBARCHART_SQL =null;
	
	
	public static final String ALL_BRANCH = "All Branches";
	
	
	public static final String TYPE2 = "Cash";
	public static final String TYPE3 = "Credit";
	
	public static final String MODE_AIR = "Air";
	public static final String MODE_SURFACE = "Surface";
	
	public static final String TYPE_BOTH = "Both";
	public static final String TYPE_CASH = "CASH";
	public static final String TYPE_CREDIT = "CREDIT";
	
	public static final String ALL_CLIENT="All Clients";
	
	public static final String ALLCITY="All Cities";
	
	public static final String TOP5CITY="Top 5 Cities";
	public static final String TOP10CITY="Top 10 Cities";
	public static final String TOP5LIMIT = "DESC LIMIT 5";
	public static final String TOP10LIMIT = "DESC LIMIT 10";
	
	public static final String ALL_NETWORK = "All Networks";
	
	public static final String ALLSERVICEGROUP = "All Service Groups";
	
	public static final String DND="All Dox/Non-Dox";
	public static final String DOX="Dox";	
	public static final String NONDOX="Non-Dox";
	
	public static final String ALL_ZONE="All Zones";

	public static final String YEAR1 = "2017-2018";
	public static final String YEAR2 = "2016-2017";
	public static final String YEAR3 = "2015-2016";
	public static final String YEAR4 = "2014-2015";
	public static final String YEAR5 = "2013-2014";
	public static final String YEAR6 = "2012-2013";
	
	public static final String EXPENSEBRANCHNAME="ONE";
	
	
	public static final String CATEGORY1SDATE = "-04-01";					// Date DD.MM format
	public static final String CATEGORY1EDATE = "-03-31";
	
	public static String filterSql="";
	public static String paymentStatus="";

	


	
	
	
}
