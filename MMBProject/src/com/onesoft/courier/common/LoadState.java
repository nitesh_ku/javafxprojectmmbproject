package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;

public class LoadState {

	public static  final List<LoadCityBean> SET_LOAD_STATEWITHNAME=new ArrayList<>();
	public static  final List<LoadCityBean> SET_LOAD_STATEWITHNAME_FROM_PINCODEMASTER=new ArrayList<>();
	//public static  final List<LoadCityWithZipcodeBean> LIST_LOAD_STATE_WITH_ZIPCODE=new ArrayList<>();
	
	
	public void loadStateWithName() throws SQLException
	{
		if(SET_LOAD_STATEWITHNAME.size()==0)
		{
			System.out.println("State with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select state_name,state_code,country_code from state_master";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCityBean loadClientBean=new LoadCityBean();
					loadClientBean.setStateName(rs.getString("state_name"));
					loadClientBean.setStateCode(rs.getString("state_code"));
					loadClientBean.setCountryCode(rs.getString("country_code"));
					
					SET_LOAD_STATEWITHNAME.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("State with name load by list...");
			
		}
	}
	
	
	public void loadStateFromPincodeMaster() throws SQLException
	{
		if(SET_LOAD_STATEWITHNAME_FROM_PINCODEMASTER.size()==0)
		{
			System.out.println("State with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select pm.state_code as st_code,sm.state_name as st_name,sm.country_code as cntry_code from state_master as sm, pincode_master as pm where sm.state_code=pm.state_code group by pm.state_code,sm.state_name,sm.country_code order by pm.state_code ASC";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCityBean loadClientBean=new LoadCityBean();
					loadClientBean.setStateName(rs.getString("st_name"));
					loadClientBean.setStateCode(rs.getString("st_code"));
					loadClientBean.setCountryCode(rs.getString("cntry_code"));
					
					SET_LOAD_STATEWITHNAME_FROM_PINCODEMASTER.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("State with name load by list...");
			
		}
	}
	
	
	/*public void loadZipcodeCityDetails() throws SQLException
	 {
		if(LIST_LOAD_CITY_WITH_ZIPCODE.size()==0)
		{		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
			
				 
		try
		{		
			st=con.createStatement();
			String sql="select pm.pincode as pincode,pm.city_code as citycode,ctym.city_name as cityname from pincode_master as pm, city_master ctym where pm.city_code=ctym.city_code";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				LoadCityWithZipcodeBean bean=new LoadCityWithZipcodeBean();
				bean.setZipcode(rs.getLong("pincode"));
				bean.setCityCode(rs.getString("citycode"));
				bean.setCityName(rs.getString("cityname"));
				LIST_LOAD_CITY_WITH_ZIPCODE.add(bean);
				
				
			}

			for(ZipcodeCityBean zip:zipcodeList)
			{
				System.out.println("Pincode: "+zip.getPincode()+", City Name: "+zip.getCity_name());
			}
			
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			
			
		}
		}
		else
		{
			System.out.println("Now get the values from variable of set for CIITY NAME with Zipcode =========");
		}
		
		
	}*/
	
	
}
