package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadUserBean;

public class LoadUser {

	public static Set<LoadUserBean> SET_LOAD_USER=new HashSet<>();
	
	public void loadUserWithID() throws SQLException
	{
		if(SET_LOAD_USER.size()==0)
		{
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select userid,username,branchcode from logindetail";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadUserBean userBean=new LoadUserBean();
					userBean.setUserid(rs.getString("username"));
					userBean.setUserName(rs.getString("userid"));
					userBean.setBranchCode(rs.getString("branchcode"));
					
					SET_LOAD_USER.add(userBean);
					
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			System.out.println("Load Branch Setbranch size is: "+SET_LOAD_USER.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");
			
		}
	}
	
}
