package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class BatchExecutor 
{	
	
// ************* PreparedStatement Process *************
	
	public void batchExecuteForBulkData_PrepStmt(PreparedStatement psmt,Connection con,int count) throws SQLException
	{
		int batchSize=50;
		int[] result;
		if(count % batchSize == 0)
		{
		//	System.out.println("Commit the batch :: "+psmt);
			result = psmt.executeBatch();
			//System.out.println("Number of rows inserted: "+ result.length);
			con.commit();
		}
	}
	
// ************* Statement Process *************
	
	public void batchExecuteForBulkData_Statement(Statement stmt,Connection con,int count) throws SQLException
	{
		int batchSize=50;
		int[] result;
		if(count % batchSize == 0)
		{
			System.out.println("Commit the batch");
			result = stmt.executeBatch();
			System.out.println("Number of rows inserted: "+ result.length);
			con.commit();
		}
	}
	
}
