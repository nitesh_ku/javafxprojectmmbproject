package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadCompanyBean;

public class LoadCompany {
	
public static  final List<LoadCompanyBean> SET_LOAD_COMPANYWITHALLDETAILS=new ArrayList<>();
	
	public void loadCompanyWithName() throws SQLException
	{
		if(SET_LOAD_COMPANYWITHALLDETAILS.size()==0)
		{
			System.out.println("Company with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select cmp_name,code,tg_line,address,company_pincode,company_city,company_state,srv_tax,pan_no,email,"
						+ "bnk_name,acc_no,ifc_code,condition1,condition2,condition3,condition4,condition5,condition6 from cmpdetail";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadCompanyBean loadCompanyBean=new LoadCompanyBean();
					loadCompanyBean.setCompanyName(rs.getString("cmp_name"));
					loadCompanyBean.setCompanyCode(rs.getString("code"));
					loadCompanyBean.setTagLine(rs.getString("tg_line"));
					loadCompanyBean.setAddress(rs.getString("address"));
					loadCompanyBean.setPincode(rs.getString("company_pincode"));
					loadCompanyBean.setCity(rs.getString("company_city"));
					loadCompanyBean.setState(rs.getString("company_state"));
					loadCompanyBean.setServiceTaxNo(rs.getString("srv_tax"));
					loadCompanyBean.setPanNo(rs.getString("pan_no"));
					loadCompanyBean.setEmail(rs.getString("email"));
					loadCompanyBean.setBankName(rs.getString("bnk_name"));
					loadCompanyBean.setAccountNo(rs.getString("acc_no"));
					loadCompanyBean.setIfscCode(rs.getString("ifc_code"));
					loadCompanyBean.setCondition1(rs.getString("condition1"));
					loadCompanyBean.setCondition2(rs.getString("condition2"));
					loadCompanyBean.setCondition3(rs.getString("condition3"));
					loadCompanyBean.setCondition4(rs.getString("condition4"));
					loadCompanyBean.setCondition5(rs.getString("condition5"));
					loadCompanyBean.setCondition6(rs.getString("condition6"));
					
					
					SET_LOAD_COMPANYWITHALLDETAILS.add(loadCompanyBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Company with name load by list...");
			
		}
	}

}
