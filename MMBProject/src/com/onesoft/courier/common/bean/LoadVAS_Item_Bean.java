package com.onesoft.courier.common.bean;

public class LoadVAS_Item_Bean {
	
	private String vasCode;
	private String vasName;
	private double vasAmount;
	
	
	public String getVasCode() {
		return vasCode;
	}
	public void setVasCode(String vasCode) {
		this.vasCode = vasCode;
	}
	public String getVasName() {
		return vasName;
	}
	public void setVasName(String vasName) {
		this.vasName = vasName;
	}
	public double getVasAmount() {
		return vasAmount;
	}
	public void setVasAmount(double vasAmount) {
		this.vasAmount = vasAmount;
	}

}
