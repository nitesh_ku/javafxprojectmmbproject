package com.onesoft.courier.common.bean;

public class LoadNetworkBean {
	
	private String networkCode;
	private String networkName;
	
	
	public String getNetworkCode() {
		return networkCode;
	}
	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	
	
	
}
