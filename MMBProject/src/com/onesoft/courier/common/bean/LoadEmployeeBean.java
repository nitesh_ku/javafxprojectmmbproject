package com.onesoft.courier.common.bean;

public class LoadEmployeeBean {
	
	private String employeeCode;
	private String employeeName;
	
	

	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

}
