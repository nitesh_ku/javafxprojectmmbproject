package com.onesoft.courier.common.bean;

public class LoadForwarderDetailsBean {
	
	private String forwarderCode;
	private String forwarderName;
	
	
	public String getForwarderCode() {
		return forwarderCode;
	}
	public void setForwarderCode(String forwarderCode) {
		this.forwarderCode = forwarderCode;
	}
	public String getForwarderName() {
		return forwarderName;
	}
	public void setForwarderName(String forwarderName) {
		this.forwarderName = forwarderName;
	}

}
