package com.onesoft.courier.common.bean;

public class LoadClientBean {
	
	private String branchcode;
	private String clientCode;
	private String clientName;
	private String type;
	private String client_city;
	private double air_dim;
	private String air_dim_formula;
	private double surface_dim;
	private String surface_dim_formula;
	private String address;
	private String pincode;
	private String client_gstin_number;
	
	private String issueRemark;
	
	
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClient_city() {
		return client_city;
	}
	public void setClient_city(String client_city) {
		this.client_city = client_city;
	}
	public double getAir_dim() {
		return air_dim;
	}
	public void setAir_dim(double air_dim) {
		this.air_dim = air_dim;
	}
	public double getSurface_dim() {
		return surface_dim;
	}
	public void setSurface_dim(double surface_dim) {
		this.surface_dim = surface_dim;
	}
	public String getSurface_dim_formula() {
		return surface_dim_formula;
	}
	public void setSurface_dim_formula(String surface_dim_formula) {
		this.surface_dim_formula = surface_dim_formula;
	}
	public String getAir_dim_formula() {
		return air_dim_formula;
	}
	public void setAir_dim_formula(String air_dim_formula) {
		this.air_dim_formula = air_dim_formula;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getClient_gstin_number() {
		return client_gstin_number;
	}
	public void setClient_gstin_number(String client_gstin_number) {
		this.client_gstin_number = client_gstin_number;
	}
	public String getIssueRemark() {
		return issueRemark;
	}
	public void setIssueRemark(String issueRemark) {
		this.issueRemark = issueRemark;
	}

}
