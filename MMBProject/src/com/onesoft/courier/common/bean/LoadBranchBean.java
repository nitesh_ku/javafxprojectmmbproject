package com.onesoft.courier.common.bean;

public class LoadBranchBean {
	
	private String branchCode;
	private String branchName;
	private String branchCity;
	private String cmpnyCode;
	private String branchPincode;
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCity() {
		return branchCity;
	}
	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}
	public String getCmpnyCode() {
		return cmpnyCode;
	}
	public void setCmpnyCode(String cmpnyCode) {
		this.cmpnyCode = cmpnyCode;
	}
	public String getBranchPincode() {
		return branchPincode;
	}
	public void setBranchPincode(String branchPincode) {
		this.branchPincode = branchPincode;
	}
	

}
