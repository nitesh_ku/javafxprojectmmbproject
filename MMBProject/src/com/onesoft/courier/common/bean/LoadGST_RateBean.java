package com.onesoft.courier.common.bean;

import java.sql.Date;

public class LoadGST_RateBean {
	
	private String tax_date;
	
	private Date gstEffectiveDate;
	
	private String tax1_cgst;
	private String tax2_sgst;
	private String tax3_igst;
	
	private double tax1_cgst_rate;
	private double tax2_sgst_rate;
	private double tax3_igst_rate;
	
	
	public String getTax_date() {
		return tax_date;
	}
	public void setTax_date(String tax_date) {
		this.tax_date = tax_date;
	}
	public String getTax1_cgst() {
		return tax1_cgst;
	}
	public void setTax1_cgst(String tax1_cgst) {
		this.tax1_cgst = tax1_cgst;
	}
	public String getTax2_sgst() {
		return tax2_sgst;
	}
	public void setTax2_sgst(String tax2_sgst) {
		this.tax2_sgst = tax2_sgst;
	}
	public String getTax3_igst() {
		return tax3_igst;
	}
	public void setTax3_igst(String tax3_igst) {
		this.tax3_igst = tax3_igst;
	}
	public double getTax1_cgst_rate() {
		return tax1_cgst_rate;
	}
	public void setTax1_cgst_rate(double tax1_cgst_rate) {
		this.tax1_cgst_rate = tax1_cgst_rate;
	}
	public double getTax2_sgst_rate() {
		return tax2_sgst_rate;
	}
	public void setTax2_sgst_rate(double tax2_sgst_rate) {
		this.tax2_sgst_rate = tax2_sgst_rate;
	}
	public double getTax3_igst_rate() {
		return tax3_igst_rate;
	}
	public void setTax3_igst_rate(double tax3_igst_rate) {
		this.tax3_igst_rate = tax3_igst_rate;
	}
/*	public String getGstEffectiveDate() {
		return gstEffectiveDate;
	}
	public void setGstEffectiveDate(String gstEffectiveDate) {
		this.gstEffectiveDate = gstEffectiveDate;
	}*/
	public Date getGstEffectiveDate() {
		return gstEffectiveDate;
	}
	public void setGstEffectiveDate(Date gstEffectiveDate) {
		this.gstEffectiveDate = gstEffectiveDate;
	}
	
	
}
