package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadSupplierBean;

public class LoadForwarderDetails {
	
	
	public static List<LoadForwarderDetailsBean> LIST_FORWARDER_DETAILS=new ArrayList<>();
	
	
	public void loadForwarderCodeWithName() throws SQLException
	{
		
		if(LIST_FORWARDER_DETAILS.size()==0)
		{
		
			System.out.println("Value getting from Set Value Using DB connection");
	
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select forwardercode,name from forwarder_details";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadForwarderDetailsBean bean=new LoadForwarderDetailsBean();
					bean.setForwarderCode(rs.getString("forwardercode"));
					bean.setForwarderName(rs.getString("name"));
					
					LIST_FORWARDER_DETAILS.add(bean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			/*System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");*/
			
		}
		
	}

}
