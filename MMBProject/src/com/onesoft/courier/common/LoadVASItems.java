package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;

import com.onesoft.courier.common.bean.LoadVAS_Item_Bean;

public class LoadVASItems {
	
public static List<LoadVAS_Item_Bean> SET_LOAD_VAS_WITH_NAME=new ArrayList<>();
	
	
	public void loadVASWithName() throws SQLException
	{
		
		if(SET_LOAD_VAS_WITH_NAME.size()==0)
		{
		
			System.out.println("Value getting from Set Value Using DB connection");
	
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select vas_code,vas_name from vas_master";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadVAS_Item_Bean bean=new LoadVAS_Item_Bean();
					bean.setVasCode(rs.getString("vas_code"));
					bean.setVasName(rs.getString("vas_name"));
					
					SET_LOAD_VAS_WITH_NAME.add(bean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			/*System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");*/
			
		}
		
	}



}
