package com.onesoft.courier.common;

import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.invoice.bean.InvoiceBean;

public class CommonVariable {
	
    public static String BRANCHCODE;
    public static String COMPANYCODE;
    public static String USERNAME;
    public static String BUSSINESSCODE;
    
    public static String LOGGED_IN_USER;
    public static String USER_ID;
	public static String USERID_AND_USERNAME;
	public static String USER_BRANCHCODE;
	public static String USER_BRANCH_CITY;
	public static String USER_BRANCH_PINCODE;
	public static String USER_IP_ADDRESS;
	public static String USER_HOST_NAME;
	
	//public static List<InvoiceBean> list_InvoiceData_Global=new ArrayList<>();
	

}
