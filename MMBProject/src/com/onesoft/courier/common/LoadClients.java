package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadClientBean;

public class LoadClients {

public static  final List<LoadClientBean> SETLOADCLIENTFORALL=new ArrayList<>();
public static  final List<LoadClientBean> SET_LOAD_CLIENTWITHNAME=new ArrayList<>();
	
	public void loadClient() throws SQLException
	{
		if(SETLOADCLIENTFORALL.size()==0)
		{
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			
					 
			try
			{		
				st=con.createStatement();
				String sql="select cm.code as clientcode,dbt.dailybookingtransaction2branch as branchcode from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code group by cm.code,dbt.dailybookingtransaction2branch";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadClientBean loadClientBean=new LoadClientBean();
					loadClientBean.setBranchcode(rs.getString("branchcode"));
					loadClientBean.setClientCode(rs.getString("clientcode"));
				
					SETLOADCLIENTFORALL.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
		}
	}
	
	
	public void loadClientWithName() throws SQLException
	{
		
		if(SET_LOAD_CLIENTWITHNAME.size()==0)
		{
			System.out.println("Client with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			
			
			try
			{		
				st=con.createStatement();
				String sql="select code, name, clientmaster2branch, city, dim_air, dim_surface, "
								+ "air_dim_formula, surface_dim_formula,pincode,address,gstin_number,client_type from clientmaster";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadClientBean loadClientBean=new LoadClientBean();
					loadClientBean.setClientName(rs.getString("name"));
					loadClientBean.setClientCode(rs.getString("code"));
					loadClientBean.setBranchcode(rs.getString("clientmaster2branch"));
					loadClientBean.setClient_city(rs.getString("city"));
					loadClientBean.setAir_dim(rs.getDouble("dim_air"));
					loadClientBean.setAir_dim_formula(rs.getString("air_dim_formula"));
					loadClientBean.setSurface_dim(rs.getDouble("dim_surface"));
					loadClientBean.setSurface_dim_formula(rs.getString("surface_dim_formula"));
					loadClientBean.setPincode(rs.getString("pincode"));
					loadClientBean.setAddress(rs.getString("address"));
					loadClientBean.setClient_gstin_number(rs.getString("gstin_number"));
					loadClientBean.setType(rs.getString("client_type"));
					
					SET_LOAD_CLIENTWITHNAME.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Client with name load by list...");
			
		}
	}
	
	
}
