package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.invoice.bean.InvoiceBean;

public class LoadInvoices {
	
	public static List<InvoiceBean> list_InvoiceData_Global=new ArrayList<>();
	//public static List<InvoiceBean> list_InvoiceInActiveData_Global=new ArrayList<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	public void loadInvoiceDate() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		//String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		int serialno=1;
		try
		{
			st=con.createStatement();
			//finalsql="select invoice_number,invoice_date,clientcode,from_date,to_date,grandtotal,remarks from invoice where invoice2branchcode='"+comboBoxBranchCode.getValue()+"' order by invoice_number";
			
			/*finalsql="select invoice_date,invoice_number,invoice2branchcode,clientcode,from_date,to_date,amount,fuel_rate,fuel,other_chrgs,insurance_amount,"
						+ "type_flat_percent,discount_Additional,rateof_percent,discount_additional_amount,reason,tax_name1,tax_rate1,tax_amount1,"
						+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,taxable_value,(tax_amount1+tax_amount2+tax_amount3) as gst_amount,"
						+ "grandtotal as total, remarks from invoice where invoice2branchcode='"+branchcode[0]+"' order by invoice_number DESC limit 50";*/
			
			finalsql="select invoice_date,invoice_number,invoice2branchcode,clientcode,from_date,to_date,amount,fuel_rate,fuel,other_chrgs,insurance_amount,"
					+ "type_flat_percent,discount_Additional,rateof_percent,discount_additional_amount,reason,tax_name1,tax_rate1,tax_amount1,"
					+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,taxable_value,(tax_amount1+tax_amount2+tax_amount3) as gst_amount,"
					+ "grandtotal as total, remarks,status from invoice order by invoice_number DESC";
			
			
			System.out.println("Invoice Table: "+finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					InvoiceBean inbean=new InvoiceBean();
					inbean.setSerieano(serialno);
					inbean.setInvoice_date(date.format(rs.getDate("invoice_date")));
					inbean.setInvoice_number(rs.getString("invoice_number"));
					inbean.setInvoice2branchcode(rs.getString("invoice2branchcode"));
					inbean.setClientcode(rs.getString("clientcode"));
					inbean.setFrom_date(date.format(rs.getDate("from_date")));
					inbean.setTo_date(date.format(rs.getDate("to_date")));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setFuel_rate(Double.parseDouble(df.format(rs.getDouble("fuel_rate"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("other_chrgs"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
					inbean.setType_flat2percent(rs.getString("type_flat_percent"));
					inbean.setDiscount_additional(rs.getString("discount_Additional"));
					inbean.setRateof_percent(Double.parseDouble(df.format(rs.getDouble("rateof_percent"))));
					inbean.setDiscount_Additional_amount(Double.parseDouble(df.format(rs.getDouble("discount_additional_amount"))));
					inbean.setReason(rs.getString("reason"));
					
					inbean.setCgst_Name(rs.getString("tax_name1"));
					inbean.setCgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate1"))));
					inbean.setCgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount1"))));

					inbean.setSgst_Name(rs.getString("tax_name2"));
					inbean.setSgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate2"))));
					inbean.setSgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount2"))));

					inbean.setIgst_Name(rs.getString("tax_name3"));
					inbean.setIgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate3"))));
					inbean.setIgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount3"))));
					
					inbean.setTaxable_amount(Double.parseDouble(df.format(rs.getDouble("taxable_value"))));
					inbean.setGst_amount(Double.parseDouble(df.format(rs.getDouble("gst_amount"))));
					inbean.setGrandtotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					inbean.setRemarks(rs.getString("remarks"));
					inbean.setStatus(rs.getString("status"));
					
					
					System.out.println("Invoice no: "+rs.getString("invoice_number")+" | GST Amt :: "+rs.getDouble("gst_amount")+" | Total GST Amt :: "+(rs.getDouble("tax_amount1")+rs.getDouble("tax_amount2")+rs.getDouble("tax_amount3")));
					
					LoadInvoices.list_InvoiceData_Global.add(inbean);
					
					
					serialno++;
					
				}
				while(rs.next());
				
				System.out.println(" \n++++++++++++++++++++++ From Load Invoice >>>>>>>>>>>>>>>");
				System.out.println(" Active List size >> "+list_InvoiceData_Global.size()+" | In Active list size >> "+list_InvoiceData_Global.size());
			}
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}

}
