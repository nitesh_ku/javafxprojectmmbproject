package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.SaveAddressBean;
import com.onesoft.courier.master.employee.bean.EmployeeRegistrationBean;
import com.onesoft.courier.sql.queries.MasterSQL_Address_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_Employee_Registration_Utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class SaveAddress {
	
	
	public static int addressCode_Global=0;
	
	
// *******************************************************************************	

	public int getAddressCode() throws SQLException
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int addressCode = 0;

		try {

			stmt = con.createStatement();
			sql = MasterSQL_Address_Utils.GET_SQL_ADDRESS_CODE;
			rs = stmt.executeQuery(sql);

			while (rs.next())
			{		
				addressCode=rs.getInt("addressid");
			}

			addressCode++;
			addressCode_Global=addressCode;

		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
		return addressCode;
	}

		
// *******************************************************************************
		

	public void saveAddress(String citycode,String phone,String add,String state,String country) throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		SaveAddressBean empBean=new SaveAddressBean();

		/*	if(!citycode.equals(""))
			{
				String[] city=citycode.replaceAll("\\s+","").split("\\|");
				empBean.setCity(city[1]);
			}*/
		empBean.setCity(citycode);
		empBean.setAddressId(getAddressCode());
		empBean.setPhone(phone);
		empBean.setAddress(add);
		empBean.setState(state);
		empBean.setCountry(country);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		try 
		{

			String query = MasterSQL_Address_Utils.INSERT_SQL_ADDRESS_DETAILS;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setInt(1, empBean.getAddressId());
			preparedStmt.setString(2, empBean.getPhone());
			preparedStmt.setString(3, empBean.getAddress());
			preparedStmt.setString(4, empBean.getCity());
			preparedStmt.setString(5, empBean.getState());
			preparedStmt.setString(6, empBean.getCountry());

			preparedStmt.executeUpdate();

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}		
		
		
// *******************************************************************************

	public void updateAddress(int addressid,String citycode,String phone,String add,String state,String country) throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		//String[] city=citycode.replaceAll("\\s+","").split("\\|");

		SaveAddressBean empBean=new SaveAddressBean();
		
		empBean.setAddressId(addressid);
		empBean.setCity(citycode);
		empBean.setPhone(phone);
		empBean.setAddress(add);
		empBean.setState(state);
		empBean.setCountry(country);
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			String query = MasterSQL_Address_Utils.UPDATE_SQL_EMP_ADDRESS_DETAILS;
			preparedStmt = con.prepareStatement(query);
				
			preparedStmt.setString(1, empBean.getPhone());
			preparedStmt.setString(2, empBean.getAddress());
			preparedStmt.setString(3, empBean.getCity());
			preparedStmt.setString(4, empBean.getState());
			preparedStmt.setString(5, empBean.getCountry());
			preparedStmt.setInt(6, empBean.getAddressId());
			
			preparedStmt.executeUpdate();
				
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}		
		

}
