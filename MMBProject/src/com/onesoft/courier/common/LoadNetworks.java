package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadNetworkBean;

public class LoadNetworks {
	
	public static  final Set<String> SET_LOAD_NETWORK_FOR_ALL=new HashSet<>();
	public static  final Set<LoadNetworkBean> SET_LOAD_NETWORK_WITH_NAME=new HashSet<>();
	
	public void loadAllNetworks() throws SQLException
	{
		
		if(SET_LOAD_NETWORK_FOR_ALL.size()==0)
		{
			System.out.println("Connection open...");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		Set<String> setNetworkFromDB = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code from vendormaster where vendor_type='N'";
			rs=st.executeQuery(sql);
		
			 while(rs.next())
			{
				SET_LOAD_NETWORK_FOR_ALL.add(rs.getString("code"));
			}
	
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			System.out.println("Connection Close...");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Direct all Set...");
		}
	}
	
// -----------------------------------------------------------------------
	
	public void loadAllNetworksWithName() throws SQLException
	{
		
		if(SET_LOAD_NETWORK_WITH_NAME.size()==0)
		{
		//System.out.println("Connection open...");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<LoadNetworkBean> setNetworkFromDB = new HashSet<LoadNetworkBean>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code,name from vendormaster where vendor_type='N'";
			rs=st.executeQuery(sql);
		
			 while(rs.next())
			{
				 LoadNetworkBean networkBean=new LoadNetworkBean();
				 
				 networkBean.setNetworkCode(rs.getString("code"));
				 networkBean.setNetworkName(rs.getString("name"));
				 
				SET_LOAD_NETWORK_WITH_NAME.add(networkBean);
			}
	
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			//System.out.println("Connection Close...");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Direct all Set...");
		}
	}
	

}
