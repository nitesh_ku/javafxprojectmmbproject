package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadItemBean;

public class LoadItem {

	
public static List<LoadItemBean> SET_LOADITEMSWITHNAME=new ArrayList<>();
	
	
	public void loadItemWithName() throws SQLException
	{	
		if(SET_LOADITEMSWITHNAME.size()==0)
		{
	
			System.out.println("Value getting from Set Value Using DB connection");
	
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select code,name from itemtype";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadItemBean bean=new LoadItemBean();
					bean.setItemCode(rs.getString("code"));
					bean.setItemName(rs.getString("name"));
					
					SET_LOADITEMSWITHNAME.add(bean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			/*System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");*/
			
		}
		
	}
	
}
