package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.sql.queries.MasterSQL_UploadPincode_Excel_Utils;

public class LoadPincodeForAll {
	
	public static List<LoadPincodeBean> list_Load_Pincode_from_Common=new ArrayList<>();
	public static Set<LoadPincodeBean> SET_LOAD_PINCODE=new HashSet<LoadPincodeBean>();
	public static Set<LoadPincodeBean> SET_LOAD_CITY=new HashSet<LoadPincodeBean>();
	public static Set<LoadPincodeBean> SET_LOAD_STATE=new HashSet<LoadPincodeBean>();
	
	public void loadPincode() throws SQLException
	{
		if(list_Load_Pincode_from_Common.size()==0)
		{
			System.err.println("Pincode details load from via DB");
			long startTime = System.nanoTime();
			

			
			ResultSet rs = null;
			Statement stmt = null;
			String sql = null;
			DBconnection dbcon = new DBconnection();
			Connection con = dbcon.ConnectDB();
	
			int slno = 1;
			try 
			{
				stmt = con.createStatement();
				sql = MasterSQL_UploadPincode_Excel_Utils.LOAD_TABLE_PINCODE_DATA_SHOW_ALL;
			
				System.out.println("SQL: " + sql);
				rs = stmt.executeQuery(sql);
	
				while (rs.next())
				{
					LoadPincodeBean pinBean = new LoadPincodeBean();
	
					pinBean.setSlno(slno);
					pinBean.setPincode(rs.getString("pincode"));
					pinBean.setCity_name(rs.getString("city_name"));
					pinBean.setState_code(rs.getString("state_code"));
					pinBean.setOda_Opa_Regular(rs.getString("oda_opa"));
					pinBean.setInternationa_Service(rs.getString("international_service"));
					pinBean.setDomestic_service(rs.getString("domestic_service"));
					pinBean.setCod_service(rs.getString("cod_serviceble"));
					pinBean.setZone_code(rs.getString("zone_code"));
					pinBean.setZone_air(rs.getString("zone_air"));
					pinBean.setZone_surface(rs.getString("zone_surface"));
					pinBean.setService_code(rs.getString("service_code"));
					pinBean.setState_gstin_code(rs.getString("state_gstin_code"));
					
					list_Load_Pincode_from_Common.add(pinBean);
					
					slno++;
				}
				
				for(LoadPincodeBean bean: list_Load_Pincode_from_Common) {
					LoadPincodeBean pincodeBean = new LoadPincodeBean();
					pincodeBean.setPincode(bean.getPincode());
					
					LoadPincodeBean cityBean = new LoadPincodeBean();
					cityBean.setCity_name(bean.getCity_name());
					
					LoadPincodeBean stateBean = new LoadPincodeBean();
					stateBean.setState_code(bean.getState_code());
					
					SET_LOAD_PINCODE.add(pincodeBean);
					SET_LOAD_CITY.add(cityBean);
					SET_LOAD_STATE.add(stateBean);
				}
				long endTime   = System.nanoTime();
		           long totalTime = endTime - startTime;
		           System.out.println("TIME -- > "+totalTime);
			}
	
			catch (Exception e) {
				System.out.println(e);
			}
	
			finally {
				dbcon.disconnect(null, stmt, rs, con);
			}
		}
		else
		{
			System.err.println("Now Pincode load from List");
		}
		
	}

}
