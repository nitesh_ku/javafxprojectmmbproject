package com.onesoft.courier.graph.controller;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.GraphFilterUtils;
import com.onesoft.courier.graph.bean.ClientVsZoneBean;
import com.onesoft.courier.graph.bean.StackBarChartBean;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;

public class StackBarChartController implements Initializable{
	
	List<String>list_clientCode=new ArrayList<>();
	List<StackBarChartBean> list_stackBarChartData=new ArrayList<>();
	
	@FXML
	private StackedBarChart<?,?> stackBarChar;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private Button btnRefresh;

	DecimalFormat df=new DecimalFormat(".##");
	

// =====================================================================	
	
	public void loadDataFromStackedBarChart() throws SQLException
	{
		list_clientCode.clear();
		list_stackBarChartData.clear();
		stackBarChar.getData().clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		
		String finalsql=null;
		try
		{
			st=con.createStatement();

			//finalsql="select dailybookingtransaction2client,zone_code, sum(total) as total from dailybookingtransaction where total is not null and zone_code is not null group by dailybookingtransaction2client,zone_code order by dailybookingtransaction2client";
			finalsql="select dailybookingtransaction2client,zone_code, sum(total) as total from dailybookingtransaction where "+GraphFilterUtils.STACKBARCHART_SQL+" group by dailybookingtransaction2client,zone_code order by dailybookingtransaction2client";
			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);

			if(!rs.next())
			{
				stackBarChar.getData().clear();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Data not found");
				alert.showAndWait();

			}
			else
			{	
				do
				{	
					StackBarChartBean sbcBean=new StackBarChartBean();
					
					sbcBean.setClientcode(rs.getString("dailybookingtransaction2client"));
					sbcBean.setZonecode(rs.getString("zone_code"));
					sbcBean.setAmount(Double.valueOf(df.format(rs.getDouble("total"))));
					
					System.out.println("Client :: "+sbcBean.getClientcode()+" | Zone :: "+sbcBean.getZonecode()+" | Amount :: "+sbcBean.getAmount());
					
					list_stackBarChartData.add(sbcBean);
					
					if(!list_clientCode.contains(rs.getString("dailybookingtransaction2client")))
					{
						list_clientCode.add(rs.getString("dailybookingtransaction2client"));
					}
				}
				while(rs.next());
			}
			
			generateStackBarChart();
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
// ================= Method for Stacked Bar Chart data =================
	
	public void generateStackBarChart()
	{
		stackBarChar.setTitle("All Clients and All Zone");

		for(String clientCode:list_clientCode)
		{
			XYChart.Series clientZoneData = new XYChart.Series();
			clientZoneData.setName(clientCode);
			for(StackBarChartBean bean: list_stackBarChartData)
			{
				if(bean.getClientcode().equals(clientCode))
				{
					clientZoneData.getData().add(new XYChart.Data(bean.getZonecode(),bean.getAmount()));
				}
			}
			stackBarChar.getData().add(clientZoneData);	
			System.out.println("Node :: "+clientZoneData.getData());//clientZoneData.getNode()
		}
		
		for (Series<?, ?> serie: stackBarChar.getData()){
            for (Data<?, ?> item: serie.getData()){
                item.getNode().setOnMouseEntered((MouseEvent event) -> {
                	Tooltip tooltip = new Tooltip();
					tooltip.setText("Client: "+serie.getName()+" | ₹ "+df.format(item.getYValue()));
					tooltip.install(item.getNode(),tooltip);
	                System.out.println("you clicked :: "+serie.getName()+" | Value :: "+item.getYValue());
                });
            }
        }
	}

// =====================================================================
	
	File mainFileLarge = new File("src/icon/refresh_icon_large.png");
	Image refresh_icon_large = new Image(mainFileLarge.toURI().toString());
	
	public void setRefreshGraphics()
	{
		File hoverRefreshSmall = new File("src/icon/refresh_icon_small.png");
		Image hoverImageSmall = new Image(hoverRefreshSmall.toURI().toString());
		btnRefresh.setGraphic(new ImageView(hoverImageSmall));
	}
	
	public void refreshIcon()
	{
		btnRefresh.setGraphic(new ImageView(refresh_icon_large));
	}
	
// =====================================================================
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
	
		try {
			loadDataFromStackedBarChart();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setRefreshGraphics();
	}

}
