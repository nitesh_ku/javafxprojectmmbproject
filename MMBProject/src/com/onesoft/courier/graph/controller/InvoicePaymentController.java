package com.onesoft.courier.graph.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.GraphFilterUtils;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.graph.bean.ClientVsZoneBean;
import com.onesoft.courier.graph.bean.ClientVsZoneTableBean;
import com.onesoft.courier.graph.bean.InvoicePaymentBean;
import com.onesoft.courier.graph.bean.InvoicePaymentTableBean;
import com.onesoft.courier.main.Main;

import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;


public class InvoicePaymentController  implements Initializable {
	
	
	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
	
	private ObservableList<InvoicePaymentTableBean> tabledata_InvoicePayment=FXCollections.observableArrayList();
	
	ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList(GraphFilterUtils.ALL_CLIENT);
	ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(GraphFilterUtils.ALL_BRANCH);
	
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<String,Double> barChar;
	
	@FXML
	private PieChart pieChart;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	

	
	@FXML
	private ComboBox<String> comboBoxClient;
	

	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExportToExcel;
	
	@FXML
	private Button btnExportToPDF;
	
	@FXML
	private Pagination pagination;
	
	
	@FXML
	private TableView<InvoicePaymentTableBean>  tableInvoicePaymentReport;
	
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, Integer> tabCol_serialno;

	@FXML
	private TableColumn<InvoicePaymentTableBean, String> tabCol_InvoiceNo;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, String> tabCol_InvoiceDate;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, String> tabCol_Branch;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, String> tabCol_client;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, Double> tabCol_Recieved;

	@FXML
	private TableColumn<InvoicePaymentTableBean, Double> tabCol_Adjustment;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, Double> tabCol_Due;
	
	@FXML
	private TableColumn<InvoicePaymentTableBean, Double> tabCol_GrandTotal;
	
	
	
	@FXML
	private CheckBox checkBoxDetails;
	
	
	
	
	// ***************************************************************************

		public void loadBranch() throws SQLException
		{
		
			LoadBranch branch=new LoadBranch();
			branch.loadBranchWithName();
			
			comboBoxBranchItems.clear();
			comboBoxBranchItems.add(GraphFilterUtils.ALL_BRANCH);
			comboBoxBranch.setValue(GraphFilterUtils.ALL_BRANCH);
			for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
			{	
				comboBoxBranchItems.add(branchBean.getBranchCode()+" | "+branchBean.getBranchName());
			}
			
			System.out.println("ALL branch list size : "+comboBoxBranchItems.size());
			
			comboBoxBranch.setItems(comboBoxBranchItems);
		}

	// ***************************************************************************

		public void loadClient() throws SQLException
		{
			LoadClients client=new LoadClients();
			client.loadClientWithName();
			comboBoxClientItems.clear();
			comboBoxClient.setValue(GraphFilterUtils.ALL_CLIENT);
			comboBoxClientItems.add(GraphFilterUtils.ALL_CLIENT);
			
			for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
			{
				comboBoxClientItems.add(clBean.getClientCode()+" | "+clBean.getClientName());
			}
			
			System.out.println("All client list size : "+ comboBoxClientItems.size());
			
			comboBoxClient.setItems(comboBoxClientItems);
		}

		
		
		public void showNetworkWiseData() throws SQLException, IOException
		{	
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			
			if(dpkFromDate.getValue()==null)
			{
				alert.setContentText("From Date field is Empty!");
				alert.showAndWait();
				dpkFromDate.requestFocus();
			}
			else if( dpkToDate.getValue()==null)
			{	
				alert.setContentText("To Date field is Empty!");
				alert.showAndWait();
				dpkToDate.requestFocus();
			}
			
			else
			{
				LocalDate from=dpkFromDate.getValue();
				LocalDate to=dpkToDate.getValue();
				Date stdate=Date.valueOf(from);
				Date edate=Date.valueOf(to);
				
				setFilterForTable(stdate, edate);
			}
		}
		
		@FXML
		public void loadData() throws SQLException, FileNotFoundException
		{	
		/*	pievalue=0.0;
			totalpievalue=0.0;*/
			
			//comboBoxClient.setValue(comboBoxClient.getValue());
			try
			{
				showNetworkWiseData();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		
		
		
		public void setFilterForTable(Date stdate,Date edate) throws SQLException, IOException
		{
			
			String sql="";
			
			String branchsql="";
			
			String clientsql="";
		
			
		
			

			String dateRange=" invoice_date between '"+ stdate+"' AND '"+edate+"'";
			
					
			if(!comboBoxBranch.getValue().equals(GraphFilterUtils.ALL_BRANCH))
			{
				String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
				branchsql="   invoice2branchcode='"+branchCode[0]+"' and";
			}
			
		
			
			if(!comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
			{
				String[] clientCode=comboBoxClient.getValue().replaceAll("\\s+","").split("\\|");
				clientsql=" clientcode='"+clientCode[0]+"' and";
			}
			
		
			
			sql=branchsql+""+clientsql+""+dateRange+" ";
			GraphFilterUtils.filterSql=branchsql+""+clientsql+""+dateRange+" ";
			System.out.println(" Filter sql: ====>> "+sql);
			
			//showGraph(sql);
			
			showPie(sql);
			
			showDetailTable(sql);
		
		
		}
		
		
//==================================================================================================	
	/*	
		public void showGraph(String sql) throws SQLException
		{
			XYChart.Series<String,Double> series1 = new XYChart.Series<>();
			
			
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectMMB_oldDB();

			Statement st=null;
			ResultSet rs=null;
			
			String paidAmt=null;
			
			String adjustment=null;
			
			String unpaid=null;
			
		//	ZoneBean netbean=new ZoneBean();
			
			InvoicePaymentBean ipBean=new InvoicePaymentBean();
			
			String finalsql=null;

			try
			{
				 
				    
				    st=con.createStatement();
				
			
					finalsql="select SUM(amtreceived) as paid_amt ,SUM( tds_amount+db_note+adjustment) as adjustment ,SUM(due) as unpaid_amt from invoice where "+sql+" ";
				
				


				System.out.println(finalsql);
				rs=st.executeQuery(finalsql);
                  
			
				if(!rs.next())
				{
					barChar.getData().clear();
					pcdata.clear();
					

					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Database Alert");
					alert.setHeaderText(null);
					alert.setContentText("Data not found");
					alert.showAndWait();

				}
				else
				{	
					do
					{	
						
							//ipBean.setClientCode(rs.getString("dailybookingtransaction2client"));
							ipBean.setPaidAmount(Double.parseDouble(df.format(rs.getDouble("paid_amt"))));
							
							ipBean.setAdjustmentAmt(Double.parseDouble(df.format(rs.getDouble("adjustment"))));
							
							ipBean.setUnpaidAmount(Double.parseDouble(df.format(rs.getDouble("unpaid_amt"))));
							
							paidAmt="Received";
							
							adjustment="Adjustment";
							
							unpaid="Due";
						
							
							series1.getData().add(new XYChart.Data(paidAmt,ipBean.getPaidAmount()));
							
							series1.getData().add(new XYChart.Data(adjustment,ipBean.getAdjustmentAmt()));
							
							series1.getData().add(new XYChart.Data(unpaid,ipBean.getUnpaidAmount()));
							
							barChar.setTitle("Invoice payment report");
							
							series1.setName("Payment Status");
							
							
					
					}
					while(rs.next());

					barChar.getData().clear();
					barChar.getData().add( series1);
					
			
						
						
						
					}
				
				
				
			
				
					
					
				for(XYChart.Series<String,Double> s:barChar.getData()){
					for(XYChart.Data<String, Double> d:s.getData()){
					d.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event->{
						
						if(event.getClickCount()==2){
							
							System.err.println("BarChart Y-axis data :"+d.getYValue());
							
							System.err.println("BarChart X-axis data :"+d.getXValue());
							
							GraphFilterUtils.paymentStatus=d.getXValue();
							
							
							Main m=new Main();
							try {
								m.showInvoiceVsClientReport();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
			
					});	
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}

			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}

		
*/		

		

		
		
		
//------------------------------------------------ Method for Pie Chart data ---------------------------------------

		public void showPie(String sql) throws SQLException
		{
			pievalue=0;
			totalpievalue=0;
			pcdata.clear();
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			//Connection con=dbcon.ConnectDB();

			Statement st=null;
			ResultSet rs=null;
			String finalsql=null;

			ArrayList<InvoicePaymentBean> list=new ArrayList<InvoicePaymentBean>();
			try
			{
				pcdata.clear();
				st=con.createStatement();
				
				
				/*if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))*/
			
					finalsql="select  SUM(grandtotal) as grandtotal, SUM(amtreceived) as paid_amt ,SUM( tds_amount+db_note+adjustment) as adjustment ,SUM(due) as unpaid_amt from invoice where "+sql+" ";
				
				

			    	System.out.println("from pie chart: "+finalsql);
				    
			    	
			    	rs=st.executeQuery(finalsql);
				
				
				
			
				
				if(! rs.next())
				{
			
                    pcdata.clear();
					

					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Database Alert");
					alert.setHeaderText(null);
					alert.setContentText("Data not found");
					alert.showAndWait();  
				}
				else
				{
					System.out.println("Test1");
					
					
					do
					{
						
					/*	System.out.println("Row of the result set :"+rs.getRow());
						
						System.out.println(rs.getDouble("paid_amt"));
						System.out.println(rs.getDouble("adjustment"));
						System.out.println(rs.getDouble("unpaid_amt"));
						System.out.println(rs.getDouble("grandtotal"));
						*/
						
						if(rs.getDouble("grandtotal")!=0.0){
						
							if(rs.getDouble("paid_amt")!=0.0 && rs.getDouble("adjustment")!=0.0 && rs.getDouble("unpaid_amt")!=0.0)
							{
						    InvoicePaymentBean ipBean=new InvoicePaymentBean();
					
							pieChart.setTitle("Invoice payment report");
							
							ipBean.setPaidAmount(Double.parseDouble(df.format(rs.getDouble("paid_amt"))));
							
							ipBean.setAdjustmentAmt(Double.parseDouble(df.format(rs.getDouble("adjustment"))));
							
							ipBean.setUnpaidAmount(Double.parseDouble(df.format(rs.getDouble("unpaid_amt"))));
							
							ipBean.setGrandTotal(Double.parseDouble(df.format(rs.getDouble("grandtotal"))));
						
							
							 list.add(ipBean);
							}
				
						}
					
					
					}
					while(rs.next());
					System.out.println("List size :: "+list.size());
					
					if(list.size()!=0){
					
					for(InvoicePaymentBean bean: list)
					{	
					
					
							pcdata.add(new PieChart.Data("Received ("+String.valueOf(df.format((bean.getPaidAmount()/bean.getGrandTotal())*100) + "%)"), bean.getPaidAmount()));
						
							pcdata.add(new PieChart.Data("Adjustment ("+String.valueOf(df.format((bean.getAdjustmentAmt()/bean.getGrandTotal())*100) + "%)"), bean.getAdjustmentAmt()));
						
							pcdata.add(new PieChart.Data("Due ("+String.valueOf(df.format((bean.getUnpaidAmount()/bean.getGrandTotal())*100) + "%)"), bean.getUnpaidAmount()));
						
					
					}
					
					
					}
					else{
						
						   pcdata.clear();
							
						   btnExportToExcel.setVisible(false);
						   btnExportToPDF.setVisible(false);
						   
						   tabledata_InvoicePayment.clear();
						   
						   tableInvoicePaymentReport.setItems(null);
						   

							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Database Alert");
							alert.setHeaderText(null);
							alert.setContentText("Data not found");
							alert.showAndWait(); 	
						
					}

					pieChart.setData(pcdata);
					
				
					
					pcdata.stream().forEach(pieData -> {
						pieData.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
							String[]  sliceName=pieData.getName().replaceAll("\\s+","").split("\\(");
							
							Tooltip tooltip = new Tooltip();
							tooltip.setText( sliceName[0]+" | ₹ "+df.format(pieData.getPieValue()));
						    Tooltip.install(pieData.getNode(), tooltip);
							
						});
						
						
					});

					
					pcdata.stream().forEach(pieData -> {
						
					
						pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
							
							
							if (event.getClickCount() == 1)
							{
								
								
								Bounds b1 = pieData.getNode().getBoundsInLocal();
								double newX = (b1.getWidth()) / 2 + b1.getMinX();
								double newY = (b1.getHeight()) / 2 + b1.getMinY();
								// Make sure pie wedge location is reset
								pieData.getNode().setTranslateX(0);
								pieData.getNode().setTranslateY(0);
								TranslateTransition tt = new TranslateTransition(
										Duration.millis(1500), pieData.getNode());
								tt.setByX(newX);
								tt.setByY(newY);
								tt.setAutoReverse(true);
								tt.setCycleCount(2);
								tt.play();
								 					
							}
							else if(event.getClickCount() == 2)
							{
								
								String[]  sliceName=pieData.getName().replaceAll("\\s+","").split("\\(");
								
								GraphFilterUtils.paymentStatus=sliceName[0].trim();
							
								Main m=new Main();
								try {

									m.showInvoiceVsClientReport();
									
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
					});
					
				
						


					
			
				}
				}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		
		
		
		
		//------------------------------------------------ Method for Show Table data ---------------------------------------
		
		public void showDetailTable(String sql) throws SQLException
		{
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			//Connection con=dbcon.ConnectDB();

			ResultSet rs=null;
			Statement st=null;
			String finalsql=null;

			try
			{
				tabledata_InvoicePayment.clear();
				
				st=con.createStatement();
				
				finalsql="select invoice_number,invoice_date,invoice2branchcode, clientcode, SUM(amtreceived) as received ,SUM( tds_amount+db_note+adjustment) as adjustment ,SUM(due) as due , SUM(grandtotal) as grandtotal from invoice "
						+"where "+sql+"  group by invoice_number,invoice_date,clientcode,invoice2branchcode";
				
				System.out.println("Final sql after filters: "+finalsql);

				rs=st.executeQuery(finalsql);
				
				int serialno=1;

				if(!rs.next())
				{
					pieChart.getData().clear();
					//barChar.getData().clear();
					pagination.setVisible(false);
					btnExportToExcel.setVisible(false);
					btnExportToPDF.setVisible(false);
					pcdata.clear();
					tableInvoicePaymentReport.getItems().clear();
				}
				else
				{
					pagination.setVisible(true);
					btnExportToExcel.setVisible(true);
					btnExportToPDF.setVisible(true);
					do
					{
						InvoicePaymentTableBean ipBean=new InvoicePaymentTableBean();

						ipBean.setSrNo(serialno);
						ipBean.setInvoiceNo(rs.getString("invoice_number"));
						
						ipBean.setInvoiceDate(date.format(rs.getDate("invoice_date")));
						
						ipBean.setClient(rs.getString("clientcode"));
						ipBean.setBranch(rs.getString("invoice2branchcode"));
					
						
						ipBean.setReceived(Double.parseDouble(df.format(rs.getDouble("received"))));
						ipBean.setAdjustment(Double.parseDouble(df.format(rs.getDouble("adjustment"))));
						ipBean.setDue(Double.parseDouble(df.format(rs.getDouble("due"))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
						ipBean.setTotalAmt(Double.parseDouble(df.format(rs.getDouble("grandtotal"))));

						tabledata_InvoicePayment.add(ipBean);
						serialno++;
					}
					while (rs.next());

					tableInvoicePaymentReport.setItems(tabledata_InvoicePayment);
					pagination.setPageCount((tabledata_InvoicePayment.size() / rowsPerPage() + 1));
					pagination.setCurrentPageIndex(0);
					pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
					
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}	
		
	// ***************************************************************************	
		
		public int itemsPerPage()
		{
			return 1;
		}

		public int rowsPerPage() 
		{
			return 10;
		}

		public GridPane createPage(int pageIndex)
		{
			int lastIndex = 0;
		
			GridPane pane=new GridPane();
			int displace = tabledata_InvoicePayment.size() % rowsPerPage();
	            
				if (displace >= 0)
				{
					lastIndex = tabledata_InvoicePayment.size() / rowsPerPage();
				}
				/*else
				{
					lastIndex = tabledata.size() / rowsPerPage() -1;
					System.out.println("Pagination lastindex from else condition: "+lastIndex);
				}*/
		  
		        
				int page = pageIndex * itemsPerPage();
		        for (int i = page; i < page + itemsPerPage(); i++)
		        {
		            if (lastIndex == pageIndex)
		            {
		                tableInvoicePaymentReport.setItems(FXCollections.observableArrayList(tabledata_InvoicePayment.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
		            }
		            else
		            {
		                tableInvoicePaymentReport.setItems(FXCollections.observableArrayList(tabledata_InvoicePayment.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
		            }
		        }
		       return pane;
		      
		    }
		
	
		
		// ***************************************************************************	
		
		@FXML
		public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
		{

			Document my_pdf_report = new Document();
			
			//String pdffile="E:/NetwrokReport.pdf";
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
			File file = fc.showSaveDialog(null);
			
			if(file !=null){
			
			PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
	        my_pdf_report.open();            
	        
	        PdfPTable my_report_table = new PdfPTable(9);
	        
	        my_report_table.setWidthPercentage(100);
	        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
	        
	        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
	        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
	      
	      //create a cell object
	       
	        PdfPCell table_cell = null;
	            
	       	table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
	       	table_cell.setBorder(Rectangle.NO_BORDER);
	       	my_report_table.addCell(table_cell);
	     
	       	table_cell=new PdfPCell(new Phrase("Invoice No",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      	
	      	table_cell=new PdfPCell(new Phrase("Invoice Date",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      	
	      	table_cell=new PdfPCell(new Phrase("Branch",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	     
	      	table_cell=new PdfPCell(new Phrase("Client",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      	 
	      	table_cell=new PdfPCell(new Phrase("Received",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      	
	      	table_cell=new PdfPCell(new Phrase("Adjustment",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      
	      	table_cell=new PdfPCell(new Phrase("Due",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	     
	     
	      	table_cell=new PdfPCell(new Phrase("Total",headingfont));
	      	table_cell.setBorder(Rectangle.NO_BORDER);
	      	my_report_table.addCell(table_cell);
	      
	     
	      	/*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
	        my_report_table.addCell(new Phrase("Booking Date",tfont));
	        my_report_table.addCell(new Phrase("Srvc Grp",tfont));
	      	 */
	      
	        for(InvoicePaymentTableBean fxBean: tabledata_InvoicePayment)
			{
	        	  
				/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
	        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
	        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
	        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
	        	*/

	        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSrNo()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	             
	            table_cell=new PdfPCell(new Phrase(fxBean.getInvoiceNo(),tfont));
	            table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	        	table_cell=new PdfPCell(new Phrase(fxBean.getInvoiceDate(),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBranch()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getClient()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	              table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getReceived()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getAdjustment()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getDue()),tfont));
	            table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	            
	       
	            
	            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getTotalAmt()),tfont));
	        	table_cell.setBorder(Rectangle.NO_BORDER);
	            my_report_table.addCell(table_cell);
	        	 
	            table_cell.setBorder(0);
			
			}
	     
	        my_pdf_report.add(my_report_table);
	        System.out.println("file chooser: "+file.getAbsoluteFile());
	           
	        System.out.println("File "+file.getName()+" successfully saved");
	         my_pdf_report.close();
		}	
		}
		
		
	
		
		//--------------------------------------------------------------------------
		
		@FXML
		public void exportToExcel() throws SQLException, IOException
		{
				
			HSSFWorkbook workbook=new HSSFWorkbook();
			HSSFSheet sheet=workbook.createSheet("Sales Data");
			HSSFRow rowhead=sheet.createRow(0);
			
			rowhead.createCell(0).setCellValue("Serial No");
			rowhead.createCell(1).setCellValue("Invoice No");
			rowhead.createCell(2).setCellValue("Invoice Date");
			rowhead.createCell(3).setCellValue("Branch");
			rowhead.createCell(4).setCellValue("Client");
			rowhead.createCell(5).setCellValue("Received");		
			rowhead.createCell(6).setCellValue("Adjustment");
			rowhead.createCell(7).setCellValue("Due");
			
			rowhead.createCell(8).setCellValue("Total");
					
			int i=2;
			
			HSSFRow row;
			
			for(InvoicePaymentTableBean ipBean: tabledata_InvoicePayment)
			{
				row = sheet.createRow((short) i);

				row.createCell(0).setCellValue(ipBean.getSrNo());
				row.createCell(1).setCellValue(ipBean.getInvoiceNo());
				row.createCell(2).setCellValue(ipBean.getInvoiceDate());
				row.createCell(3).setCellValue(ipBean.getBranch());
				row.createCell(4).setCellValue(ipBean.getClient());
		
				
				row.createCell(5).setCellValue(ipBean.getReceived());
				row.createCell(6).setCellValue(ipBean.getAdjustment());
				row.createCell(7).setCellValue(ipBean.getDue());
				row.createCell(8).setCellValue(ipBean.getTotalAmt());

				i++;
			}
				
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
			File file = fc.showSaveDialog(null);
			if(file !=null){
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			System.out.println("file chooser: "+file.getAbsoluteFile());
			
			workbook.write(fileOut);
			fileOut.close();
			
			System.out.println("File "+file.getName()+" successfully saved");
			}  
		}	
		
		
	// ***************************************************************************
		
		
		
		
		
		
		
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		dpkFromDate.setEditable(false);
		
		dpkToDate.setEditable(false);
		
		pagination.setVisible(false);
		btnExportToExcel.setVisible(false);
		btnExportToPDF.setVisible(false);
		
		
		
		tabCol_serialno.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,Integer>("srNo"));
		
		tabCol_InvoiceNo.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,String>("invoiceNo"));
		
		tabCol_InvoiceDate.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,String>("invoiceDate"));
		
		tabCol_Branch.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,String>("branch"));
		
		tabCol_client.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,String>("client"));
		
		tabCol_Recieved.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,Double>("received"));
		
		tabCol_Adjustment.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,Double>("adjustment"));
		
		tabCol_Due.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,Double>("due"));
		
		tabCol_GrandTotal.setCellValueFactory(new PropertyValueFactory<InvoicePaymentTableBean,Double>("totalAmt"));
		
		
		
		
		
		try {
			loadBranch();
			loadClient();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	

}
