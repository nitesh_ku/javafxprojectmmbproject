package com.onesoft.courier.graph.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.w3c.dom.ls.LSInput;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.GraphFilterUtils;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadZoneBean;
import com.onesoft.courier.graph.bean.ClientVsZoneBean;
import com.onesoft.courier.graph.bean.ClientVsZoneTableBean;
import com.onesoft.courier.main.Main;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class ClientVsZoneController implements Initializable{

	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
	private ObservableList<ClientVsZoneTableBean> tabledata_ClientVsZone=FXCollections.observableArrayList();
	ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList(GraphFilterUtils.ALL_CLIENT);
	ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(GraphFilterUtils.ALL_BRANCH);
	ObservableList<String> comboBoxZoneItems=FXCollections.observableArrayList(GraphFilterUtils.ALL_ZONE);
	ObservableList<String> comboBoxModeItems=FXCollections.observableArrayList(GraphFilterUtils.MODE_AIR,GraphFilterUtils.MODE_SURFACE);
	ObservableList<String> comboBoxDNDItems=FXCollections.observableArrayList(GraphFilterUtils.DND,GraphFilterUtils.DOX,GraphFilterUtils.NONDOX);
	ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(GraphFilterUtils.TYPE_BOTH,GraphFilterUtils.TYPE_CASH,GraphFilterUtils.TYPE_CREDIT);

	List<String> list_Zone = new ArrayList<>();
	
/*	List<String> list_ZoneAir = new ArrayList<>();
	List<String> list_ZoneSurface = new ArrayList<>();*/
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> barChar;
	
	@FXML
	private PieChart pieChart;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxDoxNonDox;
	
	/*@FXML
	private ComboBox<String> comboBoxMode;*/
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private ComboBox<String> comboBoxZone;
	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExportToExcel;
	
	@FXML
	private Button btnExportToPDF;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private CheckBox checkBoxDetails;
	
	
// ***************************************************************************	
	
	@FXML
	private TableView<ClientVsZoneTableBean> tableClientVsZone;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Integer> tabCol_serialno;

	@FXML
	private TableColumn<ClientVsZoneTableBean, String> tabCol_ClientCode;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, String> tabCol_ZoneCode;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Integer> tabCol_Packets;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_BillingWeight;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_InsuranceAmount;

	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_DocketCharge;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_VasTotal;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_BasicAmount;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_FuelAmount;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_GstTotal;
	
	@FXML
	private TableColumn<ClientVsZoneTableBean, Double> tabCol_GrandTotal;
	
	
// ***************************************************************************

	public void loadBranch()
	{
		comboBoxBranchItems.clear();
		comboBoxBranchItems.add(GraphFilterUtils.ALL_BRANCH);
		comboBoxBranch.setValue(GraphFilterUtils.ALL_BRANCH);
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{	
			comboBoxBranchItems.add(branchBean.getBranchCode()+" | "+branchBean.getBranchName());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
	}

// ***************************************************************************

	public void loadClient()
	{
		comboBoxClientItems.clear();
		comboBoxClient.setValue(GraphFilterUtils.ALL_CLIENT);
		comboBoxClientItems.add(GraphFilterUtils.ALL_CLIENT);
		
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			comboBoxClientItems.add(clBean.getClientCode()+" | "+clBean.getClientName());
		}
		comboBoxClient.setItems(comboBoxClientItems);
	}
	
// ***************************************************************************

	public void loadZone()
	{
		comboBoxZoneItems.clear();
		list_Zone.clear();
		comboBoxZoneItems.add(GraphFilterUtils.ALL_ZONE);
		
		for(LoadZoneBean zoneBean:LoadZone.SET_LOAD_ZONE_WITH_NAME)
		{
			list_Zone.add(zoneBean.getZoneCode());
		}
		
		Collections.sort(list_Zone);
		
		
		for(String zoneCode:list_Zone)
		{
			comboBoxZoneItems.add(zoneCode);
		}
		
		comboBoxZone.setItems(comboBoxZoneItems);
	}
	
	/*public void loadZoneViaMode()
	{
		comboBoxZoneItems.clear();
		comboBoxZoneItems.add(GraphFilterUtils.ALL_ZONE);
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(comboBoxMode.getValue().equals(GraphFilterUtils.MODE_AIR))
			{
				if(!list_ZoneAir.contains(pinBean.getZone_air()) && pinBean.getZone_air()!=null)
				{
					list_ZoneAir.add(pinBean.getZone_air());	
				}
			}
			else
			{
				if(!list_ZoneSurface.contains(pinBean.getZone_air()) && pinBean.getZone_surface()!=null)
				{
					list_ZoneSurface.add(pinBean.getZone_air());	
				}
			}	
		}
	
		Collections.sort(list_ZoneAir);
		Collections.sort(list_ZoneSurface);
		
		if(comboBoxMode.getValue().equals(GraphFilterUtils.MODE_AIR))
		{
			for(String zoneAir:list_ZoneAir)
			{
				//System.out.println("Zone air >> "+zoneAir);
				comboBoxZoneItems.add(zoneAir);
			}
		}
		else
		{
			for(String zoneSurface:list_ZoneSurface)
			{
				//System.out.println("Zone air >> "+zoneSurface);
				comboBoxZoneItems.add(zoneSurface);
			}
		}
		
		comboBoxZone.setItems(comboBoxZoneItems);
	}	*/

// ***************************************************************************		

	public void setZoneFirstValueViaClient()
	{
		if(Main.popUpWindowStage!=null)
		{
		Main.popUpWindowStage.close();
		}
		
		/*if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
		{
			comboBoxZone.setValue(comboBoxZoneItems.get(1));
		}
		else
		{
			comboBoxZone.setValue(GraphFilterUtils.ALL_ZONE);
		}*/
	}
	
	
// ***************************************************************************	

	public void showNetworkWiseData() throws SQLException, IOException
	{	
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty Field Validation");
		alert.setHeaderText(null);
		
		if(dpkFromDate.getValue()==null)
		{
			alert.setContentText("From Date field is Empty!");
			alert.showAndWait();
			dpkFromDate.requestFocus();
		}
		else if( dpkToDate.getValue()==null)
		{	
			alert.setContentText("To Date field is Empty!");
			alert.showAndWait();
			dpkToDate.requestFocus();
		}
		/*else if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT) && comboBoxZone.getValue().equals(GraphFilterUtils.ALL_ZONE))
		{
			alert.setContentText("Client and Zone both fields could not be 'ALL'!\nPlease select single client or Zone in one of them except 'ALL'");
			alert.showAndWait();
			comboBoxClient.requestFocus();
		}*/
		else
		{
			LocalDate from=dpkFromDate.getValue();
			LocalDate to=dpkToDate.getValue();
			Date stdate=Date.valueOf(from);
			Date edate=Date.valueOf(to);
			
			setFilterForTable(stdate, edate);
		}
	}
	
	@FXML
	public void loadData() throws SQLException, FileNotFoundException
	{	
	
		try
		{
			showNetworkWiseData();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
		
// ***************************************************************************
	
	public void setFilterForTable(Date stdate,Date edate) throws SQLException, IOException
	{
		tabledata_ClientVsZone.clear();
		
		String sql="";
		String branchsql="";
		String typesql="";
		String clientsql="";
		String dndsql="";
		String modesql="";
		String zonesql="";
		String dnd="";

		String dateRange=" booking_date between '"+ stdate+"' AND '"+edate+"'";
		
				
		if(!comboBoxBranch.getValue().equals(GraphFilterUtils.ALL_BRANCH))
		{
			String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
			branchsql=" dailybookingtransaction2branch='"+branchCode[0]+"' and";
		}
		
	/*	if(comboBoxType.getValue().equals(GraphFilterUtils.TYPE_CASH))
		{
			typesql=" client_type='T' and";
		}
		else if(comboBoxType.getValue().equals(GraphFilterUtils.TYPE_CREDIT))
		{
			typesql=" client_type='F' and";
		}*/
		
		if(comboBoxDoxNonDox.getValue().equals(GraphFilterUtils.DOX))
		{
			dndsql=" dox_nondox='D' and";
		}
		else if(comboBoxDoxNonDox.getValue().equals(GraphFilterUtils.NONDOX))
		{
			dndsql=" dox_nondox='N' and";
		}
		
		if(!comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
		{
			String[] clientCode=comboBoxClient.getValue().replaceAll("\\s+","").split("\\|");
			clientsql=" dailybookingtransaction2client='"+clientCode[0]+"' and";
		}
		
		if(!comboBoxZone.getValue().equals(GraphFilterUtils.ALL_ZONE))
		{
			zonesql=" zone_code='"+comboBoxZone.getValue()+"' and";
		}
	
		//modesql=comboBoxMode.getValue();
		
		sql=branchsql+""+typesql+""+zonesql+""+clientsql+""+dndsql+""+dateRange+" and total is not null ";
		
		
		if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT) && comboBoxZone.getValue().equals(GraphFilterUtils.ALL_ZONE))
		{
			
			barChar.getData().clear();
			pieChart.getData().clear();
			
			
			GraphFilterUtils.STACKBARCHART_SQL=sql+"and zone_code is not null";
			new Main().showStackedBarChartViaClientVsZone();
			System.out.println("All zone and client====>> "+GraphFilterUtils.STACKBARCHART_SQL);	
		}
		else
		{
			showGraph(sql);
			showPie(sql);
			showDetailTable(sql);
			System.out.println("normal sql: ====>> "+sql);
		}
	
	}


// ------------------------------------------------ Method for Bar Chart data ---------------------------------------
	
	public void showGraph(String sql) throws SQLException
	{
		XYChart.Series series1 = new XYChart.Series();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
	//	ZoneBean netbean=new ZoneBean();
		ClientVsZoneBean czBean=new ClientVsZoneBean();
		
		String finalsql=null;

		try
		{
			//barChar.setTitle("Networks report between" +stdate+" to "+edate);
			st=con.createStatement();
			
			if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
			{
				finalsql="select SUM(total) as total, dailybookingtransaction2client from dailybookingtransaction where "+sql+" group by dailybookingtransaction2client";
			}
			else
			{
				finalsql="select SUM(total) as total, zone_code from dailybookingtransaction where "+sql+" group by zone_code";
			}
			//finalsql="select SUM(total) as total, zone_code from dailybookingtransaction where dailybookingtransaction2client='55555' group by zone_code,total";
			
			/*String finalsql="select vnd.code,sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code "
					+ "and vnd.vendor_type='N' "+sql+" Group by vnd.code";	*/


			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);

			if(!rs.next())
			{
				barChar.getData().clear();
				pcdata.clear();
				tabledata_ClientVsZone.clear();

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Data not found");
				alert.showAndWait();

			}
			else
			{	
				do
				{	
					if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
					{
						czBean.setClientCode(rs.getString("dailybookingtransaction2client"));
						czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						series1.getData().add(new XYChart.Data(czBean.getClientCode(),czBean.getTotal()));
						barChar.setTitle("ALL Clients Data");
						
						series1.setName("Clients");
						
					}
					else
					{
						if(rs.getString("zone_code")!=null)
						{
						barChar.setTitle("ALL Zone Data");
						czBean.setZoneCode(rs.getString("zone_code"));
						czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						series1.getData().add(new XYChart.Data(czBean.getZoneCode(),czBean.getTotal()));
						series1.setName("Zone");
						}
					}
				}
				while(rs.next());

				barChar.getData().clear();
				barChar.getData().add(series1);
				
				for (Series<?, ?> serie: barChar.getData()){
		            for (Data<?, ?> item: serie.getData()){
		                item.getNode().setOnMousePressed((MouseEvent event) -> {
		                    System.out.println("you clicked "+item.toString()+serie.toString());
		                });
		            }
		        }
				
				/* Timeline tl = new Timeline();
			        tl.getKeyFrames().add(new KeyFrame(Duration.millis(500), 
			            new EventHandler<ActionEvent>() {
			                @Override public void handle(ActionEvent actionEvent) {
			                for (Series<?, ?> series : barChar.getData()) {
			                    for (Data<?, ?> data : series.getData()) {
			                        data.setExtraValue(Math.random() * 100);
			                    }
			                }
			            }
			        }));
			        tl.setCycleCount(Animation.INDEFINITE);
			        tl.play();*/
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}

			
//------------------------------------------------ Method for Pie Chart data ---------------------------------------

	public void showPie(String sql) throws SQLException
	{
		pievalue=0;
		totalpievalue=0;
		pcdata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		String finalsql=null;

		ArrayList<ClientVsZoneBean> list=new ArrayList<ClientVsZoneBean>();
		try
		{
			pcdata.clear();
			st=con.createStatement();
			/*String finalsql="select vnd.code,sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code "
					+ "and vnd.vendor_type='N' "+sql+" Group by vnd.code";	*/
			
			if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
			{
				finalsql="select SUM(total) as total, dailybookingtransaction2client from dailybookingtransaction where "+sql+" group by dailybookingtransaction2client";
			}
			else
			{
				finalsql="select SUM(total) as total, zone_code from dailybookingtransaction where "+sql+" group by zone_code";
			}
			

			System.out.println("from pie chart: "+finalsql);
			rs=st.executeQuery(finalsql);
			if(!rs.next())
			{
				//btnexcel.setVisible(false);
			//	btnExportPDF.setVisible(false);
				tabledata_ClientVsZone.clear();
			}
			else
			{
				//btnexcel.setVisible(true);
				//btnExportPDF.setVisible(true);
				do
				{
					ClientVsZoneBean czBean=new ClientVsZoneBean();
					
					if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
					{
						pieChart.setTitle("ALL Clients Data");
						czBean.setClientCode(rs.getString("dailybookingtransaction2client"));
						czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						pievalue=pievalue+czBean.getTotal();
						totalpievalue=totalpievalue+czBean.getTotal();
						
						list.add(czBean);
						
					}
					else
					{
						
						
						pieChart.setTitle("ALL Zone Data");
						/*if(rs.getString("zone_code")!=null)
						{*/
							
							czBean.setZoneCode(rs.getString("zone_code"));
							czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
							System.err.println("if Zone code :: "+czBean.getZoneCode()+" | Amount :: "+czBean.getTotal());
							pievalue=pievalue+czBean.getTotal();
							totalpievalue=totalpievalue+czBean.getTotal();
					/*	}*/
						
						
						
						
					/*	if(rs.getString("zone_code")==null)
						{
							System.err.println(" if Zone code :: "+rs.getString("zone_code"));
						}
						else
						{
							System.out.println("else Zone code :: "+rs.getString("zone_code"));
							pieChart.setTitle("ALL Zone Data");
							czBean.setZoneCode(rs.getString("zone_code"));
							czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
							pievalue=pievalue+czBean.getTotal();
							totalpievalue=totalpievalue+czBean.getTotal();
						}*/
						
						list.add(czBean);
					}
					
					
					/*ClientVsZoneBean czBean=new ClientVsZoneBean();

					czBean.setBookingdate(rs.getString(1));
					czBean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));

					pievalue=pievalue+czBean.getTotal();
					totalpievalue=totalpievalue+czBean.getTotal();*/

					
					System.out.println("List size :: "+list.size());

					//pcdata.add(new PieChart.Data(netbean.getBookingdate(),netbean.getTotal()));
				}
				while(rs.next());

				for(ClientVsZoneBean bean: list)
				{	
					System.out.println("List Zone :: "+bean.getZoneCode());
					
					if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
					{
						pcdata.add(new PieChart.Data(bean.getClientCode()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
					}
					else
					{
						if(bean.getZoneCode()!=null)
						{
						pcdata.add(new PieChart.Data(bean.getZoneCode()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
						}
					}
				}
				
				
			   

				pieChart.setData(pcdata);
				
				pcdata.stream().forEach(pieData -> {
					pieData.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
						
						Tooltip tooltip = new Tooltip();
						tooltip.setText("₹ "+df.format(pieData.getPieValue()));
					    Tooltip.install(pieData.getNode(), tooltip);
						
					});
				});

				
				pcdata.stream().forEach(pieData -> {
					pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
						
						if (event.getClickCount() == 1)
						{
							Bounds b1 = pieData.getNode().getBoundsInLocal();
							double newX = (b1.getWidth()) / 2 + b1.getMinX();
							double newY = (b1.getHeight()) / 2 + b1.getMinY();
							// Make sure pie wedge location is reset
							pieData.getNode().setTranslateX(0);
							pieData.getNode().setTranslateY(0);
							TranslateTransition tt = new TranslateTransition(
									Duration.millis(1500), pieData.getNode());
							tt.setByX(newX);
							tt.setByY(newY);
							tt.setAutoReverse(true);
							tt.setCycleCount(2);
							tt.play();
							 					
						}
						else if(event.getClickCount() == 2)
						{
							
							String[] branchCode=pieData.getName().replaceAll("\\s+","").split("\\(");
							System.out.println("Node name >> "+branchCode[0].trim()+" | Value: "+pieData.getPieValue());
						
							Main m=new Main();
							try {
								m.showClientVsZoneServicesReport(sql, comboBoxClient.getValue(), comboBoxZone.getValue(),branchCode[0].trim());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
				});


			}

		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}	
	
//------------------------------------------------ Method for Show Table data ---------------------------------------
	
	public void showDetailTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;

		try
		{
			tabledata_ClientVsZone.clear();
			st=con.createStatement();
			
			finalsql="select zone_code,dailybookingtransaction2client,SUM(amount) as basicamount,SUM(packets) as pcs,"
					+ "SUM(billing_weight) as bill_Wt,SUM(insurance_amount) insrnc_amt,SUM(docket_charge) as docket_charge,"
					+ "SUM(fuel) as fuel_amount,SUM(vas_total) as vas_total,SUM(tax_amount1+tax_amount2+tax_amount3) as gst_total,"
					+ "SUM(total) as total from dailybookingtransaction where "+sql+" group by dailybookingtransaction2client,zone_code";
			
			System.out.println("Final sql after filters: "+finalsql);

			rs=st.executeQuery(finalsql);
			int serialno=1;

			if(!rs.next())
			{
				pieChart.getData().clear();
				barChar.getData().clear();
				pagination.setVisible(false);
				btnExportToExcel.setVisible(false);
				btnExportToPDF.setVisible(false);
				pcdata.clear();
				tableClientVsZone.getItems().clear();
			}
			else
			{
				pagination.setVisible(true);
				btnExportToExcel.setVisible(true);
				btnExportToPDF.setVisible(true);
				do
				{
					ClientVsZoneBean czBean=new ClientVsZoneBean();

					czBean.setSerialno(serialno);
					czBean.setClientCode(rs.getString("dailybookingtransaction2client"));
					czBean.setZoneCode(rs.getString("zone_code"));
					czBean.setPcs(rs.getInt("pcs"));
					czBean.setBillingWeight(Double.parseDouble(dfWeight.format(rs.getDouble("bill_Wt"))));
					czBean.setBasicAmount(Double.parseDouble(dfWeight.format(rs.getDouble("basicamount"))));
					czBean.setInsuranceAmount(Double.parseDouble(df.format(rs.getDouble("insrnc_amt"))));
					czBean.setDocketCharge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
					czBean.setVas_total(Double.parseDouble(df.format(rs.getDouble("vas_total"))));
					czBean.setFuelAmount(Double.parseDouble(df.format(rs.getDouble("fuel_amount"))));
					czBean.setGst_amount(Double.parseDouble(df.format(rs.getDouble("gst_total"))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
					czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));

					tabledata_ClientVsZone.add(new ClientVsZoneTableBean(czBean.getSerialno(), czBean.getClientCode(), czBean.getZoneCode(), czBean.getPcs(),
							czBean.getBillingWeight(), czBean.getBasicAmount(), czBean.getInsuranceAmount(), czBean.getDocketCharge(), czBean.getFuelAmount(), czBean.getVas_total(), czBean.getGst_amount(), czBean.getTotal()));
					serialno++;
				}
				while (rs.next());

				tableClientVsZone.setItems(tabledata_ClientVsZone);
				pagination.setPageCount((tabledata_ClientVsZone.size() / rowsPerPage() + 1));
				pagination.setCurrentPageIndex(0);
				pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}	
	
// ***************************************************************************	
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 10;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
	
		GridPane pane=new GridPane();
		int displace = tabledata_ClientVsZone.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata_ClientVsZone.size() / rowsPerPage();
			}
			/*else
			{
				lastIndex = tabledata.size() / rowsPerPage() -1;
				System.out.println("Pagination lastindex from else condition: "+lastIndex);
			}*/
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                tableClientVsZone.setItems(FXCollections.observableArrayList(tabledata_ClientVsZone.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	                tableClientVsZone.setItems(FXCollections.observableArrayList(tabledata_ClientVsZone.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
// ***************************************************************************	
	
	public static void showSaveDialog() 
	{
	    FileChooser fc = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fc.getExtensionFilters().addAll(
				new ExtensionFilter("Excel Files","*.xls"),
					new ExtensionFilter("PDF Files", "*.pdf"));
        
        //Show save file dialog
        File file = fc.showSaveDialog(null);
        
        file.getAbsoluteFile();
        System.out.println("file chooser: "+file.getAbsoluteFile());
        
    }	
	
// ***************************************************************************	
	
	@FXML
	public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
	{

		Document my_pdf_report = new Document();
		
		//String pdffile="E:/NetwrokReport.pdf";
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
		File file = fc.showSaveDialog(null);
		
		PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
        my_pdf_report.open();            
        
        PdfPTable my_report_table = new PdfPTable(14);
        
        my_report_table.setWidthPercentage(100);
        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
      
      //create a cell object
       
        PdfPCell table_cell = null;
            
       	table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
       	table_cell.setBorder(Rectangle.NO_BORDER);
       	my_report_table.addCell(table_cell);
     
       	table_cell=new PdfPCell(new Phrase("Client code",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	
      	table_cell=new PdfPCell(new Phrase("Zone Code",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	
      	table_cell=new PdfPCell(new Phrase("PCS",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Bill Wt.",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	 
      	table_cell=new PdfPCell(new Phrase("Amount",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	
      	table_cell=new PdfPCell(new Phrase("Insrnc Amt",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      
      	table_cell=new PdfPCell(new Phrase("Docket Chrg",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Fuel",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	
      	table_cell=new PdfPCell(new Phrase("VAS Total",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
        table_cell=new PdfPCell(new Phrase("GST",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Total",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      
     
      	/*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
        my_report_table.addCell(new Phrase("Booking Date",tfont));
        my_report_table.addCell(new Phrase("Srvc Grp",tfont));
      	 */
      
        for(ClientVsZoneTableBean fxBean: tabledata_ClientVsZone)
		{
        	  
			/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
        	*/

        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSlno()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
             
            table_cell=new PdfPCell(new Phrase(fxBean.getClientCode(),tfont));
            table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(fxBean.getZoneCode(),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getPcs()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingWeight()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
              table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBasicAmount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getInsuranceAmount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getDocketCharge()),tfont));
            table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
         	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFuelAmount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
         	
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getVas_total()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getGst_amount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
        	 
            table_cell.setBorder(0);
		
		}
     
        my_pdf_report.add(my_report_table);
        System.out.println("file chooser: "+file.getAbsoluteFile());
           
        System.out.println("File "+file.getName()+" successfully saved");
         my_pdf_report.close();
			
	}
	
	//--------------------------------------------------------------------------
		
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sales Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Client Code");
		rowhead.createCell(2).setCellValue("Zone Code");
		rowhead.createCell(3).setCellValue("PCS");
		rowhead.createCell(4).setCellValue("Billing Weight");
		rowhead.createCell(5).setCellValue("Amount");		
		rowhead.createCell(6).setCellValue("Insurance Amount");
		rowhead.createCell(7).setCellValue("Docket Charge");
		rowhead.createCell(8).setCellValue("Fuel");
		rowhead.createCell(9).setCellValue("VAS Total");		
		rowhead.createCell(10).setCellValue("GST");
		rowhead.createCell(11).setCellValue("Total");
				
		int i=2;
		
		HSSFRow row;
		
		for(ClientVsZoneTableBean czBean: tabledata_ClientVsZone)
		{
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(czBean.getSlno());
			row.createCell(1).setCellValue(czBean.getClientCode());
			row.createCell(2).setCellValue(czBean.getZoneCode());
			row.createCell(3).setCellValue(czBean.getPcs());
			row.createCell(4).setCellValue(czBean.getBillingWeight());
			row.createCell(5).setCellValue(czBean.getBasicAmount());
			row.createCell(6).setCellValue(czBean.getInsuranceAmount());
			row.createCell(7).setCellValue(czBean.getDocketCharge());
			row.createCell(8).setCellValue(czBean.getFuelAmount());
			row.createCell(9).setCellValue(czBean.getVas_total());
			row.createCell(10).setCellValue(czBean.getGst_amount());
			row.createCell(11).setCellValue(czBean.getTotal());

			i++;
		}
			
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		System.out.println("file chooser: "+file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		System.out.println("File "+file.getName()+" successfully saved");
	      
	}	
	
	
// ***************************************************************************
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		//comboBoxBranch.setValue(GraphFilterUtils.ALL_BRANCH);
	//	comboBoxClient.setValue(GraphFilterUtils.ALL_CLIENT);
		comboBoxType.setValue(GraphFilterUtils.TYPE_BOTH);
		//comboBoxMode.setValue(GraphFilterUtils.MODE_AIR);
		comboBoxDoxNonDox.setValue(GraphFilterUtils.DND);
		comboBoxZone.setValue(GraphFilterUtils.ALL_ZONE);
		
		comboBoxBranch.setItems(comboBoxBranchItems);
		comboBoxType.setItems(comboBoxTypeItems);
		comboBoxDoxNonDox.setItems(comboBoxDNDItems);
		//comboBoxMode.setItems(comboBoxModeItems);
		comboBoxClient.setItems(comboBoxClientItems);
		
		
		loadBranch();
		loadClient();
		loadZone();
		//loadZoneViaMode();
		
		
		pagination.setVisible(false);
		btnExportToExcel.setVisible(false);
		btnExportToPDF.setVisible(false);
		
		tabCol_serialno.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Integer>("slno"));
		tabCol_ClientCode.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,String>("clientCode"));
		tabCol_ZoneCode.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,String>("zoneCode"));
		tabCol_Packets.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Integer>("pcs"));
		tabCol_BillingWeight.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("billingWeight"));
		tabCol_BasicAmount.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("basicAmount"));
		tabCol_InsuranceAmount.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("insuranceAmount"));
		tabCol_DocketCharge.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("docketCharge"));
		tabCol_FuelAmount.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("fuelAmount"));
		tabCol_VasTotal.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("vas_total"));
		tabCol_GstTotal.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("gst_amount"));
		tabCol_GrandTotal.setCellValueFactory(new PropertyValueFactory<ClientVsZoneTableBean,Double>("total"));
		
	}
	
}
