package com.onesoft.courier.graph.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.GraphFilterUtils;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.graph.bean.ClientVsZoneBean;
import com.onesoft.courier.graph.bean.ClientVsZoneTableBean;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class ClientVsZone_ServiceController implements Initializable{

	private ObservableList<PieChart.Data> pieChartdata=FXCollections.observableArrayList();
	
	List<String> list_ZoneAir = new ArrayList<>();
	List<String> list_ZoneSurface = new ArrayList<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> barChar;
	
	@FXML
	private PieChart pieChart;
	
// ***************************************************************************
	
	public void setValuesForServicePieChart(String sql,String client,String zone,String selected_Client_Zone) throws SQLException 
	{
        showPie(sql,client,zone,selected_Client_Zone);
    }	
	
	
// ***************************************************************************	

	public void showPie(String sql,String client,String zone,String selected_Client_Zone) throws SQLException
	{
		pievalue=0;
		totalpievalue=0;
		pieChartdata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		String finalsql=null;

		ArrayList<ClientVsZoneBean> list=new ArrayList<ClientVsZoneBean>();
		try
		{
			pieChartdata.clear();
			st=con.createStatement();
			
			if(client.equals(GraphFilterUtils.ALL_CLIENT))
			{
				finalsql="select dailybookingtransaction2service,SUM(total) as total from dailybookingtransaction where "+sql+" and dailybookingtransaction2client='"+selected_Client_Zone+"' group by dailybookingtransaction2service";
			}
			else
			{
				finalsql="select dailybookingtransaction2service,SUM(total) as total from dailybookingtransaction where "+sql+" and zone_code='"+selected_Client_Zone+"' group by dailybookingtransaction2service";
			}
			
			System.out.println("Service >> from pie chart: "+finalsql);
			rs=st.executeQuery(finalsql);
			if(!rs.next())
			{
			
			}
			else
			{
				do
				{
					ClientVsZoneBean czBean=new ClientVsZoneBean();
					
					czBean.setServiceCode(rs.getString("dailybookingtransaction2service"));
					czBean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					pievalue=pievalue+czBean.getTotal();
					totalpievalue=totalpievalue+czBean.getTotal();
					
					list.add(czBean);
					
				}
				while(rs.next());

				for(ClientVsZoneBean bean: list)
				{	
					
					if(client.equals(GraphFilterUtils.ALL_CLIENT))
					{
						pieChart.setTitle("Services Map with Client : "+selected_Client_Zone);
					}
					else
					{
						pieChart.setTitle("Services Map with Zone : "+selected_Client_Zone);
					}	
					
					pieChartdata.add(new PieChart.Data(bean.getServiceCode()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
					
				}

				pieChart.setData(pieChartdata);

			/*	pieChartdata.stream().forEach(pieData -> {
					pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
						
						if (event.getClickCount() == 2)
						{
						Bounds b1 = pieData.getNode().getBoundsInLocal();
						double newX = (b1.getWidth()) / 2 + b1.getMinX();
						double newY = (b1.getHeight()) / 2 + b1.getMinY();
						// Make sure pie wedge location is reset
						pieData.getNode().setTranslateX(0);
						pieData.getNode().setTranslateY(0);
						TranslateTransition tt = new TranslateTransition(
								Duration.millis(1500), pieData.getNode());
						tt.setByX(newX);
						tt.setByY(newY);
						tt.setAutoReverse(true);
						tt.setCycleCount(2);
						tt.play();

						System.out.println("Node name >> "+pieData.getName()+" | Value: "+pieData.getPieValue());
						}
					});
				});*/


			}

		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
// ***************************************************************************
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		
		
	}
	
}
