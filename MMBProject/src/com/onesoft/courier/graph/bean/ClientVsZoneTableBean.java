package com.onesoft.courier.graph.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ClientVsZoneTableBean
{

	private SimpleIntegerProperty slno;
	private SimpleStringProperty zoneCode;
	private SimpleStringProperty clientCode;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty billingWeight;
	private SimpleDoubleProperty basicAmount;
	private SimpleDoubleProperty insuranceAmount;
	private SimpleDoubleProperty docketCharge;
	private SimpleDoubleProperty fuelAmount;
	private SimpleDoubleProperty gst_amount;
	private SimpleDoubleProperty vas_total;
	private SimpleDoubleProperty total;
	
	
	public ClientVsZoneTableBean(int slno,String clientCode,String zoneCode,int pcs,double billingWeight,double basicAmount,
			double insuranceAmt,double docketCharge,double fuelAmt,double vasTotal,double gstAmount,double grandtotal)
	{
		super();
		this.slno=new SimpleIntegerProperty(slno);
		this.clientCode=new SimpleStringProperty(clientCode);
		this.zoneCode=new SimpleStringProperty(zoneCode);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.billingWeight=new SimpleDoubleProperty(billingWeight);
		this.basicAmount=new SimpleDoubleProperty(basicAmount);
		this.insuranceAmount=new SimpleDoubleProperty(insuranceAmt);
		this.docketCharge=new SimpleDoubleProperty(docketCharge);
		this.fuelAmount=new SimpleDoubleProperty(fuelAmt);
		this.vas_total=new SimpleDoubleProperty(vasTotal);
		this.gst_amount=new SimpleDoubleProperty(gstAmount);
		this.total=new SimpleDoubleProperty(grandtotal);
			
	}
	
	public String getZoneCode() {
		return zoneCode.get();
	}
	public void setZoneCode(SimpleStringProperty zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getClientCode() {
		return clientCode.get();
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public double getBasicAmount() {
		return basicAmount.get();
	}
	public void setBasicAmount(SimpleDoubleProperty basicAmount) {
		this.basicAmount = basicAmount;
	}

	public double getDocketCharge() {
		return docketCharge.get();
	}
	public void setDocketCharge(SimpleDoubleProperty docketCharge) {
		this.docketCharge = docketCharge;
	}
	public double getFuelAmount() {
		return fuelAmount.get();
	}
	public void setFuelAmount(SimpleDoubleProperty fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	public double getGst_amount() {
		return gst_amount.get();
	}
	public void setGst_amount(SimpleDoubleProperty gst_amount) {
		this.gst_amount = gst_amount;
	}
	public double getVas_total() {
		return vas_total.get();
	}
	public void setVas_total(SimpleDoubleProperty vas_total) {
		this.vas_total = vas_total;
	}
	public double getTotal() {
		return total.get();
	}
	public void setTotal(SimpleDoubleProperty total) {
		this.total = total;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getBillingWeight() {
		return billingWeight.get();
	}
	public void setBillingWeight(SimpleDoubleProperty billingWeight) {
		this.billingWeight = billingWeight;
	}

	public double getInsuranceAmount() {
		return insuranceAmount.get();
	}

	public void setInsuranceAmount(SimpleDoubleProperty insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	
}
