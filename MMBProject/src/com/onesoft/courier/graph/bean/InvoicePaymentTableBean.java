package com.onesoft.courier.graph.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class InvoicePaymentTableBean {

	public InvoicePaymentTableBean() {
		
		this.srNo = new SimpleIntegerProperty();
		this.invoiceNo = new SimpleStringProperty();
		this.invoiceDate = new SimpleStringProperty();
		this.branch = new SimpleStringProperty();
		this.received =new SimpleDoubleProperty();
		this.adjustment =new SimpleDoubleProperty();
		this.due =new SimpleDoubleProperty();
		this.totalAmt =new SimpleDoubleProperty();
		this.client=new SimpleStringProperty();
	}


	private SimpleIntegerProperty srNo;
	
	private SimpleStringProperty  invoiceNo;
	
	private SimpleStringProperty invoiceDate;
	
	private SimpleStringProperty branch;
	
	private SimpleDoubleProperty received;
	
	private SimpleDoubleProperty adjustment;
	
	private SimpleDoubleProperty due;
	
	private SimpleDoubleProperty totalAmt;
	
	private SimpleStringProperty client;

	public final SimpleIntegerProperty srNoProperty() {
		return this.srNo;
	}
	

	public final int getSrNo() {
		return this.srNoProperty().get();
	}
	

	public final void setSrNo(final int srNo) {
		this.srNoProperty().set(srNo);
	}
	

	public final SimpleStringProperty invoiceNoProperty() {
		return this.invoiceNo;
	}
	

	public final String getInvoiceNo() {
		return this.invoiceNoProperty().get();
	}
	

	public final void setInvoiceNo(final String invoiceNo) {
		this.invoiceNoProperty().set(invoiceNo);
	}
	

	public final SimpleStringProperty invoiceDateProperty() {
		return this.invoiceDate;
	}
	

	public final String getInvoiceDate() {
		return this.invoiceDateProperty().get();
	}
	

	public final void setInvoiceDate(final String invoiceDate) {
		this.invoiceDateProperty().set(invoiceDate);
	}
	

	public final SimpleStringProperty branchProperty() {
		return this.branch;
	}
	

	public final String getBranch() {
		return this.branchProperty().get();
	}
	

	public final void setBranch(final String branch) {
		this.branchProperty().set(branch);
	}
	

	public final SimpleDoubleProperty receivedProperty() {
		return this.received;
	}
	

	public final double getReceived() {
		return this.receivedProperty().get();
	}
	

	public final void setReceived(final double received) {
		this.receivedProperty().set(received);
	}
	

	public final SimpleDoubleProperty adjustmentProperty() {
		return this.adjustment;
	}
	

	public final double getAdjustment() {
		return this.adjustmentProperty().get();
	}
	

	public final void setAdjustment(final double adjustment) {
		this.adjustmentProperty().set(adjustment);
	}
	

	public final SimpleDoubleProperty dueProperty() {
		return this.due;
	}
	

	public final double getDue() {
		return this.dueProperty().get();
	}
	

	public final void setDue(final double due) {
		this.dueProperty().set(due);
	}
	

	public final SimpleDoubleProperty totalAmtProperty() {
		return this.totalAmt;
	}
	

	public final double getTotalAmt() {
		return this.totalAmtProperty().get();
	}
	

	public final void setTotalAmt(final double totalAmt) {
		this.totalAmtProperty().set(totalAmt);
	}


	public final SimpleStringProperty clientProperty() {
		return this.client;
	}
	


	public final String getClient() {
		return this.clientProperty().get();
	}
	


	public final void setClient(final String client) {
		this.clientProperty().set(client);
	}
	
	
	
	
	
	
}
