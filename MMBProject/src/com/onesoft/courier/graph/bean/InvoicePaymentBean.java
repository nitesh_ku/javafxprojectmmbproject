package com.onesoft.courier.graph.bean;

public class InvoicePaymentBean {

	private String clientCode;
	
	private String branchCode;
	
	private double grandTotal;
	
	private double amtReceived;
	
	private double  tdsAmount;
	
	private double  dbNote;
	
	private double adjustment;
	
	private double due;
	
	private double paidAmount;
	
	private double adjustmentAmt;
	
	private double unpaidAmount;
	

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public double getAmtReceived() {
		return amtReceived;
	}

	public void setAmtReceived(double amtReceived) {
		this.amtReceived = amtReceived;
	}

	public double getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public double getDbNote() {
		return dbNote;
	}

	public void setDbNote(double dbNote) {
		this.dbNote = dbNote;
	}

	public double getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(double adjustment) {
		this.adjustment = adjustment;
	}

	public double getDue() {
		return due;
	}

	public void setDue(double due) {
		this.due = due;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public double getUnpaidAmount() {
		return unpaidAmount;
	}

	public void setUnpaidAmount(double unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}

	public double getAdjustmentAmt() {
		return adjustmentAmt;
	}

	public void setAdjustmentAmt(double adjustmentAmt) {
		this.adjustmentAmt = adjustmentAmt;
	}
	
	
	
	
}
