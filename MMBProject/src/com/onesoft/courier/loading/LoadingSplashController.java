package com.onesoft.courier.loading;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadGST_Rates;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.LoadSupplier;
import com.onesoft.courier.common.LoadUser;
import com.onesoft.courier.common.LoadVASItems;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.main.Main;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class LoadingSplashController implements Initializable
{
	@FXML
	private ImageView imgProgessBar;
	
	Task<Void> task;
	
	
	public void loadCommonMethod() throws SQLException
	{
		
		System.err.println("Before loading ----------------------------- 1 list size: >> "+LoadBranch.SET_LOADBRANCHWITHNAME.size());
		System.err.println("Before loading ----------------------------- 2 list size: >>"+LoadBranch.SETLOADBRANCHFORALL.size());
		System.err.println("Before loading ----------------------------- 3 list size: >> "+LoadPincodeForAll.list_Load_Pincode_from_Common.size());
		System.err.println("Before loading ----------------------------- 4 list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME.size());
		System.err.println("Before loading ----------------------------- 5 list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size());
		System.err.println("Before loading ----------------------------- 6 list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NETWORK.size());
		System.err.println("Before loading ----------------------------- 7 list size: >> "+LoadNetworks.SET_LOAD_NETWORK_WITH_NAME.size());
		System.err.println("Before loading ----------------------------- 8 list size: >> "+LoadNetworks.SET_LOAD_NETWORK_FOR_ALL.size());
		System.err.println("Before loading ----------------------------- 9 list size: >> "+LoadSupplier.SET_LOADSUPPLIERWITHNAME.size());
		System.err.println("Before loading ----------------------------- 10 list size: >> "+LoadUser.SET_LOAD_USER.size());
		System.err.println("Before loading ----------------------------- 11 list size: >> "+LoadVASItems.SET_LOAD_VAS_WITH_NAME.size());
		System.err.println("Before loading ----------------------------- 12 list size: >> "+LoadZone.SET_LOAD_ZONE_WITH_NAME.size());
		System.err.println("Before loading ----------------------------- 13 list size: >> "+LoadZone.SET_LOAD_ZONE_FOR_ALL.size());
		System.err.println("Before loading ----------------------------- 14 list size: >> "+LoadCountry.SET_LOAD_COUNTRYWITHNAME.size());
		System.err.println("Before loading ----------------------------- 15 list size: >> "+LoadClients.SET_LOAD_CLIENTWITHNAME.size());
		
		
		
		
		new LoadBranch().loadBranchWithName();
		System.err.println("loading ----------------------------- 1 Complete list size: >> "+LoadBranch.SET_LOADBRANCHWITHNAME.size());
		//new LoadBranch().loadOrigin();
		System.err.println("loading ----------------------------- 2 Complete"+LoadBranch.SETLOADBRANCHFORALL.size());
		new LoadPincodeForAll().loadPincode();
		System.err.println("loading ----------------------------- 3 Complete list size: >> "+LoadPincodeForAll.list_Load_Pincode_from_Common.size());
		new LoadServiceGroups().loadServicesWithName();
		System.err.println("loading ----------------------------- 4 Complete list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME.size());
		new LoadServiceGroups().loadServicesFromServiceType();
		System.err.println("loading ----------------------------- 5 Complete list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size());
		new LoadServiceGroups().loadServicesWithNetwork();
		System.err.println("loading ----------------------------- 6 Complete list size: >> "+LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NETWORK.size());
		new LoadNetworks().loadAllNetworksWithName();
		System.err.println("loading ----------------------------- 7 Complete list size: >> "+LoadNetworks.SET_LOAD_NETWORK_WITH_NAME.size());
		new LoadNetworks().loadAllNetworks();
		System.err.println("loading ----------------------------- 8 Complete list size: >> "+LoadNetworks.SET_LOAD_NETWORK_FOR_ALL.size());
		new LoadSupplier().loadSupplierWithName();
		System.err.println("loading ----------------------------- 9 Complete list size: >> "+LoadSupplier.SET_LOADSUPPLIERWITHNAME.size());
		new LoadUser().loadUserWithID();
		System.err.println("loading ----------------------------- 10 Complete list size: >> "+LoadUser.SET_LOAD_USER.size());
		new LoadVASItems().loadVASWithName();
		System.err.println("loading ----------------------------- 11 Complete list size: >> "+LoadVASItems.SET_LOAD_VAS_WITH_NAME.size());
		new LoadZone().loadZoneWithName();
		System.err.println("loading ----------------------------- 12 Complete list size: >> "+LoadZone.SET_LOAD_ZONE_WITH_NAME.size());
		new LoadZone().loadZone();
		System.err.println("loading ----------------------------- 13 Complete list size: >> "+LoadZone.SET_LOAD_ZONE_FOR_ALL.size());
		new LoadCountry().loadCountryWithName();
		System.err.println("loading ----------------------------- 14 Complete list size: >> "+LoadCountry.SET_LOAD_COUNTRYWITHNAME.size());
		new LoadClients().loadClientWithName();
		System.err.println("loading ----------------------------- 15 Complete list size: >> "+LoadClients.SET_LOAD_CLIENTWITHNAME.size());
		new LoadForwarderDetails().loadForwarderCodeWithName();
		new LoadState().loadStateWithName();
		new LoadGST_Rates().loadLatestGST_onLoad();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		imgProgessBar.setImage(new Image("/icon/loading.gif"));
		
		 task = new Task<Void>() 
	        {
	        	public Void call() throws Exception 
	        	{
	        		
	              loadCommonMethod();
	              
	              return null;
	        	}
	        };
	        
	        
	        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
	               public void handle(WorkerStateEvent t) {
	            	   try {
	            		   Main.loadingStage.close();
						Main.loginpage();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                     
	               }
	        });
	        
	        
	        new Thread(task).start();
		
	}
	
}
