package com.onesoft.courier.search.bean;


import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SearchExpensesTableBean {

	private SimpleIntegerProperty slno;
	private SimpleStringProperty expenseid;
	private SimpleStringProperty expenseDate;
	private SimpleStringProperty branch;
	private SimpleStringProperty expense_code;
	private SimpleStringProperty expense_type;
	private SimpleStringProperty remarks;
	
	
	private SimpleDoubleProperty amount;
	private SimpleDoubleProperty cgst;
	private SimpleDoubleProperty sgst;
	private SimpleDoubleProperty igst;
	
	
	public SearchExpensesTableBean(int slno,String expenseid,String date,String branch,String expensecode,String exprenstype,String remarks,double amount)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.expenseid=new SimpleStringProperty(expenseid);
		this.expenseDate=new SimpleStringProperty(date);
		this.branch=new SimpleStringProperty(branch);
		this.expense_code=new SimpleStringProperty(expensecode);
		this.expense_type=new SimpleStringProperty(exprenstype);
		this.remarks=new SimpleStringProperty(remarks);
		this.amount=new SimpleDoubleProperty(amount);
		
	}
	
	

	public String getExpenseDate() {
		return expenseDate.get();
	}
	public void setExpenseDate(SimpleStringProperty expenseDate) {
		this.expenseDate = expenseDate;
	}
	public String getBranch() {
		return branch.get();
	}
	public void setBranch(SimpleStringProperty branch) {
		this.branch = branch;
	}
	public String getExpense_code() {
		return expense_code.get();
	}
	public void setExpense_code(SimpleStringProperty expense_code) {
		this.expense_code = expense_code;
	}
	public String getExpense_type() {
		return expense_type.get();
	}
	public void setExpense_type(SimpleStringProperty expense_type) {
		this.expense_type = expense_type;
	}
	public String getRemarks() {
		return remarks.get();
	}
	public void setRemarks(SimpleStringProperty remarks) {
		this.remarks = remarks;
	}
	public double getAmount() {
		return amount.get();
	}
	public void setAmount(SimpleDoubleProperty amount) {
		this.amount = amount;
	}
	public double getCgst() {
		return cgst.get();
	}
	public void setCgst(SimpleDoubleProperty cgst) {
		this.cgst = cgst;
	}
	public double getSgst() {
		return sgst.get();
	}
	public void setSgst(SimpleDoubleProperty sgst) {
		this.sgst = sgst;
	}
	public double getIgst() {
		return igst.get();
	}
	public void setIgst(SimpleDoubleProperty igst) {
		this.igst = igst;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getExpenseid() {
		return expenseid.get();
	}
	public void setExpenseid(SimpleStringProperty expenseid) {
		expenseid = expenseid;
	}
	
}
