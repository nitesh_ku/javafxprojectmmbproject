package com.onesoft.courier.search.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Predicate;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.forwarder.bean.ForwarderBean;
import com.onesoft.courier.forwarder.bean.ForwarderTableBean;
import com.onesoft.courier.ratemaster.controller.ImportClientRateExcelController;
import com.onesoft.courier.search.bean.SearchExpenseBean;
import com.onesoft.courier.search.bean.SearchExpensesTableBean;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SearchController implements Initializable{
	
	private ObservableList<SearchExpensesTableBean> tabledata_SearchExpenseDetails=FXCollections.observableArrayList();
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	List<String> list_existingTable= new ArrayList<>();
	List<String> list_newFile= new ArrayList<>();
	List<String> list_selectedNewColumns=new ArrayList<>();
	//String[] columns={"NewUser","OldUser","hostname","pcname","duration"};
	String[] columns={"hostname","pcname","duration"};
	
	@FXML
	private ImageView imageView_SearchIcon;
	
	@FXML
	private Button btnSearch;
	
	@FXML
	private TextField txtSearchBox;
	
	@FXML
	private CheckBox chkbox_ExpenseID;
	
	@FXML
	private CheckBox chkBox_Date;
	
	@FXML
	private CheckBox chkBox_Branch;
	
	@FXML
	private CheckBox chkBox_ExpenseType;
	
	
// ===================================================================================
	
	@FXML
	private TableView<SearchExpensesTableBean> tableSearchExpense;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_ExpenseId;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_Date;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_Branch;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_ExpenseCode;

	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_ExpenseType;
	
	@FXML
	private TableColumn<SearchExpensesTableBean, Double> tabColumn_Amount;
		
	@FXML
	private TableColumn<SearchExpensesTableBean, String> tabColumn_Remarks;
	
	
// ===================================================================================

	public void loadTableViaDB() throws SQLException
	{
		tabledata_SearchExpenseDetails.clear();
		
		
		SearchExpenseBean exBean=new SearchExpenseBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		int slno=1;
		
		try
		{	
			st=con.createStatement();
			String sql="select uid,expense_date,branch,expense_code,expense_type,amount, remarks from expenses order by uid";
				rs=st.executeQuery(sql);
		
				if(!rs.next())
				{
				
				}
				else
				{
					do
					{
						exBean.setSlno(slno);
						exBean.setExpenseid(rs.getString("uid"));
						exBean.setExpenseDate(date.format(rs.getDate("expense_date")));
						exBean.setBranch(rs.getString("branch"));
						exBean.setExpense_code(rs.getString("expense_code"));
						exBean.setExpense_type(rs.getString("expense_type"));
						exBean.setAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					
						exBean.setRemarks(rs.getString("remarks"));
						
						tabledata_SearchExpenseDetails.add(new SearchExpensesTableBean(exBean.getSlno(), exBean.getExpenseid(),exBean.getExpenseDate(), exBean.getBranch() , exBean.getExpense_code(),
								exBean.getExpense_type(), exBean.getRemarks(), exBean.getAmount()));
						
						slno++;
					}
					while(rs.next());
					
					tableSearchExpense.setItems(tabledata_SearchExpenseDetails);
					
				//	receivePickupTable.setItems(receivePickupTabledata);
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
	}
	
	
	
// ===================================================================================

	public void getColumnNames() throws SQLException
	{
		tabledata_SearchExpenseDetails.clear();


		SearchExpenseBean exBean=new SearchExpenseBean();

		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		ResultSetMetaData rsMetaData=null;
		int slno=1;
		
		
		boolean isColumnExist=false;
		
		String addColSubQuery=null;
		String alterQuery=null;

		try
		{	
			st=con.createStatement();
			String sql="select * from clientrate_zone";
			rs=st.executeQuery(sql);
			
			rsMetaData=rs.getMetaData();
			int colCount=rsMetaData.getColumnCount();
			
			for (int i = 1; i <= colCount; i++ ) 
			{
				//System.out.println("Column name "+i+": "+rsMetaData.getColumnName(i));	  
				list_existingTable.add(rsMetaData.getColumnName(i));
			}
			
			
			
			for(String newColumns: ImportClientRateExcelController.list_zones)
			{
				if(list_existingTable.contains(newColumns)==false)
				{
					System.out.println("New Columns: "+newColumns);
					list_selectedNewColumns.add(newColumns);
				}
			}
			
			for(String finalColumnName: list_selectedNewColumns)
			{
				addColSubQuery=addColSubQuery+", Add "+finalColumnName+" varchar(20)";
			}
			
		
			//System.out.println("alter Query: ALTER TABLE employees "+addColSubQuery.substring(5));
			alterQuery="ALTER TABLE clientrate_zone "+addColSubQuery.substring(5);
			System.out.println(alterQuery);
			st.execute(alterQuery);
			//String alterQuery = "ALTER TABLE employees ADD address CHAR(100) ";
			
		
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
			
	
	public void addDynamicColumn()
	{
		for(String zone:ImportClientRateExcelController.list_zones)
		{
			System.out.println("Zone >> "+zone);
		}
	}
	
// ===================================================================================	
	
	/**
	 * @param location
	 * @param resources
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imageView_SearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		
		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,Integer>("slno"));
		tabColumn_ExpenseId.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("expenseid"));
		tabColumn_Date.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("expenseDate"));
		tabColumn_Branch.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("branch"));
		tabColumn_ExpenseCode.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("expense_code"));
		tabColumn_ExpenseType.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("expense_type"));
		tabColumn_Amount.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,Double>("amount"));
		tabColumn_Remarks.setCellValueFactory(new PropertyValueFactory<SearchExpensesTableBean,String>("remarks"));
		
		for(int i=0;i<columns.length;i++)
		{
			list_newFile.add(columns[i]);
		}
		
	//	addDynamicColumn();
		
		try {
			//loadTableViaDB();
			getColumnNames();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		FilteredList<SearchExpensesTableBean> filteredData=new  FilteredList<>(tabledata_SearchExpenseDetails,e -> true);
		
		txtSearchBox.setOnKeyPressed(e -> {
			txtSearchBox.textProperty().addListener((ObservableValue, oldValue, newValue) ->{
				
				filteredData.setPredicate((Predicate<? super SearchExpensesTableBean>) user ->{
					if(newValue==null || newValue.isEmpty())
					{
						return true;
					}
					String lowerCaseFilter=newValue.toLowerCase();
					
					if(chkBox_Date.isSelected()==true)
					{
						if(user.getExpenseDate().toLowerCase().contains(lowerCaseFilter))
						{
							System.out.println("Expense ID running.......");
							return true;
						}
					}
					
					if(chkBox_Branch.isSelected()==true)
					{
						if(user.getBranch().toLowerCase().contains(lowerCaseFilter))
						{
							return true;
						}
					}
					
					if(chkBox_ExpenseType.isSelected()==true)
					{
						if(user.getExpense_type().toLowerCase().contains(lowerCaseFilter))
						{
							return true;
						}
					}
					
					if(chkbox_ExpenseID.isSelected()==true)
					{
						if(user.getExpenseid().toLowerCase().contains(lowerCaseFilter))
						{
							return true;
						}
					}
					
					
					return false;
					
				});
			});	
			
			SortedList<SearchExpensesTableBean> sortedData=new  SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(tableSearchExpense.comparatorProperty());
			tableSearchExpense.setItems(sortedData);
			
		});
		
		
		
	}
	
	
	
	

}
