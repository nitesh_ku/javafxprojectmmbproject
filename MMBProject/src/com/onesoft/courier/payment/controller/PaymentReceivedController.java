package com.onesoft.courier.payment.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.payment.bean.PaymentPaidTableBean;
import com.onesoft.courier.payment.bean.PaymentReceivingBean;
import com.onesoft.courier.payment.bean.PaymentUnpaidTableBean;
import com.onesoft.courier.payment.utils.PaymentUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class PaymentReceivedController implements Initializable {

	private ObservableList<PaymentUnpaidTableBean> unpaidtabledata=FXCollections.observableArrayList();
	private ObservableList<PaymentPaidTableBean> paidtabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboboxpaymentmodeitems=FXCollections.observableArrayList(PaymentUtils.PAYMENTMODE_CASH,PaymentUtils.PAYMENTMODE_CHEQUE,PaymentUtils.PAYMENTMODE_NETBANKING);
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	public double totalReceivedAmount=0;
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private ImageView imgSearchIcon1;
	
	@FXML
	TextField txtSearchByName;
	
	/*
	@FXML
	TextField txtCurrentDate;
	*/
	
	@FXML
	TextField txtInvoiceDate;
	
	@FXML
	TextField txtFromDate;
	
	@FXML
	TextField txtToDate;
	
	@FXML
	TextField txtAmount;
	
	@FXML
	TextField txtAmtReceived;
	
	@FXML
	TextField txtTotalReceivedAmt;
	
	@FXML
	TextField txtTDS;
	
	@FXML
	TextField txtDB_Note;
	
	@FXML
	TextField txtAdjustment;
	
	@FXML
	TextField txtDue;
	
	@FXML
	TextField txtBankName;

	@FXML
	TextField txtChequeNo;
	
	@FXML
	TextField txtRemarks;
	
	@FXML
	ComboBox<String> comboBoxPaymentMode;
	
	@FXML
	DatePicker dpkcurrentdate;
	
	@FXML
	DatePicker dpkChequeDate;
	
	@FXML
	Button btnreceived;
	
	@FXML
	Button btnreset;
	
//---------------------------------------------------------------------------------------------------------	
	
	@FXML
	private TableView<PaymentPaidTableBean> paidtable;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, Integer> paidserialno;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, String> paidclientname;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, String> paidinvoicenumber;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, String> paidinvoicedate;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, String> paidpaymentdate;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, Double> paidbillamount;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, Double> paidamtreceived;
	
	@FXML
	private TableColumn<PaymentPaidTableBean, String> paidpaymentmode;
	
//---------------------------------------------------------------------------------------------------------	
	
	@FXML
	private TableView<PaymentUnpaidTableBean> unpaidtable;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, Integer> paymentserialno;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> clientname;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> invoicenumber;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> invoicedate;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> invoicebranch;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, Double> billamount;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, Double> amtreceived;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, Double> due;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> paymentmode;
	
	@FXML
	private TableColumn<PaymentUnpaidTableBean, String> status;
	
	
	Main m=new Main();
	
//--------------------------------------------------------------------------------------------------	
	
	public void showInvoiceDetails() throws SQLException
	{
		
		System.out.println("Search running ====================");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		txtTDS.setDisable(false);
		txtDB_Note.setDisable(false);
		txtAdjustment.setDisable(false);
	
		
		Statement st=null;
		ResultSet rs=null;
		
		
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		PreparedStatement preparedStmt=null;
		
		if(txtSearchByName.getText().length()>5)
		{
		String[] invoicenumber=txtSearchByName.getText().replaceAll("\\s+","").split("\\|");
		
		//System.out.println("Client name: "+clientcode[0]);
		//System.out.println("Invoice No.: "+clientcode[1]);
		
			try
			{	
				String sql = "select Invoice_date,from_date,to_date,grandtotal,amtreceived, tds_amount, db_note, adjustment,due, invoice_status from invoice where invoice_number=?";
			/*	String sql="select inv.Invoice_date,inv.from_date,inv.to_date,inv.grandtotal,sum(inpay.amount) as totalreceivedAmt ,"
						+ "inv.amtreceived, inv.tds_amount, inv.db_note,inv.adjustment,inv.due, inv.invoice_status from invoice as inv,"
						+ "invoicepaymenthistory as inpay where inv.invoice_number=inpay.invoiceno and inv.invoice_number=? "
						+ "group by inv.Invoice_date,inv.from_date,inv.to_date,inv.grandtotal,inv.amtreceived,inv.tds_amount, "
						+ "inv.db_note, inv.adjustment,inv.due, inv.invoice_status";*/
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,invoicenumber[1]);
				System.out.println("SHOW sql :: "+preparedStmt);
				rs = preparedStmt.executeQuery();
			
				if(!rs.next())
				{
					
				}
				else
				{
					paybean.setStatus(rs.getString("invoice_status"));
					
					if(rs.getString("invoice_status").equals("D"))
					{
						paybean.setInvoice_date(date.format(rs.getDate("invoice_date")));
						paybean.setFrom_date(date.format(rs.getDate("from_date")));
						paybean.setTo_date(date.format(rs.getDate("to_date")));
						paybean.setBillamount(rs.getDouble("grandtotal"));
						paybean.setAmtReceived(rs.getDouble("amtreceived"));
						paybean.setAdjustment(rs.getDouble("adjustment"));
						paybean.setTds(rs.getDouble("tds_amount"));
						paybean.setDbNote(rs.getDouble("db_note"));
						paybean.setDue(rs.getDouble("due"));
						//totalReceivedAmount=rs.getDouble("totalreceivedAmt");
					}
					else
					{
						paybean.setInvoice_date(date.format(rs.getDate("invoice_date")));
						paybean.setFrom_date(date.format(rs.getDate("from_date")));
						paybean.setTo_date(date.format(rs.getDate("to_date")));
						paybean.setBillamount(rs.getDouble("grandtotal"));
						paybean.setDue(rs.getDouble("grandtotal"));
					}
					
					getDataFromInvoicePaymentHistory(invoicenumber[1]);
					
				}
				
				dpkcurrentdate.setValue(LocalDate.now());
				txtInvoiceDate.setText(paybean.getInvoice_date());
				txtFromDate.setText(paybean.getFrom_date());
				txtToDate.setText(paybean.getTo_date());
				txtAmtReceived.setText("0.0");
				
				txtTotalReceivedAmt.setText(String.valueOf(totalReceivedAmount));
				
				
				
				if(String.valueOf(paybean.getBillamount()).equals("null") && String.valueOf(paybean.getDue()).equals("null"))
				{
					txtAmount.setText("0.0");
					txtDue.setText("0.0");
					
					txtAdjustment.setText("0.0");
					txtTDS.setText("0.0");
					txtAmtReceived.setText("0.0");
					comboBoxPaymentMode.setDisable(true);
					btnreceived.setDisable(true);
				}
				else
				{
					txtAmount.setText(String.valueOf(paybean.getBillamount()));
					//txtDue.setText(String.valueOf(paybean.getBillamount()));
					txtDue.setText(String.valueOf(paybean.getDue()));
					
					if(paybean.getStatus().equals("D"))
					{
						if(paybean.getDbNote()==0)
						{
							txtDB_Note.setText("0.0");
						}
						else
						{
							txtDB_Note.setText(df.format(paybean.getDbNote()));
						}
						
						if(paybean.getAdjustment()==0)
						{
							txtAdjustment.setText("0.0");
						}
						else
						{
							txtAdjustment.setText(df.format(paybean.getAdjustment()));
						}
						
						if(paybean.getTds()==0)
						{
							txtTDS.setText("0.0");
						}
						else
						{
							txtTDS.setText(df.format(paybean.getTds()));
						}
						
						/*if(paybean.getAmtReceived()==0)
						{
							txtAmtReceived.setText("0.0");
						}
						else
						{
							txtAmtReceived.setText(df.format(paybean.getAmtReceived()));
						}*/
						
						/*if((paybean.getAdjustment()+paybean.getAmtReceived()+paybean.getTds()+paybean.getDbNote())==0)
						{
							txtAmount.setText(String.valueOf(paybean.getBillamount()));
						}
						else
						{
							txtAmount.setText(String.valueOf(paybean.getBillamount()-(paybean.getAdjustment()+paybean.getAmtReceived()+paybean.getTds()+paybean.getDbNote())));	
						}*/
						
						
					}
					else
					{
						txtDB_Note.setText("0.0");
						txtAdjustment.setText("0.0");
						txtTDS.setText("0.0");
						txtAmtReceived.setText("0.0");
					}
					
					comboBoxPaymentMode.setDisable(false);
					btnreceived.setDisable(false);
				}
		
				comboBoxPaymentMode.setValue("Cheque");
				
			
				
				//savePaymentDetails();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
		}
	}
	
	public void getDataFromInvoicePaymentHistory(String invoiceno) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		PreparedStatement preparedStmt=null;
		
	
		//String[] invoicenumber=txtSearchByName.getText().replaceAll("\\s+","").split("\\|");
		
		try
		{	
			//String sql = "select Invoice_date,from_date,to_date,grandtotal,amtreceived, tds_amount, db_note, adjustment,due, invoice_status from invoice where invoice_number=?";
			String sql="select invoiceno,sum(amount) as totalreceivedAmt from invoicepaymenthistory where invoiceno=? group by invoiceno";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,invoiceno);
			System.out.println("SHOW History sql :: "+preparedStmt);
			rs = preparedStmt.executeQuery();

			if(rs.next())
			{
				totalReceivedAmount=rs.getDouble("totalreceivedAmt");	
			}

		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}
	
//--------------------------------------------------------------------------------------------------
	
	public void loadSearchByName() throws SQLException
	{
		System.out.println("Search running");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		List<String> clientList=new ArrayList<String>();
	 
		try
		{	
			st=con.createStatement();
			String sql="select clientcode, invoice_number from invoice where status!='P'";
			rs=st.executeQuery(sql);
		
			if(!rs.next())
			{
				
				
			}
			else
			{
				do
				{
					clientList.add(rs.getString("clientcode")+" | "+rs.getString("invoice_number"));
				}
				while(rs.next());
			}
			
			TextFields.bindAutoCompletion(txtSearchByName, clientList);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
//--------------------------------------------------------------------------------------------------	
	
	public void showUnpaidInvoices() throws SQLException
	{
	
		unpaidtabledata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		
		try
		{	
			st=con.createStatement();
			String sql="select clientcode, invoice_number, invoice_date,grandtotal,amtreceived,due,payment_mode,status from invoice where status !='P' order by invoice_number";
			rs=st.executeQuery(sql);
	
			int serialno=1;
			
			if(!rs.next())
			{
				
				
			}
			else
			{
				do
				{
					paybean.setSerialno(serialno);
					paybean.setClientname(rs.getString("clientcode"));
					paybean.setInvoice_number(rs.getString("invoice_number"));
					paybean.setInvoice_date(date.format(rs.getDate("invoice_date")));
					paybean.setBillamount(rs.getDouble("grandtotal"));
					paybean.setAmtReceived(rs.getDouble("amtreceived"));
					paybean.setDue(rs.getDouble("due"));
					
					if(rs.getString("status")==null)
					{
						paybean.setStatus("--");	
					}
					else
					{
						paybean.setStatus(rs.getString("status"));		
					}
					
					unpaidtabledata.addAll(new PaymentUnpaidTableBean(paybean.getSerialno(), paybean.getClientname(), paybean.getInvoice_number(), paybean.getInvoice_date(),
							paybean.getBillamount(), paybean.getAmtReceived(), paybean.getDue(),null, paybean.getStatus()));
					
					serialno++;
				}
				while(rs.next());
				unpaidtable.setItems(unpaidtabledata);
				
			}
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}

	}
	
//--------------------------------------------------------------------------------------------------
	
	public void showPaidInvoices() throws SQLException
	{
		
		paidtabledata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		
		try
		{	
			st=con.createStatement();
			String sql="select clientcode, invoice_number, invoice_date, paymentdate, grandtotal, amtreceived, payment_mode from invoice where status ='P' order by invoice_number";
			rs=st.executeQuery(sql);
	
			int serialno=1;
			
			if(!rs.next())
			{
				
				
			}
			else
			{
				do
				{
					paybean.setPaidserialno(serialno);
					paybean.setPaidclientname(rs.getString("clientcode"));
					paybean.setPaidinvoicenumber(rs.getString("invoice_number"));
					paybean.setPaidinvoicedate(date.format(rs.getDate("invoice_date")));
					paybean.setPaidpaymentDate(date.format(rs.getDate("paymentdate")));
					paybean.setPaidbillamount(rs.getDouble("grandtotal"));
					paybean.setPaidamtReceived(rs.getDouble("amtreceived"));
					paybean.setPaidpaymentMode(rs.getString("payment_mode"));
					
					
					/*if(rs.getString("status")==null)
					{
						paybean.setStatus("--");	
					}
					else
					{
						paybean.setStatus(rs.getString("status"));		
					}*/
					
					paidtabledata.addAll(new PaymentPaidTableBean(paybean.getPaidserialno(), paybean.getPaidclientname(), paybean.getPaidinvoicenumber(), paybean.getPaidinvoicedate(),
							paybean.getPaidpaymentDate(),paybean.getPaidbillamount(), paybean.getPaidamtReceived(), paybean.getPaidpaymentMode()));
					
					serialno++;
				}
				while(rs.next());
				paidtable.setItems(paidtabledata);
				
			}
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
//--------------------------------------------------------------------------------------------------	

	/*public void calculatepayment()
	{
		double d=Double.valueOf(txtAmtReceived.getText())+Double.valueOf(txtTDS.getText())+Double.valueOf(txtDB_Note.getText())+Double.valueOf(txtAdjustment.getText());
		
		if(d<Double.valueOf(txtAmount.getText()))
		{
			txtDue.setText(String.valueOf(df.format(Double.valueOf(txtAmount.getText())-Double.valueOf(txtAmtReceived.getText())-Double.valueOf(txtTDS.getText())-Double.valueOf(txtDB_Note.getText())-Double.valueOf(txtAdjustment.getText()))));
	
			txtAmtReceived.setDisable(false);
			txtTDS.setDisable(false);
			txtDB_Note.setDisable(false);
			txtAdjustment.setDisable(false);

		}
		else if(d==Double.valueOf(txtAmount.getText()))
		{
			
			txtDue.setText("0.0");
			
			if(Double.parseDouble(txtAmtReceived.getText())==0.0)
			{
				txtAmtReceived.setDisable(true);
			}
			
			if(Double.parseDouble(txtTDS.getText())==0.0)
			{
				txtTDS.setDisable(true);
			}
				
			if(Double.parseDouble(txtDB_Note.getText())==0.0)
			{
				txtDB_Note.setDisable(true);
			}
			
			if(Double.parseDouble(txtAdjustment.getText())==0.0)
			{
				txtAdjustment.setDisable(true);
			}
			
			if(Double.parseDouble(txtDue.getText())==0.0)
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				comboBoxPaymentMode.requestFocus();
			}
		}
		else if(d>Double.valueOf(txtAmount.getText()))
		{
			txtDue.setText("0.0");
			
			if(Double.parseDouble(txtAmtReceived.getText())==0.0)
			{
				txtAmtReceived.setDisable(true);
			}
			
			if(Double.parseDouble(txtTDS.getText())==0.0)
			{
				txtTDS.setDisable(true);
			}
			
			if(Double.parseDouble(txtDB_Note.getText())==0.0)
			{
				txtDB_Note.setDisable(true);
			}
			
			if(Double.parseDouble(txtAdjustment.getText())==0.0)
			{
				txtAdjustment.setDisable(true);
			}
			
			if(Double.parseDouble(txtDue.getText())==0.0)
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				
				if(txtAmtReceived.isFocused()==true)
				{
					alert.setContentText("Please Enter Correct Amount In Received Amount");
					txtDue.requestFocus();
				}
				
				else if(txtAdjustment.isFocused()==true)
				{
					alert.setContentText("Please Enter Correct Amount In Adjustment Amount");
					txtAdjustment.requestFocus();
				}
				
				else if(txtDB_Note.isFocused()==true)
				{
					alert.setContentText("Please Enter Correct Amount In DB Note Amount");
					txtDue.requestFocus();
				}
				
				else if(txtTDS.isFocused()==true)
				{
					alert.setContentText("Please Enter Correct Amount In TDS Amount");
					txtDue.requestFocus();
				}
				alert.showAndWait();
			}
		}
	}*/

//--------------------------------------------------------------------------------------------------	

	public void setValidation() throws SQLException, ParseException
	{
		
		if(txtAmtReceived.getText()==null || txtAmtReceived.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("Amt Reveived field is Empty!");
			alert.showAndWait();
			txtAmtReceived.requestFocus();
		}
		
		else if(!txtAmtReceived.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Number Validation");
			alert.setHeaderText(null);
			alert.setContentText("Amt Received: Please enter only numeric value");
			alert.showAndWait();
			txtAmtReceived.requestFocus();
		}
		
		else if(txtTDS.getText()==null || txtTDS.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("TDS amount field is Empty!");
			alert.showAndWait();
			txtTDS.requestFocus();
		}
		
		else if(!txtTDS.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Number Validation");
			alert.setHeaderText(null);
			alert.setContentText("TDS: Please enter only numeric value");
			alert.showAndWait();
			txtTDS.requestFocus();
		}
		
		else if(txtDB_Note.getText()==null || txtDB_Note.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("DB Note amount field is Empty!");
			alert.showAndWait();
			txtDB_Note.requestFocus();
		}
		
		else if(!txtDB_Note.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Number Validation");
			alert.setHeaderText(null);
			alert.setContentText("DB Note: Please enter only numeric value");
			alert.showAndWait();
			txtDB_Note.requestFocus();
		}
	
		else if(txtAdjustment.getText()==null || txtAdjustment.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("Adjustment amount field is Empty!");
			alert.showAndWait();
			txtAdjustment.requestFocus();
		}
		else if(!txtAdjustment.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Number Validation");
			alert.setHeaderText(null);
			alert.setContentText("Adjustment: Please enter only numeric value");
			alert.showAndWait();
			txtAdjustment.requestFocus();
		}
		
		else if(comboBoxPaymentMode.getValue().equals(PaymentUtils.PAYMENTMODE_CHEQUE))
		{
		
			if(txtBankName.getText()==null || txtBankName.getText().isEmpty())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Empty Field Validation");
				alert.setHeaderText(null);
				alert.setContentText("Bank Name field is Empty!");
				alert.showAndWait();
				txtBankName.requestFocus();
			}
			
			else if(txtChequeNo.getText()==null || txtChequeNo.getText().isEmpty())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Empty Field Validation");
				alert.setHeaderText(null);
				alert.setContentText("Cheque No. field is Empty!");
				alert.showAndWait();
				txtChequeNo.requestFocus();
			}
			else if(!txtChequeNo.getText().matches("-?\\d+(\\.\\d+)?"))
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Number Validation");
				alert.setHeaderText(null);
				alert.setContentText("Cheque No.: Please enter only numeric value");
				alert.showAndWait();
				txtChequeNo.requestFocus();
			}
			
			else if(dpkChequeDate.getValue()==null)
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Empty Field Validation");
				alert.setHeaderText(null);
				alert.setContentText("Cheque Date field is Empty!");
				alert.showAndWait();
				dpkChequeDate.requestFocus();
			}
		}
		
		else
		{
			double totalAmt=totalReceivedAmount+Double.valueOf(txtAmtReceived.getText())+Double.valueOf(txtAdjustment.getText())+Double.valueOf(txtTDS.getText())+Double.valueOf(txtDB_Note.getText());
			System.out.println("Total AMt :: "+totalAmt+" | Billing AMount :: "+Double.valueOf(txtAmount.getText()));
			if(totalAmt<=Double.valueOf(txtAmount.getText()))
			{
				if(Double.valueOf(txtDue.getText())==0)
				{
					String[] invoicenumber=txtSearchByName.getText().replaceAll("\\s+","").split("\\|");
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setHeaderText(null);
					alert.setContentText("Complete Payment against Invoice No: "+invoicenumber[1]+" is already received");
					alert.showAndWait();
					//txtAmtReceived.requestFocus();
				}
				else
				{
					savePaymentDetails();	
				}
				
			}
			else
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Empty Field Validation");
				alert.setHeaderText(null);
				alert.setContentText("Received Amount should be less than Billing Amount\nPlease check");
				alert.showAndWait();
				txtAmtReceived.requestFocus();
			}
			
			
			
		}
		
	
	}
	
//--------------------------------------------------------------------------------------------------

	public void savePaymentDetails() throws SQLException, ParseException
	{
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		String[] invoicenumber=txtSearchByName.getText().replaceAll("\\s+","").split("\\|");
		
		LocalDate chqdate=null;
		Date chequedate = null;
		LocalDate from=dpkcurrentdate.getValue();
		Date currentdate=Date.valueOf(from);
		
		if(comboBoxPaymentMode.getValue().equals(PaymentUtils.PAYMENTMODE_CHEQUE))
		{
			chqdate=dpkcurrentdate.getValue();
			chequedate=Date.valueOf(chqdate);
			paybean.setBankName(txtBankName.getText());
			paybean.setChequeNo(Integer.parseInt(txtChequeNo.getText()));
		}
		
		
		paybean.setInvoice_number(invoicenumber[1]);
		paybean.setAmtReceived(Double.parseDouble(txtAmtReceived.getText())+totalReceivedAmount);
		paybean.setTds(Double.parseDouble(txtTDS.getText()));
		paybean.setDbNote(Double.parseDouble(txtDB_Note.getText()));
		paybean.setAdjustment(Double.parseDouble(txtAdjustment.getText()));
		
		System.out.println("Bill Amount :: "+paybean.getBillamount());
		System.out.println("Amt Received :: "+paybean.getAmtReceived());
		System.out.println("TDS :: "+paybean.getTds());
		System.out.println("DB Note :: "+paybean.getDbNote());
		System.out.println("Adjusted :: "+paybean.getAdjustment());
		System.out.println("Total Received :: "+totalReceivedAmount);
		//System.out.println("Amt Received :: "+paybean.getAmtReceived());
		
		paybean.setDue(Double.valueOf(txtAmount.getText())-(paybean.getAmtReceived()+paybean.getTds()+paybean.getDbNote()+paybean.getAdjustment()));
		System.out.println("Due :: "+paybean.getDue());
		paybean.setPaymentMode(comboBoxPaymentMode.getValue());
		paybean.setPaymentremarks(txtRemarks.getText());
		
		if(paybean.getDue()==0.0)
		{
		paybean.setStatus("P");
		}
		else
		{
		paybean.setStatus("D");	
		}
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = "update invoice set paymentdate=?, amtreceived=?, tds_amount=?, db_note=?, adjustment=?, due=?, payment_mode=?, bank_name=?, cheque_no=?,"
							+ " cheque_date=?, paymentremarks=? ,invoice_status=? where invoice_number=?";
		
			
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setDate(1, currentdate);
			preparedStmt.setDouble(2, paybean.getAmtReceived());
			preparedStmt.setDouble(3, paybean.getTds());
			preparedStmt.setDouble(4, paybean.getDbNote());
			preparedStmt.setDouble(5, paybean.getAdjustment());
			preparedStmt.setDouble(6, paybean.getDue());
			preparedStmt.setString(7, paybean.getPaymentMode());
			preparedStmt.setString(8, paybean.getBankName());
			preparedStmt.setInt(9, paybean.getChequeNo());
			preparedStmt.setDate(10, chequedate);
			preparedStmt.setString(11, paybean.getPaymentremarks());
			preparedStmt.setString(12, paybean.getStatus());
			preparedStmt.setString(13, paybean.getInvoice_number());
			
		int status=	preparedStmt.executeUpdate();
			
		if(status==1)
		{
			
			savaDataInPaymentHistory(paybean.getInvoice_number());
			
			showUnpaidInvoices();
			showPaidInvoices();
			reset();
			totalReceivedAmount=0;
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Alert");
			alert.setHeaderText(null);
			alert.setContentText("Payment Receiving Process Completed Successfully!\nThank you...");
			alert.showAndWait();
			
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Alert");
			alert.setHeaderText(null);
			alert.setContentText("Payment Receiving Process not completed\nplease try again...");
			alert.showAndWait();
			txtSearchByName.requestFocus();
			
		}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

//--------------------------------------------------------------------------------------------------
	
	public void savaDataInPaymentHistory(String invoiceNo) throws SQLException
	{
		PaymentReceivingBean paybean=new PaymentReceivingBean();
		//String[] invoicenumber=txtSearchByName.getText().replaceAll("\\s+","").split("\\|");
		java.util.Date datetime=new java.util.Date();   
		   System.out.println("Time >>>>>>> "+new SimpleDateFormat("HH:mm:ss").format(datetime));
		
		LocalDate chqdate=null;
		Date chequedate = null;
		LocalDate paymentLocalDate=dpkcurrentdate.getValue();
		Date paymentDate=Date.valueOf(paymentLocalDate);
		
		/*
		if(comboBoxPaymentMode.getValue().equals(PaymentUtils.PAYMENTMODE_CHEQUE))
		{
			chqdate=dpkcurrentdate.getValue();
			chequedate=Date.valueOf(chqdate);
			paybean.setBankName(txtBankName.getText());
			paybean.setChequeNo(Integer.parseInt(txtChequeNo.getText()));
		}
		*/
		
		paybean.setInvoice_number(invoiceNo);
		paybean.setPaymentTime(new SimpleDateFormat("HH:mm:ss").format(datetime));
		paybean.setAmtReceived(Double.parseDouble(txtAmtReceived.getText()));
	//	paybean.setPaymentHistory_Amount();
		paybean.setIpAddress(CommonVariable.USER_IP_ADDRESS);
		paybean.setHostname(CommonVariable.USER_HOST_NAME);
		paybean.setUserid(CommonVariable.USER_ID);
			
		
		if(Double.parseDouble(txtDue.getText())==0.0)
		{
			paybean.setStatus("P");
		}
		else
		{
			paybean.setStatus("D");	
		}
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = "INSERT INTO invoicepaymenthistory(invoiceno, paymentdate, paymenttime, amount, userid, ip_address, hostname, "
					+ "create_date, lastmodifieddate) VALUES (?, ?, ?, ?, ?, ?, ?, CURRENT_DATE,CURRENT_TIMESTAMP)";
			
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, paybean.getInvoice_number());
			preparedStmt.setDate(2, paymentDate);
			preparedStmt.setString(3, paybean.getPaymentTime());
			preparedStmt.setDouble(4, paybean.getAmtReceived());
			preparedStmt.setString(5, paybean.getUserid());
			preparedStmt.setString(6, paybean.getIpAddress());
			preparedStmt.setString(7, paybean.getHostname());
			
			preparedStmt.executeUpdate();
			
			reset();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
//--------------------------------------------------------------------------------------------------	

	public void setBankTextfields()
	{
		if(comboBoxPaymentMode.getValue().equals(PaymentUtils.PAYMENTMODE_CHEQUE))
		{
			txtBankName.setDisable(false);
			txtChequeNo.setDisable(false);
			dpkChequeDate.setDisable(false);
		}
		else
		{
			txtBankName.setDisable(true);
			txtChequeNo.setDisable(true);
			dpkChequeDate.setDisable(true);
			
			txtBankName.clear();
			txtChequeNo.clear();
			dpkChequeDate.getEditor().clear();
		}
		
	}
	

//--------------------------------------------------------------------------------------------------	
	
	public void reset() throws IOException
	{
		//m.showPaymentReceiving();	
	}
	
	

// ==================================================================================================
	
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, ParseException{

		Node n=(Node) e.getSource();
		
		
		if(n.getId().equals("txtSearchByName"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtSearchByName.getText().equals("") || txtSearchByName.getText().isEmpty())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty field");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the Invoice No. Or Name");
					alert.showAndWait();
					txtSearchByName.requestFocus();
				}
				else
				{
				dpkcurrentdate.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("dpkcurrentdate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAmtReceived.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAmtReceived"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtTDS.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtTDS"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtDB_Note.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtDB_Note"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAdjustment.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAdjustment"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxPaymentMode.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxPaymentMode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtBankName.isDisable()==false)
				{
					txtBankName.requestFocus();
				}
				else
				{
					txtRemarks.requestFocus();
				}
			}
		}
		else if(n.getId().equals("txtBankName"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtBankName.isDisable()==false)
				{
				txtChequeNo.requestFocus();
				}
				else
				{
					txtRemarks.requestFocus();
				}
			}
		}
		else if(n.getId().equals("txtChequeNo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtBankName.isDisable()==false)
				{
				dpkChequeDate.requestFocus();
				}
				else
				{
					txtRemarks.requestFocus();
				}
			}
		}
		else if(n.getId().equals("dpkChequeDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtRemarks.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtRemarks"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				btnreceived.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnreceived"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				setValidation();
				//txtRemarks.requestFocus();
			}
		}
	}
	
	
//--------------------------------------------------------------------------------------------------
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		imgSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIcon1.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtAmtReceived.setDisable(false);
		txtTDS.setDisable(false);
		txtDB_Note.setDisable(false);
		txtAdjustment.setDisable(false);
		txtInvoiceDate.setEditable(false);
		dpkcurrentdate.setEditable(false);
		txtTotalReceivedAmt.setEditable(false);
		
		comboBoxPaymentMode.setDisable(true);
		txtBankName.setDisable(true);
		txtChequeNo.setDisable(true);
		dpkChequeDate.setDisable(true);
		btnreceived.setDisable(true);
				
		comboBoxPaymentMode.setItems(comboboxpaymentmodeitems);

		paymentserialno.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,Integer>("paymentserialno"));
		invoicenumber.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,String>("invoice_number"));
		clientname.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,String>("clientname"));
		invoicedate.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,String>("invoice_date"));
		billamount.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,Double>("amount"));
		amtreceived.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,Double>("amtReceived"));
		due.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,Double>("due"));
		status.setCellValueFactory(new PropertyValueFactory<PaymentUnpaidTableBean,String>("status"));
		
		paidserialno.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,Integer>("paidserialno"));
		paidclientname.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,String>("paidclientname"));		
		paidinvoicenumber.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,String>("paidinvoicenumber"));		
		paidinvoicedate.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,String>("paidinvoicedate"));		
		paidpaymentdate.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,String>("paidpaymentDate"));		
		paidpaymentmode.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,String>("paidpaymentMode"));		
		paidbillamount.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,Double>("paidbillamount"));
		paidamtreceived.setCellValueFactory(new PropertyValueFactory<PaymentPaidTableBean,Double>("paidamtReceived"));
		
		try {
			loadSearchByName();
			showUnpaidInvoices();
			showPaidInvoices();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
