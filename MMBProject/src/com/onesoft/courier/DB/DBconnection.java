package com.onesoft.courier.DB;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBconnection {
	
	public Connection ConnectDB()
	{
	Connection con=null;
	
	
	
	try{
		File file = new File("src/new.properties");
		FileInputStream fileInput = new FileInputStream(file);
		Properties  props = new Properties();
		 props.load(fileInput);
		//String mail= props.getProperty("from");
		
		 
		/* System.out.println("Drivers: "+props.getProperty("db_drivers"));
			System.out.println("URL: "+props.getProperty("connection_url"));
			System.out.println("Username: "+props.getProperty("db_username"));
			System.out.println("Password: "+props.getProperty("db_password"));
		*/
		Class.forName(props.getProperty("db_drivers"));
		con=DriverManager.getConnection(props.getProperty("connection_url"),props.getProperty("db_username"),props.getProperty("db_password"));
		
		/*Class.forName("org.postgresql.Driver");
		con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/MMB","postgres","root");*/
		
		
		// System.out.println("\n");
		
		return con;
		
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	return null;

}
	
	public void disconnect(PreparedStatement psmt,Statement st, ResultSet rs, Connection con) throws SQLException
	{
		if(psmt != null)
			psmt.close();
		if(st != null)
			st.close();
		if(rs != null)
			rs.close();
		if(con != null)
			con.close();
		
		//System.out.println("connection closed successfully");
		 System.out.println("\n");
	}
	
}

