package com.onesoft.courier.ratemaster.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SpotRateTableBean {

	private SimpleIntegerProperty sno;
	private SimpleStringProperty client;
	private SimpleStringProperty date;
	private SimpleStringProperty forwarder;
	private SimpleStringProperty network;
	private SimpleStringProperty service;
	private SimpleStringProperty destination;
	private SimpleDoubleProperty saleRate;
	private SimpleDoubleProperty purchaseRate;
	private SimpleDoubleProperty weightFrom;
	private SimpleDoubleProperty weightTo;
	private SimpleStringProperty validTill;
	private SimpleStringProperty approve;

	public SpotRateTableBean(int sno, String client, String date, String forwarder, String network, String service,
			String destination, double saleRate, double purchaseRate, double weightFrom, double weightTo, String validTill, String approve) {
		this.sno = new SimpleIntegerProperty(sno);
		this.client = new SimpleStringProperty(client);
		this.date = new SimpleStringProperty(date);
		this.forwarder = new SimpleStringProperty(forwarder);
		this.network = new SimpleStringProperty(network);
		this.service = new SimpleStringProperty(service);
		this.destination = new SimpleStringProperty(destination);
		this.saleRate = new SimpleDoubleProperty(saleRate);
		this.purchaseRate = new SimpleDoubleProperty(purchaseRate);
		this.weightFrom = new SimpleDoubleProperty(weightFrom);
		this.weightTo = new SimpleDoubleProperty(weightTo);
		this.validTill = new SimpleStringProperty(validTill);
		this.approve = new SimpleStringProperty(approve);
	}

	public int getSno() {
		return sno.get();
	}

	public void setSno(SimpleIntegerProperty sno) {
		this.sno = sno;
	}

	public String getClient() {
		return client.get();
	}

	public void setClient(SimpleStringProperty client) {
		this.client = client;
	}

	public String getForwarder() {
		return forwarder.get();
	}

	public void setForwarder(SimpleStringProperty forwarder) {
		this.forwarder = forwarder;
	}

	public String getNetwork() {
		return network.get();
	}

	public void setNetwork(SimpleStringProperty network) {
		this.network = network;
	}

	public String getService() {
		return service.get();
	}

	public void setService(SimpleStringProperty service) {
		this.service = service;
	}

	public String getDestination() {
		return destination.get();
	}

	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}

	public double getSaleRate() {
		return saleRate.get();
	}

	public void setSaleRate(SimpleDoubleProperty saleRate) {
		this.saleRate = saleRate;
	}

	public double getPurchaseRate() {
		return purchaseRate.get();
	}

	public void setPurchaseRate(SimpleDoubleProperty purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getWeightFrom() {
		return weightFrom.get();
	}

	public void setWeightFrom(SimpleDoubleProperty weightFrom) {
		this.weightFrom = weightFrom;
	}

	public double getWeightTo() {
		return weightTo.get();
	}

	public void setWeightTo(SimpleDoubleProperty weightTo) {
		this.weightTo = weightTo;
	}

	public String getApprove() {
		return approve.get();
	}

	public void setApprove(SimpleStringProperty approve) {
		this.approve = approve;
	}

	public String getDate() {
		return date.get();
	}

	public void setDate(SimpleStringProperty date) {
		this.date = date;
	}

	public String getValidTill() {
		return validTill.get();
	}

	public void setValidTill(SimpleStringProperty validTill) {
		this.validTill = validTill;
	}

}
