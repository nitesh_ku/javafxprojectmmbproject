package com.onesoft.courier.ratemaster.bean;


import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ForwarderRateZoneTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty Zones;
	private SimpleStringProperty da;
	private SimpleStringProperty db;
	private SimpleStringProperty dc;
	private SimpleStringProperty dd;
	private SimpleStringProperty de;
	private SimpleStringProperty df;
	private SimpleStringProperty dg;
	private SimpleStringProperty dh;
	private SimpleStringProperty di;
	private SimpleStringProperty dj;
	private SimpleStringProperty dk;
	private SimpleStringProperty dl;
	private SimpleStringProperty dm;
	
	public ForwarderRateZoneTableBean(int slno,String zones,String da,String db,String dc,String dd,String de,String df,String dg,
			String dh,String di,String dj,String dk,String dl,String dm)
	{
		this.setSlno(new SimpleIntegerProperty(slno));
		this.setZones(new SimpleStringProperty(zones));
		this.da=new SimpleStringProperty(da);
		this.db=new SimpleStringProperty(db);
		this.dc=new SimpleStringProperty(dc);
		this.dd=new SimpleStringProperty(dd);
		this.de=new SimpleStringProperty(de);
		this.df=new SimpleStringProperty(df);
		this.dg=new SimpleStringProperty(dg);
		this.dh=new SimpleStringProperty(dh);
		this.di=new SimpleStringProperty(di);
		this.dj=new SimpleStringProperty(dj);
		this.dk=new SimpleStringProperty(dk);
		this.dl=new SimpleStringProperty(dl);
		this.dm=new SimpleStringProperty(dm);
	}
	
	
	public String getDa() {
		return da.get();
	}
	public void setDa(SimpleStringProperty da) {
		this.da = da;
	}
	public String getDb() {
		return db.get();
	}
	public void setDb(SimpleStringProperty db) {
		this.db = db;
	}
	public String getDc() {
		return dc.get();
	}
	public void setDc(SimpleStringProperty dc) {
		this.dc = dc;
	}
	public String getDd() {
		return dd.get();
	}
	public void setDd(SimpleStringProperty dd) {
		this.dd = dd;
	}
	public String getDe() {
		return de.get();
	}
	public void setDe(SimpleStringProperty de) {
		this.de = de;
	}
	public String getDf() {
		return df.get();
	}
	public void setDf(SimpleStringProperty df) {
		this.df = df;
	}
	public String getDg() {
		return dg.get();
	}
	public void setDg(SimpleStringProperty dg) {
		this.dg = dg;
	}
	public String getDh() {
		return dh.get();
	}
	public void setDh(SimpleStringProperty dh) {
		this.dh = dh;
	}
	public String getDi() {
		return di.get();
	}
	public void setDi(SimpleStringProperty di) {
		this.di = di;
	}
	public String getDj() {
		return dj.get();
	}
	public void setDj(SimpleStringProperty dj) {
		this.dj = dj;
	}
	public String getDk() {
		return dk.get();
	}
	public void setDk(SimpleStringProperty dk) {
		this.dk = dk;
	}
	public String getDl() {
		return dl.get();
	}
	public void setDl(SimpleStringProperty dl) {
		this.dl = dl;
	}
	public String getDm() {
		return dm.get();
	}
	public void setDm(SimpleStringProperty dm) {
		this.dm = dm;
	}


	public String getZones() {
		return Zones.get();
	}


	public void setZones(SimpleStringProperty zones) {
		Zones = zones;
	}


	public int getSlno() {
		return slno.get();
	}


	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	
	
	
	
	

}
