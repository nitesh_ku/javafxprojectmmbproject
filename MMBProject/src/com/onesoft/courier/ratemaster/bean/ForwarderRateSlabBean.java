package com.onesoft.courier.ratemaster.bean;

public class ForwarderRateSlabBean {
	
	private String forwarder;
	private String network;
	private String service;
	private double basic_range;
	private double add_slab;
	private double add_range;
	private String kg;
	
	
	
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public double getBasic_range() {
		return basic_range;
	}
	public void setBasic_range(double basic_range) {
		this.basic_range = basic_range;
	}
	public double getAdd_slab() {
		return add_slab;
	}
	public void setAdd_slab(double add_slab) {
		this.add_slab = add_slab;
	}
	public double getAdd_range() {
		return add_range;
	}
	public void setAdd_range(double add_range) {
		this.add_range = add_range;
	}
	public String getKg() {
		return kg;
	}
	public void setKg(String kg) {
		this.kg = kg;
	}
	public String getForwarder() {
		return forwarder;
	}
	public void setForwarder(String forwarder) {
		this.forwarder = forwarder;
	}
	

}
