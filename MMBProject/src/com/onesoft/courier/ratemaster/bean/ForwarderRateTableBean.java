package com.onesoft.courier.ratemaster.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ForwarderRateTableBean {

	private SimpleIntegerProperty slno;
	private SimpleIntegerProperty slno_in_db;
	
	//private SimpleStringProperty clientCode;
	private SimpleStringProperty networkCode;
	private SimpleStringProperty serviceCode;
	private SimpleStringProperty fromZoneOrCityCode;
	private SimpleStringProperty ToZoneOrCityCode;
	private SimpleStringProperty slabType;
	private SimpleStringProperty mappingType;
	private SimpleDoubleProperty weightUpto;
	private SimpleDoubleProperty slab;
	private SimpleDoubleProperty dox;
	private SimpleDoubleProperty nonDox;
	
	public ForwarderRateTableBean(int slno, int slno_in_db,String networkcode, String slabtype, double weight, double slab, double dox, double nondox)
	{
		this.setSlno_in_db(new SimpleIntegerProperty(slno_in_db));
		this.slno=new SimpleIntegerProperty(slno);
		this.networkCode=new SimpleStringProperty(networkcode);
		this.slabType=new SimpleStringProperty(slabtype);
		this.weightUpto=new SimpleDoubleProperty(weight);
		this.slab=new SimpleDoubleProperty(slab);
		this.dox=new SimpleDoubleProperty(dox);
		this.nonDox=new SimpleDoubleProperty(nondox);
		
	}
	
	

	public String getNetworkCode() {
		return networkCode.get();
	}
	public void setNetworkCode(SimpleStringProperty networkCode) {
		this.networkCode = networkCode;
	}
	public String getServiceCode() {
		return serviceCode.get();
	}
	public void setServiceCode(SimpleStringProperty serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getFromZoneOrCityCode() {
		return fromZoneOrCityCode.get();
	}
	public void setFromZoneOrCityCode(SimpleStringProperty fromZoneOrCityCode) {
		this.fromZoneOrCityCode = fromZoneOrCityCode;
	}
	public String getToZoneOrCityCode() {
		return ToZoneOrCityCode.get();
	}
	public void setToZoneOrCityCode(SimpleStringProperty toZoneOrCityCode) {
		ToZoneOrCityCode = toZoneOrCityCode;
	}
	public String getSlabType() {
		return slabType.get();
	}
	public void setSlabType(SimpleStringProperty slabType) {
		this.slabType = slabType;
	}
	public String getMappingType() {
		return mappingType.get();
	}
	public void setMappingType(SimpleStringProperty mappingType) {
		this.mappingType = mappingType;
	}
	public double getWeightUpto() {
		return weightUpto.get();
	}
	public void setWeightUpto(SimpleDoubleProperty weightUpto) {
		this.weightUpto = weightUpto;
	}
	public double getSlab() {
		return slab.get();
	}
	public void setSlab(SimpleDoubleProperty slab) {
		this.slab = slab;
	}
	public double getDox() {
		return dox.get();
	}
	public void setDox(SimpleDoubleProperty dox) {
		this.dox = dox;
	}
	public double getNonDox() {
		return nonDox.get();
	}
	public void setNonDox(SimpleDoubleProperty nonDox) {
		this.nonDox = nonDox;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public int getSlno_in_db() {
		return slno_in_db.get();
	}
	public void setSlno_in_db(SimpleIntegerProperty slno_in_db) {
		this.slno_in_db = slno_in_db;
	}
	
	
	

}
