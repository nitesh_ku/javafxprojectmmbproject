package com.onesoft.courier.ratemaster.bean;


import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ClientRateZoneTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty Zones;
	private SimpleStringProperty da;
	private SimpleStringProperty db;
	private SimpleStringProperty dc;
	private SimpleStringProperty dd;
	private SimpleStringProperty de;
	private SimpleStringProperty df;
	private SimpleStringProperty dg;
	private SimpleStringProperty dh;
	private SimpleStringProperty di;
	private SimpleStringProperty dj;
	private SimpleStringProperty dk;
	private SimpleStringProperty dl;
	private SimpleStringProperty dm;
	private SimpleStringProperty dn;
	private SimpleStringProperty do1;
	private SimpleStringProperty dp;
	private SimpleStringProperty dq;
	private SimpleStringProperty dr;
	private SimpleStringProperty ds;
	private SimpleStringProperty dt;
	private SimpleStringProperty add_reverse;
	private SimpleStringProperty kg_reverse;
	
	
	public ClientRateZoneTableBean(int slno,String zones,String da,String db,String dc,String dd,String de,String df,String dg,
			String dh,String di,String dj,String dk,String dl,String dm,String dn,String do1,String dp,String dq,String dr,String ds,String dt,String add_reverse,String kg_reverse)
	{
		this.setSlno(new SimpleIntegerProperty(slno));
		this.setZones(new SimpleStringProperty(zones));
		this.da=new SimpleStringProperty(da);
		this.db=new SimpleStringProperty(db);
		this.dc=new SimpleStringProperty(dc);
		this.dd=new SimpleStringProperty(dd);
		this.de=new SimpleStringProperty(de);
		this.df=new SimpleStringProperty(df);
		this.dg=new SimpleStringProperty(dg);
		this.dh=new SimpleStringProperty(dh);
		this.di=new SimpleStringProperty(di);
		this.dj=new SimpleStringProperty(dj);
		this.dk=new SimpleStringProperty(dk);
		this.dl=new SimpleStringProperty(dl);
		this.dm=new SimpleStringProperty(dm);
		this.dn=new SimpleStringProperty(dn);
		this.do1=new SimpleStringProperty(do1);
		this.dp=new SimpleStringProperty(dp);
		this.dq=new SimpleStringProperty(dq);
		this.dr=new SimpleStringProperty(dr);
		this.ds=new SimpleStringProperty(ds);
		this.dt=new SimpleStringProperty(dt);
		this.add_reverse=new SimpleStringProperty(add_reverse);
		this.kg_reverse=new SimpleStringProperty(kg_reverse);
		
	}
	
	
	public String getDa() {
		return da.get();
	}
	public void setDa(SimpleStringProperty da) {
		this.da = da;
	}
	public String getDb() {
		return db.get();
	}
	public void setDb(SimpleStringProperty db) {
		this.db = db;
	}
	public String getDc() {
		return dc.get();
	}
	public void setDc(SimpleStringProperty dc) {
		this.dc = dc;
	}
	public String getDd() {
		return dd.get();
	}
	public void setDd(SimpleStringProperty dd) {
		this.dd = dd;
	}
	public String getDe() {
		return de.get();
	}
	public void setDe(SimpleStringProperty de) {
		this.de = de;
	}
	public String getDf() {
		return df.get();
	}
	public void setDf(SimpleStringProperty df) {
		this.df = df;
	}
	public String getDg() {
		return dg.get();
	}
	public void setDg(SimpleStringProperty dg) {
		this.dg = dg;
	}
	public String getDh() {
		return dh.get();
	}
	public void setDh(SimpleStringProperty dh) {
		this.dh = dh;
	}
	public String getDi() {
		return di.get();
	}
	public void setDi(SimpleStringProperty di) {
		this.di = di;
	}
	public String getDj() {
		return dj.get();
	}
	public void setDj(SimpleStringProperty dj) {
		this.dj = dj;
	}
	public String getDk() {
		return dk.get();
	}
	public void setDk(SimpleStringProperty dk) {
		this.dk = dk;
	}
	public String getDl() {
		return dl.get();
	}
	public void setDl(SimpleStringProperty dl) {
		this.dl = dl;
	}
	public String getDm() {
		return dm.get();
	}
	public void setDm(SimpleStringProperty dm) {
		this.dm = dm;
	}
	public String getZones() {
		return Zones.get();
	}
	public void setZones(SimpleStringProperty zones) {
		Zones = zones;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getDn() {
		return dn.get();
	}
	public void setDn(SimpleStringProperty dn) {
		this.dn = dn;
	}
	public String getDo1() {
		return do1.get();
	}
	public void setDo1(SimpleStringProperty do1) {
		this.do1 = do1;
	}
	public String getDp() {
		return dp.get();
	}
	public void setDp(SimpleStringProperty dp) {
		this.dp = dp;
	}
	public String getDq() {
		return dq.get();
	}
	public void setDq(SimpleStringProperty dq) {
		this.dq = dq;
	}
	public String getDr() {
		return dr.get();
	}
	public void setDr(SimpleStringProperty dr) {
		this.dr = dr;
	}
	public String getDs() {
		return ds.get();
	}
	public void setDs(SimpleStringProperty ds) {
		this.ds = ds;
	}
	public String getDt() {
		return dt.get();
	}
	public void setDt(SimpleStringProperty dt) {
		this.dt = dt;
	}
	public String getAdd_reverse() {
		return add_reverse.get();
	}
	public void setAdd_reverse(SimpleStringProperty add_reverse) {
		this.add_reverse = add_reverse;
	}
	public String getKg_reverse() {
		return kg_reverse.get();
	}
	public void setKg_reverse(SimpleStringProperty kg_reverse) {
		this.kg_reverse = kg_reverse;
	}
	
	
	
	
	

}
