package com.onesoft.courier.ratemaster.bean;

public class ReadSheetWiseExcelFileControllerBean {
	public ReadSheetWiseExcelFileControllerBean (){}

	private String client;
	private String network;
	private String service;
	private String mapingZone;
     private String source;
     private String destinetion;
     private String type;
     private Double weight;
     private Double slab;
     private Double dox;
     private Double nonDox;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestinetion() {
		return destinetion;
	}
	public void setDestinetion(String destinetion) {
		this.destinetion = destinetion;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getSlab() {
		return slab;
	}
	public void setSlab(Double slab) {
		this.slab = slab;
	}
	public Double getDox() {
		return dox;
	}
	public void setDox(Double dox) {
		this.dox = dox;
	}
	public Double getNonDox() {
		return nonDox;
	}
	public void setNonDox(Double nonDox) {
		this.nonDox = nonDox;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getMapingZone() {
		return mapingZone;
	}
	public void setMapingZone(String mapingZone) {
		this.mapingZone = mapingZone;
	}
     
}
