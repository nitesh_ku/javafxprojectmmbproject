package com.onesoft.courier.ratemaster.bean;

public class ClientRateZoneBean {
	
	
	private String sheetName;
	private String client;
	private String network;
	private String service;
	private String zone_code;
	
	private double basic_range;
	private double add_slab;
	private double add_range;

	private String type;
	private String kg;
	

	private String add_rate_colmn;
	private String kg_rate_colmn;
	private String da;
	private String db;
	private String dc;
	private String dd;
	private String de;
	private String df;
	private String dg;
	private String dh;
	private String di;
	private String dj;
	private String dk;
	private String dl;
	private String dm;
	private String dn;
	private String do1;
	private String dp;
	private String dq;
	private String dr;
	private String ds;
	private String dt;
	private String additional_reverse;
	private String kg_reverse;
	
	
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public String getDo1() {
		return do1;
	}
	public void setDo1(String do1) {
		this.do1 = do1;
	}
	public String getDp() {
		return dp;
	}
	public void setDp(String dp) {
		this.dp = dp;
	}
	public String getDq() {
		return dq;
	}
	public void setDq(String dq) {
		this.dq = dq;
	}
	public String getDr() {
		return dr;
	}
	public void setDr(String dr) {
		this.dr = dr;
	}
	public String getDs() {
		return ds;
	}
	public void setDs(String ds) {
		this.ds = ds;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getZone_code() {
		return zone_code;
	}
	public void setZone_code(String zone_code) {
		this.zone_code = zone_code;
	}
	public String getDa() {
		return da;
	}
	public void setDa(String da) {
		this.da = da;
	}
	public String getDb() {
		return db;
	}
	public void setDb(String db) {
		this.db = db;
	}
	public String getDc() {
		return dc;
	}
	public void setDc(String dc) {
		this.dc = dc;
	}
	public String getDd() {
		return dd;
	}
	public void setDd(String dd) {
		this.dd = dd;
	}
	public String getDe() {
		return de;
	}
	public void setDe(String de) {
		this.de = de;
	}
	public String getDf() {
		return df;
	}
	public void setDf(String df) {
		this.df = df;
	}
	public String getDg() {
		return dg;
	}
	public void setDg(String dg) {
		this.dg = dg;
	}
	public String getDh() {
		return dh;
	}
	public void setDh(String dh) {
		this.dh = dh;
	}
	public String getDi() {
		return di;
	}
	public void setDi(String di) {
		this.di = di;
	}
	public String getDj() {
		return dj;
	}
	public void setDj(String dj) {
		this.dj = dj;
	}
	public String getDk() {
		return dk;
	}
	public void setDk(String dk) {
		this.dk = dk;
	}
	public String getDl() {
		return dl;
	}
	public void setDl(String dl) {
		this.dl = dl;
	}
	public String getDm() {
		return dm;
	}
	public void setDm(String dm) {
		this.dm = dm;
	}
	
	public double getBasic_range() {
		return basic_range;
	}
	public void setBasic_range(double basic_range) {
		this.basic_range = basic_range;
	}
	public double getAdd_slab() {
		return add_slab;
	}
	public void setAdd_slab(double add_slab) {
		this.add_slab = add_slab;
	}
	public double getAdd_range() {
		return add_range;
	}
	public void setAdd_range(double add_range) {
		this.add_range = add_range;
	}
	public String getKg() {
		return kg;
	}
	public void setKg(String kg) {
		this.kg = kg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAdd_rate_colmn() {
		return add_rate_colmn;
	}
	public void setAdd_rate_colmn(String add_rate_colmn) {
		this.add_rate_colmn = add_rate_colmn;
	}
	public String getKg_rate_colmn() {
		return kg_rate_colmn;
	}
	public void setKg_rate_colmn(String kg_rate_colmn) {
		this.kg_rate_colmn = kg_rate_colmn;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public String getAdditional_reverse() {
		return additional_reverse;
	}
	public void setAdditional_reverse(String additional_reverse) {
		this.additional_reverse = additional_reverse;
	}
	public String getKg_reverse() {
		return kg_reverse;
	}
	public void setKg_reverse(String kg_reverse) {
		this.kg_reverse = kg_reverse;
	}

}
