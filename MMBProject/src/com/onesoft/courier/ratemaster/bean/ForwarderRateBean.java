package com.onesoft.courier.ratemaster.bean;

public class ForwarderRateBean {
	
	private int slno_in_db;
	
	private String networkCode;
	private String serviceCode;
	private String fromZoneOrCityCode;
	private String ToZoneOrCityCode;
	private String slabType;
	private String mappingType;
	private double weightUpto;
	private double slab;
	private double dox;
	private double nonDox;
	
	private String sheetName;
	

	public String getNetworkCode() {
		return networkCode;
	}
	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getFromZoneOrCityCode() {
		return fromZoneOrCityCode;
	}
	public void setFromZoneOrCityCode(String fromZoneOrCityCode) {
		this.fromZoneOrCityCode = fromZoneOrCityCode;
	}
	public String getToZoneOrCityCode() {
		return ToZoneOrCityCode;
	}
	public void setToZoneOrCityCode(String toZoneOrCityCode) {
		ToZoneOrCityCode = toZoneOrCityCode;
	}
	public String getSlabType() {
		return slabType;
	}
	public void setSlabType(String slabType) {
		this.slabType = slabType;
	}
	public double getWeightUpto() {
		return weightUpto;
	}
	public void setWeightUpto(double weightUpto) {
		this.weightUpto = weightUpto;
	}
	public double getSlab() {
		return slab;
	}
	public void setSlab(double slab) {
		this.slab = slab;
	}
	public double getDox() {
		return dox;
	}
	public void setDox(double dox) {
		this.dox = dox;
	}
	public double getNonDox() {
		return nonDox;
	}
	public void setNonDox(double nonDox) {
		this.nonDox = nonDox;
	}
	public String getMappingType() {
		return mappingType;
	}
	public void setMappingType(String mappingType) {
		this.mappingType = mappingType;
	}
	public int getSlno_in_db() {
		return slno_in_db;
	}
	public void setSlno_in_db(int slno_in_db) {
		this.slno_in_db = slno_in_db;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

}
