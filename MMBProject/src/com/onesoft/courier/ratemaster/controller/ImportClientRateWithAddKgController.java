package com.onesoft.courier.ratemaster.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.sound.midi.SysexMessage;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.controlsfx.control.CheckListView;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.ratemaster.bean.ClientRateBean;

import com.onesoft.courier.ratemaster.bean.ClientRateZoneBean;
import com.onesoft.courier.ratemaster.bean.ClientRateZoneTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Rate_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ImportClientRateWithAddKgController implements Initializable {

	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	private ObservableList<ClientRateZoneTableBean> tabledata_ClientRate=FXCollections.observableArrayList();
	private List<String> list_FixedZones= Arrays.asList("DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM","DN","DO","DP","DQ","DR","DS","DT");
	
	
	
	public String clientCode_global=null;
	public String networkCode_global=null;
	public String serviceCode_global=null;
	
	private List<String> list_Clients=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	public static List<String> list_zones=new ArrayList<>();
	
	/*private ObservableList<String> faultSheetName=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList2=FXCollections.observableArrayList();*/
	
	public int update_insert_count=0;
	boolean isExcelFileIsCorrect=false;
	boolean isServiceAvailableInExcelFile=false;
	
	private List<ClientRateZoneBean> list_ZoneWiseClientRate=new ArrayList<>();
	private ObservableList<String> listViewServiceUploadItems=FXCollections.observableArrayList();
	//private ObservableList<String> listViewServiceItems=FXCollections.observableArrayList();
	
	private List<String> listClientNetworkServiceName=new ArrayList<>();
	private List<String> list_IsClientRateAvailable_Status=new ArrayList<>();
	
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	int count=0;
	
	boolean flag_emptySheet=false;
	String alertForEmptySheet=null;
	

	@FXML
	private Label labBasic_MinimumCharges;
	
	@FXML
	private Label labAdd_Slab;
	
	@FXML
	private Label labAdd_Range;
	
	@FXML
	private Label labService;
	
	@FXML
	private Label labBrowseFile;

	/*@FXML
	private Label labBasic_Range_View;
	
	@FXML
	private Label labAdd_Slab_View;
	
	@FXML
	private Label labAdd_Range_View;
	
	@FXML
	private Label labService_View;*/
	
	
	@FXML
	private ImageView imgViewSearchIcon_ClientRateImport;
	 
	@FXML
	private TextField txtClient;

	@FXML
	private TextField txtBrowseExcel;
	
	@FXML
	private TextField txtBasic_range;
	
	@FXML
	private TextField txtAdd_slab;
	
	@FXML
	private TextField txtAdd_range;
	
	/*@FXML
	private TextField txt_Basic_range_View;
	
	@FXML
	private TextField txtAdd_slab_View;
	
	@FXML
	private TextField txtAdd_range_View;*/
	
	@FXML
	private CheckBox checkBox_Kg;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	/*@FXML
	private ComboBox<String> comboBoxService;*/

	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnSave;

	@FXML
	private Button btnReset;

	@FXML
	private Button btnUpdate;
	
	/*@FXML
	private ListView<String> listViewService;*/
	
	/*@FXML
	private ListView<String> listView_Service_upload;*/
	
	/*@FXML
	private ListView<String> listViewService;*/
	
	@FXML
	private RadioButton rdBtnAddUpdateSlab;
	
	@FXML
	private RadioButton rdBtnUploadFile;
	
	
// ===============================================	
	
	@FXML
	private TableView<ClientRateZoneTableBean> tableAddClientRate;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Integer> tableCol_slno;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, String> tableCol_Zone;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_da;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_db;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dc;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dd;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_de;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_df;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dg;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dh;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_di;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dj;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dk;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dl;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dm;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dn;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_do1;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dp;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dq;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dr;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_ds;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dt;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_AddReverse;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_KgReverse;
	
	
	
	
// *******************************************************************************

	public void loadClients() throws SQLException 
	{
		list_Clients.clear();
		
		new LoadClients().loadClientWithName();
			for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_Clients.add(bean.getClientName() + " | " + bean.getClientCode());
		}

		TextFields.bindAutoCompletion(txtClient, list_Clients);
	}		
		
// ******************************************************************************
		
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	

		
// ******************************************************************************
		
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();

		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();

			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());

			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}

		/*for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);*/

		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
		}	
	
	
// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		
		if (comboBoxNetwork.getValue() != null) 
		{
			listViewServiceUploadItems.clear();
			//listViewServiceItems.clear();
			set_services.clear();
			
			comboBoxServiceItems.clear(); 

			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}

			/*
				txtAdd_range.clear();
				txtAdd_slab.clear();
				txtBasic_range.clear();*/
				
				for(String srvcCode: set_services)
				{
					comboBoxServiceItems.add(srvcCode);
					listViewServiceUploadItems.add(srvcCode);
				}
				comboBoxService.setItems(comboBoxServiceItems);
				//listView_Service_upload.setItems(listViewServiceUploadItems);
			
			
			//checkListViewService.getSelectionModel().getSelectedItem();
			/*
				listViewService.setCellFactory(lv -> {
				    ListCell<String> cell = new ListCell<String>() {
				        @Override
				        protected void updateItem(String item, boolean empty) {
				            super.updateItem(item, empty);
				            if (empty) {
				                setText(null);
				            } else {
				                setText(item.toString());
				            }
				        }
				    };
				    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
				        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
				            String item = cell.getItem();
				           
				            
				            try {
								System.out.println(item);
								loadClientRateTable(item);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

				        }
				    });
				    return cell ;
				});
		*/
			//comboBoxService.setItems(comboBoxServiceItems);
		}
	}

	
// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
		
		
		
		if (n.getId().equals("rdBtnAddUpdateSlab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnUploadFile.requestFocus();
			}
		}
		
		if (n.getId().equals("rdBtnUploadFile")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtClient.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				comboBoxService.requestFocus();
			}
		}
		
		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				if(rdBtnAddUpdateSlab.isSelected()==true)
				{
					txtBasic_range.requestFocus();
				}
				else
				{
					btnBrowse.requestFocus();
				}
			}
		}

		else if (n.getId().equals("txtBasic_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				checkBox_Kg.requestFocus();
			}
		}
		
		
		else if (n.getId().equals("checkBox_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(checkBox_Kg.isSelected()==true)
				{
					btnSave.requestFocus();
				}
				else
				{
					txtAdd_slab.requestFocus();
				}
				
			}
		}
		
		else if (n.getId().equals("txtAdd_slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				txtAdd_range.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtAdd_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
		
				
				
				btnSave.requestFocus();
			}
		}
	
		
		else if (n.getId().equals("btnBrowse")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				fileAttachment();
			}
		}

		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				//checkClientRateSlabDetails();
				setValidation();
			}
		}
		
		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				//checkClientRateSlabDetails();
				setValidation();
			}
		}
	}	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		//double test=Double.valueOf(txtWeightUpto.getText());


		if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Client");
			alert.showAndWait();
			txtClient.requestFocus();
		} 

		else if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Network field is Empty!");
			alert.showAndWait();
			comboBoxNetwork.requestFocus();
		}

	

		else if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select Excel File");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}
	
		else
		{
			
			if(rdBtnAddUpdateSlab.isSelected()==true)
			{
				checkClientRateSlabDetails();	
			}
			else
			{
				progressBarTest();
			}
			
			//progressBarTest();
		}

	}		
	
// ******************************************************************************
	
	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
			//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
		
    	selectedFile = fc.showOpenDialog(null);
    	
    	if(selectedFile != null)
    	{
    		a=5*1024*1024;
    		size=selectedFile.length();	
    		
    	   	if(size<a)
    	   	{
    	   		txtBrowseExcel.getText();
    	   		txtBrowseExcel.setText(selectedFile.getAbsolutePath());
    	   		btnUpdate.requestFocus();
    	   	}
    	   	else
    	   	{
    	   		txtBrowseExcel.clear();
    	   	}
    	}
    	else
    	{
    		size=0;
    	}
	}
	
	
// ******************************************************************************	
	
	public void progressBarTest() 
	{
        Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
        ProgressBar progressBar = new ProgressBar();

        progressBar.setMinWidth(400);
        progressBar.setVisible(true);

        VBox updatePane = new VBox();
        updatePane.setPadding(new Insets(35));
        updatePane.setSpacing(3.0d);
        Label lbl = new Label("Processing.......");
        lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
        updatePane.getChildren().add(lbl);
        updatePane.getChildren().addAll(progressBar);

        taskUpdateStage.setScene(new Scene(updatePane));
        taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
        taskUpdateStage.show();
        
        task = new Task<Void>() 
        {
        	public Void call() throws Exception 
        	{
        		save();
                
        		int max = 1000;
                for (int i = 1; i <= max; i++) 
                {
                           // Thread.sleep(100);
                            /*if (BirtReportExportCon.FLAG == true) {
                                   break;
                            }*/
                }
                return null;
        	}
        };
        
        
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
               public void handle(WorkerStateEvent t) {
                      taskUpdateStage.close();
                      
                  if(isServiceAvailableInExcelFile==true)
                  {
                      
                    if(isExcelFileIsCorrect==false)
          	   		{
          	   		Alert alert = new Alert(AlertType.INFORMATION);
      				alert.setTitle("Import Alert");
      				//alert.setTitle("Empty Field Validation");
      				alert.setContentText("Please select correct Excel format file");
      				alert.showAndWait();
      				isExcelFileIsCorrect=true;
          	   		}
                    else
                    {
                    	System.out.println("List size :: "+list_ZoneWiseClientRate.size()+" | Count :: "+update_insert_count);
                    	Alert alert = new Alert(AlertType.INFORMATION);
          				alert.setTitle("Task Complete or Not Complete");
                      if (list_ZoneWiseClientRate.size()==update_insert_count) {
          				
          				//alert.setTitle("Empty Field Validation");
          				alert.setContentText("Rates successfully uploaded...");
          				alert.showAndWait();
                      }	
                      else if(update_insert_count==0)
                      {
                    	  alert.setContentText("Rates not uploaded...\nPlease Check");
            				alert.showAndWait();
                      }
                      else if(update_insert_count<list_ZoneWiseClientRate.size())
                      {
                    	  alert.setContentText("Rates Partially uploaded...");
            				alert.showAndWait();
                      }
          			}
                    isServiceAvailableInExcelFile=false;
                  }
                  else
                  {
                	  Alert alert = new Alert(AlertType.INFORMATION);
        				alert.setTitle("Import Alert");
        				//alert.setTitle("Empty Field Validation");
        				alert.setContentText("Service not avialable in selected file...\nPlease check");
        				alert.showAndWait();
        				isExcelFileIsCorrect=true;
                  }
                	 
               }
        });
        
        
        progressBar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();
      
	}
	
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
		//importExcelFile(selectedFile);
	}
	
// ******************************************************************************	
	
	public void importNewExcel(File selectedFile) throws IOException
	{
		list_ZoneWiseClientRate.clear();
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			
			
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			
			
			clientCode_global=clientCode[1];
			networkCode_global=networkCode[1];
			//serviceCode_global=fileName;
			
			System.out.println("File Name: "+fileName);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			
			//ObservableList<String> selectedService=listView_Service_upload.getCheckModel().getCheckedItems();
			/*
			for(String s:selectedService)
			{
				System.err.println("Selected Service for KG: "+s);
			}*/
			int matchSheetCount=0;
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName().toUpperCase();
				
				
				if(comboBoxServiceItems.contains(sheetName))
				{
					matchSheetCount++;
				}
			}
			
			int index=0;

			
			if(matchSheetCount>0)
			{
				System.out.println("Service Available in sheet");
				isServiceAvailableInExcelFile=true;
			}
			else
			{
				isServiceAvailableInExcelFile=false;
				System.out.println("Service not Available in sheet");
			}


		
			if(isServiceAvailableInExcelFile==true)
			{
				for(int i=0;i<workbook.getNumberOfSheets();i++)
				{
					Sheet Sheet = workbook.getSheetAt(i);
					String sheetName=Sheet.getSheetName().toUpperCase();
				
					System.out.println("Sheet Name >?>?>?>?>?>?>?>?>?>?>?>?>?>?>?>? "+sheetName);
					
					if(comboBoxServiceItems.contains(sheetName))
					{
						HSSFRow row;
						HSSFRow row_add_kg;
						serviceCode_global=sheetName;
						
						list_zones.clear();
					
						if(Sheet.getLastRowNum()!=0)
						{
							int maxCell=0;
							for(int k=1;k<=Sheet.getLastRowNum();k++)
							{
								row=(HSSFRow) Sheet.getRow(k);
								maxCell=  row.getLastCellNum();
								System.out.println("Total Columns >> "+maxCell);
								break;
								
							}
							
							if(maxCell>=2 && maxCell<=23)
							{
								isExcelFileIsCorrect=true;
								for(int k=1;k<=Sheet.getLastRowNum();k++)
								{
									row=(HSSFRow) Sheet.getRow(k);
									row_add_kg=(HSSFRow) Sheet.getRow(0);
								
									if(row.getCell(0).toString().equals("Additional") || row.getCell(0).toString().equals("KG"))
									{
										
									}
									else
									{
											list_zones.add(row.getCell(0).toString());
									}
									
									ClientRateZoneBean clZoneBean=new ClientRateZoneBean();

									clZoneBean.setSheetName(sheetName);
									clZoneBean.setClient(clientCode[1]);
									clZoneBean.setNetwork(networkCode[1]);
									clZoneBean.setService(sheetName);
									
									
									
									if(row.getCell(0).toString().equals("DO"))
									{
										clZoneBean.setZone_code("DO");
									}
									else
									{
										clZoneBean.setZone_code(row.getCell(0).toString());
									}


									if(row.getCell(1)==null || row_add_kg.getCell(1).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(1).toString().equals("KG_R"))
									{
										clZoneBean.setDa("0");
									}
									else
									{
										clZoneBean.setDa(row.getCell(1).toString());
									}
									
									if(row.getCell(2)==null || row_add_kg.getCell(2).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(2).toString().equals("KG_R"))
									{
										clZoneBean.setDb("0");
									}
									else
									{
										clZoneBean.setDb(row.getCell(2).toString());
									}

									if(row.getCell(3)==null || row_add_kg.getCell(3).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(3).toString().equals("KG_R"))
									{
										clZoneBean.setDc("0");
									}
									else
									{
										clZoneBean.setDc(row.getCell(3).toString());
									}

									if(row.getCell(4)==null || row_add_kg.getCell(4).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(4).toString().equals("KG_R"))
									{
										clZoneBean.setDd("0");
									}
									else
									{
										clZoneBean.setDd(row.getCell(4).toString());
									}

									if(row.getCell(5)==null || row_add_kg.getCell(5).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(5).toString().equals("KG_R"))
									{
										clZoneBean.setDe("0");
									}
									else
									{
										clZoneBean.setDe(row.getCell(5).toString());
									}

									if(row.getCell(6)==null || row_add_kg.getCell(6).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(6).toString().equals("KG_R"))
									{
										clZoneBean.setDf("0");
									}
									else
									{
										clZoneBean.setDf(row.getCell(6).toString());
									}

									if(row.getCell(7)==null || row_add_kg.getCell(7).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(7).toString().equals("KG_R"))
									{
										clZoneBean.setDg("0");
									}
									else
									{
										clZoneBean.setDg(row.getCell(7).toString());
									}

									if(row.getCell(8)==null || row_add_kg.getCell(8).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(8).toString().equals("KG_R"))
									{
										clZoneBean.setDh("0");
									}
									else
									{
										clZoneBean.setDh(row.getCell(8).toString());
									}

									if(row.getCell(9)==null || row_add_kg.getCell(9).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(9).toString().equals("KG_R"))
									{
										clZoneBean.setDi("0");
									}
									else
									{
										clZoneBean.setDi(row.getCell(9).toString());
									}

									if(row.getCell(10)==null || row_add_kg.getCell(10).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(10).toString().equals("KG_R"))
									{
										clZoneBean.setDj("0");
									}
									else
									{
										clZoneBean.setDj(row.getCell(10).toString());
									}

									if(row.getCell(11)==null || row_add_kg.getCell(11).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(11).toString().equals("KG_R"))
									{
										clZoneBean.setDk("0");
									}
									else
									{
										clZoneBean.setDk(row.getCell(11).toString());
									}

									if(row.getCell(12)==null || row_add_kg.getCell(12).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(12).toString().equals("KG_R"))
									{
										clZoneBean.setDl("0");
									}
									else
									{
										clZoneBean.setDl(row.getCell(12).toString());
									}

									if(row.getCell(13)==null || row_add_kg.getCell(13).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(13).toString().equals("KG_R"))
									{
										clZoneBean.setDm("0");
									}
									else
									{
										clZoneBean.setDm(row.getCell(13).toString());
									}

									if(row.getCell(14)==null || row_add_kg.getCell(14).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(14).toString().equals("KG_R"))
									{
										clZoneBean.setDn("0");
									}
									else
									{
										clZoneBean.setDn(row.getCell(14).toString());
									}

									if(row.getCell(15)==null || row_add_kg.getCell(15).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(15).toString().equals("KG_R"))
									{
										clZoneBean.setDo1("0");
									}
									else
									{
										clZoneBean.setDo1(row.getCell(15).toString());
									}

									if(row.getCell(16)==null || row_add_kg.getCell(16).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(16).toString().equals("KG_R"))
									{
										clZoneBean.setDp("0");
									}
									else
									{
										clZoneBean.setDp(row.getCell(16).toString());
									}

									if(row.getCell(17)==null || row_add_kg.getCell(17).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(17).toString().equals("KG_R"))
									{
										clZoneBean.setDq("0");
									}
									else
									{
										clZoneBean.setDq(row.getCell(17).toString());
									}

									if(row.getCell(18)==null || row_add_kg.getCell(18).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(18).toString().equals("KG_R"))
									{
										clZoneBean.setDr("0");
									}
									else
									{
										clZoneBean.setDr(row.getCell(18).toString());
									}

									if(row.getCell(19)==null || row_add_kg.getCell(19).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(19).toString().equals("KG_R"))
									{
										clZoneBean.setDs("0");
									}
									else
									{
										clZoneBean.setDs(row.getCell(19).toString());
									}

									if(row.getCell(20)==null || row_add_kg.getCell(20).toString().equals("ADDITIONAL_R") || row_add_kg.getCell(20).toString().equals("KG_R"))
									{
										clZoneBean.setDt("0");
									}
									else
									{
										clZoneBean.setDt(row.getCell(20).toString());
									}
									
									
			// ================= Additional Rate ========================	
									
									index++;
									System.out.println("Entry ============== "+index);
									
									for(int add_index=1;add_index<=maxCell;add_index++)
									{
										if(row_add_kg.getCell(add_index)==null)
										{
											/*clZoneBean.setAdditional_reverse("0");
											clZoneBean.setKg_reverse("0");*/
										}
										else if(row_add_kg.getCell(add_index).toString().equals("ADDITIONAL_R"))
										{
											if(row.getCell(add_index)!=null)
											{
												clZoneBean.setAdditional_reverse(row.getCell(add_index).toString());
												System.out.println("Additional Value : "+clZoneBean.getAdditional_reverse());
											}
											else
											{
												clZoneBean.setAdditional_reverse("0");
												System.out.println("KG Value : "+clZoneBean.getAdditional_reverse());
											}
										}
										else if(row_add_kg.getCell(add_index).toString().equals("KG_R"))
										{
											if(row.getCell(add_index)!=null)
											{
												clZoneBean.setKg_reverse(row.getCell(add_index).toString());
												System.out.println("KG Value : "+clZoneBean.getKg_reverse());
											}
											else
											{
												clZoneBean.setKg_reverse("0");
												System.out.println("KG Value : "+clZoneBean.getKg_reverse());
											}
										}
									
									}
									
									
									list_ZoneWiseClientRate.add(clZoneBean);
								}
								
								
								
							}
							else
							{
								isExcelFileIsCorrect=false;
							}
						}
						
						
						for(String fixZon:list_FixedZones)
						{
							if(!list_zones.contains(fixZon))
							{
								deleteExtraZoneRates(clientCode[1],networkCode[1],sheetName,fixZon);
							}
						}
						
					}
					else
					{
						System.err.println(sheetName+" :: service not available with given network :: "+comboBoxNetwork.getValue());
					}
					
					
					
				}
			}

		
			
					
		


			for(ClientRateZoneBean clzBean: list_ZoneWiseClientRate)
			{
				System.out.println("Type: >>>> SheetName: "+clzBean.getSheetName()+" | Client: "+clzBean.getClient()+" | Network: "+clzBean.getNetwork()+" | Service: "+clzBean.getService()+" >> "+clzBean.getZone_code() +" | DA: "+clzBean.getDa()+" | DB: "+clzBean.getDb()+" | Zone TYpe: "+clzBean.getZone_code()+" | Add Rvr ::"+clzBean.getAdditional_reverse()+" | KG Rvr :: "+clzBean.getKg_reverse());
			}
			
			
			System.err.println("Client Network Service Name list size: "+listClientNetworkServiceName.size());
			
			for(String name: listClientNetworkServiceName)
			{
				System.out.println("Name: >>> "+name);
			}
			
			//for(cli)
			
			
			checkClientRateZoneDetails();
			
			
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
// ******************************************************************************

	/*public void importExcelFile(File selectedFile)
	{
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
					
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				
				if(sheetName.contains("-"))
				{
				
				//String string = sheetName;
				String[] parts = sheetName.split("-");
				String part1 = parts[0]; 
				String part2 = parts[1]; 
				
				String checkWeight=null;
				String checkSlabType=null;
				String checkDox=null;
				String checkNonDox=null;
				
				
				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						ClientRateBean clientRateBean=new ClientRateBean();
						clientRateBean.setClientCode(clientCode[1]);
						clientRateBean.setNetworkCode(networkCode[1]);
						clientRateBean.setServiceCode(comboBoxService.getValue().trim());
						clientRateBean.setMappingType("zone");
						clientRateBean.setFromZoneOrCityCode(part1);
						clientRateBean.setToZoneOrCityCode(part2);
						
						if(row.getCell(0).getCellType()==Cell.CELL_TYPE_STRING)
						{
							String slab_type=row.getCell(0).getStringCellValue();
							if(slab_type.length()==1&&slab_type.equals("B")||slab_type.equals("K")||slab_type.equals("E"))
							{
								clientRateBean.setSlabType(slab_type);
							}
							
			    	    }
						
						if(row.getCell(1).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setWeightUpto(row.getCell(1).getNumericCellValue());
							checkWeight="ok";
						}
						else
						{
							checkWeight=null;
						}
						
						if(row.getCell(2).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setSlab(row.getCell(2).getNumericCellValue());
							checkSlabType="ok";
						}
						else
						{
							checkSlabType=null;
						}
						
						if(row.getCell(3).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setDox(row.getCell(3).getNumericCellValue());
							checkDox="ok";
						}
						else
						{
							checkDox=null;
						}
						
						if(row.getCell(4).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setNonDox(row.getCell(4).getNumericCellValue());
							checkNonDox="ok";
						}
						else
						{
							checkNonDox=null;
						}
						 
						 
						sheetWiseDataList.addAll(clientRateBean);
						
						//System.out.println("Weight >>>>>>>>>> "+checkWeight);
						
						 if(clientRateBean.getSlabType()==null||
							checkWeight==null ||
							checkSlabType==null ||
							checkDox==null ||
							checkNonDox==null)
				    	  { 
							 //clientRateBean.setSheetName(Sheet.getSheetName());
				                faultSheetName.add(Sheet.getSheetName());
				                sheetWiseDataList.clear();
				    	      }
						}
					
						if(Sheet.getLastRowNum()!=sheetWiseDataList.size())
				    	{
				    		sheetWiseDataList.clear();
				    	}
				    
						if(Sheet.getLastRowNum()==sheetWiseDataList.size())
				    	{
							sheetWiseDataList2.addAll(sheetWiseDataList);
							sheetWiseDataList.clear();
				    	}
					}
					
					else
					{
						sheetWiseDataList.clear();
						flag_emptySheet=true;
						alertForEmptySheet="Sheet is empyt";
						faultSheetName.add(sheetName);
					//break;
					}
				}	
			
				else
				{
					sheetWiseDataList.clear();
					flag_emptySheet=true;
					alertForEmptySheet="Selected Excel file is empyt";
					faultSheetName.add(sheetName);
					//break;
				}
			}

		for(String bean: faultSheetName)
		{
			System.out.println("Sheet Name >>>>>>>>>> "+bean);
		}

		for(ClientRateBean bean : sheetWiseDataList2) 
		{
		System.out.println(bean.getClientCode()+" "+bean.getNetworkCode()+" "+bean.getServiceCode()+" "+
		bean.getMappingType()+" "+bean.getFromZoneOrCityCode()
		+" "+bean.getToZoneOrCityCode()+"  "+bean.getSlabType()
		+" "+bean.getWeightUpto()+" "+bean.getSlab()
		+" "+bean.getDox()+" "+bean.getNonDox());
		}

			
				//((FileInputStream) workbook).close();
			//saveClientRateDetails();
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}	*/	
	
	
	
	
// ******************************************************************************
	
	public int saveClientRateZoneWise(boolean isClientRateAvailable,String client, String network, String service, String zone_code) throws SQLException
	{
	
		int count=0;
		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
			
		try 
		{
			for(ClientRateZoneBean clBean: list_ZoneWiseClientRate)
			{
				if(client.equals(clBean.getClient()) && network.equals(clBean.getNetwork()) &&
						service.equals(clBean.getService()) && zone_code.equals(clBean.getZone_code()))
				{
					if (isClientRateAvailable==false ) 
					{
						//count++;
						String query=null;
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							query = MasterSQL_Client_Rate_Utils.UPDATE_SQL_CLIENTRATE_ZONE_WITH_ADD_KG;
						}
						else
						{
							query = MasterSQL_Client_Rate_Utils.UPDATE_SQL_CLIENTRATE_ZONE_DETAILS;
						}
						preparedStmt = con.prepareStatement(query);
						
						//System.out.println("DO Value: "+clBean.getDo1());
						
						preparedStmt.setString(1, clBean.getDa());
						preparedStmt.setString(2, clBean.getDb());
						preparedStmt.setString(3, clBean.getDc());
						preparedStmt.setString(4, clBean.getDd());
						preparedStmt.setString(5, clBean.getDe());
						preparedStmt.setString(6, clBean.getDf());
						preparedStmt.setString(7, clBean.getDg());
						preparedStmt.setString(8, clBean.getDh());
						preparedStmt.setString(9, clBean.getDi());
						preparedStmt.setString(10, clBean.getDj());
						preparedStmt.setString(11, clBean.getDk());
						preparedStmt.setString(12, clBean.getDl());
						preparedStmt.setString(13, clBean.getDm());
						preparedStmt.setString(14, clBean.getDn());
						preparedStmt.setString(15, clBean.getDo1());
						preparedStmt.setString(16, clBean.getDp());
						preparedStmt.setString(17, clBean.getDq());
						preparedStmt.setString(18, clBean.getDr());
						preparedStmt.setString(19, clBean.getDs());
						preparedStmt.setString(20, clBean.getDt());
						
						if(clBean.getAdditional_reverse()==null)
						{
							preparedStmt.setDouble(21, 0);
						}
						else
						{
							preparedStmt.setDouble(21, Double.valueOf(clBean.getAdditional_reverse()));
						}
						
						if(clBean.getKg_reverse()==null)
						{
							preparedStmt.setDouble(22, 0);
						}
						else
						{
							preparedStmt.setDouble(22, Double.valueOf(clBean.getKg_reverse()));
						}
						
						//preparedStmt.setString(21, clBean.getAdditional_reverse());
						//preparedStmt.setString(22, clBean.getKg_reverse());
						preparedStmt.setString(23, clBean.getClient());
						preparedStmt.setString(24, clBean.getNetwork());
						preparedStmt.setString(25, clBean.getService());
						preparedStmt.setString(26, clBean.getZone_code());
						
						//System.out.println("Update >>>> "+preparedStmt);
						
						count=preparedStmt.executeUpdate();
						break;
					
					}
					else
					{
						
						//count++;
						String query=null;
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_ZONE_WITH_ADD_KG;
						}
						else
						{
							query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_ZONE_DETAILS;
						}
						
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, clBean.getClient());
						preparedStmt.setString(2, clBean.getNetwork());
						preparedStmt.setString(3, clBean.getService());
						
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							preparedStmt.setString(4,null);
						}
						else
						{
							preparedStmt.setString(4, clBean.getZone_code());
						}
						
						preparedStmt.setString(5, clBean.getDa());
						preparedStmt.setString(6, clBean.getDb());
						preparedStmt.setString(7, clBean.getDc());
						preparedStmt.setString(8, clBean.getDd());
						preparedStmt.setString(9, clBean.getDe());
						preparedStmt.setString(10, clBean.getDf());
						preparedStmt.setString(11, clBean.getDg());
						preparedStmt.setString(12, clBean.getDh());
						preparedStmt.setString(13, clBean.getDi());
						preparedStmt.setString(14, clBean.getDj());
						preparedStmt.setString(15, clBean.getDk());
						preparedStmt.setString(16, clBean.getDl());
						preparedStmt.setString(17, clBean.getDm());
						preparedStmt.setString(18, clBean.getDn());
						preparedStmt.setString(19, clBean.getDo1());
						preparedStmt.setString(20, clBean.getDp());
						preparedStmt.setString(21, clBean.getDq());
						preparedStmt.setString(22, clBean.getDr());
						preparedStmt.setString(23, clBean.getDs());
						preparedStmt.setString(24, clBean.getDt());
						
						
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							preparedStmt.setString(25, clBean.getZone_code());
						}
						else
						{
							preparedStmt.setString(25, "Basic");
						}
						
						if(clBean.getAdditional_reverse()==null)
						{
							preparedStmt.setDouble(26, 0);
						}
						else
						{
							preparedStmt.setDouble(26, Double.valueOf(clBean.getAdditional_reverse()));
						}
						
						if(clBean.getKg_reverse()==null)
						{
							preparedStmt.setDouble(27, 0);
						}
						else
						{
							preparedStmt.setDouble(27, Double.valueOf(clBean.getKg_reverse()));
						}
						//preparedStmt.setString(26, clBean.getAdditional_reverse());
						//preparedStmt.setString(27, clBean.getKg_reverse());
						
						//System.out.println("Insert >>>> "+preparedStmt);
					
						count=preparedStmt.executeUpdate();
						//System.out.println("Out of range :: "+count);
						break;
						
					}
				}
				
				//System.out.println("Add_R ::"+clBean.getAdditional_reverse()+" | KG_R :: "+clBean.getKg_reverse()+" | DN :: "+clBean.getDn()+" | DO :: "+clBean.getDo1());
			}
			//list_ZoneWiseClientRate.clear();
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		return count;
		
		
	}

// ******************************************************************************
	
	public void checkClientRateSlabDetails() throws SQLException
	{
		
		tabledata_ClientRate.clear();

		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String service=comboBoxService.getValue();
		
		boolean isClientRateAvailable=false;

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;
		
		

		try {
				
				sql = MasterSQL_Client_Rate_Utils.CHECK_SLAB_DETAILS_SQL_CLIENTRATE;
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,clientCode[1]);
				preparedStmt.setString(2,networkCode[1]);
				preparedStmt.setString(3,service);

				System.out.println("PS Sql: "+ preparedStmt);

				rs = preparedStmt.executeQuery();

				if(!rs.next())
				{
					isClientRateAvailable=true;
				
				}
				else
				{
					isClientRateAvailable=false;
				
				}
			
		/*
			for(String s: list_IsClientRateAvailable_Status)
			{
				System.err.println("New Name: >> "+s);
			}
		*/	
			saveClientRateSlabDetails(isClientRateAvailable);
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}


	
// ******************************************************************************
	
	public void checkClientRateZoneDetails() throws SQLException
	{
		tabledata_ClientRate.clear();
		
		boolean isClientRateAvailable=false;

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;


		int count = 0;
		try 
		{
			for(ClientRateZoneBean clBean: list_ZoneWiseClientRate)
			{
				//stmt = con.createStatement();
				
				if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
				{
					sql = MasterSQL_Client_Rate_Utils.CHECK_ZONE_DETAILS_SQL_CLIENTRATE_WITH_ADD_KG;
				}
				else
				{
					sql = MasterSQL_Client_Rate_Utils.CHECK_ZONE_DETAILS_SQL_CLIENTRATE;
				}
				//rs = stmt.executeQuery(sql);
	
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,clBean.getClient());
				preparedStmt.setString(2,clBean.getNetwork());
				preparedStmt.setString(3,clBean.getService());
				preparedStmt.setString(4,clBean.getZone_code());
	
				//System.out.println("PS Sql: "+ preparedStmt);
				rs = preparedStmt.executeQuery();
	
				if(!rs.next())
				{
					isClientRateAvailable=true;
				}
				else
				{
					isClientRateAvailable=false;
				}
				int returnValue=saveClientRateZoneWise(isClientRateAvailable,clBean.getClient(),clBean.getNetwork(),clBean.getService(),clBean.getZone_code());
				System.out.println("Zone :: "+clBean.getZone_code());
				
				/*if(!clBean.getZone_code().equals("Additional") || !clBean.getZone_code().equals("KG"))
				{
					for(String fixZon:list_FixedZones)
					{
						if(!list_zones.contains(fixZon))
						{
							deleteExtraZoneRates(clBean.getClient(),clBean.getNetwork(),clBean.getService(),clBean.getZone_code());
						}
					}
				}*/
					
				count=count+returnValue;
			}
			
			
			update_insert_count=count;
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	

// ******************************************************************************
	
	public void deleteExtraZoneRates(String client,String network,String service,String zone_code) throws SQLException
	{
			
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		int count=1;
			
		try 
		{
			
			
			String query = MasterSQL_Client_Rate_Utils.DELETE_EXTRA_ZONE_RATES;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, client);
			preparedStmt.setString(2, network);
			preparedStmt.setString(3, service);
			preparedStmt.setString(4, zone_code);
				
			System.out.println("Delete Extra Zore Rate SQL: "+preparedStmt);
			int result=preparedStmt.executeUpdate();
			System.err.println("Deleted entry :: "+result);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
			
	}
	
	
// ******************************************************************************
	
	public void saveClientRateSlabDetails(boolean isClientRateAvailable) throws SQLException
	{

		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String service=comboBoxService.getValue();
		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		System.out.println("Status: " +isClientRateAvailable);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		int insertUpdateStatus=0;

		try 
		{
				if (isClientRateAvailable==false) 
				{
					//count++;
					String query = MasterSQL_Client_Rate_Utils.UPDATE_SQL_CLIENTRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					preparedStmt.setDouble(1, Double.valueOf(txtBasic_range.getText()));
					
					if(txtAdd_slab.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(2, 0);
					}
					else
					{
						preparedStmt.setDouble(2, Double.valueOf(txtAdd_slab.getText()));
					}
					
					if(txtAdd_range.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(3, 0);
					}
					else
					{
						preparedStmt.setDouble(3, Double.valueOf(txtAdd_range.getText()));
					}
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(4, "T");
					}
					else
					{
						preparedStmt.setString(4, "F");
					}
					
					preparedStmt.setString(5, clientCode[1]);
					preparedStmt.setString(6, networkCode[1]);
					preparedStmt.setString(7,service);
					
				insertUpdateStatus = preparedStmt.executeUpdate();
				
				System.out.println("insertUpdateStatus from Update >>> "+insertUpdateStatus);
				}
				else
				{
					//count++;
					String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					preparedStmt.setString(1, clientCode[1]);
					preparedStmt.setString(2, networkCode[1]);
					preparedStmt.setString(3,service);

					preparedStmt.setDouble(4, Double.valueOf(txtBasic_range.getText()));
					
					if(txtAdd_slab.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(5, 0);
					}
					else
					{
						preparedStmt.setDouble(5, Double.valueOf(txtAdd_slab.getText()));
					}
					
					if(txtAdd_range.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(6, 0);
					}
					else
					{
						preparedStmt.setDouble(6, Double.valueOf(txtAdd_range.getText()));
					}
					
					
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(7, "T");
					}
					else
					{
						preparedStmt.setString(7, "F");
					}
					
					preparedStmt.setString(8, MasterSQL_Client_Rate_Utils.APPLICATION_ID_FORALL);
					preparedStmt.setString(9 , MasterSQL_Client_Rate_Utils.LAST_MODIFIED_USER_FORALL);

					insertUpdateStatus = preparedStmt.executeUpdate();
				
					System.out.println("insertUpdateStatus from Insert >>> "+insertUpdateStatus);
				}
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Add Slab INFORMATION");
				alert.setHeaderText(null);
				
				System.out.println("insertUpdateStatus >>> "+insertUpdateStatus);
				
				if(insertUpdateStatus==1)
				{
					if (isClientRateAvailable==false) 
					{
						if(checkBox_Kg.isSelected()==true)
						{
							deleteAdditionalKG_From_RateZone(clientCode[1], networkCode[1], service);
							deleteReverse_AdditionalKG_From_RateZone(clientCode[1], networkCode[1], service);
						}
						alert.setContentText("Slab Updated Successfully...!!");
						alert.showAndWait();
						txtClient.requestFocus();
						
					}
					else
					{
						if(checkBox_Kg.isSelected()==true)
						{
							deleteAdditionalKG_From_RateZone(clientCode[1], networkCode[1], service);
							deleteReverse_AdditionalKG_From_RateZone(clientCode[1], networkCode[1], service);
						}
						alert.setContentText("Slab Added Successfully...!!");
						alert.showAndWait();
						txtClient.requestFocus();
						
					}
					
					txtAdd_range.setText("0");
					txtAdd_slab.setText("0");
					txtBasic_range.setText("0");
					checkBox_Kg.setSelected(false);
					txtAdd_range.setDisable(false);
					txtBasic_range.setDisable(false);
					txtAdd_slab.setDisable(false);
				}
				else
				{
					if (isClientRateAvailable==false) 
					{
						alert.setContentText("Slab is not update...!!\nPlease Check");
						alert.showAndWait();
						txtClient.requestFocus();
					}
					else
					{
						alert.setContentText("Slab is not added...!!\nPlease Check");
						alert.showAndWait();	
						txtClient.requestFocus();
					}
						
				}
			//checkClientRateZoneDetails();
			//saveClientRateZoneWise(isClientRateAvailable);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}


	}
	
	
// ******************************************************************************	

	public void deleteAdditionalKG_From_RateZone(String client,String network,String service) throws SQLException
	{
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		int count=1;
		
		try 
		{
			//Additional
			
				String query = MasterSQL_Client_Rate_Utils.DELETE_ADD_KG_FROM_RATEZONE;
				preparedStmt = con.prepareStatement(query);
				con.setAutoCommit(false);
				
				for(int i=1;i<=2;i++)
				{
					preparedStmt.setString(1, client);
					preparedStmt.setString(2, network);
					preparedStmt.setString(3, service);
					
					if(i==1)
					{
						preparedStmt.setString(4, "Additional");
					}
					else if(i==2)
					{
						preparedStmt.setString(4, "KG");
					}
					
					preparedStmt.addBatch();
					
				}
				
				int[] status=preparedStmt.executeBatch();
				
				System.err.println("Delete batch size :: "+status.length);
				con.commit();
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	
	
// ******************************************************************************	

	public void deleteReverse_AdditionalKG_From_RateZone(String client,String network,String service) throws SQLException
	{
			
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		int count=1;
			
		try 
		{
			
			String query = MasterSQL_Client_Rate_Utils.UPDATE_REVERSE_ADD_KG_FROM_RATEZONE;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, client);
			preparedStmt.setString(2, network);
			preparedStmt.setString(3, service);
			preparedStmt.setString(4, "Basic");
				
			System.out.println("Reverse SQL: "+preparedStmt);
			int result=preparedStmt.executeUpdate();
			System.err.println("Deleted entry :: "+result);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
			
	}		
	
	
	/*public void saveClientRateSlabDetails() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		int count=1;
		
		try 
		{
			for(String clientDetail: list_IsClientRateAvailable_Status)
			{
				
				System.out.println(" +++++++++++++++++++++++++++++++++++++++++++ count >> "+count);
				String[] clientNetworkServiceCode=null; 
				clientNetworkServiceCode=clientDetail.replaceAll("\\s+","").split("\\,");
				System.out.println("Client >> "+clientNetworkServiceCode[0]+" | Network >> "+clientNetworkServiceCode[1]+" | Service >> "+clientNetworkServiceCode[2]+" | Status >> "+clientNetworkServiceCode[3]);
				
					count++;
				String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_SLAB_DETAILS;
				preparedStmt = con.prepareStatement(query);

				if (clientNetworkServiceCode[3].equals("true")) 
				{
					
					System.out.println("Service >> "+clientNetworkServiceCode[2]);
					preparedStmt.setString(1, clientNetworkServiceCode[0]);
					preparedStmt.setString(2, clientNetworkServiceCode[1]);
					preparedStmt.setString(3, clientNetworkServiceCode[2]);

					preparedStmt.setString(4, MasterSQL_Client_Rate_Utils.APPLICATION_ID_FORALL);
					preparedStmt.setString(5 , MasterSQL_Client_Rate_Utils.LAST_MODIFIED_USER_FORALL);
					preparedStmt.addBatch();
				}
			}

			int[] batchStatus=preparedStmt.executeBatch();
			
			System.out.println("Client Slab inserted row via batch >> "+batchStatus.length);

			//checkClientRateZoneDetails();
			
			
			
			//saveClientRateZoneWise(isClientRateAvailable);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}



	}*/

	
// 	******************************************************************************
	
	public void alertForSaveClientRate()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not found");
		//alert.setHeaderText("Hello");
		
	/*	if(faultSheetName.size()==0)
		{
			alert.setContentText("Client Rate Successfully saved...");
			alert.showAndWait();
			//reset();
		}
		else
		{
			errorSheetAlert();
			
		}*/
		
		//sheetWiseDataList.clear();
		
		count=0;		
	}
	
	
	public void alertForEmptyExcelOrSheet()
	{
		flag_emptySheet=false;
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Empty Sheet");
		alert.setHeaderText(null);
		
	/*	if(alertForEmptySheet.equals("Sheet is empyt"))
		{
			errorSheetAlert();
			
		}
		else if(alertForEmptySheet.equals("Selected Excel file is empyt"))
		{
			errorSheetAlert();
			
		}*/
		
	}
	

// ******************************************************************************	
	
	/*public void errorSheetAlert()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Excel Sheet Alert");
		alert.setHeaderText(null);
		
		alert.setHeaderText("Sheet is Empty/Incorrect Content/Incorrect Sheet Name\nPlease Check...");
		
		if(faultSheetName.size()>0)
		{
		Exception ex = new FileNotFoundException("Could not find file blabla.txt");

		// Create expandable Exception.
		//StringWriter sw = new StringWriter();
		//PrintWriter pw = new PrintWriter(sw);
		//ex.printStackTrace(pw);
		//String exceptionText = sw.toString();

		Label label = new Label("Error Sheets");

		TextArea textArea = new TextArea();
		
		String name=null;
		
		
		if(faultSheetName.size()==1)
		{
			for(String sheetName: faultSheetName)
			{
				textArea.setText(sheetName);
			}
		}
		else
		{
			for(String sheetName: faultSheetName)
			{
				name=name+","+sheetName;
						
			}
			
			textArea.setText(name.substring(5));
			
			//System.out.println("Name: >>>>>>>>>>>>>>>>> "+name);
		}
		
		textArea.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
		textArea.setStyle("-fx-text-fill: red;");
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(450);
		textArea.setMaxHeight(200);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(250);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);
		}
		alert.showAndWait();
		
		
		//reset();
		
		
	}*/
	
// ******************************************************************************	

	public void loadClientRateTable(String service) throws SQLException
	{
		tabledata_ClientRate.clear();

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;



		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_Client_Rate_Utils.LOADTABLE_SQL_CLIENTRATE;
			//rs = stmt.executeQuery(sql);


			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode[1]);
			preparedStmt.setString(2,networkCode[1]);
			preparedStmt.setString(3,service);

			ClientRateZoneBean clZoneBean=new ClientRateZoneBean();
			
			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();
			slno=1;

			while (rs.next()) {

				ClientRateZoneBean clzBean=new ClientRateZoneBean();

				
				/*clZoneBean.setBasic_range(rs.getDouble("basic_range"));
				clZoneBean.setAdd_slab(rs.getDouble("add_slab"));
				clZoneBean.setAdd_range(rs.getDouble("add_range"));*/
				clzBean.setZone_code(rs.getString("zone_code"));
				//clzBean.setKg(rs.getString("kg"));
				clzBean.setDa(rs.getString("da"));
				clzBean.setDb(rs.getString("db"));
				clzBean.setDc(rs.getString("dc"));
				clzBean.setDd(rs.getString("dd"));
				clzBean.setDe(rs.getString("de"));
				clzBean.setDf(rs.getString("df"));
				clzBean.setDg(rs.getString("dg"));
				clzBean.setDh(rs.getString("dh"));
				clzBean.setDi(rs.getString("di"));
				clzBean.setDj(rs.getString("dj"));
				clzBean.setDk(rs.getString("dk"));
				clzBean.setDl(rs.getString("dl"));
				clzBean.setDm(rs.getString("dm"));
				
				clzBean.setDn(rs.getString("dn"));
				clzBean.setDo1(rs.getString("do1"));
				clzBean.setDp(rs.getString("dp"));
				clzBean.setDq(rs.getString("dq"));
				clzBean.setDr(rs.getString("dr"));
				clzBean.setDs(rs.getString("ds"));
				clzBean.setDt(rs.getString("dt"));
				clzBean.setAdditional_reverse(rs.getString("additional_reverse"));
				clzBean.setKg_reverse(rs.getString("kg_reverse"));
			

				tabledata_ClientRate.add(new ClientRateZoneTableBean(slno, clzBean.getZone_code(), clzBean.getDa(),clzBean.getDb(),
						clzBean.getDc(), clzBean.getDd(), clzBean.getDe(), clzBean.getDf(), clzBean.getDg(), clzBean.getDh(), 
						clzBean.getDi(), clzBean.getDj(), clzBean.getDk(), clzBean.getDl(), clzBean.getDm(), clzBean.getDn(), 
						clzBean.getDo1(), clzBean.getDp(), clzBean.getDq(), clzBean.getDr(), clzBean.getDs(), clzBean.getDt(), 
						clzBean.getAdditional_reverse(), clzBean.getKg_reverse()));

				slno++;
			}

			tableAddClientRate.setItems(tabledata_ClientRate);
			
			/*txt_Basic_range_View.setText(String.valueOf(clZoneBean.getBasic_range()));
			txtAdd_slab_View.setText(String.valueOf(clZoneBean.getAdd_slab()));
			txtAdd_range_View.setText(String.valueOf(clZoneBean.getAdd_range()));*/
			

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
	public void loadClientSlab(String service) throws SQLException
	{
		tabledata_ClientRate.clear();

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		try 
		{
			sql = MasterSQL_Client_Rate_Utils.LOADData_SQL_CLIENTSLAB;
		
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode[1]);
			preparedStmt.setString(2,networkCode[1]);
			preparedStmt.setString(3,service);
			
			ClientRateZoneBean clZoneBean=new ClientRateZoneBean();
			
			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					clZoneBean.setBasic_range(rs.getDouble("basic_range"));
					clZoneBean.setAdd_slab(rs.getDouble("add_slab"));
					clZoneBean.setAdd_range(rs.getDouble("add_range"));
					clZoneBean.setKg(rs.getString("kg"));
				}while (rs.next()); 

				txtBasic_range.setText(String.valueOf(clZoneBean.getBasic_range()));
				
				
				if(clZoneBean.getKg().equals("F"))
				{
					txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
					txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
				
					checkBox_Kg.setSelected(false);
					txtAdd_range.setDisable(false);
					txtAdd_slab.setDisable(false);
				}
				else
				{
					txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
					txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
					checkBox_Kg.setSelected(true);
					txtAdd_range.clear();
					txtAdd_range.setDisable(true);
					txtAdd_slab.clear();
					txtAdd_slab.setDisable(true);
				}
			}
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}

// ******************************************************************************	

	public void setAddUpdate_And_ViewOnly() throws SQLException
	{
		if(rdBtnAddUpdateSlab.isSelected()==true)
		{
			checkBox_Kg.setSelected(false);
			btnBrowse.setDisable(true);
			btnUpdate.setDisable(true);
			txtBrowseExcel.setDisable(true);
			
			txtAdd_range.setDisable(false);
			txtBasic_range.setDisable(false);
			txtAdd_slab.setDisable(false);
			checkBox_Kg.setDisable(false);
			
			txtAdd_range.setText("0");
			txtAdd_slab.setText("0");
			txtBasic_range.setText("0");
			btnSave.setDisable(false);
			//listView_Service_upload.setDisable(false);
			listViewServiceUploadItems.clear();
		/*	comboBoxNetworkItems.clear();
			comboBoxServiceItems.clear();
			
			loadNetworks();*/
		}
		else
		{
		/*	comboBoxNetworkItems.clear();
			comboBoxServiceItems.clear();
			
			loadNetworks();*/
			
			btnSave.setDisable(true);
			checkBox_Kg.setSelected(false);
			
			txtAdd_range.clear();
			txtAdd_slab.clear();
			txtBasic_range.clear();
			
			btnBrowse.setDisable(false);
			btnUpdate.setDisable(false);
			txtBrowseExcel.setDisable(false);
			
			txtAdd_range.setDisable(true);
			txtBasic_range.setDisable(true);
			txtAdd_slab.setDisable(true);
			checkBox_Kg.setDisable(true);
			
			//listView_Service_upload.setDisable(true);
			listViewServiceUploadItems.clear();
			
		}
			
	}
	
	public void getService() throws SQLException
	{
		loadClientSlab(comboBoxService.getValue());
		loadClientRateTable(comboBoxService.getValue());
	}
	
// ******************************************************************************	
	
/*	public void reset()
	{

		sheetWiseDataList.clear();
		sheetWiseDataList2.clear();
		faultSheetName.clear();
		
		txtClient.clear();
		txtBrowseExcel.clear();
		comboBoxNetwork.getSelectionModel().clearSelection();
		//comboBoxService.getSelectionModel().clearSelection();
		txtClient.requestFocus();
	}*/

// ******************************************************************************			

	public void checkForKG()
	{
		if(checkBox_Kg.isSelected()==true)
		{
			labBasic_MinimumCharges.setText("Minimum Charges");
			txtAdd_range.setDisable(true);
			txtAdd_slab.setDisable(true);
			labAdd_Slab.setDisable(true);
			labAdd_Range.setDisable(true);
			txtAdd_range.clear();
			txtAdd_slab.clear();
			
		}
		else
		{
			labBasic_MinimumCharges.setText("Basic Range");
			txtAdd_range.setDisable(false);
			txtAdd_slab.setDisable(false);
			labAdd_Slab.setDisable(false);
			labAdd_Range.setDisable(false);
		}
	}
	
// ******************************************************************************		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	    
		//labAlert.setVisible(false);
		
		
		
		imgViewSearchIcon_ClientRateImport.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtBrowseExcel.setEditable(false);
		
		tableCol_slno.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Integer>("slno"));
		tableCol_Zone.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,String>("zones"));
	    tableCol_da.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("da"));
		tableCol_db.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("db"));
		tableCol_dc.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dc"));
		tableCol_dd.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dd"));
		tableCol_de.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("de"));
		tableCol_df.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("df"));
	    tableCol_dg.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dg"));
		tableCol_dh.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dh"));
		tableCol_di.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("di"));
		tableCol_dj.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dj"));
		tableCol_dk.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dk"));
		tableCol_dl.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dl"));
		tableCol_dm.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dm"));
		tableCol_dn.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dn"));
		tableCol_do1.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("do1"));
	    tableCol_dp.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dp"));
		tableCol_dq.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dq"));
		tableCol_dr.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dr"));
		tableCol_ds.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("ds"));
		tableCol_dt.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dt"));
		tableCol_AddReverse.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("add_reverse"));
		tableCol_KgReverse.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("kg_reverse"));
		
		ClientRateBean bean=new ClientRateBean();
		
		System.out.println("Slabtype: "+bean.getSlabType()  +" | Fixed Zone list size :: "+list_FixedZones.size());
		System.out.println("Dox: "+bean.getDox());
	    
		try {
			loadClients();
			loadNetworks();
			loadServices();
			//loadClientRateTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*listView_Service_upload.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		           
		            
		            try {
						System.out.println(item);
						loadClientSlab(item);
						loadClientRateTable(item);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});*/
		
		btnBrowse.setDisable(true);
		btnUpdate.setDisable(true);
		txtBrowseExcel.setDisable(true);
		
		txtAdd_range.setText("0");
		txtAdd_slab.setText("0");
		txtBasic_range.setText("0");
		
		//listView_Service_upload.getSelectionModel().clearSelection();
		
		
	}

}
