package com.onesoft.courier.ratemaster.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.common.bean.LoadZoneBean;
import com.onesoft.courier.ratemaster.bean.ClientRateBean;
import com.onesoft.courier.ratemaster.bean.ClientRateTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Rate_Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ClientRateController implements Initializable{
	
	//private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	//private ObservableList<String> comboBoxFromZoneOrCityItems=FXCollections.observableArrayList();
	//private ObservableList<String> comboBoxToZoneOrCityItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSlabTypeItems=FXCollections.observableArrayList(
																MasterSQL_Client_Rate_Utils.SLAB_TYPE_BASIC,
																MasterSQL_Client_Rate_Utils.SLAB_TYPE_ADDITIONAL,
																MasterSQL_Client_Rate_Utils.SLAB_TYPE_KG);
	
	private ObservableList<ClientRateTableBean> tabledata_ClientRate=FXCollections.observableArrayList();
	
	private List<String> list_Clients=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	private List<String> list_Zone=new ArrayList<>();
	private List<String> list_City=new ArrayList<>();
	
	AutoCompletionBinding<String> autoBindFromZoneOrCity;
	 AutoCompletionBinding<String> autoBindToZoneOrCity;
	 
	 DecimalFormat df=new DecimalFormat(".##");
	 
	 DecimalFormat df1=new DecimalFormat("#.000");
		DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	/*@FXML
	private ComboBox<String> comboBoxClient;*/
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	/*@FXML
	private ComboBox<String> comboBoxFromZoneOrCity;
	
	@FXML
	private ComboBox<String> comboBoxToZoneOrCity;*/
	
	@FXML
	private ComboBox<String> comboBoxSlabType;
	
	@FXML
	private TextField txtWeightUpto;
	
	@FXML
	private TextField txtSlab;
	
	@FXML
	private TextField txtDox;
	
	@FXML
	private TextField txtNonDox;
	
	@FXML
	private TextField txtClient;
	

	
	@FXML
	private TextField txtFromZoneOrCity;
	
	@FXML
	private TextField txtToZoneOrCity;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnDelete;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private RadioButton rdBtnZoneWise;
	
	@FXML
	private RadioButton rdBtnCityWise;
	
	@FXML
	private Label labFromZoneOrCity;
	
	@FXML
	private Label labToZoneOrCity;
	
	@FXML
	private Label labSlab;
	
	
// ===============================================	

	
	
	@FXML
	private TableView<ClientRateTableBean> tableAddClientRate;

	@FXML
	private TableColumn<ClientRateTableBean, Integer> tableCol_Slno;

	@FXML
	private TableColumn<ClientRateTableBean, String> tableCol_ClientCode;

	@FXML
	private TableColumn<ClientRateTableBean, String> tableCol_SlabType;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_WeightUpTo;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_Slab;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_Dox;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_NonDox;

	/*@FXML
	private TableColumn<ClientRateTableBean, String> tableCol_Zone;*/

		
// *******************************************************************************

	public void loadClients() throws SQLException 
	{
		list_Clients.clear();
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_Clients.add(bean.getClientName() + " | " + bean.getClientCode());
		}
		TextFields.bindAutoCompletion(txtClient, list_Clients);
	}		
	
// ******************************************************************************
	
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();
		
		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	

	
// ******************************************************************************
	
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();
		
		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();
			
			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());
			
			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}
		
		/*for(String srvcCode: set_services)
		{
			comboBoxServiceItems.add(srvcCode);
		}
		comboBoxService.setItems(comboBoxServiceItems);*/
		
		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
	}	
	
// ******************************************************************************

	public void loadZone() throws SQLException 
	{
		new LoadZone().loadZoneWithName();

		for (LoadZoneBean bean : LoadZone.SET_LOAD_ZONE_WITH_NAME) 
		{
			list_Zone.add(bean.getZoneName()+" | "+bean.getZoneCode());
			
		}
		
		autoBindFromZoneOrCity=TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);
		autoBindToZoneOrCity=TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);
	}		
	
// *******************************************************************************

	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName()+" | "+bean.getCityCode());
		}
		
		
	}	

// ******************************************************************************	

	public void selectZoneOrCity()
	{
		 
		
		if(rdBtnZoneWise.isSelected()==true)
		{
			txtFromZoneOrCity.clear();
			txtFromZoneOrCity.setPromptText("Select From Zone");
			autoBindFromZoneOrCity.dispose();
			autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);
			
			txtToZoneOrCity.clear();
			txtToZoneOrCity.setPromptText("Select To Zone");
			autoBindToZoneOrCity.dispose();
			autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);
			
			labFromZoneOrCity.setText("From Zone");
			labToZoneOrCity.setText("To Zone");
	
		}
		else
		{
			txtFromZoneOrCity.clear();
			txtFromZoneOrCity.setPromptText("Select From City");
			autoBindFromZoneOrCity.dispose();
			autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_City);
			
			txtToZoneOrCity.clear();
			txtToZoneOrCity.setPromptText("Select To City");
			autoBindToZoneOrCity.dispose();
			autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_City);
			
			labFromZoneOrCity.setText("From City");
			labToZoneOrCity.setText("To City");
			
		}
		
	}
	
// ******************************************************************************	

	public void showSlabLabelAndTextBox()
	{
		if(comboBoxSlabType.getValue().equals(MasterSQL_Client_Rate_Utils.SLAB_TYPE_ADDITIONAL))
		{
			labSlab.setVisible(true);
			txtSlab.setVisible(true);
		}
		else
		{
			labSlab.setVisible(false);
			txtSlab.setVisible(false);
		}
	}
	
// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		if (comboBoxNetwork.getValue() != null) 
		{
			set_services.clear();
			comboBoxServiceItems.clear();
		
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		
			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}
			
			for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);
		}
	}
	
	
// ******************************************************************************
	
	public void saveClientRateDetails() throws SQLException
	{
		
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String[] fromZoneOrCityCode=txtFromZoneOrCity.getText().replaceAll("\\s+","").split("\\|");
		String[] toZoneOrCityCode=txtToZoneOrCity.getText().replaceAll("\\s+","").split("\\|");
		
		ClientRateBean clientRateBean=new ClientRateBean();
		
		clientRateBean.setClientCode(clientCode[1]);
		clientRateBean.setNetworkCode(networkCode[1]);
		clientRateBean.setServiceCode(comboBoxService.getValue());
		clientRateBean.setFromZoneOrCityCode(fromZoneOrCityCode[1]);
		clientRateBean.setToZoneOrCityCode(toZoneOrCityCode[1]);
		clientRateBean.setSlabType(String.valueOf(comboBoxSlabType.getValue().charAt(0)));
		clientRateBean.setWeightUpto(Double.valueOf(txtWeightUpto.getText()));
		clientRateBean.setDox(Double.valueOf(txtDox.getText()));
		clientRateBean.setNonDox(Double.valueOf(txtNonDox.getText()));
		
		if(rdBtnZoneWise.isSelected()==true)
		{
			clientRateBean.setMappingType(MasterSQL_Client_Rate_Utils.MAPPING_TYPE_ZONE);
		}
		else
		{
			clientRateBean.setMappingType(MasterSQL_Client_Rate_Utils.MAPPING_TYPE_CITY);
		}
		
		if(txtSlab.isVisible()==true)
		{
			clientRateBean.setSlab(Double.valueOf(txtSlab.getText()));
		}
		else
		{
			clientRateBean.setSlab(0);
		}

		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		try 
		{
		
			System.out.println("Client Code: "+clientRateBean.getClientCode());
			System.out.println("Network Code: "+clientRateBean.getNetworkCode());
			System.out.println("Service Code: "+clientRateBean.getServiceCode());
			System.out.println("Mapping Type: "+clientRateBean.getMappingType());
			System.out.println("From Zone/City: "+clientRateBean.getFromZoneOrCityCode());
			System.out.println("To Zone/City: "+clientRateBean.getToZoneOrCityCode());
			System.out.println("Slab Type: "+clientRateBean.getSlabType());
			System.out.println("Weight Up To: "+clientRateBean.getWeightUpto());
			System.out.println("Slab: "+clientRateBean.getSlab());
			System.out.println("Dox: "+clientRateBean.getDox());
			System.out.println("Non Dox: "+clientRateBean.getNonDox());
			
			
			String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_DETAILS;
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, clientRateBean.getClientCode());
			preparedStmt.setString(2, clientRateBean.getNetworkCode());
			preparedStmt.setString(3, clientRateBean.getServiceCode());
			preparedStmt.setString(4, clientRateBean.getMappingType());
			preparedStmt.setString(5, clientRateBean.getFromZoneOrCityCode());
			preparedStmt.setString(6, clientRateBean.getToZoneOrCityCode());
			
			if(clientRateBean.getSlabType().equals("A"))
			{
				preparedStmt.setString(7, "E");	
			}
			else
			{
				preparedStmt.setString(7, clientRateBean.getSlabType());
			}
			preparedStmt.setDouble(8, clientRateBean.getWeightUpto());
			preparedStmt.setDouble(9, clientRateBean.getSlab());
			preparedStmt.setDouble(10, clientRateBean.getDox());
			preparedStmt.setDouble(11, clientRateBean.getNonDox());
			preparedStmt.setString(12, MasterSQL_Client_Rate_Utils.APPLICATION_ID_FORALL);
			preparedStmt.setString(13 , MasterSQL_Client_Rate_Utils.LAST_MODIFIED_USER_FORALL);
			
			int status=preparedStmt.executeUpdate();

			if(status==1)
			{
				//reset();
				
				txtClient.setDisable(true);
				comboBoxNetwork.setDisable(true);
				comboBoxService.setDisable(true);
				txtFromZoneOrCity.setDisable(true);
				txtToZoneOrCity.setDisable(true);
				rdBtnCityWise.setDisable(true);
				rdBtnZoneWise.setDisable(true);
				
				comboBoxSlabType.requestFocus();
				
				loadClientRateTable();
				alert.setContentText("Client Rate Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Client Rate not saved!\nPlease try again...");
				alert.showAndWait();
			}
						
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	

// ******************************************************************************	

	public void loadClientRateTable() throws SQLException
	{
		tabledata_ClientRate.clear();
		
		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;

		

		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_Client_Rate_Utils.LOADTABLE_SQL_CLIENTRATE;
			//rs = stmt.executeQuery(sql);
			
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode[1]);
			
			
			System.out.println("PS Sql: "+ preparedStmt);
			
			rs = preparedStmt.executeQuery();

			while (rs.next()) {
				
				ClientRateBean crBean=new ClientRateBean();
				
				crBean.setSlno_in_db(rs.getInt("clientratemasterid"));
				crBean.setClientCode(rs.getString("clientratemaster2client"));
				crBean.setSlabType(rs.getString("slabtype"));
				crBean.setWeightUpto(rs.getDouble("weightupto"));
				crBean.setSlab(rs.getDouble("slab"));
				crBean.setDox(rs.getDouble("dox"));
				crBean.setNonDox(rs.getDouble("nondox"));
				
				tabledata_ClientRate.add(new ClientRateTableBean(slno, crBean.getSlno_in_db(),crBean.getClientCode(), crBean.getSlabType(),
									crBean.getWeightUpto(), crBean.getSlab(), crBean.getDox(), crBean.getNonDox()));
				
				slno++;
			}
			
			tableAddClientRate.setItems(tabledata_ClientRate);
			
			/*for(ClientRateTableBean bean: tabledata_ClientRate)
			{
				System.out.println("DB slno: "+bean.getSlno_in_db()+" | Slab Type: "+bean.getSlabType());
			}*/
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	

	// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		
		if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				loadClientRateTable();
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				comboBoxService.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnZoneWise.requestFocus();
			}
		}

		else if (n.getId().equals("rdBtnZoneWise")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnCityWise.requestFocus();
			}

		}

		else if (n.getId().equals("rdBtnCityWise")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				selectZoneOrCity();
				txtFromZoneOrCity.requestFocus();
			}
		}

		else if (n.getId().equals("txtFromZoneOrCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtToZoneOrCity.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				txtClient.setDisable(false);
				comboBoxNetwork.setDisable(false);
				comboBoxService.setDisable(false);
				rdBtnCityWise.setDisable(false);
				rdBtnZoneWise.setDisable(false);
				txtClient.requestFocus();
			}
			
		}

		else if (n.getId().equals("txtToZoneOrCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxSlabType.requestFocus();
			}
			
		}

		else if (n.getId().equals("comboBoxSlabType")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtWeightUpto.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				txtFromZoneOrCity.setDisable(false);
				txtToZoneOrCity.setDisable(false);
				txtFromZoneOrCity.requestFocus();
			}
		}

		else if (n.getId().equals("txtWeightUpto")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(txtSlab.isVisible()==true)
				{
					txtSlab.requestFocus();
				}
				else
				{
					txtDox.requestFocus();
				}
				
			}
		}

		else if (n.getId().equals("txtSlab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				txtDox.requestFocus();
			}
		}

		else if (n.getId().equals("txtDox")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtNonDox.requestFocus();
			}
		}

		else if (n.getId().equals("txtNonDox")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnSave.requestFocus();
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}


	}
	
	
	// *******************************************************************************	
	
		public void setValidation() throws SQLException 
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);
			
			//double test=Double.valueOf(txtWeightUpto.getText());
			
			
			if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Client");
				alert.showAndWait();
				txtClient.requestFocus();
			} 
			
			else if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Network");
				alert.showAndWait();
				comboBoxNetwork.requestFocus();
			}
			
			else if(comboBoxService.getValue()==null)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Service field is Empty!");
				alert.showAndWait();
				comboBoxService.requestFocus();
			}
			
			else if (txtFromZoneOrCity.getText() == null || txtFromZoneOrCity.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a From Zone");
				alert.showAndWait();
				txtFromZoneOrCity.requestFocus();
			}
			
			else if (txtToZoneOrCity.getText() == null || txtToZoneOrCity.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a To Zone");
				alert.showAndWait();
				txtToZoneOrCity.requestFocus();
			}
			
			else if(comboBoxSlabType.getValue()==null)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Slab Type field is Empty!");
				alert.showAndWait();
				comboBoxSlabType.requestFocus();
			}
			
			else if (txtWeightUpto.getText() == null || txtWeightUpto.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a WeightUpto");
				alert.showAndWait();
				txtWeightUpto.requestFocus();
			}
		
			// expression for 45,1.1,0.1,.5 type values
			
			else if(!txtWeightUpto.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("WeightUpto Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtWeightUpto.clear();
				txtWeightUpto.requestFocus();
			}
			
			else if(txtSlab.isVisible()==true)
			{
				if(txtSlab.getText() == null || txtSlab.getText().isEmpty())
				{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Slab");
				alert.showAndWait();
				txtSlab.requestFocus();
				}
				
				else if (!txtSlab.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Slab Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtSlab.clear();
					txtSlab.requestFocus();
				}
				
				else if (txtDox.getText() == null || txtDox.getText().isEmpty()) 
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("You did not enter a Dox");
					alert.showAndWait();
					txtDox.requestFocus();
				}
				
				else if (!txtDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Dox Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtDox.clear();
					txtDox.requestFocus();
				}
				
				else if (txtNonDox.getText() == null || txtNonDox.getText().isEmpty()) 
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("You did not enter a Non-Dox");
					alert.showAndWait();
					txtNonDox.requestFocus();
				}
				
				else if (!txtNonDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Non-Dox Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtNonDox.clear();
					txtNonDox.requestFocus();
				}
				
				else
				{
					saveClientRateDetails();
				}
			}
			
			else if (txtDox.getText() == null || txtDox.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Dox");
				alert.showAndWait();
				txtDox.requestFocus();
			}
			
			else if (!txtDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Dox Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtDox.clear();
				txtDox.requestFocus();
			}
			
			else if (txtNonDox.getText() == null || txtNonDox.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Non-Dox");
				alert.showAndWait();
				txtNonDox.requestFocus();
			}
			
			else if (!txtNonDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Non-Dox Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtNonDox.clear();
				txtNonDox.requestFocus();
			}
			else
			{
				saveClientRateDetails();
			}
			
		}	
			
		
// ******************************************************************************
		
	public void reset()
	{
		txtClient.requestFocus();
		txtClient.clear();

		txtFromZoneOrCity.clear();
		txtToZoneOrCity.clear();
		txtWeightUpto.clear();
		txtSlab.setText("0");
		txtDox.clear();
		txtNonDox.clear();
		rdBtnZoneWise.setSelected(true);
		
		labFromZoneOrCity.setText("From Zone");
		labToZoneOrCity.setText("To Zone");
		
		comboBoxNetwork.getSelectionModel().clearSelection();
		comboBoxService.getSelectionModel().clearSelection();
		comboBoxServiceItems.clear();
		comboBoxSlabType.setValue(MasterSQL_Client_Rate_Utils.SLAB_TYPE_BASIC);
		
		txtSlab.setVisible(false);
		labSlab.setVisible(false);
		
		
		autoBindFromZoneOrCity.dispose();
		autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);
		autoBindToZoneOrCity.dispose();
		autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);
		
		comboBoxNetwork.setDisable(false);
		comboBoxService.setDisable(false);
		rdBtnCityWise.setDisable(false);
		rdBtnZoneWise.setDisable(false);
		txtFromZoneOrCity.setDisable(false);
		txtToZoneOrCity.setDisable(false);
		
	}
		
	
// ******************************************************************************	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		comboBoxSlabType.setValue(MasterSQL_Client_Rate_Utils.SLAB_TYPE_BASIC);
		comboBoxSlabType.setItems(comboBoxSlabTypeItems);
		
		labSlab.setVisible(false);
		txtSlab.setVisible(false);
		
		rdBtnZoneWise.setSelected(true);
		
		try 
		{
			loadClients();
			loadNetworks();
			loadServices();
			loadZone();
			loadCity();
			//loadClientRateTable();
			
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		tableCol_Slno.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Integer>("slno"));
		tableCol_ClientCode.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,String>("clientCode"));
		tableCol_Dox.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("dox"));
		tableCol_NonDox.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("nonDox"));
		tableCol_Slab.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("slab"));
		tableCol_SlabType.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,String>("slabType"));
		tableCol_WeightUpTo.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("weightUpto"));
		
	}

}
