package com.onesoft.courier.ratemaster.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.text.AbstractDocument.BranchElement;


import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.controlsfx.control.textfield.TextFields;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.ratemaster.bean.ClientRateBean;
import com.onesoft.courier.ratemaster.bean.ClientRateTableBean;
import com.onesoft.courier.ratemaster.bean.ReadSheetWiseExcelFileControllerBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Rate_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ImportClientRateExcelController implements Initializable {

	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	
	
	private ObservableList<ClientRateTableBean> tabledata_ClientRate=FXCollections.observableArrayList();
	
	
	private List<String> list_Clients=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	
	private ObservableList<String> faultSheetName=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList2=FXCollections.observableArrayList();
	
	
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	int count=0;
	
	public static List<String> list_zones=new ArrayList<>();
	
	boolean flag_emptySheet=false;
	String alertForEmptySheet=null;
	
	@FXML
	private ImageView imgViewSearchIcon_ClientRateImport;
	 
	@FXML
	private TextField txtClient;

	@FXML
	private TextField txtBrowseExcel;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;

	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnSave;
	
	/*@FXML
	private Label labAlert;*/
	
	
// ===============================================	
	
	@FXML
	private TableView<ClientRateTableBean> tableAddClientRate;

	@FXML
	private TableColumn<ClientRateTableBean, Integer> tableCol_Slno;

	@FXML
	private TableColumn<ClientRateTableBean, String> tableCol_ClientCode;

	@FXML
	private TableColumn<ClientRateTableBean, String> tableCol_SlabType;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_WeightUpTo;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_Slab;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_Dox;

	@FXML
	private TableColumn<ClientRateTableBean, Double> tableCol_NonDox;	

	
	
// *******************************************************************************

	public void loadClients() throws SQLException 
	{
		list_Clients.clear();
		
		new LoadClients().loadClientWithName();
			for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_Clients.add(bean.getClientName() + " | " + bean.getClientCode());
		}

		TextFields.bindAutoCompletion(txtClient, list_Clients);
	}		
		
// ******************************************************************************
		
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	

		
// ******************************************************************************
		
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();

		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();

			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());

			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}

		/*for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);*/

		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
		}	
	
	
// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		if (comboBoxNetwork.getValue() != null) 
		{
			set_services.clear();
			comboBoxServiceItems.clear();

			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}

			for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);
		}
	}

	
// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
		
		if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
			
				loadClientRateTable();
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				comboBoxService.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnBrowse.requestFocus();
			}
		}
		
		else if (n.getId().equals("btnBrowse")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				fileAttachment();
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
	}	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		//double test=Double.valueOf(txtWeightUpto.getText());


		if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Client");
			alert.showAndWait();
			txtClient.requestFocus();
		} 

		else if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Network field is Empty!");
			alert.showAndWait();
			comboBoxNetwork.requestFocus();
		}

		else if(comboBoxService.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Service field is Empty!");
			alert.showAndWait();
			comboBoxService.requestFocus();
		}

		else if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select Excel File");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}
	
		else
		{
			progressBarTest();
		}

	}		
	
// ******************************************************************************
	
	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
			//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
		
    	selectedFile = fc.showOpenDialog(null);
    	
    	if(selectedFile != null)
    	{
    		a=5*1024*1024;
    		size=selectedFile.length();	
    		
    	   	if(size<a)
    	   	{
    	   		txtBrowseExcel.getText();
    	   		txtBrowseExcel.setText(selectedFile.getAbsolutePath());
    	   		btnSave.requestFocus();
    	   	}
    	   	else
    	   	{
    	   		txtBrowseExcel.clear();
    	   	}
    	}
    	else
    	{
    		size=0;
    	}
	}
	
	
// ******************************************************************************	
	
	public void progressBarTest() 
	{
        Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
        ProgressBar progressBar = new ProgressBar();

        progressBar.setMinWidth(400);
        progressBar.setVisible(true);

        VBox updatePane = new VBox();
        updatePane.setPadding(new Insets(35));
        updatePane.setSpacing(3.0d);
        Label lbl = new Label("Processing.......");
        lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
        updatePane.getChildren().add(lbl);
        updatePane.getChildren().addAll(progressBar);

        taskUpdateStage.setScene(new Scene(updatePane));
        taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
        taskUpdateStage.show();
        
        task = new Task<Void>() 
        {
        	public Void call() throws Exception 
        	{
        		save();
                
        		int max = 1000;
                for (int i = 1; i <= max; i++) 
                {
                           // Thread.sleep(100);
                            /*if (BirtReportExportCon.FLAG == true) {
                                   break;
                            }*/
                }
                return null;
        	}
        };
        
        
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
               public void handle(WorkerStateEvent t) {
                      taskUpdateStage.close();
                      
                      if(flag_emptySheet==true)
                      {
                    	  alertForEmptyExcelOrSheet();
                      }
                      else
                      {
                    	  alertForSaveClientRate();
                      }
               }
        });
        
        
        progressBar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();
      
	}
	
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
		//importExcelFile(selectedFile);
	}
	
// ******************************************************************************	
	
	public void importNewExcel(File selectedFile) throws IOException
	{
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
					
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				System.out.println("Sheet Name: >>> "+sheetName);
			
				//String string = sheetName;
				/*String[] parts = sheetName.split("-");
				String part1 = parts[0]; 
				String part2 = parts[1]; */
				
				
				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=0;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						
						if(k>0)
						{
							list_zones.add(row.getCell(0).toString());
						}
						
						System.out.println(row.getCell(0).toString()+" "+row.getCell(1).toString()+" "+
								row.getCell(2).toString()+" "+row.getCell(3).toString()+" "+
								row.getCell(4).toString()+" "+row.getCell(5).toString()+" "+
								row.getCell(6).toString()+" "+row.getCell(7).toString()+" "+
								row.getCell(8).toString()+" "+row.getCell(9).toString()+" "+
								row.getCell(10).toString()+" "+row.getCell(11).toString()+" "+
								row.getCell(12).toString()+" "+row.getCell(13).toString());
					}
					
					
					
					
					
					}
				
			}

		for(String bean: faultSheetName)
		{
			System.out.println("Sheet Name >>>>>>>>>> "+bean);
		}

		for(ClientRateBean bean : sheetWiseDataList2) 
		{
		System.out.println(bean.getClientCode()+" "+bean.getNetworkCode()+" "+bean.getServiceCode()+" "+
		bean.getMappingType()+" "+bean.getFromZoneOrCityCode()
		+" "+bean.getToZoneOrCityCode()+"  "+bean.getSlabType()
		+" "+bean.getWeightUpto()+" "+bean.getSlab()
		+" "+bean.getDox()+" "+bean.getNonDox());
		}

			
				//((FileInputStream) workbook).close();
			//saveClientRateDetails();
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
// ******************************************************************************

	/*public void importExcelFile(File selectedFile)
	{
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
					
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				
				if(sheetName.contains("-"))
				{
				
				//String string = sheetName;
				String[] parts = sheetName.split("-");
				String part1 = parts[0]; 
				String part2 = parts[1]; 
				
				String checkWeight=null;
				String checkSlabType=null;
				String checkDox=null;
				String checkNonDox=null;
				
				
				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						ClientRateBean clientRateBean=new ClientRateBean();
						clientRateBean.setClientCode(clientCode[1]);
						clientRateBean.setNetworkCode(networkCode[1]);
						clientRateBean.setServiceCode(comboBoxService.getValue().trim());
						clientRateBean.setMappingType("zone");
						clientRateBean.setFromZoneOrCityCode(part1);
						clientRateBean.setToZoneOrCityCode(part2);
						
						if(row.getCell(0).getCellType()==Cell.CELL_TYPE_STRING)
						{
							String slab_type=row.getCell(0).getStringCellValue();
							if(slab_type.length()==1&&slab_type.equals("B")||slab_type.equals("K")||slab_type.equals("E"))
							{
								clientRateBean.setSlabType(slab_type);
							}
							
			    	    }
						
						if(row.getCell(1).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setWeightUpto(row.getCell(1).getNumericCellValue());
							checkWeight="ok";
						}
						else
						{
							checkWeight=null;
						}
						
						if(row.getCell(2).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setSlab(row.getCell(2).getNumericCellValue());
							checkSlabType="ok";
						}
						else
						{
							checkSlabType=null;
						}
						
						if(row.getCell(3).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setDox(row.getCell(3).getNumericCellValue());
							checkDox="ok";
						}
						else
						{
							checkDox=null;
						}
						
						if(row.getCell(4).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setNonDox(row.getCell(4).getNumericCellValue());
							checkNonDox="ok";
						}
						else
						{
							checkNonDox=null;
						}
						 
						 
						sheetWiseDataList.addAll(clientRateBean);
						
						//System.out.println("Weight >>>>>>>>>> "+checkWeight);
						
						 if(clientRateBean.getSlabType()==null||
							checkWeight==null ||
							checkSlabType==null ||
							checkDox==null ||
							checkNonDox==null)
				    	  { 
							 //clientRateBean.setSheetName(Sheet.getSheetName());
				                faultSheetName.add(Sheet.getSheetName());
				                sheetWiseDataList.clear();
				    	      }
						}
					
						if(Sheet.getLastRowNum()!=sheetWiseDataList.size())
				    	{
				    		sheetWiseDataList.clear();
				    	}
				    
						if(Sheet.getLastRowNum()==sheetWiseDataList.size())
				    	{
							sheetWiseDataList2.addAll(sheetWiseDataList);
							sheetWiseDataList.clear();
				    	}
					}
					
					else
					{
						sheetWiseDataList.clear();
						flag_emptySheet=true;
						alertForEmptySheet="Sheet is empyt";
						faultSheetName.add(sheetName);
					//break;
					}
				}	
			
				else
				{
					sheetWiseDataList.clear();
					flag_emptySheet=true;
					alertForEmptySheet="Selected Excel file is empyt";
					faultSheetName.add(sheetName);
					//break;
				}
			}

		for(String bean: faultSheetName)
		{
			System.out.println("Sheet Name >>>>>>>>>> "+bean);
		}

		for(ClientRateBean bean : sheetWiseDataList2) 
		{
		System.out.println(bean.getClientCode()+" "+bean.getNetworkCode()+" "+bean.getServiceCode()+" "+
		bean.getMappingType()+" "+bean.getFromZoneOrCityCode()
		+" "+bean.getToZoneOrCityCode()+"  "+bean.getSlabType()
		+" "+bean.getWeightUpto()+" "+bean.getSlab()
		+" "+bean.getDox()+" "+bean.getNonDox());
		}

			
				//((FileInputStream) workbook).close();
			//saveClientRateDetails();
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}	*/	
	
	
	
	
// ******************************************************************************
	
	public void saveClientRateDetails() throws SQLException
	{
		
		
		System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
			
		try 
		{
			for(ClientRateBean clientRateBean: sheetWiseDataList2)
			{
				
				count++;
				String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_DETAILS;
				preparedStmt = con.prepareStatement(query);
				
				
				preparedStmt.setString(1, clientRateBean.getClientCode());
				preparedStmt.setString(2, clientRateBean.getNetworkCode());
				preparedStmt.setString(3, clientRateBean.getServiceCode());
				preparedStmt.setString(4, clientRateBean.getMappingType());
				preparedStmt.setString(5, clientRateBean.getFromZoneOrCityCode());
				preparedStmt.setString(6, clientRateBean.getToZoneOrCityCode());
				preparedStmt.setString(7, clientRateBean.getSlabType());
				preparedStmt.setDouble(8, clientRateBean.getWeightUpto());
				preparedStmt.setDouble(9, clientRateBean.getSlab());
				preparedStmt.setDouble(10, clientRateBean.getDox());
				preparedStmt.setDouble(11, clientRateBean.getNonDox());
				preparedStmt.setString(12, MasterSQL_Client_Rate_Utils.APPLICATION_ID_FORALL);
				preparedStmt.setString(13 , MasterSQL_Client_Rate_Utils.LAST_MODIFIED_USER_FORALL);

				preparedStmt.executeUpdate();

				/*if(status==1)
				{
					//reset();
	
					alert.setContentText("Client Rate Successfully saved...");
					alert.showAndWait();
				}
				else
				{
					alert.setContentText("Client Rate not saved!\nPlease try again...");
					alert.showAndWait();
				}*/

				}
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
		
	}
	
	
// 	******************************************************************************
	
	public void alertForSaveClientRate()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not found");
		//alert.setHeaderText("Hello");
		
		if(faultSheetName.size()==0)
		{
			alert.setContentText("Client Rate Successfully saved...");
			alert.showAndWait();
			reset();
		}
		else
		{
			errorSheetAlert();
			
		}
		
		sheetWiseDataList.clear();
		count=0;		
	}
	
	
	public void alertForEmptyExcelOrSheet()
	{
		flag_emptySheet=false;
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Empty Sheet");
		alert.setHeaderText(null);
		
		if(alertForEmptySheet.equals("Sheet is empyt"))
		{
			errorSheetAlert();
			
		}
		else if(alertForEmptySheet.equals("Selected Excel file is empyt"))
		{
			errorSheetAlert();
			
		}
		
	}
	

// ******************************************************************************	
	
	public void errorSheetAlert()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Excel Sheet Alert");
		alert.setHeaderText(null);
		
		alert.setHeaderText("Sheet is Empty/Incorrect Content/Incorrect Sheet Name\nPlease Check...");
		
		if(faultSheetName.size()>0)
		{
		Exception ex = new FileNotFoundException("Could not find file blabla.txt");

		// Create expandable Exception.
		//StringWriter sw = new StringWriter();
		//PrintWriter pw = new PrintWriter(sw);
		//ex.printStackTrace(pw);
		//String exceptionText = sw.toString();

		Label label = new Label("Error Sheets");

		TextArea textArea = new TextArea();
		
		String name=null;
		
		
		if(faultSheetName.size()==1)
		{
			for(String sheetName: faultSheetName)
			{
				textArea.setText(sheetName);
			}
		}
		else
		{
			for(String sheetName: faultSheetName)
			{
				name=name+","+sheetName;
						
			}
			
			textArea.setText(name.substring(5));
			
			//System.out.println("Name: >>>>>>>>>>>>>>>>> "+name);
		}
		
		textArea.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
		textArea.setStyle("-fx-text-fill: red;");
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(450);
		textArea.setMaxHeight(200);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(250);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);
		}
		alert.showAndWait();
		
		
		reset();
		
		
	}
	
// ******************************************************************************	

	public void loadClientRateTable() throws SQLException
	{
		tabledata_ClientRate.clear();

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;



		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_Client_Rate_Utils.LOADTABLE_SQL_CLIENTRATE;
			//rs = stmt.executeQuery(sql);


			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode[1]);


			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();

			while (rs.next()) {

				ClientRateBean crBean=new ClientRateBean();

				crBean.setSlno_in_db(rs.getInt("clientratemasterid"));
				crBean.setClientCode(rs.getString("clientratemaster2client"));
				crBean.setSlabType(rs.getString("slabtype"));
				crBean.setWeightUpto(rs.getDouble("weightupto"));
				crBean.setSlab(rs.getDouble("slab"));
				crBean.setDox(rs.getDouble("dox"));
				crBean.setNonDox(rs.getDouble("nondox"));

				tabledata_ClientRate.add(new ClientRateTableBean(slno,crBean.getSlno_in_db(), crBean.getClientCode(), crBean.getSlabType(),
						crBean.getWeightUpto(), crBean.getSlab(), crBean.getDox(), crBean.getNonDox()));

				slno++;
			}

			tableAddClientRate.setItems(tabledata_ClientRate);

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// ******************************************************************************	
	
	public void reset()
	{

		sheetWiseDataList.clear();
		sheetWiseDataList2.clear();
		faultSheetName.clear();
		
		txtClient.clear();
		txtBrowseExcel.clear();
		comboBoxNetwork.getSelectionModel().clearSelection();
		comboBoxService.getSelectionModel().clearSelection();
		txtClient.requestFocus();
	}
	
	
// ******************************************************************************		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	    
		//labAlert.setVisible(false);
		
		imgViewSearchIcon_ClientRateImport.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtBrowseExcel.setEditable(false);
		
	    tableCol_Slno.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Integer>("slno"));
		tableCol_ClientCode.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,String>("clientCode"));
		tableCol_Dox.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("dox"));
		tableCol_NonDox.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("nonDox"));
		tableCol_Slab.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("slab"));
		tableCol_SlabType.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,String>("slabType"));
		tableCol_WeightUpTo.setCellValueFactory(new PropertyValueFactory<ClientRateTableBean,Double>("weightUpto"));
		
		ClientRateBean bean=new ClientRateBean();
		
		System.out.println("Slabtype: "+bean.getSlabType());
		System.out.println("Dox: "+bean.getDox());
	    
		try {
			loadClients();
			loadNetworks();
			loadServices();
			//loadClientRateTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
