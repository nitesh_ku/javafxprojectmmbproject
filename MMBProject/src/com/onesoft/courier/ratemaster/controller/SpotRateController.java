package com.onesoft.courier.ratemaster.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Predicate;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.controller.DataEntryController;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.ratemaster.bean.SpotRateBean;
import com.onesoft.courier.ratemaster.bean.SpotRateTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_SpotRate_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SpotRateController implements Initializable {

	private ObservableList<String> comboboxDestinationTypeItems = FXCollections.observableArrayList("SELECT", "CITY",
			"STATE", "PINCODE");
	private ObservableList<String> comboboxTypeItems = FXCollections.observableArrayList("PER KG", "FLAT");
	private ObservableList<SpotRateTableBean> tabledata_SpotRate = FXCollections.observableArrayList();

	private Set<String> list_Network = new HashSet<>();
	private Set<String> list_Service = new HashSet<>();
	private Set<String> list_Forwarder = new HashSet<>();
	private Set<String> list_Client = new HashSet<>();
	private Set<String> list_Pincode = new HashSet<>();
	private Set<String> list_City = new HashSet<>();
	private Set<String> list_State = new HashSet<>();

	AutoCompletionBinding<String> pincode;
	AutoCompletionBinding<String> city;
	AutoCompletionBinding<String> state;

	@FXML
	private DatePicker dateDate;

	@FXML
	private DatePicker dateValidTill;

	@FXML
	private ComboBox<String> comboBoxDestinationType;

	@FXML
	private ComboBox<String> comboBoxType;

	@FXML
	private CheckBox checkBoxFuel;

	@FXML
	private CheckBox checkBoxTaxable;

	@FXML
	private TextField txtClient;

	@FXML
	private TextField txtForwarder;

	@FXML
	private TextField txtNetwork;

	@FXML
	private TextField txtService;

	@FXML
	private TextField txtCity;

	@FXML
	private TextField txtWeightFrom;

	@FXML
	private TextField txtWeightTo;

	@FXML
	private TextField txtSaleRate;

	@FXML
	private TextField txtPurchaseRate;

	@FXML
	private TextField txtApprovedBy;

	@FXML
	private TextField txtRemark;

	@FXML
	private TextField txtSearchSpotRate;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnReset;

	@FXML
	private Button btnExportToExcel;

	@FXML
	private Pagination tablePagination;

	@FXML
	private TableView<SpotRateTableBean> spotRateTable;

	@FXML
	private TableColumn<SpotRateTableBean, Integer> tabColumn_Sno;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Client;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Date;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Forwarder;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Network;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Service;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Destination;

	@FXML
	private TableColumn<SpotRateTableBean, Double> tabColumn_SaleRate;

	@FXML
	private TableColumn<SpotRateTableBean, Double> tabColumn_PurchaseRate;

	@FXML
	private TableColumn<SpotRateTableBean, Double> tabColumn_WeighttFrom;

	@FXML
	private TableColumn<SpotRateTableBean, Double> tabColumn_WeightTO;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Valid;

	@FXML
	private TableColumn<SpotRateTableBean, String> tabColumn_Approve;

	// ******************************************************************************

	public void loadNetwork() throws SQLException {
		new LoadNetworks().loadAllNetworksWithName();

		for (LoadNetworkBean bean : LoadNetworks.SET_LOAD_NETWORK_WITH_NAME) {
			list_Network.add(bean.getNetworkName() + " | " + bean.getNetworkCode());
		}
		TextFields.bindAutoCompletion(txtNetwork, list_Network);
	}

	// ******************************************************************************

	public void loadService() throws SQLException {
		new LoadServiceGroups().loadServicesFromServiceType();

		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE) {
			list_Service.add(bean.getServiceName() + " | " + bean.getServiceCode());
		}
		TextFields.bindAutoCompletion(txtService, list_Service);
	}

	// ******************************************************************************

	public void loadForwarder() throws SQLException {
		new LoadForwarderDetails().loadForwarderCodeWithName();

		for (LoadForwarderDetailsBean bean : LoadForwarderDetails.LIST_FORWARDER_DETAILS) {
			list_Forwarder.add(bean.getForwarderName() + " | " + bean.getForwarderCode());
		}
		TextFields.bindAutoCompletion(txtForwarder, list_Forwarder);
	}

	// ******************************************************************************

	public void loadClient() throws SQLException {
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) {
			list_Client.add(bean.getClientName() + " | " + bean.getClientCode());
		}
		TextFields.bindAutoCompletion(txtClient, list_Client);
	}

	// ******************************************************************************

	public void loadPincodeCityState() throws SQLException {
		new LoadPincodeForAll().loadPincode();

		for (LoadPincodeBean bean : LoadPincodeForAll.SET_LOAD_PINCODE) {
			list_Pincode.add(bean.getPincode());
		}
		for (LoadPincodeBean bean : LoadPincodeForAll.SET_LOAD_CITY) {
			list_City.add(bean.getCity_name());
		}
		for (LoadPincodeBean bean : LoadPincodeForAll.SET_LOAD_STATE) {
			list_State.add(bean.getState_code());
		}
		System.out.println("PINCODE --> " + list_Pincode.size());
		System.out.println("CITY --> " + list_City.size());
		System.out.println("STATE --> " + list_State.size());
	}

	// ******************************************************************************

	public void loadCityTextField() {

		if (comboBoxDestinationType.getValue().equals("PINCODE")) {
			txtCity.clear();
			if (city != null) {
				city.dispose();
			}
			if (state != null) {
				state.dispose();
			}
			pincode = TextFields.bindAutoCompletion(txtCity, list_Pincode);
			System.out.println("PIINCODE");
		} else if (comboBoxDestinationType.getValue().equals("CITY")) {
			txtCity.clear();
			if (pincode != null) {
				pincode.dispose();
			}
			if (state != null) {
				state.dispose();
			}
			city = TextFields.bindAutoCompletion(txtCity, list_City);
			System.out.println("CITY");
		} else if (comboBoxDestinationType.getValue().equals("STATE")) {
			txtCity.clear();
			if (pincode != null) {
				pincode.dispose();
			}
			if (city != null) {
				city.dispose();
			}
			state = TextFields.bindAutoCompletion(txtCity, list_State);
			System.out.println("STATE");
		}
	}

	// ******************************************************************************

	public void useEnterAsTabKey(KeyEvent ev) throws SQLException {

		if (ev.getCode().equals(KeyCode.ENTER) && txtClient.isFocused()) {
			dateDate.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && dateDate.isFocused()) {
			txtForwarder.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtForwarder.isFocused()) {
			txtNetwork.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtNetwork.isFocused()) {
			txtService.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtService.isFocused()) {
			comboBoxDestinationType.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && comboBoxDestinationType.isFocused()) {
			txtCity.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtCity.isFocused()) {
			txtWeightFrom.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtWeightFrom.isFocused()) {
			txtWeightTo.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtWeightTo.isFocused()) {
			txtSaleRate.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtSaleRate.isFocused()) {
			txtPurchaseRate.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtPurchaseRate.isFocused()) {
			dateValidTill.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && dateValidTill.isFocused()) {
			txtApprovedBy.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtApprovedBy.isFocused()) {
			comboBoxType.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && comboBoxType.isFocused()) {
			checkBoxFuel.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && checkBoxFuel.isFocused()) {
			checkBoxTaxable.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && checkBoxTaxable.isFocused()) {
			txtRemark.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && txtRemark.isFocused()) {
			btnSave.requestFocus();
		} else if (ev.getCode().equals(KeyCode.ENTER) && btnSave.isFocused()) {
			setValidation();
		} 
	}

	// *************************************************************************

	public void reset() {
		txtClient.requestFocus();
		txtClient.clear();
		dateDate.setValue(null);
		txtForwarder.clear();
		txtNetwork.clear();
		txtService.clear();
		comboBoxDestinationType.setValue(comboboxDestinationTypeItems.get(0));
		txtCity.clear();
		txtWeightFrom.clear();
		txtWeightTo.clear();
		txtSaleRate.clear();
		txtPurchaseRate.clear();
		dateValidTill.setValue(null);
		txtApprovedBy.clear();
		comboBoxType.setValue(comboboxTypeItems.get(0));
		checkBoxFuel.setSelected(false);
		checkBoxTaxable.setSelected(false);
		txtRemark.clear();
	}

	// *************** Method to set Validation *******************

	public void setValidation() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter Client");
			alert.showAndWait();
			txtClient.requestFocus();
		} else if (dateDate.getValue() == null) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Date");
			alert.showAndWait();
			dateDate.requestFocus();
		} else if (txtSaleRate.getText() == null || txtSaleRate.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter Sale Rate");
			alert.showAndWait();
			txtSaleRate.requestFocus();
		} else if (dateValidTill.getValue() == null) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Valid Till");
			alert.showAndWait();
			dateValidTill.requestFocus();
		} else if (txtWeightFrom.getText().matches("^[0-9.]+") == false && !txtWeightFrom.getText().isEmpty()) {
			alert.setTitle("Invalid Input Validation");
			alert.setContentText("You did not enter a Valid Weight");
			alert.showAndWait();
			txtWeightFrom.requestFocus();
		} else if (txtWeightTo.getText().matches("^[0-9.]+") == false && !txtWeightTo.getText().isEmpty()) {
			alert.setTitle("Invalid Input Validation");
			alert.setContentText("You did not enter a Valid Weight");
			alert.showAndWait();
			txtWeightTo.requestFocus();
		} else if (txtSaleRate.getText().matches("^[0-9.]+") == false && !txtSaleRate.getText().isEmpty()) {
			alert.setTitle("Invalid Input Validation");
			alert.setContentText("You did not enter a Valid Sale Rate");
			alert.showAndWait();
			txtSaleRate.requestFocus();
		} else if (txtPurchaseRate.getText().matches("^[0-9.]+") == false && !txtPurchaseRate.getText().isEmpty()) {
			alert.setTitle("Invalid Input Validation");
			alert.setContentText("You did not enter a Valid Purchase Rate");
			alert.showAndWait();
			txtPurchaseRate.requestFocus();
		} else {
				validateSaveUpdateSpotRate();
		}
	}

	// *************************************************************************

	public void validateSaveUpdateSpotRate() throws SQLException {
		String[] splt = {};
		SpotRateBean rateBean = new SpotRateBean();

		rateBean.setDate(Date.valueOf(dateDate.getValue()));
		if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
		} else {
			splt = txtClient.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setClient(splt[1]);
		}
		if (txtForwarder.getText() == null || txtForwarder.getText().isEmpty()) {
			rateBean.setForwarder(" ");
		} else {
			splt = txtForwarder.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setForwarder(splt[1]);
		}
		if (txtNetwork.getText() == null || txtNetwork.getText().isEmpty()) {
			rateBean.setNetwork(" ");
		} else {
			splt = txtNetwork.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setNetwork(splt[1]);
		}
		if (txtService.getText() == null || txtService.getText().isEmpty()) {
			rateBean.setService(" ");
		} else {
			splt = txtService.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setService(splt[1]);
		}

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_SpotRate_Utils.VALIDIATE_SQL_SPOTRATE;
			preparedStmt = con.prepareStatement(query);
			System.out.println(preparedStmt);

			preparedStmt.setString(1, rateBean.getClient());
			preparedStmt.setDate(2, rateBean.getDate());
			preparedStmt.setString(3, rateBean.getForwarder());
			preparedStmt.setString(4, rateBean.getNetwork());
			preparedStmt.setString(5, rateBean.getService());
			rs = preparedStmt.executeQuery();
			if (rs.next() == false) {
				System.out.println("no entry found");
				saveSpotRate();
			} else {
				System.out.println("data exists");
				updateSpotRate();
			}
			reset();
			loadSpotRateTable();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}

	// *************************************************************************

	public void saveSpotRate() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		String[] splt = {};

		SpotRateBean rateBean = new SpotRateBean();

		if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
		} else {
			splt = txtClient.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setClient(splt[1]);
		}
		rateBean.setDate(Date.valueOf(dateDate.getValue()));
		if (txtForwarder.getText() == null || txtForwarder.getText().isEmpty()) {
			rateBean.setForwarder(" ");
		} else {
			splt = txtForwarder.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setForwarder(splt[1]);
		}
		if (txtNetwork.getText() == null || txtNetwork.getText().isEmpty()) {
			rateBean.setNetwork(" ");
		} else {
			splt = txtNetwork.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setNetwork(splt[1]);
		}
		if (txtService.getText() == null || txtService.getText().isEmpty()) {
			rateBean.setService(" ");
		} else {
			splt = txtService.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setService(splt[1]);
		}
		if (comboBoxDestinationType.getValue().equals("SELECT")) {
			rateBean.setDestinationType(" ");
		} else {
			rateBean.setDestinationType(comboBoxDestinationType.getValue());
		}
		if (txtCity.getText() == null || txtCity.getText().isEmpty()) {
			rateBean.setCity(" ");
		} else {
			rateBean.setCity(txtCity.getText());
		}
		if (txtWeightFrom.getText() == null || txtWeightFrom.getText().isEmpty()) {
			rateBean.setWeightFrom(0.0);
		} else {
			rateBean.setWeightFrom(Double.valueOf(txtWeightFrom.getText()));
		}
		if (txtWeightTo.getText() == null || txtWeightTo.getText().isEmpty()) {
			rateBean.setWeightTo(0.0);
		} else {
			rateBean.setWeightTo(Double.valueOf(txtWeightTo.getText()));
		}
		if (txtSaleRate.getText() == null || txtSaleRate.getText().isEmpty()) {
			rateBean.setSaleRate(0.0);
		} else {
			rateBean.setSaleRate(Double.valueOf(txtSaleRate.getText()));
		}
		if (txtPurchaseRate.getText() == null || txtPurchaseRate.getText().isEmpty()) {
			rateBean.setPurchaseRate(0.0);
		} else {
			rateBean.setPurchaseRate(Double.valueOf(txtPurchaseRate.getText()));
		}
		rateBean.setValidTill(Date.valueOf(dateValidTill.getValue()));
		if (txtApprovedBy.getText() == null || txtApprovedBy.getText().isEmpty()) {
			rateBean.setApprovedBy(" ");
		} else {
			rateBean.setApprovedBy(txtApprovedBy.getText());
		}
		rateBean.setType(comboBoxType.getValue());
		if (checkBoxFuel.isSelected() == true) {
			rateBean.setFuel("T");
		} else {
			rateBean.setFuel("F");
		}
		if (checkBoxTaxable.isSelected() == true) {
			rateBean.setTaxable("T");
		} else {
			rateBean.setTaxable("F");
		}
		if (txtRemark.getText() == null || txtRemark.getText().isEmpty()) {
			rateBean.setRemark(" ");
		} else {
			rateBean.setRemark(txtRemark.getText());
		}

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_SpotRate_Utils.INSERT_SQL_SPOTRATE;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, rateBean.getClient());
			preparedStmt.setDate(2, rateBean.getDate());
			preparedStmt.setString(3, rateBean.getForwarder());
			preparedStmt.setString(4, rateBean.getNetwork());
			preparedStmt.setString(5, rateBean.getService());
			preparedStmt.setString(6, rateBean.getDestinationType());
			preparedStmt.setString(7, rateBean.getCity());
			preparedStmt.setDouble(8, rateBean.getWeightFrom());
			preparedStmt.setDouble(9, rateBean.getWeightTo());
			preparedStmt.setDouble(10, rateBean.getSaleRate());
			preparedStmt.setDouble(11, rateBean.getPurchaseRate());
			preparedStmt.setDate(12, rateBean.getValidTill());
			preparedStmt.setString(13, rateBean.getApprovedBy());
			preparedStmt.setString(14, rateBean.getType());
			preparedStmt.setString(15, rateBean.getFuel());
			preparedStmt.setString(16, rateBean.getTaxable());
			preparedStmt.setString(17, rateBean.getRemark());

			
			System.out.println("Insert SQL>  "+preparedStmt);
			
			int status = preparedStmt.executeUpdate();

			
			if (status == 1) {
				alert.setContentText("SpotRate successfully saved...");
				alert.showAndWait();
				if(DataEntryController.spotRate_CallFromDataEntry==false)
				{
					DataEntryController.spotRate_CallFromDataEntry=true;
					Main.hyperLinkStage.hide();
				}
			} else {
				alert.setContentText("SpotRate is not saved \nPlease try again...!");
				alert.showAndWait();
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

	// *************************************************************************

	public void updateSpotRate() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		String[] splt = {};

		SpotRateBean rateBean = new SpotRateBean();

		if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
		} else {
			splt = txtClient.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setClient(splt[1]);
		}
		rateBean.setDate(Date.valueOf(dateDate.getValue()));
		if (txtForwarder.getText() == null || txtForwarder.getText().isEmpty()) {
			rateBean.setForwarder(" ");
		} else {
			splt = txtForwarder.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setForwarder(splt[1]);
		}
		if (txtNetwork.getText() == null || txtNetwork.getText().isEmpty()) {
			rateBean.setNetwork(" ");
		} else {
			splt = txtNetwork.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setNetwork(splt[1]);
		}
		if (txtService.getText() == null || txtService.getText().isEmpty()) {
			rateBean.setService(" ");
		} else {
			splt = txtService.getText().toString().replaceAll("\\s+", "").split("\\|");
			rateBean.setService(splt[1]);
		}
		if (comboBoxDestinationType.getValue().equals("SELECT")) {
			rateBean.setDestinationType(" ");
		} else {
			rateBean.setDestinationType(comboBoxDestinationType.getValue());
		}
		if (txtCity.getText() == null || txtCity.getText().isEmpty()) {
			rateBean.setCity(" ");
		} else {
			rateBean.setCity(txtCity.getText());
		}
		if (txtWeightFrom.getText() == null || txtWeightFrom.getText().isEmpty()) {
			rateBean.setWeightFrom(0.0);
		} else {
			rateBean.setWeightFrom(Double.valueOf(txtWeightFrom.getText()));
		}
		if (txtWeightTo.getText() == null || txtWeightTo.getText().isEmpty()) {
			rateBean.setWeightTo(0.0);
		} else {
			rateBean.setWeightTo(Double.valueOf(txtWeightTo.getText()));
		}
		if (txtSaleRate.getText() == null || txtSaleRate.getText().isEmpty()) {
			rateBean.setSaleRate(0.0);
		} else {
			rateBean.setSaleRate(Double.valueOf(txtSaleRate.getText()));
		}
		if (txtPurchaseRate.getText() == null || txtPurchaseRate.getText().isEmpty()) {
			rateBean.setPurchaseRate(0.0);
		} else {
			rateBean.setPurchaseRate(Double.valueOf(txtPurchaseRate.getText()));
		}
		rateBean.setValidTill(Date.valueOf(dateValidTill.getValue()));
		if (txtApprovedBy.getText() == null || txtApprovedBy.getText().isEmpty()) {
			rateBean.setApprovedBy(" ");
		} else {
			rateBean.setApprovedBy(txtApprovedBy.getText());
		}
		rateBean.setType(comboBoxType.getValue());
		if (checkBoxFuel.isSelected() == true) {
			rateBean.setFuel("T");
		} else {
			rateBean.setFuel("F");
		}
		if (checkBoxTaxable.isSelected() == true) {
			rateBean.setTaxable("T");
		} else {
			rateBean.setTaxable("F");
		}
		if (txtRemark.getText() == null || txtRemark.getText().isEmpty()) {
			rateBean.setRemark(" ");
		} else {
			rateBean.setRemark(txtRemark.getText());
		}

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try {
			String query = MasterSQL_SpotRate_Utils.UPDATE_SQL_SPOTRATE;
			preparedStmt = con.prepareStatement(query);
			System.out.println(preparedStmt);

			preparedStmt.setString(1, rateBean.getDestinationType());
			preparedStmt.setString(2, rateBean.getCity());
			preparedStmt.setDouble(3, rateBean.getWeightFrom());
			preparedStmt.setDouble(4, rateBean.getWeightTo());
			preparedStmt.setDouble(5, rateBean.getSaleRate());
			preparedStmt.setDouble(6, rateBean.getPurchaseRate());
			preparedStmt.setDate(7, rateBean.getValidTill());
			preparedStmt.setString(8, rateBean.getApprovedBy());
			preparedStmt.setString(9, rateBean.getType());
			preparedStmt.setString(10, rateBean.getFuel());
			preparedStmt.setString(11, rateBean.getTaxable());
			preparedStmt.setString(12, rateBean.getRemark());
			preparedStmt.setString(13, rateBean.getClient());
			preparedStmt.setDate(14, rateBean.getDate());
			preparedStmt.setString(15, rateBean.getForwarder());
			preparedStmt.setString(16, rateBean.getNetwork());
			preparedStmt.setString(17, rateBean.getService());

			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				alert.setContentText("Spot Rate successfully updated...");
				alert.showAndWait();
				if(DataEntryController.spotRate_CallFromDataEntry==false)
				{
					DataEntryController.spotRate_CallFromDataEntry=true;
					Main.hyperLinkStage.hide();
				}
			} 
			else {
				alert.setContentText("Spot Rate is not updated \nPlease try again...!");
				alert.showAndWait();
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

	// *************************************************************************

	public void loadSpotRateTable() throws SQLException {

		tabledata_SpotRate.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		SpotRateBean rateBean = new SpotRateBean();

		int slno = 1;
		try {

			stmt = con.createStatement();
			sql = MasterSQL_SpotRate_Utils.LOAD_TABLE_SQL_SPOTRATE;

			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				rateBean.setSlno(slno);
				rateBean.setClient(rs.getString("spotrate_client"));
				rateBean.setTableDate(rs.getString("create_date"));
				rateBean.setForwarder(rs.getString("spotrate_forwarder"));
				rateBean.setNetwork(rs.getString("spotrate_network"));
				rateBean.setService(rs.getString("spotrate_service"));
				rateBean.setCity(rs.getString("spotrate_destination"));
				rateBean.setSaleRate(rs.getDouble("sale_rate"));
				rateBean.setPurchaseRate(rs.getDouble("purchase_rate"));
				rateBean.setWeightFrom(rs.getDouble("from_weight"));
				rateBean.setWeightTo(rs.getDouble("to_weight"));
				rateBean.setTableValidTill(rs.getString("expiry_date"));
				rateBean.setApprovedBy(rs.getString("approved_by"));

				tabledata_SpotRate.add(new SpotRateTableBean(rateBean.getSlno(), rateBean.getClient(),
						rateBean.getTableDate(), rateBean.getForwarder(), rateBean.getNetwork(), rateBean.getService(),
						rateBean.getCity(), rateBean.getSaleRate(), rateBean.getPurchaseRate(),
						rateBean.getWeightFrom(), rateBean.getWeightTo(), rateBean.getTableValidTill(),
						rateBean.getApprovedBy()));

				slno++;
			}
			spotRateTable.setItems(tabledata_SpotRate);

			tablePagination.setPageCount((tabledata_SpotRate.size() / rowsPerPage() + 1));
			tablePagination.setCurrentPageIndex(0);
			tablePagination.setPageFactory((Integer pageIndex) -> createPage_SpotRate(pageIndex));
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}

	// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 15;
	}

	public GridPane createPage_SpotRate(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_SpotRate.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_SpotRate.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				spotRateTable.setItems(FXCollections.observableArrayList(
						tabledata_SpotRate.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				spotRateTable.setItems(FXCollections.observableArrayList(tabledata_SpotRate
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}
	
	
	// ========================================================================================

		@FXML
		public void exportToExcel_SpotRate() throws SQLException, IOException {

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Spot Rate");
			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("Serial No");
			rowhead.createCell(1).setCellValue("Client");
			rowhead.createCell(2).setCellValue("Date");
			rowhead.createCell(3).setCellValue("Forwarder");
			rowhead.createCell(4).setCellValue("Network");
			rowhead.createCell(5).setCellValue("Service");
			rowhead.createCell(6).setCellValue("Destination");
			rowhead.createCell(7).setCellValue("Sale Rate");
			rowhead.createCell(8).setCellValue("Purchase Rate");
			rowhead.createCell(9).setCellValue("Weight From");
			rowhead.createCell(10).setCellValue("Weight To");
			rowhead.createCell(11).setCellValue("Valid Till");
			rowhead.createCell(12).setCellValue("Approved By");

			int i = 1;

			HSSFRow row;

			for (SpotRateTableBean rateBean : tabledata_SpotRate) {
				row = sheet.createRow((short) i);

				row.createCell(0).setCellValue(rateBean.getSno());
				row.createCell(1).setCellValue(rateBean.getClient());
				row.createCell(2).setCellValue(rateBean.getDate());
				row.createCell(3).setCellValue(rateBean.getForwarder());
				row.createCell(4).setCellValue(rateBean.getNetwork());
				row.createCell(5).setCellValue(rateBean.getService());
				row.createCell(6).setCellValue(rateBean.getDestination());
				row.createCell(7).setCellValue(rateBean.getSaleRate());
				row.createCell(8).setCellValue(rateBean.getPurchaseRate());
				row.createCell(9).setCellValue(rateBean.getWeightFrom());
				row.createCell(10).setCellValue(rateBean.getWeightTo());
				row.createCell(11).setCellValue(rateBean.getValidTill());
				row.createCell(12).setCellValue(rateBean.getApprove());

				i++;
			}

			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
			File file = fc.showSaveDialog(null);
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			// System.out.println("file chooser: "+file.getAbsoluteFile());

			workbook.write(fileOut);
			fileOut.close();

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Export to excel alert");
			alert.setHeaderText(null);
			alert.setContentText("File " + file.getName() + " successfully saved");
			alert.showAndWait();

		}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			loadClient();
			loadForwarder();
			loadNetwork();
			loadService();
			loadPincodeCityState();
			loadSpotRateTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		comboBoxDestinationType.setItems(comboboxDestinationTypeItems);
		comboBoxDestinationType.setValue(comboboxDestinationTypeItems.get(0));
		comboBoxType.setItems(comboboxTypeItems);
		comboBoxType.setValue(comboboxTypeItems.get(0));

		tabColumn_Sno.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, Integer>("sno"));
		tabColumn_Client.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("client"));
		tabColumn_Date.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("date"));
		tabColumn_Forwarder.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("forwarder"));
		tabColumn_Network.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("network"));
		tabColumn_Service.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("service"));
		tabColumn_Destination.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("destination"));
		tabColumn_SaleRate.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, Double>("saleRate"));
		tabColumn_PurchaseRate.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, Double>("purchaseRate"));
		tabColumn_WeighttFrom.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, Double>("weightFrom"));
		tabColumn_WeightTO.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, Double>("weightTo"));
		tabColumn_Valid.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("validTill"));
		tabColumn_Approve.setCellValueFactory(new PropertyValueFactory<SpotRateTableBean, String>("approve"));
		
		FilteredList<SpotRateTableBean> filteredSpotRateData=new  FilteredList<>(tabledata_SpotRate,e -> true);
		txtSearchSpotRate.setOnKeyPressed(e -> {
			txtSearchSpotRate.textProperty().addListener((ObservableValue, oldValue, newValue) ->{
				filteredSpotRateData.setPredicate((Predicate<? super SpotRateTableBean>) user ->{
					if(newValue==null || newValue.isEmpty())
					{
						return true;
					}
					String lowerCaseFilter=newValue.toLowerCase();
					if(String.valueOf(user.getSno()).contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getClient().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getDate().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getForwarder().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getNetwork().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getService().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getDestination().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(String.valueOf(user.getSaleRate()).toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(String.valueOf(user.getPurchaseRate()).toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(String.valueOf(user.getWeightFrom()).toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(String.valueOf(user.getWeightTo()).toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getValidTill().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					if(user.getApprove().toLowerCase().contains(lowerCaseFilter)) {
						return true;
					}
					return false;
				});
			});	
			SortedList<SpotRateTableBean> sortedData=new  SortedList<>(filteredSpotRateData);
			sortedData.comparatorProperty().bind(spotRateTable.comparatorProperty());
			spotRateTable.setItems(sortedData);
		});
	}

}
