package com.onesoft.courier.ratemaster.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.groovy.runtime.wrappers.BooleanWrapper;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
//import com.onesoft.courier.ratemaster.bean.ClientRateBean;
import com.onesoft.courier.ratemaster.bean.ForwarderRateZoneBean;
import com.onesoft.courier.ratemaster.bean.ForwarderRateZoneTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_ImportForwarder_Rate_Utils;

/*import com.onesoft.courier.ratemaster.bean.ForwarderRateZoneBean;
import com.onesoft.courier.ratemaster.bean.ForwarderRateZoneTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_ImportForwarder_Rate_Utils;*/

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ImportForwarderRateWithAddKgController implements Initializable {

	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	private ObservableList<ForwarderRateZoneTableBean> tabledata_ForwarderRate=FXCollections.observableArrayList();
	
	
	public String forwarderCode_global=null;
	public String networkCode_global=null;
	public String serviceCode_global=null;
	
	private List<String> list_Forwarders=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	public static List<String> list_zones=new ArrayList<>();
	
	/*private ObservableList<String> faultSheetName=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList2=FXCollections.observableArrayList();*/
	
	public int update_insert_count=0;
	boolean isExcelFileIsCorrect=false;
	boolean isServiceAvailableInExcelFile=false;
	
	private List<ForwarderRateZoneBean> list_ZoneWiseForwarderRate=new ArrayList<>();
	private ObservableList<String> listViewServiceUploadItems=FXCollections.observableArrayList();
	//private ObservableList<String> listViewServiceItems=FXCollections.observableArrayList();
	
	private List<String> listForwarderNetworkServiceName=new ArrayList<>();
	private List<String> list_IsForwarderRateAvailable_Status=new ArrayList<>();
	
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	int count=0;
	
	boolean flag_emptySheet=false;
	String alertForEmptySheet=null;
	

	@FXML
	private Label labBasic_MinimumCharges;
	
	@FXML
	private Label labAdd_Slab;
	
	@FXML
	private Label labAdd_Range;
	
	@FXML
	private Label labService;
	
	@FXML
	private Label labBrowseFile;

	/*@FXML
	private Label labBasic_Range_View;
	
	@FXML
	private Label labAdd_Slab_View;
	
	@FXML
	private Label labAdd_Range_View;
	
	@FXML
	private Label labService_View;*/
	
	
	@FXML
	private ImageView imgViewSearchIcon_ForwarderRateImport;
	 
	@FXML
	private TextField txtForwarder;

	@FXML
	private TextField txtBrowseExcel;
	
	@FXML
	private TextField txtBasic_range;
	
	@FXML
	private TextField txtAdd_slab;
	
	@FXML
	private TextField txtAdd_range;
	
	/*@FXML
	private TextField txt_Basic_range_View;
	
	@FXML
	private TextField txtAdd_slab_View;
	
	@FXML
	private TextField txtAdd_range_View;*/
	
	@FXML
	private CheckBox checkBox_Kg;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	/*@FXML
	private ComboBox<String> comboBoxService;*/

	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnSave;

	@FXML
	private Button btnReset;

	@FXML
	private Button btnUpdate;
	
	/*@FXML
	private ListView<String> listViewService;*/
	
	/*@FXML
	private ListView<String> listView_Service_upload;*/
	
	/*@FXML
	private ListView<String> listViewService;*/
	
	@FXML
	private RadioButton rdBtnAddUpdateSlab;
	
	@FXML
	private RadioButton rdBtnUploadFile;
	
	
// ===============================================	
	
	@FXML
	private TableView<ForwarderRateZoneTableBean> tableAddForwarderRate;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Integer> tableCol_slno;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, String> tableCol_Zone;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_da;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_db;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dc;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dd;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_de;

	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_df;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dg;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dh;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_di;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dj;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dk;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dl;
	
	@FXML
	private TableColumn<ForwarderRateZoneTableBean, Double> tableCol_dm;

	
	
// *******************************************************************************

	public void loadForwarders() throws SQLException 
	{
		list_Forwarders.clear();
		
		//new LoadClients().loadClientWithName();
		new LoadForwarderDetails().loadForwarderCodeWithName();
			for (LoadForwarderDetailsBean frBean : LoadForwarderDetails.LIST_FORWARDER_DETAILS) 
		{
			list_Forwarders.add(frBean.getForwarderName() + " | " + frBean.getForwarderCode());
		}

		TextFields.bindAutoCompletion(txtForwarder, list_Forwarders);
	}		
		
// ******************************************************************************
		
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	

		
// ******************************************************************************
		
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();

		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();

			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());

			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}

		/*for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);*/

		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
		}	
	
	
// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		
		if (comboBoxNetwork.getValue() != null) 
		{
			listViewServiceUploadItems.clear();
			//listViewServiceItems.clear();
			set_services.clear();
			
			comboBoxServiceItems.clear(); 

			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}

			/*
				txtAdd_range.clear();
				txtAdd_slab.clear();
				txtBasic_range.clear();*/
				
				for(String srvcCode: set_services)
				{
					comboBoxServiceItems.add(srvcCode);
					listViewServiceUploadItems.add(srvcCode);
				}
				comboBoxService.setItems(comboBoxServiceItems);
				//listView_Service_upload.setItems(listViewServiceUploadItems);
			
			
			//checkListViewService.getSelectionModel().getSelectedItem();
			/*
				listViewService.setCellFactory(lv -> {
				    ListCell<String> cell = new ListCell<String>() {
				        @Override
				        protected void updateItem(String item, boolean empty) {
				            super.updateItem(item, empty);
				            if (empty) {
				                setText(null);
				            } else {
				                setText(item.toString());
				            }
				        }
				    };
				    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
				        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
				            String item = cell.getItem();
				           
				            
				            try {
								System.out.println(item);
								loadClientRateTable(item);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

				        }
				    });
				    return cell ;
				});
		*/
			//comboBoxService.setItems(comboBoxServiceItems);
		}
	}

	
// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
		
		
		
		if (n.getId().equals("rdBtnAddUpdateSlab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnUploadFile.requestFocus();
			}
		}
		
		if (n.getId().equals("rdBtnUploadFile")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtForwarder.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtForwarder")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				comboBoxService.requestFocus();
			}
		}
		
		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				if(rdBtnAddUpdateSlab.isSelected()==true)
				{
					txtBasic_range.requestFocus();
				}
				else
				{
					btnBrowse.requestFocus();
				}
			}
		}

		else if (n.getId().equals("txtBasic_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				checkBox_Kg.requestFocus();
			}
		}
		
		
		else if (n.getId().equals("checkBox_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(checkBox_Kg.isSelected()==true)
				{
					btnSave.requestFocus();
				}
				else
				{
					txtAdd_slab.requestFocus();
				}
				
			}
		}
		
		else if (n.getId().equals("txtAdd_slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				txtAdd_range.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtAdd_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
		
				
				
				btnSave.requestFocus();
			}
		}
	
		
		else if (n.getId().equals("btnBrowse")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				fileAttachment();
			}
		}

		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				//checkClientRateSlabDetails();
				setValidation();
			}
		}
		
		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				checkForwarderRateSlabDetails();
				//setValidation();
			}
		}
	}	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		//double test=Double.valueOf(txtWeightUpto.getText());


		if (txtForwarder.getText() == null || txtForwarder.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Forwarder");
			alert.showAndWait();
			txtForwarder.requestFocus();
		} 

		else if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Network field is Empty!");
			alert.showAndWait();
			comboBoxNetwork.requestFocus();
		}

	

		else if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select Excel File");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}
	
		else
		{
			progressBarTest();
		}

	}		
	
// ******************************************************************************
	
	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
			//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
		
    	selectedFile = fc.showOpenDialog(null);
    	
    	if(selectedFile != null)
    	{
    		a=5*1024*1024;
    		size=selectedFile.length();	
    		
    	   	if(size<a)
    	   	{
    	   		txtBrowseExcel.getText();
    	   		txtBrowseExcel.setText(selectedFile.getAbsolutePath());
    	   		btnUpdate.requestFocus();
    	   	}
    	   	else
    	   	{
    	   		txtBrowseExcel.clear();
    	   	}
    	}
    	else
    	{
    		size=0;
    	}
	}
	
	
// ******************************************************************************	
	
	public void progressBarTest() 
	{
        Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
        ProgressBar progressBar = new ProgressBar();

        progressBar.setMinWidth(400);
        progressBar.setVisible(true);

        VBox updatePane = new VBox();
        updatePane.setPadding(new Insets(35));
        updatePane.setSpacing(3.0d);
        Label lbl = new Label("Processing.......");
        lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
        updatePane.getChildren().add(lbl);
        updatePane.getChildren().addAll(progressBar);

        taskUpdateStage.setScene(new Scene(updatePane));
        taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
        taskUpdateStage.show();
        
        task = new Task<Void>() 
        {
        	public Void call() throws Exception 
        	{
        		save();
                
        		int max = 1000;
                for (int i = 1; i <= max; i++) 
                {
                           // Thread.sleep(100);
                            /*if (BirtReportExportCon.FLAG == true) {
                                   break;
                            }*/
                }
                return null;
        	}
        };
        
        
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
               public void handle(WorkerStateEvent t) {
                      taskUpdateStage.close();
                      
                  if(isServiceAvailableInExcelFile==true)
                  {
                      
                    if(isExcelFileIsCorrect==false)
          	   		{
          	   		Alert alert = new Alert(AlertType.INFORMATION);
      				alert.setTitle("Import Alert");
      				//alert.setTitle("Empty Field Validation");
      				alert.setContentText("Please select correct Excel format file");
      				alert.showAndWait();
      				isExcelFileIsCorrect=true;
          	   		}
                    else
                    {
                      if (list_ZoneWiseForwarderRate.size()==update_insert_count) {
          				
          				Alert alert = new Alert(AlertType.INFORMATION);
          				alert.setTitle("Task Complete");
          				//alert.setTitle("Empty Field Validation");
          				alert.setContentText("Rates successfully uploaded...");
          				alert.showAndWait();
                      }	
          			}
                    isServiceAvailableInExcelFile=false;
                  }
                  else
                  {
                	  Alert alert = new Alert(AlertType.INFORMATION);
        				alert.setTitle("Import Alert");
        				//alert.setTitle("Empty Field Validation");
        				alert.setContentText("Service not avialable in selected file...\nPlease check");
        				alert.showAndWait();
        				isExcelFileIsCorrect=true;
                  }
                	 
               }
        });
        
        
        progressBar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();
      
	}
	
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
		//importExcelFile(selectedFile);
	}
	
// ******************************************************************************	
	
	public void importNewExcel(File selectedFile) throws IOException
	{
		list_ZoneWiseForwarderRate.clear();
		try
		{
			String[] ForwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			
			
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			
			
			forwarderCode_global=ForwarderCode[1];
			networkCode_global=networkCode[1];
			//serviceCode_global=fileName;
			
			System.out.println("File Name: "+fileName);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			
			//ObservableList<String> selectedService=listView_Service_upload.getCheckModel().getCheckedItems();
			/*
			for(String s:selectedService)
			{
				System.err.println("Selected Service for KG: "+s);
			}*/
			int matchSheetCount=0;
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName().toUpperCase();
				
				
				if(comboBoxServiceItems.contains(sheetName))
				{
					matchSheetCount++;
				}
			}
			

			
			if(matchSheetCount>0)
			{
				System.out.println("Service Available in sheet");
				isServiceAvailableInExcelFile=true;
			}
			else
			{
				isServiceAvailableInExcelFile=false;
				System.out.println("Service not Available in sheet");
			}


		
			if(isServiceAvailableInExcelFile==true)
			{
				for(int i=0;i<workbook.getNumberOfSheets();i++)
				{
					Sheet Sheet = workbook.getSheetAt(i);
					String sheetName=Sheet.getSheetName().toUpperCase();
				
					System.out.println("Sheet Name >?>?>?>?>?>?>?>?>?>?>?>?>?>?>?>? "+sheetName);
					
					if(comboBoxServiceItems.contains(sheetName))
					{
						HSSFRow row;
						serviceCode_global=sheetName;
					
						if(Sheet.getLastRowNum()!=0)
						{
							int maxCell=0;
							for(int k=1;k<=Sheet.getLastRowNum();k++)
							{
								row=(HSSFRow) Sheet.getRow(k);
								maxCell=  row.getLastCellNum();
								System.out.println("Total Columns >> "+maxCell);
								break;
								
							}
							
							if(maxCell>=2 && maxCell<=21)
							{
								isExcelFileIsCorrect=true;
								for(int k=1;k<=Sheet.getLastRowNum();k++)
								{
									row=(HSSFRow) Sheet.getRow(k);
								
									if(row.getCell(0).toString().equals("Additional") || row.getCell(0).toString().equals("KG"))
									{
										
									}
									else
									{
											list_zones.add(row.getCell(0).toString());
									}
									
									ForwarderRateZoneBean clZoneBean=new ForwarderRateZoneBean();

									clZoneBean.setSheetName(sheetName);
									clZoneBean.setForwarder(ForwarderCode[1]);
									clZoneBean.setNetwork(networkCode[1]);
									clZoneBean.setService(sheetName);
									if(row.getCell(0).toString().equals("DO"))
									{
										clZoneBean.setZone_code("DO");
									}
									else
									{
										clZoneBean.setZone_code(row.getCell(0).toString());
									}


									if(row.getCell(1)==null)
									{
										clZoneBean.setDa("0");
									}
									else
									{
										clZoneBean.setDa(row.getCell(1).toString());
									}

									if(row.getCell(2)==null)
									{
										clZoneBean.setDb("0");
									}
									else
									{
										clZoneBean.setDb(row.getCell(2).toString());
									}

									if(row.getCell(3)==null)
									{
										clZoneBean.setDc("0");
									}
									else
									{
										clZoneBean.setDc(row.getCell(3).toString());
									}

									if(row.getCell(4)==null)
									{
										clZoneBean.setDd("0");
									}
									else
									{
										clZoneBean.setDd(row.getCell(4).toString());
									}

									if(row.getCell(5)==null)
									{
										clZoneBean.setDe("0");
									}
									else
									{
										clZoneBean.setDe(row.getCell(5).toString());
									}

									if(row.getCell(6)==null)
									{
										clZoneBean.setDf("0");
									}
									else
									{
										clZoneBean.setDf(row.getCell(6).toString());
									}

									if(row.getCell(7)==null)
									{
										clZoneBean.setDg("0");
									}
									else
									{
										clZoneBean.setDg(row.getCell(7).toString());
									}

									if(row.getCell(8)==null)
									{
										clZoneBean.setDh("0");
									}
									else
									{
										clZoneBean.setDh(row.getCell(8).toString());
									}

									if(row.getCell(9)==null)
									{
										clZoneBean.setDi("0");
									}
									else
									{
										clZoneBean.setDi(row.getCell(9).toString());
									}

									if(row.getCell(10)==null)
									{
										clZoneBean.setDj("0");
									}
									else
									{
										clZoneBean.setDj(row.getCell(10).toString());
									}

									if(row.getCell(11)==null)
									{
										clZoneBean.setDk("0");
									}
									else
									{
										clZoneBean.setDk(row.getCell(11).toString());
									}

									if(row.getCell(12)==null)
									{
										clZoneBean.setDl("0");
									}
									else
									{
										clZoneBean.setDl(row.getCell(12).toString());
									}

									if(row.getCell(13)==null)
									{
										clZoneBean.setDm("0");
									}
									else
									{
										clZoneBean.setDm(row.getCell(13).toString());
									}

									if(row.getCell(14)==null)
									{
										clZoneBean.setDn("0");
									}
									else
									{
										clZoneBean.setDn(row.getCell(14).toString());
									}

									if(row.getCell(15)==null)
									{
										clZoneBean.setDo1("0");
									}
									else
									{
										clZoneBean.setDo1(row.getCell(15).toString());
									}

									if(row.getCell(16)==null)
									{
										clZoneBean.setDp("0");
									}
									else
									{
										clZoneBean.setDp(row.getCell(16).toString());
									}

									if(row.getCell(17)==null)
									{
										clZoneBean.setDq("0");
									}
									else
									{
										clZoneBean.setDq(row.getCell(17).toString());
									}

									if(row.getCell(18)==null)
									{
										clZoneBean.setDr("0");
									}
									else
									{
										clZoneBean.setDr(row.getCell(18).toString());
									}

									if(row.getCell(19)==null)
									{
										clZoneBean.setDs("0");
									}
									else
									{
										clZoneBean.setDs(row.getCell(19).toString());
									}

									if(row.getCell(20)==null)
									{
										clZoneBean.setDt("0");
									}
									else
									{
										clZoneBean.setDt(row.getCell(20).toString());
									}
									list_ZoneWiseForwarderRate.add(clZoneBean);
								}
							}
							else
							{
								isExcelFileIsCorrect=false;
							}
						}
					}
					else
					{
						System.err.println(sheetName+" :: service not available with given network :: "+comboBoxNetwork.getValue());
					}
				}
			}

			/**/



			for(ForwarderRateZoneBean clzBean: list_ZoneWiseForwarderRate)
			{
				System.out.println("Type: >>>> SheetName: "+clzBean.getSheetName()+" | Forwarder: "+clzBean.getForwarder()+" | Network: "+clzBean.getNetwork()+" | Service: "+clzBean.getService()+" >> "+clzBean.getZone_code() +" | DA: "+clzBean.getDa()+" | DB: "+clzBean.getDb()+" | Zone TYpe: "+clzBean.getZone_code());
			}
			
			
			System.err.println("Forwarder Network Service Name list size: "+listForwarderNetworkServiceName.size());
			
			for(String name: listForwarderNetworkServiceName)
			{
				System.out.println("Name: >>> "+name);
			}
			
			
			
			checkForwarderRateZoneDetails();
			
			
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	
// ******************************************************************************
	
	public int saveForwarderRateZoneWise(boolean isForwarderRateAvailable,String forwarder, String network, String service, String zone_code) throws SQLException
	{
	
		int count=0;
		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
			
		try 
		{
			for(ForwarderRateZoneBean clBean: list_ZoneWiseForwarderRate)
			{
				if(forwarder.equals(clBean.getForwarder()) && network.equals(clBean.getNetwork()) &&
						service.equals(clBean.getService()) && zone_code.equals(clBean.getZone_code()))
				{
					if (isForwarderRateAvailable==false ) 
					{
						count++;
						String query=null;
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							query = MasterSQL_ImportForwarder_Rate_Utils.UPDATE_SQL_FORWARDERRATE_ZONE_WITH_ADD_KG;
						}
						else
						{
							query = MasterSQL_ImportForwarder_Rate_Utils.UPDATE_SQL_FORWARDERRATE_ZONE_DETAILS;
						}
						preparedStmt = con.prepareStatement(query);
						
						System.out.println("DO Value: "+clBean.getDo1());
						
						preparedStmt.setString(1, clBean.getDa());
						preparedStmt.setString(2, clBean.getDb());
						preparedStmt.setString(3, clBean.getDc());
						preparedStmt.setString(4, clBean.getDd());
						preparedStmt.setString(5, clBean.getDe());
						preparedStmt.setString(6, clBean.getDf());
						preparedStmt.setString(7, clBean.getDg());
						preparedStmt.setString(8, clBean.getDh());
						preparedStmt.setString(9, clBean.getDi());
						preparedStmt.setString(10, clBean.getDj());
						preparedStmt.setString(11, clBean.getDk());
						preparedStmt.setString(12, clBean.getDl());
						preparedStmt.setString(13, clBean.getDm());
						preparedStmt.setString(14, clBean.getDn());
						preparedStmt.setString(15, clBean.getDo1());
						preparedStmt.setString(16, clBean.getDp());
						preparedStmt.setString(17, clBean.getDq());
						preparedStmt.setString(18, clBean.getDr());
						preparedStmt.setString(19, clBean.getDs());
						preparedStmt.setString(20, clBean.getDt());
						preparedStmt.setString(21, clBean.getForwarder());
						preparedStmt.setString(22, clBean.getNetwork());
						preparedStmt.setString(23, clBean.getService());
						preparedStmt.setString(24, clBean.getZone_code());
						
						System.out.println("Update >>>> "+preparedStmt);
						
						count=preparedStmt.executeUpdate();
						break;
					
					}
					else
					{
						
						count++;
						String query=null;
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							query = MasterSQL_ImportForwarder_Rate_Utils.INSERT_SQL_FORWARDERRATE_ZONE_WITH_ADD_KG;
						}
						else
						{
							query = MasterSQL_ImportForwarder_Rate_Utils.INSERT_SQL_FORWARDERRATE_ZONE_DETAILS;
						}
						
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, clBean.getForwarder());
						preparedStmt.setString(2, clBean.getNetwork());
						preparedStmt.setString(3, clBean.getService());
						
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							preparedStmt.setString(4,null);
						}
						else
						{
							preparedStmt.setString(4, clBean.getZone_code());
						}
						
						preparedStmt.setString(5, clBean.getDa());
						preparedStmt.setString(6, clBean.getDb());
						preparedStmt.setString(7, clBean.getDc());
						preparedStmt.setString(8, clBean.getDd());
						preparedStmt.setString(9, clBean.getDe());
						preparedStmt.setString(10, clBean.getDf());
						preparedStmt.setString(11, clBean.getDg());
						preparedStmt.setString(12, clBean.getDh());
						preparedStmt.setString(13, clBean.getDi());
						preparedStmt.setString(14, clBean.getDj());
						preparedStmt.setString(15, clBean.getDk());
						preparedStmt.setString(16, clBean.getDl());
						preparedStmt.setString(17, clBean.getDm());
						preparedStmt.setString(18, clBean.getDn());
						preparedStmt.setString(19, clBean.getDo1());
						preparedStmt.setString(20, clBean.getDp());
						preparedStmt.setString(21, clBean.getDq());
						preparedStmt.setString(22, clBean.getDr());
						preparedStmt.setString(23, clBean.getDs());
						preparedStmt.setString(24, clBean.getDt());
						
						if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
						{
							preparedStmt.setString(25, clBean.getZone_code());
						}
						else
						{
							preparedStmt.setString(25, "Basic");
						}
						
						System.out.println("Insert >>>> "+preparedStmt);
					
						count=preparedStmt.executeUpdate();
						break;
						
					}
				}
			}
			//list_ZoneWiseClientRate.clear();
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		return count;
		
		
	}

// ******************************************************************************
	
	public void checkForwarderRateSlabDetails() throws SQLException
	{
		
		tabledata_ForwarderRate.clear();

		String[] forwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String service=comboBoxService.getValue();
		
		boolean isForwarderRateAvailable=false;

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;
		
		

		try {
				
				sql = MasterSQL_ImportForwarder_Rate_Utils.CHECK_SLAB_DETAILS_SQL_FORWARDERRATE;
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,forwarderCode[1]);
				preparedStmt.setString(2,networkCode[1]);
				preparedStmt.setString(3,service);

				System.out.println("PS Sql: "+ preparedStmt);

				rs = preparedStmt.executeQuery();

				if(!rs.next())
				{
					isForwarderRateAvailable=true;
				
				}
				else
				{
					isForwarderRateAvailable=false;
				
				}
			
		/*
			for(String s: list_IsClientRateAvailable_Status)
			{
				System.err.println("New Name: >> "+s);
			}
		*/	
			saveForwarderRateSlabDetails(isForwarderRateAvailable);
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}


	
// ******************************************************************************
	
	public void checkForwarderRateZoneDetails() throws SQLException
	{
		tabledata_ForwarderRate.clear();
		
		boolean isForwarderRateAvailable=false;

		System.out.println("Load table running...");
		String[] forwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;


		int count = 0;
		try 
		{
			for(ForwarderRateZoneBean clBean: list_ZoneWiseForwarderRate)
			{
				//stmt = con.createStatement();
				
				if(clBean.getZone_code().equals("Additional") || clBean.getZone_code().equals("KG"))
				{
					sql = MasterSQL_ImportForwarder_Rate_Utils.CHECK_ZONE_DETAILS_SQL_FORWARDERRATE_WITH_ADD_KG;
				}
				else
				{
					sql = MasterSQL_ImportForwarder_Rate_Utils.CHECK_ZONE_DETAILS_SQL_FORWARDERRATE;
				}
				//rs = stmt.executeQuery(sql);
	
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,clBean.getForwarder());
				preparedStmt.setString(2,clBean.getNetwork());
				preparedStmt.setString(3,clBean.getService());
				preparedStmt.setString(4,clBean.getZone_code());
	
				//System.out.println("PS Sql: "+ preparedStmt);
				rs = preparedStmt.executeQuery();
	
				if(!rs.next())
				{
					isForwarderRateAvailable=true;
				}
				else
				{
					isForwarderRateAvailable=false;
				}
				count=count+saveForwarderRateZoneWise(isForwarderRateAvailable,clBean.getForwarder(),clBean.getNetwork(),clBean.getService(),clBean.getZone_code());
			}
			
			
			update_insert_count=count;
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	
// ******************************************************************************
	
	public void saveForwarderRateSlabDetails(boolean isForwarderRateAvailable) throws SQLException
	{

		String[] forwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String service=comboBoxService.getValue();
		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		System.out.println("Status: " +isForwarderRateAvailable);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		
		int insertUpdateStatus=0;

		try 
		{
				if (isForwarderRateAvailable==false) 
				{
					//count++;
					String query = MasterSQL_ImportForwarder_Rate_Utils.UPDATE_SQL_FORWARDERRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					preparedStmt.setDouble(1, Double.valueOf(txtBasic_range.getText()));
					
					if(txtAdd_slab.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(2, 0);
					}
					else
					{
						preparedStmt.setDouble(2, Double.valueOf(txtAdd_slab.getText()));
					}
					
					if(txtAdd_range.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(3, 0);
					}
					else
					{
						preparedStmt.setDouble(3, Double.valueOf(txtAdd_range.getText()));
					}
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(4, "T");
					}
					else
					{
						preparedStmt.setString(4, "F");
					}
					
					preparedStmt.setString(5, forwarderCode[1]);
					preparedStmt.setString(6, networkCode[1]);
					preparedStmt.setString(7,service);
					
				insertUpdateStatus = preparedStmt.executeUpdate();
				
				System.out.println("insertUpdateStatus from Update >>> "+insertUpdateStatus);
				}
				else
				{
					//count++;
					String query = MasterSQL_ImportForwarder_Rate_Utils.INSERT_SQL_FORWARDERRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					preparedStmt.setString(1, forwarderCode[1]);
					preparedStmt.setString(2, networkCode[1]);
					preparedStmt.setString(3,service);

					preparedStmt.setDouble(4, Double.valueOf(txtBasic_range.getText()));
					
					if(txtAdd_slab.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(5, 0);
					}
					else
					{
						preparedStmt.setDouble(5, Double.valueOf(txtAdd_slab.getText()));
					}
					
					if(txtAdd_range.getText().isEmpty()==true)
					{
						preparedStmt.setDouble(6, 0);
					}
					else
					{
						preparedStmt.setDouble(6, Double.valueOf(txtAdd_range.getText()));
					}
					
					
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(7, "T");
					}
					else
					{
						preparedStmt.setString(7, "F");
					}
					
					preparedStmt.setString(8, MasterSQL_ImportForwarder_Rate_Utils.APPLICATION_ID_FORALL);
					preparedStmt.setString(9 , MasterSQL_ImportForwarder_Rate_Utils.LAST_MODIFIED_USER_FORALL);

					insertUpdateStatus = preparedStmt.executeUpdate();
				
					System.out.println("insertUpdateStatus from Insert >>> "+insertUpdateStatus);
				}
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Add Slab INFORMATION");
				alert.setHeaderText(null);
				
				System.out.println("insertUpdateStatus >>> "+insertUpdateStatus);
				
				if(insertUpdateStatus==1)
				{
					if (isForwarderRateAvailable==false) 
					{
						if(checkBox_Kg.isSelected()==true)
						{
							deleteAdditionalKG_From_RateZone(forwarderCode[1], networkCode[1], service);
						}
						alert.setContentText("Slab Updated Successfully...!!");
						alert.showAndWait();
						txtForwarder.requestFocus();
						
					}
					else
					{
						if(checkBox_Kg.isSelected()==true)
						{
							deleteAdditionalKG_From_RateZone(forwarderCode[1], networkCode[1], service);
						}
						alert.setContentText("Slab Added Successfully...!!");
						alert.showAndWait();
						txtForwarder.requestFocus();
						
					}
					
					txtAdd_range.setText("0");
					txtAdd_slab.setText("0");
					txtBasic_range.setText("0");
					checkBox_Kg.setSelected(false);
					txtAdd_range.setDisable(false);
					txtBasic_range.setDisable(false);
					txtAdd_slab.setDisable(false);
				}
				else
				{
					if (isForwarderRateAvailable==false) 
					{
						alert.setContentText("Slab is not update...!!\nPlease Check");
						alert.showAndWait();
						txtForwarder.requestFocus();
					}
					else
					{
						alert.setContentText("Slab is not added...!!\nPlease Check");
						alert.showAndWait();	
						txtForwarder.requestFocus();
					}
						
				}
			//checkClientRateZoneDetails();
			//saveClientRateZoneWise(isClientRateAvailable);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}


	}
	
	
// ******************************************************************************	

	public void deleteAdditionalKG_From_RateZone(String forwarder,String network,String service) throws SQLException
	{
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
		int count=1;
		
		boolean isBulkDataAvailable=false;
		
		try 
		{
			//Additional
			
			//if()
			
				String query = MasterSQL_ImportForwarder_Rate_Utils.DELETE_ADD_KG_FROM_RATEZONE;
				preparedStmt = con.prepareStatement(query);
				con.setAutoCommit(false);
				
				for(int i=1;i<=2;i++)
				{
					preparedStmt.setString(1, forwarder);
					preparedStmt.setString(2, network);
					preparedStmt.setString(3, service);
					
					if(i==1)
					{
						preparedStmt.setString(4, "Additional");
					}
					else if(i==2)
					{
						preparedStmt.setString(4, "KG");
					}
					
					//preparedStmt.
					preparedStmt.addBatch();
					
				}
				
				
				int[] status=preparedStmt.executeBatch();
				
				System.err.println("Delete batch size :: "+status.length);
				con.commit();
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	
	

	
// 	******************************************************************************
	
	public void alertForSaveForwarderRate()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not found");
		//alert.setHeaderText("Hello");
		
	/*	if(faultSheetName.size()==0)
		{
			alert.setContentText("Client Rate Successfully saved...");
			alert.showAndWait();
			//reset();
		}
		else
		{
			errorSheetAlert();
			
		}*/
		
		//sheetWiseDataList.clear();
		
		count=0;		
	}
	
	
	public void alertForEmptyExcelOrSheet()
	{
		flag_emptySheet=false;
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Empty Sheet");
		alert.setHeaderText(null);
		
	/*	if(alertForEmptySheet.equals("Sheet is empyt"))
		{
			errorSheetAlert();
			
		}
		else if(alertForEmptySheet.equals("Selected Excel file is empyt"))
		{
			errorSheetAlert();
			
		}*/
		
	}
	
	
// ******************************************************************************	

	public void loadForwarderRateTable(String service) throws SQLException
	{
		tabledata_ForwarderRate.clear();

		System.out.println("Load table running...");
		String[] forwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;



		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_ImportForwarder_Rate_Utils.LOADTABLE_SQL_FORWARDERRATE;
			//rs = stmt.executeQuery(sql);


			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,forwarderCode[1]);
			preparedStmt.setString(2,networkCode[1]);
			preparedStmt.setString(3,service);

			ForwarderRateZoneBean clZoneBean=new ForwarderRateZoneBean();
			
			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();
			slno=1;

			while (rs.next()) {

				ForwarderRateZoneBean clzBean=new ForwarderRateZoneBean();

				
				/*clZoneBean.setBasic_range(rs.getDouble("basic_range"));
				clZoneBean.setAdd_slab(rs.getDouble("add_slab"));
				clZoneBean.setAdd_range(rs.getDouble("add_range"));*/
				clzBean.setZone_code(rs.getString("zone_code"));
				//clzBean.setKg(rs.getString("kg"));
				clzBean.setDa(rs.getString("da"));
				clzBean.setDb(rs.getString("db"));
				clzBean.setDc(rs.getString("dc"));
				clzBean.setDd(rs.getString("dd"));
				clzBean.setDe(rs.getString("de"));
				clzBean.setDf(rs.getString("df"));
				clzBean.setDg(rs.getString("dg"));
				clzBean.setDh(rs.getString("dh"));
				clzBean.setDi(rs.getString("di"));
				clzBean.setDj(rs.getString("dj"));
				clzBean.setDk(rs.getString("dk"));
				clzBean.setDl(rs.getString("dl"));
				clzBean.setDm(rs.getString("dm"));
			
				System.out.println("Zone Name >>> "+clzBean.getZone_code());

				tabledata_ForwarderRate.add(new ForwarderRateZoneTableBean(slno, clzBean.getZone_code(), clzBean.getDa(),clzBean.getDb(),
						clzBean.getDc(), clzBean.getDd(), clzBean.getDe(), clzBean.getDf(), clzBean.getDg(), clzBean.getDh(), 
						clzBean.getDi(), clzBean.getDj(), clzBean.getDk(), clzBean.getDl(), clzBean.getDm()
						));

				slno++;
			}

			System.out.println("Table data size >> "+tabledata_ForwarderRate.size());
			tableAddForwarderRate.setItems(tabledata_ForwarderRate);
			
			/*txt_Basic_range_View.setText(String.valueOf(clZoneBean.getBasic_range()));
			txtAdd_slab_View.setText(String.valueOf(clZoneBean.getAdd_slab()));
			txtAdd_range_View.setText(String.valueOf(clZoneBean.getAdd_range()));*/
			

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
	public void loadForwarderSlab(String service) throws SQLException
	{
		tabledata_ForwarderRate.clear();

		System.out.println("Load table running...");
		String[] forwarderCode=txtForwarder.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		try 
		{
			sql = MasterSQL_ImportForwarder_Rate_Utils.LOADData_SQL_FORWARDERSLAB;
		
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,forwarderCode[1]);
			preparedStmt.setString(2,networkCode[1]);
			preparedStmt.setString(3,service);
			
			ForwarderRateZoneBean clZoneBean=new ForwarderRateZoneBean();
			
			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
				txtBasic_range.setText(String.valueOf(clZoneBean.getBasic_range()));
				txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
				txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
			}
			else
			{
				do
				{
					clZoneBean.setBasic_range(rs.getDouble("basic_range"));
					clZoneBean.setAdd_slab(rs.getDouble("add_slab"));
					clZoneBean.setAdd_range(rs.getDouble("add_range"));
					clZoneBean.setKg(rs.getString("kg"));
				}while (rs.next()); 

				txtBasic_range.setText(String.valueOf(clZoneBean.getBasic_range()));
				
				
				if(clZoneBean.getKg().equals("F"))
				{
					txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
					txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
				
					checkBox_Kg.setSelected(false);
					txtAdd_range.setDisable(false);
					txtAdd_slab.setDisable(false);
				}
				else
				{
					txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
					txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
					checkBox_Kg.setSelected(true);
					txtAdd_range.clear();
					txtAdd_range.setDisable(true);
					txtAdd_slab.clear();
					txtAdd_slab.setDisable(true);
				}
			}
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}

// ******************************************************************************	

	public void setAddUpdate_And_ViewOnly() throws SQLException
	{
		if(rdBtnAddUpdateSlab.isSelected()==true)
		{
			checkBox_Kg.setSelected(false);
			btnBrowse.setDisable(true);
			btnUpdate.setDisable(true);
			txtBrowseExcel.setDisable(true);
			
			txtAdd_range.setDisable(false);
			txtBasic_range.setDisable(false);
			txtAdd_slab.setDisable(false);
			checkBox_Kg.setDisable(false);
			
			txtAdd_range.setText("0");
			txtAdd_slab.setText("0");
			txtBasic_range.setText("0");
			btnSave.setDisable(false);
			//listView_Service_upload.setDisable(false);
			listViewServiceUploadItems.clear();
		/*	comboBoxNetworkItems.clear();
			comboBoxServiceItems.clear();
			
			loadNetworks();*/
		}
		else
		{
		/*	comboBoxNetworkItems.clear();
			comboBoxServiceItems.clear();
			
			loadNetworks();*/
			
			btnSave.setDisable(true);
			checkBox_Kg.setSelected(false);
			
			txtAdd_range.clear();
			txtAdd_slab.clear();
			txtBasic_range.clear();
			
			btnBrowse.setDisable(false);
			btnUpdate.setDisable(false);
			txtBrowseExcel.setDisable(false);
			
			txtAdd_range.setDisable(true);
			txtBasic_range.setDisable(true);
			txtAdd_slab.setDisable(true);
			checkBox_Kg.setDisable(true);
			
			//listView_Service_upload.setDisable(true);
			listViewServiceUploadItems.clear();
			
		}
			
	}
	
	public void getService() throws SQLException
	{
		loadForwarderSlab(comboBoxService.getValue());
		loadForwarderRateTable(comboBoxService.getValue());
	}
	
// ******************************************************************************	
	
/*	public void reset()
	{

		sheetWiseDataList.clear();
		sheetWiseDataList2.clear();
		faultSheetName.clear();
		
		txtForwarder.clear();
		txtBrowseExcel.clear();
		comboBoxNetwork.getSelectionModel().clearSelection();
		//comboBoxService.getSelectionModel().clearSelection();
		txtForwarder.requestFocus();
	}*/

// ******************************************************************************			

	public void checkForKG()
	{
		if(checkBox_Kg.isSelected()==true)
		{
			labBasic_MinimumCharges.setText("Minimum Charges");
			txtAdd_range.setDisable(true);
			txtAdd_slab.setDisable(true);
			labAdd_Slab.setDisable(true);
			labAdd_Range.setDisable(true);
			txtAdd_range.clear();
			txtAdd_slab.clear();
			
		}
		else
		{
			labBasic_MinimumCharges.setText("Basic Range");
			txtAdd_range.setDisable(false);
			txtAdd_slab.setDisable(false);
			labAdd_Slab.setDisable(false);
			labAdd_Range.setDisable(false);
		}
	}
	
// ******************************************************************************		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	    
		//labAlert.setVisible(false);
		
		imgViewSearchIcon_ForwarderRateImport.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtBrowseExcel.setEditable(false);
		
		tableCol_slno.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Integer>("slno"));
		tableCol_Zone.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,String>("zones"));
	    tableCol_da.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("da"));
		tableCol_db.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("db"));
		tableCol_dc.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dc"));
		tableCol_dd.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dd"));
		tableCol_de.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("de"));
		tableCol_df.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("df"));
	    tableCol_dg.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dg"));
		tableCol_dh.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dh"));
		tableCol_di.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("di"));
		tableCol_dj.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dj"));
		tableCol_dk.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dk"));
		tableCol_dl.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dl"));
		tableCol_dm.setCellValueFactory(new PropertyValueFactory<ForwarderRateZoneTableBean,Double>("dm"));
					
		
	    
		try {
			loadForwarders();
			loadNetworks();
			loadServices();
			//loadClientRateTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*listView_Service_upload.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		           
		            
		            try {
						System.out.println(item);
						loadClientSlab(item);
						loadClientRateTable(item);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});*/
		
		btnBrowse.setDisable(true);
		btnUpdate.setDisable(true);
		txtBrowseExcel.setDisable(true);
		
		txtAdd_range.setText("0");
		txtAdd_slab.setText("0");
		txtBasic_range.setText("0");
		
		//listView_Service_upload.getSelectionModel().clearSelection();
		
		
	}

}
