package com.onesoft.courier.ratemaster.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.controlsfx.control.CheckListView;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.ratemaster.bean.ClientRateBean;

import com.onesoft.courier.ratemaster.bean.ClientRateZoneBean;
import com.onesoft.courier.ratemaster.bean.ClientRateZoneTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Rate_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class NewImportClientRateExcelController implements Initializable {

	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<ClientRateZoneTableBean> tabledata_ClientRate=FXCollections.observableArrayList();
	
	
	public String clientCode_global=null;
	public String networkCode_global=null;
	public String serviceCode_global=null;
	
	private List<String> list_Clients=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	public static List<String> list_zones=new ArrayList<>();
	
	/*private ObservableList<String> faultSheetName=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList=FXCollections.observableArrayList();
	private ObservableList<ClientRateBean> sheetWiseDataList2=FXCollections.observableArrayList();*/
	
	public int update_insert_count=0;
	boolean isExcelFileIsCorrect=false;
	
	private List<ClientRateZoneBean> list_ZoneWiseClientRate=new ArrayList<>();
	private ObservableList<String> checkListViewServiceItems=FXCollections.observableArrayList();
	
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	int count=0;
	
	boolean flag_emptySheet=false;
	String alertForEmptySheet=null;
	
	@FXML
	private ImageView imgViewSearchIcon_ClientRateImport;
	 
	@FXML
	private TextField txtClient;

	@FXML
	private TextField txtBrowseExcel;
	
	@FXML
	private TextField txtBasic_range;
	
	@FXML
	private TextField txtAdd_slab;
	
	@FXML
	private TextField txtAdd_range;
	
	@FXML
	private CheckBox checkBox_Kg;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	/*@FXML
	private ComboBox<String> comboBoxService;*/

	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private CheckListView<String> checkListViewService;
	
	/*@FXML
	private Label labAlert;*/
	
	
// ===============================================	
	
	@FXML
	private TableView<ClientRateZoneTableBean> tableAddClientRate;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Integer> tableCol_slno;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, String> tableCol_Zone;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_da;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_db;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dc;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dd;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_de;

	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_df;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dg;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dh;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_di;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dj;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dk;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dl;
	
	@FXML
	private TableColumn<ClientRateZoneTableBean, Double> tableCol_dm;

	
	
// *******************************************************************************

	public void loadClients() throws SQLException 
	{
		list_Clients.clear();
		
		new LoadClients().loadClientWithName();
			for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_Clients.add(bean.getClientName() + " | " + bean.getClientCode());
		}

		TextFields.bindAutoCompletion(txtClient, list_Clients);
	}		
		
// ******************************************************************************
		
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	

		
// ******************************************************************************
		
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();

		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();

			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());

			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}

		/*for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);*/

		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
		}	
	
	
// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		
		if (comboBoxNetwork.getValue() != null) 
		{
			checkListViewServiceItems.clear();
			set_services.clear();
		//	comboBoxServiceItems.clear(); 

			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}

			for(String srvcCode: set_services)
			{
				checkListViewServiceItems.add(srvcCode);
			}
			checkListViewService.setItems(checkListViewServiceItems);
			//comboBoxService.setItems(comboBoxServiceItems);
		}
	}

	
// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();
		
		if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
			
				//loadClientRateTable();
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				txtBasic_range.requestFocus();
			}
		}

		else if (n.getId().equals("txtBasic_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				txtAdd_slab.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtAdd_slab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				txtAdd_range.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtAdd_range")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setServiceViaNetwork();
				checkBox_Kg.requestFocus();
			}
		}
		
		else if (n.getId().equals("checkBox_Kg")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnBrowse.requestFocus();
			}
		}
		
		else if (n.getId().equals("btnBrowse")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				fileAttachment();
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
	}	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		//double test=Double.valueOf(txtWeightUpto.getText());


		if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Client");
			alert.showAndWait();
			txtClient.requestFocus();
		} 

		else if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Network field is Empty!");
			alert.showAndWait();
			comboBoxNetwork.requestFocus();
		}

	

		else if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select Excel File");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}
	
		else
		{
			progressBarTest();
		}

	}		
	
// ******************************************************************************
	
	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
			//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
		
    	selectedFile = fc.showOpenDialog(null);
    	
    	if(selectedFile != null)
    	{
    		a=5*1024*1024;
    		size=selectedFile.length();	
    		
    	   	if(size<a)
    	   	{
    	   		txtBrowseExcel.getText();
    	   		txtBrowseExcel.setText(selectedFile.getAbsolutePath());
    	   		btnSave.requestFocus();
    	   	}
    	   	else
    	   	{
    	   		txtBrowseExcel.clear();
    	   	}
    	}
    	else
    	{
    		size=0;
    	}
	}
	
	
// ******************************************************************************	
	
	public void progressBarTest() 
	{
        Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
        ProgressBar progressBar = new ProgressBar();

        progressBar.setMinWidth(400);
        progressBar.setVisible(true);

        VBox updatePane = new VBox();
        updatePane.setPadding(new Insets(35));
        updatePane.setSpacing(3.0d);
        Label lbl = new Label("Processing.......");
        lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
        updatePane.getChildren().add(lbl);
        updatePane.getChildren().addAll(progressBar);

        taskUpdateStage.setScene(new Scene(updatePane));
        taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
        taskUpdateStage.show();
        
        task = new Task<Void>() 
        {
        	public Void call() throws Exception 
        	{
        		save();
                
        		int max = 1000;
                for (int i = 1; i <= max; i++) 
                {
                           // Thread.sleep(100);
                            /*if (BirtReportExportCon.FLAG == true) {
                                   break;
                            }*/
                }
                return null;
        	}
        };
        
        
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
               public void handle(WorkerStateEvent t) {
                      taskUpdateStage.close();
                      
                   
                      
                    if(isExcelFileIsCorrect==false)
          	   		{
          	   		Alert alert = new Alert(AlertType.INFORMATION);
      				alert.setTitle("Import Alert");
      				//alert.setTitle("Empty Field Validation");
      				alert.setContentText("Please select correct Excel format file");
      				alert.showAndWait();
      				isExcelFileIsCorrect=true;
          	   		}
                    else
                    {
                      if (list_ZoneWiseClientRate.size()==update_insert_count) {
          				
          				Alert alert = new Alert(AlertType.INFORMATION);
          				alert.setTitle("Task Complete");
          				//alert.setTitle("Empty Field Validation");
          				alert.setContentText("Rates successfully uploaded...");
          				alert.showAndWait();
                      }	
          			}
               }
        });
        
        
        progressBar.progressProperty().bind(task.progressProperty());
        new Thread(task).start();
      
	}
	
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
		//importExcelFile(selectedFile);
	}
	
// ******************************************************************************	
	
	public void importNewExcel(File selectedFile) throws IOException
	{
		list_ZoneWiseClientRate.clear();
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			
			
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			
			
			clientCode_global=clientCode[1];
			networkCode_global=networkCode[1];
			serviceCode_global=fileName;
			
			System.out.println("File Name: "+fileName);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
					
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
			
				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					int maxCell=0;
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						maxCell=  row.getLastCellNum();
						System.out.println("Total Columns >> "+maxCell);
						break;
						
					}
					
					if(maxCell>=2 && maxCell<=21)
					{
						isExcelFileIsCorrect=true;
						for(int k=1;k<=Sheet.getLastRowNum();k++)
						{
							row=(HSSFRow) Sheet.getRow(k);
							
							if(k>0)
							{
								list_zones.add(row.getCell(0).toString());
							}
							
								//list_zones.add(row.getCell(0).toString());
								
							ClientRateZoneBean clZoneBean=new ClientRateZoneBean();
								
							clZoneBean.setClient(clientCode[1]);
							clZoneBean.setNetwork(networkCode[1]);
							clZoneBean.setService(fileName);
							if(row.getCell(0).toString().equals("DO"))
							{
								clZoneBean.setZone_code("DO1");
							}
							else
							{
								clZoneBean.setZone_code(row.getCell(0).toString());
							}
						//	clZoneBean.setDa(row.getCell(1).toString());
						//	clZoneBean.setDb(row.getCell(2).toString());
							
							/*clZoneBean.setDd(row.getCell(4).toString());
							clZoneBean.setDe(row.getCell(5).toString());
							clZoneBean.setDf(row.getCell(6).toString());
							clZoneBean.setDg(row.getCell(7).toString());
							clZoneBean.setDh(row.getCell(8).toString());
							clZoneBean.setDi(row.getCell(9).toString());
							clZoneBean.setDj(row.getCell(10).toString());
							clZoneBean.setDk(row.getCell(11).toString());
							clZoneBean.setDl(row.getCell(12).toString());
							clZoneBean.setDm(row.getCell(13).toString());*/
							
					
							
							if(row.getCell(1)==null)
							{
								clZoneBean.setDa("0");
							}
							else
							{
								clZoneBean.setDa(row.getCell(1).toString());
							}
							
							if(row.getCell(2)==null)
							{
								clZoneBean.setDb("0");
							}
							else
							{
								clZoneBean.setDb(row.getCell(2).toString());
							}
							
							if(row.getCell(3)==null)
							{
								clZoneBean.setDc("0");
							}
							else
							{
								clZoneBean.setDc(row.getCell(3).toString());
							}
							
							if(row.getCell(4)==null)
							{
								clZoneBean.setDd("0");
							}
							else
							{
								clZoneBean.setDd(row.getCell(4).toString());
							}
							
							if(row.getCell(5)==null)
							{
								clZoneBean.setDe("0");
							}
							else
							{
								clZoneBean.setDe(row.getCell(5).toString());
							}
							
							if(row.getCell(6)==null)
							{
								clZoneBean.setDf("0");
							}
							else
							{
								clZoneBean.setDf(row.getCell(6).toString());
							}
							
							if(row.getCell(7)==null)
							{
								clZoneBean.setDg("0");
							}
							else
							{
								clZoneBean.setDg(row.getCell(7).toString());
							}
							
							if(row.getCell(8)==null)
							{
								clZoneBean.setDh("0");
							}
							else
							{
								clZoneBean.setDh(row.getCell(8).toString());
							}
							
							if(row.getCell(9)==null)
							{
								clZoneBean.setDi("0");
							}
							else
							{
								clZoneBean.setDi(row.getCell(9).toString());
							}
							
							if(row.getCell(10)==null)
							{
								clZoneBean.setDj("0");
							}
							else
							{
								clZoneBean.setDj(row.getCell(10).toString());
							}
							
							if(row.getCell(11)==null)
							{
								clZoneBean.setDk("0");
							}
							else
							{
								clZoneBean.setDk(row.getCell(11).toString());
							}
							
							if(row.getCell(12)==null)
							{
								clZoneBean.setDl("0");
							}
							else
							{
								clZoneBean.setDl(row.getCell(12).toString());
							}
							
							if(row.getCell(13)==null)
							{
								clZoneBean.setDm("0");
							}
							else
							{
								clZoneBean.setDm(row.getCell(13).toString());
							}
							
							if(row.getCell(14)==null)
							{
								clZoneBean.setDn("0");
							}
							else
							{
								clZoneBean.setDn(row.getCell(14).toString());
							}
							
							if(row.getCell(15)==null)
							{
								clZoneBean.setDo1("0");
							}
							else
							{
								clZoneBean.setDo1(row.getCell(15).toString());
							}
							
							if(row.getCell(16)==null)
							{
								clZoneBean.setDp("0");
							}
							else
							{
								clZoneBean.setDp(row.getCell(16).toString());
							}
							
							if(row.getCell(17)==null)
							{
								clZoneBean.setDq("0");
							}
							else
							{
								clZoneBean.setDq(row.getCell(17).toString());
							}
							
							if(row.getCell(18)==null)
							{
								clZoneBean.setDr("0");
							}
							else
							{
								clZoneBean.setDr(row.getCell(18).toString());
							}
							
							if(row.getCell(19)==null)
							{
								clZoneBean.setDs("0");
							}
							else
							{
								clZoneBean.setDs(row.getCell(19).toString());
							}
							
							if(row.getCell(20)==null)
							{
								clZoneBean.setDt("0");
							}
							else
							{
								clZoneBean.setDt(row.getCell(20).toString());
							}
						
							list_ZoneWiseClientRate.add(clZoneBean);
							
						}
					}
					else
					 {
						 isExcelFileIsCorrect=false;
						
					 }
					
					/*for(ClientRateZoneBean bean: list_ZoneWiseClientRate)
					{
						if(bean.getZone_code().equals("DO1"))
						{
						System.out.println(bean.getZone_code()+" | "+bean.getDn()+" | "+bean.getDo1());
						}
					}*/
				}
				
			}

			checkClientRateSlabDetails();
			//saveClientRateSlabDetails();
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
// ******************************************************************************

	/*public void importExcelFile(File selectedFile)
	{
		try
		{
			String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
			
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
						
			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
					
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				
				if(sheetName.contains("-"))
				{
				
				//String string = sheetName;
				String[] parts = sheetName.split("-");
				String part1 = parts[0]; 
				String part2 = parts[1]; 
				
				String checkWeight=null;
				String checkSlabType=null;
				String checkDox=null;
				String checkNonDox=null;
				
				
				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						ClientRateBean clientRateBean=new ClientRateBean();
						clientRateBean.setClientCode(clientCode[1]);
						clientRateBean.setNetworkCode(networkCode[1]);
						clientRateBean.setServiceCode(comboBoxService.getValue().trim());
						clientRateBean.setMappingType("zone");
						clientRateBean.setFromZoneOrCityCode(part1);
						clientRateBean.setToZoneOrCityCode(part2);
						
						if(row.getCell(0).getCellType()==Cell.CELL_TYPE_STRING)
						{
							String slab_type=row.getCell(0).getStringCellValue();
							if(slab_type.length()==1&&slab_type.equals("B")||slab_type.equals("K")||slab_type.equals("E"))
							{
								clientRateBean.setSlabType(slab_type);
							}
							
			    	    }
						
						if(row.getCell(1).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setWeightUpto(row.getCell(1).getNumericCellValue());
							checkWeight="ok";
						}
						else
						{
							checkWeight=null;
						}
						
						if(row.getCell(2).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setSlab(row.getCell(2).getNumericCellValue());
							checkSlabType="ok";
						}
						else
						{
							checkSlabType=null;
						}
						
						if(row.getCell(3).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setDox(row.getCell(3).getNumericCellValue());
							checkDox="ok";
						}
						else
						{
							checkDox=null;
						}
						
						if(row.getCell(4).getCellType()==Cell.CELL_TYPE_NUMERIC)
						{
							clientRateBean.setNonDox(row.getCell(4).getNumericCellValue());
							checkNonDox="ok";
						}
						else
						{
							checkNonDox=null;
						}
						 
						 
						sheetWiseDataList.addAll(clientRateBean);
						
						//System.out.println("Weight >>>>>>>>>> "+checkWeight);
						
						 if(clientRateBean.getSlabType()==null||
							checkWeight==null ||
							checkSlabType==null ||
							checkDox==null ||
							checkNonDox==null)
				    	  { 
							 //clientRateBean.setSheetName(Sheet.getSheetName());
				                faultSheetName.add(Sheet.getSheetName());
				                sheetWiseDataList.clear();
				    	      }
						}
					
						if(Sheet.getLastRowNum()!=sheetWiseDataList.size())
				    	{
				    		sheetWiseDataList.clear();
				    	}
				    
						if(Sheet.getLastRowNum()==sheetWiseDataList.size())
				    	{
							sheetWiseDataList2.addAll(sheetWiseDataList);
							sheetWiseDataList.clear();
				    	}
					}
					
					else
					{
						sheetWiseDataList.clear();
						flag_emptySheet=true;
						alertForEmptySheet="Sheet is empyt";
						faultSheetName.add(sheetName);
					//break;
					}
				}	
			
				else
				{
					sheetWiseDataList.clear();
					flag_emptySheet=true;
					alertForEmptySheet="Selected Excel file is empyt";
					faultSheetName.add(sheetName);
					//break;
				}
			}

		for(String bean: faultSheetName)
		{
			System.out.println("Sheet Name >>>>>>>>>> "+bean);
		}

		for(ClientRateBean bean : sheetWiseDataList2) 
		{
		System.out.println(bean.getClientCode()+" "+bean.getNetworkCode()+" "+bean.getServiceCode()+" "+
		bean.getMappingType()+" "+bean.getFromZoneOrCityCode()
		+" "+bean.getToZoneOrCityCode()+"  "+bean.getSlabType()
		+" "+bean.getWeightUpto()+" "+bean.getSlab()
		+" "+bean.getDox()+" "+bean.getNonDox());
		}

			
				//((FileInputStream) workbook).close();
			//saveClientRateDetails();
			fileInputStream.close();
		}
		
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}	*/	
	
	
	
	
// ******************************************************************************
	
	public int saveClientRateZoneWise(boolean isClientRateAvailable,String client, String network, String service, String zone_code) throws SQLException
	{
		
		int count=0;
		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;
			
		try 
		{
			for(ClientRateZoneBean clBean: list_ZoneWiseClientRate)
			{
				if(client.equals(clBean.getClient()) && network.equals(clBean.getNetwork()) &&
						service.equals(clBean.getService()) && zone_code.equals(clBean.getZone_code()))
				{
					if (isClientRateAvailable==false ) 
					{
						count++;
						String query = MasterSQL_Client_Rate_Utils.UPDATE_SQL_CLIENTRATE_ZONE_DETAILS;
						preparedStmt = con.prepareStatement(query);
						
						System.out.println("DO Value: "+clBean.getDo1());
						
						preparedStmt.setString(1, clBean.getDa());
						preparedStmt.setString(2, clBean.getDb());
						preparedStmt.setString(3, clBean.getDc());
						preparedStmt.setString(4, clBean.getDd());
						preparedStmt.setString(5, clBean.getDe());
						preparedStmt.setString(6, clBean.getDf());
						preparedStmt.setString(7, clBean.getDg());
						preparedStmt.setString(8, clBean.getDh());
						preparedStmt.setString(9, clBean.getDi());
						preparedStmt.setString(10, clBean.getDj());
						preparedStmt.setString(11, clBean.getDk());
						preparedStmt.setString(12, clBean.getDl());
						preparedStmt.setString(13, clBean.getDm());
						preparedStmt.setString(14, clBean.getDn());
						preparedStmt.setString(15, clBean.getDo1());
						preparedStmt.setString(16, clBean.getDp());
						preparedStmt.setString(17, clBean.getDq());
						preparedStmt.setString(18, clBean.getDr());
						preparedStmt.setString(19, clBean.getDs());
						preparedStmt.setString(20, clBean.getDt());
						preparedStmt.setString(21, clBean.getClient());
						preparedStmt.setString(22, clBean.getNetwork());
						preparedStmt.setString(23, clBean.getService());
						preparedStmt.setString(24, clBean.getZone_code());
						
						System.out.println("Update >>>> "+preparedStmt);
						
						count=preparedStmt.executeUpdate();
						break;
					
					}
					else
					{
						
						count++;
						String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_ZONE_DETAILS;
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, clBean.getClient());
						preparedStmt.setString(2, clBean.getNetwork());
						preparedStmt.setString(3, clBean.getService());
						preparedStmt.setString(4, clBean.getZone_code());
						preparedStmt.setString(5, clBean.getDa());
						preparedStmt.setString(6, clBean.getDb());
						preparedStmt.setString(7, clBean.getDc());
						preparedStmt.setString(8, clBean.getDd());
						preparedStmt.setString(9, clBean.getDe());
						preparedStmt.setString(10, clBean.getDf());
						preparedStmt.setString(11, clBean.getDg());
						preparedStmt.setString(12, clBean.getDh());
						preparedStmt.setString(13, clBean.getDi());
						preparedStmt.setString(14, clBean.getDj());
						preparedStmt.setString(15, clBean.getDk());
						preparedStmt.setString(16, clBean.getDl());
						preparedStmt.setString(17, clBean.getDm());
						preparedStmt.setString(18, clBean.getDn());
						preparedStmt.setString(19, clBean.getDo1());
						preparedStmt.setString(20, clBean.getDp());
						preparedStmt.setString(21, clBean.getDq());
						preparedStmt.setString(22, clBean.getDr());
						preparedStmt.setString(23, clBean.getDs());
						preparedStmt.setString(24, clBean.getDt());
						
						System.out.println("Insert >>>> "+preparedStmt);
					
						count=preparedStmt.executeUpdate();
						break;
						
					}
				}
			}
			//list_ZoneWiseClientRate.clear();
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		return count;
		
		
	}

// ******************************************************************************
	
	public void checkClientRateSlabDetails() throws SQLException
	{
		tabledata_ClientRate.clear();
		
		boolean isClientRateAvailable=false;

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;



		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_Client_Rate_Utils.CHECK_SLAB_DETAILS_SQL_CLIENTRATE;
			//rs = stmt.executeQuery(sql);


			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode_global);
			preparedStmt.setString(2,networkCode_global);
			preparedStmt.setString(3,serviceCode_global);


			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();

			if(!rs.next())
			{
				isClientRateAvailable=true;
			}
			else
			{
				isClientRateAvailable=false;
			}

			saveClientRateSlabDetails(isClientRateAvailable);
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	
// ******************************************************************************
	
	public void checkClientRateZoneDetails() throws SQLException
	{
		tabledata_ClientRate.clear();
		
		boolean isClientRateAvailable=false;

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;


		int count = 0;
		try 
		{
			for(ClientRateZoneBean clBean: list_ZoneWiseClientRate)
			{
				
				//stmt = con.createStatement();
				sql = MasterSQL_Client_Rate_Utils.CHECK_ZONE_DETAILS_SQL_CLIENTRATE;
				//rs = stmt.executeQuery(sql);
	
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,clBean.getClient());
				preparedStmt.setString(2,clBean.getNetwork());
				preparedStmt.setString(3,clBean.getService());
				preparedStmt.setString(4,clBean.getZone_code());
	
				//System.out.println("PS Sql: "+ preparedStmt);
				rs = preparedStmt.executeQuery();
	
				if(!rs.next())
				{
					isClientRateAvailable=true;
				}
				else
				{
					isClientRateAvailable=false;
				}
				count=count+saveClientRateZoneWise(isClientRateAvailable,clBean.getClient(),clBean.getNetwork(),clBean.getService(),clBean.getZone_code());
			}
			
			
			update_insert_count=count;
			
			
			
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	
// ******************************************************************************
	
	public void saveClientRateSlabDetails(boolean isClientRateAvailable) throws SQLException
	{


		//System.out.println("Sheet Size: "+sheetWiseDataList.size());
		
		System.out.println("Status: " +isClientRateAvailable);

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		try 
		{
			for(ClientRateZoneBean clientRateBean: list_ZoneWiseClientRate)
			{
				if (isClientRateAvailable==false) 
				{
					//count++;
					String query = MasterSQL_Client_Rate_Utils.UPDATE_SQL_CLIENTRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					

					preparedStmt.setDouble(1, Double.valueOf(txtBasic_range.getText()));
					preparedStmt.setDouble(2, Double.valueOf(txtAdd_slab.getText()));
					preparedStmt.setDouble(3, Double.valueOf(txtAdd_range.getText()));
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(4, "T");
					}
					else
					{
						preparedStmt.setString(4, "F");
					}
					
					preparedStmt.setString(5, clientRateBean.getClient());
					preparedStmt.setString(6, clientRateBean.getNetwork());
					preparedStmt.setString(7, clientRateBean.getService());
					
					preparedStmt.executeUpdate();
				
					break;
				}
				else
				{
					
					//count++;
					String query = MasterSQL_Client_Rate_Utils.INSERT_SQL_CLIENTRATE_SLAB_DETAILS;
					preparedStmt = con.prepareStatement(query);

					preparedStmt.setString(1, clientRateBean.getClient());
					preparedStmt.setString(2, clientRateBean.getNetwork());
					preparedStmt.setString(3, clientRateBean.getService());

					preparedStmt.setDouble(4, Double.valueOf(txtBasic_range.getText()));
					preparedStmt.setDouble(5, Double.valueOf(txtAdd_slab.getText()));
					preparedStmt.setDouble(6, Double.valueOf(txtAdd_range.getText()));
					
					if(checkBox_Kg.isSelected()==true)
					{
						preparedStmt.setString(7, "T");
					}
					else
					{
						preparedStmt.setString(7, "F");
					}
					
					preparedStmt.setString(8, MasterSQL_Client_Rate_Utils.APPLICATION_ID_FORALL);
					preparedStmt.setString(9 , MasterSQL_Client_Rate_Utils.LAST_MODIFIED_USER_FORALL);

					preparedStmt.executeUpdate();
				
					break;
				}

			}

			checkClientRateZoneDetails();
			//saveClientRateZoneWise(isClientRateAvailable);
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}


	}

	
// 	******************************************************************************
	
	public void alertForSaveClientRate()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not found");
		//alert.setHeaderText("Hello");
		
	/*	if(faultSheetName.size()==0)
		{
			alert.setContentText("Client Rate Successfully saved...");
			alert.showAndWait();
			//reset();
		}
		else
		{
			errorSheetAlert();
			
		}*/
		
		//sheetWiseDataList.clear();
		
		count=0;		
	}
	
	
	public void alertForEmptyExcelOrSheet()
	{
		flag_emptySheet=false;
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Empty Sheet");
		alert.setHeaderText(null);
		
	/*	if(alertForEmptySheet.equals("Sheet is empyt"))
		{
			errorSheetAlert();
			
		}
		else if(alertForEmptySheet.equals("Selected Excel file is empyt"))
		{
			errorSheetAlert();
			
		}*/
		
	}
	

// ******************************************************************************	
	
	/*public void errorSheetAlert()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Data Not Found: Excel Sheet Alert");
		alert.setHeaderText(null);
		
		alert.setHeaderText("Sheet is Empty/Incorrect Content/Incorrect Sheet Name\nPlease Check...");
		
		if(faultSheetName.size()>0)
		{
		Exception ex = new FileNotFoundException("Could not find file blabla.txt");

		// Create expandable Exception.
		//StringWriter sw = new StringWriter();
		//PrintWriter pw = new PrintWriter(sw);
		//ex.printStackTrace(pw);
		//String exceptionText = sw.toString();

		Label label = new Label("Error Sheets");

		TextArea textArea = new TextArea();
		
		String name=null;
		
		
		if(faultSheetName.size()==1)
		{
			for(String sheetName: faultSheetName)
			{
				textArea.setText(sheetName);
			}
		}
		else
		{
			for(String sheetName: faultSheetName)
			{
				name=name+","+sheetName;
						
			}
			
			textArea.setText(name.substring(5));
			
			//System.out.println("Name: >>>>>>>>>>>>>>>>> "+name);
		}
		
		textArea.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
		textArea.setStyle("-fx-text-fill: red;");
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(450);
		textArea.setMaxHeight(200);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(250);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);
		}
		alert.showAndWait();
		
		
		//reset();
		
		
	}*/
	
// ******************************************************************************	

	public void loadClientRateTable(String service) throws SQLException
	{
		tabledata_ClientRate.clear();

		System.out.println("Load table running...");
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;



		int slno = 1;
		try {

			//stmt = con.createStatement();
			sql = MasterSQL_Client_Rate_Utils.LOADTABLE_SQL_CLIENTRATE;
			//rs = stmt.executeQuery(sql);


			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode[1]);
			preparedStmt.setString(2,networkCode[1]);
			preparedStmt.setString(3,service);

			ClientRateZoneBean clZoneBean=new ClientRateZoneBean();
			
			System.out.println("PS Sql: "+ preparedStmt);

			rs = preparedStmt.executeQuery();
			slno=1;

			while (rs.next()) {

				ClientRateZoneBean clzBean=new ClientRateZoneBean();

				
				clZoneBean.setBasic_range(rs.getDouble("basic_range"));
				clZoneBean.setAdd_slab(rs.getDouble("add_slab"));
				clZoneBean.setAdd_range(rs.getDouble("add_range"));
				clzBean.setZone_code(rs.getString("zone_code"));
				clzBean.setKg(rs.getString("kg"));
				clzBean.setDa(rs.getString("da"));
				clzBean.setDb(rs.getString("db"));
				clzBean.setDc(rs.getString("dc"));
				clzBean.setDd(rs.getString("dd"));
				clzBean.setDe(rs.getString("de"));
				clzBean.setDf(rs.getString("df"));
				clzBean.setDg(rs.getString("dg"));
				clzBean.setDh(rs.getString("dh"));
				clzBean.setDi(rs.getString("di"));
				clzBean.setDj(rs.getString("dj"));
				clzBean.setDk(rs.getString("dk"));
				clzBean.setDl(rs.getString("dl"));
				clzBean.setDm(rs.getString("dm"));
			

				/*tabledata_ClientRate.add(new ClientRateZoneTableBean(slno, clzBean.getZone_code(), clzBean.getDa(),clzBean.getDb(),
						clzBean.getDc(), clzBean.getDd(), clzBean.getDe(), clzBean.getDf(), clzBean.getDg(), clzBean.getDh(), 
						clzBean.getDi(), clzBean.getDj(), clzBean.getDk(), clzBean.getDl(), clzBean.getDm()
						));*/

				slno++;
			}

			tableAddClientRate.setItems(tabledata_ClientRate);
			
			txtBasic_range.setText(String.valueOf(clZoneBean.getBasic_range()));
			txtAdd_slab.setText(String.valueOf(clZoneBean.getAdd_slab()));
			txtAdd_range.setText(String.valueOf(clZoneBean.getAdd_range()));
			

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// ******************************************************************************	
	
/*	public void reset()
	{

		sheetWiseDataList.clear();
		sheetWiseDataList2.clear();
		faultSheetName.clear();
		
		txtClient.clear();
		txtBrowseExcel.clear();
		comboBoxNetwork.getSelectionModel().clearSelection();
		//comboBoxService.getSelectionModel().clearSelection();
		txtClient.requestFocus();
	}*/
	
	
// ******************************************************************************		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	    
		//labAlert.setVisible(false);
		
		imgViewSearchIcon_ClientRateImport.setImage(new Image("/icon/searchicon_blue.png"));
		
		txtBrowseExcel.setEditable(false);
		
		tableCol_slno.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Integer>("slno"));
		tableCol_Zone.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,String>("zones"));
	    tableCol_da.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("da"));
		tableCol_db.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("db"));
		tableCol_dc.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dc"));
		tableCol_dd.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dd"));
		tableCol_de.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("de"));
		tableCol_df.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("df"));
	    tableCol_dg.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dg"));
		tableCol_dh.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dh"));
		tableCol_di.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("di"));
		tableCol_dj.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dj"));
		tableCol_dk.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dk"));
		tableCol_dl.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dl"));
		tableCol_dm.setCellValueFactory(new PropertyValueFactory<ClientRateZoneTableBean,Double>("dm"));
					
		ClientRateBean bean=new ClientRateBean();
		
		System.out.println("Slabtype: "+bean.getSlabType());
		System.out.println("Dox: "+bean.getDox());
	    
		try {
			loadClients();
			loadNetworks();
			loadServices();
			//loadClientRateTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		checkListViewService.setCellFactory(lv -> {
		    ListCell<String> cell = new ListCell<String>() {
		        @Override
		        protected void updateItem(String item, boolean empty) {
		            super.updateItem(item, empty);
		            if (empty) {
		                setText(null);
		            } else {
		                setText(item.toString());
		            }
		        }
		    };
		    cell.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
		        if (event.getButton()== MouseButton.PRIMARY && (! cell.isEmpty())) {
		            String item = cell.getItem();
		           
		            
		            try {
						System.out.println(item);
						loadClientRateTable(item);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
		    });
		    return cell ;
		});
		
	}

}
