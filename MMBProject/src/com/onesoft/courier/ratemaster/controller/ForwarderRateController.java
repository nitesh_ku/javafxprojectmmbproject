package com.onesoft.courier.ratemaster.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.common.bean.LoadZoneBean;
import com.onesoft.courier.home.controller.HomeController;
import com.onesoft.courier.ratemaster.bean.ForwarderRateTableBean;
import com.onesoft.courier.ratemaster.bean.ForwarderRateBean;
import com.onesoft.courier.ratemaster.bean.ForwarderRateTableBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Rate_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_Forwarder_Rate_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_Forwarder_Rate_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ForwarderRateController implements Initializable{
	
	private ObservableList<ForwarderRateTableBean> tabledata_ForwarderRate=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSlabTypeItems=FXCollections.observableArrayList(
																MasterSQL_Forwarder_Rate_Utils.FORWARDER_SLAB_TYPE_BASIC,
																MasterSQL_Forwarder_Rate_Utils.FORWARDER_SLAB_TYPE_ADDITIONAL,
																MasterSQL_Forwarder_Rate_Utils.FORWARDER_SLAB_TYPE_KG);
	
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	private List<String> list_Zone=new ArrayList<>();
	private List<String> list_City=new ArrayList<>();
	
	private	AutoCompletionBinding<String> autoBindFromZoneOrCity;
	private AutoCompletionBinding<String> autoBindToZoneOrCity;
	
	@FXML
	private ImageView imgViewSearchIcon_forwarderRate;

	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	@FXML
	private ComboBox<String> comboBoxSlabType;
	
	@FXML
	private TextField txtWeightUpto;
	
	@FXML
	private TextField txtSlab;
	
	@FXML
	private TextField txtDox;
	
	@FXML
	private TextField txtNonDox;
	
	/*@FXML
	private TextField txtClient;*/
	
	@FXML
	private TextField txtFromZoneOrCity;
	
	@FXML
	private TextField txtToZoneOrCity;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnDelete;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private RadioButton rdBtnZoneWise;
	
	@FXML
	private RadioButton rdBtnCityWise;
	
	@FXML
	private Label labFromZoneOrCity;
	
	@FXML
	private Label labToZoneOrCity;
	
	@FXML
	private Label labSlab;
	
// ===============================================	

	@FXML
	private TableView<ForwarderRateTableBean> tableAddForwarderRate;

	@FXML
	private TableColumn<ForwarderRateTableBean, Integer> tableCol_Slno;

	@FXML
	private TableColumn<ForwarderRateTableBean, String> tableCol_NetworkCode;

	@FXML
	private TableColumn<ForwarderRateTableBean, String> tableCol_SlabType;

	@FXML
	private TableColumn<ForwarderRateTableBean, Double> tableCol_WeightUpTo;

	@FXML
	private TableColumn<ForwarderRateTableBean, Double> tableCol_Slab;

	@FXML
	private TableColumn<ForwarderRateTableBean, Double> tableCol_Dox;

	@FXML
	private TableColumn<ForwarderRateTableBean, Double> tableCol_NonDox;	

	
// ******************************************************************************
		
	public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();

		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	


	// ******************************************************************************

	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();

		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();

			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());

			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}

		/*for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);*/

		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
	}	

	// ******************************************************************************

	public void loadZone() throws SQLException 
	{
		new LoadZone().loadZoneWithName();

		for (LoadZoneBean bean : LoadZone.SET_LOAD_ZONE_WITH_NAME) 
		{
			list_Zone.add(bean.getZoneName()+" | "+bean.getZoneCode());

		}

		autoBindFromZoneOrCity=TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);
		autoBindToZoneOrCity=TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);
	}		

	// *******************************************************************************

	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName()+" | "+bean.getCityCode());
		}


	}	

// ******************************************************************************	

	public void selectZoneOrCity()
	{
		if(rdBtnZoneWise.isSelected()==true)
		{
			txtFromZoneOrCity.clear();
			txtFromZoneOrCity.setPromptText("Select From Zone");
			autoBindFromZoneOrCity.dispose();
			autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);

			txtToZoneOrCity.clear();
			txtToZoneOrCity.setPromptText("Select To Zone");
			autoBindToZoneOrCity.dispose();
			autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);

			labFromZoneOrCity.setText("From Zone");
			labToZoneOrCity.setText("To Zone");

		}
		else
		{
			txtFromZoneOrCity.clear();
			txtFromZoneOrCity.setPromptText("Select From City");
			autoBindFromZoneOrCity.dispose();
			autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_City);

			txtToZoneOrCity.clear();
			txtToZoneOrCity.setPromptText("Select To City");
			autoBindToZoneOrCity.dispose();
			autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_City);

			labFromZoneOrCity.setText("From City");
			labToZoneOrCity.setText("To City");

		}

	}

// ******************************************************************************	

	public void showSlabLabelAndTextBox()
	{
		if(comboBoxSlabType.getValue().equals(MasterSQL_Forwarder_Rate_Utils.FORWARDER_SLAB_TYPE_ADDITIONAL))
		{
			labSlab.setVisible(true);
			txtSlab.setVisible(true);
		}
		else
		{
			labSlab.setVisible(false);
			txtSlab.setVisible(false);
		}
	}

// ******************************************************************************	

	public void setServiceViaNetwork()
	{
		if (comboBoxNetwork.getValue() != null) 
		{
			set_services.clear();
			comboBoxServiceItems.clear();

			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceCode());
				}
			}

			for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);
		}
	}
	


// *************************************************************************************
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		
		/*if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				//loadClientRateTable();
				comboBoxNetwork.requestFocus();
			}
		}*/

		if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				loadForwarderRateTable();
				setServiceViaNetwork();
				comboBoxService.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnZoneWise.requestFocus();
			}
		}

		else if (n.getId().equals("rdBtnZoneWise")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnCityWise.requestFocus();
			}

		}

		else if (n.getId().equals("rdBtnCityWise")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				selectZoneOrCity();
				txtFromZoneOrCity.requestFocus();
			}
		}

		else if (n.getId().equals("txtFromZoneOrCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtToZoneOrCity.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				
				comboBoxNetwork.setDisable(false);
				comboBoxService.setDisable(false);
				rdBtnCityWise.setDisable(false);
				rdBtnZoneWise.setDisable(false);
				comboBoxNetwork.requestFocus();
			}
			
		}

		else if (n.getId().equals("txtToZoneOrCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxSlabType.requestFocus();
			}
			
		}

		else if (n.getId().equals("comboBoxSlabType")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtWeightUpto.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				txtFromZoneOrCity.setDisable(false);
				txtToZoneOrCity.setDisable(false);
				txtFromZoneOrCity.requestFocus();
			}
		}

		else if (n.getId().equals("txtWeightUpto")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(txtSlab.isVisible()==true)
				{
					txtSlab.requestFocus();
				}
				else
				{
					txtDox.requestFocus();
				}
				
			}
		}

		else if (n.getId().equals("txtSlab")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				txtDox.requestFocus();
			}
		}

		else if (n.getId().equals("txtDox")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtNonDox.requestFocus();
			}
		}

		else if (n.getId().equals("txtNonDox")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnSave.requestFocus();
			}
		}

		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}


	}
	
	
// *******************************************************************************	
	
		public void setValidation() throws SQLException 
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);
			
			//double test=Double.valueOf(txtWeightUpto.getText());
			
			if (comboBoxNetwork.getValue() == null || comboBoxNetwork.getValue().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Network");
				alert.showAndWait();
				comboBoxNetwork.requestFocus();
			}
			
			else if(comboBoxService.getValue()==null)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Service field is Empty!");
				alert.showAndWait();
				comboBoxService.requestFocus();
			}
			
			else if (txtFromZoneOrCity.getText() == null || txtFromZoneOrCity.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a From Zone");
				alert.showAndWait();
				txtFromZoneOrCity.requestFocus();
			}
			
			else if (txtToZoneOrCity.getText() == null || txtToZoneOrCity.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a To Zone");
				alert.showAndWait();
				txtToZoneOrCity.requestFocus();
			}
			
			else if(comboBoxSlabType.getValue()==null)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Slab Type field is Empty!");
				alert.showAndWait();
				comboBoxSlabType.requestFocus();
			}
			
			else if (txtWeightUpto.getText() == null || txtWeightUpto.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a WeightUpto");
				alert.showAndWait();
				txtWeightUpto.requestFocus();
			}
		
			// expression for 45,1.1,0.1,.5 type values
			
			else if(!txtWeightUpto.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("WeightUpto Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtWeightUpto.clear();
				txtWeightUpto.requestFocus();
			}
			
			else if(txtSlab.isVisible()==true)
			{
				if(txtSlab.getText() == null || txtSlab.getText().isEmpty())
				{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Slab");
				alert.showAndWait();
				txtSlab.requestFocus();
				}
				
				else if (!txtSlab.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Slab Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtSlab.clear();
					txtSlab.requestFocus();
				}
				
				else if (txtDox.getText() == null || txtDox.getText().isEmpty()) 
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("You did not enter a Dox");
					alert.showAndWait();
					txtDox.requestFocus();
				}
				
				else if (!txtDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Dox Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtDox.clear();
					txtDox.requestFocus();
				}
				
				else if (txtNonDox.getText() == null || txtNonDox.getText().isEmpty()) 
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("You did not enter a Non-Dox");
					alert.showAndWait();
					txtNonDox.requestFocus();
				}
				
				else if (!txtNonDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Non-Dox Textfield: please enter only numeric values!!");
					alert.showAndWait();
					txtNonDox.clear();
					txtNonDox.requestFocus();
				}
				
				else
				{
					System.out.println("Forwarder Rate Saved..");
					saveForwarderRateDetails();
				}
			}
			
			else if (txtDox.getText() == null || txtDox.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Dox");
				alert.showAndWait();
				txtDox.requestFocus();
			}
			
			else if (!txtDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Dox Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtDox.clear();
				txtDox.requestFocus();
			}
			
			else if (txtNonDox.getText() == null || txtNonDox.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Non-Dox");
				alert.showAndWait();
				txtNonDox.requestFocus();
			}
			
			else if (!txtNonDox.getText().matches("\\d{0,7}([\\.]\\d{0,4})?"))
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Non-Dox Textfield: please enter only numeric values!!");
				alert.showAndWait();
				txtNonDox.clear();
				txtNonDox.requestFocus();
			}
			else
			{
				System.out.println("Forwarder Rate Saved..");
				
				saveForwarderRateDetails();
			}
			
		}	
		
		
		
// ******************************************************************************

	public void saveForwarderRateDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String[] fromZoneOrCityCode=txtFromZoneOrCity.getText().replaceAll("\\s+","").split("\\|");
		String[] toZoneOrCityCode=txtToZoneOrCity.getText().replaceAll("\\s+","").split("\\|");

		ForwarderRateBean forwarderRateBean=new ForwarderRateBean();

		forwarderRateBean.setNetworkCode(networkCode[1]);
		forwarderRateBean.setServiceCode(comboBoxService.getValue());
		forwarderRateBean.setFromZoneOrCityCode(fromZoneOrCityCode[1]);
		forwarderRateBean.setToZoneOrCityCode(toZoneOrCityCode[1]);
		forwarderRateBean.setSlabType(String.valueOf(comboBoxSlabType.getValue().charAt(0)));
		forwarderRateBean.setWeightUpto(Double.valueOf(txtWeightUpto.getText()));
		forwarderRateBean.setDox(Double.valueOf(txtDox.getText()));
		forwarderRateBean.setNonDox(Double.valueOf(txtNonDox.getText()));

		if(rdBtnZoneWise.isSelected()==true)
		{
			forwarderRateBean.setMappingType(MasterSQL_Forwarder_Rate_Utils.FORWARDER_MAPPING_TYPE_ZONE);
		}
		else
		{
			forwarderRateBean.setMappingType(MasterSQL_Forwarder_Rate_Utils.FORWARDER_MAPPING_TYPE_CITY);
		}

		if(txtSlab.isVisible()==true)
		{
			forwarderRateBean.setSlab(Double.valueOf(txtSlab.getText()));
		}
		else
		{
			forwarderRateBean.setSlab(0);
		}

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		try 
		{			
			System.out.println("Network Code: "+forwarderRateBean.getNetworkCode());
			System.out.println("Service Code: "+forwarderRateBean.getServiceCode());
			System.out.println("Mapping Type: "+forwarderRateBean.getMappingType());
			System.out.println("From Zone/City: "+forwarderRateBean.getFromZoneOrCityCode());
			System.out.println("To Zone/City: "+forwarderRateBean.getToZoneOrCityCode());
			System.out.println("Slab Type: "+forwarderRateBean.getSlabType());
			System.out.println("Weight Up To: "+forwarderRateBean.getWeightUpto());
			System.out.println("Slab: "+forwarderRateBean.getSlab());
			System.out.println("Dox: "+forwarderRateBean.getDox());
			System.out.println("Non Dox: "+forwarderRateBean.getNonDox());

			String query = MasterSQL_Forwarder_Rate_Utils.INSERT_SQL_FORWARDERRATE_DETAILS;
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, forwarderRateBean.getNetworkCode());
			preparedStmt.setString(2, forwarderRateBean.getServiceCode());
			preparedStmt.setString(3, forwarderRateBean.getMappingType());
			preparedStmt.setString(4, forwarderRateBean.getFromZoneOrCityCode());
			preparedStmt.setString(5, forwarderRateBean.getToZoneOrCityCode());

			if(forwarderRateBean.getSlabType().equals("A"))
			{
				preparedStmt.setString(6, "E");	
			}
			else
			{
				preparedStmt.setString(6, forwarderRateBean.getSlabType());
			}
			
			preparedStmt.setDouble(7, forwarderRateBean.getWeightUpto());
			preparedStmt.setDouble(8, forwarderRateBean.getSlab());
			preparedStmt.setDouble(9, forwarderRateBean.getDox());
			preparedStmt.setDouble(10, forwarderRateBean.getNonDox());
			preparedStmt.setString(11, MasterSQL_Forwarder_Rate_Utils.FORWARDER_APPLICATION_ID_FORALL);
			preparedStmt.setString(12 , MasterSQL_Forwarder_Rate_Utils.FORWARDER_LAST_MODIFIED_USER_FORALL);

			int status=preparedStmt.executeUpdate();

			if(status==1)
			{
				//reset();

				comboBoxNetwork.setDisable(true);
				comboBoxService.setDisable(true);
				txtFromZoneOrCity.setDisable(true);
				txtToZoneOrCity.setDisable(true);
				rdBtnCityWise.setDisable(true);
				rdBtnZoneWise.setDisable(true);
				comboBoxSlabType.requestFocus();
				loadForwarderRateTable();
				alert.setContentText("Client Rate Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Client Rate not saved!\nPlease try again...");
				alert.showAndWait();
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

// ******************************************************************************		

	/*public void checkBasicEntryForAdditionalRate() throws SQLException
	{
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		String[] fromZoneOrCityCode=txtFromZoneOrCity.getText().replaceAll("\\s+","").split("\\|");
		String[] toZoneOrCityCode=txtToZoneOrCity.getText().replaceAll("\\s+","").split("\\|");

		ForwarderRateBean forwarderRateBean=new ForwarderRateBean();

		forwarderRateBean.setNetworkCode(networkCode[1]);
		forwarderRateBean.setServiceCode(comboBoxService.getValue());
		forwarderRateBean.setFromZoneOrCityCode(fromZoneOrCityCode[1]);
		forwarderRateBean.setToZoneOrCityCode(toZoneOrCityCode[1]);
		forwarderRateBean.setSlabType(String.valueOf(comboBoxSlabType.getValue().charAt(0)));
		forwarderRateBean.setWeightUpto(Double.valueOf(txtWeightUpto.getText()));
		forwarderRateBean.setDox(Double.valueOf(txtDox.getText()));
		forwarderRateBean.setNonDox(Double.valueOf(txtNonDox.getText()));
		
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		boolean isRowAvailable=false;

		try 
		{
			
			sql = MasterSQL_Forwarder_Rate_Utils.CHECK_BASIC_RATE_ENTRY_IN_DB_SQL_FORWARDERRATE;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,forwarderRateBean.getNetworkCode());
			preparedStmt.setString(2,forwarderRateBean.getServiceCode());
			preparedStmt.setString(3,forwarderRateBean.getFromZoneOrCityCode());
			preparedStmt.setString(4,forwarderRateBean.getToZoneOrCityCode());
			preparedStmt.setString(5,forwarderRateBean.getSlabType());

			System.out.println("PS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			
			while (rs.next()) 
			{
				isRowAvailable=true;
			}
			
			
			if(isRowAvailable==true)
			{
				
			}
			else
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Please add atleast 1 Basic rate before adding Additional Rate");
				alert.setTitle("Basic rate alert");
				//alert.setContentText("You did not enter a Network");
				alert.showAndWait();
			}
		}
		
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
		
	}*/
	
// ******************************************************************************	

	public void loadForwarderRateTable() throws SQLException
	{
		tabledata_ForwarderRate.clear();

		System.out.println("Load table running...");
		String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		int slno = 1;

		try 
		{
			//stmt = con.createStatement();
			sql = MasterSQL_Forwarder_Rate_Utils.LOADTABLE_SQL_FORWARDERRATE;
			//rs = stmt.executeQuery(sql);

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,networkCode[1]);

			System.out.println("PS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
				ForwarderRateBean frBean=new ForwarderRateBean();
				frBean.setSlno_in_db(rs.getInt("forwarderratemasterid"));
				frBean.setNetworkCode(rs.getString("forwarderratemaster2vendorcode"));
				frBean.setSlabType(rs.getString("slabtype"));
				frBean.setWeightUpto(rs.getDouble("weightupto"));
				frBean.setSlab(rs.getDouble("slab"));
				frBean.setDox(rs.getDouble("dox"));
				frBean.setNonDox(rs.getDouble("nondox"));
					tabledata_ForwarderRate.add(new ForwarderRateTableBean(slno, frBean.getSlno_in_db(),frBean.getNetworkCode(), frBean.getSlabType(),
						frBean.getWeightUpto(), frBean.getSlab(), frBean.getDox(), frBean.getNonDox()));
					slno++;
			}
				tableAddForwarderRate.setItems(tabledata_ForwarderRate);
				/*for(ForwarderRateTableBean bean: tabledata_ClientRate)
				{
					System.out.println("DB slno: "+bean.getSlno_in_db()+" | Slab Type: "+bean.getSlabType());
				}*/
		}
		
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
		
	
// ******************************************************************************

	public void reset()
	{
		comboBoxNetwork.requestFocus();
		
		txtFromZoneOrCity.clear();
		txtToZoneOrCity.clear();
		txtWeightUpto.clear();
		txtSlab.setText("0");
		txtDox.clear();
		txtNonDox.clear();
		rdBtnZoneWise.setSelected(true);

		labFromZoneOrCity.setText("From Zone");
		labToZoneOrCity.setText("To Zone");

		comboBoxNetwork.getSelectionModel().clearSelection();
		comboBoxService.getSelectionModel().clearSelection();
		comboBoxServiceItems.clear();
		comboBoxSlabType.setValue(MasterSQL_Client_Rate_Utils.SLAB_TYPE_BASIC);

		txtSlab.setVisible(false);
		labSlab.setVisible(false);

		autoBindFromZoneOrCity.dispose();
		autoBindFromZoneOrCity = TextFields.bindAutoCompletion(txtFromZoneOrCity, list_Zone);
		autoBindToZoneOrCity.dispose();
		autoBindToZoneOrCity = TextFields.bindAutoCompletion(txtToZoneOrCity, list_Zone);

		comboBoxNetwork.setDisable(false);
		comboBoxService.setDisable(false);
		rdBtnCityWise.setDisable(false);
		rdBtnZoneWise.setDisable(false);
		txtFromZoneOrCity.setDisable(false);
		txtToZoneOrCity.setDisable(false);
		
	}	

	
// ******************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		comboBoxNetwork.requestFocus();	
		
		imgViewSearchIcon_forwarderRate.setImage(new Image("/icon/searchicon_blue.png"));
		
		comboBoxSlabType.setValue(MasterSQL_Forwarder_Rate_Utils.FORWARDER_SLAB_TYPE_BASIC);
		comboBoxSlabType.setItems(comboBoxSlabTypeItems);
		
		labSlab.setVisible(false);
		txtSlab.setVisible(false);
		
		rdBtnZoneWise.setSelected(true);
		comboBoxNetwork.requestFocus();
		
		
		tableCol_Slno.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,Integer>("slno"));
		tableCol_NetworkCode.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,String>("networkCode"));
		tableCol_Dox.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,Double>("dox"));
		tableCol_NonDox.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,Double>("nonDox"));
		tableCol_Slab.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,Double>("slab"));
		tableCol_SlabType.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,String>("slabType"));
		tableCol_WeightUpTo.setCellValueFactory(new PropertyValueFactory<ForwarderRateTableBean,Double>("weightUpto"));
		
		try 
		{
		
			loadNetworks();
			loadServices();
			loadZone();
			loadCity();
			//loadClientRateTable();
			
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
