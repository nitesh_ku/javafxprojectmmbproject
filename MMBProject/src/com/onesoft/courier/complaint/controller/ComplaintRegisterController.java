package com.onesoft.courier.complaint.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import com.onesoft.courier.complaint.bean.ComplaintRegisterBean;
import com.onesoft.courier.complaint.utils.ComplaintUtils;
import com.onesoft.courier.home.controller.HomeController;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.complaint.bean.ComplaintPendingTableBean;
import com.onesoft.courier.complaint.bean.ComplaintSolvedTableBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ComplaintRegisterController implements Initializable{
	
	private ObservableList<ComplaintPendingTableBean> pendingComplaintTabledata=FXCollections.observableArrayList();
	private ObservableList<ComplaintSolvedTableBean> solvedComplaintTabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxStatusItems=FXCollections.observableArrayList(ComplaintUtils.STATUS_SOLVED,ComplaintUtils.STATUS_UNSOLVED);
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	Main m=new Main();
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private ImageView imgSearchIcon1;
	
	@FXML
	CheckBox chkSMS;
	
	@FXML
	CheckBox chkEmail;
	
	@FXML
	private TextField txtindate;
	
	@FXML
	private TextField txtcomplaintno;
	
	@FXML
	private TextField txtawbno;
	
	@FXML
	private TextField txtname;
	
	@FXML
	private TextField txtemail;
	
	@FXML
	private TextField txtphoneno;
	
	@FXML
	private TextField txtcomplaintreason;
	
	@FXML
	private TextField txtremarks;
	
	@FXML
	private TextField txtbookingdate;
	
	@FXML
	private TextField txtweight;
	
	@FXML
	private TextField txtpcs;
	
	@FXML
	private TextField txtshippername;
	
	@FXML
	private TextField txtreceivername;
	
	@FXML
	private TextField txtshipperPhoneno;
	
	@FXML
	private TextField txtreceiverPhoneno;
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtService;
	
	@FXML
	private TextField txtNetwork;

	@FXML
	private ComboBox<String> comboBoxStatus;
	
	@FXML
	private TextArea txtareaTracking;
	
	@FXML
	private Button btnSubmit;
	
	@FXML	
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	
	
//================================================================

	@FXML
	private TableView<ComplaintPendingTableBean> pendingComplaintTable;
		
	@FXML
	private TableColumn<ComplaintPendingTableBean, Integer> pendingtable_serialno;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_complaintno;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_complaintname;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_emailID;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_indate;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_awbno;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_status;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, String> pendingtable_remarks;
	
	@FXML
	private TableColumn<ComplaintPendingTableBean, Long> pendingtable_phoneno;
	
	
//================================================================

	@FXML
	private TableView<ComplaintSolvedTableBean> solvedComplaintTable;
		
	@FXML
	private TableColumn<ComplaintSolvedTableBean, Integer> solvedtable_serialno;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_complaintno;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_complaintname;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_emailID;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_indate;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_awbno;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_status;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, String> solvedtable_remarks;
	
	@FXML
	private TableColumn<ComplaintSolvedTableBean, Long> solvedtable_phoneno;


// ============================================================================================================	

	/*public void showComplaintNumber() throws SQLException
	{
		String complaintNo=generateComplaintnumber();
		
		txtcomplaintno.setText(complaintNo);
		txtawbno.requestFocus();
	}*/
	
	
// ============================================================================================================	
	
	public void getdetailsViaCompalaintNo() throws SQLException
	{
		txtname.clear();
		txtphoneno.clear();
		txtemail.clear();
		txtcomplaintreason.clear();
		txtemail.clear();
	
		txtcomplaintno.setEditable(false);
		txtawbno.setEditable(false);
		
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		comboBoxStatus.setItems(comboBoxStatusItems);
		crbean.setComplaintno(txtcomplaintno.getText());
		//crbean.setAwbno(txtawbno.getText());
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			/*if(!txtcomplaintno.getText().equals(""))
			{*/

				String query = "select cr.awb_no, cr.complaint_no, cr.complainant_name, cr.complaint_date, cr.complainant_phoneno, cr.emailid, cr.status,"
							+ "cr.complaint_reason, cr.remarks, dbt.booking_date,dbt.billing_weight, dbt.dailybookingtransaction2city, "
							+ "dbt.dailybookingtransaction2service, dbt.dailybookingtransaction2network, dbt.packets from dailybookingtransaction as dbt,"
							+ "complaintregister as cr where dbt.air_way_bill_number=cr.awb_no and cr.complaint_no=?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, crbean.getComplaintno());	
		/*	}
			else
			{
				System.out.println("3rd query running:");
				String query = "select cr.awb_no, cr.complaint_no, cr.complainant_name, cr.complaint_date, cr.complainant_phoneno, cr.emailid, cr.status,"
							+ "cr.complaint_reason, cr.remarks, dbt.booking_date,dbt.billing_weight, dbt.dailybookingtransaction2city,"
							+ "dbt.dailybookingtransaction2service, dbt.dailybookingtransaction2network, dbt.packets from dailybookingtransaction as dbt,"
							+ "complaintregister as cr where dbt.air_way_bill_number=cr.awb_no and cr.awb_no=?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, crbean.getAwbno());	
			}*/
			
			rs = preparedStmt.executeQuery();
			
			if(!rs.next())
			{
				
			}
			else
			{
				do{
				crbean.setAwbno(rs.getString("awb_no"));	
				crbean.setComplaintno(rs.getString("complaint_no"));
				crbean.setName(rs.getString("complainant_name"));
				crbean.setPhone_no(rs.getLong("complainant_phoneno"));
				crbean.setComplaintdate(date.format(rs.getDate("complaint_date")));
				crbean.setEmail(rs.getString("emailid"));
				crbean.setStatus(rs.getString("status"));
				crbean.setComplaintreason(rs.getString("complaint_reason"));
				crbean.setRemarks(rs.getString("remarks"));
				crbean.setBookingdate(date.format(rs.getDate("booking_date")));
				crbean.setWeight(Double.parseDouble(df.format(rs.getDouble("billing_weight"))));
				crbean.setDestination(rs.getString("dailybookingtransaction2city"));
				crbean.setService(rs.getString("dailybookingtransaction2service"));
				crbean.setNetwork(rs.getString("dailybookingtransaction2network"));
				crbean.setPcs(rs.getInt("packets"));
				crbean.setRemarks(rs.getString("remarks"));
				}while(rs.next());
			}
		
			txtawbno.setText(crbean.getAwbno());
			txtcomplaintno.setText(crbean.getComplaintno());
			txtname.setText(crbean.getName());
			txtindate.setText(crbean.getComplaintdate());
			txtphoneno.setText(String.valueOf(crbean.getPhone_no()));
			txtemail.setText(crbean.getEmail());
			
			txtcomplaintreason.setText(crbean.getComplaintreason());
			txtbookingdate.setText(crbean.getBookingdate());
			txtweight.setText(String.valueOf(crbean.getWeight()));
			txtDestination.setText(crbean.getDestination());
			txtService.setText(crbean.getService());
			txtNetwork.setText(crbean.getNetwork());
			txtpcs.setText(String.valueOf(crbean.getPcs()));
			txtareaTracking.setText(crbean.getRemarks());
			
			
			if(crbean.getStatus().equals(ComplaintUtils.STATUS_SOLVED))
			{
				comboBoxStatus.setValue(crbean.getStatus());
			}
			else
			{
			comboBoxStatus.setValue(ComplaintUtils.STATUS_UNSOLVED);
			}
		
			txtname.requestFocus();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}	
	
	
// ============================================================================================================	
	
		public void getDetailfromAWBnoWithComplaintno() throws SQLException
		{
			txtname.clear();
			txtphoneno.clear();
			txtemail.clear();
			txtcomplaintreason.clear();
			txtemail.clear();
		
			txtcomplaintno.setEditable(false);
			txtawbno.setEditable(false);
			
			ComplaintRegisterBean crbean=new ComplaintRegisterBean();
			comboBoxStatus.setItems(comboBoxStatusItems);
			//crbean.setComplaintno(txtcomplaintno.getText());
			crbean.setAwbno(txtawbno.getText());
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			ResultSet rs=null;
			
			PreparedStatement preparedStmt=null;
			
			try
			{	
		
					String query = "select cr.awb_no, cr.complaint_no, cr.complainant_name, cr.complaint_date, cr.complainant_phoneno, cr.emailid, cr.status,"
								+ "cr.complaint_reason, cr.remarks, dbt.booking_date,dbt.billing_weight, dbt.dailybookingtransaction2city,"
								+ "dbt.dailybookingtransaction2service, dbt.dailybookingtransaction2network, dbt.packets from dailybookingtransaction as dbt,"
								+ "complaintregister as cr where dbt.air_way_bill_number=cr.awb_no and cr.awb_no=?";
					preparedStmt = con.prepareStatement(query);
					preparedStmt.setString(1, crbean.getAwbno());	

				
				rs = preparedStmt.executeQuery();
				
				if(!rs.next())
				{
					
				}
				else
				{
					do{
					crbean.setAwbno(rs.getString("awb_no"));	
					crbean.setComplaintno(rs.getString("complaint_no"));
					crbean.setName(rs.getString("complainant_name"));
					crbean.setPhone_no(rs.getLong("complainant_phoneno"));
					crbean.setComplaintdate(date.format(rs.getDate("complaint_date")));
					crbean.setEmail(rs.getString("emailid"));
					crbean.setStatus(rs.getString("status"));
					crbean.setComplaintreason(rs.getString("complaint_reason"));
					crbean.setRemarks(rs.getString("remarks"));
					crbean.setBookingdate(date.format(rs.getDate("booking_date")));
					crbean.setWeight(Double.parseDouble(df.format(rs.getDouble("billing_weight"))));
					crbean.setDestination(rs.getString("dailybookingtransaction2city"));
					crbean.setService(rs.getString("dailybookingtransaction2service"));
					crbean.setNetwork(rs.getString("dailybookingtransaction2network"));
					crbean.setPcs(rs.getInt("packets"));
					crbean.setRemarks(rs.getString("remarks"));
					}while(rs.next());
				}
			
				txtawbno.setText(crbean.getAwbno());
				txtcomplaintno.setText(crbean.getComplaintno());
				txtname.setText(crbean.getName());
				txtindate.setText(crbean.getComplaintdate());
				txtphoneno.setText(String.valueOf(crbean.getPhone_no()));
				txtemail.setText(crbean.getEmail());
				//comboBoxStatus.setValue(crbean.getStatus());
				txtcomplaintreason.setText(crbean.getComplaintreason());
				txtbookingdate.setText(crbean.getBookingdate());
				txtweight.setText(String.valueOf(crbean.getWeight()));
				txtDestination.setText(crbean.getDestination());
				txtService.setText(crbean.getService());
				txtNetwork.setText(crbean.getNetwork());
				txtpcs.setText(String.valueOf(crbean.getPcs()));
				txtareaTracking.setText(crbean.getRemarks());
				
				if(crbean.getStatus().equals(ComplaintUtils.STATUS_SOLVED))
				{
					comboBoxStatus.setValue(crbean.getStatus());
				}
				else
				{
				comboBoxStatus.setValue(ComplaintUtils.STATUS_UNSOLVED);
				}
				
				txtname.requestFocus();
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, rs, con);
			}
		}		

		
// ============================================================================================================	

	public void checkComplaintno() throws SQLException
	{
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		comboBoxStatus.setItems(comboBoxStatusItems);
		crbean.setAwbno(txtawbno.getText());
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String query = "select complaint_no from complaintregister where awb_no=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, crbean.getAwbno());
			
			rs = preparedStmt.executeQuery();
			
			if(!rs.next())
			{
				//System.out.println("if running");
				getdetailsViaAWBno();
			}
			else
			{
				//System.out.println("else running");
				getDetailfromAWBnoWithComplaintno();
			}
		
			
			txtname.requestFocus();
		
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	
// ============================================================================================================	
	
	public void getdetailsViaAWBno() throws SQLException
	{
		txtcomplaintno.clear();
		txtname.clear();
		txtphoneno.clear();
		txtemail.clear();
		txtcomplaintreason.clear();
		txtemail.clear();
		txtareaTracking.clear();
		
		txtcomplaintno.setEditable(false);
		txtawbno.setEditable(false);
		
		
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		comboBoxStatus.setItems(comboBoxStatusItems);
		crbean.setAwbno(txtawbno.getText());
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String query = "select booking_date,billing_weight, dailybookingtransaction2city, dailybookingtransaction2service, dailybookingtransaction2network, packets from dailybookingtransaction where air_way_bill_number=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, crbean.getAwbno());
			
			rs = preparedStmt.executeQuery();
			
			if(!rs.next())
			{
				
			}
			else
			{
				txtcomplaintno.setDisable(true);
				
				do{
				crbean.setBookingdate(date.format(rs.getDate("booking_date")));
				crbean.setWeight(Double.parseDouble(df.format(rs.getDouble("billing_weight"))));
				crbean.setDestination(rs.getString("dailybookingtransaction2city"));
				crbean.setService(rs.getString("dailybookingtransaction2service"));
				crbean.setNetwork(rs.getString("dailybookingtransaction2network"));
				crbean.setPcs(rs.getInt("packets"));
				}while(rs.next());
			}
		
			txtbookingdate.setText(crbean.getBookingdate());
			txtweight.setText(String.valueOf(crbean.getWeight()));
			txtDestination.setText(crbean.getDestination());
			txtService.setText(crbean.getService());
			txtNetwork.setText(crbean.getNetwork());
			txtpcs.setText(String.valueOf(crbean.getPcs()));
			
			comboBoxStatus.setValue(ComplaintUtils.STATUS_UNSOLVED);
			
			txtname.requestFocus();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}

	}
	
// ============================================================================================================

	public void complaintRegisterValidation() throws SQLException, ParseException
	{
		System.out.println("Validation 1 =============");
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemail.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    
	    boolean checkmail=matcher.matches();

	    System.out.println("Validation 2 =============");
	    
	    
	    
	    if(txtawbno.getText()!=null && txtawbno.getText().isEmpty())
		{
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Complaint Registration");
			alert.setTitle("Empty Field");
			alert.setContentText("AWB no. field is Empty!");
			alert.showAndWait();
			txtawbno.requestFocus();
		}
	    else if(txtname.getText()!=null && txtname.getText().isEmpty())
		{
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Complaint Registration");
			alert.setTitle("Empty Field");
			alert.setContentText("Name field is Empty!");
			alert.showAndWait();
			txtname.requestFocus();
		}
	    else if(txtphoneno.getText()!=null && txtphoneno.getText().isEmpty())
		{
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Complaint Registration");
			alert.setTitle("Empty Field");
			alert.setContentText("Phone no. field is Empty!");
			alert.showAndWait();
			txtphoneno.requestFocus();
		}
	    else if(!txtphoneno.getText().matches("[0-9]*"))
		{
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtphoneno.requestFocus();
		}
	    else if(txtphoneno.getText().length()>10)
	    {
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Complaint Registration");
			alert.setHeaderText(null);
			alert.setContentText("Please enter a valid Mobile no.");
			alert.showAndWait();
			txtphoneno.requestFocus();
	    }
	 
	    /*else if(txtemail.getText()!=null && txtemail.getText().isEmpty())
		{
	    	Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Complaint Registration");
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Email field is Empty!");
			alert.showAndWait();
			txtemail.requestFocus();
		}*/
	    else if(!txtemail.getText().equals(""))
	    {
	    	if(checkmail==false)
	    	{
	    		Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Complaint Registration");
				alert.setHeaderText(null);
				alert.setContentText("Please enter a valid Email ID");
				alert.showAndWait();
				txtemail.requestFocus();
	    	
	    	
				}
	    	else
	    	{	
	    		System.out.println("Validation 3 =============");
	    		registerComplaint();
	    	}
	    }
	    
	    else if(txtcomplaintreason.getText()!=null && txtcomplaintreason.getText().isEmpty())
 		{
 	    	Alert alert = new Alert(AlertType.INFORMATION);
 			alert.setTitle("Complaint Registration");
 			alert.setTitle("Empty Field Validation");
 			alert.setContentText("Complaint Reason field is Empty!");
 			alert.showAndWait();
 			txtcomplaintreason.requestFocus();
 		}
	    else //if(txtemail.getText().equals(""))
		{
	    	System.out.println("Validation 4 =============");
		   	registerComplaint();
		}
	    
	}
	   

// ============================================================================================================	
	
	public void registerComplaint() throws SQLException, ParseException
	{
		pendingComplaintTabledata.clear();
		solvedComplaintTabledata.clear();
		
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		java.util.Date now = new java.util.Date();
		//String autocomplaintno=generateComplaintnumber();
			
		LocalDate indate = LocalDate.parse(txtindate.getText(), formatter);
		Date complaintdate=Date.valueOf(indate);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		String query=null;
		
		comboBoxStatus.setItems(comboBoxStatusItems);
		
		//String firstTimecompaintno="COM121000001";
		
		String currentdatetime = sdfDate.format(now);
	
		try
		{	
			
			/*if(autocomplaintno.equals("null"))
			{
				crbean.setComplaintno(firstTimecompaintno);	
			}
			else
			{
				crbean.setComplaintno(autocomplaintno);
			}*/
			
			crbean.setAwbno(txtawbno.getText());
			crbean.setName(txtname.getText());
			if(!txtphoneno.getText().equals(""))
			{
				crbean.setPhone_no(Long.parseLong(txtphoneno.getText()));	
			}
			
			crbean.setStatus(comboBoxStatus.getValue());
			crbean.setEmail(txtemail.getText());
			crbean.setComplaintreason(txtcomplaintreason.getText());
			
			
			if(txtcomplaintno.getText().equals(""))
			{
				
				//query = "insert into complaintregister(complaint_no, complaint_date, awb_no, complainant_name, complainant_phoneno, emailid, status,complaint_reason,remarks) values(?,?,?,?,?,?,?,?,?)";
				query = "insert into complaintregister(complaint_date, awb_no, complainant_name, complainant_phoneno, emailid, status,"
						+ "complaint_reason,remarks) values(?,?,?,?,?,?,?,?)";
				preparedStmt = con.prepareStatement(query);
				
				//preparedStmt.setString(1, crbean.getComplaintno());
				preparedStmt.setDate(1, complaintdate);
				preparedStmt.setString(2, crbean.getAwbno());
				preparedStmt.setString(3, crbean.getName());
				preparedStmt.setLong(4, crbean.getPhone_no());
				preparedStmt.setString(5, crbean.getEmail());
				preparedStmt.setString(6, crbean.getStatus());
				preparedStmt.setString(7, crbean.getComplaintreason());
				preparedStmt.setString(8, CommonVariable.LOGGED_IN_USER+" "+currentdatetime+" "+crbean.getStatus());
				//preparedStmt.setString(9, HomeController.loggedInUSER+" "+currentdatetime+" "+crbean.getStatus());
				
			}
			else
			{
				String userdetails=null;
				String[] trackuser=txtareaTracking.getText().split("\\,");
			
				query="update complaintregister set  complainant_name=?,complainant_phoneno=?,emailid=?,status=?,complaint_reason=?,remarks=? where complaint_no=?";
			
				preparedStmt = con.prepareStatement(query);
			
				preparedStmt.setString(1, crbean.getName());
				preparedStmt.setLong(2, crbean.getPhone_no());
				preparedStmt.setString(3, crbean.getEmail());
				preparedStmt.setString(4, crbean.getStatus());
				preparedStmt.setString(5, crbean.getComplaintreason());
			
				if(trackuser.length>4)
				{
					System.out.println("Track user if");
					userdetails=trackuser[0]+","+trackuser[1]+","+trackuser[2]+","+trackuser[3];	
					//preparedStmt.setString(6, HomeController.loggedInUSER+" "+currentdatetime+" "+crbean.getStatus()+" , "+userdetails);
					preparedStmt.setString(6, CommonVariable.LOGGED_IN_USER+" "+currentdatetime+" "+crbean.getStatus()+" , "+userdetails);
				}
				else
				{
					System.out.println("Track user else");
					
					preparedStmt.setString(6, CommonVariable.LOGGED_IN_USER+" "+currentdatetime+" "+crbean.getStatus()+" , "+txtareaTracking.getText());
					//preparedStmt.setString(6, HomeController.loggedInUSER+" "+currentdatetime+" "+crbean.getStatus()+" , "+txtareaTracking.getText());
				}
				preparedStmt.setString(7, txtcomplaintno.getText());
				
			}
			
			preparedStmt.executeUpdate();
			
			showPendingComplaints();
			showSolvedComplaints();
			
			
			if(txtcomplaintno.getText().equals(""))
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Complaint Registration");
				alert.setHeaderText(null);
				alert.setContentText("Complaint against awb no.: "+crbean.getAwbno()+" is successfully registered!"
						+ "\n"
						+ "Your complaint no. is: "+crbean.getComplaintno());
				alert.showAndWait();
			}
			else
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Complaint Registration Updated");
				alert.setHeaderText(null);
				alert.setContentText("Complaint Details against Complaint no.: "+crbean.getAwbno()+" is successfully Updated!");
				alert.showAndWait();
			}
			mailCheckbox(crbean.getComplaintno());
			refresh();
				
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}	


		
// ============================================================================================================
	
	/*public String generateComplaintnumber() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select complaint_no from complaintregister order by complaint_no DESC limit 1";
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				crbean.setComplaintno("null");
			}
			else
			{
				//System.out.println(rs.getString("complaint_no"));
				crbean.setComplaint_startno(rs.getString("complaint_no").replaceAll("[^A-Za-z]+", ""));
				crbean.setComplaint_endno(Integer.parseInt(rs.getString("complaint_no").replaceAll("\\D+",""))+1);
				crbean.setComplaintno(crbean.getComplaint_startno()+crbean.getComplaint_endno());
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return crbean.getComplaintno();
	}*/
	
	
//--------------------------------------------------------------------------------------------------
	
	public void showPendingComplaints() throws SQLException
	{
		ComplaintRegisterBean crbean=new ComplaintRegisterBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select * from complaintregister where status='"+ComplaintUtils.STATUS_UNSOLVED+"' order by complaint_no";
			rs=st.executeQuery(sql);
	
			int serialno=1;
			
			if(!rs.next())
			{

			}
			else
			{
				do
				{
					crbean.setSerialno(serialno);
					crbean.setComplaintno(rs.getString("complaint_no"));
					crbean.setName(rs.getString("complainant_name"));
					crbean.setIndate(date.format(rs.getDate("complaint_date")));
					crbean.setAwbno(rs.getString("awb_no"));
					crbean.setPhone_no(rs.getLong("complainant_phoneno"));
					crbean.setEmail(rs.getString("emailid"));
					crbean.setStatus(rs.getString("status"));
					crbean.setRemarks(rs.getString("complaint_reason"));
			
					pendingComplaintTabledata.addAll(new ComplaintPendingTableBean(crbean.getSerialno(), crbean.getComplaintno(), crbean.getName(), crbean.getEmail(), crbean.getIndate(), crbean.getAwbno(), crbean.getStatus(), crbean.getRemarks(), crbean.getPhone_no()));
					
					serialno++;
				}
				while(rs.next());
				
				pendingComplaintTable.setItems(pendingComplaintTabledata);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
// -------------------------------------------------------------------------------------------------
	
		public void showSolvedComplaints() throws SQLException
		{
			ComplaintRegisterBean crbean=new ComplaintRegisterBean();
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			
			try
			{	
				st=con.createStatement();
				String sql="select * from complaintregister where status='"+ComplaintUtils.STATUS_SOLVED+"'order by complaint_no";
				rs=st.executeQuery(sql);
		
				int serialno=1;
				
				if(!rs.next())
				{

					
				}
				else
				{
					do
					{
						crbean.setSerialno(serialno);
						crbean.setComplaintno(rs.getString("complaint_no"));
						crbean.setName(rs.getString("complainant_name"));
						crbean.setIndate(date.format(rs.getDate("complaint_date")));
						crbean.setAwbno(rs.getString("awb_no"));
						crbean.setPhone_no(rs.getLong("complainant_phoneno"));
						crbean.setEmail(rs.getString("emailid"));
						//crbean.setStatus(rs.getString("status"));
						crbean.setRemarks(rs.getString("complaint_reason"));
				
						solvedComplaintTabledata.addAll(new ComplaintSolvedTableBean(crbean.getSerialno(), crbean.getComplaintno(), crbean.getName(), crbean.getEmail(), crbean.getIndate(), crbean.getAwbno(), null, crbean.getRemarks(), crbean.getPhone_no()));
						
						serialno++;
					}
					while(rs.next());
					
					solvedComplaintTable.setItems(solvedComplaintTabledata);
					
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
	
// ====================================================================================================================	

	public void mailCheckbox(String cmpno) throws SQLException
	{
		if(chkEmail.isSelected()==true)
		{
			sendMail(cmpno);
		}
		
	}		
		
	
// ====================================================================================================================	

	public void sendMail(String complaintno)
	{	
		
		String msg= "Complaint Details:"
				+ "<table border=2 bordercolor=black>"
					+ "<tr>"
						+ "<td><b>Complaint No.</b></td>"
						+ "<td><b>Complainant Name</b></td>"
						+ "<td><b>AWB No.</b></td>"
						+ "<td><b>Complaint Date</b></td>"
						+ "<td><b>Phone No</b></td>"
						+ "<td><b>Mail ID</b></td>"
						+ "<td><b>Status</b></td>"
						+ "<td><b>Reason</b></td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>"+complaintno+"</td>"
						+ "<td>"+txtname.getText()+"</td>"
						+ "<td>"+txtawbno.getText()+"</td>"
						+ "<td>"+txtindate.getText()+"</td>"
						+ "<td>"+txtphoneno.getText()+"</td>"
						+ "<td>"+txtemail.getText()+"</td>"
						+ "<td>"+comboBoxStatus.getValue()+"</td>"
						+ "<td>"+txtcomplaintreason.getText()+"</td>"
					+ "</tr>"
				+ "</table>";
		
		
		Properties props = new Properties(); 
		props.put("mail.smtp.host","mail.onesoft.in"); 
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port","587");
					
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication("nitesh@onesoft.in","nitesh@321");  
				      }  
				  });  
						   
		try
		{  
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("nitesh@onesoft.in"));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress("surjeet@onesoft.in"));
			message.setSubject("Complaint Registration Details");
		
			
			message.setContent(msg,"text/html");
			Transport.send(message);
						    	
			System.out.println("Email send Successfully");
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		} 
		
		 System.out.println("Authentication method is closed");
		 System.out.println("\n");
		
	}	
	

	
// ============================================================================================================

	public void refresh() throws SQLException
	{
		java.util.Date currentdate = new java.util.Date();
		txtindate.setText(date.format(currentdate));
		
		txtcomplaintno.setDisable(false);
		txtcomplaintno.setEditable(true);
		txtawbno.setEditable(true);
		
		txtcomplaintno.clear();
		txtawbno.clear();
		txtname.clear();
		txtphoneno.clear();
		txtemail.clear();
		txtcomplaintreason.clear();
		txtemail.clear();
		
		txtbookingdate.clear();
		txtService.clear();
		txtNetwork.clear();
		txtweight.clear();
		txtpcs.clear();
		txtDestination.clear();
		txtshippername.clear();
		txtshipperPhoneno.clear();
		txtreceivername.clear();
		txtreceiverPhoneno.clear();
		txtareaTracking.clear();
		
		//showComplaintNumber();
	}


// ============================================================================================================	

	public void exit() throws IOException, SQLException
	{
		m.homePage();
	}
	
	
// ============================================================================================================	


	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, ParseException{

		Node n=(Node) e.getSource();
		
		
		
		if(n.getId().equals("txtname"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtphoneno.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtphoneno"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtemail.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtemail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtcomplaintreason.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtcomplaintreason"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtshippername.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtshippername"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtreceivername.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtreceivername"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtshipperPhoneno.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtshipperPhoneno"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtreceiverPhoneno.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtreceiverPhoneno"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkSMS.requestFocus();
			}
		}
		
		else if(n.getId().equals("chkSMS"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkEmail.requestFocus();
			}
		}
		else if(n.getId().equals("chkEmail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				btnSubmit.requestFocus();
			}
		}
		else if(n.getId().equals("btnSubmit"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				complaintRegisterValidation();
				
			}
		}
	}
// ============================================================================================================

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	
		imgSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIcon1.setImage(new Image("/icon/searchicon_blue.png"));
		
		String s="niteshkumar";
		System.out.println("length: "+s.length());
	
		java.util.Date currentdate = new java.util.Date();
		txtindate.setText(date.format(currentdate));
		comboBoxStatus.setItems(comboBoxStatusItems);
		txtawbno.requestFocus();
		
		  //dd/MM/yyyy
		/*    java.util.Date now = new java.util.Date();
		    String strDate = sdfDate.format(now);
		    System.out.println("current date and time: "+strDate);*/
		
		pendingtable_serialno.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,Integer>("serialno"));
		
		pendingtable_complaintno.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("complaintno"));
		pendingtable_complaintname.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("complaintname"));
		pendingtable_emailID.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("emailID"));
		pendingtable_indate.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("indate"));
		pendingtable_awbno.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("awbno"));
		pendingtable_status.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("status"));
		pendingtable_remarks.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,String>("remarks"));
		pendingtable_phoneno.setCellValueFactory(new PropertyValueFactory<ComplaintPendingTableBean,Long>("phone"));
		
		
		solvedtable_serialno.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,Integer>("serialno"));
		
		solvedtable_complaintno.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("complaintno"));
		solvedtable_complaintname.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("complaintname"));
		solvedtable_emailID.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("emailID"));
		solvedtable_indate.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("indate"));
		solvedtable_awbno.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("awbno"));
		//solvedtable_status.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("status"));
		solvedtable_remarks.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,String>("remarks"));
		solvedtable_phoneno.setCellValueFactory(new PropertyValueFactory<ComplaintSolvedTableBean,Long>("phone"));
		
		
		
		try {
			//showComplaintNumber();
			showPendingComplaints();
			showSolvedComplaints();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}
