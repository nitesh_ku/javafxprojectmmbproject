package com.onesoft.courier.complaint.bean;

import org.controlsfx.control.CheckComboBox;

import javafx.fxml.FXML;

public class ComplaintRegisterBean {

	private int serialno;
	
	private String complaintno;
	private String complaint_startno;
	private int complaint_endno;
	private String indate;
	private String complaintdate;
	private String awbno;
	private String name;
	private String email;
	private String status;
	private String complaintreason;
	private String remarks;
	private String bookingdate;
	private String destination;
	private String service;
	private String network;
	private String shipper;
	private String receiver;
	
	private double weight;
	
	private int pcs;
	private long phone_no;
	private long sp_phone_no;
	private long rc_phone_no;
	
	public String getComplaintno() {
		return complaintno;
	}
	public void setComplaintno(String complaintno) {
		this.complaintno = complaintno;
	}
	public String getIndate() {
		return indate;
	}
	public void setIndate(String indate) {
		this.indate = indate;
	}
	public String getAwbno() {
		return awbno;
	}
	public void setAwbno(String awbno) {
		this.awbno = awbno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComplaintreason() {
		return complaintreason;
	}
	public void setComplaintreason(String complaintreason) {
		this.complaintreason = complaintreason;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public long getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(long phone_no) {
		this.phone_no = phone_no;
	}
	public long getSp_phone_no() {
		return sp_phone_no;
	}
	public void setSp_phone_no(long sp_phone_no) {
		this.sp_phone_no = sp_phone_no;
	}
	public long getRc_phone_no() {
		return rc_phone_no;
	}
	public void setRc_phone_no(long rc_phone_no) {
		this.rc_phone_no = rc_phone_no;
	}
	public int getSerialno() {
		return serialno;
	}
	public void setSerialno(int serialno) {
		this.serialno = serialno;
	}
	public String getComplaint_startno() {
		return complaint_startno;
	}
	public void setComplaint_startno(String complaint_startno) {
		this.complaint_startno = complaint_startno;
	}
	public int getComplaint_endno() {
		return complaint_endno;
	}
	public void setComplaint_endno(int complaint_endno) {
		this.complaint_endno = complaint_endno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getComplaintdate() {
		return complaintdate;
	}
	public void setComplaintdate(String complaintdate) {
		this.complaintdate = complaintdate;
	}
	
	
	
}
