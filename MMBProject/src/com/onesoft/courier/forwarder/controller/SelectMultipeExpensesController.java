package com.onesoft.courier.forwarder.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.forwarder.bean.AddOn_AvailableExpensesTableBean;
import com.onesoft.courier.forwarder.bean.AddOn_SelectedExpensesTableBean;
import com.onesoft.courier.forwarder.bean.MultipleAvailableExpense_TableBean;
import com.onesoft.courier.forwarder.bean.MultipleExpenseBean;
import com.onesoft.courier.forwarder.bean.MultipleSelectedExpense_TableBean;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SelectMultipeExpensesController implements Initializable {
	
	
	
	ObservableList<MultipleAvailableExpense_TableBean> tableDate_AvailableExpensesItems = FXCollections.observableArrayList();
	List <MultipleExpenseBean> list_AvailableExpensesData= new ArrayList<MultipleExpenseBean>();
	List <MultipleExpenseBean> list_AvailableExpenses_GetValues= new ArrayList<MultipleExpenseBean>();
	List <Integer> list_AvailableIndexCount= new ArrayList<>();
	
	
	ObservableList<MultipleSelectedExpense_TableBean> tableDate_SelectedExpensesItems = FXCollections.observableArrayList();
	List <MultipleExpenseBean> list_SelectedExpensesData= new ArrayList<MultipleExpenseBean>();
	List <MultipleExpenseBean> list_SelectedExpenses_GetValues= new ArrayList<MultipleExpenseBean>();
	List <Integer> list_SelectedIndexCount= new ArrayList<>();
	
	
	
	CheckBox checkboxAvailableSelect = new CheckBox();
	
	@FXML
	private Button btnAdd;
	
	@FXML
	private Button btnRemove;
	
	@FXML
	private ImageView imageView_SearchIcon;
	
// ***********************************************************************************************

	@FXML
	private TableView<MultipleSelectedExpense_TableBean> tableView_SelectedMultiple;

	@FXML
	private TableColumn<MultipleSelectedExpense_TableBean, String> tabCol_SelectedUsername;

	@FXML
	private TableColumn<MultipleSelectedExpense_TableBean, String> tabCol_SelectedPassword;
	

// ***********************************************************************************************

	@FXML
	private TableView<MultipleAvailableExpense_TableBean> tableView_AvailableExpenses;

	@FXML
	private TableColumn<MultipleAvailableExpense_TableBean, String> tabCol_AvailableUsername;

	@FXML
	private TableColumn<MultipleAvailableExpense_TableBean, String> tabCol_AvailablePassword;

	@FXML
	private TableColumn<MultipleAvailableExpense_TableBean, Boolean> tabcol_AvailableSelect;

// ***********************************************************************************************	 
	 
	public void loadTable_Available() throws SQLException 
	{
			int slno = 0;

			// dataTable.clear();
			Statement st = null;
			ResultSet rs = null;
			String sql = null;
			DBconnection dbc = new DBconnection();
			Connection con = dbc.ConnectDB();
			System.out.println("Database connected successfully");

			try {
				sql = "select username,password,branchcode from logindetail";
				st = con.createStatement();
				rs = st.executeQuery(sql);

				while (rs.next()) {

					MultipleExpenseBean mubean = new MultipleExpenseBean();

					mubean.setUsername(rs.getString("username"));
					mubean.setPassword(rs.getString("password"));
					mubean.setSelect(false);

					// Population list using CheckBoxBean object
					//list_AvailableExpensesData.add(mubean);
				//	System.out.println("Branch Code: "+ mubean.getBranchCode());

					tableDate_AvailableExpensesItems.add(new MultipleAvailableExpense_TableBean(mubean.getUsername(), mubean.getPassword(),mubean.isSelect()));

				}
				tableView_AvailableExpenses.setItems(tableDate_AvailableExpensesItems);
				clickInboxRow();

			}

			catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			} finally {
				dbc.disconnect(null, st, rs, con);
					//show();
			}
		
	}	
	
// ***********************************************************************************************

	public void selectAllBoxes(ActionEvent e) 
	{
		list_AvailableIndexCount.clear();
		int count = 0;

		for (MultipleAvailableExpense_TableBean tabbean : tableDate_AvailableExpensesItems) 
		{
			if (checkboxAvailableSelect.isSelected() == true) 
			{
				tabbean.setSelect(true);
				list_AvailableIndexCount.add(count);
				count++;

				// graphicD();
				tableView_AvailableExpenses.getSelectionModel().selectAll();
				// guiTable.setEditable(false);
			}
			else 
			{
				list_AvailableIndexCount.clear();
				tableView_AvailableExpenses.getSelectionModel().clearSelection();
				tabbean.setSelect(false);
			}

			tableView_AvailableExpenses.refresh();
		}
	}
	
	public void check()
	{
		Collections.sort(list_AvailableIndexCount);
		
		for(int i=0;i<list_AvailableIndexCount.size();i++)
		{
			System.out.println("Count list index: "+list_AvailableIndexCount.get(i).shortValue());
		}
	}
	
	
	public void showIndex()
	{
		//check();
		Collections.sort(list_AvailableIndexCount);
		System.out.println("Index method running...");
		
		int check=1;
		
		for(int i=0; i<tableDate_AvailableExpensesItems.size();i++)
		{
			System.out.println(">>> loop running.. <<<");
			
			if(i==0)
			{
				System.out.println("Checked Index: from if : "+list_AvailableIndexCount.get(i));
				//System.out.println("index from if: >>> "+ i);
				//System.out.println("Deleted Item >>> "+tableDate_AvailableExpensesItems.get(list_AvailableIndexCount.get(i)).getUsername()+" | "+tableDate_AvailableExpensesItems.get(list_AvailableIndexCount.get(i)).getUsername());
				tableDate_AvailableExpensesItems.remove(list_AvailableIndexCount.get(i));
				//System.out.println("Table size from if: >>> " +tableDate_AvailableExpensesItems.size());
			}
			else
			{
				if(i<list_AvailableIndexCount.size())
				{
					//System.out.println("index from else if: ========== "+ i);
					//System.out.println("Deleted Item from else if: ========== "+tableDate_AvailableExpensesItems.get(list_AvailableIndexCount.get(i)).getUsername()+" | "+tableDate_AvailableExpensesItems.get(list_AvailableIndexCount.get(i)).getUsername());
					System.out.println("Checked Index: from else : "+(list_AvailableIndexCount.get(i)));
				tableDate_AvailableExpensesItems.remove(list_AvailableIndexCount.get(i)-check);
				//System.out.println("Table size from else if: ========== " +tableDate_AvailableExpensesItems.size());
				check++;
				}
			}
			
		}
	}


// ***********************************************************************************************
	
	
	public void clickInboxRow() throws Exception 
	{
		tableView_AvailableExpenses.setRowFactory(tv -> {
			TableRow<MultipleAvailableExpense_TableBean> row = new TableRow<>();
			// System.out.println("=====print repeat Data===");
			row.setOnMouseClicked(event -> {
				MultipleAvailableExpense_TableBean rowData = row.getItem();

				try 
				{
					if (event.getClickCount() == 1 && (!row.isEmpty())) 
					{
						tableDate_AvailableExpensesItems.remove(row.getIndex());
						//tableView_AvailableExpenses.getSelectionModel().focus(row.getIndex()+1);
					}

				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}

			});
			return row;
		});
	}
	
	
	// ***********************************************************************************************
	
	public void dynamicCheckBoxInAvailableTable()
	 {
		// Dynamic check box 
			tabcol_AvailableSelect.setCellFactory(col -> {
	            CheckBoxTableCell<MultipleAvailableExpense_TableBean, Boolean> callback = new CheckBoxTableCell<>(index -> {
	            	 BooleanProperty select = new SimpleBooleanProperty(tableView_AvailableExpenses.getItems().get(index).isSelect()); 
	                select.addListener((obs, wasSelect, isSelect) -> {
	                	MultipleAvailableExpense_TableBean sel = tableView_AvailableExpenses.getItems().get(index);
	                    sel.setSelect(isSelect);
	                    
	                     tableView_AvailableExpenses.getSelectionModel().select(index);
	                     
	                     if(isSelect == true) 
		                    {
	                    	 MultipleAvailableExpense_TableBean selectedItem = tableView_AvailableExpenses.getSelectionModel().getSelectedItem();
	                    	 list_AvailableIndexCount.add(index);
	                    	 
		                    }
		                    else if(isSelect == false) 
		                    {
		                    	checkboxAvailableSelect.setSelected(false);
		                    	list_AvailableIndexCount.remove(index);
		                    }
		                    else
		                    {
		                    	tableView_AvailableExpenses.getSelectionModel().clearSelection(index);
		                    }
	                    
	                      });
	                
	                return select ;
	            });
	            return callback ;
	        });
	 }
	
	
// ***********************************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imageView_SearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		tabCol_AvailableUsername.setCellValueFactory(new PropertyValueFactory<MultipleAvailableExpense_TableBean, String>("username"));
		tabCol_AvailablePassword.setCellValueFactory(new PropertyValueFactory<MultipleAvailableExpense_TableBean, String>("password"));
	
		tabCol_SelectedUsername.setCellValueFactory(new PropertyValueFactory<MultipleSelectedExpense_TableBean, String>("username"));
		tabCol_SelectedPassword.setCellValueFactory(new PropertyValueFactory<MultipleSelectedExpense_TableBean, String>("password"));
		
		dynamicCheckBoxInAvailableTable();
		
		checkboxAvailableSelect.setOnAction(e -> selectAllBoxes(e));
		
		//Create CheckBox at Column
		tabcol_AvailableSelect.setGraphic(checkboxAvailableSelect);
		
		tableView_AvailableExpenses.setEditable(true);
		
		try {
			loadTable_Available();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} 

}
