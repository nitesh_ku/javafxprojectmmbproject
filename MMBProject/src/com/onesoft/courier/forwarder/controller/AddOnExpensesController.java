package com.onesoft.courier.forwarder.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.forwarder.bean.AddOnExpensesBean;
import com.onesoft.courier.forwarder.bean.AddOn_AvailableExpensesTableBean;
import com.onesoft.courier.forwarder.bean.AddOn_SelectedExpensesTableBean;
import com.onesoft.courier.forwarder.bean.MultipleAvailableExpense_TableBean;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AddOnExpensesController implements Initializable {

	
	
	ObservableList<AddOn_AvailableExpensesTableBean> tableDate_AvailableExpensesItems = FXCollections.observableArrayList();
	ObservableList<AddOn_AvailableExpensesTableBean> sorted_AvailableExpensesItems = FXCollections.observableArrayList();
	List <AddOnExpensesBean> list_AvailableExpensesData= new ArrayList<AddOnExpensesBean>();
	List <AddOnExpensesBean> list_AvailableExpenses_GetValues= new ArrayList<AddOnExpensesBean>();
	List <Integer> list_AvailableIndexCount= new ArrayList<>();
	
	
	ObservableList<AddOn_SelectedExpensesTableBean> tableDate_SelectedExpensesItems = FXCollections.observableArrayList();
	List <AddOnExpensesBean> list_SelectedExpensesData= new ArrayList<AddOnExpensesBean>();
	List <AddOnExpensesBean> list_SelectedExpenses_GetValues= new ArrayList<AddOnExpensesBean>();
	List <Integer> list_SelectedIndexCount= new ArrayList<>();
	
	CheckBox checkboxAvailableSelect = new CheckBox();
	CheckBox checkBoxSelectedTable=new CheckBox();
	
	List <Integer> removeIndex_FromAvailable=new ArrayList<>(); 
	
	@FXML
	private Button btnAddExpenses;
	
	@FXML
	private Button btnRemoveExpenses;
	
	@FXML
	private ImageView imgViewSearchIcon;
	
	@FXML
	private TextField txtSearch;
	
// ***********************************************************************************************
	
	@FXML
	private TableView<AddOn_SelectedExpensesTableBean> tableView_SelectedExpenses;
	

	@FXML
	private TableColumn<AddOn_SelectedExpensesTableBean, String> tabCol_SelectedExpenseCode;
	
	@FXML
	private TableColumn<AddOn_SelectedExpensesTableBean,Double> tabCol_SelectedExpenseDefaultAmount;
	
	@FXML
	private TableColumn<AddOn_SelectedExpensesTableBean,Boolean> tabcol_SelectedSelect;
	
	
// ***********************************************************************************************	
	
	@FXML
	private TableView<AddOn_AvailableExpensesTableBean> tableView_AvailableExpenses;
	
	
	@FXML
	private TableColumn<AddOn_AvailableExpensesTableBean, String> tabCol_AvailableExpenseName;
	
	@FXML
	private TableColumn<AddOn_AvailableExpensesTableBean, String> tabCol_AvailableExpenseCode;
	
	@FXML
	private TableColumn<AddOn_AvailableExpensesTableBean,Double> tabCol_AvailableExpenseDefaultAmount;
	
	@FXML
	private TableColumn<AddOn_AvailableExpensesTableBean,Boolean> tabcol_AvailableSelect;
	 
	 
// ***********************************************************************************************	 
	 
	 public void loadTable_AvailableExpenses() throws SQLException
	 {
		 if(ForwarderDetailsController.LIST_SELECTEDITEMS.size()!=0)
			{
				for (AddOnExpensesBean addOnExpensesBean : 	ForwarderDetailsController.LIST_AVAILABLEITEMS) 
				{
					tableDate_AvailableExpensesItems.add(new AddOn_AvailableExpensesTableBean(addOnExpensesBean.getExpenseName(),addOnExpensesBean.getExpenseCode(),
							addOnExpensesBean.getExpenseDefaultAmount()));
					tableView_AvailableExpenses.setItems(tableDate_AvailableExpensesItems);
				}
			}
		 
		 else
		 {
		 int slno=0;
			 
		 	 //dataTable.clear();
			 Statement st=null;
			 ResultSet rs=null;
			 String sql=null;
			 DBconnection dbc = new DBconnection();
			 Connection con = dbc.ConnectDB();
			 System.out.println("Database connected successfully");
			 
			 try{
				 sql="select code,name,rate from expensestype";
				 st = con.createStatement();
				 rs = st.executeQuery(sql);
				 
				 while(rs.next()) {
					 
					 AddOnExpensesBean cbbean=new AddOnExpensesBean();
					 
					 cbbean.setExpenseName(rs.getString("name"));
					 cbbean.setExpenseCode(rs.getString("code"));
					 cbbean.setExpenseDefaultAmount(rs.getDouble("rate"));
					 cbbean.setSelect(false);
					 
					 // Population list using CheckBoxBean object
					 list_AvailableExpensesData.add(cbbean);
					 
					 tableDate_AvailableExpensesItems.add(new AddOn_AvailableExpensesTableBean( cbbean.getExpenseName(), cbbean.getExpenseCode(),
							 		cbbean.getExpenseDefaultAmount()));
					
				 }
				 tableView_AvailableExpenses.setItems(tableDate_AvailableExpensesItems);
				 
				 clickAvailableRow();
				 //clickSelectedRow();
			 }
			 
			 catch(Exception e) {
				 System.out.println(e);
				 e.printStackTrace();
			 }
			 finally{
				dbc.disconnect(null, st, rs, con);
				 //System.out.println("Connection closed");
			 }
		 }
		
		
	 }
	 

// ***********************************************************************************************	 

	 public void clickSelectedRow() throws Exception {
			tableView_SelectedExpenses.setRowFactory(tv -> {
				TableRow<AddOn_SelectedExpensesTableBean> row = new TableRow<>();
				// System.out.println("=====print repeat Data===");
				row.setOnMouseClicked(event -> {
					AddOn_SelectedExpensesTableBean rowData = row.getItem();

					try {
						if (event.getClickCount() == 1 && (!row.isEmpty())) {
							tableDate_AvailableExpensesItems.add(new AddOn_AvailableExpensesTableBean(row.getItem().getExpenseName(),row.getItem().getExpenseCode(),row.getItem().getExpenseDefaultAmount()));
							tableView_AvailableExpenses.setItems(tableDate_AvailableExpensesItems);
							tableView_SelectedExpenses.getSelectionModel().clearSelection();
							
							 ForwarderDetailsController.LIST_AVAILABLEITEMS.clear();
							
						  	 for(AddOn_AvailableExpensesTableBean beanTableData: tableDate_AvailableExpensesItems)
	                    	 {
	                    		 AddOnExpensesBean addOnExpensesBean=new AddOnExpensesBean();
	                    		 addOnExpensesBean.setExpenseName(beanTableData.getExpenseName());
	                    		 addOnExpensesBean.setExpenseCode(beanTableData.getExpenseCode());
	                    		 addOnExpensesBean.setExpenseDefaultAmount(beanTableData.getExpenseDefaultAmount());
	     					
	     						ForwarderDetailsController.LIST_AVAILABLEITEMS.add(addOnExpensesBean);
	                    	 }
							
	                    	 tableDate_SelectedExpensesItems.remove(row.getIndex());
	                    	 
	                    	
	                    	 
	                  
	                    	 ForwarderDetailsController.LIST_SELECTEDITEMS.clear();
	            		 	 
	            		 	 for(AddOn_SelectedExpensesTableBean beanTableData: tableDate_SelectedExpensesItems)
	            		 	 {
	            		 		AddOnExpensesBean beanForSelected=new AddOnExpensesBean();
	            		 		beanForSelected.setExpenseName(beanTableData.getExpenseName());
	            		 		beanForSelected.setExpenseCode(beanTableData.getExpenseCode());
	            		 		beanForSelected.setExpenseDefaultAmount(beanTableData.getExpenseDefaultAmount());
	    					
	    						ForwarderDetailsController.LIST_SELECTEDITEMS.add(beanForSelected);
	            		 	 }
	            		  
							// tableView_AvailableExpenses.getSelectionModel().focus(row.getIndex()+1);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				});
				return row;
			});
		}
	 
// ***********************************************************************************************		 
	 
	public void clickAvailableRow() throws Exception {
		tableView_AvailableExpenses.setRowFactory(tv -> {
			TableRow<AddOn_AvailableExpensesTableBean> row = new TableRow<>();
			// System.out.println("=====print repeat Data===");
			row.setOnMouseClicked(event -> {
				AddOn_AvailableExpensesTableBean rowData = row.getItem();

				try {
					if (event.getClickCount() == 1 && (!row.isEmpty())) {
						
						tableDate_SelectedExpensesItems.add(new AddOn_SelectedExpensesTableBean(row.getItem().getExpenseName(),row.getItem().getExpenseCode(),row.getItem().getExpenseDefaultAmount(),false));
            		 	tableView_SelectedExpenses.setItems(tableDate_SelectedExpensesItems);
            		 	 
            		 	 ForwarderDetailsController.LIST_SELECTEDITEMS.clear();
            		 	 for(AddOn_SelectedExpensesTableBean beanTableData: tableDate_SelectedExpensesItems)
            		 	 {
            		 		AddOnExpensesBean beanForSelected=new AddOnExpensesBean();
            		 		beanForSelected.setExpenseName(beanTableData.getExpenseName());
            		 		beanForSelected.setExpenseCode(beanTableData.getExpenseCode());
            		 		beanForSelected.setExpenseDefaultAmount(beanTableData.getExpenseDefaultAmount());
    					
    						ForwarderDetailsController.LIST_SELECTEDITEMS.add(beanForSelected);
            		 	 }
            		 	 
            		 	tableDate_AvailableExpensesItems.remove(row.getIndex());
                    	 
                    	 ForwarderDetailsController.LIST_AVAILABLEITEMS.clear();
                    	 for(AddOn_AvailableExpensesTableBean beanTableData: tableDate_AvailableExpensesItems)
                    	 {
                    		 AddOnExpensesBean addOnExpensesBean=new AddOnExpensesBean();
                    		 addOnExpensesBean.setExpenseName(beanTableData.getExpenseName());
                    		 addOnExpensesBean.setExpenseCode(beanTableData.getExpenseCode());
                    		 addOnExpensesBean.setExpenseDefaultAmount(beanTableData.getExpenseDefaultAmount());
     					
     						ForwarderDetailsController.LIST_AVAILABLEITEMS.add(addOnExpensesBean);
                    	 }
                    	 
                    	 tableView_AvailableExpenses.getSelectionModel().clearSelection();
                    	 clickSelectedRow();
						// tableView_AvailableExpenses.getSelectionModel().focus(row.getIndex()+1);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			});
			return row;
		});
	}

	
	public void loadSelectedTable()
	{
		if(ForwarderDetailsController.LIST_SELECTEDITEMS.size()!=0)
		{
			for (AddOnExpensesBean addOnExpensesBean : 	ForwarderDetailsController.LIST_SELECTEDITEMS) 
			{
				tableDate_SelectedExpensesItems.add(new AddOn_SelectedExpensesTableBean(addOnExpensesBean.getExpenseName(),addOnExpensesBean.getExpenseCode(),addOnExpensesBean.getExpenseDefaultAmount(),false));
				tableView_SelectedExpenses.setItems(tableDate_SelectedExpensesItems);
			}
		}
	}
	
// ***********************************************************************************************	 
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imgViewSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		
		
	//	tabCol_AvailableExpenseSlno.setCellValueFactory(new PropertyValueFactory<AddOn_AvailableExpensesTableBean, Integer>("slno"));
		tabCol_AvailableExpenseName.setCellValueFactory(new PropertyValueFactory<AddOn_AvailableExpensesTableBean, String>("expenseName"));
		tabCol_AvailableExpenseCode.setCellValueFactory(new PropertyValueFactory<AddOn_AvailableExpensesTableBean, String>("expenseCode"));
		tabCol_AvailableExpenseDefaultAmount.setCellValueFactory(new PropertyValueFactory<AddOn_AvailableExpensesTableBean, Double>("expenseDefaultAmount"));
		
		
		//tabCol_SelectedSlno.setCellValueFactory(new PropertyValueFactory<AddOn_SelectedExpensesTableBean, Integer>("slno"));
		tabCol_SelectedExpenseCode.setCellValueFactory(new PropertyValueFactory<AddOn_SelectedExpensesTableBean, String>("expenseCode"));
		tabCol_SelectedExpenseDefaultAmount.setCellValueFactory(new PropertyValueFactory<AddOn_SelectedExpensesTableBean, Double>("expenseDefaultAmount"));

		

		
		try {
			clickAvailableRow();
			clickSelectedRow();
			
			loadSelectedTable();
			loadTable_AvailableExpenses();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
