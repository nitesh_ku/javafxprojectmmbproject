package com.onesoft.courier.forwarder.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.TextFields;
import org.omg.stub.java.rmi._Remote_Stub;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.forwarder.bean.AddOnExpensesBean;
import com.onesoft.courier.forwarder.bean.AddOn_AvailableExpensesTableBean;
import com.onesoft.courier.forwarder.bean.AddOn_SelectedExpensesTableBean;
import com.onesoft.courier.forwarder.bean.ForwarderBean;
import com.onesoft.courier.forwarder.bean.ForwarderTableBean;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class ForwarderDetailsController implements Initializable{
	
	public static List<AddOnExpensesBean> LIST_SELECTEDITEMS=new ArrayList<>();
	public static List<AddOnExpensesBean> LIST_AVAILABLEITEMS=new ArrayList<>();
	
	private ObservableList<ForwarderTableBean> tabledata_ForwarderDetails=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxCityItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxStateItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxCountryItems=FXCollections.observableArrayList("IN");
	private ObservableList<String> comboBoxStatusItems=FXCollections.observableArrayList("Active","In-Active");
	
	
	private List<String> list_City=new ArrayList<>();
	private Set<String> list_State=new HashSet<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	/*@FXML
	private CheckBox checkBoxAddOnExpenses;*/
	
	@FXML
	private Button btnMultipleSelection;
	
	@FXML
	private TextField txtForwarderCode;
	
	@FXML
	private TextField txtName;
	
	@FXML
	private TextField txtContactPerson;
	
	@FXML
	private TextField txtPhone;
	
	@FXML
	private TextField txtWebsite;
	
	@FXML
	private TextField txtEmail;
	
	@FXML
	private TextField txtAddress;

	@FXML
	private TextField txtCity;
	
// =================================================	
	
	@FXML
	private ComboBox<String> comboBoxCity;
	
	@FXML
	private ComboBox<String> comboBoxState;
	
	@FXML
	private ComboBox<String> comboBoxCountry;

	@FXML
	private ComboBox<String> comboBoxStatus;
	
// =================================================
	
	@FXML
	private Button btnSubmit;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnUpdate;
	
	@FXML
	private Button btnExportToExcel;

// =================================================	
	
	@FXML
	private Pagination pagination_ForwarderDetails;
	
// =================================================	

	@FXML
	private ImageView imgViewSearchIcon_forwarder;
	
// =================================================	
	
	@FXML
	private TableView<ForwarderTableBean> tableForwarder;
	
	@FXML
	private TableColumn<ForwarderTableBean, Integer> tabColumn_slno;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_ForwarderCode;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Name;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_ContactPerson;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Phone;

	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Address;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_City;
		
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Website;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Email;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Status;
	
	@FXML
	private TableColumn<ForwarderTableBean, String> tabColumn_Indate;

	
// ******************************************************************************

	public void loadCity() throws SQLException
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME)
		{
			list_City.add(bean.getCityName());
		}
		TextFields.bindAutoCompletion(txtCity, list_City);
		
	}
	
// ******************************************************************************
	
	public void loadState() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_State.add(bean.getStateCode());
			
		}
		
		for(String state:list_State)
		{
			comboBoxStateItems.add(state);
		}

		comboBoxState.setItems(comboBoxStateItems);
	}
	
// ******************************************************************************

	public void autoFillStateViaCity() throws SQLException 
	{
		if(txtCity.getText()== null || txtCity.getText().isEmpty())
		{
			
		}
		else if(!txtCity.getText().equals("") || !txtCity.getText().isEmpty())
		{
			for(LoadCityBean cityBean:LoadCity.SET_LOAD_CITYWITHNAME)
			{
				if(txtCity.getText().equals(cityBean.getCityName()))
				{
					comboBoxState.setValue(cityBean.getStateCode());
					comboBoxCountry.setValue("IN");
					break;
				}
			}
		}
		else
		{
			comboBoxState.getSelectionModel().clearSelection();
			comboBoxCountry.getSelectionModel().clearSelection();
		}
	}
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		if (n.getId().equals("txtForwarderCode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtName.requestFocus();
			}
		}

		else if (n.getId().equals("txtName")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtContactPerson.requestFocus();
			}
		}

		else if (n.getId().equals("txtContactPerson")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtPhone.requestFocus();
			}
		}

		else if (n.getId().equals("txtPhone")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtWebsite.requestFocus();
			}
		}

		else if (n.getId().equals("txtWebsite")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtEmail.requestFocus();
			}
		}

		else if (n.getId().equals("txtEmail")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAddress.requestFocus();
			}
		}

		/*else if (n.getId().equals("txtAddress")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxCity.requestFocus();
			}
		}*/
		
		else if (n.getId().equals("txtAddress")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtCity.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtCity")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxState.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxState")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxCountry.requestFocus();
			}
		}
		
		else if (n.getId().equals("comboBoxCountry")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxStatus.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxStatus")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(btnSubmit.isDisable()==true)
				{
					btnUpdate.requestFocus();
				}
				else
				{
					btnSubmit.requestFocus();	
				}
			}
		}
		
		else if (n.getId().equals("btnSubmit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
		
		else if (n.getId().equals("btnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}
	}
	
	
	
// *************** Method to set Validation *******************

	public void setValidation() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		
		LIST_AVAILABLEITEMS.clear();
		LIST_SELECTEDITEMS.clear();
		
		if (txtForwarderCode.getText() == null || txtForwarderCode.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Forwarder Code");
			alert.showAndWait();
			txtForwarderCode.requestFocus();
		} 
		
		else if (txtName.getText() == null || txtName.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtName.requestFocus();
		}
		
		else if(!txtPhone.getText().matches("[0-9]*"))
		{
	    	alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only Numbers");
			alert.showAndWait();
			txtPhone.requestFocus();
		}
		
		else {
			
			if(btnSubmit.isDisable()==true)
			{
				updateForwarderDetails();	
			}
			else
			{
				saveForwarderDetails();
			}
			
		}
	}	
	

// *************************************************************************

	public void saveForwarderDetails() throws SQLException {
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		ForwarderBean forBean=new ForwarderBean();

		forBean.setForwarderCode(txtForwarderCode.getText());
		forBean.setName(txtName.getText());
		forBean.setContactPerson(txtContactPerson.getText());
		
		System.out.println("Phone number: "+txtPhone.getText());
		
		if(txtPhone.getText().equals(""))
		{
			forBean.setPhone("0");
		}
		else
		{
			forBean.setPhone(txtPhone.getText());
		}
		
		forBean.setWebsite(txtWebsite.getText());
		forBean.setEmail(txtEmail.getText());
		forBean.setAddress(txtAddress.getText());
		forBean.setCity(txtCity.getText());
		forBean.setState(comboBoxState.getValue());
		forBean.setCountry(comboBoxCountry.getValue());
		forBean.setStatus(comboBoxStatus.getValue());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try {
			
			System.out.println("phone value From try: "+forBean.getPhone());
			String query = "insert into forwarder_details(forwardercode,name,contactperson,phone,website,email,address,city,state,country,"
					+ "status,indate,create_date,lastmodifieddate) values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, forBean.getForwarderCode());
			preparedStmt.setString(2, forBean.getName());
			preparedStmt.setString(3, forBean.getContactPerson());
			preparedStmt.setLong(4, Long.valueOf(forBean.getPhone().toString()));
			preparedStmt.setString(5, forBean.getWebsite());
			preparedStmt.setString(6, forBean.getEmail());
			preparedStmt.setString(7, forBean.getAddress());
			preparedStmt.setString(8, forBean.getCity());
			preparedStmt.setString(9, forBean.getState());
			preparedStmt.setString(10, forBean.getCountry());
			preparedStmt.setString(11, forBean.getStatus());
			

			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				alert.setContentText("Forwarder details successfully saved...");
				alert.showAndWait();
			} else {
				alert.setContentText("Forwarder details is not saved \nPlease try again...!");
				alert.showAndWait();
			}
			
			txtForwarderCode.requestFocus();
			reset();
			loadForwarderTable();

			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}	
	
	
// *************************************************************************

	public void updateForwarderDetails() throws SQLException 
	{

		
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		ForwarderBean forBean = new ForwarderBean();

		forBean.setForwarderCode(txtForwarderCode.getText());
		forBean.setName(txtName.getText());
		forBean.setContactPerson(txtContactPerson.getText());
		forBean.setPhone(txtPhone.getText());
		forBean.setWebsite(txtWebsite.getText());
		forBean.setEmail(txtEmail.getText());
		forBean.setAddress(txtAddress.getText());
		forBean.setCity(txtCity.getText());
		forBean.setState(comboBoxState.getValue());
		forBean.setCountry(comboBoxCountry.getValue());
		forBean.setStatus(comboBoxStatus.getValue());

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = "update forwarder_details SET name=?,contactperson=?,phone=?,website=?,email=?,address=?,city=?,state=?,country=?,"
					+ "status=?,lastmodifieddate=CURRENT_TIMESTAMP where forwardercode=?";
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, forBean.getName());
			preparedStmt.setString(2, forBean.getContactPerson());
			preparedStmt.setLong(3, Long.valueOf(forBean.getPhone()));
			preparedStmt.setString(4, forBean.getWebsite());
			preparedStmt.setString(5, forBean.getEmail());
			preparedStmt.setString(6, forBean.getAddress());
			preparedStmt.setString(7, forBean.getCity());
			preparedStmt.setString(8, forBean.getState());
			preparedStmt.setString(9, forBean.getCountry());
			preparedStmt.setString(10, forBean.getStatus());
			preparedStmt.setString(11, forBean.getForwarderCode());

			int status = preparedStmt.executeUpdate();

			if (status == 1) 
			{
				alert.setContentText("Record Updated successfully...");
				alert.showAndWait();
			} 
			else
			{
				alert.setContentText("Something went wrong...! \n please try again");
				alert.showAndWait();
			}

			//btnSubmit.setDisable(false);
			txtForwarderCode.requestFocus();
			reset();
			loadForwarderTable();

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}


	
// *************************************************************************

	public void loadForwarderDetailsViaForwarderCode() throws SQLException 
	{

		
		//btnSubmit.setDisable(true);
		//tabledata_ForwarderDetails.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt = null;

		ForwarderBean forBean = new ForwarderBean();
		forBean.setForwarderCode(txtForwarderCode.getText());

		int slno = 1;
		try {

			sql = "select forwardercode,name,contactperson,phone,website,email,address,city,status,state,country from forwarder_details where forwardercode=?";
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, forBean.getForwarderCode());
			
			rs = preparedStmt.executeQuery();
		
		
			
			if(!rs.next()==true)
			{
				btnSubmit.setDisable(false);
				btnUpdate.setDisable(true);
			}
			else
			{
				do{
					txtForwarderCode.setEditable(false);
					btnSubmit.setDisable(true);
					btnUpdate.setDisable(false);
					
					forBean.setSlno(slno);
					forBean.setForwarderCode(rs.getString("forwardercode"));
					forBean.setName(rs.getString("name"));
					forBean.setContactPerson(rs.getString("contactperson"));
					forBean.setPhone(rs.getString("phone"));
					forBean.setAddress(rs.getString("address"));
					forBean.setWebsite(rs.getString("website"));
					forBean.setEmail(rs.getString("email"));
					forBean.setCity(rs.getString("city"));
					forBean.setStatus(rs.getString("status"));
					forBean.setState(rs.getString("state"));
					forBean.setCountry(rs.getString("country"));
				}while (rs.next());
			}
			
			
			txtName.setText(forBean.getName());
			txtContactPerson.setText(forBean.getContactPerson());
			txtPhone.setText(forBean.getPhone());
			txtWebsite.setText(forBean.getWebsite());
			txtEmail.setText(forBean.getEmail());
			txtCity.setText(forBean.getCity());
			txtAddress.setText(forBean.getAddress());
			comboBoxState.setValue(forBean.getState());
			comboBoxCountry.setValue(forBean.getCountry());
			comboBoxStatus.setValue(forBean.getStatus());
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
		}

		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	
	
	
// *************************************************************************

	public void loadForwarderTable() throws SQLException 
	{

		tabledata_ForwarderDetails.clear();
		
		//tableForwarder.refresh();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		ForwarderBean forBean=new ForwarderBean();

		int slno = 1;
		try {

			stmt = con.createStatement();
			sql = "select forwardercode,name,contactperson,phone,website,email,address,city,status,indate from forwarder_details order by slno DESC";
		
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				forBean.setSlno(slno);
				forBean.setForwarderCode(rs.getString("forwardercode"));
				forBean.setName(rs.getString("name"));
				forBean.setContactPerson(rs.getString("contactperson"));
				forBean.setPhone(rs.getString("phone"));
				forBean.setAddress(rs.getString("address"));
				forBean.setWebsite(rs.getString("website"));
				forBean.setEmail(rs.getString("email"));
				forBean.setCity(rs.getString("city"));
				forBean.setStatus(rs.getString("status"));
				forBean.setIndate(date.format(rs.getDate("indate")));
			
				//System.out.println(forBean.getForwarderCode());
				
				tabledata_ForwarderDetails.add(new ForwarderTableBean(forBean.getSlno(), forBean.getForwarderCode(), forBean.getName(),
						forBean.getContactPerson(), forBean.getPhone(), forBean.getWebsite(), forBean.getEmail(), forBean.getAddress(),forBean.getCity(), forBean.getStatus(), forBean.getIndate()));

				slno++;
			}

			tableForwarder.setItems(tabledata_ForwarderDetails);

			pagination_ForwarderDetails.setPageCount((tabledata_ForwarderDetails.size() / rowsPerPage() + 1));
			pagination_ForwarderDetails.setCurrentPageIndex(0);
			pagination_ForwarderDetails.setPageFactory((Integer pageIndex) -> createPage_ForwarderDetails(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}

	
// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 20;
	}

	public GridPane createPage_ForwarderDetails(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_ForwarderDetails.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_ForwarderDetails.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableForwarder.setItems(FXCollections.observableArrayList(tabledata_ForwarderDetails
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableForwarder.setItems(FXCollections.observableArrayList(tabledata_ForwarderDetails
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}
	
	
// *************************************************************************
	
	public void reset()
	{
		btnSubmit.setDisable(false);
		btnUpdate.setDisable(true);
		txtForwarderCode.setEditable(true);
		txtForwarderCode.clear();
		txtName.clear();
		txtAddress.clear();
		txtCity.clear();
		txtContactPerson.clear();
		txtEmail.clear();
		txtWebsite.clear();
		txtPhone.clear();
		comboBoxCountry.getSelectionModel().clearSelection();
		comboBoxState.getSelectionModel().clearSelection();
		comboBoxStatus.getSelectionModel().clearSelection();
		txtForwarderCode.requestFocus();
	}

	
// *************************************************************************
	
	public void loadMultipleSelectionForm() throws IOException
	{
			Main m=new Main();
			m.showMultipleSelection();
	}	
	
// *************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		LIST_SELECTEDITEMS.clear();
		
		btnUpdate.setDisable(true);
		
		imgViewSearchIcon_forwarder.setImage(new Image("/icon/searchicon_blue.png"));
		
		comboBoxCountry.setItems(comboBoxCountryItems);
		comboBoxStatus.setItems(comboBoxStatusItems);
		
		tabColumn_slno.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,Integer>("slno"));
		tabColumn_ForwarderCode.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("forwarderCode"));
		tabColumn_Name.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("name"));
		tabColumn_ContactPerson.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("contactPerson"));
		tabColumn_Phone.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("phone"));
		tabColumn_Address.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("address"));
		tabColumn_City.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("city"));
		tabColumn_Website.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("website"));
		tabColumn_Email.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("email"));
		tabColumn_Status.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("status"));
		tabColumn_Indate.setCellValueFactory(new PropertyValueFactory<ForwarderTableBean,String>("indate"));
	
		try 
		{
			loadCity();
			loadState();
			loadForwarderTable();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		
	}



}
