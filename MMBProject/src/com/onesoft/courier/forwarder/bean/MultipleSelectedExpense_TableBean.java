package com.onesoft.courier.forwarder.bean;

import javafx.beans.property.SimpleStringProperty;

public class MultipleSelectedExpense_TableBean {

	private SimpleStringProperty username;
	private SimpleStringProperty password;
	
	
	
	public MultipleSelectedExpense_TableBean(String username, String password)
	{
		this.username=new SimpleStringProperty(username);
		this.password=new SimpleStringProperty(password);
	
		
	}
	
	
	public String getUsername() {
		return username.get();
	}
	public void setUsername(SimpleStringProperty username) {
		this.username = username;
	}
	public String getPassword() {
		return password.get();
	}
	public void setPassword(SimpleStringProperty password) {
		this.password = password;
	}
	
	
}
