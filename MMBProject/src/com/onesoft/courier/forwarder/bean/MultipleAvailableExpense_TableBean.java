package com.onesoft.courier.forwarder.bean;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class MultipleAvailableExpense_TableBean {

	private SimpleStringProperty username;
	private SimpleStringProperty password;
	
	private SimpleBooleanProperty select;
	
	public MultipleAvailableExpense_TableBean(String username, String password, boolean select)
	{
		this.username=new SimpleStringProperty(username);
		this.password=new SimpleStringProperty(password);
		
		
		this.select=new SimpleBooleanProperty(select);
	}
	
	public String getUsername() {
		return username.get();
	}
	public void setUsername(SimpleStringProperty username) {
		this.username = username;
	}
	public String getPassword() {
		return password.get();
	}
	public void setPassword(SimpleStringProperty password) {
		this.password = password;
	}

	
	public boolean isSelect() {
		return select.get();
	}
	public SimpleBooleanProperty setSelectProperty(boolean b) {
	        return select;
	}
	
	public void setSelect(Boolean new_val) {
		this.select.set(new_val);
		
	}
	
	
	
}
