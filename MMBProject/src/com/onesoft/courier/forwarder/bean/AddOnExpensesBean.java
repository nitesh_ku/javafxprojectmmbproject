package com.onesoft.courier.forwarder.bean;

public class AddOnExpensesBean {
	
	
	private String expenseName;
	private String expenseCode;
	private double expenseDefaultAmount;
	
	private boolean select;

	public String getExpenseName() {
		return expenseName;
	}

	public void setExpenseName(String expenseName) {
		this.expenseName = expenseName;
	}

	public String getExpenseCode() {
		return expenseCode;
	}

	public void setExpenseCode(String expenseCode) {
		this.expenseCode = expenseCode;
	}

	public double getExpenseDefaultAmount() {
		return expenseDefaultAmount;
	}

	public void setExpenseDefaultAmount(double expenseDefaultAmount) {
		this.expenseDefaultAmount = expenseDefaultAmount;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}
	
	

}
