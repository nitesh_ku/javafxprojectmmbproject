package com.onesoft.courier.forwarder.bean;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AddOn_SelectedExpensesTableBean {
	
	//private SimpleIntegerProperty slno;
	private SimpleStringProperty expenseName;
	private SimpleStringProperty expenseCode;
	private SimpleDoubleProperty expenseDefaultAmount;
	
	private SimpleBooleanProperty select;
	
	
	public AddOn_SelectedExpensesTableBean(String expenseName,String expenseCode, double defaultAmount, boolean select){
		//this.expenseName = new SimpleStringProperty(expenseName);
		//this.slno=new SimpleIntegerProperty(slno);
		this.expenseName = new SimpleStringProperty(expenseName);
		this.expenseCode = new SimpleStringProperty(expenseCode);
		this.expenseDefaultAmount = new SimpleDoubleProperty(defaultAmount);
		this.select=new SimpleBooleanProperty(select);	
	}

	public String getExpenseName() {
		return expenseName.get();
	}

	public void setExpenseName(SimpleStringProperty expenseName) {
		this.expenseName = expenseName;
	}

	public String getExpenseCode() {
		return expenseCode.get();
	}

	public void setExpenseCode(SimpleStringProperty expenseCode) {
		this.expenseCode = expenseCode;
	}

	public Double getExpenseDefaultAmount() {
		return expenseDefaultAmount.get();
	}

	public void setExpenseDefaultAmount(SimpleDoubleProperty expenseDefaultAmount) {
		this.expenseDefaultAmount = expenseDefaultAmount;
	}

	
	public AddOn_SelectedExpensesTableBean(boolean select)
	{
		
		this.select=new SimpleBooleanProperty(select);	
	}
	
	
	public boolean isSelect() {
		return select.get();
	}
	public SimpleBooleanProperty setSelectProperty(boolean b) {
	        return select;
	}
	
	public void setSelect(Boolean new_val) {
		this.select.set(new_val);
		
	}

/*	public Integer getSlno() {
		return slno.get();
	}

	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}*/
	
	

}
