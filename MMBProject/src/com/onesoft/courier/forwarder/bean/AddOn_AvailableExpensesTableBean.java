package com.onesoft.courier.forwarder.bean;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AddOn_AvailableExpensesTableBean {
	
	
	private SimpleStringProperty expenseName;
	private SimpleStringProperty expenseCode;
	private SimpleDoubleProperty expenseDefaultAmount;
	
	
	
	
	public AddOn_AvailableExpensesTableBean(String expenseName, String expenseCode, double defaultAmount){
		
		this.expenseName = new SimpleStringProperty(expenseName);
		this.expenseCode = new SimpleStringProperty(expenseCode);
		this.expenseDefaultAmount = new SimpleDoubleProperty(defaultAmount);
			
	}

	public String getExpenseName() {
		return expenseName.get();
	}

	public void setExpenseName(SimpleStringProperty expenseName) {
		this.expenseName = expenseName;
	}

	public String getExpenseCode() {
		return expenseCode.get();
	}

	public void setExpenseCode(SimpleStringProperty expenseCode) {
		this.expenseCode = expenseCode;
	}

	public Double getExpenseDefaultAmount() {
		return expenseDefaultAmount.get();
	}

	public void setExpenseDefaultAmount(SimpleDoubleProperty expenseDefaultAmount) {
		this.expenseDefaultAmount = expenseDefaultAmount;
	}

	
	
	

	

}
