package com.onesoft.courier.forwarder.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ForwarderTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty forwarderCode;
	private SimpleStringProperty name;
	private SimpleStringProperty contactPerson;
	private SimpleStringProperty phone;
	private SimpleStringProperty website;
	private SimpleStringProperty email;
	private SimpleStringProperty address;
	private SimpleStringProperty city;
	private SimpleStringProperty state;
	private SimpleStringProperty country;
	private SimpleStringProperty status;
	private SimpleStringProperty indate;
	
	
	public ForwarderTableBean(int slno, String forwarderCode, String name, String contactPerson, String phone, String website, String email ,
			String address,String city , String status, String indate)
	{

		this.slno=new SimpleIntegerProperty(slno);
		this.forwarderCode=new SimpleStringProperty(forwarderCode);
		this.name=new SimpleStringProperty(name);
		this.contactPerson=new SimpleStringProperty(contactPerson);
		this.phone=new SimpleStringProperty(phone);
		this.website=new SimpleStringProperty(website);
		this.email=new SimpleStringProperty(email);
		this.address=new SimpleStringProperty(address);
		this.city=new SimpleStringProperty(city);
		this.status=new SimpleStringProperty(status);
		this.indate=new SimpleStringProperty(indate);
		
		
	}
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getForwarderCode() {
		return forwarderCode.get();
	}
	public void setForwarderCode(SimpleStringProperty forwarderCode) {
		this.forwarderCode = forwarderCode;
	}
	public String getName() {
		return name.get();
	}
	public void setName(SimpleStringProperty name) {
		this.name = name;
	}
	public String getContactPerson() {
		return contactPerson.get();
	}
	public void setContactPerson(SimpleStringProperty contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getPhone() {
		return phone.get();
	}
	public void setPhone(SimpleStringProperty phone) {
		this.phone = phone;
	}
	public String getWebsite() {
		return website.get();
	}
	public void setWebsite(SimpleStringProperty website) {
		this.website = website;
	}
	public String getEmail() {
		return email.get();
	}
	public void setEmail(SimpleStringProperty email) {
		this.email = email;
	}
	public String getAddress() {
		return address.get();
	}
	public void setAddress(SimpleStringProperty address) {
		this.address = address;
	}
	public String getCity() {
		return city.get();
	}
	public void setCity(SimpleStringProperty city) {
		this.city = city;
	}
	public String getState() {
		return state.get();
	}
	public void setState(SimpleStringProperty state) {
		this.state = state;
	}
	public String getCountry() {
		return country.get();
	}
	public void setCountry(SimpleStringProperty country) {
		this.country = country;
	}
	public String getStatus() {
		return status.get();
	}
	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}


	public String getIndate() {
		return indate.get();
	}


	public void setIndate(SimpleStringProperty indate) {
		this.indate = indate;
	}
	
}
