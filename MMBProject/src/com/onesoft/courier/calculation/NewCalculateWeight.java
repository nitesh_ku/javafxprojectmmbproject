package com.onesoft.courier.calculation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sound.midi.SoundbankResource;
import javax.sound.midi.SysexMessage;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.calculation.bean.NewCalculateWeightBean;

public class NewCalculateWeight {
	
	NewCalculateWeightBean cwBean_Global=new NewCalculateWeightBean();
	
	NewCalculateWeightBean cwBean_Global_for_NewRate=new NewCalculateWeightBean();
	double basicRate=0;
	double additionalRate=0;
	double kgRate=0;
	
	
	public double additional_Slab=0;
	public double additional_DoxNonDox=0;
	
	public double basicWeight=0;
	public double additionalWeight=0;
	public double kgWeight=0;
	
	public double basicAmount=0;
	
	public double additionalAmount=0;
	public double additiona_slab_weight=0;
	double additiona_floor=0;
	
	public double kgAmount=0;
	public double kg_slab_weight=0;
	double kg_floor=0;
	
	public double finalAmount=0;
	
	double weight_after_Slab=0;
	double weight_after_Slab_floor=0;
	double weight_min_amt=0;
	double weight_add_amt=0;
	double total_tdd_min_add=0;
	double total_criti_min_add=0;
	
	public double getNewClientRateDetails(String client,String network,String service,String source,String destination,double billingWeight) throws SQLException
	{

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		String sql=null;
		String subSql=null;
		PreparedStatement preparedStmt=null;
	
			
		try 
		{
				
			sql = "select client,network,service,zone_code,"+source+",basic_range,add_slab,add_range,kg from clientrate_slab as crs, "
					+ "clientrate_zone as crz where crs.clientrate2client=crz.client and crs.clientrate2network=crz.network "
					+ "and crs.clientrate2service=crz.service and crz.client=? and crz.network=? "
					+ "and crz.service=? and crz.zone_code=?";
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,client);
			preparedStmt.setString(2,network);
			preparedStmt.setString(3,service);
			preparedStmt.setString(4,destination);
			
			
			rs = preparedStmt.executeQuery();
			
			System.out.println("Query : "+preparedStmt);
			
			int rowCount=0;

			if(!rs.next())
			{
			
			}
			else
			{
				
			do
			{
				cwBean_Global.setClient(rs.getString("client"));
				cwBean_Global.setNetwork(rs.getString("network"));
				cwBean_Global.setService(rs.getString("service"));
				cwBean_Global.setSource_zone(source);
				cwBean_Global.setDestination_zone(destination);
				cwBean_Global.setMinRates(rs.getString(source));
				cwBean_Global.setBasic_range(rs.getDouble("basic_range"));
				cwBean_Global.setAdd_slab(rs.getDouble("add_slab"));
				cwBean_Global.setAdd_range(rs.getDouble("add_range"));
				cwBean_Global.setKg_status(rs.getString("kg"));

			}while(rs.next());
			
			String[] rates=null;
			
			
			
			if(cwBean_Global.getMinRates().contains(","))
			{
				rates=cwBean_Global.getMinRates().split(",");
			}
			else
			{
				basicRate=Double.valueOf(cwBean_Global.getMinRates());
			}
			
			System.out.println("Source: "+source);
			System.out.println("Destination: "+destination);
			System.out.println("Minimum Rates: "+cwBean_Global.getMinRates());
			System.out.println("Basic Range: "+cwBean_Global.getBasic_range());
			System.out.println("Add Slab: "+cwBean_Global.getAdd_slab());
			System.out.println("Add Range: "+cwBean_Global.getAdd_range());
			System.out.println("KG Status: "+cwBean_Global.getKg_status());
			
			if(!cwBean_Global.getMinRates().contains(","))
			{
				System.out.println("Basic Rate: "+basicRate);
			}
			else if(rates.length==2)
			{
				basicRate=Double.valueOf(rates[0]);
				additionalRate=Double.valueOf(rates[1]);
				
				System.out.println("Basic Rate: "+basicRate);
				System.out.println("Additional Rate: "+additionalRate);
			}
			else if(rates.length==3)
			{
				basicRate=Double.valueOf(rates[0]);
				additionalRate=Double.valueOf(rates[1]);
				kgRate=Double.valueOf(rates[2]);
				
				System.out.println("Basic Rate: "+basicRate);
				System.out.println("Additional Rate: "+additionalRate);
				System.out.println("KG Rates: "+kgRate);
			}
		
			rates=null;
			
			System.out.println("\n");
			
			if(billingWeight<=cwBean_Global.getBasic_range())
			{
				basicAmount=basicRate;
				finalAmount=basicAmount;
				System.out.println("Final Basic Amount: "+finalAmount);
				
			}
			
			else if(billingWeight>cwBean_Global.getBasic_range() && billingWeight<=cwBean_Global.getAdd_range())
			{
				additionalWeight=billingWeight-cwBean_Global.getBasic_range();
				basicAmount=basicRate;
				
				additiona_slab_weight=additionalWeight/cwBean_Global.getAdd_slab();
				additiona_floor=Math.floor(additionalWeight/cwBean_Global.getAdd_slab());
				
				
				if(additiona_floor!=additiona_slab_weight)
				{
					additiona_floor=additiona_floor+1;
					additionalAmount=additiona_floor*additionalRate;
				}
				else
				{
					additionalAmount=additiona_floor*additionalRate;
					//System.out.println("integer value >> "+floor);
				}
				
				//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
				finalAmount=basicAmount+additionalAmount;
				
				if(additionalAmount==0)
				{
					finalAmount=0;
					System.out.println("If Additional rates is not available");
					System.out.println("Final Amount: "+finalAmount);
				}
				else
				{
					System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional Amount: "+ additionalAmount);
					System.out.println("Final Amount: "+finalAmount);
				}
			}

			else if(billingWeight>cwBean_Global.getAdd_range())
			{
				if(kgRate>0)
				{
					
					billingWeight=Math.ceil(billingWeight);
					kgAmount=billingWeight*kgRate;
					
					/*if(billingWeight%2==0)
					{
						kgAmount=billingWeight*kgRate;
					}
					else
					{
						kg_floor=Math.floor(billingWeight)+1;
						kgAmount=kg_floor*kgRate;
					}*/
					
					//kgAmount=billingWeight*kgRate;
					finalAmount=kgAmount;
					System.out.println("Final Amount only from kg: "+finalAmount);
				}
				else
				{
					additionalWeight=billingWeight-cwBean_Global.getBasic_range();
					basicAmount=basicRate;
					
					additiona_slab_weight=additionalWeight/cwBean_Global.getAdd_slab();
					additiona_floor=Math.floor(additionalWeight/cwBean_Global.getAdd_slab());
					
					
					if(additiona_floor!=additiona_slab_weight)
					{
						additiona_floor=additiona_floor+1;
						additionalAmount=additiona_floor*additionalRate;
					}
					else
					{
						additionalAmount=additiona_floor*additionalRate;
						//System.out.println("integer value >> "+floor);
					}
					
					//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
					finalAmount=basicAmount+additionalAmount;
					
					System.out.println("If Kg rates not avaibable");
					System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional Amount: "+ additionalAmount);
					System.out.println("Final Amount: "+finalAmount);
				}
			}
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		//return finalAmount;
		return finalAmount;
		
	}
	
	
	public double getTDD(double tdd_Min, double tdd_Add, double tdd_Slab, double billingWeight)
	{
		System.out.println("\n");
		
		System.out.println("tdd Min: "+tdd_Min);
		System.out.println("tdd Add: "+tdd_Add);
		System.out.println("tdd Slab: "+tdd_Slab);
		System.out.println("tdd bill weight: "+billingWeight);
		
		
		weight_after_Slab=billingWeight/tdd_Slab;
		weight_after_Slab_floor=Math.floor(billingWeight/tdd_Slab);
		//double weight_min_amt=0;
		//double weight_add_amt=0;
		
		if(weight_after_Slab!=weight_after_Slab_floor)
		{
			weight_after_Slab_floor=weight_after_Slab_floor+1;
			weight_min_amt=tdd_Min;
			weight_add_amt=tdd_Add*(weight_after_Slab_floor-1);
			
			
		}
		else
		{
			weight_min_amt=tdd_Min;
			weight_add_amt=tdd_Add*(weight_after_Slab_floor-1);
		}
		
		return total_tdd_min_add=weight_min_amt+weight_add_amt;
		
	}

	public double getCritical(double tdd_Min, double tdd_Add, double tdd_Slab, double billingWeight)
	{
		
		
		System.out.println("\n");
		System.out.println("Crit >> min: "+tdd_Min+" | add: "+tdd_Add+" | slab: "+tdd_Slab+" | billing weigh: "+billingWeight);
		
		weight_after_Slab=billingWeight/tdd_Slab;
		weight_after_Slab_floor=Math.floor(billingWeight/tdd_Slab);
		//double weight_min_amt=0;
		//double weight_add_amt=0;
		
		if(weight_after_Slab!=weight_after_Slab_floor)
		{
			weight_after_Slab_floor=weight_after_Slab_floor+1;
			weight_min_amt=tdd_Min;
			weight_add_amt=tdd_Add*(weight_after_Slab_floor-1);
			
		}
		else
		{
			weight_min_amt=tdd_Min;
			weight_add_amt=tdd_Add*(weight_after_Slab_floor-1);
		}
		
		System.err.println("Return Crit: >>> "+(weight_min_amt+weight_add_amt));
		return total_criti_min_add=weight_min_amt+weight_add_amt;
		
	}
	
	
	public double calculateRateViaListOfRates(String basicRateFrmList,String addRatefrmList,String kgRatefrmList,String kgStatus,double billingWeight,double basic_range,double add_slab,double add_range)
	{
		System.err.println("Re-Calculation Method running... basic >> "+basicRateFrmList+" | add >> "+addRatefrmList+" | KG >> "+kgRatefrmList+" | add slab >> "+add_slab+" | add range :: "+add_range);
		
		double basicRate=0;
		double additionalRate=0;
		double kgRate=0;
		
		//double additional_Slab=0;
		//double additional_DoxNonDox=0;
		//double basicWeight=0;
		double additionalWeight=0;
	//	double kgWeight=0;
		
		double basicAmount=0;
		
		double additionalAmount=0;
		double additiona_slab_weight=0;
		double additiona_floor=0;
		
		double kgAmount=0;
		//double kg_slab_weight=0;
		double kg_floor=0;
		
		double finalAmount=0;
		
		
		
		//double weight_after_Slab=0;
		//double weight_after_Slab_floor=0;
		//double weight_min_amt=0;
		//double weight_add_amt=0;
		//double total_tdd_min_add=0;
		//double total_criti_min_add=0;
		
		
		String[] rates=null;
		
		/*if(rateFrmList.contains(","))
		{
			rates=rateFrmList.split(",");
		}
		else
		{
			basicRate=Double.valueOf(rateFrmList);
		}*/
		
		//System.out.println("Source: "+source);
		//System.out.println("Destination: "+destination);
		//System.out.println("Minimum Rates: "+cwBean_Global.getMinRates());
		//System.out.println("Basic Range: "+cwBean_Global.getBasic_range());
		//System.out.println("Add Slab: "+cwBean_Global.getAdd_slab());
		//System.out.println("Add Range: "+cwBean_Global.getAdd_range());
		//System.out.println("KG Status: "+cwBean_Global.getKg_status());
		
		/*if(!rateFrmList.contains(","))
		{
			System.out.println("Basic Rate: "+basicRate);
		}
		else if(rates.length==2)
		{
			basicRate=Double.valueOf(rates[0]);
			additionalRate=Double.valueOf(rates[1]);
			
			System.out.println("Basic Rate: "+basicRate);
			System.out.println("Additional Rate: "+additionalRate);
		}
		else if(rates.length==3)
		{
			basicRate=Double.valueOf(rates[0]);
			additionalRate=Double.valueOf(rates[1]);
			kgRate=Double.valueOf(rates[2]);
			
			System.out.println("Basic Rate: "+basicRate);
			System.out.println("Additional Rate: "+additionalRate);
			System.out.println("KG Rates: "+kgRate);
		}*/
		
		if(basicRateFrmList!=null)
		{
			basicRate=Double.valueOf(basicRateFrmList);
		}
		else
		{
			basicRate=0;
		}
		
		if(addRatefrmList!=null || !addRatefrmList.equals("0"))
		{
			additionalRate=Double.valueOf(addRatefrmList);
		}
		else 
		{
			additionalRate=0;
		}
		
		if(kgRatefrmList!=null || !kgRatefrmList.equals("0"))
		{
			kgRate=Double.valueOf(kgRatefrmList);
		}
		else
		{
			kgRate=0;
		}
		
		
		System.err.println("Add rate: "+additionalRate+"  >>>>>>>>> | KG rate: "+kgRate);
		
	
		rates=null;
		
		System.out.println("\n");
		
		if(kgStatus.equals("T"))
		{
			if(billingWeight>basic_range)
			{
				finalAmount=basicRate*billingWeight;
			}
			else
			{
				finalAmount=basicRate*basic_range;
			}
			System.err.println("Final amount if kg status is True: "+ finalAmount);
		}
		else
		{
			if(billingWeight<=basic_range)
			{
				basicAmount=basicRate;
				finalAmount=basicAmount;
				System.out.println("Final Basic Amount: "+finalAmount);
				
			}
			
			else if(billingWeight>basic_range && billingWeight<=add_range)
			{
				additionalWeight=billingWeight-basic_range;
				basicAmount=basicRate;
				
				additiona_slab_weight=additionalWeight/add_slab;
				additiona_floor=Math.floor(additionalWeight/add_slab);
				
				if(additiona_floor!=additiona_slab_weight)
				{
					additiona_floor=additiona_floor+1;
					additionalAmount=additiona_floor*additionalRate;
				}
				else
				{
					additionalAmount=additiona_floor*additionalRate;
					//System.out.println("integer value >> "+floor);
				}
				
				//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
				finalAmount=basicAmount+additionalAmount;
				
				if(additionalAmount==0)
				{
					finalAmount=0;
					System.out.println("If Additional rates is not available");
					System.out.println("Final Amount: "+finalAmount);
				}
				else
				{
					System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional Amount: "+ additionalAmount);
					System.out.println("Final Amount: "+finalAmount);
				}
			}
	
			else if(billingWeight>add_range)
			{
				if(kgRate>0)
				{
					
					billingWeight=Math.ceil(billingWeight);
					kgAmount=billingWeight*kgRate;
					
					/*if(billingWeight%2==0)
					{
						kgAmount=billingWeight*kgRate;
					}
					else
					{
						kg_floor=Math.floor(billingWeight)+1;
						kgAmount=kg_floor*kgRate;
					}*/
					
					//kgAmount=billingWeight*kgRate;
					finalAmount=kgAmount;
					System.out.println("Final Amount only from kg: "+finalAmount);
				}
				else
				{
					additionalWeight=billingWeight-basic_range;
					basicAmount=basicRate;
					
					additiona_slab_weight=additionalWeight/add_slab;
					additiona_floor=Math.floor(additionalWeight/add_slab);
					
					
					System.out.println("Floor >>>> "+additiona_floor+" >>>> Additional Amt >>>  1 >>>> "+additionalAmount);
					
					if(additiona_floor!=additiona_slab_weight)
					{
						additiona_floor=additiona_floor+1;
						additionalAmount=additiona_floor*additionalRate;
					}
					else
					{
						additionalAmount=additiona_floor*additionalRate;
						//System.out.println("integer value >> "+floor);
					}
					
					//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
					finalAmount=basicAmount+additionalAmount;
					
					System.out.println("If Kg rates not avaibable");
					System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional >> Amount: "+ additionalAmount);
					System.out.println("Final Amount: "+finalAmount);
				}
			}
		}
		return finalAmount;
		
	}
	
	
	public double loadNewRateForCalulation(Boolean isReverse,String client,String network,String service,String source,String destination,double billingWeight) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		String sql=null;
		String subSql=null;
		PreparedStatement preparedStmt=null;
		
		double finalBasicAmount=0;
		
		try
		
		{
			sql = "select client,network,service,zone_code,"+source+",basic_range,add_slab,add_range,kg from clientrate_slab as crs, "
					+ "clientrate_zone as crz where crs.clientrate2client=crz.client and crs.clientrate2network=crz.network "
					+ "and crs.clientrate2service=crz.service and crz.client=? and crz.network=? "
					+ "and crz.service=? and crz.zone_code=?";
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,client);
			preparedStmt.setString(2,network);
			preparedStmt.setString(3,service);
			preparedStmt.setString(4,destination);
			
			
			rs = preparedStmt.executeQuery();
			
			System.out.println("Query >>> sql : "+preparedStmt);
			
			int rowCount=0;

			if(!rs.next())
			{
				System.err.println(" ==================== Rates not found ================================>>>>>>>>>>>>>>>>>>>>>");
				
				
			}
			else
			{
				
			do
			{
				cwBean_Global_for_NewRate.setClient(rs.getString("client"));
				cwBean_Global_for_NewRate.setNetwork(rs.getString("network"));
				cwBean_Global_for_NewRate.setService(rs.getString("service"));
				cwBean_Global_for_NewRate.setSource_zone(source);
				cwBean_Global_for_NewRate.setDestination_zone(destination);
				cwBean_Global_for_NewRate.setMinRates(rs.getString(source));
				cwBean_Global_for_NewRate.setBasic_range(rs.getDouble("basic_range"));
				cwBean_Global_for_NewRate.setAdd_slab(rs.getDouble("add_slab"));
				cwBean_Global_for_NewRate.setAdd_range(rs.getDouble("add_range"));
				cwBean_Global_for_NewRate.setKg_status(rs.getString("kg"));
				
				System.err.println("Cient :::: "+rs.getString("client"));
				System.err.println("Network :::: "+rs.getString("network"));
				System.err.println("Service :::: "+rs.getString("service"));
				System.err.println("Source Zone :::: "+source);
				System.err.println("Desti Zone :::: "+destination);
				System.err.println("Min Rate :::: "+source);
				System.err.println("Basic Range :::: "+rs.getString("basic_range"));
				System.err.println("Add Slab :::: "+rs.getString("add_slab"));
				System.err.println("Add Range :::: "+rs.getString("add_range"));
				System.err.println("KG Status :::: "+rs.getString("kg"));
				
				System.err.println("KG Status >>>>>>>>>>>>>> "+cwBean_Global_for_NewRate.getKg_status());

			}while(rs.next());
			}
			
			finalBasicAmount=loadAdditinalKg(isReverse,client, network, service,billingWeight,source,destination);
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return finalBasicAmount;
		
	}
	
	public double loadAdditinalKg(Boolean isReverse,String client,String network,String service,double billingWeight,String source, String destination) throws SQLException
	{
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		String sql=null;
		String subSql=null;
		PreparedStatement preparedStmt=null;
		
		
		
		try
		{
				/*sql="select (select "+cwBean_Global_for_NewRate.getSource_zone()+" as additional_rate from clientrate_zone where client=? and network=? and service=? "
								+ "and rate_type='Additional' and zone_code is Null),(select "+cwBean_Global_for_NewRate.getSource_zone()+" as kg_rate from "
								+ "clientrate_zone where client=? and network=? and service=? and rate_type='KG' and zone_code is Null)";*/
				
			if(isReverse==true)
			{
				sql="select additional_reverse,kg_reverse from clientrate_zone where client=? and network=? and service=? and zone_code=?";
				
				
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,client);
				preparedStmt.setString(2,network);
				preparedStmt.setString(3,service);
				preparedStmt.setString(4, destination);
			
				rs = preparedStmt.executeQuery();
			}
			else
			{
				sql="select (select "+source+" as additional_rate from clientrate_zone where client=? and network=? and service=? "
						+ "and rate_type='Additional' and zone_code is Null),(select "+source+" as kg_rate from "
						+ "clientrate_zone where client=? and network=? and service=? and rate_type='KG' and zone_code is Null)";
			
			//sql = "select "+source+" from clientrate_zone where client=? and network=? and service=? and type=? OR type=?";
			
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,client);
				preparedStmt.setString(2,network);
				preparedStmt.setString(3,service);
				preparedStmt.setString(4,client);
				preparedStmt.setString(5,network);
				preparedStmt.setString(6,service);
				
				rs = preparedStmt.executeQuery();
			}
				
			
			System.out.println("Query : "+preparedStmt);
			
			int rowCount=0;

			if(!rs.next())
			{
			
			}
			else
			{
				do
				{
					//if()select (select zonedetail2zonetype as source from zonedetail where zone2city='DEL'),(select zonedetail2zonetype as destination from zonedetail where zone2city='KNJ')
					if(isReverse==true)
					{
						cwBean_Global_for_NewRate.setAdd_rate(rs.getDouble("additional_reverse"));
						cwBean_Global_for_NewRate.setKg_rate(rs.getDouble("kg_reverse"));
					}
					else
					{
						cwBean_Global_for_NewRate.setAdd_rate(rs.getDouble("additional_rate"));
						cwBean_Global_for_NewRate.setKg_rate(rs.getDouble("kg_rate"));
					}
					
					
	
				}while(rs.next());
			}
			
			System.out.println("Min Rates:: >>>>>>>>>>>>>>>> "+cwBean_Global_for_NewRate.getMinRates());
			
			if(cwBean_Global_for_NewRate.getMinRates()!=null)
			{
				basicRate=Double.valueOf(cwBean_Global_for_NewRate.getMinRates());
			}
			else
			{
				basicRate=0;
			}
			
			additionalRate=cwBean_Global_for_NewRate.getAdd_rate();
			kgRate=cwBean_Global_for_NewRate.getKg_rate();
			
			System.out.println("Client: "+cwBean_Global_for_NewRate.getClient());
			System.out.println("Network: "+cwBean_Global_for_NewRate.getNetwork());
			System.out.println("Service: "+cwBean_Global_for_NewRate.getService());
			System.out.println("Source: "+cwBean_Global_for_NewRate.getSource_zone());
			System.out.println("Destination: "+cwBean_Global_for_NewRate.getDestination_zone());
			System.out.println("Basic Range: "+cwBean_Global_for_NewRate.getBasic_range());
			System.out.println("Add Slab: "+cwBean_Global_for_NewRate.getAdd_slab());
			System.out.println("Add Range: "+cwBean_Global_for_NewRate.getAdd_range());
			System.out.println("KG Status: "+cwBean_Global_for_NewRate.getKg_status());
			System.out.println("Basic Rates: "+basicRate);
			System.out.println("Additional Rate: "+additionalRate);
			System.out.println("KG Rate: "+kgRate);
			
			
			if(cwBean_Global_for_NewRate.getKg_status().equals("T"))
			{
				if(billingWeight>cwBean_Global_for_NewRate.getBasic_range())
				{
					finalAmount=basicRate*billingWeight;
				}
				else
				{
					finalAmount=basicRate*cwBean_Global_for_NewRate.getBasic_range();
				}
				System.err.println("Final amount if kg status is True: "+ finalAmount);
			}
			else
			{
				if(billingWeight<=cwBean_Global_for_NewRate.getBasic_range())
				{
					basicAmount=basicRate;
					finalAmount=basicAmount;
					System.err.println("Final Basic Amount: "+finalAmount);
				}
				
				else if(billingWeight>cwBean_Global_for_NewRate.getBasic_range() && billingWeight<=cwBean_Global_for_NewRate.getAdd_range())
				{
					additionalWeight=billingWeight-cwBean_Global_for_NewRate.getBasic_range();
					basicAmount=basicRate;
					
					additiona_slab_weight=additionalWeight/cwBean_Global_for_NewRate.getAdd_slab();
					additiona_floor=Math.floor(additionalWeight/cwBean_Global_for_NewRate.getAdd_slab());
					
					
					if(additiona_floor!=additiona_slab_weight)
					{
						additiona_floor=additiona_floor+1;
						additionalAmount=additiona_floor*additionalRate;
					}
					else
					{
						additionalAmount=additiona_floor*additionalRate;
						//System.out.println("integer value >> "+floor);
					}
					
					//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
					finalAmount=basicAmount+additionalAmount;
					
					if(additionalAmount==0)
					{
						finalAmount=0;
						System.out.println("If Additional rates is not available");
						System.err.println("Final Amount: "+finalAmount);
					}
					else
					{
						System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional Amount: "+ additionalAmount);
						System.err.println("Final Amount: "+finalAmount);
					}
				}

				else if(billingWeight>cwBean_Global_for_NewRate.getAdd_range())
				{
					if(kgRate>0)
					{
						billingWeight=Math.ceil(billingWeight);
						kgAmount=billingWeight*kgRate;
						
						/*if(billingWeight%2==0)
						{
							kgAmount=billingWeight*kgRate;
						}
						else
						{
							kg_floor=Math.floor(billingWeight)+1;
							kgAmount=kg_floor*kgRate;
						}*/
						
						//kgAmount=billingWeight*kgRate;
						finalAmount=kgAmount;
						System.err.println("Final Amount only from kg: "+finalAmount);
					}
					else
					{
						additionalWeight=billingWeight-cwBean_Global_for_NewRate.getBasic_range();
						basicAmount=basicRate;
						
						additiona_slab_weight=additionalWeight/cwBean_Global_for_NewRate.getAdd_slab();
						additiona_floor=Math.floor(additionalWeight/cwBean_Global_for_NewRate.getAdd_slab());
						
						
						if(additiona_floor!=additiona_slab_weight)
						{
							additiona_floor=additiona_floor+1;
							additionalAmount=additiona_floor*additionalRate;
						}
						else
						{
							additionalAmount=additiona_floor*additionalRate;
							//System.out.println("integer value >> "+floor);
						}
						
						//additionalAmount=(additionalWeight/cwBean_Global.getAdd_slab())*additionalRate;
						finalAmount=basicAmount+additionalAmount;
						
						System.out.println("If Kg rates not avaibable");
						System.out.println("Final Basic Amount: "+basicAmount+" | Final Additional Amount: "+ additionalAmount);
						System.err.println("Final Amount: "+finalAmount);
					}
				}
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return finalAmount;
	}

	
}
