package com.onesoft.courier.calculation.bean;

public class TaxCalculatorBean {
	
	
	private String taxname1;
	private String taxname2;
	private String taxname3;
	
	private double taxrate1;
	private double taxrate2;
	private double taxrate3;
	
	private double taxAmount1;
	private double taxAmount2;
	private double taxAmount3;
	
	
	public double getTaxAmount1() {
		return taxAmount1;
	}
	public void setTaxAmount1(double taxAmount1) {
		this.taxAmount1 = taxAmount1;
	}
	public double getTaxAmount2() {
		return taxAmount2;
	}
	public void setTaxAmount2(double taxAmount2) {
		this.taxAmount2 = taxAmount2;
	}
	public double getTaxAmount3() {
		return taxAmount3;
	}
	public void setTaxAmount3(double taxAmount3) {
		this.taxAmount3 = taxAmount3;
	}
	public double getTaxrate1() {
		return taxrate1;
	}
	public void setTaxrate1(double taxrate1) {
		this.taxrate1 = taxrate1;
	}
	public double getTaxrate2() {
		return taxrate2;
	}
	public void setTaxrate2(double taxrate2) {
		this.taxrate2 = taxrate2;
	}
	public double getTaxrate3() {
		return taxrate3;
	}
	public void setTaxrate3(double taxrate3) {
		this.taxrate3 = taxrate3;
	}
	public String getTaxname1() {
		return taxname1;
	}
	public void setTaxname1(String taxname1) {
		this.taxname1 = taxname1;
	}
	public String getTaxname2() {
		return taxname2;
	}
	public void setTaxname2(String taxname2) {
		this.taxname2 = taxname2;
	}
	public String getTaxname3() {
		return taxname3;
	}
	public void setTaxname3(String taxname3) {
		this.taxname3 = taxname3;
	}
	
	
	

}
