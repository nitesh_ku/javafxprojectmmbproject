package com.onesoft.courier.calculation.bean;

public class NewCalculateWeightBean {
	
	private String client;
	private String network;
	private String service;
	private String source_zone;
	private String destination_zone;
	private double basic_range;
	private double add_slab;
	private double add_range;
	private String kg_status;
	private double basic_rate;
	private double add_rate;
	private double kg_rate;
	
	private String minRates;
	
	
	
	
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getSource_zone() {
		return source_zone;
	}
	public void setSource_zone(String source_zone) {
		this.source_zone = source_zone;
	}
	public String getDestination_zone() {
		return destination_zone;
	}
	public void setDestination_zone(String destination_zone) {
		this.destination_zone = destination_zone;
	}
	public double getBasic_range() {
		return basic_range;
	}
	public void setBasic_range(double basic_range) {
		this.basic_range = basic_range;
	}
	public double getAdd_slab() {
		return add_slab;
	}
	public void setAdd_slab(double add_slab) {
		this.add_slab = add_slab;
	}
	public double getAdd_range() {
		return add_range;
	}
	public void setAdd_range(double add_range) {
		this.add_range = add_range;
	}
	public String getKg_status() {
		return kg_status;
	}
	public void setKg_status(String kg_status) {
		this.kg_status = kg_status;
	}
	public double getBasic_rate() {
		return basic_rate;
	}
	public void setBasic_rate(double basic_rate) {
		this.basic_rate = basic_rate;
	}
	public double getAdd_rate() {
		return add_rate;
	}
	public void setAdd_rate(double add_rate) {
		this.add_rate = add_rate;
	}
	public double getKg_rate() {
		return kg_rate;
	}
	public void setKg_rate(double kg_rate) {
		this.kg_rate = kg_rate;
	}
	public String getMinRates() {
		return minRates;
	}
	public void setMinRates(String minRates) {
		this.minRates = minRates;
	}


	
	
}
