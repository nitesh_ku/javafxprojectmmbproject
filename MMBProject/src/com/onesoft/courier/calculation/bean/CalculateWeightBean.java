package com.onesoft.courier.calculation.bean;

public class CalculateWeightBean {
	
	private String client;
	private String network;
	private String service;
	private String fromZoneOrCity;
	private String toZoneOrCity;
	private String slabType;
	private double slab;
	private double weightupto;
	private double dox_NonDox;
	
	
	
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getFromZoneOrCity() {
		return fromZoneOrCity;
	}
	public void setFromZoneOrCity(String fromZoneOrCity) {
		this.fromZoneOrCity = fromZoneOrCity;
	}
	public String getToZoneOrCity() {
		return toZoneOrCity;
	}
	public void setToZoneOrCity(String toZoneOrCity) {
		this.toZoneOrCity = toZoneOrCity;
	}
	public String getSlabType() {
		return slabType;
	}
	public void setSlabType(String slabType) {
		this.slabType = slabType;
	}
	public double getSlab() {
		return slab;
	}
	public void setSlab(double slab) {
		this.slab = slab;
	}
	public double getWeightupto() {
		return weightupto;
	}
	public void setWeightupto(double weightupto) {
		this.weightupto = weightupto;
	}
	public double getDox_NonDox() {
		return dox_NonDox;
	}
	public void setDox_NonDox(double dox_NonDox) {
		this.dox_NonDox = dox_NonDox;
	}


}
