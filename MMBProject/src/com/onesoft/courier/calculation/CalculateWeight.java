package com.onesoft.courier.calculation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.DataEntryBean;
import com.onesoft.courier.calculation.bean.CalculateWeightBean;

public class CalculateWeight {
	
	
	public double additional_Slab=0;
	public double additional_DoxNonDox=0;
	
	public double basicWeight=0;
	public double additionalWeight=0;
	public double kgWeight=0;
	
	public double basicAmount=0;
	public double additionalAmount=0;
	public double kgAmount=0;
	
	public double finalAmount=0;
	
		
	List<CalculateWeightBean> list_WeightData=new ArrayList<>();
		
	public double getClientRateDetails(String client,String network,String service,String source,String destination,String type, double weight) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		String sql=null;
		String subSql=null;
		
		//DataEntryBean deBean=new DataEntryBean();
		
		if(type.equals("Dox"))
		{
			type="dox";
		}
		else
		{
			type="nondox";
		}
		
		CalculateWeightBean cwBean=new CalculateWeightBean();
		
		cwBean.setClient(client);
		cwBean.setNetwork(network);
		cwBean.setService(service);
		cwBean.setWeightupto(weight);
		cwBean.setFromZoneOrCity(source);
		cwBean.setToZoneOrCity(destination);
		
		subSql=cwBean.getWeightupto()+" and clientratemaster2client='"+cwBean.getClient()+"' and clientratemaster2vendorcode='"+cwBean.getNetwork()+
				"' and clientratemaster2servicecode='"+cwBean.getService()+"' and sourcezone='"+cwBean.getFromZoneOrCity()+"' and targetzone='"
				+cwBean.getToZoneOrCity()+"' order by weightupto";
		
		try 
		{
			st = con.createStatement();
			sql="(select slabtype,slab,weightupto,"+type+" from clientratemaster where weightupto <= "+subSql+" desc limit 2) "
					+ "UNION DISTINCT (select slabtype,slab,weightupto,"+type+" from clientratemaster where weightupto >= "+subSql+" asc limit 1) order by weightupto";
			
			rs = st.executeQuery(sql);			
			
			System.out.println("Final Sql: "+sql);
						
			int rowCount=0;

			while(rs.next())
			{
				rowCount++;
				
				CalculateWeightBean bean=new CalculateWeightBean();
				
				bean.setClient(cwBean.getClient());
				bean.setNetwork(cwBean.getNetwork());
				bean.setService(cwBean.getService());
				bean.setSlabType(rs.getString("slabtype"));
				bean.setWeightupto(rs.getDouble("weightupto"));
				bean.setSlab(rs.getDouble("slab"));
				bean.setDox_NonDox(rs.getDouble(type));
				
				list_WeightData.add(bean);

			}
			
			System.out.println("row count: "+rowCount);
			
			if(list_WeightData.size()>0)
			{
				if(rowCount==1)		//if list contains only 1 row
				{
					
				}
				else if(rowCount>2)		//if list contains only 1 row
				{
					list_WeightData.remove(0);
				}
				
			// Condition to perform Calculation process
				
				if(rowCount==1)		//if list contains only 1 row
				{
					if(list_WeightData.get(0).getSlabType().equals("B"))
					{
						for(CalculateWeightBean bean:list_WeightData)
						{
							basicAmount=bean.getDox_NonDox();
						}
						finalAmount=basicAmount;
					}
					else
					{
						for(CalculateWeightBean bean:list_WeightData)
						{
							kgAmount=(bean.getWeightupto()*bean.getDox_NonDox());
							finalAmount=kgAmount;
						}
					}
				}
				
				else
				{
					if(list_WeightData.get(0).getSlabType().equals("B") && list_WeightData.get(1).getSlabType().equals("B"))
					{
						
						basicAmount=list_WeightData.get(1).getDox_NonDox();
						basicWeight=list_WeightData.get(1).getWeightupto();
						if(weight<=basicWeight)
						{
							finalAmount=basicAmount;
						}	
						/*else
						{
							System.out.println("multiple row >> Final Basic Weight: 0.0");
						}*/
					}
					else if(list_WeightData.get(0).getSlabType().equals("K") && list_WeightData.get(1).getSlabType().equals("K") || list_WeightData.get(0).getSlabType().equals("E") && list_WeightData.get(1).getSlabType().equals("K"))
					{
						if((weight%1)!=0)	// Condition to check the number is decimal or not
						{
							//System.out.println("not a integer");
		
							weight=Math.floor(weight)+1;
							kgAmount=weight*list_WeightData.get(1).getDox_NonDox();
							finalAmount=kgAmount;
						}
						else
						{
							//System.out.println("integer");
							
							kgAmount=weight*list_WeightData.get(1).getDox_NonDox();
							finalAmount=kgAmount;
					    }
					}

					else if(list_WeightData.get(0).getSlabType().equals("B") && list_WeightData.get(1).getSlabType().equals("E"))
					{
						for(CalculateWeightBean bean:list_WeightData)
						{
							if(bean.getSlabType().equals("B"))
							{
								basicWeight=bean.getWeightupto();
								basicAmount=bean.getDox_NonDox();
							}
							
							if(bean.getSlabType().equals("E"))
							{
								additionalWeight=bean.getWeightupto();
								additional_DoxNonDox=bean.getDox_NonDox();
								additional_Slab=bean.getSlab();
							}
						}
						
						additionalAmount=(((weight-basicWeight)/additional_Slab)*additional_DoxNonDox);
						finalAmount=basicAmount+additionalAmount;
					}
				}
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return finalAmount;
	}

	
	

}
