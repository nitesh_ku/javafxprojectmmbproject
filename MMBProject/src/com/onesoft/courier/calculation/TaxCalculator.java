package com.onesoft.courier.calculation;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.onesoft.courier.calculation.bean.TaxCalculatorBean;

public class TaxCalculator {
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	public static List<TaxCalculatorBean> LIST_GST_TAXES_WITH_AMOUNT=new ArrayList<>();
	
	public static double taxAmount1=0;
	public static double taxAmount2=0;
	public static double taxAmount3=0;
	
	public double calculateFuelAmount(double basicAmount,double totalAddedVAS,double insuranceAmt,double fuelRate,boolean fov_status,boolean fuel_Excluding_fov)
	{
		double fuelAmount=0;
		double totalAmountBeforeFuelCalculation=0;
		
		System.err.println(" ============ basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
		
		if(fov_status==false && fuel_Excluding_fov==false)
		{
			totalAmountBeforeFuelCalculation=basicAmount;
			System.out.println(" >>>> if Both False: >> basic: "+basicAmount+" = "+totalAmountBeforeFuelCalculation);
		}
		else if(fov_status==false && fuel_Excluding_fov==true)
		{
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS;
			System.out.println(" >>>> if only Fuel Excluding true: >> basic+TotaVAS >> "+basicAmount+"+"+totalAddedVAS+" = "+totalAmountBeforeFuelCalculation);
		}
		else if(fov_status==true && fuel_Excluding_fov==false)
		{
			totalAmountBeforeFuelCalculation=basicAmount+insuranceAmt;
			System.out.println(" >>>> if only FOV true: >> basic+insurance Amount >> "+basicAmount+"+"+insuranceAmt+" = "+totalAmountBeforeFuelCalculation);
		}
		else if(fov_status==true && fuel_Excluding_fov==true)
		{
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS+insuranceAmt;
			System.out.println(" >>>> if both true: >> basic+TotaVAS+insurance Amount >> "+basicAmount+"+"+totalAddedVAS+"+"+insuranceAmt+" = "+totalAmountBeforeFuelCalculation);
		}
		
		
		/*if(fov_status==false)
		{
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS;
			System.err.println(" >>>>>>>>>>>>>>>>> basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
			System.out.println(" >>>> if FOV status: "+fov_status +" | basic + totalvas: "+basicAmount+" + "+totalAddedVAS+" = "+totalAmountBeforeFuelCalculation);
		}
		else
		{
			System.err.println(" >>>>>>>>>>>>>>>>> basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS+insuranceAmt;
			System.out.println(" >>>> if FOV status: "+fov_status +" | basic + totalvas + Ins Amt: "+basicAmount+" + "+totalAddedVAS+" + "+insuranceAmt+" = "+totalAmountBeforeFuelCalculation);
		}*/
		
		fuelAmount=(totalAmountBeforeFuelCalculation*fuelRate)/100;
		return fuelAmount;
	}
	
	
	/*public double calculateFuelAmount(double basicAmount,double totalAddedVAS,double insuranceAmt,double fuelRate,boolean fov_status,boolean fuel_Excluding_fov)
	{
		double fuelAmount=0;
		double totalAmountBeforeFuelCalculation=0;
		
		System.err.println(" ============ basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
		
		if(fov_status==false)
		{
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS;
			System.err.println(" >>>>>>>>>>>>>>>>> basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
			System.out.println(" >>>> if FOV status: "+fov_status +" | basic + totalvas: "+basicAmount+" + "+totalAddedVAS+" = "+totalAmountBeforeFuelCalculation);
		}
		else
		{
			System.err.println(" >>>>>>>>>>>>>>>>> basic: "+basicAmount+" | VAS: "+totalAddedVAS+" | Ins Amnt: "+insuranceAmt+" | Fuel Rate: "+fuelRate+" | FOV Status: "+fov_status);
			totalAmountBeforeFuelCalculation=basicAmount+totalAddedVAS+insuranceAmt;
			System.out.println(" >>>> if FOV status: "+fov_status +" | basic + totalvas + Ins Amt: "+basicAmount+" + "+totalAddedVAS+" + "+insuranceAmt+" = "+totalAmountBeforeFuelCalculation);
		}
		
		fuelAmount=(totalAmountBeforeFuelCalculation*fuelRate)/100;
		return fuelAmount;
	}*/
	
	
	public void calculateGST(double taxableValue, double taxrate1, double taxrate2, double taxrate3)
	{	
		System.out.println("Amount: "+taxableValue+" | rate1: "+taxrate1+" | rate2: "+taxrate2+" | rate3: "+taxrate3);
		
		if(taxrate3==0)
		{
			taxAmount1=(taxableValue*taxrate1)/100;
			taxAmount2=(taxableValue*taxrate2)/100;
			
			System.out.println("CGST: >>>>>>>>> "+taxAmount1);
			System.out.println("SGST: >>>>>>>>> "+taxAmount2);
		}
		else
		{
			taxAmount3=(taxableValue*taxrate3)/100;
			System.out.println("IGST: >>>>>>>>> "+taxAmount3);
		}
	}
	
	
}
