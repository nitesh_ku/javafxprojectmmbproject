package com.onesoft.courier.main;
	
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.Animation;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.onesoft.courier.booking.controller.DataEntryController;
import com.onesoft.courier.booking.controller.DataEntryPopUpController;
import com.onesoft.courier.graph.controller.ClientVsZoneController;
import com.onesoft.courier.graph.controller.ClientVsZone_ServiceController;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;


public class Main extends Application implements MapComponentInitializedListener {
	
	public static Stage primaryStage;
	public static Stage loadingStage;
	public static Stage loginStage;
	public static BorderPane borderLayout;
	private static BorderPane loginBorderLayout;
	private static BorderPane loadingBorderLayout;
	
	public static Stage dimensionStage;
	public static Stage dataEntryPopUpStage;
	public static Stage addOnExpensesStage;
	public static Stage selectedVASITemStage;
	//public static Stage clientVASStage;
	public static Stage hyperLinkStage;
	public static Stage multipleSelectStage;
	public static Stage popUpWindowStage;
	
	List<String> list_cordinates=new ArrayList<>();
	
	GoogleMapView mapView;
	GoogleMap map;
	
	@Override
	public void start(Stage primaryStage) throws IOException, SQLException {
		
		this.primaryStage =primaryStage;
		this.loginStage=primaryStage;
		this.loadingStage=primaryStage;
		//this.popUpWindowStage=primaryStage;
		
		this.primaryStage.setTitle("MMB Project");
		
		splashPage();
		//loginpage();
		//homePage();
		
		
	
		
		/*try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}*/
	}
	
	
// *****************************************************************************

	public void showGoogleMap()
	{
		list_cordinates.add("28.6310791 | 77.2764348 | Location 1");
		list_cordinates.add("28.712919 | 77.184277 | Location 2");
		
		mapView = new GoogleMapView();
	    mapView.addMapInializedListener(this);
	    popUpWindowStage=new Stage();
	    Scene scene = new Scene(mapView);
	    
	    popUpWindowStage.setTitle("JavaFX and Google Maps");
	    popUpWindowStage.setScene(scene);
	    popUpWindowStage.show();
	}
	
	
	@Override
	public void mapInitialized() {
	
		MapOptions mapOptions = null;
		
		int noOfCoordinates=0;
		
		for(String coordinates: list_cordinates)
		{
			noOfCoordinates++;
			
			String[] axis=coordinates.replaceAll("\\s+","").split("\\|");
			System.out.println("Co-ordinate "+noOfCoordinates+" :: Latitude >> " +axis[0]+" | Longitude >> "+axis[1]);
			  mapOptions  = new MapOptions();

			    mapOptions.center(new LatLong(Double.valueOf(axis[0]), Double.valueOf(axis[1])))
			            .mapType(MapTypeIdEnum.ROADMAP)
			            .overviewMapControl(false)
			            .panControl(false)
			            .rotateControl(false)
			            .scaleControl(false)
			            .streetViewControl(false)
			            .zoomControl(false)
			            .zoom(11);
			    map = mapView.createMap(mapOptions);

		}
		
		for(String coordinates: list_cordinates)
		{
			String[] axis=coordinates.split("\\|");
			MarkerOptions markerOptions = new MarkerOptions();

			markerOptions.position( new LatLong(Double.valueOf(axis[0].trim()), Double.valueOf(axis[1].trim())))
	                .visible(Boolean.TRUE)
	                .title(axis[2].trim());
	    			
			Marker marker = new Marker( markerOptions);
			map.addMarker(marker); 
		}
		
		
	   /* MapOptions mapOptions = new MapOptions();

	    mapOptions.center(new LatLong(28.6310791, 77.2764348))
	            .mapType(MapTypeIdEnum.ROADMAP)
	            .overviewMapControl(false)
	            .panControl(false)
	            .rotateControl(false)
	            .scaleControl(false)
	            .streetViewControl(false)
	            .zoomControl(false)
	            .zoom(12);

	    map = mapView.createMap(mapOptions);

	    //Add a marker to the map
	    MarkerOptions markerOptions = new MarkerOptions();

	    markerOptions.position( new LatLong(28.6310791, 77.2764348) )
	                .visible(Boolean.TRUE)
	                .title("My Marker");

	    Marker marker = new Marker( markerOptions );

	    map.addMarker(marker);*/
		
	}
	
	
// *****************************************************************************
	
	public static void splashPage() throws IOException
	{
		loadingStage.setTitle("MMB Project");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/loading/LoadingSplash.fxml"));
		loadingBorderLayout = loader.load();
		Scene scene = new Scene(loadingBorderLayout);
		
		//mainLayout.setStyle("-fx-background-color: cornsilk;");
		//primaryStage.initStyle(StageStyle.UNDECORATED);
		loadingStage.initStyle(StageStyle.DECORATED);
		loadingStage.setMaximized(false);
		loadingStage.setScene(scene);
		loadingStage.setResizable(false);
		loadingStage.show();
		
	}
	
	
	public static void loginpage() throws IOException
	{
		primaryStage.setTitle("MMB Project");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/login/controller/Login.fxml"));
		loginBorderLayout = loader.load();
		Scene scene = new Scene(loginBorderLayout);
		
		//mainLayout.setStyle("-fx-background-color: cornsilk;");
		
		primaryStage.setMaximized(false);
		loginStage.setScene(scene);
		primaryStage.setResizable(false);
		loginStage.show();
	}
	

// *****************************************************************************	
	
	public static void homePage() throws IOException, SQLException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/home/controller/Home.fxml"));
		borderLayout = loader.load();
		Scene scene = new Scene(borderLayout);
		
		//mainLayout.setStyle("-fx-background-color: cornsilk;");
		primaryStage.setMaximized(true);
		primaryStage.setScene(scene);
		primaryStage.setOnCloseRequest(e -> Platform.exit());
		primaryStage.setResizable(true);
		primaryStage.show();
		
	}
	
// *****************************************************************************
	
	public void showInward() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/InwardPage.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
		//alertbox();
		
	}
	
	
// *****************************************************************************
	
	public void showOutward() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/OutwardPage.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
				
	}
	
	
// *****************************************************************************
	
	public void showDataEntry() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/DataEntry.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}
	
// *****************************************************************************
	
	public void showAutoCalculation() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/AutoCalculation.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}	
	
// *****************************************************************************

	public void showImportBookingData() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/ImportBookingData.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}	
	

// *****************************************************************************

	public void showUpdateBookingData() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/UpdateBookingData.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}		

// *****************************************************************************

	public void showCloudInward() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/CloudInward.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}
	
// *****************************************************************************

	public void showManifestForm() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/Manifest.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}	


// *****************************************************************************

	public void showDimensionForm() throws IOException {
		
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/DimensionForm.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		//borderLayout.setCenter(bpZoStage stage=new Stage();neReport);
		dimensionStage=new Stage();
		
		Scene sc=new Scene(bpZoneReport);
		

		dimensionStage.initModality(Modality.APPLICATION_MODAL);
		dimensionStage.setScene(sc);
		dimensionStage.setTitle("Enter Dimensions for AWB no: "+ DataEntryController.awbno);
		dimensionStage.setResizable(false);
		dimensionStage.show();
		

	}
	
// *****************************************************************************

	public void showDataEntryPopupPage(String awbNo) throws IOException, SQLException {
		
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/DataEntryPopUp.fxml"));
		BorderPane bpZoneReport = loader.load();
		DataEntryController dataEntryController = (DataEntryController) loader.getController();
        dataEntryController.setAWBNoViaOTHController(awbNo);
		// primaryStage.setMaximized(true);
		//borderLayout.setCenter(bpZoStage stage=new Stage();neReport);
		dataEntryPopUpStage=new Stage();
		
		Scene sc=new Scene(bpZoneReport);
		

		dataEntryPopUpStage.initModality(Modality.APPLICATION_MODAL);
		dataEntryPopUpStage.setScene(sc);
		//dataEntryPopUpStage.setTitle("Enter Dimensions for AWB no: "+ DataEntryController.awbno);
		dataEntryPopUpStage.setResizable(false);
		dataEntryPopUpStage.show();
		
	}		
	
// *****************************************************************************

	public void showClientVsZoneServicesReport(String sql,String client,String zone,String selected_Client_Zone) throws IOException, SQLException {


		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/ClientVsZone_ServiceReport.fxml"));
		BorderPane bpZoneReport = loader.load();
		ClientVsZone_ServiceController dataEntryController = (ClientVsZone_ServiceController) loader.getController();
		dataEntryController.setValuesForServicePieChart(sql, client, zone,selected_Client_Zone);
		// primaryStage.setMaximized(true);
		//borderLayout.setCenter(bpZoStage stage=new Stage();neReport);
		popUpWindowStage=new Stage();

		Scene sc=new Scene(bpZoneReport);

		popUpWindowStage.initModality(Modality.APPLICATION_MODAL);
		popUpWindowStage.setScene(sc);
		//dataEntryPopUpStage.setTitle("Enter Dimensions for AWB no: "+ DataEntryController.awbno);
		popUpWindowStage.setResizable(false);
		popUpWindowStage.show();

	}	
	
// *****************************************************************************

	public void showAddToPayForm() throws IOException, SQLException 
	{

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/ToPayDetails.fxml"));
		BorderPane bpToPay = loader.load();
		//ClientVsZone_ServiceController dataEntryController = (ClientVsZone_ServiceController) loader.getController();
		//dataEntryController.setValuesForServicePieChart(sql, client, zone,selected_Client_Zone);
		// primaryStage.setMaximized(true);
		//borderLayout.setCenter(bpZoStage stage=new Stage();neReport);
		popUpWindowStage=new Stage();

		Scene sc=new Scene(bpToPay);

		popUpWindowStage.initModality(Modality.APPLICATION_MODAL);
		popUpWindowStage.setScene(sc);
		popUpWindowStage.setTitle("TO PAY");
		popUpWindowStage.setResizable(false);
		popUpWindowStage.show();

	}		
	
// *****************************************************************************
	
	public void showForwarderDetails() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/forwarder/controller/ForwarderDetails.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
				
	}

// *****************************************************************************

	public void showAddOnExpenses() throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/forwarder/controller/AddOnExpense.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		addOnExpensesStage=new Stage();
		
		Scene sc=new Scene(bpZoneReport);
		

		addOnExpensesStage.initModality(Modality.APPLICATION_MODAL);
		addOnExpensesStage.setScene(sc);
		addOnExpensesStage.setTitle("Add On Expenses Form");
		addOnExpensesStage.setResizable(false);
		addOnExpensesStage.show();

	}	
	
	
// *****************************************************************************
	
	public void showSelectedVASItems() throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/SelectedVASItemPreview.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		selectedVASITemStage=new Stage();

		Scene sc=new Scene(bpZoneReport);

		selectedVASITemStage.initModality(Modality.APPLICATION_MODAL);
		selectedVASITemStage.setScene(sc);
		selectedVASITemStage.setTitle("Selected VAS items window");
		selectedVASITemStage.setResizable(false);
		selectedVASITemStage.show();

	}
	
	
// *****************************************************************************
	
	public void showClientVAS_InDataEntry() throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/vas/controller/ClientVAS_Popup.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		hyperLinkStage=new Stage();

		Scene sc=new Scene(bpZoneReport);

		hyperLinkStage.initModality(Modality.APPLICATION_MODAL);
		hyperLinkStage.initOwner(hyperLinkStage.getOwner());
		hyperLinkStage.setScene(sc);
		hyperLinkStage.setTitle("Add/Update Client VAS");
		hyperLinkStage.setResizable(false);
		
		hyperLinkStage.show();

	}	
	
	
// *****************************************************************************

	public void showSpotRate_InDataEntry() throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/SpotRatePopUp.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		hyperLinkStage=new Stage();

		Scene sc=new Scene(bpZoneReport);

		hyperLinkStage.initModality(Modality.APPLICATION_MODAL);
		hyperLinkStage.initOwner(hyperLinkStage.getOwner());
		hyperLinkStage.setScene(sc);
		hyperLinkStage.setTitle("Add/Update Client VAS");
		hyperLinkStage.setResizable(false);

		hyperLinkStage.show();

	}	

	
// *****************************************************************************
	
	public void showSearchPage() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/search/controller/SearchPage.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
		//alertbox();
		
	}
	
	
// *****************************************************************************

	public void showMultipleSelection() throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/forwarder/controller/SelectMultipeExpenses.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		multipleSelectStage=new Stage();
		
		Scene sc=new Scene(bpZoneReport);
		
		multipleSelectStage.initModality(Modality.APPLICATION_MODAL);
		multipleSelectStage.setScene(sc);
		multipleSelectStage.setTitle("Add On Expenses Form");
		multipleSelectStage.setResizable(false);
		multipleSelectStage.show();

	}
	
	
// *****************************************************************************
	
	public void showClientBooking() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/clientbooking/controller/ClientBooking.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}	
	
	
// *****************************************************************************
	
	public void showArea() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/location/controller/Area.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);

	}	
	
// *****************************************************************************
	
	public void showCity() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/location/controller/City.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	

// *****************************************************************************
	
	public void showState() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/location/controller/State.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
		}	
	
// *****************************************************************************
	
	public void showCountry() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/location/controller/Country.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
	
// *****************************************************************************
	
	public void showEmployeeRegistrationDetails() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/employee/controller/EmployeeRegistrationDetails.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
	
// *****************************************************************************
	
	public void showZone() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/zone/controller/Zone.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}
	
// *****************************************************************************
	
	public void showClient() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/client/controller/Client.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	

	
// *****************************************************************************
	
	public void showClientRate() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/ClientRate.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showSpotRate() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/SpotRate.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}		
	
// *****************************************************************************
	
	public void showImportClientRateExcel() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/ImportClientRateExcel.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}
	
	
// *****************************************************************************
	
	public void showImportForwarderRateExcel() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/ImportForwarderRateExcel.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	

// *****************************************************************************
	
	public void showNewImportClientRateExcel() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/NewImportClientRateExcel.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************
	
	public void showForwarderRate() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/ratemaster/controller/ForwarderRate.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************
	
	public void showClientVAS() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/vas/controller/ClientVAS.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}		
	
	
// *****************************************************************************
	
	public void showVAS() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/vas/controller/VAS.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}		


// *****************************************************************************
	
	public void showUploadPincodeExcel() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/pincode/controller/UploadPincodeExcel.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}
	
	
// *****************************************************************************

	public void showGenerateInvoice() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/invoice/controller/Invoice.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showToPayInvoice() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/invoice/controller/ToPayInvoice.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}		
	

// *****************************************************************************

	public void showGenerateInvoicePerforma() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/invoice/controller/InvoicePerforma.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showInvoiceDeleteReprint() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/invoice/controller/InvoiceDeleteReprint.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}		

// *****************************************************************************

	public void showComplaintRegister() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/complaint/controller/ComplaintRegister.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}

// *****************************************************************************

	public void showAutoUpdateDeliveryStatus() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/administration/controller/AutoUpdateDeliveryStatus.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}
	
	
	// *****************************************************************************

	public void showManualUpdateDeliveryStatus() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/administration/controller/ManualUpdateDeliveryStatus.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showAddClient() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/master/addclient/controller/AddClient.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showClientVsZoneReport() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/ClientVsZoneReport.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************

	public void showStackedBarChartViaClientVsZone() throws IOException {
		
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/StackBarChart.fxml"));
		BorderPane bpToPay = loader.load();
		//ClientVsZone_ServiceController dataEntryController = (ClientVsZone_ServiceController) loader.getController();
		//dataEntryController.setValuesForServicePieChart(sql, client, zone,selected_Client_Zone);
		// primaryStage.setMaximized(true);
		//borderLayout.setCenter(bpZoStage stage=new Stage();neReport);
		popUpWindowStage=new Stage();

		Scene sc=new Scene(bpToPay);

		popUpWindowStage.initModality(Modality.APPLICATION_MODAL);
		popUpWindowStage.setScene(sc);
		popUpWindowStage.setTitle("Stack Bar Chart");
		//popUpWindowStage.setResizable(false);
		popUpWindowStage.show();
		
		/*FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/StackBarChart.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);*/
	}	
	

			
	
// *****************************************************************************

	public void showStackedBarChart() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/StackBarChart.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}
	
// *****************************************************************************

	public void showPaymentReceiving() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/payment/controller/PaymentReceived.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	

// *****************************************************************************	
	
	public void showInvoicePaymentReport() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/InvoicePayment.fxml"));
		BorderPane bpZoneReport = loader.load();
		// primaryStage.setMaximized(true);
		borderLayout.setCenter(bpZoneReport);
	}	
	
// *****************************************************************************	

	public void showInvoiceVsClientReport() throws IOException, SQLException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graph/controller/InvoicePaymentStackBarChart.fxml"));
		BorderPane bpZoneReport = loader.load();

		popUpWindowStage=new Stage();

		Scene sc=new Scene(bpZoneReport);

		popUpWindowStage.initModality(Modality.APPLICATION_MODAL);
		popUpWindowStage.setScene(sc);
		//dataEntryPopUpStage.setTitle("Enter Dimensions for AWB no: "+ DataEntryController.awbno);
		popUpWindowStage.setResizable(false);
		popUpWindowStage.show();

	}


		
	

// *****************************************************************************	

public static void main(String[] args) {
		launch(args);
	}
}
