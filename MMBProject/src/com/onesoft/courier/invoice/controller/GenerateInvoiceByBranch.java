package com.onesoft.courier.invoice.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.invoice.bean.InvoiceBean;

public class GenerateInvoiceByBranch {
	
	public static String branchcode=null;
			
	
	public void getInvoiceByBranch(String branchInvoice) throws SQLException
	{
		System.out.println("Generate Invoice class running ..."); 
		System.out.println("Invoice no is: "+ branchInvoice);
		
		branchcode=branchInvoice;
		getLatestInvoiceNo();
	
	}
	
	
	public String getLatestInvoiceNo() throws SQLException
	{
		System.out.println("From Latest Invoice method...!! Invoice no is: "+ branchcode);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		InvoiceBean inbean=new InvoiceBean();
		
		
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select invoice_number,invoice2branchcode from invoice where invoice2branchcode='"+branchcode+"' order by invoice_number DESC limit 1";
			rs=st.executeQuery(sql);
			System.out.println(sql);
	
			//System.out.println("row count: "+rs.getRow());
			
			if(!rs.next())
			{
				System.out.println("No value: ");
				//txtInvoiceNo.setEditable(true);
				
			}
			else
			{
				
				inbean.setInvoice_number(rs.getString("invoice_number"));
				
			
				
				//inbean.setInvoice_start(rs.getString("invoice_start_no"));
				//inbean.setInvoice_end(rs.getLong("invoice_end_no"));
				
				//System.out.println("Latest invoice number is: "+ inbean.getInvoice_number());
				
				if(inbean.getInvoice_number().matches("[a-zA-Z0-9]+"))
				{
					inbean.setInvoice_start(inbean.getInvoice_number().replaceAll("[^A-Za-z]+", ""));
					inbean.setInvoice_end(Long.parseLong(inbean.getInvoice_number().replaceAll("\\D+","")));
					inbean.setInvoice_number(inbean.getInvoice_start()+(inbean.getInvoice_end()+1));
					//inbean.setInvoice_number("JKJ");
					System.out.println("Latest invoice number is: number is alphnumeric "+ inbean.getInvoice_number());
				}
				else
				{
					System.out.println("Latest invoice number is: number is not alphnumeric "+ inbean.getInvoice_number());
				}
				
				//txtInvoiceNo.setText(inbean.getInvoice_number());
				//txtInvoiceNo.setEditable(false);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return inbean.getInvoice_number();
		
	}

}
