package com.onesoft.courier.invoice.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.ConvertAmountInWords;
import com.onesoft.courier.common.GenerateSaveReport;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCompany;
import com.onesoft.courier.common.LoadGST_Rates;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadCompanyBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.invoice.bean.InvoiceBean;
import com.onesoft.courier.invoice.bean.InvoiceBillJavaFXBean;
import com.onesoft.courier.invoice.bean.InvoiceTableBean;
import com.onesoft.courier.invoice.utils.InvoiceUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.sf.jasperreports.engine.JRException;

public class InvoicePerformaController implements Initializable{

	private ObservableList<InvoiceTableBean> detailedtabledata=FXCollections.observableArrayList();
	private ObservableList<InvoiceBillJavaFXBean> invoicetabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSubClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxDiscountAddtitionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.INVOICEDISCOUNT,InvoiceUtils.INVOICEADDITIONAL);
	//private ObservableList<String> comboBoxTypeAdditionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.a);
	private ObservableList<String> comboBoxTypeFlatPercentItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.TYPE1,InvoiceUtils.TYPE2);
	
	InvoiceBean invcBean=new InvoiceBean();
	
	List<String> awbNoToInoviceGeneratedList=new ArrayList<>();
	List<InvoiceBean> list_CompleteInvoiceDetails=new ArrayList<>();
	List<InvoiceBean> list_InvoiceAndCompanyDetails=new ArrayList<>();
	List<String> list_ClientsViaBranchAndDateRange=new ArrayList<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	
	public double cgst_amount=0;
	public double sgst_amount=0;
	public double igst_amount=0;
	public double cgst_rate=0;
	public double sgst_rate=0;
	public double igst_rate=0;
	
	public String invoiceNumberGenerated=null;
	public String invoiceNumberToPreview=null;
	public String branchCodeToGetCompanyDetail=null;
	public String summarySQL=null;
	public int awbCountToInvoicePrint=0;
	//public String summarySQL_part2=null;
	
	public double gst_amount_old=0;
	
	public String branch_Code_If_All_Branch_selected=null;
	public String branch_City_If_All_Branch_selected=null;
	public String branch_City=null;
	public String client_City=null;
	
	public String branch_State=null;
	public String client_State=null;
	
	Set<String> awb=new HashSet<String>();
	
	public String checkBoxStatus;
	public String src = "E:\\NewEclipseWorkspace_2017\\MMBProject\\src\\com\\onesoft\\courier\\invoice\\controller\\Invoice.jrxml";
	Map<String , Object> parameters = new HashMap<>();
	
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private Label labSummaryNetwork;
	
	@FXML
	private Label labSummaryService;
	
	@FXML
	private Label labSummaryDoxNonDox;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnPrint;
	
	@FXML
	private TextField txtInvoiceNo;
	
	@FXML
	private TextField txtamount;
	
	/*@FXML
	private TextField txtCOD;
	
	@FXML
	private TextField txtSub_Client;
	
	@FXML
	private TextField txtFOD;
	
	@FXML
	private TextField txtDocket;*/
	
	@FXML
	private TextField txtFuel;
	
	@FXML
	private TextField txtVasTotal_Amt;
	
	@FXML
	private TextField txtInsurance;
	
	@FXML
	private TextField txtSubTotal;
	
	@FXML
	private TextField txtReason;
	
	@FXML
	private TextField txtTypeAmount;
	
	@FXML
	private TextField txtDiscountAddtitionalAmt;
	
	@FXML
	private TextField txtGST;
	
	@FXML
	private TextField txtGrandTotal;
	
	@FXML
	private TextField txtremarks;
	
	@FXML
	private TextField txtFrom;
	
	@FXML
	private TextField txtTo;
	
	@FXML
	private TextField txtNew;
	
	@FXML
	private TextField txtdiscountLessAmt;
	
	@FXML
	private CheckBox chkExportToExcel;
	
	@FXML
	private CheckBox chkFilter;
	
	@FXML
	private CheckBox chkShowDue;
	
	@FXML
	private CheckBox chkBankDetails;
	
	@FXML
	private CheckBox chkGST;
	
	/*@FXML
	private CheckBox chkPCS;
	
	@FXML
	private CheckBox chkD_N;
	
	@FXML
	private CheckBox chkLogo;
	
	@FXML
	private CheckBox chkStatus;*/
	
	@FXML
	private CheckBox chkBill;
	
	@FXML
	private ComboBox<String> comboBoxBranchCode;
	
	@FXML
	private ComboBox<String> comboBoxClientCode;
	
	/*@FXML
	private ComboBox<String> comboBoxSubClient;*/
	
	@FXML
	private ComboBox<String> comboBoxDisAdd;
	
	@FXML
	private ComboBox<String> comboBoxTypeFlatPercent;
	
	@FXML
	private DatePicker dpkInvoiceDate;
	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	

//================================================================

	@FXML
	private TableView<InvoiceBillJavaFXBean> invoiceTable;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Integer> invc_Col_serialno;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_InvoiceDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_InvoiceNo;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_ClientCode;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_FromDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_ToDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_BasicAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_Fuel;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_VasAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_InsuranceAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_TaxableAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_GSTAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_GrandTotal;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_Remarks;
	
	
//================================================================	
	
	@FXML
	private TableView<InvoiceTableBean> detailedtable;
	
	@FXML
	private TableColumn<InvoiceTableBean, Integer> serialno;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> awb_no;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> bookingdate;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> branch;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> clientcode;
	
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> billingweight;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> amount;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> insuracneamount;
	
	@FXML
	private TableColumn<InvoiceTableBean, Integer> pcs;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> docketcharge;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> otherVas;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> fuel;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> total;
	
	public String updateinvoice=null;
	
	
// ====================================================================================================================
	
	Main m=new Main();
	
	
// ====== Check invoice replaced by getInvoiceByBranch and getLatestInvoiceNo() from GenerateInvoiceByBranch class =============	
	
	/*public void checkInvoice() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		InvoiceBean inbean=new InvoiceBean();
		
		
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select * from invoiceseries order by invoice_end_no DESC limit 1";
			rs=st.executeQuery(sql);
	
			//System.out.println("row count: "+rs.getRow());
			
			if(!rs.next())
			{
				//System.out.println("No value: ");
				txtInvoiceNo.setEditable(true);
				
			}
			else
			{
				inbean.setInvoice_start(rs.getString("invoice_start_no"));
				inbean.setInvoice_end(rs.getLong("invoice_end_no"));
				inbean.setInvoice_number(inbean.getInvoice_start()+(inbean.getInvoice_end()+1));
				invcBean.setInvoice_number(inbean.getInvoice_number());
				//txtInvoiceNo.setText(inbean.getInvoice_number());
				//txtInvoiceNo.setEditable(false);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
	
// ====================================================================================================================
	
	public void loadGSTRates_OnDateChange() throws SQLException
	{
		
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
		
		if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
		{
			System.out.println("CGST: "+LoadGST_Rates.cgst_rate_fix+" | SGST: "+LoadGST_Rates.sgst_rate_fix+" | IGST: "+LoadGST_Rates.igst_rate_fix);	
		}
		else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
		{
			LoadGST_Rates loadGST=new LoadGST_Rates();
			loadGST.loadGST_OnChangeDate(invoiceDate);
			//System.out.println(invoiceDate+" is Less than Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
		}
		
		else 
		{
			System.out.println("CGST: "+LoadGST_Rates.cgst_rate_fix+" | SGST: "+LoadGST_Rates.sgst_rate_fix+" | IGST: "+LoadGST_Rates.igst_rate_fix);
		}
		
	}
	
	
// ====================================================================================================================	

	public void getInvoiceSeries() throws IOException, SQLException, NumberFormatException
	{
		generatePerformaInvoice();
	}
	
// ====================================================================================================================	

	public void generatePerformaInvoice() throws SQLException
	{
		LocalDate indate=dpkInvoiceDate.getValue();
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date invoicedate=Date.valueOf(indate);
		Date fromdate=Date.valueOf(from);
		Date todate=Date.valueOf(to);
		
		InvoiceBean inbean=new InvoiceBean();
		
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
				
		inbean.setInvoice_number("Performa Invoice");
		inbean.setInvoice2branchcode(branchcode[0]);
		inbean.setClientcode(clientcode[0]);
		inbean.setAmountAfterDis_Add(Double.parseDouble(txtDiscountAddtitionalAmt.getText()));
						
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
		{
			inbean.setDiscount_additional(comboBoxDisAdd.getValue());
		}
		else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
		{
			inbean.setDiscount_additional(comboBoxDisAdd.getValue());
		}

	// ******************

		if(!txtReason.getText().equals(null) && !txtReason.getText().isEmpty())
		{
			inbean.setReason(txtReason.getText());
		}
		else
		{

		}

	// ******************

		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			inbean.setType_flat2percent("P");
		}
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
		{
			inbean.setType_flat2percent("F");
		}

	// ******************

		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2) && !txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty())
		{
			inbean.setRateof_percent(0.0);
		}
		else if(!txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty()) 
		{
			inbean.setRateof_percent(Double.parseDouble(txtTypeAmount.getText()));
		}
		else
		{
			inbean.setRateof_percent(0.0);
		}

	// ******************	

		if(!txtdiscountLessAmt.getText().equals(null) && !txtdiscountLessAmt.getText().isEmpty())
		{
			inbean.setDiscount_Additional_amount(Double.parseDouble(txtdiscountLessAmt.getText()));
		}
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
		{
			inbean.setDiscount_Additional_amount(Double.parseDouble(txtTypeAmount.getText()));
		}
		else
		{
			inbean.setDiscount_Additional_amount(0.0);
		}

	// ******************

		inbean.setFuel(Double.parseDouble(txtFuel.getText()));
		inbean.setVas_amount(Double.valueOf(txtVasTotal_Amt.getText()));

		inbean.setInsurance_amount(Double.parseDouble(txtInsurance.getText()));
		inbean.setBasicAmount(Double.valueOf(txtamount.getText()));

		inbean.setCgst_Name("CGST");
		inbean.setCgst_rate(cgst_rate);
		inbean.setCgst_amt(cgst_amount);

		inbean.setSgst_Name("SGST");
		inbean.setSgst_rate(sgst_rate);
		inbean.setSgst_amt(sgst_amount);

		inbean.setIgst_Name("IGST");
		inbean.setIgst_rate(igst_rate);
		inbean.setIgst_amt(igst_amount);
		inbean.setGst_amount(inbean.getCgst_amt()+inbean.getSgst_amt()+inbean.getIgst_amt());
		inbean.setTaxable_amount(Double.valueOf(txtDiscountAddtitionalAmt.getText()));


		inbean.setGst(Double.parseDouble(txtGST.getText()));
		inbean.setGrandtotal(Double.parseDouble(txtGrandTotal.getText()));
		inbean.setRemarks(txtremarks.getText());
		inbean.setStatus("Active");
		inbean.setFrom_date(date.format(fromdate));
		inbean.setTo_date(date.format(todate));
		inbean.setInvoice_date(date.format(invoicedate));

		list_CompleteInvoiceDetails.add(inbean);

		System.err.println("Invoice No: "+inbean.getInvoice_number());
		System.err.println("Invoice Date: "+invoicedate);
		System.err.println("Client Code: "+inbean.getClientcode());
		System.err.println("Amount After Dis_Add: "+inbean.getAmountAfterDis_Add());
		System.err.println("Ins Amt: "+inbean.getInsurance_amount());
		System.err.println("Dis_Add: "+inbean.getDiscount_additional());
		System.err.println("Reason: "+inbean.getReason());
		System.err.println("Type Flat_%: "+inbean.getType_flat2percent());
		System.err.println("Rate of %: "+inbean.getRateof_percent());
		System.err.println("VAS Amt: "+inbean.getVas_amount());
		System.err.println("Dis_Add_amt: "+inbean.getDiscount_Additional_amount());
		System.err.println("Fuel: "+inbean.getFuel());
		System.err.println("From date: "+fromdate);
		System.err.println("To date: "+todate);

		System.err.println("Cgst Rate: "+inbean.getCgst_rate());
		System.err.println("Cgst Amt: "+inbean.getCgst_amt());
		System.err.println("Sgst Rate: "+inbean.getSgst_rate());
		System.err.println("Sgst Amt: "+inbean.getSgst_amt());
		System.err.println("Igst Rate: "+inbean.getIgst_rate());
		System.err.println("Igst amt: "+inbean.getIgst_amt());

		//System.err.println("Invoice No: "+inbean.getGst());
		System.err.println("Grand Total: "+inbean.getGrandtotal());
		System.err.println("Remark: "+inbean.getRemarks());
		System.err.println("Status: "+inbean.getStatus());
	
		branchCodeToGetCompanyDetail=inbean.getInvoice2branchcode();
		
		
		try 
		{
			showInvoice_CompanyDetails();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		catch (JRException e) 
		{
			e.printStackTrace();
		}
		
	}

// ====================================================================================================================	

	public void showInvoice_CompanyDetails() throws IOException, JRException, SQLException
	{
		ConvertAmountInWords cnvrtWrd=new ConvertAmountInWords();
		
		for(InvoiceBean inBean: list_CompleteInvoiceDetails)
		{
			InvoiceBean generateInvoiceBean=new InvoiceBean();

			String clientNameAddressPincodeGSTIN=getClientNameViaClientCode(inBean.getClientcode());
			String[] name_Address_pincode_GSTIN=clientNameAddressPincodeGSTIN.split("\\|");

			String ClientStateCode_Name_GstinCode=getStateNameViaClientCode(name_Address_pincode_GSTIN[2].trim());
			String[] StateCode_Name_GstinCode=ClientStateCode_Name_GstinCode.split("\\|");

			generateInvoiceBean.setInvoice_number(inBean.getInvoice_number());
			generateInvoiceBean.setInvoice_date(inBean.getInvoice_date());
			generateInvoiceBean.setInvoice2branchcode(inBean.getInvoice2branchcode());
			generateInvoiceBean.setClientcode(inBean.getClientcode());
			generateInvoiceBean.setClientname(name_Address_pincode_GSTIN[0].trim());
			generateInvoiceBean.setClientAddress(name_Address_pincode_GSTIN[1].trim());
			generateInvoiceBean.setClientGSTIN(name_Address_pincode_GSTIN[3].trim());
			generateInvoiceBean.setClientStateName(StateCode_Name_GstinCode[1].trim());
			generateInvoiceBean.setClient_StateGSTIN_code(StateCode_Name_GstinCode[2].trim());
			generateInvoiceBean.setFrom_date(inBean.getFrom_date());
			generateInvoiceBean.setTo_date(inBean.getTo_date());
			generateInvoiceBean.setBasicAmount(inBean.getBasicAmount());
			generateInvoiceBean.setFuel_rate(inBean.getFuel_rate());
			generateInvoiceBean.setFuel(inBean.getFuel());
			generateInvoiceBean.setVas_amount(inBean.getVas_amount());
			generateInvoiceBean.setInsurance_amount(inBean.getInsurance_amount());
			generateInvoiceBean.setType_flat2percent(inBean.getType_flat2percent());
			generateInvoiceBean.setDiscount_additional(inBean.getDiscount_additional());
			generateInvoiceBean.setRateof_percent(inBean.getRateof_percent());
			generateInvoiceBean.setDiscount_Additional_amount(inBean.getDiscount_Additional_amount());
			generateInvoiceBean.setReason(inBean.getReason());
			generateInvoiceBean.setCgst_Name(inBean.getCgst_Name());
			generateInvoiceBean.setCgst_rate(inBean.getCgst_rate());
			generateInvoiceBean.setCgst_amt(inBean.getCgst_amt());
			generateInvoiceBean.setSgst_Name(inBean.getSgst_Name());
			generateInvoiceBean.setSgst_rate(inBean.getSgst_rate());
			generateInvoiceBean.setSgst_amt(inBean.getSgst_amt());
			generateInvoiceBean.setIgst_Name(inBean.getIgst_Name());
			generateInvoiceBean.setIgst_rate(inBean.getIgst_rate());
			generateInvoiceBean.setIgst_amt(inBean.getIgst_amt());
			generateInvoiceBean.setTaxable_amount(inBean.getTaxable_amount());
			generateInvoiceBean.setGst_amount(inBean.getGst_amount());
			generateInvoiceBean.setGrandtotal(inBean.getGrandtotal());
			generateInvoiceBean.setAmountInWords(cnvrtWrd.convertToWords(generateInvoiceBean.getGrandtotal().intValue()));
			generateInvoiceBean.setAwbCount(awbCountToInvoicePrint);

			generateInvoiceBean.setRemarks(inBean.getRemarks());
			generateInvoiceBean.setCompanyCode(getCompanyViaBranch(inBean.getInvoice2branchcode()));

			for(LoadCompanyBean cmpnyBean:LoadCompany.SET_LOAD_COMPANYWITHALLDETAILS)
			{
				if(generateInvoiceBean.getCompanyCode().equals(cmpnyBean.getCompanyCode()))
				{
					generateInvoiceBean.setCompanyName(cmpnyBean.getCompanyName());
					//generateInvoiceBean.setCompanyCode(cmpnyBean.getCompanyCode());
					generateInvoiceBean.setCmpny_tagLine(cmpnyBean.getTagLine());
					generateInvoiceBean.setCmpny_address(cmpnyBean.getAddress());
					generateInvoiceBean.setCmpny_city(cmpnyBean.getCity());
					generateInvoiceBean.setCmpny_serviceTaxNo(cmpnyBean.getServiceTaxNo());
					generateInvoiceBean.setCmpny_panNo(cmpnyBean.getPanNo());
					generateInvoiceBean.setCmpny_email(cmpnyBean.getEmail());
					generateInvoiceBean.setCmpny_bankName(cmpnyBean.getBankName());
					generateInvoiceBean.setCmpny_accountNo(cmpnyBean.getAccountNo());
					generateInvoiceBean.setCmpny_ifscCode(cmpnyBean.getIfscCode());

					generateInvoiceBean.setCondition1(cmpnyBean.getCondition1());
					generateInvoiceBean.setCondition2(cmpnyBean.getCondition2());
					generateInvoiceBean.setCondition3(cmpnyBean.getCondition3());
					generateInvoiceBean.setCondition4(cmpnyBean.getCondition4());
					generateInvoiceBean.setCondition5(cmpnyBean.getCondition5());
					generateInvoiceBean.setCondition6(cmpnyBean.getCondition6());

					generateInvoiceBean.setCmpny_state(cmpnyBean.getState());
					generateInvoiceBean.setCmpny_pincode(cmpnyBean.getPincode());

					list_InvoiceAndCompanyDetails.add(generateInvoiceBean);
					break;

				}
			}
		}
		
		
		System.out.println("New List size >>>> "+list_InvoiceAndCompanyDetails.size());
		
		/*for(InvoiceBean inBean: list_InvoiceAndCompanyDetails)
		{
			System.out.println("Invoice No.: "+inBean.getInvoice_number());
			System.out.println("Invoice Date: "+inBean.getInvoice_date());
			System.out.println("Branch: "+inBean.getInvoice2branchcode());
			System.out.println("Client Code: "+inBean.getClientcode());
			System.out.println("From Date: "+inBean.getFrom_date());
			System.out.println("To Date: "+inBean.getTo_date());
			System.out.println("Basic Amount: "+inBean.getBasicAmount());
			System.out.println("Fuel Rate: "+inBean.getFuel_rate());
			System.out.println("Fuel: "+inBean.getFuel());
			System.out.println("VAS Amount: "+inBean.getVas_amount());
			System.out.println("Insurance Amount: "+inBean.getInsurance_amount());
			System.out.println("Rate of Percent: "+inBean.getRateof_percent());
			System.out.println("Type Flat/Percent: "+inBean.getType_flat2percent());
			System.out.println("Dis/Add: "+inBean.getDiscount_additional());
			System.out.println("Dis/Add Amount: "+inBean.getDiscount_Additional_amount());
			System.out.println("Reason: "+inBean.getReason());
			System.out.println("Tax Name 2: "+inBean.getCgst_Name());
			System.out.println("CGST Rate: "+inBean.getCgst_rate());
			System.out.println("CGST Amount: "+inBean.getCgst_amt());
			System.out.println("Tax Name 2: "+inBean.getSgst_Name());
			System.out.println("SGST Rate: "+inBean.getSgst_rate());
			System.out.println("SGST Amount: "+inBean.getSgst_amt());
			System.out.println("Tax Name3: "+inBean.getIgst_Name());
			System.out.println("IGST Rate: "+inBean.getIgst_rate());
			System.out.println("IGST Amount: "+inBean.getIgst_amt());
			System.out.println("Taxable Amount: "+inBean.getTaxable_amount());
			System.out.println("GST Amount: "+inBean.getGst_amount());
			System.out.println("Grand Total: "+inBean.getGrandtotal());
			System.out.println("Amount In Words: "+inBean.getAmountInWords());
			System.out.println("Remarks: "+inBean.getRemarks());
			
			
			System.out.println("\n*************** Company Details ****************\n");
			
			System.out.println("Company Code: "+inBean.getCompanyCode());
			System.out.println("Company Name: "+inBean.getCompanyName());
			System.out.println("Tag Line: "+inBean.getCmpny_tagLine());
			System.out.println("Address: "+inBean.getCmpny_address());
			System.out.println("Tax No: "+inBean.getCmpny_serviceTaxNo());
			System.out.println("Pan No: "+inBean.getCmpny_panNo());
			System.out.println("Email: "+inBean.getCmpny_email());
			System.out.println("Bank Name: "+inBean.getCmpny_bankName());
			System.out.println("A/c No: "+inBean.getCmpny_accountNo());
			System.out.println("IFSC Code: "+inBean.getCmpny_ifscCode());
			System.out.println("Condi. 1: "+inBean.getCondition1());
			System.out.println("Condi. 2: "+inBean.getCondition2());
			System.out.println("Condi. 3: "+inBean.getCondition3());
			System.out.println("Condi. 4: "+inBean.getCondition4());
			System.out.println("Condi. 5: "+inBean.getCondition5());
			System.out.println("Condi. 6: "+inBean.getCondition6());
			System.out.println("Company State: "+inBean.getCmpny_state());
			System.out.println("Company Pincode: "+inBean.getCmpny_pincode());
			
			break;
		}*/
		
		showInvoiceInJasperViewer();
		
	}

// ====================================================================================================================
	
	public void showInvoiceInJasperViewer() throws IOException, JRException, SQLException
	{
		for(InvoiceBean inBean: list_InvoiceAndCompanyDetails) 
		{
			parameters.put("condition1", inBean.getCondition1());
			parameters.put("condition2", inBean.getCondition2());
			parameters.put("condition3", inBean.getCondition3());
			parameters.put("condition4", inBean.getCondition4());
			parameters.put("condition5", inBean.getCondition5());
			parameters.put("condition6", inBean.getCondition6());
			parameters.put("panNumber", inBean.getCmpny_panNo());
			parameters.put("tagLine", inBean.getCmpny_tagLine());
			parameters.put("address", inBean.getCmpny_address());
			parameters.put("email", inBean.getCmpny_email());
			parameters.put("grandTotal", inBean.getGrandtotal());
			parameters.put("amountInWords", inBean.getAmountInWords());
			parameters.put("type", "");
			parameters.put("value", 0.0);
			parameters.put("fuelTaxName", "Fuel");
			parameters.put("CGSTName", "CGST @"+df.format(inBean.getCgst_rate()));
			parameters.put("SGSTName", "SGST @"+df.format(inBean.getSgst_rate()));
			parameters.put("IGSTName", "IGST @"+df.format(inBean.getIgst_rate()));
			parameters.put("fuel", inBean.getFuel());
			parameters.put("CGST", inBean.getCgst_amt());
			parameters.put("SGST", inBean.getSgst_amt());
			parameters.put("IGST", inBean.getIgst_amt());
			parameters.put("amount", inBean.getBasicAmount());
			parameters.put("name", inBean.getCompanyName());
			parameters.put("taxNumber", inBean.getCmpny_serviceTaxNo());
			parameters.put("bankName", inBean.getCmpny_bankName());
			parameters.put("accountNumber", inBean.getCmpny_accountNo());
			parameters.put("IFSCCode", inBean.getCmpny_ifscCode());
			parameters.put("taxableValue", inBean.getTaxable_amount());
			parameters.put("saccode", "");
			parameters.put("compState", inBean.getCmpny_state());
			parameters.put("compStateCode", inBean.getCmpny_pincode());
			parameters.put("InvoiceNumber", inBean.getInvoice_number());
			parameters.put("dateFrom", inBean.getFrom_date());
			parameters.put("dateTo", inBean.getTo_date());
			parameters.put("InvoiceDate", inBean.getInvoice_date());
			
			parameters.put("clientName", inBean.getClientname());
			parameters.put("clientCode", inBean.getClientcode());
			parameters.put("clientAddress", inBean.getClientAddress());
			parameters.put("subClientCode", "");
			parameters.put("awbCount", inBean.getAwbCount());
			parameters.put("supplyCity", inBean.getCmpny_city());
			
			parameters.put("clientState", inBean.getClientStateName());
			parameters.put("clientStateCode", inBean.getClient_StateGSTIN_code());
			parameters.put("GSTIN", inBean.getClientGSTIN());
			
			
			LocalDate localFromDate=LocalDate.parse(inBean.getFrom_date(), localdateformatter);
			Date fromDate=Date.valueOf(localFromDate);
			LocalDate loacalToDate=LocalDate.parse(inBean.getTo_date(), localdateformatter);
			Date toDate=Date.valueOf(loacalToDate);
			
			summarySQL="select air_way_bill_number,master_awbno,booking_date,dailybookingtransaction2city,billing_weight,packets,amount,"
					+ "dox_nondox from dailybookingtransaction where booking_date between '"+fromDate+"' and '"+toDate+"' and "
					+ " dailybookingtransaction2client='"+inBean.getClientcode()+"' and dailybookingtransaction2branch='"+inBean.getInvoice2branchcode()+"' and invoice_number IS NULL and mps_type='T' and travel_status='dataentry'";
			
			System.out.println(">>>>>>>>>>>>> "+summarySQL);
			break;
		}				
		
		new GenerateSaveReport().exportReport(summarySQL,checkBoxStatus = String.valueOf(chkExportToExcel.isSelected()), src, parameters);
		
		comboBoxClientCode.requestFocus();
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		comboBoxClientCode.getSelectionModel().clearSelection();
		txtamount.clear();
		txtFuel.clear();
		txtGrandTotal.clear();
		txtSubTotal.clear();
		txtDiscountAddtitionalAmt.clear();
		txtdiscountLessAmt.clear();
		txtGST.clear();
		txtInsurance.clear();
		txtReason.clear();
		txtTypeAmount.clear();
		txtVasTotal_Amt.clear();
		comboBoxDisAdd.setDisable(true);
		list_InvoiceAndCompanyDetails.clear();
		list_CompleteInvoiceDetails.clear();
		invoiceNumberToPreview=null;
		invoiceNumberGenerated=null;
		
		
		cgst_amount=0;
		sgst_amount=0;
		igst_amount=0;
		cgst_rate=0;
		sgst_rate=0;
		igst_rate=0;
		
		invoiceNumberGenerated=null;
		invoiceNumberToPreview=null;
		summarySQL=null;
		
		awbCountToInvoicePrint=0;
		chkBill.setSelected(false);
		detailedtabledata.clear();
		
		
		//public String summarySQL_part2=null;
		
				
	}
	
// ====================================================================================================================
	
	public String getCompanyViaBranch(String branchName)
	{
		String cmpnyCode=null;
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{	
			if(branchName.equals(branchBean.getBranchCode()))
			{
				cmpnyCode=branchBean.getCmpnyCode();
				break;
			}
		}
		return cmpnyCode;
	}
	
// ====================================================================================================================
	
	public String getClientNameViaClientCode(String clientcode)
	{
		String clientName_Address_pincode_gstinNo=null;
		System.out.println("Client code 1 >>>>>>> "+clientcode);
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientcode.equals(clBean.getClientCode()))
			{
				clientName_Address_pincode_gstinNo=clBean.getClientName()+" | "+clBean.getAddress()+" | "+clBean.getPincode()+" | "+clBean.getClient_gstin_number();
				break;
			}
		}
		return clientName_Address_pincode_gstinNo;
	}
	
// ====================================================================================================================
	
	public String getStateNameViaClientCode(String pincode)
	{
		String stateName=null;
		String stateCode=null;
		String state_gstin_code=null;
		
		
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pincode.equals(pinBean.getPincode()))
			{
				stateCode=pinBean.getState_code();
				state_gstin_code=pinBean.getState_gstin_code();
				break;
			}
		}
		
		for(LoadCityBean stateBean: LoadState.SET_LOAD_STATEWITHNAME)
		{
			if(stateCode.equals(stateBean.getStateCode()))
			{
				stateName=stateBean.getStateName();
				break;
			}
		}
		return stateCode+" | "+stateName+" | "+state_gstin_code;
	}	

	
// ====================================================================================================================
	
	public void rangeFilter() throws SQLException
	{
		if(chkFilter.isSelected()==true)
		{
			txtFrom.setDisable(false);
			txtTo.setDisable(false);
			txtNew.setDisable(false);
		}
		else
		{
			txtFrom.clear();
			txtTo.clear();
			txtNew.clear();
			
			txtFrom.setDisable(true);
			txtTo.setDisable(true);
			txtNew.setDisable(true);
			//showWithoutInvoiceTable(updateinvoice);
		}
	}
	
	
// ====================================================================================================================

	public void loadBranch() throws SQLException
	{	
		new	LoadBranch().loadBranchWithName();
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			System.out.println("Company from branch: >> "+branchBean.getCmpnyCode());
			
			comboBoxBranchItems.add(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());
		}
		comboBoxBranchCode.setItems(comboBoxBranchItems);

	}
	
	
// ====================================================================================================================

	public void loadClients() throws SQLException
	{
		String[] branchCode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		
		if(txtInvoiceNo.getText()!=null)
		{
			txtInvoiceNo.setEditable(false);
		}else
		{
			txtInvoiceNo.setEditable(true);
		}
		loadInvoiceDate();
		
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
		Set<String> setclient = new HashSet<String>();
		System.out.println("Before Try >>>");
		
		try
		{	
			if(!branchCode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
			{
				comboBoxClientCode.getItems().clear();
				sql="select dbt.dailybookingtransaction2client,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and dbt.dailybookingtransaction2Branch='"+branchCode[0]+"' and invoice_number IS NULL and dbt.mps_type='T' and booking_date between '"+stdate+"' and '"+edate+"' group by dbt.dailybookingtransaction2client,cm.name";
			}
			else
			{
				sql="select cm.code,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code and travel_status='dataentry'  and booking_date between '"+stdate+"' and '"+edate+"' group by dbt.dailybookingtransaction2client,cm.name";
			}
			
			setclient.clear();
			 
			st=con.createStatement();
			rs=st.executeQuery(sql);
			System.out.println("Branch Sql: "+sql);
			
			if(!rs.next())
			{
				if(branchCode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
				{
					
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Database Alert");
					alert.setHeaderText(null);
					alert.setContentText("No match found");
					alert.showAndWait();
				
					comboBoxBranchCode.requestFocus();
					comboBoxClientCode.getItems().clear();
				}
			}
			else
			{
				comboBoxClientCode.setPromptText("select client");
				
				do
				{
					setclient.add(rs.getString(1)+" | "+ rs.getString(2));
				}
				while(rs.next());
			
			comboBoxClientCode.getItems().clear();
			
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClientCode.setItems(comboBoxClientItems);
			
			dpkFromDate.setEditable(false);
			
			dpkFromDate.setOnMouseClicked(e -> {
				 if(comboBoxClientItems.isEmpty()==false)
			        	dpkFromDate.hide();
			    });
			
			dpkToDate.setEditable(false);
			
			dpkToDate.setOnMouseClicked(e -> {
				 if(comboBoxClientItems.isEmpty()==false)
			        	dpkToDate.hide();
			    });
			
			comboBoxBranchCode.setOnMouseClicked(e -> {
		        if(comboBoxClientItems.isEmpty()==false)
		        	comboBoxBranchCode.hide();
		    });
			
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
				
	}
	
	
// ====================================================================================================================
	
	public void showDetails() throws SQLException
	{
		String[] clientcode=null;
		
		if(comboBoxClientCode.getValue()!=null)
		{
			String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		
			LocalDate gstEffectiveDate=dpkInvoiceDate.getValue();
			LocalDate from=dpkFromDate.getValue();
			LocalDate to=dpkToDate.getValue();
			Date stdate=Date.valueOf(from);
			Date edate=Date.valueOf(to);
			Date dateForGST_Rates=Date.valueOf(gstEffectiveDate);
			//String joinsql="";
			String sql="";
			String branchValue="";
			String clientSubClient="";
				
			try
			{
				if(!branchcode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
				{
					branchValue=" dbt.dailybookingtransaction2branch='"+branchcode[0]+"' and ";
				}
				
				if(!comboBoxClientCode.getValue().equals(null))
				{
					clientSubClient="dbt.dailybookingtransaction2client='"+clientcode[0]+"' and ";
				}
				
				sql=branchValue+clientSubClient+" dbt.invoice_number IS NULL and mps_type='T' and dbt.travel_status='dataentry' and dbt.booking_date between '"+stdate+"' and '"+edate+"'";
				System.out.println("Sub query : "+sql);
				
				updateinvoice=sql;
				
				getData(sql,branchcode[0],clientcode[0]);
			
				if(!comboBoxClientCode.getValue().equals(null))
				{
					chkBill.setDisable(false);
				}
				else
				{
					chkBill.setDisable(true);
				}
				
				loadAwbNumbersToGenerateInvoice(sql);
			}	
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
		}
				
	}


// ====================================================================================================================	

	public void getData(String sql,String branchcode,String clientCode) throws SQLException
	{
		
		System.out.println("Client Code: "+clientCode+" | Branch Code: "+branchcode);
		double gst_amount=0;
		//j
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		
	//	new LoadClients().loadClientWithName();
		for(com.onesoft.courier.common.bean.LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientCode.equals(clBean.getClientCode()))
			{
				client_City=clBean.getClient_city();
				branch_Code_If_All_Branch_selected=clBean.getBranchcode();
				//branch_City_If_All_Branch_selected=clBean.getClient_city();
				break;
			}
		}

		//new LoadBranch().loadBranchWithName();
		System.out.println("Branch value in case ALl BRanch : "+branchcode);
		if(branchcode.equals("AllBranches"))
		{
			for(LoadBranchBean brBean:LoadBranch.SET_LOADBRANCHWITHNAME)
			{
				if(branch_Code_If_All_Branch_selected.equals(brBean.getBranchCode()))
				{
					branch_City=brBean.getBranchCity();
					break;
				}
			}
		}
		else
		{
			for(LoadBranchBean brBean:LoadBranch.SET_LOADBRANCHWITHNAME)
			{
				if(branchcode.equals(brBean.getBranchCode()))
				{
					branch_City=brBean.getBranchCity();
					break;
				}
			}
		}
		
		//new LoadCity().loadCityWithNameFromPinCodeMaster();
		//System.out.println("City List size: "+LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER.size());
		
		for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
		{
		/*for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{*/
			
			if(branch_City.equals(pinBean.getCity_name()))
			{
				branch_State=pinBean.getState_code();
				break;
			}
		}
		
		
		for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
		{
		/*for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{*/
			if(client_City.equals(pinBean.getCity_name()))
			{
				System.out.println("Client State: "+pinBean.getState_code());
				client_State=pinBean.getState_code();
				break;
			}
		}

		System.out.println("Branch State: >> "+branch_State+" | City >> "+branch_City);
		System.out.println("Client State: >> "+client_State+" | City >> "+client_City);
		
		InvoiceBean inbean=new InvoiceBean();
		
		try
		{
			st=con.createStatement();
			finalsql="select sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance_amt,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_total) as vas_amt,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as gst_amt,sum(dbt.total) as total from dailybookingtransaction as dbt where "+sql;
			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
				
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amt"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas_amt"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setGst(Double.parseDouble(df.format(rs.getDouble("gst_amt"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						
				}while(rs.next());
			}
			
			
			txtamount.setText(String.valueOf(inbean.getBasicAmount()));
			txtFuel.setText(String.valueOf(inbean.getFuel()));
			
			System.out.println("VAS >> "+inbean.getVas_amount()+" | COD >> "+inbean.getCod_amount()+" | FOD >> "+inbean.getFod_amount()+" | Docket >> "+inbean.getDocket_charge());
			
			double vas_total=inbean.getVas_amount()+inbean.getCod_amount()+inbean.getFod_amount()+inbean.getDocket_charge();
			
			txtVasTotal_Amt.setText(df.format(vas_total));
			
	
				
			txtInsurance.setText(String.valueOf(inbean.getInsurance_amount()));
			txtSubTotal.setText(df.format(inbean.getBasicAmount()+inbean.getFuel()+vas_total+inbean.getInsurance_amount()));
			if(Double.valueOf(txtSubTotal.getText())==0)
			{
				comboBoxDisAdd.setDisable(true);
			}
			else
			{
				comboBoxDisAdd.setDisable(false);
			}
			
			txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
			
			
			gst_amount_old=inbean.getGst();
			txtGST.setText(df.format(inbean.getGst()));
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
			if(Double.valueOf(txtSubTotal.getText())==0 || txtSubTotal.getText().equals(null))
			{
				comboBoxDisAdd.setDisable(true);
			}
			else if(!txtSubTotal.getText().equals(null))
			{
				comboBoxDisAdd.setDisable(false);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	//loadInvoiceDate();
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================
	
	public void loadInvoiceDate() throws SQLException
	{
		invoicetabledata.clear();
		//list_CompleteInvoiceDetails.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		
		int serialno=1;
		try
		{
			st=con.createStatement();
			//finalsql="select invoice_number,invoice_date,clientcode,from_date,to_date,grandtotal,remarks from invoice where invoice2branchcode='"+comboBoxBranchCode.getValue()+"' order by invoice_number";
			
			finalsql="select invoice_date,invoice_number,invoice2branchcode,clientcode,from_date,to_date,amount,fuel_rate,fuel,other_chrgs,insurance_amount,"
						+ "type_flat_percent,discount_Additional,rateof_percent,discount_additional_amount,reason,tax_name1,tax_rate1,tax_amount1,"
						+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,taxable_value,(tax_amount1+tax_amount2+tax_amount3) as gst_amount,"
						+ "grandtotal as total, remarks from invoice where invoice2branchcode='"+branchcode[0]+"' order by invoice_number DESC limit 50";
			
			
			System.out.println("Invoice Table: "+finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					InvoiceBean inbean=new InvoiceBean();
					inbean.setSerieano(serialno);
					inbean.setInvoice_date(date.format(rs.getDate("invoice_date")));
					inbean.setInvoice_number(rs.getString("invoice_number"));
					inbean.setInvoice2branchcode(rs.getString("invoice2branchcode"));
					inbean.setClientcode(rs.getString("clientcode"));
					inbean.setFrom_date(date.format(rs.getDate("from_date")));
					inbean.setTo_date(date.format(rs.getDate("to_date")));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setFuel_rate(Double.parseDouble(df.format(rs.getDouble("fuel_rate"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("other_chrgs"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
					inbean.setType_flat2percent(rs.getString("type_flat_percent"));
					inbean.setDiscount_additional(rs.getString("discount_Additional"));
					inbean.setRateof_percent(Double.parseDouble(df.format(rs.getDouble("rateof_percent"))));
					inbean.setDiscount_Additional_amount(Double.parseDouble(df.format(rs.getDouble("discount_additional_amount"))));
					inbean.setReason(rs.getString("reason"));
					
					inbean.setCgst_Name(rs.getString("tax_name1"));
					inbean.setCgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate1"))));
					inbean.setCgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount1"))));

					inbean.setSgst_Name(rs.getString("tax_name2"));
					inbean.setSgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate2"))));
					inbean.setSgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount2"))));

					inbean.setIgst_Name(rs.getString("tax_name3"));
					inbean.setIgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate3"))));
					inbean.setIgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount3"))));
					
					inbean.setTaxable_amount(Double.parseDouble(df.format(rs.getDouble("taxable_value"))));
					inbean.setGst_amount(Double.parseDouble(df.format(rs.getDouble("gst_amount"))));
					inbean.setGrandtotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					inbean.setRemarks(rs.getString("remarks"));
					
					
					//list_CompleteInvoiceDetails.add(inbean);
										
					invoicetabledata.add(new InvoiceBillJavaFXBean(inbean.getSerieano(),inbean.getInvoice_number(), inbean.getInvoice_date(),
							inbean.getClientcode(), inbean.getFrom_date(), inbean.getTo_date(),inbean.getBasicAmount(), inbean.getFuel(),
							inbean.getVas_amount(), inbean.getInsurance_amount(), inbean.getTaxable_amount(), inbean.getGst_amount(),
							inbean.getGrandtotal(), inbean.getRemarks()));
					
					serialno++;
					
				}while(rs.next());
			}
			
			invoiceTable.setItems(invoicetabledata);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
// ====================================================================================================================	
	
	public void loadAwbNumbersToGenerateInvoice(String sql) throws SQLException
	{
		detailedtabledata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		Set<String> networkset = new HashSet<String>();
		Set<String> serviceset = new HashSet<String>();
		Set<String> dnset = new HashSet<String>();
		
		
		
		InvoiceBean inbean=new InvoiceBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select master_awbno,booking_date,dailybookingtransaction2branch,dailybookingtransaction2client,billing_weight,"
					+ "packets,amount,insurance_amount,docket_charge,vas_total,fuel,total from dailybookingtransaction where "+sql.replaceAll("dbt.", "");
			
			System.out.println("From detailed table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			System.out.println("resultset size is : ======================  "+rs.getRow());
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					
					awbNoToInoviceGeneratedList.add(rs.getString("master_awbno"));
					
					inbean.setSerieano(serialno);
					inbean.setAbw_no(rs.getString("master_awbno"));
					inbean.setBooking_date(date.format(rs.getDate("booking_date")));
					inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
					inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
					inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("billing_weight"))));
					inbean.setPcs(rs.getInt("packets"));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas_total"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					
					detailedtabledata.add(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
							inbean.getInvoice2branchcode(), inbean.getClientcode(), inbean.getBilling_weight(), inbean.getPcs(), 
							inbean.getBasicAmount(), inbean.getInsurance_amount(), inbean.getDocket_charge(), inbean.getVas_amount(),
							inbean.getFuel(), inbean.getTotal()));
						
					serialno++;
					
				}while(rs.next());
				System.err.println("Detail Size: >>          "+detailedtabledata.size());
				
				awbCountToInvoicePrint=detailedtabledata.size();
				detailedtable.setItems(detailedtabledata);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================	

	public void showDetaildTableData() throws SQLException
	{
			
		if(txtTo.getText().equals("."))
		{
		}
		else if(txtTo.getText().equals(""))
		{
		//System.out.println("blank textfield");
		}
		else
		{
			detailedtabledata.clear();
			awb.clear();
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
				
			ResultSet rs=null;
			Statement st=null;
			String finalsql=null;
			
			Set<String> networkset = new HashSet<String>();
			Set<String> serviceset = new HashSet<String>();
			Set<String> dnset = new HashSet<String>();
			
			
			InvoiceBean inbean=new InvoiceBean();
			int serialno=1;
			try
			{
				st=con.createStatement();
				finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
						+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
						+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
						+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and bcdi.billing_weight between '"+txtFrom.getText()+"' and '"+txtTo.getText()+"' and "+updateinvoice+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
								+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";
				
				
				//summarySQL=finalsql;
				//System.out.println("From detailed table: "+finalsql);
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
				
					detailedtabledata.clear();
					labSummaryNetwork.setText("Networks:-");
					labSummaryService.setText("Services:-");
					labSummaryDoxNonDox.setText("D/N:-");
					
					chkBill.setDisable(true);
					txtamount.clear();
					//txtCOD.clear();
					//txtFOD.clear();
					txtInsurance.clear();
					//txtDocket.clear();
					txtFuel.clear();
					txtVasTotal_Amt.clear();
					txtSubTotal.clear();
					txtDiscountAddtitionalAmt.clear();
					txtGST.clear();
					txtGrandTotal.clear();
					
				}
				else
				{
					do
					{
					
						awb.add(rs.getString("air_way_bill_number"));
						
						networkset.add(rs.getString("dailybookingtransaction2network"));
						serviceset.add(rs.getString("dailybookingtransaction2service"));
						dnset.add(rs.getString("dox_nondox"));
						
						
						inbean.setSerieano(serialno);
						inbean.setAbw_no(rs.getString("air_way_bill_number"));
						inbean.setBooking_date(date.format(rs.getDate("booking_date")));
						inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
						inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
						inbean.setSub_clientcode(rs.getString("sub_client_code"));
						inbean.setFwd_weight(Double.parseDouble(df.format(rs.getDouble("fwd_weight"))));
						inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("weight"))));
						inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
						inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
						inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
						inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
						inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
						inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
						inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
						inbean.setGst(Double.parseDouble(df.format(rs.getDouble("tax"))));
						inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
							
						serialno++;
						
					}while(rs.next());
					
					
					System.err.println("Detail Size: >>          "+detailedtabledata.size());
					
					awbCountToInvoicePrint=detailedtabledata.size();
					
					detailedtable.setItems(detailedtabledata);
					printAWB();
					showSummary(updateinvoice, networkset, serviceset, dnset);
				}
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally
			{	
				dbcon.disconnect(null, st, rs, con);
				System.out.println("Detailed table size: ========= "+detailedtabledata.size());
			}
		}
	}	
	
// ====================================================================================================================

	public void printAWB()
	{
		for(String a:awb)
		{
			System.out.println("awb: "+a);
		}
	}
	
// ====================================================================================================================

	public void showSummary(String sql,Set<String> networkset,Set<String> serviceset,Set<String> dnset) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		int records = 0;
		
		try
		{
			String count=null;
			
			String networkList=" ";
			String serviceList=" ";
			String dnList=" ";
			
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			// --------------------- Network Summary ---------------------------------
			
			for(String net:networkset)
			{
			
				finalsql = "select dbt.dailybookingtransaction2network from dailybookingtransaction as dbt where dailybookingtransaction2network='"+net+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					//System.out.println("no data available");
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				if(net.equals(net) && records>0)
				{
					count=net+":"+records+" ";
				}
				
				records=0;
				
				if(!count.equals(null))
				{
					networkList=networkList.concat(count);
				}
			}
			rs.last();
			labSummaryNetwork.setText(networkList);
			count=null;
			//networkList="Network:- ";
			
			
			// --------------------- Service Summary ---------------------------------
			
			for(String service:serviceset)
			{	
				finalsql = "select dbt.dailybookingtransaction2service from dailybookingtransaction as dbt where dbt.dailybookingtransaction2service='"+service+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				
				if(service.equals(service) && records>0)
				{
					count=service+":"+records+" ";
				}
				
				records=0;
			
				if(!count.equals(null))
				{
				serviceList=serviceList.concat(count);
				}
			}
			rs.last();
			labSummaryService.setText(serviceList);
			count=null;
			//serviceList="Service:- ";
			
			
			// --------------------- Dox Non-Dox Summary ---------------------------------
			
			for(String dn:dnset)
			{
				finalsql = "select dbt.dox_nondox from dailybookingtransaction as dbt where dbt.dox_nondox='"+dn+"' and "+sql;
				rs=st.executeQuery(finalsql);
					
				if(!rs.next())
				{
				
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
						
				if(dn.equals(dn) && records>0)
				{
					count=dn+":"+records+" ";
				}
						
				records=0;
					
				if(!count.equals(null))
				{
				dnList=dnList.concat(count);
				}
			}
			rs.last();		
			labSummaryDoxNonDox.setText(dnList);
			count=null;
			//dnList="D/N:- ";
			
			
		}	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			serviceset.clear();
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
// ====================================================================================================================	
	
	public void setDiscountAdd()
	{
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT))
		{
			txtTypeAmount.clear();
			txtReason.clear();
			txtTypeAmount.setDisable(true);
			txtReason.setDisable(true);
			comboBoxTypeFlatPercent.setDisable(true);
			comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		}
		else
		{
			txtTypeAmount.clear();
			txtReason.setDisable(false);
			comboBoxTypeFlatPercent.setDisable(false);
		}
	
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
	//	txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		
	}
	
	
	public void type()
	{
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
		{
			txtTypeAmount.clear();
			txtdiscountLessAmt.clear();
			txtTypeAmount.setDisable(true);
			txtdiscountLessAmt.setDisable(true);
			//txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
			txtGST.setText(df.format(gst_amount_old));
			//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		}
		
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.clear();
			txtGST.setText(df.format(gst_amount_old));
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(false);
		}
		else
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.clear();
			txtGST.setText(df.format(gst_amount_old));
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(true);
		}
		
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
		//txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
	}

// ====================================================================================================================
	
	public void setPercentFlat()
	{
		double discount=0.0;
		InvoiceBean inbean=new InvoiceBean();
		
		
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
		{	
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(df.format(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-discount);
				
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-Integer.parseInt(txtTypeAmount.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText()));
				
			}
				
		}
		else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
		{
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(df.format(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+discount);
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+Integer.parseInt(txtTypeAmount.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText()));
				
			}
		}
		
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
	
		txtDiscountAddtitionalAmt.setText(df.format(inbean.getAmountAfterDis_Add()));
		//getGST_onDateChangeWithoutAdd_dis();
		
		/*if(Double.valueOf(txtTypeAmount.getText())>0)
		{*/
		
		if(chkGST.isSelected()==false)
		{
			System.err.println("if GST check Box selected....");
			igst_amount=0;
			igst_rate=0;
			cgst_rate=0;
			sgst_rate=0;
			cgst_amount=0;
			sgst_amount=0;
			txtGST.setText("0");
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())));
		}
		else
		{
			if(branch_State.equals(client_State))
			{
				System.out.println(txtDiscountAddtitionalAmt.getText());
				
				if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_fix;
					sgst_rate=LoadGST_Rates.sgst_rate_fix;
					txtGST.setText(df.format(cgst_amount+sgst_amount));
					System.out.println("1 CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");	
				}
				else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_Temp)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_Temp)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_Temp;
					sgst_rate=LoadGST_Rates.sgst_rate_Temp;
					System.out.println("2 CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				}
				
				else 
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_fix;
					sgst_rate=LoadGST_Rates.sgst_rate_fix;
					System.out.println("3 CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				}
				
				/*cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				
				System.out.println("CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");*/
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				
				//txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
			}
			else
			{
				System.out.println(txtDiscountAddtitionalAmt.getText());
				if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
					igst_rate=LoadGST_Rates.igst_rate_fix;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("4 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);	
				}
				else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_Temp)/100;
					igst_rate=LoadGST_Rates.igst_rate_Temp;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("5 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
				
				}
				
				else 
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
					igst_rate=LoadGST_Rates.igst_rate_fix;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("6 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				}
				
				
				/*igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				System.out.println("CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);*/
				txtGST.setText(df.format(igst_amount));
			}
		}
		/*}
		else
		{
			txtGST.setText(df.format(gst_amount_old));
		}*/
		grandtotal();
	}

	
// ====================================================================================================================

	public void grandtotal()
	{
		
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
		}
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		}
	}
	

// ====================================================================================================================	

	public void getGST_onDateChangeWithoutAdd_dis()
	{
		//d
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
		System.err.println("In GST Calculation Method....");
		
		
		if(chkGST.isSelected()==false)
		{
			System.err.println("if GST check Box selected....");
			igst_amount=0;
			igst_rate=0;
			cgst_rate=0;
			sgst_rate=0;
			cgst_amount=0;
			sgst_amount=0;
			txtGST.setText("0");
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())));
		}
		else
		{
			int status=invoiceDate.compareTo(LoadGST_Rates.gst_Date_Fix);
			
			
			
		if(branch_State.equals(client_State))
		{
			System.err.println("if GST check Box not selected iffffffff....");
			
			if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_Temp)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_Temp)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_Temp;
				sgst_rate=LoadGST_Rates.sgst_rate_Temp;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				System.out.println("A >> CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//System.out.println("CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
			
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.after(LoadGST_Rates.gst_Date_Fix))
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_fix;
				sgst_rate=LoadGST_Rates.sgst_rate_fix;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				System.out.println("B >> CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//txtGST.setText(df.format(gst_amount_old));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && status==0)
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_fix;
				sgst_rate=LoadGST_Rates.sgst_rate_fix;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				System.out.println("C >> CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//txtGST.setText(df.format(gst_amount_old));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			//System.out.println("A >> CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
		}
		else
		{
			System.err.println("if GST check Box not selected elseeeeeeeeee....");
			
			
			if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
				
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_Temp)/100;
				igst_rate=LoadGST_Rates.igst_rate_Temp;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;

				System.out.println("D >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.after(LoadGST_Rates.gst_Date_Fix))
			{
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				igst_rate=LoadGST_Rates.igst_rate_fix;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;
				System.out.println("E >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				//txtGST.setText(df.format(gst_amount_old));
				//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && status==0)
			{
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				igst_rate=LoadGST_Rates.igst_rate_fix;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;
				System.out.println("F >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				//txtGST.setText(df.format(gst_amount_old));
				//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			//System.out.println("B >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
		}
		}
		
	}
	
	
// ====================================================================================================================
	
	public void showGenerateButton()
	{
		if(chkBill.isSelected()==true)
		{
			btnGenerate.setDisable(false);
		}
		else
		{
			btnGenerate.setDisable(true);
		}
	}
	
// ====================================================================================================================

	public void getServiceTaxPercentage() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		String sql=null;
		
		try
		{	
			/*st=con.createStatement();
			sql="select effective_date ,sum(tax+tax1+tax2) as servicetax from expensestaxtype group by effective_date order by effective_date DESC limit 1";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
			invcBean.setServiceTaxPercentage(rs.getDouble("servicetax"));
			}
			*/
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================	

	public void reset() throws IOException
	{
		
		
		dpkFromDate.setEditable(false);
		
		dpkFromDate.setOnMouseClicked(e -> {
		        if(!dpkFromDate.isEditable()==true)
		        	dpkFromDate.show();
		    });
		
		dpkToDate.setEditable(false);
		
		dpkToDate.setOnMouseClicked(e -> {
		        if(!dpkToDate.isEditable()==true)
		        	dpkToDate.show();
		    });
		
		m.showGenerateInvoice();	
	}
		
// ====================================================================================================================	

	public void exit() throws SQLException
	{
		try {
			m.homePage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

// ==================================================================================================
	
	@FXML
	public void datePickerEnterTab(KeyEvent e) throws SQLException
	{
		//loadClients();
		System.out.println("this method: ===>>>"+e);
		
		if(e.getCode().equals(KeyCode.ENTER))
		{
			System.out.println("this method running ===================== ");
			
			dpkToDate.requestFocus();
		}
		
	}
	
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, ParseException, NumberFormatException, IOException
	{

	Node n=(Node) e.getSource();
		
		
	if(n.getId().equals("txtInvoiceNo"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			//loadClients();
			dpkInvoiceDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkInvoiceDate"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			dpkFromDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkFromDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//loadClients();
				dpkToDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkToDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxBranchCode.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxBranchCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				loadClients();
				comboBoxClientCode.requestFocus();
			}
		}
	
		// *********************
	
	
		else if(n.getId().equals("txtamount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtFuel.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtFuel"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtVasTotal_Amt.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtVasTotal_Amt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtInsurance.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtInsurance"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtSubTotal.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtSubTotal"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
	
		else if(n.getId().equals("comboBoxClientCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
	
		else if(n.getId().equals("comboBoxClientCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
		
	/*	else if(n.getId().equals("comboBoxSubClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}*/
		
		else if(n.getId().equals("comboBoxDisAdd"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtReason.isDisable()==true)
				{
					txtDiscountAddtitionalAmt.requestFocus();
				}
				else
				{
					txtReason.requestFocus();	
				}
			}
		}
		
		else if(n.getId().equals("txtReason"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxTypeFlatPercent.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxTypeFlatPercent"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.isDisable()==true)
				{
					//txtremarks.requestFocus();
					txtDiscountAddtitionalAmt.requestFocus();
				}
				else
				{
					txtTypeAmount.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("txtTypeAmount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.getText().equals("") || txtTypeAmount.getText().isEmpty())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty field");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the discount or additional amount...");
					alert.showAndWait();
					txtTypeAmount.requestFocus();
				}
				else
				{
					if(txtdiscountLessAmt.isDisable()==true)
					{
						txtDiscountAddtitionalAmt.requestFocus();
					}
					else
					{
						txtdiscountLessAmt.requestFocus();
					}
				}
			}
		}
	
		else if(n.getId().equals("txtdiscountLessAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtDiscountAddtitionalAmt.requestFocus();
			}
		}

	/*	else if(n.getId().equals("txtDiscountAddtitionalAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkGST.requestFocus();
			}
		}*/
	
		else if(n.getId().equals("chkGST"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText(null);
					alert.setContentText("Please select Discount/Additional type");
					alert.showAndWait();
					comboBoxTypeFlatPercent.requestFocus();
				} 
				else if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && !comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					setPercentFlat();
					txtGST.requestFocus();
					
				}
					
				else
				{
					getGST_onDateChangeWithoutAdd_dis();
					txtGST.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("txtDiscountAddtitionalAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText(null);
					alert.setContentText("Please select Discount/Additional type");
					alert.showAndWait();
					comboBoxTypeFlatPercent.requestFocus();
				}
				else
				{
					getGST_onDateChangeWithoutAdd_dis();
					txtGST.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("txtGST"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtGrandTotal.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtGrandTotal"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtremarks.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtremarks"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkBill.requestFocus();
			}
		}
	
		else if(n.getId().equals("chkBill"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(chkBill.isSelected()==true)
				{
					btnGenerate.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("btnGenerate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				getInvoiceSeries();
			}
		}
	
	
	}	
	
	
// ====================================================================================================================
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		
		imgSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		dpkFromDate.requestFocus();
		
		comboBoxDisAdd.setValue(InvoiceUtils.INVOICDEFAULT);
		comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		
		
		comboBoxDisAdd.setItems(comboBoxDiscountAddtitionalItems);
		comboBoxTypeFlatPercent.setItems(comboBoxTypeFlatPercentItems);
		
		txtReason.setDisable(true);
		txtTypeAmount.setDisable(true);
		txtFrom.setDisable(true);
		txtTo.setDisable(true);
		txtNew.setDisable(true);
		//comboBoxSubClient.setDisable(true);
		comboBoxTypeFlatPercent.setDisable(true);
		comboBoxDisAdd.setDisable(true);
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		txtdiscountLessAmt.setDisable(true);
		
		dpkFromDate.setValue(LocalDate.now());
		dpkToDate.setValue(LocalDate.now());
		dpkInvoiceDate.setValue(LocalDate.now());
		
		try
		{
			new LoadCompany().loadCompanyWithName();
			//checkInvoice();
			loadBranch();
			
			getServiceTaxPercentage();
		//	loadInvoiceDate();
			//loadClients();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		invc_Col_serialno.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Integer>("slno"));
		invc_Col_InvoiceNo.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceNumber"));
		invc_Col_InvoiceDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceDate"));
		invc_Col_FromDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("from_date"));
		invc_Col_ToDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("to_date"));
		invc_Col_ClientCode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientCode"));
		invc_Col_BasicAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("basicAmount"));
		invc_Col_Fuel.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("fuel"));
		invc_Col_VasAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("vas_amount"));
		invc_Col_InsuranceAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("insuranceAmount"));
		invc_Col_TaxableAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("taxableAmount"));
		invc_Col_GSTAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("gst_Amount"));
		invc_Col_GrandTotal.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceGrandTotal"));
		invc_Col_Remarks.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceRemarks"));
		
		serialno.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("awb_no"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("booking_date"));
		branch.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("invoice2branchcode"));
		clientcode.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("clientcode"));
		billingweight.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("insurance_amount"));
		pcs.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Integer>("pcs"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("docket_charge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("other_amount"));
		amount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("fuel"));
		total.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("total"));
		
		chkGST.setSelected(true);
		
		
		/*double amout=999999.12;
		int a=(int)amout;
		ConvertAmountInWords cnvrtWrd=new ConvertAmountInWords();
		
		System.err.println("Amount >> "+a+" >>>>>>>>>>>>>>>> amount to words >> "+cnvrtWrd.convertToWords(Integer.valueOf(a)));*/
	
	}
	
	

}
