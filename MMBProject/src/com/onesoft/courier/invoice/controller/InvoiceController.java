package com.onesoft.courier.invoice.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.ConvertAmountInWords;
import com.onesoft.courier.common.GenerateSaveReport;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCompany;
import com.onesoft.courier.common.LoadGST_Rates;
import com.onesoft.courier.common.LoadInvoices;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadCompanyBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.invoice.bean.InvoiceBean;
import com.onesoft.courier.invoice.bean.InvoiceBillJavaFXBean;
import com.onesoft.courier.invoice.bean.InvoiceDeleteReprintBean;
import com.onesoft.courier.invoice.bean.InvoiceTableBean;
import com.onesoft.courier.invoice.utils.InvoiceUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

public class InvoiceController implements Initializable{

	private ObservableList<InvoiceTableBean> detailedtabledata=FXCollections.observableArrayList();
	private ObservableList<InvoiceBillJavaFXBean> invoicetabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSubClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxDiscountAddtitionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.INVOICEDISCOUNT,InvoiceUtils.INVOICEADDITIONAL);
	//private ObservableList<String> comboBoxTypeAdditionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.a);
	private ObservableList<String> comboBoxTypeFlatPercentItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.TYPE1,InvoiceUtils.TYPE2);
	
	InvoiceBean invcBean=new InvoiceBean();
	
	List<String> awbNoToInoviceGeneratedList=new ArrayList<>();
	List<InvoiceBean> list_CompleteInvoiceDetails=new ArrayList<>();
	List<InvoiceBean> list_InvoiceAndCompanyDetails=new ArrayList<>();
	List<String> list_ClientsViaBranchAndDateRange=new ArrayList<>();
	private List<String> list_InvoiceForSelectedAWBRecords=new ArrayList<>();
	private List<String> list_FormatMisMatch=new ArrayList<>();
	private List<InvoiceBean> list_DetailedTableData=new ArrayList<>();
	
	Map<String, InvoiceBean> hashMap_InvoiceData=new HashMap<>();
	
	public double cgst_amount=0;
	public double sgst_amount=0;
	public double igst_amount=0;
	public double cgst_rate=0;
	public double sgst_rate=0;
	public double igst_rate=0;
	
	public String invoiceNumberGenerated=null;
	public String invoiceNumberToPreview=null;
	public String branchCodeToGetCompanyDetail=null;
	public String summarySQL=null;
	public int awbCountToInvoicePrint=0;
	//public String summarySQL_part2=null;
	
	public double gst_amount_old=0;
	
	public String branch_Code_If_All_Branch_selected=null;
	public String branch_City_If_All_Branch_selected=null;
	public String branch_City=null;
	public String client_City=null;
	
	public String branch_State=null;
	public String client_State=null;
	
	Set<String> awb=new HashSet<String>();
	
	public String checkBoxStatus;
	public String src = "E:\\NewEclipseWorkspace_2017\\MMBProject\\src\\com\\onesoft\\courier\\invoice\\controller\\Invoice.jrxml";
	Map<String , Object> parameters = new HashMap<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	
	@FXML
	private RadioButton rdBtnFreshInvoice;
	
	@FXML
	private RadioButton rdBtnReGenerateInvoice;
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private Label labSummaryNetwork;
	
	@FXML
	private Label labSummaryService;
	
	@FXML
	private Label labSummaryDoxNonDox;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnLoad;
	
	@FXML
	private TextField txtBrowseExcelFile;
	
	@FXML
	private TextField txtInvoiceNo;
	
	@FXML
	private TextField txtamount;
	
	/*@FXML
	private TextField txtCOD;
	
	@FXML
	private TextField txtSub_Client;
	
	@FXML
	private TextField txtFOD;
	
	@FXML
	private TextField txtDocket;*/
	
	@FXML
	private TextField txtFuel;
	
	@FXML
	private TextField txtVasTotal_Amt;
	
	@FXML
	private TextField txtInsurance;
	
	@FXML
	private TextField txtSubTotal;
	
	@FXML
	private TextField txtReason;
	
	@FXML
	private TextField txtTypeAmount;
	
	@FXML
	private TextField txtDiscountAddtitionalAmt;
	
	@FXML
	private TextField txtGST;
	
	@FXML
	private TextField txtGrandTotal;
	
	@FXML
	private TextField txtremarks;
	
	@FXML
	private TextField txtFrom;
	
	@FXML
	private TextField txtTo;
	
	@FXML
	private TextField txtNew;
	
	@FXML
	private TextField txtdiscountLessAmt;
	
	@FXML
	private CheckBox chkExportToExcel;
	
	@FXML
	private CheckBox chkFilter;
	
	@FXML
	private CheckBox chkShowDue;
	
	@FXML
	private CheckBox chkBankDetails;
	
	@FXML
	private CheckBox chkGST;
	
	/*@FXML
	private CheckBox chkPCS;
	
	@FXML
	private CheckBox chkD_N;
	
	@FXML
	private CheckBox chkLogo;
	
	@FXML
	private CheckBox chkStatus;*/
	
	@FXML
	private CheckBox chkBill;
	
	@FXML
	private ComboBox<String> comboBoxBranchCode;
	
	@FXML
	private ComboBox<String> comboBoxClientCode;
	
	/*@FXML
	private ComboBox<String> comboBoxSubClient;*/
	
	@FXML
	private ComboBox<String> comboBoxDisAdd;
	
	@FXML
	private ComboBox<String> comboBoxTypeFlatPercent;
	
	@FXML
	private DatePicker dpkInvoiceDate;
	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	

//================================================================

	@FXML
	private TableView<InvoiceBillJavaFXBean> invoiceTable;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Integer> invc_Col_serialno;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_InvoiceDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_InvoiceNo;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_ClientCode;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_FromDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_ToDate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_BasicAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_Fuel;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_VasAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_InsuranceAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_TaxableAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_GSTAmt;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invc_Col_GrandTotal;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invc_Col_Remarks;
	
	
//================================================================	
	
	@FXML
	private TableView<InvoiceTableBean> detailedtable;
	
	@FXML
	private TableColumn<InvoiceTableBean, Integer> serialno;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> awb_no;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> bookingdate;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> branch;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> clientcode;
	
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> billingweight;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> amount;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> insuracneamount;
	
	@FXML
	private TableColumn<InvoiceTableBean, Integer> pcs;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> docketcharge;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> otherVas;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> fuel;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> total;
	
	public String updateinvoice=null;
	
	
// ====================================================================================================================
	
	Main m=new Main();
	
	
// ====== Check invoice replaced by getInvoiceByBranch and getLatestInvoiceNo() from GenerateInvoiceByBranch class =============	
	
	/*public void checkInvoice() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		InvoiceBean inbean=new InvoiceBean();
		
		
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select * from invoiceseries order by invoice_end_no DESC limit 1";
			rs=st.executeQuery(sql);
	
			//System.out.println("row count: "+rs.getRow());
			
			if(!rs.next())
			{
				//System.out.println("No value: ");
				txtInvoiceNo.setEditable(true);
				
			}
			else
			{
				inbean.setInvoice_start(rs.getString("invoice_start_no"));
				inbean.setInvoice_end(rs.getLong("invoice_end_no"));
				inbean.setInvoice_number(inbean.getInvoice_start()+(inbean.getInvoice_end()+1));
				invcBean.setInvoice_number(inbean.getInvoice_number());
				//txtInvoiceNo.setText(inbean.getInvoice_number());
				//txtInvoiceNo.setEditable(false);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
	
// ====================================================================================================================
	
	public void loadGSTRates_OnDateChange() throws SQLException
	{
		
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
		
		if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
		{
			System.out.println("CGST: "+LoadGST_Rates.cgst_rate_fix+" | SGST: "+LoadGST_Rates.sgst_rate_fix+" | IGST: "+LoadGST_Rates.igst_rate_fix);	
		}
		else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
		{
			LoadGST_Rates loadGST=new LoadGST_Rates();
			loadGST.loadGST_OnChangeDate(invoiceDate);
			//System.out.println(invoiceDate+" is Less than Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
		}
		
		else 
		{
			System.out.println("CGST: "+LoadGST_Rates.cgst_rate_fix+" | SGST: "+LoadGST_Rates.sgst_rate_fix+" | IGST: "+LoadGST_Rates.igst_rate_fix);
		}
		
	}
	
	
	/*public void loadGSTRates_OnDateChange() throws SQLException
	{
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
		if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix))
		{
			
			System.out.println(invoiceDate+" is Greater than Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
		}
		
		
		else if(LoadGST_Rates.gst_Date_Temp!=null)
		{
			if(invoiceDate.after(LoadGST_Rates.gst_Date_Temp) && invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
			for(LoadGST_RateBean gstBean: LoadGST_Rates.LIST_LATEST_GST_RATE_SLAB)
			{
				LoadGST_Rates.gst_Date_Fix=gstBean.getGstEffectiveDate();
				LoadGST_Rates.cgst_rate_fix=gstBean.getTax1_cgst_rate();
				LoadGST_Rates.sgst_rate_fix=gstBean.getTax2_sgst_rate();
				LoadGST_Rates.igst_rate_fix=gstBean.getTax3_igst_rate();		
				//System.out.println("Current GST Fix date >> "+LoadGST_Rates.gst_Date_Fix);
				System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_fix+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_fix+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_fix);
			}
			LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
			Date invoiceDate=Date.valueOf(local_InvoiceDate);
			//System.out.println(invoiceDate+" is In between Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
			}
			else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
				
				LoadGST_Rates loadGST=new LoadGST_Rates();
				loadGST.loadGST_OnChangeDate(invoiceDate);
				LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
				Date invoiceDate=Date.valueOf(local_InvoiceDate);
				//System.out.println(invoiceDate+" is Less  >> than Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
			}
			else 
			{
				for(LoadGST_RateBean gstBean: LoadGST_Rates.LIST_LATEST_GST_RATE_SLAB)
				{
					LoadGST_Rates.gst_Date_Fix=gstBean.getGstEffectiveDate();
					LoadGST_Rates.cgst_rate_fix=gstBean.getTax1_cgst_rate();
					LoadGST_Rates.sgst_rate_fix=gstBean.getTax2_sgst_rate();
					LoadGST_Rates.igst_rate_fix=gstBean.getTax3_igst_rate();		
					//System.out.println("Current GST Fix date >> "+LoadGST_Rates.gst_Date_Fix);
					System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_fix+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_fix+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_fix);
				}
				//System.out.println(invoiceDate+" is equal to Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
			}
			
		}
		
		else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
		{
			
			LoadGST_Rates loadGST=new LoadGST_Rates();
			loadGST.loadGST_OnChangeDate(invoiceDate);
			LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
			Date invoiceDate=Date.valueOf(local_InvoiceDate);
			//System.out.println(invoiceDate+" is Less than Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
		}

		
		
		else 
		{
			for(LoadGST_RateBean gstBean: LoadGST_Rates.LIST_LATEST_GST_RATE_SLAB)
			{
				LoadGST_Rates.gst_Date_Fix=gstBean.getGstEffectiveDate();
				LoadGST_Rates.cgst_rate_fix=gstBean.getTax1_cgst_rate();
				LoadGST_Rates.sgst_rate_fix=gstBean.getTax2_sgst_rate();
				LoadGST_Rates.igst_rate_fix=gstBean.getTax3_igst_rate();		
				//System.out.println("Current GST Fix date >> "+LoadGST_Rates.gst_Date_Fix);
				System.out.println(gstBean.getTax1_cgst()+": "+LoadGST_Rates.cgst_rate_fix+" | "+gstBean.getTax2_sgst()+": "+LoadGST_Rates.sgst_rate_fix+" | "+gstBean.getTax3_igst()+": "+LoadGST_Rates.igst_rate_fix);
			}
			//System.out.println(invoiceDate+" is equal to Current GST rate Date "+LoadGST_Rates.gst_Date_Fix);
		}
			
		
	}*/
	
	
// ====================================================================================================================	

	public void getInvoiceSeries() throws IOException, SQLException, NumberFormatException
	{
	
		InvoiceBean inbean=new InvoiceBean();
		String[] branchcode=null;
		branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		Statement st=null;
		String sql=null;

		//ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		inbean.setInvoice2branchcode(branchcode[0]);
		
		try
		{	
			sql = "select invoice_number,invoice2branchcode from invoice where invoice2branchcode='"+branchcode[0]+"' order by invoice_number DESC LIMIT 1";
			System.out.println(sql);
			st=con.createStatement();
			rs=st.executeQuery(sql);
				
			if(!rs.next())
			{
				System.err.println("Invoice no not found");
				generateFreshInvoiceNumber();
			}
			else
			{
				do
				{
					System.err.println("Invoice no found");
					incrementExistingInvoiceNo_ForNewInvoice(rs.getString("invoice_number"));
				}
				while (rs.next());
			}
			
			
			
			
			/*String query = "select invoice_number,invoice2branchcode from invoice where invoice2branchcode=? order by invoice_number DESC";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1,inbean.getInvoice2branchcode());
			//preparedStmt.setLong(2, inbean.getInvoice_end());
			rs=preparedStmt.executeUpdate();*/
			
		
			//invoiceGeneratedAwbNo();
			/*saveInvoiceDate();
			loadInvoiceDate();
			for(String invoicedAwbno: awbNoToInoviceGeneratedList)
			{
				updateDBT_Invoice(invoicedAwbno);
			}
			reset();
			detailedtabledata.clear();
			labSummaryNetwork.setText("Networks:-");
			labSummaryService.setText("Services:-");
			labSummaryDoxNonDox.setText("D/N:-");
			
			
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation");
			alert.setHeaderText("Send mail request");
			alert.setContentText("Do you want to copy of invoice in your mail?");
			
			ButtonType buttonTypeYes = new ButtonType("Yes");
			ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
			//ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
			
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeYes)
			{
			
			sendMail();
				}
			else if(result.get() == buttonTypeNo)
				{
				//System.out.println("No Mail");
				}
			*/
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

// ====================================================================================================================	

	
	public void incrementExistingInvoiceNo_ForNewInvoice(String invoiceNo) throws SQLException
	{
		String[] splitInvoiceNo=invoiceNo.replaceAll("\\s+","").split("\\/");
		
		int invoiceSeries=Integer.valueOf(splitInvoiceNo[2])+1;
		
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);

		int currentyear = Integer.valueOf(String.valueOf(invoiceDate).substring(0, 4));

		int nextYear = currentyear+1;
		int previousYear = currentyear-1;
		System.out.println("Current year: "+currentyear);
		System.out.println("Next year: "+nextYear);
		System.out.println("Previous year: "+previousYear);

		String FinancialYearStartDate=currentyear+"-4-1";
		System.out.println("Financial year: "+FinancialYearStartDate);


		String financialYr=null;
		int dateStatus=invoiceDate.compareTo(Date.valueOf(FinancialYearStartDate));

		if(dateStatus>=0)
		{
			financialYr=currentyear+"-"+String.valueOf(nextYear).substring(2);
		}
		else 
		{
			financialYr=previousYear+"-"+String.valueOf(currentyear).substring(2);
		}
		
		
		invoiceNumberGenerated=splitInvoiceNo[0]+"/"+financialYr+"/"+invoiceSeries;
		saveInvoiceDate();
		
		
	}
	
	
// ====================================================================================================================
	
	public void generateFreshInvoiceNumber() throws SQLException
	{
			String branch=null;
			String[] branchcode=null;
			String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");

			if(comboBoxBranchCode.getValue().equals(InvoiceUtils.ALLBRANCH))
			{
				if(branch_Code_If_All_Branch_selected==null)
				{	
					for(com.onesoft.courier.common.bean.LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
					{
						if(clientcode[0].equals(clBean.getClientCode()))
						{
							branch_Code_If_All_Branch_selected=clBean.getBranchcode();
							branchcode=branch_Code_If_All_Branch_selected.replaceAll("\\s+","").split("\\|");
							break;
						}
					}
				}
				else
				{
					branchcode=branch_Code_If_All_Branch_selected.replaceAll("\\s+","").split("\\|");
				}
			}
			else
			{
				branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			}


			LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
			Date invoiceDate=Date.valueOf(local_InvoiceDate);

			int currentyear = Integer.valueOf(String.valueOf(invoiceDate).substring(0, 4));

			int nextYear = currentyear+1;
			int previousYear = currentyear-1;
			System.out.println("Current year: "+currentyear);
			System.out.println("Next year: "+nextYear);
			System.out.println("Previous year: "+previousYear);

			String FinancialYearStartDate=currentyear+"-4-1";
			System.out.println("Financial year: "+FinancialYearStartDate);


			String financialYr=null;
			int dateStatus=invoiceDate.compareTo(Date.valueOf(FinancialYearStartDate));

			if(dateStatus>=0)
			{
				financialYr=currentyear+"-"+String.valueOf(nextYear).substring(2);
			}
			else 
			{
				financialYr=previousYear+"-"+String.valueOf(currentyear).substring(2);
			}


			System.out.println("invoice series:  "+branchcode[0]+"/"+financialYr+"/1100001");


			invoiceNumberGenerated=branchcode[0]+"/"+financialYr+"/1100001";
			saveInvoiceDate();
					
			
	}
	
	
// ====================================================================================================================	

	String clientname_Sendmail=null;
	
	public void sendMail()
	{	
		LocalDate indate=dpkInvoiceDate.getValue();
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date invoicedate=Date.valueOf(indate);
		Date fromdate=Date.valueOf(from);
		Date todate=Date.valueOf(to);
		
		String msg= "Invoice Details:"
				+ "<table border=2 bordercolor=black><tr><td><b>Invoice No.</b></td><td><b>Invoice Date</b></td><td><b>Billing Period</b></td>"
				+ "<td><b>Amount</b></td><td><b>Fuel</b></td><td><b>Service Tax</b></td><td><b>Grand Total</b></td></tr>"
				+ "<tr><td>"+txtInvoiceNo.getText()+"</td><td>"+date.format(invoicedate)+"</td><td>"+date.format(fromdate)+" to "+date.format(todate)+"</td>"
						+ "<td>"+txtamount.getText()+"</td><td>"+txtFuel.getText()+"</td><td>"+txtGST.getText()+"</td><td>"+txtGrandTotal.getText()+"</td></tr></table>";
		
		
		Properties props = new Properties(); 
		props.put("mail.smtp.host","mail.onesoft.in"); 
		props.put("mail.smtp.auth","true");
		props.put("mail.smtp.port","587");
					
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication("nitesh@onesoft.in","nitesh@321");  
				      }  
				  });  
						   
		try
		{  
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("nitesh@onesoft.in"));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress("nitesh@onesoft.in"));
			message.setSubject("Invoice Details");
			
			
			message.setContent(msg,"text/html");
			Transport.send(message);
						    	
			System.out.println("Email send Successfully");
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		} 
		
		 //System.out.println("Authentication method is closed");
		 //System.out.println("\n");
		
	}
	
// ====================================================================================================================

	public void updateDBT_Invoice(String invoiceNo) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		
		boolean isBulkDataAvailable=false;
		int count=0;
		
		try
		{	
			String query = "update dailybookingtransaction set invoice_number=? where air_way_bill_number=?";
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			if(list_InvoiceForSelectedAWBRecords.isEmpty()==false)
			{
				if(list_InvoiceForSelectedAWBRecords.size()>200)
				{
					isBulkDataAvailable=true;
				}
				else
				{
					isBulkDataAvailable=false;
				}
				
				for(String invoicedAwbno: list_InvoiceForSelectedAWBRecords)
				{
					preparedStmt.setString(1,invoiceNo);
					preparedStmt.setString(2,invoicedAwbno);
					preparedStmt.addBatch();
					
					count++;
					if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}
				}
			}
			else
			{
				if(awbNoToInoviceGeneratedList.size()>200)
				{
					isBulkDataAvailable=true;
				}
				else
				{
					isBulkDataAvailable=false;
				}
				
				for(String invoicedAwbno: awbNoToInoviceGeneratedList)
				{
					preparedStmt.setString(1,invoiceNo);
					preparedStmt.setString(2,invoicedAwbno);
					preparedStmt.addBatch();
					
					count++;
					if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}
				}
			}
			
			int[] result = preparedStmt.executeBatch();
			System.out.println("Invoice No. Updated in DailyBookingTransaction Table : "+ result.length);
			con.commit();
			
			/*System.out.println("From update DBT: "+ txtInvoiceNo.getText() +" "+awbno);
			st = con.createStatement();
			String query = "update dailybookingtransaction as dbt set invoice_number='"+invoiceNo+"' where air_way_bill_number='"+awbno+"'";
			System.out.println("SQL quer from update: "+query);
			st.executeUpdate(query);
            System.out.println("Invoice Updated...");*/
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
// ====================================================================================================================		

	public void saveInvoiceDate() throws SQLException
	{
		LocalDate indate=dpkInvoiceDate.getValue();
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date invoicedate=Date.valueOf(indate);
		Date fromdate=Date.valueOf(from);
		Date todate=Date.valueOf(to);
		
		InvoiceBean inbean=new InvoiceBean();
		
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		clientname_Sendmail=clientcode[1];
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			inbean.setInvoice_number(invoiceNumberGenerated);
			inbean.setInvoice2branchcode(branchcode[0]);
			inbean.setClientcode(clientcode[0]);
			inbean.setAmountAfterDis_Add(Double.parseDouble(txtDiscountAddtitionalAmt.getText()));
						
			if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
			{
				inbean.setDiscount_additional(comboBoxDisAdd.getValue());
			}
			else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
			{
				inbean.setDiscount_additional(comboBoxDisAdd.getValue());
			}
			
			//System.out.println("Discount additional: "+inbean.getDiscount_additional());
			
			if(!txtReason.getText().equals(null) && !txtReason.getText().isEmpty())
			{
				inbean.setReason(txtReason.getText());
			}
			else
			{
				
			}
			
			//System.out.println("Reason: "+inbean.getReason());
			
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				inbean.setType_flat2percent("P");
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setType_flat2percent("F");
			}
			
			
			//System.out.println("Flat or percent: "+inbean.getType_flat2percent());
			
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2) && !txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty())
			{
				inbean.setRateof_percent(0.0);
			}
			else if(!txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty()) 
			{
				inbean.setRateof_percent(Double.parseDouble(txtTypeAmount.getText()));
			}
			
			else
			{
				inbean.setRateof_percent(0.0);
			}
			//System.out.println("rate of percent: "+inbean.getRateof_percent());
			
			if(!txtdiscountLessAmt.getText().equals(null) && !txtdiscountLessAmt.getText().isEmpty())
			{
				inbean.setDiscount_Additional_amount(Double.parseDouble(txtdiscountLessAmt.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setDiscount_Additional_amount(Double.parseDouble(txtTypeAmount.getText()));
			}
			else
			{
				inbean.setDiscount_Additional_amount(0.0);
			}
			//System.out.println("discount less amount: "+inbean.getDiscount_Additional_amount());
			
			inbean.setFuel(Double.parseDouble(txtFuel.getText()));
			inbean.setVas_amount(Double.valueOf(txtVasTotal_Amt.getText()));
			
			//inbean.setOther_amount(Double.parseDouble(txtVasTotal_Amt.getText()));
			//inbean.va
			inbean.setInsurance_amount(Double.parseDouble(txtInsurance.getText()));
			inbean.setBasicAmount(Double.valueOf(txtamount.getText()));
			
			
			/*
			LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
			Date invoiceDate=Date.valueOf(local_InvoiceDate);
			
			int status=invoiceDate.compareTo(LoadGST_Rates.gst_Date_Fix);*/
			
			inbean.setCgst_Name("CGST");
			inbean.setCgst_rate(cgst_rate);
			inbean.setCgst_amt(cgst_amount);
			
			inbean.setSgst_Name("SGST");
			inbean.setSgst_rate(sgst_rate);
			inbean.setSgst_amt(sgst_amount);
			
			inbean.setIgst_Name("IGST");
			inbean.setIgst_rate(igst_rate);
			inbean.setIgst_amt(igst_amount);
			inbean.setInvoice_paid_status("UN");
			inbean.setGst_amount(inbean.getCgst_amt()+inbean.getSgst_amt()+inbean.getIgst_amt());
			inbean.setTaxable_amount(Double.valueOf(txtDiscountAddtitionalAmt.getText()));
			
			
			inbean.setGst(Double.parseDouble(txtGST.getText()));
			inbean.setGrandtotal(Double.parseDouble(txtGrandTotal.getText()));
			inbean.setRemarks(txtremarks.getText());
			inbean.setStatus("Active");
			inbean.setFrom_date(date.format(fromdate));
			inbean.setTo_date(date.format(todate));
			inbean.setInvoice_date(date.format(invoicedate));
			
			LoadInvoices.list_InvoiceData_Global.add(inbean);
			
			System.out.println(" \n++++++++++++++++++++++ From save Invoice >>>>>>>>>>>>>>>");
			
			Test();
			list_CompleteInvoiceDetails.add(inbean);
			
			/*System.err.println("Invoice No: "+inbean.getInvoice_number());
			System.err.println("Invoice Date: "+invoicedate);
			System.err.println("Client Code: "+inbean.getClientcode());
			System.err.println("Amount After Dis_Add: "+inbean.getAmountAfterDis_Add());
			System.err.println("Ins Amt: "+inbean.getInsurance_amount());
			System.err.println("Dis_Add: "+inbean.getDiscount_additional());
			System.err.println("Reason: "+inbean.getReason());
			System.err.println("Type Flat_%: "+inbean.getType_flat2percent());
			System.err.println("Rate of %: "+inbean.getRateof_percent());
			System.err.println("VAS Amt: "+inbean.getVas_amount());
			System.err.println("Dis_Add_amt: "+inbean.getDiscount_Additional_amount());
			System.err.println("Fuel: "+inbean.getFuel());
			System.err.println("From date: "+fromdate);
			System.err.println("To date: "+todate);
						
			System.err.println("Cgst Rate: "+inbean.getCgst_rate());
			System.err.println("Cgst Amt: "+inbean.getCgst_amt());
			System.err.println("Sgst Rate: "+inbean.getSgst_rate());
			System.err.println("Sgst Amt: "+inbean.getSgst_amt());
			System.err.println("Igst Rate: "+inbean.getIgst_rate());
			System.err.println("Igst amt: "+inbean.getIgst_amt());
			
			//System.err.println("Invoice No: "+inbean.getGst());
			System.err.println("Grand Total: "+inbean.getGrandtotal());
			System.err.println("Remark: "+inbean.getRemarks());
			System.err.println("Status: "+inbean.getStatus());
			
			
			*/
			
			
			/*String query = "insert into invoice(invoice_number, invoice_date,invoice2branchcode,clientcode,clientname,"
							+ "sub_clientcode,amount,insurance_amount,docket_charge,cod_amount,fod_amount,discount_additional,"
							+ "reason,type_flat_percent,rateof_percent,discount_additional_amount,fuel,from_date,to_date,"
							+ "servicetax,grandtotal,remarks,status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";*/
			
			String query = "insert into invoice(invoice_number,invoice_date,invoice2branchcode,from_date,to_date,clientcode,"
					+ "type_flat_percent,discount_additional,rateof_percent,discount_additional_amount,reason,insurance_amount,"
					+ "other_chrgs,amount,fuel,taxable_value,tax_name1,tax_rate1,tax_amount1,tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,"
					+ "grandtotal,remarks,status,invoice_status) "
					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,inbean.getInvoice_number());
			preparedStmt.setDate(2,invoicedate);
			preparedStmt.setString(3, inbean.getInvoice2branchcode());
			preparedStmt.setDate(4, fromdate);
			preparedStmt.setDate(5, todate);
			preparedStmt.setString(6, inbean.getClientcode());
			preparedStmt.setString(7, inbean.getType_flat2percent());
			preparedStmt.setString(8, inbean.getDiscount_additional());
			preparedStmt.setDouble(9, inbean.getRateof_percent());
			preparedStmt.setDouble(10, inbean.getDiscount_Additional_amount());
			preparedStmt.setString(11, inbean.getReason());
			preparedStmt.setDouble(12, inbean.getInsurance_amount());
			preparedStmt.setDouble(13, inbean.getVas_amount());
			preparedStmt.setDouble(14, inbean.getBasicAmount());
			preparedStmt.setDouble(15, inbean.getFuel());
			preparedStmt.setDouble(16, inbean.getTaxable_amount());
			preparedStmt.setString(17, inbean.getCgst_Name());
			preparedStmt.setDouble(18, inbean.getCgst_rate());
			preparedStmt.setDouble(19, inbean.getCgst_amt());
			preparedStmt.setString(20, inbean.getSgst_Name());
			preparedStmt.setDouble(21, inbean.getSgst_rate());
			preparedStmt.setDouble(22, inbean.getSgst_amt());
			preparedStmt.setString(23, inbean.getIgst_Name());
			preparedStmt.setDouble(24, inbean.getIgst_rate());
			preparedStmt.setDouble(25, inbean.getIgst_amt());
			preparedStmt.setDouble(26, inbean.getGrandtotal());
			preparedStmt.setString(27, inbean.getRemarks());
			preparedStmt.setString(28, inbean.getStatus());
			preparedStmt.setString(29, inbean.getInvoice_paid_status());
			
			
			//preparedStmt.setDouble(7, inbean.getAmountAfterDis_Add());
			
			
			//preparedStmt.setDouble(20, inbean.getGst());
			
			preparedStmt.executeUpdate();
			
			detailedtabledata.clear();
			
			updateDBT_Invoice(invoiceNumberGenerated);
			
			awbNoToInoviceGeneratedList.clear();
			invoiceNumberToPreview=invoiceNumberGenerated;
			branchCodeToGetCompanyDetail=inbean.getInvoice2branchcode();
			invoiceNumberGenerated=null;
		
			System.out.println(">>> Branch Code >> "+branchCodeToGetCompanyDetail);
			System.out.println(">>> Invoice No Code >> "+invoiceNumberToPreview);
			
			//loadInvoiceDate();
		
			System.err.println("?>?>>>?>?>?>?>?>?>?>?>?>???????????????????????   " +list_CompleteInvoiceDetails.size());
			showInvoice_CompanyDetails();
			setInvoiceDataToInvoiceTableViaList(branchcode[0]);
			Test();
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}

	
// ====================================================================================================================		

	public void updateInvoiceDate() throws SQLException
	{
			LocalDate indate=dpkInvoiceDate.getValue();
			LocalDate from=dpkFromDate.getValue();
			LocalDate to=dpkToDate.getValue();
			Date invoicedate=Date.valueOf(indate);
			Date fromdate=Date.valueOf(from);
			Date todate=Date.valueOf(to);

			InvoiceBean inbean=new InvoiceBean();

			String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
			String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			clientname_Sendmail=clientcode[1];
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();


			PreparedStatement preparedStmt=null;

			try
			{	
				/*if(rdBtnReGenerateInvoice.isSelected()==true)
				{*/
					inbean.setInvoice_number(txtInvoiceNo.getText());	
				/*}
				else
				{
					inbean.setInvoice_number(invoiceNumberGenerated);
				}*/


				inbean.setInvoice2branchcode(branchcode[0]);
				inbean.setClientcode(clientcode[0]);
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtDiscountAddtitionalAmt.getText()));

				if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
				{
					inbean.setDiscount_additional(comboBoxDisAdd.getValue());
				}
				else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
				{
					inbean.setDiscount_additional(comboBoxDisAdd.getValue());
				}

				//System.out.println("Discount additional: "+inbean.getDiscount_additional());

				if(!txtReason.getText().equals(null) && !txtReason.getText().isEmpty())
				{
					inbean.setReason(txtReason.getText());
				}
				else
				{

				}

				//System.out.println("Reason: "+inbean.getReason());

				if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
				{
					inbean.setType_flat2percent("P");
				}
				else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
				{
					inbean.setType_flat2percent("F");
				}


				//System.out.println("Flat or percent: "+inbean.getType_flat2percent());

				if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2) && !txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty())
				{
					inbean.setRateof_percent(0.0);
				}
				else if(!txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty()) 
				{
					inbean.setRateof_percent(Double.parseDouble(txtTypeAmount.getText()));
				}

				else
				{
					inbean.setRateof_percent(0.0);
				}
				//System.out.println("rate of percent: "+inbean.getRateof_percent());

				if(!txtdiscountLessAmt.getText().equals(null) && !txtdiscountLessAmt.getText().isEmpty())
				{
					inbean.setDiscount_Additional_amount(Double.parseDouble(txtdiscountLessAmt.getText()));
				}
				else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
				{
					inbean.setDiscount_Additional_amount(Double.parseDouble(txtTypeAmount.getText()));
				}
				else
				{
					inbean.setDiscount_Additional_amount(0.0);
				}
				//System.out.println("discount less amount: "+inbean.getDiscount_Additional_amount());

				inbean.setFuel(Double.parseDouble(txtFuel.getText()));
				inbean.setVas_amount(Double.valueOf(txtVasTotal_Amt.getText()));

				//inbean.setOther_amount(Double.parseDouble(txtVasTotal_Amt.getText()));
				//inbean.va
				inbean.setInsurance_amount(Double.parseDouble(txtInsurance.getText()));
				inbean.setBasicAmount(Double.valueOf(txtamount.getText()));


				/*
					LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
					Date invoiceDate=Date.valueOf(local_InvoiceDate);

					int status=invoiceDate.compareTo(LoadGST_Rates.gst_Date_Fix);*/

				inbean.setCgst_Name("CGST");
				inbean.setCgst_rate(cgst_rate);
				inbean.setCgst_amt(cgst_amount);

				inbean.setSgst_Name("SGST");
				inbean.setSgst_rate(sgst_rate);
				inbean.setSgst_amt(sgst_amount);

				inbean.setIgst_Name("IGST");
				inbean.setIgst_rate(igst_rate);
				inbean.setIgst_amt(igst_amount);
				inbean.setGst_amount(inbean.getCgst_amt()+inbean.getSgst_amt()+inbean.getIgst_amt());
				inbean.setTaxable_amount(Double.valueOf(txtDiscountAddtitionalAmt.getText()));


				inbean.setGst(Double.parseDouble(txtGST.getText()));
				inbean.setGrandtotal(Double.parseDouble(txtGrandTotal.getText()));
				inbean.setRemarks(txtremarks.getText());
				inbean.setStatus("Active");
				inbean.setFrom_date(date.format(fromdate));
				inbean.setTo_date(date.format(todate));
				inbean.setInvoice_date(date.format(invoicedate));

				
				int invNo=0;
				if(LoadInvoices.list_InvoiceData_Global.isEmpty()==false)
				{
					for(InvoiceBean bean: LoadInvoices.list_InvoiceData_Global)
					{
						
						if(txtInvoiceNo.getText().equals(bean.getInvoice_number()))
						{
							LoadInvoices.list_InvoiceData_Global.remove(invNo);
							break;
						}
						invNo++;
					}
				}
				
				LoadInvoices.list_InvoiceData_Global.add(inbean);

				System.out.println(" \n++++++++++++++++++++++ From save Invoice >>>>>>>>>>>>>>>");

				Test();
				
			 invNo=0;
				if(list_CompleteInvoiceDetails.isEmpty()==false)
				{
					for(InvoiceBean bean: list_CompleteInvoiceDetails)
					{
						
						if(txtInvoiceNo.getText().equals(bean.getInvoice_number()))
						{
							list_CompleteInvoiceDetails.remove(invNo);
							break;
						}
						invNo++;
					}
				}
				
				
				list_CompleteInvoiceDetails.add(inbean);

				/*System.err.println("Invoice No: "+inbean.getInvoice_number());
					System.err.println("Invoice Date: "+invoicedate);
					System.err.println("Client Code: "+inbean.getClientcode());
					System.err.println("Amount After Dis_Add: "+inbean.getAmountAfterDis_Add());
					System.err.println("Ins Amt: "+inbean.getInsurance_amount());
					System.err.println("Dis_Add: "+inbean.getDiscount_additional());
					System.err.println("Reason: "+inbean.getReason());
					System.err.println("Type Flat_%: "+inbean.getType_flat2percent());
					System.err.println("Rate of %: "+inbean.getRateof_percent());
					System.err.println("VAS Amt: "+inbean.getVas_amount());
					System.err.println("Dis_Add_amt: "+inbean.getDiscount_Additional_amount());
					System.err.println("Fuel: "+inbean.getFuel());
					System.err.println("From date: "+fromdate);
					System.err.println("To date: "+todate);

					System.err.println("Cgst Rate: "+inbean.getCgst_rate());
					System.err.println("Cgst Amt: "+inbean.getCgst_amt());
					System.err.println("Sgst Rate: "+inbean.getSgst_rate());
					System.err.println("Sgst Amt: "+inbean.getSgst_amt());
					System.err.println("Igst Rate: "+inbean.getIgst_rate());
					System.err.println("Igst amt: "+inbean.getIgst_amt());

					//System.err.println("Invoice No: "+inbean.getGst());
					System.err.println("Grand Total: "+inbean.getGrandtotal());
					System.err.println("Remark: "+inbean.getRemarks());
					System.err.println("Status: "+inbean.getStatus());


				 */


				/*String query = "insert into invoice(invoice_number, invoice_date,invoice2branchcode,clientcode,clientname,"
									+ "sub_clientcode,amount,insurance_amount,docket_charge,cod_amount,fod_amount,discount_additional,"
									+ "reason,type_flat_percent,rateof_percent,discount_additional_amount,fuel,from_date,to_date,"
									+ "servicetax,grandtotal,remarks,status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";*/

				String query = "update invoice set invoice_date=?,invoice2branchcode=?,from_date=?,to_date=?,clientcode=?,"
						+ "type_flat_percent=?,discount_additional=?,rateof_percent=?,discount_additional_amount=?,reason=?,insurance_amount=?,"
						+ "other_chrgs=?,amount=?,fuel=?,taxable_value=?,tax_name1=?,tax_rate1=?,tax_amount1=?,tax_name2=?,tax_rate2=?,tax_amount2=?,tax_name3=?,tax_rate3=?,tax_amount3=?,"
						+ "grandtotal=?,remarks=?,status=? where invoice_number=?";


				preparedStmt = con.prepareStatement(query);

				
				preparedStmt.setDate(1,invoicedate);
				preparedStmt.setString(2, inbean.getInvoice2branchcode());
				preparedStmt.setDate(3, fromdate);
				preparedStmt.setDate(4, todate);
				preparedStmt.setString(5, inbean.getClientcode());
				preparedStmt.setString(6, inbean.getType_flat2percent());
				preparedStmt.setString(7, inbean.getDiscount_additional());
				preparedStmt.setDouble(8, inbean.getRateof_percent());
				preparedStmt.setDouble(9, inbean.getDiscount_Additional_amount());
				preparedStmt.setString(10, inbean.getReason());
				preparedStmt.setDouble(11, inbean.getInsurance_amount());
				preparedStmt.setDouble(12, inbean.getVas_amount());
				preparedStmt.setDouble(13, inbean.getBasicAmount());
				preparedStmt.setDouble(14, inbean.getFuel());
				preparedStmt.setDouble(15, inbean.getTaxable_amount());
				preparedStmt.setString(16, inbean.getCgst_Name());
				preparedStmt.setDouble(17, inbean.getCgst_rate());
				preparedStmt.setDouble(18, inbean.getCgst_amt());
				preparedStmt.setString(19, inbean.getSgst_Name());
				preparedStmt.setDouble(20, inbean.getSgst_rate());
				preparedStmt.setDouble(21, inbean.getSgst_amt());
				preparedStmt.setString(22, inbean.getIgst_Name());
				preparedStmt.setDouble(23, inbean.getIgst_rate());
				preparedStmt.setDouble(24, inbean.getIgst_amt());
				preparedStmt.setDouble(25, inbean.getGrandtotal());
				preparedStmt.setString(26, inbean.getRemarks());
				preparedStmt.setString(27, inbean.getStatus());
				preparedStmt.setString(28,inbean.getInvoice_number());


				//preparedStmt.setDouble(7, inbean.getAmountAfterDis_Add());


				//preparedStmt.setDouble(20, inbean.getGst());

				preparedStmt.executeUpdate();

				detailedtabledata.clear();

				/*if(rdBtnReGenerateInvoice.isSelected()==true)
				{*/
					updateDBT_Invoice(txtInvoiceNo.getText());	
				/*}
				else
				{
					updateDBT_Invoice(invoiceNumberGenerated);
				}*/



				awbNoToInoviceGeneratedList.clear();
				invoiceNumberToPreview=inbean.getInvoice_number();
				branchCodeToGetCompanyDetail=inbean.getInvoice2branchcode();
				invoiceNumberGenerated=null;

				System.out.println(">>> Branch Code >> "+branchCodeToGetCompanyDetail);
				System.out.println(">>> Invoice No Code >> "+invoiceNumberToPreview);

				//loadInvoiceDate();

				System.err.println("?>?>>>?>?>?>?>?>?>?>?>?>???????????????????????   " +list_CompleteInvoiceDetails.size());
				showInvoice_CompanyDetails();
				setInvoiceDataToInvoiceTableViaList(branchcode[0]);
				Test();


			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}

	}		
	
	
// ====================================================================================================================	

	public void showInvoice_CompanyDetails() throws IOException, JRException, SQLException
	{
		ConvertAmountInWords cnvrtWrd=new ConvertAmountInWords();
		
		for(InvoiceBean inBean: list_CompleteInvoiceDetails)
		{
			if(inBean.getInvoice_number().equals(invoiceNumberToPreview))
			{
				InvoiceBean generateInvoiceBean=new InvoiceBean();
			
				String clientNameAddressPincodeGSTIN=getClientNameViaClientCode(inBean.getClientcode());
				String[] name_Address_pincode_GSTIN=clientNameAddressPincodeGSTIN.split("\\|");
				
				String ClientStateCode_Name_GstinCode=getStateNameViaClientCode(name_Address_pincode_GSTIN[2].trim());
				String[] StateCode_Name_GstinCode=ClientStateCode_Name_GstinCode.split("\\|");
				
				generateInvoiceBean.setInvoice_number(invoiceNumberToPreview);
				generateInvoiceBean.setInvoice_date(inBean.getInvoice_date());
				generateInvoiceBean.setInvoice2branchcode(inBean.getInvoice2branchcode());
				generateInvoiceBean.setClientcode(inBean.getClientcode());
				generateInvoiceBean.setClientname(name_Address_pincode_GSTIN[0].trim());
				generateInvoiceBean.setClientAddress(name_Address_pincode_GSTIN[1].trim());
				generateInvoiceBean.setClientGSTIN(name_Address_pincode_GSTIN[3].trim());
				generateInvoiceBean.setClientStateName(StateCode_Name_GstinCode[1].trim());
				generateInvoiceBean.setClient_StateGSTIN_code(StateCode_Name_GstinCode[2].trim());
				generateInvoiceBean.setFrom_date(inBean.getFrom_date());
				generateInvoiceBean.setTo_date(inBean.getTo_date());
				generateInvoiceBean.setBasicAmount(inBean.getBasicAmount());
				generateInvoiceBean.setFuel_rate(inBean.getFuel_rate());
				generateInvoiceBean.setFuel(inBean.getFuel());
				generateInvoiceBean.setVas_amount(inBean.getVas_amount());
				generateInvoiceBean.setInsurance_amount(inBean.getInsurance_amount());
				generateInvoiceBean.setType_flat2percent(inBean.getType_flat2percent());
				generateInvoiceBean.setDiscount_additional(inBean.getDiscount_additional());
				generateInvoiceBean.setRateof_percent(inBean.getRateof_percent());
				generateInvoiceBean.setDiscount_Additional_amount(inBean.getDiscount_Additional_amount());
				generateInvoiceBean.setReason(inBean.getReason());
				generateInvoiceBean.setCgst_Name(inBean.getCgst_Name());
				generateInvoiceBean.setCgst_rate(inBean.getCgst_rate());
				generateInvoiceBean.setCgst_amt(inBean.getCgst_amt());
				generateInvoiceBean.setSgst_Name(inBean.getSgst_Name());
				generateInvoiceBean.setSgst_rate(inBean.getSgst_rate());
				generateInvoiceBean.setSgst_amt(inBean.getSgst_amt());
				generateInvoiceBean.setIgst_Name(inBean.getIgst_Name());
				generateInvoiceBean.setIgst_rate(inBean.getIgst_rate());
				generateInvoiceBean.setIgst_amt(inBean.getIgst_amt());
				generateInvoiceBean.setTaxable_amount(inBean.getTaxable_amount());
				generateInvoiceBean.setGst_amount(inBean.getGst_amount());
				generateInvoiceBean.setGrandtotal(inBean.getGrandtotal());
				generateInvoiceBean.setAmountInWords(cnvrtWrd.convertToWords(generateInvoiceBean.getGrandtotal().intValue()));
				generateInvoiceBean.setAwbCount(awbCountToInvoicePrint);
				
				generateInvoiceBean.setRemarks(inBean.getRemarks());
				generateInvoiceBean.setCompanyCode(getCompanyViaBranch(inBean.getInvoice2branchcode()));
				
				for(LoadCompanyBean cmpnyBean:LoadCompany.SET_LOAD_COMPANYWITHALLDETAILS)
				{
					if(generateInvoiceBean.getCompanyCode().equals(cmpnyBean.getCompanyCode()))
					{
						generateInvoiceBean.setCompanyName(cmpnyBean.getCompanyName());
						//generateInvoiceBean.setCompanyCode(cmpnyBean.getCompanyCode());
						generateInvoiceBean.setCmpny_tagLine(cmpnyBean.getTagLine());
						generateInvoiceBean.setCmpny_address(cmpnyBean.getAddress());
						generateInvoiceBean.setCmpny_city(cmpnyBean.getCity());
						generateInvoiceBean.setCmpny_serviceTaxNo(cmpnyBean.getServiceTaxNo());
						generateInvoiceBean.setCmpny_panNo(cmpnyBean.getPanNo());
						generateInvoiceBean.setCmpny_email(cmpnyBean.getEmail());
						generateInvoiceBean.setCmpny_bankName(cmpnyBean.getBankName());
						generateInvoiceBean.setCmpny_accountNo(cmpnyBean.getAccountNo());
						generateInvoiceBean.setCmpny_ifscCode(cmpnyBean.getIfscCode());
						
						generateInvoiceBean.setCondition1(cmpnyBean.getCondition1());
						generateInvoiceBean.setCondition2(cmpnyBean.getCondition2());
						generateInvoiceBean.setCondition3(cmpnyBean.getCondition3());
						generateInvoiceBean.setCondition4(cmpnyBean.getCondition4());
						generateInvoiceBean.setCondition5(cmpnyBean.getCondition5());
						generateInvoiceBean.setCondition6(cmpnyBean.getCondition6());
						
						generateInvoiceBean.setCmpny_state(cmpnyBean.getState());
						generateInvoiceBean.setCmpny_pincode(cmpnyBean.getPincode());
						
						list_InvoiceAndCompanyDetails.add(generateInvoiceBean);
						break;
						
					}
				}
			}
		}
		
		
		System.out.println("New List size >>>> "+list_InvoiceAndCompanyDetails.size());
		
		/*for(InvoiceBean inBean: list_InvoiceAndCompanyDetails)
		{
			System.out.println("Invoice No.: "+inBean.getInvoice_number());
			System.out.println("Invoice Date: "+inBean.getInvoice_date());
			System.out.println("Branch: "+inBean.getInvoice2branchcode());
			System.out.println("Client Code: "+inBean.getClientcode());
			System.out.println("From Date: "+inBean.getFrom_date());
			System.out.println("To Date: "+inBean.getTo_date());
			System.out.println("Basic Amount: "+inBean.getBasicAmount());
			System.out.println("Fuel Rate: "+inBean.getFuel_rate());
			System.out.println("Fuel: "+inBean.getFuel());
			System.out.println("VAS Amount: "+inBean.getVas_amount());
			System.out.println("Insurance Amount: "+inBean.getInsurance_amount());
			System.out.println("Rate of Percent: "+inBean.getRateof_percent());
			System.out.println("Type Flat/Percent: "+inBean.getType_flat2percent());
			System.out.println("Dis/Add: "+inBean.getDiscount_additional());
			System.out.println("Dis/Add Amount: "+inBean.getDiscount_Additional_amount());
			System.out.println("Reason: "+inBean.getReason());
			System.out.println("Tax Name 2: "+inBean.getCgst_Name());
			System.out.println("CGST Rate: "+inBean.getCgst_rate());
			System.out.println("CGST Amount: "+inBean.getCgst_amt());
			System.out.println("Tax Name 2: "+inBean.getSgst_Name());
			System.out.println("SGST Rate: "+inBean.getSgst_rate());
			System.out.println("SGST Amount: "+inBean.getSgst_amt());
			System.out.println("Tax Name3: "+inBean.getIgst_Name());
			System.out.println("IGST Rate: "+inBean.getIgst_rate());
			System.out.println("IGST Amount: "+inBean.getIgst_amt());
			System.out.println("Taxable Amount: "+inBean.getTaxable_amount());
			System.out.println("GST Amount: "+inBean.getGst_amount());
			System.out.println("Grand Total: "+inBean.getGrandtotal());
			System.out.println("Amount In Words: "+inBean.getAmountInWords());
			System.out.println("Remarks: "+inBean.getRemarks());
			
			
			System.out.println("\n*************** Company Details ****************\n");
			
			System.out.println("Company Code: "+inBean.getCompanyCode());
			System.out.println("Company Name: "+inBean.getCompanyName());
			System.out.println("Tag Line: "+inBean.getCmpny_tagLine());
			System.out.println("Address: "+inBean.getCmpny_address());
			System.out.println("Tax No: "+inBean.getCmpny_serviceTaxNo());
			System.out.println("Pan No: "+inBean.getCmpny_panNo());
			System.out.println("Email: "+inBean.getCmpny_email());
			System.out.println("Bank Name: "+inBean.getCmpny_bankName());
			System.out.println("A/c No: "+inBean.getCmpny_accountNo());
			System.out.println("IFSC Code: "+inBean.getCmpny_ifscCode());
			System.out.println("Condi. 1: "+inBean.getCondition1());
			System.out.println("Condi. 2: "+inBean.getCondition2());
			System.out.println("Condi. 3: "+inBean.getCondition3());
			System.out.println("Condi. 4: "+inBean.getCondition4());
			System.out.println("Condi. 5: "+inBean.getCondition5());
			System.out.println("Condi. 6: "+inBean.getCondition6());
			System.out.println("Company State: "+inBean.getCmpny_state());
			System.out.println("Company Pincode: "+inBean.getCmpny_pincode());
			
			break;
		}*/
		
		showInvoiceInJasperViewer();
		
	}

// ====================================================================================================================
	
	public void showInvoiceInJasperViewer() throws IOException, JRException, SQLException
	{
	//	Test();
		
		for(InvoiceBean inBean: list_InvoiceAndCompanyDetails) 
		{
			parameters.put("condition1", inBean.getCondition1());
			parameters.put("condition2", inBean.getCondition2());
			parameters.put("condition3", inBean.getCondition3());
			parameters.put("condition4", inBean.getCondition4());
			parameters.put("condition5", inBean.getCondition5());
			parameters.put("condition6", inBean.getCondition6());
			parameters.put("panNumber", inBean.getCmpny_panNo());
			parameters.put("tagLine", inBean.getCmpny_tagLine());
			parameters.put("address", inBean.getCmpny_address());
			parameters.put("email", inBean.getCmpny_email());
			parameters.put("grandTotal", inBean.getGrandtotal());
			parameters.put("amountInWords", inBean.getAmountInWords());
			parameters.put("type", "");
			parameters.put("value", 0.0);
			parameters.put("fuelTaxName", "Fuel");
			parameters.put("CGSTName", "CGST @"+inBean.getCgst_rate());
			parameters.put("SGSTName", "SGST @"+inBean.getSgst_rate());
			parameters.put("IGSTName", "IGST @"+inBean.getIgst_rate());
			parameters.put("fuel", inBean.getFuel());
			parameters.put("CGST", inBean.getCgst_amt());
			parameters.put("SGST", inBean.getSgst_amt());
			parameters.put("IGST", inBean.getIgst_amt());
			parameters.put("amount", inBean.getBasicAmount());
			parameters.put("name", inBean.getCompanyName());
			parameters.put("taxNumber", inBean.getCmpny_serviceTaxNo());
			parameters.put("bankName", inBean.getCmpny_bankName());
			parameters.put("accountNumber", inBean.getCmpny_accountNo());
			parameters.put("IFSCCode", inBean.getCmpny_ifscCode());
			parameters.put("taxableValue", inBean.getTaxable_amount());
			parameters.put("saccode", "");
			parameters.put("compState", inBean.getCmpny_state());
			parameters.put("compStateCode", inBean.getCmpny_pincode());
			parameters.put("InvoiceNumber", inBean.getInvoice_number());
			parameters.put("dateFrom", inBean.getFrom_date());
			parameters.put("dateTo", inBean.getTo_date());
			parameters.put("InvoiceDate", inBean.getInvoice_date());
			
			parameters.put("clientName", inBean.getClientname());
			parameters.put("clientCode", inBean.getClientcode());
			parameters.put("clientAddress", inBean.getClientAddress());
			parameters.put("subClientCode", "");
			parameters.put("awbCount", inBean.getAwbCount());
			parameters.put("supplyCity", inBean.getCmpny_city());
			
			parameters.put("clientState", inBean.getClientStateName());
			parameters.put("clientStateCode", inBean.getClient_StateGSTIN_code());
			parameters.put("GSTIN", inBean.getClientGSTIN());
			
			
			/*System.out.println("Invoice No.: "+inBean.getInvoice_number());
			System.out.println("Invoice Date: "+inBean.getInvoice_date());
			System.out.println("Branch: "+inBean.getInvoice2branchcode());
			System.out.println("Client Code: "+inBean.getClientcode());
			System.out.println("From Date: "+inBean.getFrom_date());
			System.out.println("To Date: "+inBean.getTo_date());
			System.out.println("Basic Amount: "+inBean.getBasicAmount());
			System.out.println("Fuel Rate: "+inBean.getFuel_rate());
			System.out.println("Fuel: "+inBean.getFuel());
			System.out.println("VAS Amount: "+inBean.getVas_amount());
			System.out.println("Insurance Amount: "+inBean.getInsurance_amount());
			System.out.println("Rate of Percent: "+inBean.getRateof_percent());
			System.out.println("Type Flat/Percent: "+inBean.getType_flat2percent());
			System.out.println("Dis/Add: "+inBean.getDiscount_additional());
			System.out.println("Dis/Add Amount: "+inBean.getDiscount_Additional_amount());
			System.out.println("Reason: "+inBean.getReason());
			System.out.println("Tax Name 2: "+inBean.getCgst_Name());
			System.out.println("CGST Rate: "+inBean.getCgst_rate());
			System.out.println("CGST Amount: "+inBean.getCgst_amt());
			System.out.println("Tax Name 2: "+inBean.getSgst_Name());
			System.out.println("SGST Rate: "+inBean.getSgst_rate());
			System.out.println("SGST Amount: "+inBean.getSgst_amt());
			System.out.println("Tax Name3: "+inBean.getIgst_Name());
			System.out.println("IGST Rate: "+inBean.getIgst_rate());
			System.out.println("IGST Amount: "+inBean.getIgst_amt());
			System.out.println("Taxable Amount: "+inBean.getTaxable_amount());
			System.out.println("GST Amount: "+inBean.getGst_amount());
			System.out.println("Grand Total: "+inBean.getGrandtotal());
			System.out.println("Amount In Words: "+inBean.getAmountInWords());
			System.out.println("Remarks: "+inBean.getRemarks());
			
			
			System.out.println("\n*************** Company Details ****************\n");
			
			System.out.println("Company Code: "+inBean.getCompanyCode());
			System.out.println("Company Name: "+inBean.getCompanyName());
			System.out.println("Tag Line: "+inBean.getCmpny_tagLine());
			System.out.println("Address: "+inBean.getCmpny_address());
			System.out.println("Tax No: "+inBean.getCmpny_serviceTaxNo());
			System.out.println("Pan No: "+inBean.getCmpny_panNo());
			System.out.println("Email: "+inBean.getCmpny_email());
			System.out.println("Bank Name: "+inBean.getCmpny_bankName());
			System.out.println("A/c No: "+inBean.getCmpny_accountNo());
			System.out.println("IFSC Code: "+inBean.getCmpny_ifscCode());
			System.out.println("Condi. 1: "+inBean.getCondition1());
			System.out.println("Condi. 2: "+inBean.getCondition2());
			System.out.println("Condi. 3: "+inBean.getCondition3());
			System.out.println("Condi. 4: "+inBean.getCondition4());
			System.out.println("Condi. 5: "+inBean.getCondition5());
			System.out.println("Condi. 6: "+inBean.getCondition6());
			System.out.println("Company State: "+inBean.getCmpny_state());
			System.out.println("Company Pincode: "+inBean.getCmpny_pincode());
			*/
			
			System.out.println("Invoice Date >> "+inBean.getInvoice_date());
			
			LocalDate localFromDate=LocalDate.parse(inBean.getFrom_date(), localdateformatter);
			Date fromDate=Date.valueOf(localFromDate);
			LocalDate loacalToDate=LocalDate.parse(inBean.getTo_date(), localdateformatter);
			Date toDate=Date.valueOf(loacalToDate);
			
			summarySQL="select air_way_bill_number,master_awbno,booking_date,dailybookingtransaction2city,billing_weight,packets,amount,"
					+ "dox_nondox from dailybookingtransaction where booking_date between '"+fromDate+"' and '"+toDate+"' and "
					+ "invoice_number='"+invoiceNumberToPreview+"'";
			
			
			
			System.out.println(summarySQL);
			break;
		}				
		
		new GenerateSaveReport().exportReport(summarySQL,checkBoxStatus = String.valueOf(chkExportToExcel.isSelected()), src, parameters);
		
		comboBoxClientCode.requestFocus();
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		comboBoxClientCode.getSelectionModel().clearSelection();
		txtamount.clear();
		txtFuel.clear();
		txtGrandTotal.clear();
		txtSubTotal.clear();
		txtDiscountAddtitionalAmt.clear();
		txtdiscountLessAmt.clear();
		txtGST.clear();
		txtInsurance.clear();
		txtReason.clear();
		txtTypeAmount.clear();
		txtVasTotal_Amt.clear();
		txtremarks.clear();
		comboBoxDisAdd.setDisable(true);
		list_InvoiceAndCompanyDetails.clear();
		list_CompleteInvoiceDetails.clear();
		invoiceNumberToPreview=null;
		invoiceNumberGenerated=null;
		
		
		cgst_amount=0;
		sgst_amount=0;
		igst_amount=0;
		cgst_rate=0;
		sgst_rate=0;
		igst_rate=0;
		
		invoiceNumberGenerated=null;
		invoiceNumberToPreview=null;
		summarySQL=null;
		awbCountToInvoicePrint=0;
		chkBill.setSelected(false);
		
		if(LoadInvoices.list_InvoiceData_Global.isEmpty()==true)
		{
			new LoadInvoices().loadInvoiceDate();
		}
		
		//public String summarySQL_part2=null;
		
				
	}
	
// ====================================================================================================================
	
	public String getCompanyViaBranch(String branchName)
	{
		String cmpnyCode=null;
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{	
			if(branchName.equals(branchBean.getBranchCode()))
			{
				cmpnyCode=branchBean.getCmpnyCode();
				break;
			}
		}
		return cmpnyCode;
	}
	
// ====================================================================================================================
	
	public String getClientNameViaClientCode(String clientcode)
	{
		String clientName_Address_pincode_gstinNo=null;
		System.out.println("Client code 1 >>>>>>> "+clientcode);
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientcode.equals(clBean.getClientCode()))
			{
				clientName_Address_pincode_gstinNo=clBean.getClientName()+" | "+clBean.getAddress()+" | "+clBean.getPincode()+" | "+clBean.getClient_gstin_number();
				break;
			}
		}
		return clientName_Address_pincode_gstinNo;
	}
	
// ====================================================================================================================
	
	public String getStateNameViaClientCode(String pincode)
	{
		String stateName=null;
		String stateCode=null;
		String state_gstin_code=null;
		
		
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pincode.equals(pinBean.getPincode()))
			{
				stateCode=pinBean.getState_code();
				state_gstin_code=pinBean.getState_gstin_code();
				break;
			}
		}
		
		for(LoadCityBean stateBean: LoadState.SET_LOAD_STATEWITHNAME)
		{
			if(stateCode.equals(stateBean.getStateCode()))
			{
				stateName=stateBean.getStateName();
				break;
			}
		}
		return stateCode+" | "+stateName+" | "+state_gstin_code;
	}	

// ====================================================================================================================	
	
	/*public void getClientsViaBranchAndDateRange() throws SQLException
	{
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		String[] branchCode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;
		
		int pcsCount=0;
		
		try 
		{
			String sql = "select dailybookingtransaction2client from dailybookingtransaction where  dailybookingtransaction2branch=? and "
					+ "invoice_number IS NULL and mps_type='T' and travel_status='dataentry' and booking_date between ? and ? "
					+ "group by dailybookingtransaction2client";
				
			preparedStmt = con.prepareStatement(sql);
			
			preparedStmt.setString(1, branchCode[0]);
			preparedStmt.setDate(2, stdate);
			preparedStmt.setDate(2, edate);
			
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
		
			} 
			else 
			{
				list_ClientsViaBranchAndDateRange.clear();
				do 
				{
					list_ClientsViaBranchAndDateRange.add(rs.getString("dailybookingtransaction2client"));
				} 
				while (rs.next());
			}
		
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
// ====================================================================================================================
	
	public void invoiceGeneratedAwbNo() throws SQLException
	{
		InvoiceBean inbean=new InvoiceBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		inbean.setInvoice_status("Invoice Generated");
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			for(String awbno: awbNoToInoviceGeneratedList){
			
			String query = "update dailybookingtransaction set invoice_status=? where air_way_bill_number=?";
			System.out.println("from ============= "+awbno+" "+inbean.getInvoice_status());	
			//System.out.println(inbean.);
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,inbean.getInvoice_status());
			preparedStmt.setString(2,awbno);
			
			preparedStmt.executeUpdate();
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
// ====================================================================================================================	

	public void rangeFilter() throws SQLException
	{
		if(chkFilter.isSelected()==true)
		{
			txtFrom.setDisable(false);
			txtTo.setDisable(false);
			txtNew.setDisable(false);
		}
		else
		{
			txtFrom.clear();
			txtTo.clear();
			txtNew.clear();
			
			txtFrom.setDisable(true);
			txtTo.setDisable(true);
			txtNew.setDisable(true);
			//showWithoutInvoiceTable(updateinvoice);
		}
	}
	
	
// ====================================================================================================================

	public void loadBranch() throws SQLException
	{	
		
		new	LoadBranch().loadBranchWithName();
		
		System.out.println("Network Compare Contorller >> value from LoadBranch Class: >>>>>>>> ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			System.out.println("Company from branch: >> "+branchBean.getCmpnyCode());
			
			comboBoxBranchItems.add(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());
		}
		comboBoxBranchCode.setItems(comboBoxBranchItems);
		
		/*for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			comboBoxBranchCode.setValue(branchBean.getBranchName()+ " | "+branchBean.getBranchCode());
			GenerateInvoiceByBranch branchInvoice= new GenerateInvoiceByBranch();
			branchInvoice.getInvoiceByBranch(comboBoxBranchCode.getValue());
			txtInvoiceNo.setText(branchInvoice.getLatestInvoiceNo());
			//txtInvoiceNo.setText("");
			//txtInvoiceNo.clear();
			System.out.println("Text box invoice number from load branch : =-=================== "+txtInvoiceNo.getText());
			if(txtInvoiceNo.getText()!=null)
			{
				txtInvoiceNo.setEditable(false);
			}else
			{
				txtInvoiceNo.setEditable(true);
			}
			//txtInvoiceNo.setText(invcBean.getInvoice_number());
			//checkInvoice();
			break;
		}*/
		
		
	}
	
	
// ====================================================================================================================

	public void loadClients() throws SQLException
	{
		
		String[] branchCode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		
		//GenerateInvoiceByBranch branchInvoice= new GenerateInvoiceByBranch();
		//branchInvoice.getInvoiceByBranch(branchCode[0]);
		//txtInvoiceNo.setText(branchInvoice.getLatestInvoiceNo());

		System.out.println("Text box invoice number : =-=================== "+txtInvoiceNo.getText());
		if(txtInvoiceNo.getText()!=null)
		{
			txtInvoiceNo.setEditable(false);
		}else
		{
			txtInvoiceNo.setEditable(true);
		}
		//loadInvoiceDate();
		
		setInvoiceDataToInvoiceTableViaList(branchCode[0]);
		
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
		Set<String> setclient = new HashSet<String>();
		System.out.println("Before Try >>>");
		
		try
		{	
			System.out.println(" Load Branch  >>>>>>>> 1");
			
			 if(!branchCode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
			 {
				 System.out.println(" Load Branch  >>>>>>>> 2");
				 comboBoxClientCode.getItems().clear();
				// comboBoxSubClient.getItems().clear();
				/*sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranchCode.getValue()+"' and booking_date between '"+stdate+"' and '"+edate+"'";*/
				
				sql="select dbt.dailybookingtransaction2client,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and dbt.dailybookingtransaction2Branch='"+branchCode[0]+"' and invoice_number IS NULL and dbt.mps_type='T' and booking_date between '"+stdate+"' and '"+edate+"' group by dbt.dailybookingtransaction2client,cm.name";
			 }
			else
			{
				System.out.println(" Load Branch  >>>>>>>> 3");
				//comboBoxSubClient.setDisable(true);
				sql="select cm.code,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code and travel_status='dataentry'  and booking_date between '"+stdate+"' and '"+edate+"' group by dbt.dailybookingtransaction2client,cm.name";
			}
			
			 System.out.println(" Load Branch  >>>>>>>>4");
			setclient.clear();
			 
			st=con.createStatement();
			rs=st.executeQuery(sql);
			System.out.println("Branch Sql: "+sql);
			System.out.println(" Load Branch  >>>>>>>> 5");
			if(!rs.next())
			{
				btnBrowse.setDisable(true);
				//btnLoad.setDisable(true);
				txtBrowseExcelFile.setDisable(true);
				txtBrowseExcelFile.clear();
				
				System.out.println(" Load Branch  >>>>>>>> 6" + branchCode[0]);
				if(branchCode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
				{
					
				}
				else
				{
					System.out.println(" Load Branch  >>>>>>>> 7");
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				
				comboBoxBranchCode.requestFocus();
				comboBoxClientCode.getItems().clear();
				}
				//comboBoxSubClient.setDisable(true);
			}
			else
			{
				/*btnBrowse.setDisable(false);
				btnLoad.setDisable(false);
				txtBrowseExcelFile.setDisable(false);*/
				
				comboBoxClientCode.setPromptText("select client");
				
				do
				{
					
					//list_ClientsViaBranchAndDateRange.add(rs.getString(1)+" | "+ rs.getString(2));
					setclient.add(rs.getString(1)+" | "+ rs.getString(2));
				}
				while(rs.next());
			
			comboBoxClientCode.getItems().clear();
			/*
			for(String s:list_ClientsViaBranchAndDateRange)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClientCode.setItems(comboBoxClientItems);*/
			
			
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClientCode.setItems(comboBoxClientItems);
			
			dpkFromDate.setEditable(false);
			
			dpkFromDate.setOnMouseClicked(e -> {
				 if(comboBoxClientItems.isEmpty()==false)
			        	dpkFromDate.hide();
			    });
			
			dpkToDate.setEditable(false);
			
			dpkToDate.setOnMouseClicked(e -> {
				 if(comboBoxClientItems.isEmpty()==false)
			        	dpkToDate.hide();
			    });
			
			comboBoxBranchCode.setOnMouseClicked(e -> {
		        if(comboBoxClientItems.isEmpty()==false)
		        	comboBoxBranchCode.hide();
		    });
			
			//loadSubClients();
			
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}
		
		
	}
	

	
// ====================================================================================================================
	
	public void loadSubClients() throws SQLException
	{
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
	
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
	
		Set<String> setsubclient = new HashSet<String>();
	
		try
		{	
			
			 if(!comboBoxBranchCode.getValue().equals(null))
			 {
				sql="select sub_client_code from dailybookingtransaction where  dailybookingtransaction2client='"+clientcode[0]+"' and booking_date between '"+stdate+"' and '"+edate+"' group by sub_client_code";
							
			 }
			else if(!comboBoxBranchCode.getValue().equals(InvoiceUtils.ALLBRANCH) && !comboBoxBranchCode.getValue().equals(null))
			{
				sql="select sub_client_code from dailybookingtransaction where dailybookingtransaction2Branch='"+comboBoxBranchCode.getValue()+"' and dailybookingtransaction2client='"+clientcode[0]+"' and booking_date between '2017-03-27' and '2017-04-10' group by sub_client_code";
			}
			
			setsubclient.clear();
			 
			//System.out.println("sql from client data: "+sql);
			
			st=con.createStatement();
			
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				/*Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();*/
				
				//comboBoxBranchCode.requestFocus();
				//comboBoxClientCode.getItems().clear();
			
			}
			else
			{
				//comboBoxSubClient.setPromptText("select sub client");
			do
			{
				setsubclient.add(rs.getString(1));
			}while(rs.next());
			
			//comboBoxSubClient.getItems().clear();
			
			comboBoxSubClientItems.add(InvoiceUtils.SUBCLIENT2);
			
			for(String s:setsubclient)
			{
				if(!s.equals(""))
				{
					comboBoxSubClientItems.add(s);
				}
				else
				{
					comboBoxSubClientItems.add("Default");
				}
			}
			
			//comboBoxSubClient.setItems(comboBoxSubClientItems);
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}
	
	}
	
// ====================================================================================================================	
	
	public void showDetails() throws SQLException
	{
		String[] clientcode=null;
		
		if(comboBoxClientCode.getValue()!=null)
		{
			
			
			String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		
			LocalDate gstEffectiveDate=dpkInvoiceDate.getValue();
			LocalDate from=dpkFromDate.getValue();
			LocalDate to=dpkToDate.getValue();
			Date stdate=Date.valueOf(from);
			Date edate=Date.valueOf(to);
			Date dateForGST_Rates=Date.valueOf(gstEffectiveDate);
			//String joinsql="";
			String sql="";
			String branchValue="";
			String clientSubClient="";
				
			try
			{
				if(!branchcode[0].equals(InvoiceUtils.ALLBRANCH.replaceAll("\\s+","")))
				{
					branchValue=" dbt.dailybookingtransaction2branch='"+branchcode[0]+"' and ";
				}
				
				if(!comboBoxClientCode.getValue().equals(null))
				{
					clientSubClient="dbt.dailybookingtransaction2client='"+clientcode[0]+"' and ";
				}
				
				//sql=branchValue+clientSubClient+" dbt.invoice_number IS NULL OR dbt.invoice_number='' and mps_type='T' and dbt.booking_date between '"+stdate+"' and '"+edate+"'";
				sql=" topay_client is null and "+branchValue+clientSubClient+" dbt.invoice_number IS NULL and mps_type='T' and dbt.travel_status='dataentry' and dbt.booking_date between '"+stdate+"' and '"+edate+"'";
				System.out.println("Sub query : "+sql);
				
				updateinvoice=sql;
				
				/*LoadGST_Rates loadGST=new LoadGST_Rates();
				loadGST.loadGST_OnChangeDate(dateForGST_Rates);*/
				
				getData(sql,branchcode[0],clientcode[0]);
			
				if(!comboBoxClientCode.getValue().equals(null))
				{
					chkBill.setDisable(false);
				}
				else
				{
					chkBill.setDisable(true);
				}
				
				loadAwbNumbersToGenerateInvoice(sql);
				
				//showWithoutInvoiceTable(sql);
			}	
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
		}
				
	}


// ====================================================================================================================	

	public void getData(String sql,String branchcode,String clientCode) throws SQLException
	{
		
		System.out.println("Client Code: "+clientCode+" | Branch Code: "+branchcode);
		double gst_amount=0;
		//j
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		
	//	new LoadClients().loadClientWithName();
		for(com.onesoft.courier.common.bean.LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientCode.equals(clBean.getClientCode()))
			{
				client_City=clBean.getClient_city();
				branch_Code_If_All_Branch_selected=clBean.getBranchcode();
				//branch_City_If_All_Branch_selected=clBean.getClient_city();
				break;
			}
		}

		//new LoadBranch().loadBranchWithName();
		System.out.println("Branch value in case ALl BRanch : "+branchcode);
		if(branchcode.equals("AllBranches"))
		{
			for(LoadBranchBean brBean:LoadBranch.SET_LOADBRANCHWITHNAME)
			{
				if(branch_Code_If_All_Branch_selected.equals(brBean.getBranchCode()))
				{
					branch_City=brBean.getBranchCity();
					break;
				}
			}
		}
		else
		{
			for(LoadBranchBean brBean:LoadBranch.SET_LOADBRANCHWITHNAME)
			{
				if(branchcode.equals(brBean.getBranchCode()))
				{
					branch_City=brBean.getBranchCity();
					break;
				}
			}
		}
		
		//new LoadCity().loadCityWithNameFromPinCodeMaster();
		//System.out.println("City List size: "+LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER.size());
		
		for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
		{
		/*for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{*/
			
			if(branch_City.equals(pinBean.getCity_name()))
			{
				branch_State=pinBean.getState_code();
				break;
			}
		}
		
		
		for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
		{
		/*for(LoadCityBean cityBean: LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{*/
			if(client_City.equals(pinBean.getCity_name()))
			{
				System.out.println("Client State: "+pinBean.getState_code());
				client_State=pinBean.getState_code();
				break;
			}
		}

		System.out.println("Branch State: >> "+branch_State+" | City >> "+branch_City);
		System.out.println("Client State: >> "+client_State+" | City >> "+client_City);
		
		InvoiceBean inbean=new InvoiceBean();
		
		try
		{
			st=con.createStatement();
			finalsql="select sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance_amt,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_total) as vas_amt,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as gst_amt,sum(dbt.total) as total from dailybookingtransaction as dbt where "+sql;
			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
				
			
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amt"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas_amt"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setGst(Double.parseDouble(df.format(rs.getDouble("gst_amt"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						
				}while(rs.next());
			}
			
			
			txtamount.setText(String.valueOf(inbean.getBasicAmount()));
			txtFuel.setText(String.valueOf(inbean.getFuel()));
			
			System.out.println("VAS >> "+inbean.getVas_amount()+" | COD >> "+inbean.getCod_amount()+" | FOD >> "+inbean.getFod_amount()+" | Docket >> "+inbean.getDocket_charge());
			
			double vas_total=inbean.getVas_amount()+inbean.getCod_amount()+inbean.getFod_amount()+inbean.getDocket_charge();
			
			txtVasTotal_Amt.setText(df.format(vas_total));
			
	
				
			txtInsurance.setText(String.valueOf(inbean.getInsurance_amount()));
			txtSubTotal.setText(df.format(inbean.getBasicAmount()+inbean.getFuel()+vas_total+inbean.getInsurance_amount()));
			if(Double.valueOf(txtSubTotal.getText())==0)
			{
				comboBoxDisAdd.setDisable(true);
			}
			else
			{
				comboBoxDisAdd.setDisable(false);
			}
			
			txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
			
			
			gst_amount_old=inbean.getGst();
			txtGST.setText(df.format(inbean.getGst()));
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
			if(Double.valueOf(txtSubTotal.getText())==0 || txtSubTotal.getText().equals(null))
			{
				comboBoxDisAdd.setDisable(true);
			}
			else if(!txtSubTotal.getText().equals(null))
			{
				comboBoxDisAdd.setDisable(false);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	//loadInvoiceDate();
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================
	
	/*public void loadInvoiceDate() throws SQLException
	{
		invoicetabledata.clear();
		list_CompleteInvoiceDetails.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		//String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		int serialno=1;
		try
		{
			st=con.createStatement();
			//finalsql="select invoice_number,invoice_date,clientcode,from_date,to_date,grandtotal,remarks from invoice where invoice2branchcode='"+comboBoxBranchCode.getValue()+"' order by invoice_number";
			
			//finalsql="select invoice_date,invoice_number,invoice2branchcode,clientcode,from_date,to_date,amount,fuel_rate,fuel,other_chrgs,insurance_amount,"
			//			+ "type_flat_percent,discount_Additional,rateof_percent,discount_additional_amount,reason,tax_name1,tax_rate1,tax_amount1,"
			//			+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,taxable_value,(tax_amount1+tax_amount2+tax_amount3) as gst_amount,"
			//			+ "grandtotal as total, remarks from invoice where invoice2branchcode='"+branchcode[0]+"' order by invoice_number DESC limit 50";
			
			finalsql="select invoice_date,invoice_number,invoice2branchcode,clientcode,from_date,to_date,amount,fuel_rate,fuel,other_chrgs,insurance_amount,"
					+ "type_flat_percent,discount_Additional,rateof_percent,discount_additional_amount,reason,tax_name1,tax_rate1,tax_amount1,"
					+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,taxable_value,(tax_amount1+tax_amount2+tax_amount3) as gst_amount,"
					+ "grandtotal as total, remarks,status from invoice order by invoice_number DESC";
			
			
			System.out.println("Invoice Table: "+finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					InvoiceBean inbean=new InvoiceBean();
					inbean.setSerieano(serialno);
					inbean.setInvoice_date(date.format(rs.getDate("invoice_date")));
					inbean.setInvoice_number(rs.getString("invoice_number"));
					inbean.setInvoice2branchcode(rs.getString("invoice2branchcode"));
					inbean.setClientcode(rs.getString("clientcode"));
					inbean.setFrom_date(date.format(rs.getDate("from_date")));
					inbean.setTo_date(date.format(rs.getDate("to_date")));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setFuel_rate(Double.parseDouble(df.format(rs.getDouble("fuel_rate"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("other_chrgs"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
					inbean.setType_flat2percent(rs.getString("type_flat_percent"));
					inbean.setDiscount_additional(rs.getString("discount_Additional"));
					inbean.setRateof_percent(Double.parseDouble(df.format(rs.getDouble("rateof_percent"))));
					inbean.setDiscount_Additional_amount(Double.parseDouble(df.format(rs.getDouble("discount_additional_amount"))));
					inbean.setReason(rs.getString("reason"));
					
					inbean.setCgst_Name(rs.getString("tax_name1"));
					inbean.setCgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate1"))));
					inbean.setCgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount1"))));

					inbean.setSgst_Name(rs.getString("tax_name2"));
					inbean.setSgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate2"))));
					inbean.setSgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount2"))));

					inbean.setIgst_Name(rs.getString("tax_name3"));
					inbean.setIgst_rate(Double.parseDouble(df.format(rs.getDouble("tax_rate3"))));
					inbean.setIgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount3"))));
					
					inbean.setTaxable_amount(Double.parseDouble(df.format(rs.getDouble("taxable_value"))));
					inbean.setGst_amount(Double.parseDouble(df.format(rs.getDouble("gst_amount"))));
					inbean.setGrandtotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					inbean.setRemarks(rs.getString("remarks"));
					inbean.setStatus(rs.getString("status"));
					
					LoadInvoices.list_InvoiceData_Global.add(inbean);
					
					System.out.println(" \n++++++++++++++++++++++ From Load Invoice >>>>>>>>>>>>>>>");
					
					Test();
					
					//list_CompleteInvoiceDetails.add(inbean);
										
					//invoicetabledata.add(new InvoiceBillJavaFXBean(inbean.getSerieano(),inbean.getInvoice_number(), inbean.getInvoice_date(),
					//		inbean.getClientcode(), inbean.getFrom_date(), inbean.getTo_date(),inbean.getBasicAmount(), inbean.getFuel(),
					//		inbean.getVas_amount(), inbean.getInsurance_amount(), inbean.getTaxable_amount(), inbean.getGst_amount(),
					//		inbean.getGrandtotal(), inbean.getRemarks()));
					
					serialno++;
					
				}
				while(rs.next());
			}
			
		//	invoiceTable.setItems(invoicetabledata);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
// ====================================================================================================================
	
	public void setInvoiceDataToInvoiceTableViaList(String branchcode)
	{
		//String[] branchcode=comboBoxBranchCode.getValue().replaceAll("\\s+","").split("\\|");
		invoicetabledata.clear();
		
		System.out.println("Invoice table loaded >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		int serialno=1;
		
		for(InvoiceBean inbean: LoadInvoices.list_InvoiceData_Global)
		{
			if(branchcode.equals(inbean.getInvoice2branchcode()) && inbean.getStatus().equals("Active"))
			{
				
				invoicetabledata.add(new InvoiceBillJavaFXBean(serialno,inbean.getInvoice_number(), inbean.getInvoice_date(),
					inbean.getClientcode(), inbean.getFrom_date(), inbean.getTo_date(),inbean.getBasicAmount(), inbean.getFuel(),
					inbean.getVas_amount(), inbean.getInsurance_amount(), inbean.getTaxable_amount(), inbean.getGst_amount(),
					inbean.getGrandtotal(), inbean.getRemarks()));
				serialno++;
			}
		}
		
		invoiceTable.setItems(invoicetabledata);
	}
	
	
	
// ====================================================================================================================	
	
	public void loadAwbNumbersToGenerateInvoice(String sql) throws SQLException
	{
		detailedtabledata.clear();
		list_DetailedTableData.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		Set<String> networkset = new HashSet<String>();
		Set<String> serviceset = new HashSet<String>();
		Set<String> dnset = new HashSet<String>();
		
		
		
		//InvoiceBean inbean=new InvoiceBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			
			finalsql="select master_awbno,booking_date,dailybookingtransaction2branch,dailybookingtransaction2client,billing_weight,"
					+ "packets,amount,insurance_amount,docket_charge,vas_total,cod_amount,fod_amount,tax_amount1,tax_amount2,tax_amount3,fuel,total from dailybookingtransaction where "+sql.replaceAll("dbt.", "");


		/*	finalsql="select sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance_amt,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_total) as vas_amt,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as gst_amt,sum(dbt.total) as total from dailybookingtransaction as dbt where "+sql;*/
			
			/*finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
					+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
					+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and dbt.invoice_number='' and "+sql+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
							+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";*/
			System.out.println("From detailed table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			System.out.println("resultset size is : ======================  "+rs.getRow());
			
			if(!rs.next())
			{
				btnBrowse.setDisable(true);
			//	btnLoad.setDisable(true);
				txtBrowseExcelFile.setDisable(true);
				txtBrowseExcelFile.clear();
			}
			else
			{
				btnBrowse.setDisable(false);
				//btnLoad.setDisable(false);
				txtBrowseExcelFile.setDisable(false);
				txtBrowseExcelFile.clear();
				
				do
				{	
					InvoiceBean inbean=new InvoiceBean();
					/*networkset.add(rs.getString("dailybookingtransaction2network"));
					serviceset.add(rs.getString("dailybookingtransaction2service"));
					dnset.add(rs.getString("dox_nondox"));*/
					
					awbNoToInoviceGeneratedList.add(rs.getString("master_awbno"));
					
					
					
					inbean.setSerieano(serialno);
					inbean.setAbw_no(rs.getString("master_awbno"));
					System.out.println("Awb No >>>>>>>>> "+inbean.getAbw_no());
					inbean.setBooking_date(date.format(rs.getDate("booking_date")));
					inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
					inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
					inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("billing_weight"))));
					inbean.setPcs(rs.getInt("packets"));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod_amount"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod_amount"))));
					inbean.setCgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount1"))));
					inbean.setSgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount2"))));
					inbean.setIgst_amt(Double.parseDouble(df.format(rs.getDouble("tax_amount3"))));
					
					inbean.setGst_amount(inbean.getCgst_amt()+inbean.getSgst_amt()+inbean.getIgst_amt());
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas_total"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					
					
					
					list_DetailedTableData.add(inbean);
					
					detailedtabledata.add(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
							inbean.getInvoice2branchcode(), inbean.getClientcode(), inbean.getBilling_weight(), inbean.getPcs(), 
							inbean.getBasicAmount(), inbean.getInsurance_amount(), inbean.getDocket_charge(), inbean.getVas_amount(),
							inbean.getFuel(), inbean.getTotal()));
						
					serialno++;
					
				}while(rs.next());
				
				
				System.err.println("Detail Size: >>          "+detailedtabledata.size());
				
				awbCountToInvoicePrint=detailedtabledata.size();
				detailedtable.setItems(detailedtabledata);
				//showSummary(sql, networkset, serviceset, dnset);
			}
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================	

	
	/*public void showWithoutInvoiceTable(String sql) throws SQLException
	{
		detailedtabledata.clear();
		awbNoToInoviceGeneratedList.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		Set<String> networkset = new HashSet<String>();
		Set<String> serviceset = new HashSet<String>();
		Set<String> dnset = new HashSet<String>();
		
		
		
		InvoiceBean inbean=new InvoiceBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
					+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
					+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and dbt.invoice_number='' and "+sql+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
							+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";
			System.out.println("From detailed table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			System.out.println("resultset size is : ======================  "+rs.getRow());
			
			if(!rs.next())
			{
			
				detailedtabledata.clear();
				labSummaryNetwork.setText("Networks:-");
				labSummaryService.setText("Services:-");
				labSummaryDoxNonDox.setText("D/N:-");
				
				chkBill.setDisable(true);
				txtamount.clear();
				//txtCOD.clear();
			//	txtFOD.clear();
				txtInsurance.clear();
			//	txtDocket.clear();
				txtFuel.clear();
				txtVasTotal_Amt.clear();
				txtSubTotal.clear();
				txtDiscountAddtitionalAmt.clear();
				txtGST.clear();
				txtGrandTotal.clear();
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Data not found");
				alert.setHeaderText(null);
				alert.setContentText("Invoice already Generated / Data not found for the Given detail...!");
				alert.showAndWait();
				
			}
			else
			{
				do
				{
					
					networkset.add(rs.getString("dailybookingtransaction2network"));
					serviceset.add(rs.getString("dailybookingtransaction2service"));
					dnset.add(rs.getString("dox_nondox"));
					
					awbNoToInoviceGeneratedList.add(rs.getString("air_way_bill_number"));
					
					inbean.setSerieano(serialno);
					inbean.setAbw_no(rs.getString("air_way_bill_number"));
					inbean.setBooking_date(date.format(rs.getDate("booking_date")));
					inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
					inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
					inbean.setSub_clientcode(rs.getString("sub_client_code"));
					inbean.setFwd_weight(Double.parseDouble(df.format(rs.getDouble("fwd_weight"))));
					inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("weight"))));
					inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
					inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setGst(Double.parseDouble(df.format(rs.getDouble("tax"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					
					detailedtabledata.add(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
											inbean.getInvoice2branchcode(),inbean.getClientcode(), inbean.getSub_clientcode(), inbean.getFwd_weight(), inbean.getBilling_weight(),
											inbean.getBasicAmount(),inbean.getInsurance_amount() , inbean.getDocket_charge(), inbean.getCod_amount(),
											inbean.getFod_amount(), inbean.getVas_amount(), inbean.getFuel(), inbean.getGst(), inbean.getTotal()));
						
					
					serialno++;
					
				}while(rs.next());
				
				detailedtable.setItems(detailedtabledata);
				showSummary(sql, networkset, serviceset, dnset);
			}
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
		
	}*/
	
	
	
// ====================================================================================================================	

	public void showDetaildTableData() throws SQLException
	{
			
		if(txtTo.getText().equals("."))
		{
		}
		else if(txtTo.getText().equals(""))
		{
		//System.out.println("blank textfield");
		}
		else
		{
			detailedtabledata.clear();
			awb.clear();
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
				
			ResultSet rs=null;
			Statement st=null;
			String finalsql=null;
			
			Set<String> networkset = new HashSet<String>();
			Set<String> serviceset = new HashSet<String>();
			Set<String> dnset = new HashSet<String>();
			
			
			InvoiceBean inbean=new InvoiceBean();
			int serialno=1;
			try
			{
				st=con.createStatement();
				finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
						+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
						+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
						+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and bcdi.billing_weight between '"+txtFrom.getText()+"' and '"+txtTo.getText()+"' and "+updateinvoice+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
								+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";
				
				
				//summarySQL=finalsql;
				//System.out.println("From detailed table: "+finalsql);
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
				
					detailedtabledata.clear();
					labSummaryNetwork.setText("Networks:-");
					labSummaryService.setText("Services:-");
					labSummaryDoxNonDox.setText("D/N:-");
					
					chkBill.setDisable(true);
					txtamount.clear();
					//txtCOD.clear();
					//txtFOD.clear();
					txtInsurance.clear();
					//txtDocket.clear();
					txtFuel.clear();
					txtVasTotal_Amt.clear();
					txtSubTotal.clear();
					txtDiscountAddtitionalAmt.clear();
					txtGST.clear();
					txtGrandTotal.clear();
					
					/*Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Data not found");
					alert.setHeaderText(null);
					alert.setContentText("Invoice already Generated for the Given detail...!");
					alert.showAndWait();*/
				
					
					
				}
				else
				{
					do
					{
					
						awb.add(rs.getString("air_way_bill_number"));
						
						networkset.add(rs.getString("dailybookingtransaction2network"));
						serviceset.add(rs.getString("dailybookingtransaction2service"));
						dnset.add(rs.getString("dox_nondox"));
						
						
						inbean.setSerieano(serialno);
						inbean.setAbw_no(rs.getString("air_way_bill_number"));
						inbean.setBooking_date(date.format(rs.getDate("booking_date")));
						inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
						inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
						inbean.setSub_clientcode(rs.getString("sub_client_code"));
						inbean.setFwd_weight(Double.parseDouble(df.format(rs.getDouble("fwd_weight"))));
						inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("weight"))));
						inbean.setBasicAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
						inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
						inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
						inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
						inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
						inbean.setVas_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
						inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
						inbean.setGst(Double.parseDouble(df.format(rs.getDouble("tax"))));
						inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						
					/*	detailedtabledata.add(new InvoiceTableBean(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
								inbean.getInvoice2branchcode(), inbean.getClientcode(), inbean.getBilling_weight(), inbean.getPcs(), 
								inbean.getBasicAmount(), inbean.getInsurance_amount(), inbean.getDocket_charge(), inbean.getVas_amount(),
								inbean.getFuel(), inbean.getTotal())));*/
							
						
						serialno++;
						
					}while(rs.next());
					
					
					System.err.println("Detail Size: >>          "+detailedtabledata.size());
					
					awbCountToInvoicePrint=detailedtabledata.size();
					
					detailedtable.setItems(detailedtabledata);
					printAWB();
					showSummary(updateinvoice, networkset, serviceset, dnset);
				}
				
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally
			{	
				dbcon.disconnect(null, st, rs, con);
				System.out.println("Detailed table size: ========= "+detailedtabledata.size());
			}
			}
		}	
	
	
// ====================================================================================================================

	public void printAWB()
	{
		for(String a:awb)
		{
			System.out.println("awb: "+a);
		}
		
		
	}

// ====================================================================================================================
		
		
		
	public void updateWeightFilter() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
	/*	if(txtFrom.getText().equals(null) || txtFrom.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("From Textfield is blank...!!");
			alert.showAndWait();
			txtFrom.clear();
			txtFrom.requestFocus();
		}
		else if (!txtFrom.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("From Textfield: please enter only numeric values!!");
			alert.showAndWait();
			txtFrom.clear();
			txtFrom.requestFocus();
		}
		else if(txtTo.getText().equals(null) || txtTo.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("To Textfield is blank...!!");
			alert.showAndWait();
			txtTo.clear();
			txtTo.requestFocus();
		}
		else if (!txtTo.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("To Textfield: please enter only numeric values!!");
			alert.showAndWait();
			txtTo.clear();
			txtTo.requestFocus();
		}
		else if(txtNew.getText().equals(null) || txtNew.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("New Textfield is blank...!!");
			alert.showAndWait();
			txtNew.clear();
			txtNew.requestFocus();
		}
		else if (!txtNew.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Incorrect value");
			alert.setHeaderText(null);
			alert.setContentText("New TextField: please enter only numeric values!!");
			alert.showAndWait();
			txtNew.clear();
			txtNew.requestFocus();
		}
		else
		{*/
		
		st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
		
			try
			{	
				for(String awb:awb)
				{
				
					String query = "update dailybookingtransaction set billing_weight="+Double.parseDouble(txtNew.getText())+" where air_way_bill_number='"+awb+"'";
					//System.out.println(query);
					st.executeUpdate(query);
					//System.out.println("Invoice Updated...");
	            
					//showWithoutInvoiceTable(updateinvoice);
				}
				
				showDetaildTableData();
				
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				awb.clear();
				dbcon.disconnect(null, st, null, con);
			}
		//}
		
	}
	
// ====================================================================================================================

	public void showSummary(String sql,Set<String> networkset,Set<String> serviceset,Set<String> dnset) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		int records = 0;
		
		try
		{
			String count=null;
			
			String networkList=" ";
			String serviceList=" ";
			String dnList=" ";
			
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			// --------------------- Network Summary ---------------------------------
			
			for(String net:networkset)
			{
			
				finalsql = "select dbt.dailybookingtransaction2network from dailybookingtransaction as dbt where dailybookingtransaction2network='"+net+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					//System.out.println("no data available");
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				if(net.equals(net) && records>0)
				{
					count=net+":"+records+" ";
				}
				
				records=0;
				
				if(!count.equals(null))
				{
					networkList=networkList.concat(count);
				}
			}
			rs.last();
			labSummaryNetwork.setText(networkList);
			count=null;
			//networkList="Network:- ";
			
			
			// --------------------- Service Summary ---------------------------------
			
			for(String service:serviceset)
			{	
				finalsql = "select dbt.dailybookingtransaction2service from dailybookingtransaction as dbt where dbt.dailybookingtransaction2service='"+service+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				
				if(service.equals(service) && records>0)
				{
					count=service+":"+records+" ";
				}
				
				records=0;
			
				if(!count.equals(null))
				{
				serviceList=serviceList.concat(count);
				}
			}
			rs.last();
			labSummaryService.setText(serviceList);
			count=null;
			//serviceList="Service:- ";
			
			
			// --------------------- Dox Non-Dox Summary ---------------------------------
			
			for(String dn:dnset)
			{
				finalsql = "select dbt.dox_nondox from dailybookingtransaction as dbt where dbt.dox_nondox='"+dn+"' and "+sql;
				rs=st.executeQuery(finalsql);
					
				if(!rs.next())
				{
				
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
						
				if(dn.equals(dn) && records>0)
				{
					count=dn+":"+records+" ";
				}
						
				records=0;
					
				if(!count.equals(null))
				{
				dnList=dnList.concat(count);
				}
			}
			rs.last();		
			labSummaryDoxNonDox.setText(dnList);
			count=null;
			//dnList="D/N:- ";
			
			
		}	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			serviceset.clear();
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
// ====================================================================================================================	
	
	public void setDiscountAdd()
	{
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT))
		{
			txtTypeAmount.clear();
			txtReason.clear();
			txtTypeAmount.setDisable(true);
			txtReason.setDisable(true);
			comboBoxTypeFlatPercent.setDisable(true);
			comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		}
		else
		{
			txtTypeAmount.clear();
			txtReason.setDisable(false);
			comboBoxTypeFlatPercent.setDisable(false);
		}
	
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
	//	txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		
	}
	
	
	public void type()
	{
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
		{
			txtTypeAmount.clear();
			txtdiscountLessAmt.clear();
			txtTypeAmount.setDisable(true);
			txtdiscountLessAmt.setDisable(true);
			//txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
			txtGST.setText(df.format(gst_amount_old));
			//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		}
		
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.clear();
			txtGST.setText(df.format(gst_amount_old));
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(false);
		}
		else
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.clear();
			txtGST.setText(df.format(gst_amount_old));
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(true);
		}
		
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
		//txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
	}

// ====================================================================================================================
	
	public void setPercentFlat()
	{
		double discount=0.0;
		InvoiceBean inbean=new InvoiceBean();
		
		
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
		{	
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(df.format(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-discount);
				
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-Integer.parseInt(txtTypeAmount.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText()));
				
			}
				
		}
		else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
		{
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(df.format(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+discount);
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+Integer.parseInt(txtTypeAmount.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText()));
				
			}
		}
		
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
	
		txtDiscountAddtitionalAmt.setText(df.format(inbean.getAmountAfterDis_Add()));
		//getGST_onDateChangeWithoutAdd_dis();
		
		/*if(Double.valueOf(txtTypeAmount.getText())>0)
		{*/
		
		if(chkGST.isSelected()==false)
		{
			System.err.println("if GST check Box selected....");
			igst_amount=0;
			igst_rate=0;
			cgst_rate=0;
			sgst_rate=0;
			cgst_amount=0;
			sgst_amount=0;
			txtGST.setText("0");
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())));
		}
		else
		{
			if(branch_State.equals(client_State))
			{
				System.out.println(txtDiscountAddtitionalAmt.getText());
				
				if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_fix;
					sgst_rate=LoadGST_Rates.sgst_rate_fix;
					txtGST.setText(df.format(cgst_amount+sgst_amount));
					System.out.println("1 CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");	
				}
				else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_Temp)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_Temp)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_Temp;
					sgst_rate=LoadGST_Rates.sgst_rate_Temp;
					System.out.println("2 CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				}
				
				else 
				{
					cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
					sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
					igst_amount=0;
					igst_rate=0;
					cgst_rate=LoadGST_Rates.cgst_rate_fix;
					sgst_rate=LoadGST_Rates.sgst_rate_fix;
					System.out.println("3 CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				}
				
				/*cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				
				System.out.println("CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");*/
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				
				//txtGST.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
			}
			else
			{
				System.out.println(txtDiscountAddtitionalAmt.getText());
				if(invoiceDate.after(LoadGST_Rates.gst_Date_Fix) )
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
					igst_rate=LoadGST_Rates.igst_rate_fix;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("4 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);	
				}
				else if(invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_Temp)/100;
					igst_rate=LoadGST_Rates.igst_rate_Temp;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("5 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
				
				}
				
				else 
				{
					igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
					igst_rate=LoadGST_Rates.igst_rate_fix;
					cgst_rate=0;
					sgst_rate=0;
					cgst_amount=0;
					sgst_amount=0;
					System.out.println("6 CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				}
				
				
				/*igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				System.out.println("CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);*/
				txtGST.setText(df.format(igst_amount));
			}
		}
		/*}
		else
		{
			txtGST.setText(df.format(gst_amount_old));
		}*/
		grandtotal();
	}

	
// ====================================================================================================================

	public void grandtotal()
	{
		
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
		}
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		}
	}
	

// ====================================================================================================================	

	public void getGST_onDateChangeWithoutAdd_dis()
	{
		//d
		LocalDate local_InvoiceDate=dpkInvoiceDate.getValue();
		Date invoiceDate=Date.valueOf(local_InvoiceDate);
		
		System.err.println("In GST Calculation Method....");
		
		
		if(chkGST.isSelected()==false)
		{
			System.err.println("if GST check Box selected....");
			igst_amount=0;
			igst_rate=0;
			cgst_rate=0;
			sgst_rate=0;
			cgst_amount=0;
			sgst_amount=0;
			txtGST.setText("0");
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())));
		}
		else
		{
			int status=invoiceDate.compareTo(LoadGST_Rates.gst_Date_Fix);
			
			
			
		if(branch_State.equals(client_State))
		{
			System.err.println("if GST check Box not selected iffffffff....");
			
			if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_Temp)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_Temp)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_Temp;
				sgst_rate=LoadGST_Rates.sgst_rate_Temp;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				System.out.println("A >> CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//System.out.println("CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
			
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.after(LoadGST_Rates.gst_Date_Fix))
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_fix;
				sgst_rate=LoadGST_Rates.sgst_rate_fix;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				System.out.println("B >> CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//txtGST.setText(df.format(gst_amount_old));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && status==0)
			{
				cgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.cgst_rate_fix)/100;
				sgst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.sgst_rate_fix)/100;
				igst_amount=0;
				igst_rate=0;
				cgst_rate=LoadGST_Rates.cgst_rate_fix;
				sgst_rate=LoadGST_Rates.sgst_rate_fix;
				txtGST.setText(df.format(cgst_amount+sgst_amount));
				System.out.println("C >> CGST >> "+LoadGST_Rates.cgst_rate_fix+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_fix+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
				//txtGST.setText(df.format(gst_amount_old));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			//System.out.println("A >> CGST >> "+LoadGST_Rates.cgst_rate_Temp+" | Amount: "+cgst_amount+" | SGST >> "+LoadGST_Rates.sgst_rate_Temp+" | Amount: "+sgst_amount+" | IGST >> 0 | Amount: 0");
		}
		else
		{
			System.err.println("if GST check Box not selected elseeeeeeeeee....");
			
			
			if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.before(LoadGST_Rates.gst_Date_Fix))
			{
				
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_Temp)/100;
				igst_rate=LoadGST_Rates.igst_rate_Temp;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;

				System.out.println("D >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && invoiceDate.after(LoadGST_Rates.gst_Date_Fix))
			{
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				igst_rate=LoadGST_Rates.igst_rate_fix;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;
				System.out.println("E >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				//txtGST.setText(df.format(gst_amount_old));
				//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			else if(comboBoxDisAdd.getValue().equals((InvoiceUtils.INVOICDEFAULT)) && status==0)
			{
				System.out.println("???????????????????????????????????????????? "+InvoiceUtils.INVOICDEFAULT);
				igst_amount=(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*LoadGST_Rates.igst_rate_fix)/100;
				igst_rate=LoadGST_Rates.igst_rate_fix;
				cgst_rate=0;
				sgst_rate=0;
				cgst_amount=0;
				sgst_amount=0;
				System.out.println("F >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_fix+" | Amount: "+igst_amount);
				txtGST.setText(df.format(igst_amount));
				txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
				//txtGST.setText(df.format(gst_amount_old));
				//txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
			}
			//System.out.println("B >> CGST >> 0 | Amount: 0 | SGST >> 0 | Amount: 0 | IGST >> "+LoadGST_Rates.igst_rate_Temp+" | Amount: "+igst_amount);
		}
		}
		
	}
	
	
// ====================================================================================================================
	
	public void showGenerateButton()
	{
		if(chkBill.isSelected()==true)
		{
			btnGenerate.setDisable(false);
		}
		else
		{
			btnGenerate.setDisable(true);
		}
	}
	
// ====================================================================================================================

	public void getServiceTaxPercentage() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		String sql=null;
		
		try
		{	
			/*st=con.createStatement();
			sql="select effective_date ,sum(tax+tax1+tax2) as servicetax from expensestaxtype group by effective_date order by effective_date DESC limit 1";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
			invcBean.setServiceTaxPercentage(rs.getDouble("servicetax"));
			}
			*/
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================	

	public void reset() throws IOException
	{
		
		
		dpkFromDate.setEditable(false);
		
		dpkFromDate.setOnMouseClicked(e -> {
		        if(!dpkFromDate.isEditable()==true)
		        	dpkFromDate.show();
		    });
		
		dpkToDate.setEditable(false);
		
		dpkToDate.setOnMouseClicked(e -> {
		        if(!dpkToDate.isEditable()==true)
		        	dpkToDate.show();
		    });
		
		m.showGenerateInvoice();	
	}
		
// ====================================================================================================================	

	public void exit() throws SQLException
	{
		try {
			m.homePage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
// ==================================================================================================
	
	@FXML
	public void datePickerEnterTab(KeyEvent e) throws SQLException
	{
		//loadClients();
		System.out.println("this method: ===>>>"+e);
		
		if(e.getCode().equals(KeyCode.ENTER))
		{
			System.out.println("this method running ===================== ");
			
			dpkToDate.requestFocus();
		}
		
	}
	
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, ParseException, NumberFormatException, IOException
	{

	Node n=(Node) e.getSource();
		
		
	if(n.getId().equals("txtInvoiceNo"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			//loadClients();
			dpkInvoiceDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkInvoiceDate"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			dpkFromDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkFromDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//loadClients();
				dpkToDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkToDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxBranchCode.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxBranchCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				loadClients();
				comboBoxClientCode.requestFocus();
			}
		}
	
		// *********************
	
	
		else if(n.getId().equals("txtamount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtFuel.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtFuel"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtVasTotal_Amt.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtVasTotal_Amt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtInsurance.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtInsurance"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtSubTotal.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtSubTotal"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
	
		else if(n.getId().equals("comboBoxClientCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
	
		else if(n.getId().equals("comboBoxClientCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
		
	/*	else if(n.getId().equals("comboBoxSubClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}*/
		
		else if(n.getId().equals("comboBoxDisAdd"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtReason.isDisable()==true)
				{
					txtDiscountAddtitionalAmt.requestFocus();
				}
				else
				{
					txtReason.requestFocus();	
				}
			}
		}
		
		else if(n.getId().equals("txtReason"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxTypeFlatPercent.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxTypeFlatPercent"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.isDisable()==true)
				{
					//txtremarks.requestFocus();
					txtDiscountAddtitionalAmt.requestFocus();
				}
				else
				{
					txtTypeAmount.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("txtTypeAmount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.getText().equals("") || txtTypeAmount.getText().isEmpty())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty field");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the discount or additional amount...");
					alert.showAndWait();
					txtTypeAmount.requestFocus();
				}
				else
				{
					if(txtdiscountLessAmt.isDisable()==true)
					{
						txtDiscountAddtitionalAmt.requestFocus();
					}
					else
					{
						txtdiscountLessAmt.requestFocus();
					}
				}
			}
		}
	
		else if(n.getId().equals("txtdiscountLessAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtDiscountAddtitionalAmt.requestFocus();
			}
		}

	/*	else if(n.getId().equals("txtDiscountAddtitionalAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkGST.requestFocus();
			}
		}*/
	
		else if(n.getId().equals("chkGST"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText(null);
					alert.setContentText("Please select Discount/Additional type");
					alert.showAndWait();
					comboBoxTypeFlatPercent.requestFocus();
				} 
				else if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && !comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					setPercentFlat();
					txtGST.requestFocus();
					
				}
					
				else
				{
					getGST_onDateChangeWithoutAdd_dis();
					txtGST.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("txtDiscountAddtitionalAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(!comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT) && comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Confirmation");
					alert.setHeaderText(null);
					alert.setContentText("Please select Discount/Additional type");
					alert.showAndWait();
					comboBoxTypeFlatPercent.requestFocus();
				}
				else
				{
					getGST_onDateChangeWithoutAdd_dis();
					txtGST.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("txtGST"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtGrandTotal.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtGrandTotal"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtremarks.requestFocus();
			}
		}
	
		else if(n.getId().equals("txtremarks"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkBill.requestFocus();
			}
		}
	
		else if(n.getId().equals("chkBill"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(chkBill.isSelected()==true)
				{
					btnGenerate.requestFocus();
				}
			}
		}
	
		else if(n.getId().equals("btnGenerate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//getInvoiceSeries();
				
				if(rdBtnReGenerateInvoice.isSelected()==true)
				{
					updateInvoiceDate();
				}
				else
				{
					getInvoiceSeries();	
				}
			}
			
		}
	
	
	}

	
// ====================================================================================================================
	
	public void Test()
	{
		if(LoadInvoices.list_InvoiceData_Global.isEmpty()==false)
		{
			System.out.println("<< ============================== Global list is not empty ============================ >>");
		for(InvoiceBean inBean: LoadInvoices.list_InvoiceData_Global)
		{
			System.out.println("Invoice No.: "+inBean.getInvoice_number());
			System.out.println("Invoice Date: "+inBean.getInvoice_date());
			System.out.println("Branch: "+inBean.getInvoice2branchcode());
			System.out.println("Client Code: "+inBean.getClientcode());
			System.out.println("From Date: "+inBean.getFrom_date());
			System.out.println("To Date: "+inBean.getTo_date());
			System.out.println("Basic Amount: "+inBean.getBasicAmount());
			System.out.println("Fuel Rate: "+inBean.getFuel_rate());
			System.out.println("Fuel: "+inBean.getFuel());
			System.out.println("VAS Amount: "+inBean.getVas_amount());
			System.out.println("Insurance Amount: "+inBean.getInsurance_amount());
			System.out.println("Rate of Percent: "+inBean.getRateof_percent());
			System.err.println("Type Flat/Percent: "+inBean.getType_flat2percent());
			System.err.println("Dis/Add: "+inBean.getDiscount_additional());
			System.err.println("Dis/Add Amount: "+inBean.getDiscount_Additional_amount());
			System.out.println("Reason: "+inBean.getReason());
			System.out.println("Tax Name 2: "+inBean.getCgst_Name());
			System.out.println("CGST Rate: "+inBean.getCgst_rate());
			System.out.println("CGST Amount: "+inBean.getCgst_amt());
			System.out.println("Tax Name 2: "+inBean.getSgst_Name());
			System.out.println("SGST Rate: "+inBean.getSgst_rate());
			System.out.println("SGST Amount: "+inBean.getSgst_amt());
			System.out.println("Tax Name3: "+inBean.getIgst_Name());
			System.out.println("IGST Rate: "+inBean.getIgst_rate());
			System.out.println("IGST Amount: "+inBean.getIgst_amt());
			System.err.println(" >>> Taxable Amount: "+inBean.getTaxable_amount());
			System.out.println("GST Amount: "+inBean.getGst_amount());
			System.out.println("Grand Total: "+inBean.getGrandtotal());
			System.out.println("Amount In Words: "+inBean.getAmountInWords());
			System.out.println("Remarks: "+inBean.getRemarks());
			
			
			System.out.println("\n*************** Company Details ****************\n");
			
			System.out.println("Company Code: "+inBean.getCompanyCode());
			System.out.println("Company Name: "+inBean.getCompanyName());
			System.out.println("Tag Line: "+inBean.getCmpny_tagLine());
			System.out.println("Address: "+inBean.getCmpny_address());
			System.out.println("Tax No: "+inBean.getCmpny_serviceTaxNo());
			System.out.println("Pan No: "+inBean.getCmpny_panNo());
			System.out.println("Email: "+inBean.getCmpny_email());
			System.out.println("Bank Name: "+inBean.getCmpny_bankName());
			System.out.println("A/c No: "+inBean.getCmpny_accountNo());
			System.out.println("IFSC Code: "+inBean.getCmpny_ifscCode());
			System.out.println("Condi. 1: "+inBean.getCondition1());
			System.out.println("Condi. 2: "+inBean.getCondition2());
			System.out.println("Condi. 3: "+inBean.getCondition3());
			System.out.println("Condi. 4: "+inBean.getCondition4());
			System.out.println("Condi. 5: "+inBean.getCondition5());
			System.out.println("Condi. 6: "+inBean.getCondition6());
			System.out.println("Company State: "+inBean.getCmpny_state());
			System.out.println("Company Pincode: "+inBean.getCmpny_pincode());
			
				
			}
		}
		else
		{
			System.err.println("<< ============================== Global list is empty ============================ >>");
		}
	}

// ====================================================================================================================	

	public void getFreshOrReGenerateInvoice()
	{
		if(rdBtnFreshInvoice.isSelected()==true)
		{
			txtInvoiceNo.setDisable(true);
			
			
		}
		else
		{
			txtInvoiceNo.setDisable(false);
			txtInvoiceNo.setEditable(true);
		}
		
		comboBoxClientItems.clear();
		
		txtInvoiceNo.clear();
		dpkFromDate.setValue(LocalDate.now());
		dpkToDate.setValue(LocalDate.now());
		dpkInvoiceDate.setValue(LocalDate.now());
		comboBoxBranchCode.getSelectionModel().clearSelection();
		comboBoxClientCode.setValue(null);
		txtremarks.clear();
		
		
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		//comboBoxClientCode.getSelectionModel().clearSelection();
		txtamount.clear();
		txtFuel.clear();
		txtGrandTotal.clear();
		txtSubTotal.clear();
		txtDiscountAddtitionalAmt.clear();
		txtdiscountLessAmt.clear();
		txtGST.clear();
		txtInsurance.clear();
		txtReason.clear();
		txtTypeAmount.clear();
		txtVasTotal_Amt.clear();
		txtremarks.clear();
		comboBoxDisAdd.setDisable(true);
		list_InvoiceAndCompanyDetails.clear();
		list_CompleteInvoiceDetails.clear();
		invoiceNumberToPreview=null;
		invoiceNumberGenerated=null;
		
		
		cgst_amount=0;
		sgst_amount=0;
		igst_amount=0;
		cgst_rate=0;
		sgst_rate=0;
		igst_rate=0;
		
		invoiceNumberGenerated=null;
		invoiceNumberToPreview=null;
		summarySQL=null;
		awbCountToInvoicePrint=0;
		chkBill.setSelected(false);
		
		dpkFromDate.requestFocus();
	}
	
	
	

// ====================================================================================================================	
	
	public void reGenerateInActiveInvoice() throws SQLException
	{
		boolean	isInvoiceNoExist=false;
		
		for(InvoiceBean inBean: LoadInvoices.list_InvoiceData_Global)
		{
			if(inBean.getInvoice_number().equals(txtInvoiceNo.getText()) && inBean.getStatus().equals("In-Active"))
			{
				isInvoiceNoExist=true;
				dpkInvoiceDate.setValue(LocalDate.parse(inBean.getInvoice_date(), localdateformatter));
				dpkFromDate.setValue(LocalDate.parse(inBean.getFrom_date(), localdateformatter));
				dpkToDate.setValue(LocalDate.parse(inBean.getTo_date(), localdateformatter));
				
				
				System.out.println("Invoice date: >>>> "+inBean.getInvoice_date());
				System.out.println("From date: >>>> "+inBean.getFrom_date());
				System.out.println("To date: >>>> "+inBean.getTo_date());
				
				
				comboBoxBranchCode.setValue(setBranchForReGenerateInvoice(inBean.getInvoice2branchcode()));
				comboBoxClientCode.setValue(setClientForReGenerateInvoice(inBean.getClientcode()));
			}
		}
		
		if(isInvoiceNoExist==false)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			//alert.setTitle("Invoice Re-Print Alert");
			alert.setHeaderText("Invoice No. does not exist...\nPlease enter correct Invoice No.");
			alert.setContentText(null);
			alert.showAndWait();
			txtInvoiceNo.requestFocus();
			
			comboBoxClientCode.requestFocus();
			chkBill.setDisable(true);
			btnGenerate.setDisable(true);
			comboBoxClientCode.getSelectionModel().clearSelection();
			txtamount.clear();
			txtFuel.clear();
			txtGrandTotal.clear();
			txtSubTotal.clear();
			txtDiscountAddtitionalAmt.clear();
			txtdiscountLessAmt.clear();
			txtGST.clear();
			txtInsurance.clear();
			txtReason.clear();
			txtTypeAmount.clear();
			txtVasTotal_Amt.clear();
			txtremarks.clear();
			comboBoxDisAdd.setDisable(true);
			list_InvoiceAndCompanyDetails.clear();
			list_CompleteInvoiceDetails.clear();
			invoiceNumberToPreview=null;
			invoiceNumberGenerated=null;
			
			
			cgst_amount=0;
			sgst_amount=0;
			igst_amount=0;
			cgst_rate=0;
			sgst_rate=0;
			igst_rate=0;
			
			invoiceNumberGenerated=null;
			invoiceNumberToPreview=null;
			summarySQL=null;
			awbCountToInvoicePrint=0;
			chkBill.setSelected(false);
		}
		
	}
	
	public String setBranchForReGenerateInvoice(String branchCode) throws SQLException
	{
		String branchCodeName=null;
		new	LoadBranch().loadBranchWithName();
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			if(branchCode.equals(branchBean.getBranchCode()))
			{
				branchCodeName=branchBean.getBranchCode()+ " | "+branchBean.getBranchName();
				break;
			}
		}
		return branchCodeName;
	}
	
	public String setClientForReGenerateInvoice(String clientcode) throws SQLException
	{
		String clientCodeName=null;
		
		new LoadClients().loadClientWithName();
		
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientcode.equals(clBean.getClientCode()))
			{
				clientCodeName=clBean.getClientCode()+" | "+clBean.getClientName();
				break;
			}
		}
		return clientCodeName;
	}
	
// ******************************************************************************

	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
		//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));

		selectedFile = fc.showOpenDialog(null);

		if(selectedFile != null)
		{
			a=5*1024*1024;
			size=selectedFile.length();	

			if(size<a)
			{
				txtBrowseExcelFile.getText();
				txtBrowseExcelFile.setText(selectedFile.getAbsolutePath());
				btnLoad.requestFocus();
			}
			else
			{
				txtBrowseExcelFile.clear();
			}
		
		}
		else
		{
			size=0;
			btnLoad.setDisable(true);
		}
		
		if(txtBrowseExcelFile.getText().isEmpty()==false || !txtBrowseExcelFile.getText().equals(""))
		{
			btnLoad.setDisable(false);
		}
		else
		{
			btnLoad.setDisable(true);
		}
	}	

			
// ******************************************************************************	

	public void progressBarTest() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Processing.......");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);

		taskUpdateStage.setScene(new Scene(updatePane));
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				save();

				int max = 1000;
				for (int i = 1; i <= max; i++) 
				{
					// Thread.sleep(100);
					/*if (BirtReportExportCon.FLAG == true) {
			                                   break;
			                            }*/
				}
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			public void handle(WorkerStateEvent t) {
				taskUpdateStage.close();


				/*	try {
							//loadSavedBookingDataTable();
							//loadUnSavedBookingDataTable();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
				/*	if (list_ZoneWiseClientRate.size()==update_insert_count) {

							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Task Complete");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Rates successfully uploaded...");
							alert.showAndWait();

						}*/
			}
		});


		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();
	}

// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
	}		
			
			
// ******************************************************************************	
			
	public void importNewExcel(File selectedFile) throws IOException
	{
		int index=0;
		list_InvoiceForSelectedAWBRecords.clear();

		try
		{
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			System.out.println("File Name: "+fileName);

			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

				HSSFRow row;
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);

						Iterator<Cell> cellIterator = row.cellIterator();

						try
						{
							if(!String.valueOf((long)row.getCell(0).getNumericCellValue()).equals("0"))
							{
								if(row.getCell(0).getCellType()==Cell.CELL_TYPE_NUMERIC)
								{
									list_InvoiceForSelectedAWBRecords.add(String.valueOf((long)row.getCell(0).getNumericCellValue()));
									//updateBean.setMasterAwbNo(String.valueOf((long)row.getCell(0).getNumericCellValue()));
								}
								else
								{
									list_InvoiceForSelectedAWBRecords.add(row.getCell(0).getStringCellValue());
									//updateBean.setMasterAwbNo(row.getCell(0).getStringCellValue());
								}

								
							}
						}
						catch(Exception e)
						{
							list_FormatMisMatch.add(String.valueOf(row.getCell(0)));
						}
					}
				}
			}

			System.out.println(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_InvoiceForSelectedAWBRecords.size());
			System.out.println(" format mismatch list+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_FormatMisMatch.size());

			for(String awb:list_InvoiceForSelectedAWBRecords)
			{
				System.out.println("Awb :: "+awb);
			}

			/*for(String num:list_FormatMisMatch)
				{
					System.out.println("Mis Match >> "+num);
				}*/

			fileInputStream.close();

			loadSelectAwbForInvoice();
			
		}

		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	
// ====================================================================================================================
	
	public void loadSelectAwbForInvoice()
	{
		int serialno=1;
		
		double basicAmt=0;
		double subTotal=0;
		double vasTotal=0;
		double gstAmt=0;
		double fuelAmt=0;
		double insuranceAmt=0;
		
		
		detailedtabledata.clear();
		
		System.out.println("Detail list size >>>> "+list_DetailedTableData.size());
		
		for(InvoiceBean invcBean:list_DetailedTableData)
		{
			if(list_InvoiceForSelectedAWBRecords.contains(invcBean.getAbw_no()))
			{	
				basicAmt=basicAmt+invcBean.getBasicAmount();
				vasTotal=vasTotal+invcBean.getCod_amount()+invcBean.getFod_amount()+invcBean.getVas_amount()+invcBean.getDocket_charge();
				fuelAmt=fuelAmt+invcBean.getFuel();
				insuranceAmt=insuranceAmt+invcBean.getInsurance_amount();
				gstAmt=gstAmt+invcBean.getGst_amount();
				subTotal=insuranceAmt+fuelAmt+vasTotal+basicAmt;
				
				detailedtabledata.add(new InvoiceTableBean(serialno, invcBean.getAbw_no(), invcBean.getBooking_date(),
						invcBean.getInvoice2branchcode(), invcBean.getClientcode(), invcBean.getBilling_weight(), invcBean.getPcs(), 
						invcBean.getBasicAmount(), invcBean.getInsurance_amount(), invcBean.getDocket_charge(), invcBean.getVas_amount(),
						invcBean.getFuel(), invcBean.getTotal()));
				
				serialno++;
			}
		}
		
		detailedtable.setItems(detailedtabledata);
		
		txtamount.setText(df.format(basicAmt));
		txtFuel.setText(df.format(fuelAmt));
		txtVasTotal_Amt.setText(df.format(vasTotal));
		txtInsurance.setText(df.format(insuranceAmt));
		txtSubTotal.setText(df.format(subTotal));
		
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
		txtGST.setText(df.format(gstAmt));
		gst_amount_old=gstAmt;
		
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtGST.getText())));
		
	}
	
	
// ====================================================================================================================
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		getFreshOrReGenerateInvoice();
		txtBrowseExcelFile.setEditable(false);
		
		imgSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		
		dpkFromDate.requestFocus();
		
		comboBoxDisAdd.setValue(InvoiceUtils.INVOICDEFAULT);
		comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		
		
		comboBoxDisAdd.setItems(comboBoxDiscountAddtitionalItems);
		comboBoxTypeFlatPercent.setItems(comboBoxTypeFlatPercentItems);
		
		txtReason.setDisable(true);
		txtTypeAmount.setDisable(true);
		txtFrom.setDisable(true);
		txtTo.setDisable(true);
		txtNew.setDisable(true);
		//comboBoxSubClient.setDisable(true);
		comboBoxTypeFlatPercent.setDisable(true);
		comboBoxDisAdd.setDisable(true);
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		txtdiscountLessAmt.setDisable(true);
		
		dpkFromDate.setValue(LocalDate.now());
		dpkToDate.setValue(LocalDate.now());
		dpkInvoiceDate.setValue(LocalDate.now());
		
		try
		{
			new LoadCompany().loadCompanyWithName();
			//checkInvoice();
			loadBranch();
			if(LoadInvoices.list_InvoiceData_Global.isEmpty()==true)
			{
				new LoadInvoices().loadInvoiceDate();
			}
			else
			{
				System.err.println("Load Invoice Else running....");
			}
			
			getServiceTaxPercentage();
			Test();
		//	loadInvoiceDate();
			//loadClients();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		btnBrowse.setDisable(true);
		btnLoad.setDisable(true);
		txtBrowseExcelFile.setDisable(true);
		
		invc_Col_serialno.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Integer>("slno"));
		invc_Col_InvoiceNo.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceNumber"));
		invc_Col_InvoiceDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceDate"));
		invc_Col_FromDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("from_date"));
		invc_Col_ToDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("to_date"));
		invc_Col_ClientCode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientCode"));
		invc_Col_BasicAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("basicAmount"));
		invc_Col_Fuel.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("fuel"));
		invc_Col_VasAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("vas_amount"));
		invc_Col_InsuranceAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("insuranceAmount"));
		invc_Col_TaxableAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("taxableAmount"));
		invc_Col_GSTAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("gst_Amount"));
		invc_Col_GrandTotal.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceGrandTotal"));
		invc_Col_Remarks.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceRemarks"));
		
		serialno.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("awb_no"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("booking_date"));
		branch.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("invoice2branchcode"));
		clientcode.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("clientcode"));
		billingweight.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("insurance_amount"));
		pcs.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Integer>("pcs"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("docket_charge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("other_amount"));
		amount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("fuel"));
		total.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("total"));
		
		chkGST.setSelected(true);
		
		
		/*double amout=999999.12;
		int a=(int)amout;
		ConvertAmountInWords cnvrtWrd=new ConvertAmountInWords();
		
		System.err.println("Amount >> "+a+" >>>>>>>>>>>>>>>> amount to words >> "+cnvrtWrd.convertToWords(Integer.valueOf(a)));*/
	
	}
	
	

}
