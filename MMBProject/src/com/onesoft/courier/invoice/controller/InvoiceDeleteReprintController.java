package com.onesoft.courier.invoice.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.commons.collections.set.SynchronizedSortedSet;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.OutwardForwardedTableBean;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.ConvertAmountInWords;
import com.onesoft.courier.common.GenerateSaveReport;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCompany;
import com.onesoft.courier.common.LoadInvoices;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadCompanyBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.invoice.bean.InvoiceBean;
import com.onesoft.courier.invoice.bean.InvoiceBillJavaFXBean;
import com.onesoft.courier.invoice.bean.InvoiceDeleteReprintBean;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.sf.jasperreports.engine.JRException;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class InvoiceDeleteReprintController implements Initializable {

	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<InvoiceBillJavaFXBean> activeInvoicetabledata=FXCollections.observableArrayList();
	private ObservableList<InvoiceBillJavaFXBean> inActiveInvoicetabledata=FXCollections.observableArrayList();
	
	List<InvoiceBean> list_InvoiceAndCompanyDetails=new ArrayList<>();
	
	public String src = "E:\\NewEclipseWorkspace_2017\\MMBProject\\src\\com\\onesoft\\courier\\invoice\\controller\\Invoice.jrxml";
	Map<String , Object> parameters = new HashMap<>();
	
	public String checkBoxStatus;
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	public String summarySQL=null;
	
	@FXML
	private ImageView imgSearchActiveIcon;
	
	@FXML
	private ImageView imgSearchInActiveIcon;
	
	@FXML
	private CheckBox chkExportToExcel;
	
	@FXML
	private RadioButton rdBtnDelete;
	
	@FXML
	private RadioButton rdBtnReprint;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private TextField txtInvoiceNo;
	
	@FXML
	private TextField txtReason;
	
	@FXML
	private Button btnDeleteReprint;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Label labReason;
	
//================================================================

	@FXML
	private TableView<InvoiceBillJavaFXBean> active_InvoiceTable;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Integer> act_Col_serialno;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_InvoiceDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_InvoiceNo;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_ClientCode;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_FromDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_ToDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_BasicAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_Fuel;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_VasAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_InsuranceAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_TaxableAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_GSTAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> act_Col_GrandTotal;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> act_Col_Remarks;	
	
	
//================================================================

	@FXML
	private TableView<InvoiceBillJavaFXBean> inActive_InvoiceTable;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Integer> inAct_Col_serialno;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_InvoiceDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_InvoiceNo;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_ClientCode;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_FromDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_ToDate;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_BasicAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_Fuel;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_VasAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_InsuranceAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_TaxableAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_GSTAmt;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> inAct_Col_GrandTotal;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> inAct_Col_Status;		
	
	
// **********************************************************************
	
	public void setDeleteReprint()
	{
		if(rdBtnDelete.isSelected()==true)
		{
			btnDeleteReprint.setText("Delete");
			labReason.setText("Reason (*)");
		}
		else
		{
			btnDeleteReprint.setText("Re-Print");
			labReason.setText("Reason");
		}
	}
	

// **********************************************************************
	
	public void loadBranch() throws SQLException
	{	
		new	LoadBranch().loadBranchWithName();
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			comboBoxBranchItems.add(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
	}
	
// **********************************************************************

	public void taskDeleteReprint() throws SQLException, IOException, JRException
	{
		if(rdBtnDelete.isSelected()==true)
		{
			deleteConfirmation();
		}
		else
		{
			reprintInvoice();
		}
	}
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, IOException, JRException 
	{
		Node n = (Node) e.getSource();

		if (n.getId().equals("rdBtnDelete")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnReprint.requestFocus();
			}
		}	
		
		else if (n.getId().equals("rdBtnReprint")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxBranch.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxBranch")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInvoiceNo.requestFocus();
			}
		}
		

		else if (n.getId().equals("txtInvoiceNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtReason.requestFocus();
			}
		}

		else if (n.getId().equals("txtReason")) {
				if (e.getCode().equals(KeyCode.ENTER)) {
					chkExportToExcel.requestFocus();
				}
			}

		else if (n.getId().equals("chkExportToExcel")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				//setValidation();
				
				//taskDeleteReprint();
				btnDeleteReprint.requestFocus();
			}
		}
		
		else if (n.getId().equals("btnDeleteReprint")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
				
				//taskDeleteReprint();
				//comboBoxBranch.requestFocus();
			}
		}
	}
	
// *************** Method to set Validation *******************	
	
	public void setValidation() throws SQLException, IOException, JRException
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
				String email=txtemailid.getText();
			    Pattern pattern = Pattern.compile(regex);
			    Matcher matcher = pattern.matcher((CharSequence) email);
			    boolean checkmail=matcher.matches();*/


		if(comboBoxBranch.getValue()==null || comboBoxBranch.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select a Branch");
			alert.showAndWait();
			comboBoxBranch.requestFocus();
		}
		else if(txtInvoiceNo.getText()==null || txtInvoiceNo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Invoice No");
			alert.showAndWait();
			txtInvoiceNo.requestFocus();
		}

		else if(rdBtnDelete.isSelected()==true)
		{
			if(txtReason.getText()==null || txtReason.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Reason");
				alert.showAndWait();
				txtReason.requestFocus();
			}
			else
			{
				taskDeleteReprint();
				txtInvoiceNo.requestFocus();
				txtReason.clear();
			}
		}
		
		/*else if(txtReason.getText()==null || txtReason.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Reason");
			alert.showAndWait();
			txtReason.requestFocus();
		}*/

		else
		{
			
			taskDeleteReprint();
			txtInvoiceNo.requestFocus();
			txtReason.clear();
			
		}
	}	
			
	
// **********************************************************************	
	
	public void deleteConfirmation() throws SQLException
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Delete Confirmation");
		alert.setContentText("Are you sure you want to delete this invoice?");
		
		ButtonType buttonTypeYes = new ButtonType("Yes");
		ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
		
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes)
		{
			
			System.out.println("Deleted >>> ");
			deleteInvoice();
		}
		else if(result.get() == buttonTypeNo)
		{
			System.out.println("Not Deleted >>> ");
		}
	}
	
// **********************************************************************	
	
	public void deleteInvoice() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		//Statement st = null;
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Invoice Delete Status");
		alert.setHeaderText(null);
		String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");

		InvoiceDeleteReprintBean drBean=new InvoiceDeleteReprintBean();
		
		drBean.setBranchCode(branchCode[0]);
		drBean.setInvoiceNo(txtInvoiceNo.getText());
		drBean.setInvoiceStatus("In-Active");
		
		
		int deleteStatus=0;

		try 
		{
			String sql = "update invoice set status=? where invoice_number=? and invoice2branchcode=?";
			preparedStmt = con.prepareStatement(sql);

			preparedStmt.setString(1, drBean.getInvoiceStatus());
			preparedStmt.setString(2, drBean.getInvoiceNo());
			preparedStmt.setString(3, drBean.getBranchCode());
		
			System.out.println("Invoice Inactive Sql: "+preparedStmt);
			deleteStatus=preparedStmt.executeUpdate();

			if(deleteStatus>0)
			{
				// ************* Trigger named :: update_InvoiceStatus and Function named :: removeinvoicenumber()
				// ************* to use remove invoice number from selected AwbNo 
				
				//removeInvoiceFromAwbNumbers(drBean.getInvoiceNo());
				saveDeleteReprint(drBean.getInvoiceNo());
				addInActiveInvoice_To_InActiveInvoice(drBean.getInvoiceNo());
				alert.setContentText("Invoice successfully deleted...!!");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Invoice is not deleted\nPlease Check...!!");
				alert.showAndWait();
			}
			System.err.println("Execute Update return value: >> "+deleteStatus);

		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}

// **********************************************************************

	public void addInActiveInvoice_To_InActiveInvoice(String invoiceNo) throws Exception
	{
		System.out.println("Invoice number >> "+invoiceNo );
		
		System.out.println(" >>>>>>>>>>>>> Table InActive Invoice size: "+LoadInvoices.list_InvoiceData_Global.size());
		int listIndex=0;
		for(InvoiceBean bean:LoadInvoices.list_InvoiceData_Global)
		{
			if(bean.getInvoice_number().equals(invoiceNo))
			{
				System.out.println("In active if running...");
				InvoiceBean inbean=new InvoiceBean();

				inbean.setInvoice_date(bean.getInvoice_date());
				inbean.setInvoice_number(bean.getInvoice_number());
				inbean.setInvoice2branchcode(bean.getInvoice2branchcode());
				inbean.setClientcode(bean.getClientcode());
				inbean.setFrom_date(bean.getFrom_date());
				inbean.setTo_date(bean.getTo_date());
				inbean.setBasicAmount(bean.getBasicAmount());
				inbean.setFuel_rate(bean.getFuel_rate());
				inbean.setFuel(bean.getFuel());
				inbean.setVas_amount(bean.getVas_amount());
				inbean.setInsurance_amount(bean.getInsurance_amount());
				inbean.setType_flat2percent(bean.getType_flat2percent());
				inbean.setDiscount_additional(bean.getDiscount_additional());
				inbean.setRateof_percent(bean.getRateof_percent());
				inbean.setDiscount_Additional_amount(bean.getDiscount_Additional_amount());
				inbean.setReason(bean.getReason());
				
				inbean.setCgst_Name(bean.getCgst_Name());
				inbean.setCgst_rate(bean.getCgst_rate());
				inbean.setCgst_amt(bean.getCgst_amt());
		
				inbean.setSgst_Name(bean.getSgst_Name());
				inbean.setSgst_rate(bean.getSgst_rate());
				inbean.setSgst_amt(bean.getSgst_amt());
		
				inbean.setIgst_Name(bean.getIgst_Name());
				inbean.setIgst_rate(bean.getIgst_rate());
				inbean.setIgst_amt(bean.getIgst_amt());
				
				inbean.setTaxable_amount(bean.getTaxable_amount());
				inbean.setGst_amount(bean.getGst_amount());
				inbean.setGrandtotal(bean.getGrandtotal());
				inbean.setRemarks(bean.getRemarks());
				inbean.setStatus("In-Active");
				
				LoadInvoices.list_InvoiceData_Global.add(inbean);
				
				LoadInvoices.list_InvoiceData_Global.remove(listIndex);
				loadActiveInvocieTable();
				loadInActiveInvoice();
				break;
			}
			
			listIndex++;
		}
	}
	
// **********************************************************************
	
	public void loadInActiveInvoice()
	{
		inActiveInvoicetabledata.clear();

		int serialno=1;

		System.out.println("Table InActive Invoice size: "+LoadInvoices.list_InvoiceData_Global.size());
		System.out.println("Table Active Invoice size: "+LoadInvoices.list_InvoiceData_Global.size());
		
		for(InvoiceBean inbean: LoadInvoices.list_InvoiceData_Global)
		{
			if(inbean.getStatus().equals("In-Active"))
			{
				inActiveInvoicetabledata.add(new InvoiceBillJavaFXBean(serialno,inbean.getInvoice_number(), inbean.getInvoice_date(),
						inbean.getClientcode(), inbean.getFrom_date(), inbean.getTo_date(),inbean.getBasicAmount(), inbean.getFuel(),
						inbean.getVas_amount(), inbean.getInsurance_amount(), inbean.getTaxable_amount(), inbean.getGst_amount(),
						inbean.getGrandtotal(), inbean.getRemarks()));
				serialno++;
			}
		}

		inActive_InvoiceTable.setItems(inActiveInvoicetabledata);
		

	}
	
	
// **********************************************************************
	
	public void saveDeleteReprint(String invoiceNo) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		//Statement st = null;
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;

		InvoiceDeleteReprintBean drBean=new InvoiceDeleteReprintBean();
		
		drBean.setInvoiceNo(invoiceNo);
		
		if(txtReason.getText().equals("") || txtReason.getText().isEmpty())
		{
			drBean.setReason(null);
		}
		else
		{
			drBean.setReason(txtReason.getText());	
		}
		
		if(CommonVariable.USER_ID.equals(""))
		{
			drBean.setUser(null);
		}
		else
		{
			drBean.setUser(CommonVariable.USER_ID);
		}
		drBean.setApplicationId("MMB");
		drBean.setIpAddress(CommonVariable.USER_IP_ADDRESS);
		drBean.setHostName(CommonVariable.USER_HOST_NAME);
		
		if(rdBtnDelete.isSelected()==true)
		{
			drBean.setType_Delete_reprint("Delete");
		}
		else
		{
			drBean.setType_Delete_reprint("Re-Print");
		}
	
		try 
		{
			String sql = "insert into invoice_delete_reprint(invoice_no,type_delete_reprint,reason,user_id,applicationid,ip_address,"
					+ "host_name,createdate,lastmodifieddate) values(?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";
			preparedStmt = con.prepareStatement(sql);

			preparedStmt.setString(1, drBean.getInvoiceNo());
			preparedStmt.setString(2, drBean.getType_Delete_reprint());
			preparedStmt.setString(3, drBean.getReason());
			preparedStmt.setString(4, drBean.getUser());
			preparedStmt.setString(5, drBean.getApplicationId());
			preparedStmt.setString(6, drBean.getIpAddress());
			preparedStmt.setString(7, drBean.getHostName());
			
			
			System.out.println("Save Delete Reprint >> "+sql);
			preparedStmt.executeUpdate();

		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	

// **********************************************************************
	
	/*public void removeInvoiceFromAwbNumbers(String invoiceNo) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		//Statement st = null;
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;

		String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");

		InvoiceDeleteReprintBean drBean=new InvoiceDeleteReprintBean();
		
		drBean.setBranchCode(branchCode[0]);
		drBean.setInvoiceNo(txtInvoiceNo.getText());
		drBean.setInvoiceStatus("In-Active");
		
		int removeStatue=0;

		try 
		{
			String sql = "update dailybookingtransaction set invoice_number=null where invoice_number=?";
			preparedStmt = con.prepareStatement(sql);

			preparedStmt.setString(1, drBean.getInvoiceNo());
			removeStatue=preparedStmt.executeUpdate();
	
			System.out.println("Invoice Inactive Sql: "+removeStatue);
			
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}*/
	
	
	
// **********************************************************************

	public void reprintInvoice() throws IOException, JRException, SQLException
	{
		System.out.println("Reprint Running.,...... >>>>>>>>>");
		
		showInvoice_CompanyDetails();
		
	}	
	

// **********************************************************************	
	
	public String getClientNameViaClientCode(String clientcode)
	{
		String clientName_Address_pincode_gstinNo=null;
		System.out.println("Client code 1 >>>>>>> "+clientcode);
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientcode.equals(clBean.getClientCode()))
			{
				clientName_Address_pincode_gstinNo=clBean.getClientName()+" | "+clBean.getAddress()+" | "+clBean.getPincode()+" | "+clBean.getClient_gstin_number();
				break;
			}
		}
		return clientName_Address_pincode_gstinNo;
	}

	
// **********************************************************************	
	
	public String getStateNameViaClientCode(String pincode)
	{
		String stateName=null;
		String stateCode=null;
		String state_gstin_code=null;
		
		
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pincode.equals(pinBean.getPincode()))
			{
				stateCode=pinBean.getState_code();
				state_gstin_code=pinBean.getState_gstin_code();
				break;
			}
		}
		
		for(LoadCityBean stateBean: LoadState.SET_LOAD_STATEWITHNAME)
		{
			if(stateCode.equals(stateBean.getStateCode()))
			{
				stateName=stateBean.getStateName();
				break;
			}
		}
		return stateCode+" | "+stateName+" | "+state_gstin_code;
	}	
	
// **********************************************************************	
	
	public String getCompanyViaBranch(String branchName)
	{
		String cmpnyCode=null;
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{	
			if(branchName.equals(branchBean.getBranchCode()))
			{
				cmpnyCode=branchBean.getCmpnyCode();
				break;
			}
		}
		return cmpnyCode;
	}
	

// **********************************************************************	
	
	public void showInvoice_CompanyDetails() throws IOException, JRException, SQLException
	{
		ConvertAmountInWords cnvrtWrd=new ConvertAmountInWords();
		
		boolean isInvoiceExist=false;
		
		for(InvoiceBean inBean: LoadInvoices.list_InvoiceData_Global)
		{
			System.out.println("Invoice no;;; >>> "+inBean.getInvoice_number()+" | From TextBox >> "+txtInvoiceNo.getText());
			
			if(inBean.getInvoice_number().equals(txtInvoiceNo.getText()) && inBean.getStatus().equals("Active"))
			{
				isInvoiceExist=true;
				InvoiceBean generateInvoiceBean=new InvoiceBean();
			
				String clientNameAddressPincodeGSTIN=getClientNameViaClientCode(inBean.getClientcode());
				String[] name_Address_pincode_GSTIN=clientNameAddressPincodeGSTIN.split("\\|");
				
				String ClientStateCode_Name_GstinCode=getStateNameViaClientCode(name_Address_pincode_GSTIN[2].trim());
				String[] StateCode_Name_GstinCode=ClientStateCode_Name_GstinCode.split("\\|");
				
				generateInvoiceBean.setInvoice_number(txtInvoiceNo.getText());
				generateInvoiceBean.setInvoice_date(inBean.getInvoice_date());
				generateInvoiceBean.setInvoice2branchcode(inBean.getInvoice2branchcode());
				generateInvoiceBean.setClientcode(inBean.getClientcode());
				generateInvoiceBean.setClientname(name_Address_pincode_GSTIN[0].trim());
				generateInvoiceBean.setClientAddress(name_Address_pincode_GSTIN[1].trim());
				generateInvoiceBean.setClientGSTIN(name_Address_pincode_GSTIN[3].trim());
				generateInvoiceBean.setClientStateName(StateCode_Name_GstinCode[1].trim());
				generateInvoiceBean.setClient_StateGSTIN_code(StateCode_Name_GstinCode[2].trim());
				generateInvoiceBean.setFrom_date(inBean.getFrom_date());
				generateInvoiceBean.setTo_date(inBean.getTo_date());
				generateInvoiceBean.setBasicAmount(inBean.getBasicAmount());
				generateInvoiceBean.setFuel_rate(inBean.getFuel_rate());
				generateInvoiceBean.setFuel(inBean.getFuel());
				generateInvoiceBean.setVas_amount(inBean.getVas_amount());
				generateInvoiceBean.setInsurance_amount(inBean.getInsurance_amount());
				generateInvoiceBean.setType_flat2percent(inBean.getType_flat2percent());
				generateInvoiceBean.setDiscount_additional(inBean.getDiscount_additional());
				generateInvoiceBean.setRateof_percent(inBean.getRateof_percent());
				generateInvoiceBean.setDiscount_Additional_amount(inBean.getDiscount_Additional_amount());
				generateInvoiceBean.setReason(inBean.getReason());
				generateInvoiceBean.setCgst_Name(inBean.getCgst_Name());
				generateInvoiceBean.setCgst_rate(inBean.getCgst_rate());
				generateInvoiceBean.setCgst_amt(inBean.getCgst_amt());
				generateInvoiceBean.setSgst_Name(inBean.getSgst_Name());
				generateInvoiceBean.setSgst_rate(inBean.getSgst_rate());
				generateInvoiceBean.setSgst_amt(inBean.getSgst_amt());
				generateInvoiceBean.setIgst_Name(inBean.getIgst_Name());
				generateInvoiceBean.setIgst_rate(inBean.getIgst_rate());
				generateInvoiceBean.setIgst_amt(inBean.getIgst_amt());
				generateInvoiceBean.setTaxable_amount(inBean.getTaxable_amount());
				generateInvoiceBean.setGst_amount(inBean.getGst_amount());
				generateInvoiceBean.setGrandtotal(inBean.getGrandtotal());
				generateInvoiceBean.setAmountInWords(cnvrtWrd.convertToWords(generateInvoiceBean.getGrandtotal().intValue()));
				//generateInvoiceBean.setAwbCount(awbCountToInvoicePrint);
				
				generateInvoiceBean.setRemarks(inBean.getRemarks());
				generateInvoiceBean.setCompanyCode(getCompanyViaBranch(inBean.getInvoice2branchcode()));
				
				for(LoadCompanyBean cmpnyBean:LoadCompany.SET_LOAD_COMPANYWITHALLDETAILS)
				{
					if(generateInvoiceBean.getCompanyCode().equals(cmpnyBean.getCompanyCode()))
					{
						generateInvoiceBean.setCompanyName(cmpnyBean.getCompanyName());
						//generateInvoiceBean.setCompanyCode(cmpnyBean.getCompanyCode());
						generateInvoiceBean.setCmpny_tagLine(cmpnyBean.getTagLine());
						generateInvoiceBean.setCmpny_address(cmpnyBean.getAddress());
						generateInvoiceBean.setCmpny_city(cmpnyBean.getCity());
						generateInvoiceBean.setCmpny_serviceTaxNo(cmpnyBean.getServiceTaxNo());
						generateInvoiceBean.setCmpny_panNo(cmpnyBean.getPanNo());
						generateInvoiceBean.setCmpny_email(cmpnyBean.getEmail());
						generateInvoiceBean.setCmpny_bankName(cmpnyBean.getBankName());
						generateInvoiceBean.setCmpny_accountNo(cmpnyBean.getAccountNo());
						generateInvoiceBean.setCmpny_ifscCode(cmpnyBean.getIfscCode());
						
						generateInvoiceBean.setCondition1(cmpnyBean.getCondition1());
						generateInvoiceBean.setCondition2(cmpnyBean.getCondition2());
						generateInvoiceBean.setCondition3(cmpnyBean.getCondition3());
						generateInvoiceBean.setCondition4(cmpnyBean.getCondition4());
						generateInvoiceBean.setCondition5(cmpnyBean.getCondition5());
						generateInvoiceBean.setCondition6(cmpnyBean.getCondition6());
						
						generateInvoiceBean.setCmpny_state(cmpnyBean.getState());
						generateInvoiceBean.setCmpny_pincode(cmpnyBean.getPincode());
						
						list_InvoiceAndCompanyDetails.add(generateInvoiceBean);
						break;
						
					}
				}
			}
		}
		
		System.out.println("Global List size >>>> "+LoadInvoices.list_InvoiceData_Global.size());
		System.out.println("New List size >>>> "+list_InvoiceAndCompanyDetails.size());
		
		
		if(isInvoiceExist==true)
		{
			isInvoiceExist=false;
			showInvoiceInJasperViewer();	
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Invoice Re-Print Alert");
			alert.setHeaderText("Invoice No. does not exist...\nPlease enter correct Invoice No.");
			alert.setContentText(null);
			alert.showAndWait();
			txtInvoiceNo.requestFocus();
			
		}
		
	}

// ====================================================================================================================
	
	public void showInvoiceInJasperViewer() throws IOException, JRException, SQLException
	{
	//	Test();
		
		for(InvoiceBean inBean: list_InvoiceAndCompanyDetails) 
		{
			if(inBean.getInvoice_number().equals(txtInvoiceNo.getText()))
			{
				parameters.put("condition1", inBean.getCondition1());
				parameters.put("condition2", inBean.getCondition2());
				parameters.put("condition3", inBean.getCondition3());
				parameters.put("condition4", inBean.getCondition4());
				parameters.put("condition5", inBean.getCondition5());
				parameters.put("condition6", inBean.getCondition6());
				parameters.put("panNumber", inBean.getCmpny_panNo());
				parameters.put("tagLine", inBean.getCmpny_tagLine());
				parameters.put("address", inBean.getCmpny_address());
				parameters.put("email", inBean.getCmpny_email());
				parameters.put("grandTotal", inBean.getGrandtotal());
				parameters.put("amountInWords", inBean.getAmountInWords());
				parameters.put("type", "");
				parameters.put("value", 0.0);
				parameters.put("fuelTaxName", "Fuel");
				parameters.put("CGSTName", "CGST @"+inBean.getCgst_rate());
				parameters.put("SGSTName", "SGST @"+inBean.getSgst_rate());
				parameters.put("IGSTName", "IGST @"+inBean.getIgst_rate());
				parameters.put("fuel", inBean.getFuel());
				parameters.put("CGST", inBean.getCgst_amt());
				parameters.put("SGST", inBean.getSgst_amt());
				parameters.put("IGST", inBean.getIgst_amt());
				parameters.put("amount", inBean.getBasicAmount());
				parameters.put("name", inBean.getCompanyName());
				parameters.put("taxNumber", inBean.getCmpny_serviceTaxNo());
				parameters.put("bankName", inBean.getCmpny_bankName());
				parameters.put("accountNumber", inBean.getCmpny_accountNo());
				parameters.put("IFSCCode", inBean.getCmpny_ifscCode());
				parameters.put("taxableValue", inBean.getTaxable_amount());
				parameters.put("saccode", "");
				parameters.put("compState", inBean.getCmpny_state());
				parameters.put("compStateCode", inBean.getCmpny_pincode());
				parameters.put("InvoiceNumber", inBean.getInvoice_number());
				parameters.put("dateFrom", inBean.getFrom_date());
				parameters.put("dateTo", inBean.getTo_date());
				parameters.put("InvoiceDate", inBean.getInvoice_date());
				
				parameters.put("clientName", inBean.getClientname());
				parameters.put("clientCode", inBean.getClientcode());
				parameters.put("clientAddress", inBean.getClientAddress());
				parameters.put("subClientCode", "");
				parameters.put("awbCount", inBean.getAwbCount());
				parameters.put("supplyCity", inBean.getCmpny_city());
				
				parameters.put("clientState", inBean.getClientStateName());
				parameters.put("clientStateCode", inBean.getClient_StateGSTIN_code());
				parameters.put("GSTIN", inBean.getClientGSTIN());
			
			
				/*System.out.println("Invoice No.: "+inBean.getInvoice_number());
				System.out.println("Invoice Date: "+inBean.getInvoice_date());
				System.out.println("Branch: "+inBean.getInvoice2branchcode());
				System.out.println("Client Code: "+inBean.getClientcode());
				System.out.println("From Date: "+inBean.getFrom_date());
				System.out.println("To Date: "+inBean.getTo_date());
				System.out.println("Basic Amount: "+inBean.getBasicAmount());
				System.out.println("Fuel Rate: "+inBean.getFuel_rate());
				System.out.println("Fuel: "+inBean.getFuel());
				System.out.println("VAS Amount: "+inBean.getVas_amount());
				System.out.println("Insurance Amount: "+inBean.getInsurance_amount());
				System.out.println("Rate of Percent: "+inBean.getRateof_percent());
				System.out.println("Type Flat/Percent: "+inBean.getType_flat2percent());
				System.out.println("Dis/Add: "+inBean.getDiscount_additional());
				System.out.println("Dis/Add Amount: "+inBean.getDiscount_Additional_amount());
				System.out.println("Reason: "+inBean.getReason());
				System.out.println("Tax Name 2: "+inBean.getCgst_Name());
				System.out.println("CGST Rate: "+inBean.getCgst_rate());
				System.out.println("CGST Amount: "+inBean.getCgst_amt());
				System.out.println("Tax Name 2: "+inBean.getSgst_Name());
				System.out.println("SGST Rate: "+inBean.getSgst_rate());
				System.out.println("SGST Amount: "+inBean.getSgst_amt());
				System.out.println("Tax Name3: "+inBean.getIgst_Name());
				System.out.println("IGST Rate: "+inBean.getIgst_rate());
				System.out.println("IGST Amount: "+inBean.getIgst_amt());
				System.out.println("Taxable Amount: "+inBean.getTaxable_amount());
				System.out.println("GST Amount: "+inBean.getGst_amount());
				System.out.println("Grand Total: "+inBean.getGrandtotal());
				System.out.println("Amount In Words: "+inBean.getAmountInWords());
				System.out.println("Remarks: "+inBean.getRemarks());
				
				
				System.out.println("\n*************** Company Details ****************\n");
				
				System.out.println("Company Code: "+inBean.getCompanyCode());
				System.out.println("Company Name: "+inBean.getCompanyName());
				System.out.println("Tag Line: "+inBean.getCmpny_tagLine());
				System.out.println("Address: "+inBean.getCmpny_address());
				System.out.println("Tax No: "+inBean.getCmpny_serviceTaxNo());
				System.out.println("Pan No: "+inBean.getCmpny_panNo());
				System.out.println("Email: "+inBean.getCmpny_email());
				System.out.println("Bank Name: "+inBean.getCmpny_bankName());
				System.out.println("A/c No: "+inBean.getCmpny_accountNo());
				System.out.println("IFSC Code: "+inBean.getCmpny_ifscCode());
				System.out.println("Condi. 1: "+inBean.getCondition1());
				System.out.println("Condi. 2: "+inBean.getCondition2());
				System.out.println("Condi. 3: "+inBean.getCondition3());
				System.out.println("Condi. 4: "+inBean.getCondition4());
				System.out.println("Condi. 5: "+inBean.getCondition5());
				System.out.println("Condi. 6: "+inBean.getCondition6());
				System.out.println("Company State: "+inBean.getCmpny_state());
				System.out.println("Company Pincode: "+inBean.getCmpny_pincode());
				*/
			
				System.out.println("Invoice Date >> "+inBean.getInvoice_date());
				
				LocalDate localFromDate=LocalDate.parse(inBean.getFrom_date(), localdateformatter);
				Date fromDate=Date.valueOf(localFromDate);
				LocalDate loacalToDate=LocalDate.parse(inBean.getTo_date(), localdateformatter);
				Date toDate=Date.valueOf(loacalToDate);
				
				summarySQL="select air_way_bill_number,master_awbno,booking_date,dailybookingtransaction2city,billing_weight,packets,amount,"
						+ "dox_nondox from dailybookingtransaction where booking_date between '"+fromDate+"' and '"+toDate+"' and "
						+ "invoice_number='"+txtInvoiceNo.getText()+"'";
				
				System.out.println(" >>> 1 >> "+summarySQL);
			}
		}				
		
		System.out.println(" >>> 2 >> "+summarySQL);
		new GenerateSaveReport().exportReport(summarySQL,checkBoxStatus = String.valueOf(chkExportToExcel.isSelected()), src, parameters);
		
		saveDeleteReprint(txtInvoiceNo.getText());
				
	}	
	

// ====================================================================================================================
	
	public void loadActiveInvocieTable() throws Exception
	{
		activeInvoicetabledata.clear();

		int serialno=1;

		System.out.println("Table list size: "+LoadInvoices.list_InvoiceData_Global.size());
		
		for(InvoiceBean inbean: LoadInvoices.list_InvoiceData_Global)
		{
			if(inbean.getStatus().equals("Active"))
			{
				activeInvoicetabledata.add(new InvoiceBillJavaFXBean(serialno,inbean.getInvoice_number(), inbean.getInvoice_date(),
						inbean.getClientcode(), inbean.getFrom_date(), inbean.getTo_date(),inbean.getBasicAmount(), inbean.getFuel(),
						inbean.getVas_amount(), inbean.getInsurance_amount(), inbean.getTaxable_amount(), inbean.getGst_amount(),
						inbean.getGrandtotal(), inbean.getRemarks()));
				serialno++;
			}
		}

		active_InvoiceTable.setItems(activeInvoicetabledata);
		clickActiveInvoiceTableRow();
	}	


// *************************************************************************	
	
	public void clickActiveInvoiceTableRow() throws Exception
	{

		active_InvoiceTable.setRowFactory( tv -> {
			TableRow<InvoiceBillJavaFXBean> row = new TableRow<>();
			row.setOnMouseClicked(event -> {

				InvoiceBillJavaFXBean rowData = row.getItem();

				try 
				{
					/*if (event.getClickCount() == 1 && (! row.isEmpty()))
					{
 							Condition for single click
					}*/

					if (event.getClickCount() == 2 && (! row.isEmpty()) )
					{
						String[] branchCode=rowData.getInvoiceNumber().replaceAll("\\s+","").split("\\/");
						
						for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
						{
							if(branchCode[0].equals(branchBean.getBranchCode()))
							{
								comboBoxBranch.setValue(branchBean.getBranchCode()+ " | "+branchBean.getBranchName());		
								break;
							}
						}
						txtInvoiceNo.setText(rowData.getInvoiceNumber());
					}

				} 
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});
			return row ;
		});
	}	
	
	
// **********************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		imgSearchActiveIcon.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchInActiveIcon.setImage(new Image("/icon/searchicon_blue.png"));
	
		act_Col_serialno.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Integer>("slno"));
		act_Col_InvoiceNo.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceNumber"));
		act_Col_InvoiceDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceDate"));
		act_Col_FromDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("from_date"));
		act_Col_ToDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("to_date"));
		act_Col_ClientCode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientCode"));
		act_Col_BasicAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("basicAmount"));
		act_Col_Fuel.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("fuel"));
		act_Col_VasAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("vas_amount"));
		act_Col_InsuranceAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("insuranceAmount"));
		act_Col_TaxableAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("taxableAmount"));
		act_Col_GSTAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("gst_Amount"));
		act_Col_GrandTotal.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceGrandTotal"));
		act_Col_Remarks.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceRemarks"));
		
		inAct_Col_serialno.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Integer>("slno"));
		inAct_Col_InvoiceNo.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceNumber"));
		inAct_Col_InvoiceDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceDate"));
		inAct_Col_FromDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("from_date"));
		inAct_Col_ToDate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("to_date"));
		inAct_Col_ClientCode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientCode"));
		inAct_Col_BasicAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("basicAmount"));
		inAct_Col_Fuel.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("fuel"));
		inAct_Col_VasAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("vas_amount"));
		inAct_Col_InsuranceAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("insuranceAmount"));
		inAct_Col_TaxableAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("taxableAmount"));
		inAct_Col_GSTAmt.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("gst_Amount"));
		inAct_Col_GrandTotal.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceGrandTotal"));
		inAct_Col_Status.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceRemarks"));
		
		
		try 
		{
			loadBranch();
			
			new LoadCompany().loadCompanyWithName();
			
			if(LoadInvoices.list_InvoiceData_Global.isEmpty()==true)
			{
				new LoadInvoices().loadInvoiceDate();
			}
			
			loadInActiveInvoice();
			
			try {
				loadActiveInvocieTable();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

}
