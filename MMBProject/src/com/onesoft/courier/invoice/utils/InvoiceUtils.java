package com.onesoft.courier.invoice.utils;

public class InvoiceUtils {
	

	public static final String ALLBRANCH = "All Branches";
	public static final String INVOICEDISCOUNT = "Discount";
	public static final String INVOICEADDITIONAL = "Additional Charges";
	public static final String INVOICDEFAULT = "Select";
	
	public static final String TYPE1="Percent(%)";
	public static final String TYPE2="Flat";
	public static final String TYPEDEFAULT = "Select";
	
	public static final String ADDCHARGE="Additional Charges";
	
	public static final String SUBCLIENT1="Default";
	public static final String SUBCLIENT2="All";
	//public static final String SUBCLIENT3="Test";
	
	

}
