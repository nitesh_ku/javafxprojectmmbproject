package com.onesoft.courier.invoice.bean;

public class InvoiceDeleteReprintBean {
	
	private String branchCode;
	private String invoiceNo;
	private String reason;
	private String type_Delete_reprint;
	private String user;
	private String createdate;
	private String deleteReprint_Date;
	private String applicationId;
	private String ipAddress;
	private String hostName;
	private String invoiceStatus;
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getType_Delete_reprint() {
		return type_Delete_reprint;
	}
	public void setType_Delete_reprint(String type_Delete_reprint) {
		this.type_Delete_reprint = type_Delete_reprint;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getDeleteReprint_Date() {
		return deleteReprint_Date;
	}
	public void setDeleteReprint_Date(String deleteReprint_Date) {
		this.deleteReprint_Date = deleteReprint_Date;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	
	
	
}
