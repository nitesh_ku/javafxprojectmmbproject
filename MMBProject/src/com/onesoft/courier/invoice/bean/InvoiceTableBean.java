package com.onesoft.courier.invoice.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class InvoiceTableBean
{
	

	
	
	private SimpleIntegerProperty serialno;
	private SimpleStringProperty invoice_number;
	private SimpleStringProperty invoice_date;
	private SimpleStringProperty awb_no;
	private SimpleStringProperty booking_date;
	private SimpleStringProperty invoice2branchcode;
	private SimpleStringProperty from_date;
	private SimpleStringProperty to_date;
	private SimpleStringProperty clientcode;
	private SimpleStringProperty clientname;
	private SimpleStringProperty sub_clientcode;
	private SimpleStringProperty discount_additional;
	private SimpleStringProperty type_flat2percent;
	private SimpleStringProperty reason;

	private SimpleStringProperty remarks;

	private SimpleDoubleProperty serviceTaxPercentage;
	
	private SimpleDoubleProperty fwb_weight;
	private SimpleDoubleProperty billingweight;
	private SimpleDoubleProperty amount;
	private SimpleDoubleProperty insurance_amount;
	private SimpleDoubleProperty docket_charge;
	private SimpleDoubleProperty cod_amount;
	private SimpleDoubleProperty fod_amount;
	private SimpleDoubleProperty vas_total;
	private SimpleDoubleProperty rateof_percent;
	private SimpleDoubleProperty discount_Additional_amount;
	
	private SimpleDoubleProperty fuel;
	private SimpleDoubleProperty servicetax;
	private SimpleDoubleProperty cess1;
	private SimpleDoubleProperty cess2;
	private SimpleDoubleProperty subtotal;
	private SimpleDoubleProperty total;
	private SimpleDoubleProperty grandtotal;
	private SimpleDoubleProperty amountAfterDis_Add;
	
	private SimpleIntegerProperty ahc;
	private SimpleIntegerProperty mtd;
	private SimpleIntegerProperty pcs;
	
	
	public InvoiceTableBean(int serialno,String awb_no,String booking_date, String branch,String client_code,double weight,int pcs,
			double amount,double insurance_amount,double docketcharge,double vas_total, double fuel, double total)
	{
		super();
		
		this.serialno=new SimpleIntegerProperty(serialno);
		this.awb_no=new SimpleStringProperty(awb_no);
		this.booking_date=new SimpleStringProperty(booking_date);
		this.invoice2branchcode=new SimpleStringProperty(branch);
		this.clientcode=new SimpleStringProperty(client_code);
		this.billingweight=new SimpleDoubleProperty(weight);
		this.amount =new SimpleDoubleProperty(amount);
		this.insurance_amount =new SimpleDoubleProperty(insurance_amount);
		this.docket_charge =new SimpleDoubleProperty(docketcharge);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.vas_total =new SimpleDoubleProperty(vas_total);
		this.fuel =new SimpleDoubleProperty(fuel);
		this.total =new SimpleDoubleProperty(total);
	}
	
	


	
	
	public SimpleStringProperty getDiscount_additional() {
		return discount_additional;
	}






	public void setDiscount_additional(SimpleStringProperty discount_additional) {
		this.discount_additional = discount_additional;
	}






	public SimpleDoubleProperty getDiscount_Additional_amount() {
		return discount_Additional_amount;
	}






	public void setDiscount_Additional_amount(SimpleDoubleProperty discount_Additional_amount) {
		this.discount_Additional_amount = discount_Additional_amount;
	}






	public String getInvoice_number()
	{
		return invoice_number.get();
	}
	public void setInvoice_number(SimpleStringProperty invoice_number)
	{
		this.invoice_number = invoice_number;
	}
	
	
	public String getInvoice_date()
	{
		return invoice_date.get();
	}
	public void setInvoice_date(SimpleStringProperty invoice_date)
	{
		this.invoice_date = invoice_date;
	}
	
	
	public String getInvoice2branchcode()
	{
		return invoice2branchcode.get();
	}
	public void setInvoice2branchcode(SimpleStringProperty invoice2branchcode)
	{
		this.invoice2branchcode = invoice2branchcode;
	}
	
	
	public String getFrom_date()
	{
		return from_date.get();
	}
	public void setFrom_date(SimpleStringProperty from_date)
	{
		this.from_date = from_date;
	}
	
	
	public String getTo_date()
	{
		return to_date.get();
	}
	public void setTo_date(SimpleStringProperty to_date)
	{
		this.to_date = to_date;
	}
	
	
	public String getClientcode()
	{
		return clientcode.get();
	}
	public void setClientcode(SimpleStringProperty clientcode)
	{
		this.clientcode = clientcode;
	}
	
	
	public String getSub_clientcode()
	{
		return sub_clientcode.get();
	}
	public void setSub_clientcode(SimpleStringProperty sub_clientcode)
	{
		this.sub_clientcode = sub_clientcode;
	}
	
	
	public String getType_flat2percent()
	{
		return type_flat2percent.get();
	}
	public void setType_flat2percent(SimpleStringProperty type_flat2percent)
	{
		this.type_flat2percent = type_flat2percent;
	}
	
	
	public String getReason()
	{
		return reason.get();
	}
	public void setReason(SimpleStringProperty reason)
	{
		this.reason = reason;
	}
	
	

	
	
	public String getRemarks()
	{
		return remarks.get();
	}
	public void setRemarks(SimpleStringProperty remarks)
	{
		this.remarks = remarks;
	}
	
	
	public double getServiceTaxPercentage()
	{
		return serviceTaxPercentage.get();
	}
	public void setServiceTaxPercentage(SimpleDoubleProperty serviceTaxPercentage)
	{
		this.serviceTaxPercentage = serviceTaxPercentage;
	}
	
	
	public double getAmount()
	{
		return amount.get();
	}
	public void setAmount(SimpleDoubleProperty amount)
	{
		this.amount = amount;
	}
	
	
	public double getInsurance_amount()
	{
		return insurance_amount.get();
	}
	public void setInsurance_amount(SimpleDoubleProperty insurance_amount)
	{
		this.insurance_amount = insurance_amount;
	}
	
	
	public double getDocket_charge()
	{
		return docket_charge.get();
	}
	public void setDocket_charge(SimpleDoubleProperty docket_charge)
	{
		this.docket_charge = docket_charge;
	}
	
	
	public double getCod_amount()
	{
		return cod_amount.get();
	}
	public void setCod_amount(SimpleDoubleProperty cod_amount)
	{
		this.cod_amount = cod_amount;
	}
	
	
	public double getFod_amount()
	{
		return fod_amount.get();
	}
	public void setFod_amount(SimpleDoubleProperty fod_amount)
	{
		this.fod_amount = fod_amount;
	}
	
	
	/*public double getOther_amount()
	{
		return other_amount.get();
	}
	public void setOther_amount(SimpleDoubleProperty other_amount)
	{
		this.other_amount = other_amount;
	}*/
	
	
	
	public double getFuel()
	{
		return fuel.get();
	}
	public void setFuel(SimpleDoubleProperty fuel)
	{
		this.fuel = fuel;
	}
	
	
	public double getServicetax()
	{
		return servicetax.get();
	}
	public void setServicetax(SimpleDoubleProperty servicetax)
	{
		this.servicetax = servicetax;
	}
	
	
	public double getCess1()
	{
		return cess1.get();
	}
	public void setCess1(SimpleDoubleProperty cess1)
	{
		this.cess1 = cess1;
	}
	
	
	public double getCess2()
	{
		return cess2.get();
	}
	public void setCess2(SimpleDoubleProperty cess2)
	{
		this.cess2 = cess2;
	}
	
	
	public double getSubtotal()
	{
		return subtotal.get();
	}
	public void setSubtotal(SimpleDoubleProperty subtotal)
	{
		this.subtotal = subtotal;
	}
	
	
	public double getTotal()
	{
		return total.get();
	}
	public void setTotal(SimpleDoubleProperty total)
	{
		this.total = total;
	}
	
	
	public double getGrandtotal()
	{
		return grandtotal.get();
	}
	public void setGrandtotal(SimpleDoubleProperty grandtotal)
	{
		this.grandtotal = grandtotal;
	}
	
	
	public double getAmountAfterDis_Add()
	{
		return amountAfterDis_Add.get();
	}
	public void setAmountAfterDis_Add(SimpleDoubleProperty amountAfterDis_Add)
	{
		this.amountAfterDis_Add = amountAfterDis_Add;
	}
	
	
	public Integer getAhc()
	{
		return ahc.get();
	}
	public void setAhc(SimpleIntegerProperty ahc){
		this.ahc = ahc;
	}
	
	
	public Integer getMtd()
	{
		return mtd.get();
	}
	public void setMtd(SimpleIntegerProperty mtd)
	{
		this.mtd = mtd;
	}

	public String getAwb_no() {
		return awb_no.get();
	}

	public void setAwb_no(SimpleStringProperty awb_no) {
		this.awb_no = awb_no;
	}

	public String getBooking_date() {
		return booking_date.get();
	}

	public void setBooking_date(SimpleStringProperty booking_date) {
		this.booking_date = booking_date;
	}

	public Double getBillingweight() {
		return billingweight.get();
	}

	public void setBillingweight(SimpleDoubleProperty billingweight) {
		this.billingweight = billingweight;
	}

	public Integer getSerialno() {
		return serialno.get();
	}

	public void setSerialno(SimpleIntegerProperty serialno) {
		this.serialno = serialno;
	}

	public String getClientname() {
		return clientname.get();
	}

	public void setClientname(SimpleStringProperty clientname) {
		this.clientname = clientname;
	}

	public Double getRateof_percent() {
		return rateof_percent.get();
	}
	public void setRateof_percent(SimpleDoubleProperty rateof_percent) {
		this.rateof_percent = rateof_percent;
	}
	
	public Double getFwb_weight() {
		return fwb_weight.get();
	}
	public void setFwb_weight(SimpleDoubleProperty fwb_weight) {
		this.fwb_weight = fwb_weight;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getVas_total() {
		return vas_total.get();
	}
	public void setVas_total(SimpleDoubleProperty vas_total) {
		this.vas_total = vas_total;
	}




	

	
	
}
