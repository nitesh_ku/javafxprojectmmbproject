package com.onesoft.courier.invoice.bean;



public class InvoiceBean {
	

	
	
	
	
	private String invoice_start;
	private long invoice_end;
	private int serieano;
	private int awbCount;
	
	private String abw_no;
	private String booking_date;
	private String invoice_number;
	private String invoice_date;
	private String invoice2branchcode;
	
	private String clientcode;
	private String clientname;
	private String clientAddress;
	private String clientStateName;
	private String clientGSTIN;
	private String client_StateGSTIN_code;
	private String sub_clientcode;
	private String reason;
	private String remarks;
	private String from_date;
	private String to_date;
	private String amountInWords;
	
	private Double serviceTaxPercentage;
	
	private Double basicAmount;
	private Double fwd_weight;
	private Double billing_weight;
	private Double insurance_amount;
	private Double docket_charge;
	private Double cod_amount;
	private Double fod_amount;
	private Double vas_amount;
	private Double discount;
	private Double additional_charge;
	private Double fuel;
	private Double fuel_rate;
	private Double gst;
	private Double cess1;
	private Double cess2;
	private Double subtotal;
	private Double total;
	private Double grandtotal;
	private Double amountAfterDis_Add;
	private double taxable_amount;
	private double gst_amount;
	private String invoice_paid_status;
	
	private String discount_additional;
	private String type_flat2percent;
	private Double rateof_percent;
	private Double discount_Additional_amount;
	private String status;
	private String invoice_status;
	
	private double cgst_rate;
	private double sgst_rate;
	private double igst_rate;
	
	private double cgst_amt;
	private double sgst_amt;
	private double igst_amt;
	
	
	private String cgst_Name;
	private String sgst_Name;
	private String igst_Name;
	
	private int ahc;
	private int mtd;
	private int pcs;
	
	private double taxable_Value;
	
	
	// company related variables
	
	private String companyCode;
	private String companyName;
	private String cmpny_tagLine;
	private String cmpny_address;
	private String cmpny_city;
	private String cmpny_state;
	private String cmpny_country;
	private String cmpny_serviceTaxNo;
	private String cmpny_panNo;
	private String cmpny_email;
	private String cmpny_bankName;
	private String cmpny_accountNo;
	private String cmpny_ifscCode;
	private String condition1;
	private String condition2;
	private String condition3;
	private String condition4;
	private String condition5;
	private String condition6;
	private String cmpny_pincode;
	
	

	
	public double getCgst_rate() {
		return cgst_rate;
	}
	public void setCgst_rate(double cgst_rate) {
		this.cgst_rate = cgst_rate;
	}
	public double getSgst_rate() {
		return sgst_rate;
	}
	public void setSgst_rate(double sgst_rate) {
		this.sgst_rate = sgst_rate;
	}
	public double getIgst_rate() {
		return igst_rate;
	}
	public void setIgst_rate(double igst_rate) {
		this.igst_rate = igst_rate;
	}
	public String getCgst_Name() {
		return cgst_Name;
	}
	public void setCgst_Name(String cgst_Name) {
		this.cgst_Name = cgst_Name;
	}
	public String getSgst_Name() {
		return sgst_Name;
	}
	public void setSgst_Name(String sgst_Name) {
		this.sgst_Name = sgst_Name;
	}
	public String getIgst_Name() {
		return igst_Name;
	}
	public void setIgst_Name(String igst_Name) {
		this.igst_Name = igst_Name;
	}
	public String getInvoice_number()
	{
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number)
	{
		this.invoice_number = invoice_number;
	}
	public double getCgst_amt() {
		return cgst_amt;
	}
	public void setCgst_amt(double cgst_amt) {
		this.cgst_amt = cgst_amt;
	}
	public double getSgst_amt() {
		return sgst_amt;
	}
	public void setSgst_amt(double sgst_amt) {
		this.sgst_amt = sgst_amt;
	}
	public double getIgst_amt() {
		return igst_amt;
	}
	public void setIgst_amt(double igst_amt) {
		this.igst_amt = igst_amt;
	}
	
	public String getInvoice_date()
	{
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date)
	{
		this.invoice_date = invoice_date;
	}
	
	
	public String getInvoice2branchcode()
	{
		return invoice2branchcode;
	}
	public void setInvoice2branchcode(String invoice2branchcode)
	{
		this.invoice2branchcode = invoice2branchcode;
	}
	
	
	
	
	
	public String getClientcode()
	{
		return clientcode;
	}
	public void setClientcode(String clientcode)
	{
		this.clientcode = clientcode;
	}
	
	
	public String getSub_clientcode()
	{
		return sub_clientcode;
	}
	public void setSub_clientcode(String sub_clientcode)
	{
		this.sub_clientcode = sub_clientcode;
	}
	
	
	public String getType_flat2percent()
	{
		return type_flat2percent;
	}
	public void setType_flat2percent(String type_flat2percent)
	{
		this.type_flat2percent = type_flat2percent;
	}
	
	
	public String getReason()
	{
		return reason;
	}
	public void setReason(String reason)
	{
		this.reason = reason;
	}
	
	
	public String getDiscount_additional() {
		return discount_additional;
	}
	public void setDiscount_additional(String discount_additional) {
		this.discount_additional = discount_additional;
	}
	public Double getRateof_percent() {
		return rateof_percent;
	}
	public void setRateof_percent(Double rateof_percent) {
		this.rateof_percent = rateof_percent;
	}
	public Double getDiscount_Additional_amount() {
		return discount_Additional_amount;
	}
	public void setDiscount_Additional_amount(Double discount_Additional_amount) {
		this.discount_Additional_amount = discount_Additional_amount;
	}
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	
	
	
	
	
	public Double getInsurance_amount()
	{
		return insurance_amount;
	}
	public void setInsurance_amount(Double insurance_amount)
	{
		this.insurance_amount = insurance_amount;
	}
	
	
	public Double getDocket_charge()
	{
		return docket_charge;
	}
	public void setDocket_charge(Double docket_charge)
	{
		this.docket_charge = docket_charge;
	}
	
	
	public Double getCod_amount()
	{
		return cod_amount;
	}
	public void setCod_amount(Double cod_amount)
	{
		this.cod_amount = cod_amount;
	}
	
	
	public Double getFod_amount()
	{
		return fod_amount;
	}
	public void setFod_amount(Double fod_amount)
	{
		this.fod_amount = fod_amount;
	}
	
	
	public Double getDiscount()
	{
		return discount;
	}
	public void setDiscount(Double discount)
	{
		this.discount = discount;
	}
	
	
	public Double getAdditional_charge()
	{
		return additional_charge;
	}
	public void setAdditional_charge(Double additional_charge)
	{
		this.additional_charge = additional_charge;
	}
	
	
	public Double getFuel()
	{
		return fuel;
	}
	public void setFuel(Double fuel)
	{
		this.fuel = fuel;
	}
	


	public Double getCess1()
	{
		return cess1;
	}
	public void setCess1(Double cess1)
	{
		this.cess1 = cess1;
	}
	
	
	public Double getCess2()
	{
		return cess2;
	}
	public void setCess2(Double cess2)
	{
		this.cess2 = cess2;
	}
	
	
	public Double getSubtotal()
	{
		return subtotal;
	}
	public void setSubtotal(Double subtotal)
	{
		this.subtotal = subtotal;
	}
	
	
	public Double getGrandtotal()
	{
		return grandtotal;
	}
	public void setGrandtotal(Double grandtotal)
	{
		this.grandtotal = grandtotal;
	}
	
	
	public int getAhc()
	{
		return ahc;
	}
	public void setAhc(int ahc)
	{
		this.ahc = ahc;
	}
	
	
	public int getMtd()
	{
		return mtd;
	}
	public void setMtd(int mtd)
	{
		this.mtd = mtd;
	}
	
	
	
	
	
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	public Double getAmountAfterDis_Add() {
		return amountAfterDis_Add;
	}
	public void setAmountAfterDis_Add(Double amountAfterDis_Add) {
		this.amountAfterDis_Add = amountAfterDis_Add;
	}
	
	
	public Double getServiceTaxPercentage() {
		return serviceTaxPercentage;
	}
	public void setServiceTaxPercentage(Double serviceTaxPercentage) {
		this.serviceTaxPercentage = serviceTaxPercentage;
	}
	
	
	public String getInvoice_start() {
		return invoice_start;
	}
	public void setInvoice_start(String invoice_start) {
		this.invoice_start = invoice_start;
	}
	
	
	public long getInvoice_end() {
		return invoice_end;
	}
	public void setInvoice_end(long l) {
		this.invoice_end = l;
	}
	public String getAbw_no() {
		return abw_no;
	}
	public void setAbw_no(String abw_no) {
		this.abw_no = abw_no;
	}
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public Double getBilling_weight() {
		return billing_weight;
	}
	public void setBilling_weight(Double billing_weight) {
		this.billing_weight = billing_weight;
	}
	public int getSerieano() {
		return serieano;
	}
	public void setSerieano(int serieano) {
		this.serieano = serieano;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public Double getFwd_weight() {
		return fwd_weight;
	}
	public void setFwd_weight(Double fwd_weight) {
		this.fwd_weight = fwd_weight;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInvoice_status() {
		return invoice_status;
	}
	public void setInvoice_status(String invoice_status) {
		this.invoice_status = invoice_status;
	}
	public Double getBasicAmount() {
		return basicAmount;
	}
	public void setBasicAmount(Double basicAmount) {
		this.basicAmount = basicAmount;
	}
	public Double getVas_amount() {
		return vas_amount;
	}
	public void setVas_amount(Double vas_amount) {
		this.vas_amount = vas_amount;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getTaxable_Value() {
		return taxable_Value;
	}
	public void setTaxable_Value(double taxable_Value) {
		this.taxable_Value = taxable_Value;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	public double getGst_amount() {
		return gst_amount;
	}
	public void setGst_amount(double gst_amount) {
		this.gst_amount = gst_amount;
	}
	public double getTaxable_amount() {
		return taxable_amount;
	}
	public void setTaxable_amount(double taxable_amount) {
		this.taxable_amount = taxable_amount;
	}
		
	
	
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCmpny_tagLine() {
		return cmpny_tagLine;
	}
	public void setCmpny_tagLine(String cmpny_tagLine) {
		this.cmpny_tagLine = cmpny_tagLine;
	}
	public String getCmpny_address() {
		return cmpny_address;
	}
	public void setCmpny_address(String cmpny_address) {
		this.cmpny_address = cmpny_address;
	}
	public String getCmpny_city() {
		return cmpny_city;
	}
	public void setCmpny_city(String cmpny_city) {
		this.cmpny_city = cmpny_city;
	}
	public String getCmpny_state() {
		return cmpny_state;
	}
	public void setCmpny_state(String cmpny_state) {
		this.cmpny_state = cmpny_state;
	}
	public String getCmpny_country() {
		return cmpny_country;
	}
	public void setCmpny_country(String cmpny_country) {
		this.cmpny_country = cmpny_country;
	}
	public String getCmpny_serviceTaxNo() {
		return cmpny_serviceTaxNo;
	}
	public void setCmpny_serviceTaxNo(String cmpny_serviceTaxNo) {
		this.cmpny_serviceTaxNo = cmpny_serviceTaxNo;
	}
	public String getCmpny_panNo() {
		return cmpny_panNo;
	}
	public void setCmpny_panNo(String cmpny_panNo) {
		this.cmpny_panNo = cmpny_panNo;
	}
	public String getCmpny_email() {
		return cmpny_email;
	}
	public void setCmpny_email(String cmpny_email) {
		this.cmpny_email = cmpny_email;
	}
	public String getCmpny_bankName() {
		return cmpny_bankName;
	}
	public void setCmpny_bankName(String cmpny_bankName) {
		this.cmpny_bankName = cmpny_bankName;
	}
	public String getCmpny_accountNo() {
		return cmpny_accountNo;
	}
	public void setCmpny_accountNo(String cmpny_accountNo) {
		this.cmpny_accountNo = cmpny_accountNo;
	}
	public String getCmpny_ifscCode() {
		return cmpny_ifscCode;
	}
	public void setCmpny_ifscCode(String cmpny_ifscCode) {
		this.cmpny_ifscCode = cmpny_ifscCode;
	}
	public String getCondition1() {
		return condition1;
	}
	public void setCondition1(String condition1) {
		this.condition1 = condition1;
	}
	public String getCondition2() {
		return condition2;
	}
	public void setCondition2(String condition2) {
		this.condition2 = condition2;
	}
	public String getCondition3() {
		return condition3;
	}
	public void setCondition3(String condition3) {
		this.condition3 = condition3;
	}
	public String getCondition4() {
		return condition4;
	}
	public void setCondition4(String condition4) {
		this.condition4 = condition4;
	}
	public String getCondition5() {
		return condition5;
	}
	public void setCondition5(String condition5) {
		this.condition5 = condition5;
	}
	public String getCondition6() {
		return condition6;
	}
	public void setCondition6(String condition6) {
		this.condition6 = condition6;
	}
	public String getCmpny_pincode() {
		return cmpny_pincode;
	}
	public void setCmpny_pincode(String cmpny_pincode) {
		this.cmpny_pincode = cmpny_pincode;
	}
	public Double getFuel_rate() {
		return fuel_rate;
	}
	public void setFuel_rate(Double fuel_rate) {
		this.fuel_rate = fuel_rate;
	}
	public String getAmountInWords() {
		return amountInWords;
	}
	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}
	public String getClientAddress() {
		return clientAddress;
	}
	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}
	public int getAwbCount() {
		return awbCount;
	}
	public void setAwbCount(int awbCount) {
		this.awbCount = awbCount;
	}
	public String getClientStateName() {
		return clientStateName;
	}
	public void setClientStateName(String clientStateName) {
		this.clientStateName = clientStateName;
	}
	public String getClient_StateGSTIN_code() {
		return client_StateGSTIN_code;
	}
	public void setClient_StateGSTIN_code(String client_StateGSTIN_code) {
		this.client_StateGSTIN_code = client_StateGSTIN_code;
	}
	public String getClientGSTIN() {
		return clientGSTIN;
	}
	public void setClientGSTIN(String clientGSTIN) {
		this.clientGSTIN = clientGSTIN;
	}
	public String getInvoice_paid_status() {
		return invoice_paid_status;
	}
	public void setInvoice_paid_status(String invoice_paid_status) {
		this.invoice_paid_status = invoice_paid_status;
	}
	
	
}
