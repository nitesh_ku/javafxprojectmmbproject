package com.onesoft.courier.invoice.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class InvoiceBillJavaFXBean
{
	private SimpleIntegerProperty slno;
	private SimpleStringProperty invoiceNumber;
	private SimpleStringProperty invoiceDate;
	private SimpleStringProperty invoiceClientCode;
	private SimpleStringProperty from_date;
	private SimpleStringProperty to_date;
	private SimpleDoubleProperty basicAmount;
	private SimpleDoubleProperty fuel;
	private SimpleDoubleProperty vas_amount;
	private SimpleDoubleProperty insuranceAmount;
	private SimpleDoubleProperty taxableAmount;
	private SimpleDoubleProperty gst_Amount;
	private SimpleStringProperty invoiceRemarks;
	private SimpleDoubleProperty invoiceGrandTotal;
	
	
	
	public InvoiceBillJavaFXBean(int invoiceserialno,String invoicenumber,String invoicedate,String invoiceClientCode,String from_date,
			String to_date ,double basic_amt,double fuel,double vas_amt,double insurance_amt,double taxable_amount,double gst,
			double grandtotal,String remarks)
	{
		super();
		this.slno=new SimpleIntegerProperty(invoiceserialno);
		this.invoiceNumber=new SimpleStringProperty(invoicenumber);
		this.invoiceDate=new SimpleStringProperty(invoicedate);
		this.invoiceClientCode=new SimpleStringProperty(invoiceClientCode);
		this.from_date=new SimpleStringProperty(from_date);
		this.to_date=new SimpleStringProperty(to_date);
		this.basicAmount=new SimpleDoubleProperty(basic_amt);
		this.fuel=new SimpleDoubleProperty(fuel);
		this.vas_amount=new SimpleDoubleProperty(vas_amt);
		this.insuranceAmount=new SimpleDoubleProperty(insurance_amt);
		this.taxableAmount=new SimpleDoubleProperty(taxable_amount);
		this.gst_Amount=new SimpleDoubleProperty(gst);
		this.invoiceRemarks=new SimpleStringProperty(remarks);
		this.invoiceGrandTotal=new SimpleDoubleProperty(grandtotal);
		
		
	}
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public double getBasicAmount() {
		return basicAmount.get();
	}
	public void setBasicAmount(SimpleDoubleProperty basicAmount) {
		this.basicAmount = basicAmount;
	}
	public double getFuel() {
		return fuel.get();
	}
	public void setFuel(SimpleDoubleProperty fuel) {
		this.fuel = fuel;
	}
	public double getVas_amount() {
		return vas_amount.get();
	}
	public void setVas_amount(SimpleDoubleProperty vas_amount) {
		this.vas_amount = vas_amount;
	}
	public double getInsuranceAmount() {
		return insuranceAmount.get();
	}
	public void setInsuranceAmount(SimpleDoubleProperty insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public double getTaxableAmount() {
		return taxableAmount.get();
	}
	public void setTaxableAmount(SimpleDoubleProperty taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public double getGst_Amount() {
		return gst_Amount.get();
	}
	public void setGst_Amount(SimpleDoubleProperty gst_Amount) {
		this.gst_Amount = gst_Amount;
	}

	public int getInvoiceserialno() {
		return slno.get();
	}
	public void setInvoiceserialno(SimpleIntegerProperty invoiceserialno) {
		this.slno = invoiceserialno;
	}
	public String getInvoiceNumber() {
		return invoiceNumber.get();
	}
	public void setInvoiceNumber(SimpleStringProperty invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate.get();
	}
	public void setInvoiceDate(SimpleStringProperty invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFrom_date() {
		return from_date.get();
	}
	public void setFrom_date(SimpleStringProperty from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date.get();
	}
	public void setTo_date(SimpleStringProperty to_date) {
		this.to_date = to_date;
	}
	public String getInvoiceClientCode() {
		return invoiceClientCode.get();
	}
	public void setInvoiceClientCode(SimpleStringProperty invoiceClientCode) {
		this.invoiceClientCode = invoiceClientCode;
	}
	public String getInvoiceRemarks() {
		return invoiceRemarks.get();
	}
	public void setInvoiceRemarks(SimpleStringProperty invoiceRemarks) {
		this.invoiceRemarks = invoiceRemarks;
	}
	public double getInvoiceGrandTotal() {
		return invoiceGrandTotal.get();
	}
	public void setInvoiceGrandTotal(SimpleDoubleProperty invoiceGrandTotal) {
		this.invoiceGrandTotal = invoiceGrandTotal;
	}

}
