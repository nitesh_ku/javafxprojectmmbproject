package com.onesoft.courier.booking.bean;



public class OutwardBean {

	private int slno;
	private String networkCode;
	private String awbNo;
	private String forwardingNo;
	private String forwarderService;
	private String masterAwbNo;
	private String clientCode;
	private String mode_serivce;
	private int pcs;
	private double weight;
	private String destination;
	private int zipcode;
	
	private String travelStatus;
	
	
	public String getNetworkCode() {
		return networkCode;
	}
	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public String getForwardingNo() {
		return forwardingNo;
	}
	public void setForwardingNo(String forwardingNo) {
		this.forwardingNo = forwardingNo;
	}
	
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	public String getMasterAwbNo() {
		return masterAwbNo;
	}
	public void setMasterAwbNo(String masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getMode_serivce() {
		return mode_serivce;
	}
	public void setMode_serivce(String mode_serivce) {
		this.mode_serivce = mode_serivce;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getTravelStatus() {
		return travelStatus;
	}
	public void setTravelStatus(String travelStatus) {
		this.travelStatus = travelStatus;
	}
	public String getForwarderService() {
		return forwarderService;
	}
	public void setForwarderService(String forwarderService) {
		this.forwarderService = forwarderService;
	}
	

}
