package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class DataEntryTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty bookingDate;
	private SimpleStringProperty origin;
	private SimpleStringProperty destination;
	private SimpleStringProperty network;
	private SimpleStringProperty service;
	
	private SimpleLongProperty consigNOR_Phone;
	private SimpleStringProperty consigNOR_Name;
	private SimpleStringProperty consigNOR_Address;
	private SimpleStringProperty consigNOR_ContactPerson;
	private SimpleIntegerProperty consigNOR_Zipcode;
	private SimpleStringProperty consigNOR_City;
	private SimpleStringProperty consigNOR_State;
	private SimpleStringProperty consigNOR_Country;
	private SimpleStringProperty consigNOR_Email;
	
	private SimpleLongProperty consigNEE_Phone;
	private SimpleStringProperty consigNEE_Name;
	private SimpleStringProperty consigNEE_Address;
	private SimpleStringProperty consigNEE_ContactPerson;
	private SimpleIntegerProperty consigNEE_Zipcode;
	private SimpleStringProperty consigNEE_City;
	private SimpleStringProperty consigNEE_State;
	private SimpleStringProperty consigNEE_Country;
	private SimpleStringProperty consigNEE_Email;
	private SimpleStringProperty consigNEE_Remarks;
	
	private SimpleStringProperty type;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty actualWeight;
	private SimpleDoubleProperty volumeWeight;
	private SimpleDoubleProperty billWeight;
	private SimpleStringProperty insurance;
	private SimpleStringProperty invoiceNo;
	private SimpleDoubleProperty InvoiceAmount;
	private SimpleDoubleProperty InsuranceAmount;
	
	public DataEntryTableBean(int slno, String awbno, String bookingdate, String origin, String destination, String network,String service,
			String consignor_name, String consignee_name, String dnd,double actual_weight,double vol_weight, double bill_weight,int pcs)
	{
		
		this.slno=new SimpleIntegerProperty(slno);
		this.awbNo=new SimpleStringProperty(awbno);
		this.bookingDate=new SimpleStringProperty(bookingdate);
		this.origin=new SimpleStringProperty(origin);
		this.destination=new SimpleStringProperty(destination);
		this.network=new SimpleStringProperty(network);
		this.service=new SimpleStringProperty(service);
		this.consigNOR_Name=new SimpleStringProperty(consignor_name);
		this.consigNEE_Name=new SimpleStringProperty(consignee_name);
		this.type=new SimpleStringProperty(dnd);
		this.actualWeight=new SimpleDoubleProperty(actual_weight);
		this.volumeWeight=new SimpleDoubleProperty(vol_weight);
		this.billWeight=new SimpleDoubleProperty(bill_weight);
		this.pcs=new SimpleIntegerProperty(pcs);
		
		
	}
	
	
	
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	public void setOrigin(SimpleStringProperty origin) {
		this.origin = origin;
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public void setNetwork(SimpleStringProperty network) {
		this.network = network;
	}
	public void setService(SimpleStringProperty service) {
		this.service = service;
	}
	public void setConsigNOR_Phone(SimpleLongProperty consigNOR_Phone) {
		this.consigNOR_Phone = consigNOR_Phone;
	}
	public void setConsigNOR_Name(SimpleStringProperty consigNOR_Name) {
		this.consigNOR_Name = consigNOR_Name;
	}
	public void setConsigNOR_Address(SimpleStringProperty consigNOR_Address) {
		this.consigNOR_Address = consigNOR_Address;
	}
	public void setConsigNOR_ContactPerson(SimpleStringProperty consigNOR_ContactPerson) {
		this.consigNOR_ContactPerson = consigNOR_ContactPerson;
	}
	public void setConsigNOR_Zipcode(SimpleIntegerProperty consigNOR_Zipcode) {
		this.consigNOR_Zipcode = consigNOR_Zipcode;
	}
	public void setConsigNOR_City(SimpleStringProperty consigNOR_City) {
		this.consigNOR_City = consigNOR_City;
	}
	public void setConsigNOR_State(SimpleStringProperty consigNOR_State) {
		this.consigNOR_State = consigNOR_State;
	}
	public void setConsigNOR_Country(SimpleStringProperty consigNOR_Country) {
		this.consigNOR_Country = consigNOR_Country;
	}
	public void setConsigNOR_Email(SimpleStringProperty consigNOR_Email) {
		this.consigNOR_Email = consigNOR_Email;
	}
	public void setConsigNEE_Phone(SimpleLongProperty consigNEE_Phone) {
		this.consigNEE_Phone = consigNEE_Phone;
	}
	public void setConsigNEE_Name(SimpleStringProperty consigNEE_Name) {
		this.consigNEE_Name = consigNEE_Name;
	}
	public void setConsigNEE_Address(SimpleStringProperty consigNEE_Address) {
		this.consigNEE_Address = consigNEE_Address;
	}
	public void setConsigNEE_ContactPerson(SimpleStringProperty consigNEE_ContactPerson) {
		this.consigNEE_ContactPerson = consigNEE_ContactPerson;
	}
	public void setConsigNEE_Zipcode(SimpleIntegerProperty consigNEE_Zipcode) {
		this.consigNEE_Zipcode = consigNEE_Zipcode;
	}
	public void setConsigNEE_City(SimpleStringProperty consigNEE_City) {
		this.consigNEE_City = consigNEE_City;
	}
	public void setConsigNEE_State(SimpleStringProperty consigNEE_State) {
		this.consigNEE_State = consigNEE_State;
	}
	public void setConsigNEE_Country(SimpleStringProperty consigNEE_Country) {
		this.consigNEE_Country = consigNEE_Country;
	}
	public void setConsigNEE_Email(SimpleStringProperty consigNEE_Email) {
		this.consigNEE_Email = consigNEE_Email;
	}
	public void setConsigNEE_Remarks(SimpleStringProperty consigNEE_Remarks) {
		this.consigNEE_Remarks = consigNEE_Remarks;
	}
	public void setType(SimpleStringProperty type) {
		this.type = type;
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public void setActualWeight(SimpleDoubleProperty actualWeight) {
		this.actualWeight = actualWeight;
	}
	public void setVolumeWeight(SimpleDoubleProperty volumeWeight) {
		this.volumeWeight = volumeWeight;
	}
	public void setBillWeight(SimpleDoubleProperty billWeight) {
		this.billWeight = billWeight;
	}
	public void setInsurance(SimpleStringProperty insurance) {
		this.insurance = insurance;
	}
	public void setInvoiceNo(SimpleStringProperty invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public void setInvoiceAmount(SimpleDoubleProperty invoiceAmount) {
		InvoiceAmount = invoiceAmount;
	}
	public void setInsuranceAmount(SimpleDoubleProperty insuranceAmount) {
		InsuranceAmount = insuranceAmount;
	}
	
	
	public String getAwbNo() {
		return awbNo.get();
	}
	public String getBookingDate() {
		return bookingDate.get();
	}
	public String getOrigin() {
		return origin.get();
	}
	public String getDestination() {
		return destination.get();
	}
	public String getNetwork() {
		return network.get();
	}
	public String getService() {
		return service.get();
	}
	public long getConsigNOR_Phone() {
		return consigNOR_Phone.get();
	}
	public String getConsigNOR_Name() {
		return consigNOR_Name.get();
	}
	public String getConsigNOR_Address() {
		return consigNOR_Address.get();
	}
	public String getConsigNOR_ContactPerson() {
		return consigNOR_ContactPerson.get();
	}
	public int getConsigNOR_Zipcode() {
		return consigNOR_Zipcode.get();
	}
	public String getConsigNOR_City() {
		return consigNOR_City.get();
	}
	public String getConsigNOR_State() {
		return consigNOR_State.get();
	}
	public String getConsigNOR_Country() {
		return consigNOR_Country.get();
	}
	public String getConsigNOR_Email() {
		return consigNOR_Email.get();
	}
	public long getConsigNEE_Phone() {
		return consigNEE_Phone.get();
	}
	public String getConsigNEE_Name() {
		return consigNEE_Name.get();
	}
	public String getConsigNEE_Address() {
		return consigNEE_Address.get();
	}
	public String getConsigNEE_ContactPerson() {
		return consigNEE_ContactPerson.get();
	}
	public int getConsigNEE_Zipcode() {
		return consigNEE_Zipcode.get();
	}
	public String getConsigNEE_City() {
		return consigNEE_City.get();
	}
	public String getConsigNEE_State() {
		return consigNEE_State.get();
	}
	public String getConsigNEE_Country() {
		return consigNEE_Country.get();
	}
	public String getConsigNEE_Email() {
		return consigNEE_Email.get();
	}
	public String getConsigNEE_Remarks() {
		return consigNEE_Remarks.get();
	}
	public String getType() {
		return type.get();
	}
	public int getPcs() {
		return pcs.get();
	}
	public double getActualWeight() {
		return actualWeight.get();
	}
	public double getVolumeWeight() {
		return volumeWeight.get();
	}
	public double getBillWeight() {
		return billWeight.get();
	}
	public String getInsurance() {
		return insurance.get();
	}
	public String getInvoiceNo() {
		return invoiceNo.get();
	}
	public double getInvoiceAmount() {
		return InvoiceAmount.get();
	}
	public double getInsuranceAmount() {
		return InsuranceAmount.get();
	}



	public int getSlno() {
		return slno.get();
	}



	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	
	
	
	
	
	

}
