package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class OutwardForwardedTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty forwardingNo;
	private SimpleStringProperty networkCode;
	private SimpleStringProperty masterAwbNo;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty clientCode;
	private SimpleStringProperty mode_serivce;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty weight;
	private SimpleStringProperty destination;
	private SimpleIntegerProperty zipcode;
	private SimpleStringProperty forwarderService;
	
	
	public OutwardForwardedTableBean(int slno,String masterAwbNo,String awbNo,String forwardingNo,String networkCode,String clientCode,String mode_service,
			String forwarderService,int pcs,double weight,String destination,int zipcode)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.forwardingNo=new SimpleStringProperty(forwardingNo);
		this.networkCode=new SimpleStringProperty(networkCode);
		this.masterAwbNo=new SimpleStringProperty(masterAwbNo);
		this.awbNo=new SimpleStringProperty(awbNo);
		this.clientCode=new SimpleStringProperty(clientCode);
		this.mode_serivce=new SimpleStringProperty(mode_service);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.setForwarderService(new SimpleStringProperty(forwarderService));
		this.weight=new SimpleDoubleProperty(weight);
		this.destination=new SimpleStringProperty(destination);
		this.zipcode=new SimpleIntegerProperty(zipcode);
		
	}
	
	
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getForwardingNo() {
		return forwardingNo.get();
	}
	public void setForwardingNo(SimpleStringProperty forwardingNo) {
		this.forwardingNo = forwardingNo;
	}
	public String getNetworkCode() {
		return networkCode.get();
	}
	public void setNetworkCode(SimpleStringProperty networkCode) {
		this.networkCode = networkCode;
	}
	public String getMasterAwbNo() {
		return masterAwbNo.get();
	}
	public void setMasterAwbNo(SimpleStringProperty masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getAwbNo() {
		return awbNo.get();
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public String getClientCode() {
		return clientCode.get();
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public String getMode_serivce() {
		return mode_serivce.get();
	}
	public void setMode_serivce(SimpleStringProperty mode_serivce) {
		this.mode_serivce = mode_serivce;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getWeight() {
		return weight.get();
	}
	public void setWeight(SimpleDoubleProperty weight) {
		this.weight = weight;
	}
	public String getDestination() {
		return destination.get();
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public int getZipcode() {
		return zipcode.get();
	}
	public void setZipcode(SimpleIntegerProperty zipcode) {
		this.zipcode = zipcode;
	}
	public String getForwarderService() {
		return forwarderService.get();
	}
	public void setForwarderService(SimpleStringProperty forwarderService) {
		this.forwarderService = forwarderService;
	}
	
	

}
