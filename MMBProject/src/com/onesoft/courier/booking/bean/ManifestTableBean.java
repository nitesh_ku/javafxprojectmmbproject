package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ManifestTableBean {

	private SimpleStringProperty srNo;
	
	private SimpleStringProperty  masterAwbNo;
	
	private SimpleStringProperty awbNo;
	
	private SimpleStringProperty  date;
	
	private SimpleStringProperty client;
	
	private SimpleStringProperty network;
	
	private SimpleStringProperty service;
	
	private SimpleIntegerProperty destination;
	
	private SimpleDoubleProperty weight;
	
	private SimpleStringProperty packets;
	
	
	
	public ManifestTableBean(){
		
		this.srNo=new SimpleStringProperty();
		this.masterAwbNo=new SimpleStringProperty();
		this.awbNo=new SimpleStringProperty();
		this.date=new SimpleStringProperty();
		this.client=new SimpleStringProperty();
		this.network=new SimpleStringProperty();
		this.service=new SimpleStringProperty();
		this.destination=new SimpleIntegerProperty();
		this.weight=new SimpleDoubleProperty();
		this.packets=new SimpleStringProperty();
	}
	

	public final SimpleStringProperty srNoProperty() {
		return this.srNo;
	}
	

	public final String getSrNo() {
		return this.srNoProperty().get();
	}
	

	public final void setSrNo(final String srNo) {
		this.srNoProperty().set(srNo);
	}
	

	public final SimpleStringProperty masterAwbNoProperty() {
		return this.masterAwbNo;
	}
	

	public final String getMasterAwbNo() {
		return this.masterAwbNoProperty().get();
	}
	

	public final void setMasterAwbNo(final String awbNo) {
		this.masterAwbNoProperty().set(awbNo);
	}
	

	public final SimpleStringProperty dateProperty() {
		return this.date;
	}
	

	public final String getDate() {
		return this.dateProperty().get();
	}
	

	public final void setDate(final String date) {
		this.dateProperty().set(date);
	}
	

	public final SimpleStringProperty clientProperty() {
		return this.client;
	}
	

	public final String getClient() {
		return this.clientProperty().get();
	}
	

	public final void setClient(final String client) {
		this.clientProperty().set(client);
	}
	

	public final SimpleStringProperty networkProperty() {
		return this.network;
	}
	

	public final String getNetwork() {
		return this.networkProperty().get();
	}
	

	public final void setNetwork(final String network) {
		this.networkProperty().set(network);
	}
	

	public final SimpleStringProperty serviceProperty() {
		return this.service;
	}
	

	public final String getService() {
		return this.serviceProperty().get();
	}
	

	public final void setService(final String service) {
		this.serviceProperty().set(service);
	}
	

	public final SimpleIntegerProperty destinationProperty() {
		return this.destination;
	}
	

	public final Integer getDestination() {
		return this.destinationProperty().get();
	}
	

	public final void setDestination(final Integer destination) {
		this.destinationProperty().set(destination);
	}
	

	public final SimpleDoubleProperty weightProperty() {
		return this.weight;
	}
	

	public final double getWeight() {
		return this.weightProperty().get();
	}
	

	public final void setWeight(final double weight) {
		this.weightProperty().set(weight);
	}


	public final SimpleStringProperty awbNoProperty() {
		return this.awbNo;
	}
	


	public final String getAwbNo() {
		return this.awbNoProperty().get();
	}
	


	public final void setAwbNo(final String awbNo) {
		this.awbNoProperty().set(awbNo);
	}


	public final SimpleStringProperty packetsProperty() {
		return this.packets;
	}
	


	public final String getPackets() {
		return this.packetsProperty().get();
	}
	


	public final void setPackets(final String packets) {
		this.packetsProperty().set(packets);
	}
	
	
	
}
