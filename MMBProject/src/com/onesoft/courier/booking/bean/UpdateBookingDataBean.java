package com.onesoft.courier.booking.bean;

public class UpdateBookingDataBean {
	
	private int slno;
	private String masterAwbNo;
	private String bookingDate;
	private String mode_Service;
	private double actualWt;
	private double chargableBillingWt;
	private String destinationPincode;
	private int pcs;
	private String refNo;
	
	private String issue_InSheet;
	private String issue_ServiceMisMatch;
	
	
	public String getIssue_InSheet() {
		return issue_InSheet;
	}
	public void setIssue_InSheet(String issue_InSheet) {
		this.issue_InSheet = issue_InSheet;
	}
	public String getIssue_ServiceMisMatch() {
		return issue_ServiceMisMatch;
	}
	public void setIssue_ServiceMisMatch(String issue_ServiceMisMatch) {
		this.issue_ServiceMisMatch = issue_ServiceMisMatch;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getMode_Service() {
		return mode_Service;
	}
	public void setMode_Service(String mode_Service) {
		this.mode_Service = mode_Service;
	}
	public double getActualWt() {
		return actualWt;
	}
	public void setActualWt(double actualWt) {
		this.actualWt = actualWt;
	}
	public double getChargableBillingWt() {
		return chargableBillingWt;
	}
	public void setChargableBillingWt(double chargableBillingWt) {
		this.chargableBillingWt = chargableBillingWt;
	}
	public String getDestinationPincode() {
		return destinationPincode;
	}
	public void setDestinationPincode(String destinationPincode) {
		this.destinationPincode = destinationPincode;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getMasterAwbNo() {
		return masterAwbNo;
	}
	public void setMasterAwbNo(String masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	
}
