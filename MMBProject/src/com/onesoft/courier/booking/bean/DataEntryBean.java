package com.onesoft.courier.booking.bean;

public class DataEntryBean {
	
	private int slno;
	private String masterAwbNo;
	private String awbNo;
	private String bookingDate;
	private String origin;
	private String originPincode;
	private String destination;
	private String network;
	private String service;
	private String client;
	private String forwarderNo;
	private String forwarderAccount;
	private String forwarderService;
	private String pincode;
	private String city;
	private String dox_nondox;
	private String dimension;
	
	
	private String spotRate;
	private String consigNOR_Phone;
	private String consigNOR_Name;
	private String consigNOR_Address;
	private String consigNOR_ContactPerson;
	//private int consigNOR_Zipcode;
	private String consigNOR_Zipcode;
	private String consigNOR_City;
	private String consigNOR_State;
	private String consigNOR_Country;
	private String consigNOR_Email;
	
	private String consigNEE_Phone;
	private String consigNEE_Name;
	private String consigNEE_Address;
	private String consigNEE_ContactPerson;
	//private int consigNEE_Zipcode;
	private String consigNEE_Zipcode;
	private String consigNEE_City;
	private String consigNEE_State;
	private String consigNEE_Country;
	private String consigNEE_Email;
	private String consigNEE_Remarks;
	
	private String type;
	private int pcs;
	private double basicAmount;
	private double fuelRate;
	private double fuelAmount;
	private double vasTotal;
	private double taxableValueAmount;
	private double total;
	private double otherVasTotal;
	
	
	private String tax_name1;
	private String tax_name2;
	private String tax_name3;
	
	private double tax_rate1;
	private double tax_rate2;
	private double tax_rate3;

	private double tax_amount1;
	private double tax_amount2;
	private double tax_amount3;
	
	private double gst;
	

	private double docketCharges;
	private double length_dimension;
	private double width_dimension;
	private double height_dimension;
	private double actualWeight;
	private double volumeWeight;
	private double billWeight;
	private String insurance;
	private String invoiceNo;
	private double InvoiceAmount;
	private double InsuranceAmount;
	private double InsuranceRate;
	private String consignor_Consignee_Type;
	
	private String travelStatus;
	
	private double cod_amount_vas;
	private double expenseAmountTotal_VAS;
	private String addOnExpense_VAS;
	private String vasName;
	private String expenseAmount_VAS;
	private double doi_VAS;
	private double holdAtOffice_VAS;
	private double oct_Fee_VAS;
	private double hfd_Charges_VAS;
	private int hfd_floor_number;
	private double greenTax_VAS;
	private double reversePickup_VAS;
	private double tdd_VAS;
	private double criticalService_VAS;
	private double AddressCorrection_VAS;
	private double toPay_VAS;
	private double oss_VAS;
	
	private double opa_VAS;
	private double oda_VAS;
	
	
	public double getOpa_VAS() {
		return opa_VAS;
	}
	public void setOpa_VAS(double opa_VAS) {
		this.opa_VAS = opa_VAS;
	}
	public double getOda_VAS() {
		return oda_VAS;
	}
	public void setOda_VAS(double oda_VAS) {
		this.oda_VAS = oda_VAS;
	}
	
	
	public double getDoi_VAS() {
		return doi_VAS;
	}
	public void setDoi_VAS(double doi_VAS) {
		this.doi_VAS = doi_VAS;
	}
	public double getHoldAtOffice_VAS() {
		return holdAtOffice_VAS;
	}
	public void setHoldAtOffice_VAS(double holdAtOffice_VAS) {
		this.holdAtOffice_VAS = holdAtOffice_VAS;
	}
	public double getOct_Fee_VAS() {
		return oct_Fee_VAS;
	}
	public void setOct_Fee_VAS(double oct_Fee_VAS) {
		this.oct_Fee_VAS = oct_Fee_VAS;
	}
	public double getHfd_Charges_VAS() {
		return hfd_Charges_VAS;
	}
	public void setHfd_Charges_VAS(double hfd_Charges_VAS) {
		this.hfd_Charges_VAS = hfd_Charges_VAS;
	}
	public double getGreenTax_VAS() {
		return greenTax_VAS;
	}
	public void setGreenTax_VAS(double greenTax_VAS) {
		this.greenTax_VAS = greenTax_VAS;
	}
	public double getReversePickup_VAS() {
		return reversePickup_VAS;
	}
	public void setReversePickup_VAS(double reversePickup_VAS) {
		this.reversePickup_VAS = reversePickup_VAS;
	}
	public double getTdd_VAS() {
		return tdd_VAS;
	}
	public void setTdd_VAS(double tdd_VAS) {
		this.tdd_VAS = tdd_VAS;
	}
	public double getCriticalService_VAS() {
		return criticalService_VAS;
	}
	public void setCriticalService_VAS(double criticalService_VAS) {
		this.criticalService_VAS = criticalService_VAS;
	}
	public double getAddressCorrection_VAS() {
		return AddressCorrection_VAS;
	}
	public void setAddressCorrection_VAS(double addressCorrection_VAS) {
		AddressCorrection_VAS = addressCorrection_VAS;
	}
	public String getForwarderNo() {
		return forwarderNo;
	}
	public void setForwarderNo(String forwarderNo) {
		this.forwarderNo = forwarderNo;
	}
	public String getForwarderService() {
		return forwarderService;
	}
	public void setForwarderService(String forwarderService) {
		this.forwarderService = forwarderService;
	}
	public double getDocketCharges() {
		return docketCharges;
	}
	public void setDocketCharges(double docketCharges) {
		this.docketCharges = docketCharges;
	}
	public double getLength_dimension() {
		return length_dimension;
	}
	public void setLength_dimension(double length_dimension) {
		this.length_dimension = length_dimension;
	}
	public double getWidth_dimension() {
		return width_dimension;
	}
	public void setWidth_dimension(double width_dimension) {
		this.width_dimension = width_dimension;
	}
	public double getHeight_dimension() {
		return height_dimension;
	}
	public void setHeight_dimension(double height_dimension) {
		this.height_dimension = height_dimension;
	}

	
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}

	public String getConsigNOR_Name() {
		return consigNOR_Name;
	}
	public void setConsigNOR_Name(String consigNOR_Name) {
		this.consigNOR_Name = consigNOR_Name;
	}
	public String getConsigNOR_Address() {
		return consigNOR_Address;
	}
	public void setConsigNOR_Address(String consigNOR_Address) {
		this.consigNOR_Address = consigNOR_Address;
	}
	public String getConsigNOR_ContactPerson() {
		return consigNOR_ContactPerson;
	}
	public void setConsigNOR_ContactPerson(String consigNOR_ContactPerson) {
		this.consigNOR_ContactPerson = consigNOR_ContactPerson;
	}
	/*public int getConsigNOR_Zipcode() {
		return consigNOR_Zipcode;
	}
	public void setConsigNOR_Zipcode(int consigNOR_Zipcode) {
		this.consigNOR_Zipcode = consigNOR_Zipcode;
	}*/
	public String getConsigNOR_City() {
		return consigNOR_City;
	}
	public void setConsigNOR_City(String consigNOR_City) {
		this.consigNOR_City = consigNOR_City;
	}
	public String getConsigNOR_State() {
		return consigNOR_State;
	}
	public void setConsigNOR_State(String consigNOR_State) {
		this.consigNOR_State = consigNOR_State;
	}
	public String getConsigNOR_Country() {
		return consigNOR_Country;
	}
	public void setConsigNOR_Country(String consigNOR_Country) {
		this.consigNOR_Country = consigNOR_Country;
	}
	public String getConsigNOR_Email() {
		return consigNOR_Email;
	}
	public void setConsigNOR_Email(String consigNOR_Email) {
		this.consigNOR_Email = consigNOR_Email;
	}

	public String getConsigNEE_Name() {
		return consigNEE_Name;
	}
	public void setConsigNEE_Name(String consigNEE_Name) {
		this.consigNEE_Name = consigNEE_Name;
	}
	public String getConsigNEE_Address() {
		return consigNEE_Address;
	}
	public void setConsigNEE_Address(String consigNEE_Address) {
		this.consigNEE_Address = consigNEE_Address;
	}
	public String getConsigNEE_ContactPerson() {
		return consigNEE_ContactPerson;
	}
	public void setConsigNEE_ContactPerson(String consigNEE_ContactPerson) {
		this.consigNEE_ContactPerson = consigNEE_ContactPerson;
	}
/*	public int getConsigNEE_Zipcode() {
		return consigNEE_Zipcode;
	}
	public void setConsigNEE_Zipcode(int consigNEE_Zipcode) {
		this.consigNEE_Zipcode = consigNEE_Zipcode;
	}*/
	public String getConsigNEE_City() {
		return consigNEE_City;
	}
	public void setConsigNEE_City(String consigNEE_City) {
		this.consigNEE_City = consigNEE_City;
	}
	public String getConsigNEE_State() {
		return consigNEE_State;
	}
	public void setConsigNEE_State(String consigNEE_State) {
		this.consigNEE_State = consigNEE_State;
	}
	public String getConsigNEE_Country() {
		return consigNEE_Country;
	}
	public void setConsigNEE_Country(String consigNEE_Country) {
		this.consigNEE_Country = consigNEE_Country;
	}
	public String getConsigNEE_Email() {
		return consigNEE_Email;
	}
	public void setConsigNEE_Email(String consigNEE_Email) {
		this.consigNEE_Email = consigNEE_Email;
	}
	public String getConsigNEE_Remarks() {
		return consigNEE_Remarks;
	}
	public void setConsigNEE_Remarks(String consigNEE_Remarks) {
		this.consigNEE_Remarks = consigNEE_Remarks;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}
	public double getVolumeWeight() {
		return volumeWeight;
	}
	public void setVolumeWeight(double volumeWeight) {
		this.volumeWeight = volumeWeight;
	}
	public double getBillWeight() {
		return billWeight;
	}
	public void setBillWeight(double billWeight) {
		this.billWeight = billWeight;
	}
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public double getInvoiceAmount() {
		return InvoiceAmount;
	}
	public void setInvoiceAmount(double invoiceAmount) {
		InvoiceAmount = invoiceAmount;
	}
	public double getInsuranceAmount() {
		return InsuranceAmount;
	}
	public void setInsuranceAmount(double insuranceAmount) {
		InsuranceAmount = insuranceAmount;
	}
	public String getConsignor_Consignee_Type() {
		return consignor_Consignee_Type;
	}
	public void setConsignor_Consignee_Type(String consignor_Consignee_Type) {
		this.consignor_Consignee_Type = consignor_Consignee_Type;
	}
	public String getConsigNOR_Phone() {
		return consigNOR_Phone;
	}
	public void setConsigNOR_Phone(String consigNOR_Phone) {
		this.consigNOR_Phone = consigNOR_Phone;
	}
	public String getConsigNEE_Phone() {
		return consigNEE_Phone;
	}
	public void setConsigNEE_Phone(String consigNEE_Phone) {
		this.consigNEE_Phone = consigNEE_Phone;
	}
	public double getInsuranceRate() {
		return InsuranceRate;
	}
	public void setInsuranceRate(double insuranceRate) {
		InsuranceRate = insuranceRate;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getTravelStatus() {
		return travelStatus;
	}
	public void setTravelStatus(String travelStatus) {
		this.travelStatus = travelStatus;
	}
	public String getConsigNOR_Zipcode() {
		return consigNOR_Zipcode;
	}
	public void setConsigNOR_Zipcode(String consigNOR_Zipcode) {
		this.consigNOR_Zipcode = consigNOR_Zipcode;
	}
	public String getConsigNEE_Zipcode() {
		return consigNEE_Zipcode;
	}
	public void setConsigNEE_Zipcode(String consigNEE_Zipcode) {
		this.consigNEE_Zipcode = consigNEE_Zipcode;
	}
	
	
	public double getExpenseAmountTotal_VAS() {
		return expenseAmountTotal_VAS;
	}
	public void setExpenseAmountTotal_VAS(double expenseAmountTotal_VAS) {
		this.expenseAmountTotal_VAS = expenseAmountTotal_VAS;
	}
	public String getAddOnExpense_VAS() {
		return addOnExpense_VAS;
	}
	public void setAddOnExpense_VAS(String addOnExpense_VAS) {
		this.addOnExpense_VAS = addOnExpense_VAS;
	}
	public String getExpenseAmount_VAS() {
		return expenseAmount_VAS;
	}
	public void setExpenseAmount_VAS(String expenseAmount_VAS) {
		this.expenseAmount_VAS = expenseAmount_VAS;
	}
	public String getVasName() {
		return vasName;
	}
	public void setVasName(String vasName) {
		this.vasName = vasName;
	}
	
	
	public double getBasicAmount() {
		return basicAmount;
	}
	public void setBasicAmount(double basicAmount) {
		this.basicAmount = basicAmount;
	}
	public double getFuelRate() {
		return fuelRate;
	}
	public void setFuelRate(double fuelRate) {
		this.fuelRate = fuelRate;
	}
	public double getFuelAmount() {
		return fuelAmount;
	}
	public void setFuelAmount(double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	public double getVasTotal() {
		return vasTotal;
	}
	public void setVasTotal(double vasTotal) {
		this.vasTotal = vasTotal;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	public String getTax_name1() {
		return tax_name1;
	}
	public void setTax_name1(String tax_name1) {
		this.tax_name1 = tax_name1;
	}
	public String getTax_name2() {
		return tax_name2;
	}
	public void setTax_name2(String tax_name2) {
		this.tax_name2 = tax_name2;
	}
	public String getTax_name3() {
		return tax_name3;
	}
	public void setTax_name3(String tax_name3) {
		this.tax_name3 = tax_name3;
	}
	public double getTax_rate1() {
		return tax_rate1;
	}
	public void setTax_rate1(double tax_rate1) {
		this.tax_rate1 = tax_rate1;
	}
	public double getTax_rate2() {
		return tax_rate2;
	}
	public void setTax_rate2(double tax_rate2) {
		this.tax_rate2 = tax_rate2;
	}
	public double getTax_rate3() {
		return tax_rate3;
	}
	public void setTax_rate3(double tax_rate3) {
		this.tax_rate3 = tax_rate3;
	}
	public double getTax_amount1() {
		return tax_amount1;
	}
	public void setTax_amount1(double tax_amount1) {
		this.tax_amount1 = tax_amount1;
	}
	public double getTax_amount2() {
		return tax_amount2;
	}
	public void setTax_amount2(double tax_amount2) {
		this.tax_amount2 = tax_amount2;
	}
	public double getTax_amount3() {
		return tax_amount3;
	}
	public void setTax_amount3(double tax_amount3) {
		this.tax_amount3 = tax_amount3;
	}
	public double getToPay_VAS() {
		return toPay_VAS;
	}
	public void setToPay_VAS(double toPay_VAS) {
		this.toPay_VAS = toPay_VAS;
	}
	public double getOss_VAS() {
		return oss_VAS;
	}
	public void setOss_VAS(double oss_VAS) {
		this.oss_VAS = oss_VAS;
	}
	public double getCod_amount_vas() {
		return cod_amount_vas;
	}
	public void setCod_amount_vas(double cod_amount_vas) {
		this.cod_amount_vas = cod_amount_vas;
	}
	public int getHfd_floor_number() {
		return hfd_floor_number;
	}
	public void setHfd_floor_number(int hfd_floor_number) {
		this.hfd_floor_number = hfd_floor_number;
	}
	public String getForwarderAccount() {
		return forwarderAccount;
	}
	public void setForwarderAccount(String forwarderAccount) {
		this.forwarderAccount = forwarderAccount;
	}
	public String getMasterAwbNo() {
		return masterAwbNo;
	}
	public void setMasterAwbNo(String masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getDox_nondox() {
		return dox_nondox;
	}
	public void setDox_nondox(String dox_nondox) {
		this.dox_nondox = dox_nondox;
	}
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getOriginPincode() {
		return originPincode;
	}
	public void setOriginPincode(String originPincode) {
		this.originPincode = originPincode;
	}
	public double getGst() {
		return gst;
	}
	public void setGst(double gst) {
		this.gst = gst;
	}
	
	public double getOtherVasTotal() {
		return otherVasTotal;
	}
	public void setOtherVasTotal(double otherVasTotal) {
		this.otherVasTotal = otherVasTotal;
	}
	public double getTaxableValueAmount() {
		return taxableValueAmount;
	}
	public void setTaxableValueAmount(double taxableValueAmount) {
		this.taxableValueAmount = taxableValueAmount;
	}
	public String getSpotRate() {
		return spotRate;
	}
	public void setSpotRate(String spotRate) {
		this.spotRate = spotRate;
	}

}
