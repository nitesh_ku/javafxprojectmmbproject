package com.onesoft.courier.booking.bean;

import java.sql.Date;

public class ManifestControllerBean {

	private String srNo;
	
	private String masterAwbNo;
	
	private String awbNo;
	
	private Date bookingDate;
	
	private String client;
	
	private int pincode;

	private String network;
	
	private String service;
	
	private int pcs;
	
	private Double weight;
	
	private String mps;
	
	private String mps_type;

	public String getSrNo() {
		return srNo;
	}

	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}

	public String getMasterAwbNo() {
		return masterAwbNo;
	}

	public void setMasterAwbNo(String masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}

	public String getAwbNo() {
		return awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}



	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public int getPcs() {
		return pcs;
	}

	public void setPcs(int pcs) {
		this.pcs = pcs;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getMps() {
		return mps;
	}

	public void setMps(String mps) {
		this.mps = mps;
	}

	public String getMps_type() {
		return mps_type;
	}

	public void setMps_type(String mps_type) {
		this.mps_type = mps_type;
	}
	
	
}
