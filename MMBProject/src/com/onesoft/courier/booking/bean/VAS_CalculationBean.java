package com.onesoft.courier.booking.bean;

public class VAS_CalculationBean {
	
	private double hfd_type_pcs_kg_Amt;
	private int hfd_free_flr; 
	private double cod_Min_Amt;
	private double cod_Percentage_Amt;
	private double overSizeShpmnt_Min_Amt;
	private double overSizeShpmnt_Kg_Amt;
	private double insurance_Carrier_Min_Amt;
	private double insurance_Carrier_Percentage_Amt;
	private double insurance_Owner_Min_Amt;
	private double insurance_Owner_Percentage_Amt;
	private double holdAtOffice_Min_Amt;
	private double holdAtOffice_Kg_Amt;
	private double docketCharge_Min_Amt;
	private double fuelSurcharge_Amt;
	private double hdf_Charges_PerPcs_Amt;
	private double hdf_Charges_Min_Amt;
	
	private double deliveryOnInvoice_Amt;
	private double criticalSrvc_Rate_Amt;
	private double criticalSrvc_Additional_Amt;
	private double criticalSrvc_Slab_Amt;
	private double tDD_Rate_Amt;
	private double tDD_Additional_Amt;
	private double tDD_Slab_Amt;
	private double oda_Min_Amt;
	private double oda_Kg_Amt;
	private double opa_Min_Amt;
	private double opa_Kg_Amt;
	private double reversePickup_Min_Amt;
	private double reversePickup_slab_Amt;
	private double advancementFee_Min_Amt;
	private double advancementFee_Percentage_Amt;
	private double greenTax_Amt;
	private double toPay_Amt;
	private double addressCorrectionCharges_Amt;
	public double getHfd_type_pcs_kg_Amt() {
		return hfd_type_pcs_kg_Amt;
	}
	public void setHfd_type_pcs_kg_Amt(double hfd_type_pcs_kg_Amt) {
		this.hfd_type_pcs_kg_Amt = hfd_type_pcs_kg_Amt;
	}
	public int getHfd_free_flr() {
		return hfd_free_flr;
	}
	public void setHfd_free_flr(int hfd_free_flr) {
		this.hfd_free_flr = hfd_free_flr;
	}
	public double getCod_Min_Amt() {
		return cod_Min_Amt;
	}
	public void setCod_Min_Amt(double cod_Min_Amt) {
		this.cod_Min_Amt = cod_Min_Amt;
	}
	public double getCod_Percentage_Amt() {
		return cod_Percentage_Amt;
	}
	public void setCod_Percentage_Amt(double cod_Percentage_Amt) {
		this.cod_Percentage_Amt = cod_Percentage_Amt;
	}
	public double getOverSizeShpmnt_Min_Amt() {
		return overSizeShpmnt_Min_Amt;
	}
	public void setOverSizeShpmnt_Min_Amt(double overSizeShpmnt_Min_Amt) {
		this.overSizeShpmnt_Min_Amt = overSizeShpmnt_Min_Amt;
	}
	public double getOverSizeShpmnt_Kg_Amt() {
		return overSizeShpmnt_Kg_Amt;
	}
	public void setOverSizeShpmnt_Kg_Amt(double overSizeShpmnt_Kg_Amt) {
		this.overSizeShpmnt_Kg_Amt = overSizeShpmnt_Kg_Amt;
	}
	public double getInsurance_Carrier_Min_Amt() {
		return insurance_Carrier_Min_Amt;
	}
	public void setInsurance_Carrier_Min_Amt(double insurance_Carrier_Min_Amt) {
		this.insurance_Carrier_Min_Amt = insurance_Carrier_Min_Amt;
	}
	public double getInsurance_Carrier_Percentage_Amt() {
		return insurance_Carrier_Percentage_Amt;
	}
	public void setInsurance_Carrier_Percentage_Amt(double insurance_Carrier_Percentage_Amt) {
		this.insurance_Carrier_Percentage_Amt = insurance_Carrier_Percentage_Amt;
	}
	public double getInsurance_Owner_Min_Amt() {
		return insurance_Owner_Min_Amt;
	}
	public void setInsurance_Owner_Min_Amt(double insurance_Owner_Min_Amt) {
		this.insurance_Owner_Min_Amt = insurance_Owner_Min_Amt;
	}
	public double getInsurance_Owner_Percentage_Amt() {
		return insurance_Owner_Percentage_Amt;
	}
	public void setInsurance_Owner_Percentage_Amt(double insurance_Owner_Percentage_Amt) {
		this.insurance_Owner_Percentage_Amt = insurance_Owner_Percentage_Amt;
	}
	public double getHoldAtOffice_Min_Amt() {
		return holdAtOffice_Min_Amt;
	}
	public void setHoldAtOffice_Min_Amt(double holdAtOffice_Min_Amt) {
		this.holdAtOffice_Min_Amt = holdAtOffice_Min_Amt;
	}
	public double getHoldAtOffice_Kg_Amt() {
		return holdAtOffice_Kg_Amt;
	}
	public void setHoldAtOffice_Kg_Amt(double holdAtOffice_Kg_Amt) {
		this.holdAtOffice_Kg_Amt = holdAtOffice_Kg_Amt;
	}
	public double getDocketCharge_Min_Amt() {
		return docketCharge_Min_Amt;
	}
	public void setDocketCharge_Min_Amt(double docketCharge_Min_Amt) {
		this.docketCharge_Min_Amt = docketCharge_Min_Amt;
	}
	public double getFuelSurcharge_Amt() {
		return fuelSurcharge_Amt;
	}
	public void setFuelSurcharge_Amt(double fuelSurcharge_Amt) {
		this.fuelSurcharge_Amt = fuelSurcharge_Amt;
	}
	public double getHdf_Charges_PerPcs_Amt() {
		return hdf_Charges_PerPcs_Amt;
	}
	public void setHdf_Charges_PerPcs_Amt(double hdf_Charges_PerPcs_Amt) {
		this.hdf_Charges_PerPcs_Amt = hdf_Charges_PerPcs_Amt;
	}
	public double getHdf_Charges_Min_Amt() {
		return hdf_Charges_Min_Amt;
	}
	public void setHdf_Charges_Min_Amt(double hdf_Charges_Min_Amt) {
		this.hdf_Charges_Min_Amt = hdf_Charges_Min_Amt;
	}
	public double getDeliveryOnInvoice_Amt() {
		return deliveryOnInvoice_Amt;
	}
	public void setDeliveryOnInvoice_Amt(double deliveryOnInvoice_Amt) {
		this.deliveryOnInvoice_Amt = deliveryOnInvoice_Amt;
	}
	public double getCriticalSrvc_Rate_Amt() {
		return criticalSrvc_Rate_Amt;
	}
	public void setCriticalSrvc_Rate_Amt(double criticalSrvc_Rate_Amt) {
		this.criticalSrvc_Rate_Amt = criticalSrvc_Rate_Amt;
	}
	public double getCriticalSrvc_Additional_Amt() {
		return criticalSrvc_Additional_Amt;
	}
	public void setCriticalSrvc_Additional_Amt(double criticalSrvc_Additional_Amt) {
		this.criticalSrvc_Additional_Amt = criticalSrvc_Additional_Amt;
	}
	public double getCriticalSrvc_Slab_Amt() {
		return criticalSrvc_Slab_Amt;
	}
	public void setCriticalSrvc_Slab_Amt(double criticalSrvc_Slab_Amt) {
		this.criticalSrvc_Slab_Amt = criticalSrvc_Slab_Amt;
	}
	public double gettDD_Rate_Amt() {
		return tDD_Rate_Amt;
	}
	public void settDD_Rate_Amt(double tDD_Rate_Amt) {
		this.tDD_Rate_Amt = tDD_Rate_Amt;
	}
	public double gettDD_Additional_Amt() {
		return tDD_Additional_Amt;
	}
	public void settDD_Additional_Amt(double tDD_Additional_Amt) {
		this.tDD_Additional_Amt = tDD_Additional_Amt;
	}
	public double gettDD_Slab_Amt() {
		return tDD_Slab_Amt;
	}
	public void settDD_Slab_Amt(double tDD_Slab_Amt) {
		this.tDD_Slab_Amt = tDD_Slab_Amt;
	}
	public double getOda_Min_Amt() {
		return oda_Min_Amt;
	}
	public void setOda_Min_Amt(double oda_Min_Amt) {
		this.oda_Min_Amt = oda_Min_Amt;
	}
	public double getOda_Kg_Amt() {
		return oda_Kg_Amt;
	}
	public void setOda_Kg_Amt(double oda_Kg_Amt) {
		this.oda_Kg_Amt = oda_Kg_Amt;
	}
	public double getOpa_Min_Amt() {
		return opa_Min_Amt;
	}
	public void setOpa_Min_Amt(double opa_Min_Amt) {
		this.opa_Min_Amt = opa_Min_Amt;
	}
	public double getOpa_Kg_Amt() {
		return opa_Kg_Amt;
	}
	public void setOpa_Kg_Amt(double opa_Kg_Amt) {
		this.opa_Kg_Amt = opa_Kg_Amt;
	}
	public double getReversePickup_Min_Amt() {
		return reversePickup_Min_Amt;
	}
	public void setReversePickup_Min_Amt(double reversePickup_Min_Amt) {
		this.reversePickup_Min_Amt = reversePickup_Min_Amt;
	}
	public double getReversePickup_slab_Amt() {
		return reversePickup_slab_Amt;
	}
	public void setReversePickup_slab_Amt(double reversePickup_slab_Amt) {
		this.reversePickup_slab_Amt = reversePickup_slab_Amt;
	}
	public double getAdvancementFee_Min_Amt() {
		return advancementFee_Min_Amt;
	}
	public void setAdvancementFee_Min_Amt(double advancementFee_Min_Amt) {
		this.advancementFee_Min_Amt = advancementFee_Min_Amt;
	}
	public double getAdvancementFee_Percentage_Amt() {
		return advancementFee_Percentage_Amt;
	}
	public void setAdvancementFee_Percentage_Amt(double advancementFee_Percentage_Amt) {
		this.advancementFee_Percentage_Amt = advancementFee_Percentage_Amt;
	}
	public double getGreenTax_Amt() {
		return greenTax_Amt;
	}
	public void setGreenTax_Amt(double greenTax_Amt) {
		this.greenTax_Amt = greenTax_Amt;
	}
	public double getToPay_Amt() {
		return toPay_Amt;
	}
	public void setToPay_Amt(double toPay_Amt) {
		this.toPay_Amt = toPay_Amt;
	}
	public double getAddressCorrectionCharges_Amt() {
		return addressCorrectionCharges_Amt;
	}
	public void setAddressCorrectionCharges_Amt(double addressCorrectionCharges_Amt) {
		this.addressCorrectionCharges_Amt = addressCorrectionCharges_Amt;
	}

	
	
	
}
