package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AutoCalculationTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty clientCode;
	private SimpleStringProperty networkCode;
	private SimpleStringProperty serviceCode;
	private SimpleStringProperty masterAwbNo;
	private SimpleStringProperty branchCode;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty bookingDate;
	private SimpleStringProperty originPincode;
	private SimpleStringProperty destinationPincode;
	private SimpleStringProperty forwarderAccount;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty basicAmount;
	private SimpleDoubleProperty fuelAmount;
	private SimpleDoubleProperty vasTotal;
	private SimpleDoubleProperty total;
	private SimpleDoubleProperty docketCharges;
	private SimpleDoubleProperty billWeight;
	private SimpleDoubleProperty InsuranceAmount;
	private SimpleDoubleProperty gstAmount;
	
	
	public AutoCalculationTableBean(int slno,String masterAwbNo,String bookingDate,String forwardAccount,
			String clientcode,String branchCode,String networkCode, String serviceCode,int pcs,
			double billWeight,double basicAmt,double fuelAmt,double insuranceAmt,
			double vasTotal,double gstAmt,double docketCharte)
	{
		super();
		
		this.slno=new SimpleIntegerProperty(slno);
		this.masterAwbNo=new SimpleStringProperty(masterAwbNo);
		this.bookingDate=new SimpleStringProperty(bookingDate);
		this.forwarderAccount=new SimpleStringProperty(forwardAccount);
		this.clientCode=new SimpleStringProperty(clientcode);
		this.branchCode=new SimpleStringProperty(branchCode);
		this.networkCode=new SimpleStringProperty(networkCode);
		this.serviceCode=new SimpleStringProperty(serviceCode);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.billWeight=new SimpleDoubleProperty(billWeight);
		this.basicAmount=new SimpleDoubleProperty(basicAmt);
		this.fuelAmount=new SimpleDoubleProperty(fuelAmt);
		this.InsuranceAmount=new SimpleDoubleProperty(insuranceAmt);
		this.vasTotal=new SimpleDoubleProperty(vasTotal);
		this.gstAmount=new SimpleDoubleProperty(gstAmt);
		this.docketCharges=new SimpleDoubleProperty(docketCharte);
		
		
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getClientCode() {
		return clientCode.get();
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public String getNetworkCode() {
		return networkCode.get();
	}
	public void setNetworkCode(SimpleStringProperty networkCode) {
		this.networkCode = networkCode;
	}
	public String getServiceCode() {
		return serviceCode.get();
	}
	public void setServiceCode(SimpleStringProperty serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getMasterAwbNo() {
		return masterAwbNo.get();
	}
	public void setMasterAwbNo(SimpleStringProperty masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getBranchCode() {
		return branchCode.get();
	}
	public void setBranchCode(SimpleStringProperty branchCode) {
		this.branchCode = branchCode;
	}
	public String getAwbNo() {
		return awbNo.get();
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public String getBookingDate() {
		return bookingDate.get();
	}
	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getOriginPincode() {
		return originPincode.get();
	}
	public void setOriginPincode(SimpleStringProperty originPincode) {
		this.originPincode = originPincode;
	}
	public String getDestinationPincode() {
		return destinationPincode.get();
	}
	public void setDestinationPincode(SimpleStringProperty destinationPincode) {
		this.destinationPincode = destinationPincode;
	}
	public String getForwarderAccount() {
		return forwarderAccount.get();
	}
	public void setForwarderAccount(SimpleStringProperty forwarderAccount) {
		this.forwarderAccount = forwarderAccount;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getBasicAmount() {
		return basicAmount.get();
	}
	public void setBasicAmount(SimpleDoubleProperty basicAmount) {
		this.basicAmount = basicAmount;
	}
	public double getFuelAmount() {
		return fuelAmount.get();
	}
	public void setFuelAmount(SimpleDoubleProperty fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	public double getVasTotal() {
		return vasTotal.get();
	}
	public void setVasTotal(SimpleDoubleProperty vasTotal) {
		this.vasTotal = vasTotal;
	}
	public double getTotal() {
		return total.get();
	}
	public void setTotal(SimpleDoubleProperty total) {
		this.total = total;
	}
	public double getDocketCharges() {
		return docketCharges.get();
	}
	public void setDocketCharges(SimpleDoubleProperty docketCharges) {
		this.docketCharges = docketCharges;
	}
	public double getBillWeight() {
		return billWeight.get();
	}
	public void setBillWeight(SimpleDoubleProperty billWeight) {
		this.billWeight = billWeight;
	}
	public double getInsuranceAmount() {
		return InsuranceAmount.get();
	}
	public void setInsuranceAmount(SimpleDoubleProperty insuranceAmount) {
		InsuranceAmount = insuranceAmount;
	}
	public double getGstAmount() {
		return gstAmount.get();
	}
	public void setGstAmount(SimpleDoubleProperty gstAmount) {
		this.gstAmount = gstAmount;
	}
	
}