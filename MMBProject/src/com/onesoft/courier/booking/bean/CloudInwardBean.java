package com.onesoft.courier.booking.bean;

public class CloudInwardBean {
	
	private int slno;
	private String clientName;
	private String clientCode;
	private String branchCode;
	private int zipcode;
	private String destination;
	private int pcs;
	private double weight;
	private String awbNo;
	private String subAwbNo;
	
	private String subAwb_Prefix;
	private long subAwb_suffix;
	private String subAwbnoStatus;
	private String loggedInUser;
	private String unSavedSubAwbNo;
	private String bookingDate;
	private String receiveDate;
	private String origin;
	
	private String mps;
	private String mps_type;
	private String masterAwbNo;
	private String inwardFrom;
	
	private String service_Mode;
	
	
	
	
	
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public String getSubAwbNo() {
		return subAwbNo;
	}
	public void setSubAwbNo(String subAwbNo) {
		this.subAwbNo = subAwbNo;
	}
	public String getSubAwb_Prefix() {
		return subAwb_Prefix;
	}
	public void setSubAwb_Prefix(String subAwb_Prefix) {
		this.subAwb_Prefix = subAwb_Prefix;
	}
	public long getSubAwb_suffix() {
		return subAwb_suffix;
	}
	public void setSubAwb_suffix(long subAwb_suffix) {
		this.subAwb_suffix = subAwb_suffix;
	}
	public String getSubAwbnoStatus() {
		return subAwbnoStatus;
	}
	public void setSubAwbnoStatus(String subAwbnoStatus) {
		this.subAwbnoStatus = subAwbnoStatus;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getUnSavedSubAwbNo() {
		return unSavedSubAwbNo;
	}
	public void setUnSavedSubAwbNo(String unSavedSubAwbNo) {
		this.unSavedSubAwbNo = unSavedSubAwbNo;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getMps() {
		return mps;
	}
	public void setMps(String mps) {
		this.mps = mps;
	}
	public String getMps_type() {
		return mps_type;
	}
	public void setMps_type(String mps_type) {
		this.mps_type = mps_type;
	}
	public String getMasterAwbNo() {
		return masterAwbNo;
	}
	public void setMasterAwbNo(String masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getInwardFrom() {
		return inwardFrom;
	}
	public void setInwardFrom(String inwardFrom) {
		this.inwardFrom = inwardFrom;
	}
	public String getService_Mode() {
		return service_Mode;
	}
	public void setService_Mode(String service_Mode) {
		this.service_Mode = service_Mode;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	

}
