package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SavedBookingDataTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty awbno;
	private SimpleStringProperty forwarderNo;
	private SimpleStringProperty bookingDate;
	private SimpleStringProperty serviceCode;
	private SimpleStringProperty networkCode;
	private SimpleStringProperty forwarderAccount;
	
	
	public SavedBookingDataTableBean(int slno, String awbno,String forwarderNo,String bookingDate,String service,String network,String account)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbno=new SimpleStringProperty(awbno);
		this.forwarderNo=new SimpleStringProperty(forwarderNo);
		this.bookingDate=new SimpleStringProperty(bookingDate);
		this.serviceCode=new SimpleStringProperty(service);
		this.networkCode=new SimpleStringProperty(network);
		this.forwarderAccount=new SimpleStringProperty(account);
		
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getAwbno() {
		return awbno.get();
	}
	public void setAwbno(SimpleStringProperty awbno) {
		this.awbno = awbno;
	}
	public String getForwarderNo() {
		return forwarderNo.get();
	}
	public void setForwarderNo(SimpleStringProperty forwarderNo) {
		this.forwarderNo = forwarderNo;
	}
	public String getBookingDate() {
		return bookingDate.get();
	}
	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getServiceCode() {
		return serviceCode.get();
	}
	public void setServiceCode(SimpleStringProperty serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getNetworkCode() {
		return networkCode.get();
	}
	public void setNetworkCode(SimpleStringProperty networkCode) {
		this.networkCode = networkCode;
	}
	public String getForwarderAccount() {
		return forwarderAccount.get();
	}
	public void setForwarderAccount(SimpleStringProperty forwarderAccount) {
		this.forwarderAccount = forwarderAccount;
	}
	
	
	
}
