package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class UpdateBookingDataTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty masterAwbNo;
	private SimpleStringProperty bookingDate;
	private SimpleStringProperty mode_Service;
	private SimpleDoubleProperty actualWt;
	private SimpleDoubleProperty chargableBillingWt;
	private SimpleStringProperty destinationPincode;
	private SimpleIntegerProperty pcs;
	private SimpleStringProperty refNo;
	private SimpleStringProperty Issue_Sheet_IncorrectService;
	
	
	
	public UpdateBookingDataTableBean(int slno,String masterAwb,String bookingDate,String modeService,double actWt,double billWt,
			String pincode,int pcs,String refNo)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.masterAwbNo=new SimpleStringProperty(masterAwb);
		this.bookingDate=new SimpleStringProperty(bookingDate);
		this.mode_Service=new SimpleStringProperty(modeService);
		this.actualWt=new SimpleDoubleProperty(actWt);
		this.chargableBillingWt=new SimpleDoubleProperty(billWt);
		this.destinationPincode=new SimpleStringProperty(pincode);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.refNo=new SimpleStringProperty(refNo);
	}
	
	
	public UpdateBookingDataTableBean(int slno,String masterAwb,String issue_Sheet_Service)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.masterAwbNo=new SimpleStringProperty(masterAwb);
		this.setIssue_Sheet_IncorrectService(new SimpleStringProperty(issue_Sheet_Service));
		
	}

	
	public String getMasterAwbNo() {
		return masterAwbNo.get();
	}
	public void setMasterAwbNo(SimpleStringProperty masterAwbNo) {
		this.masterAwbNo = masterAwbNo;
	}
	public String getBookingDate() {
		return bookingDate.get();
	}
	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getMode_Service() {
		return mode_Service.get();
	}
	public void setMode_Service(SimpleStringProperty mode_Service) {
		this.mode_Service = mode_Service;
	}
	public double getActualWt() {
		return actualWt.get();
	}
	public void setActualWt(SimpleDoubleProperty actualWt) {
		this.actualWt = actualWt;
	}
	public double getChargableBillingWt() {
		return chargableBillingWt.get();
	}
	public void setChargableBillingWt(SimpleDoubleProperty chargableBillingWt) {
		this.chargableBillingWt = chargableBillingWt;
	}
	public String getDestinationPincode() {
		return destinationPincode.get();
	}
	public void setDestinationPincode(SimpleStringProperty destinationPincode) {
		this.destinationPincode = destinationPincode;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public String getRefNo() {
		return refNo.get();
	}
	public void setRefNo(SimpleStringProperty refNo) {
		this.refNo = refNo;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getIssue_Sheet_IncorrectService() {
		return Issue_Sheet_IncorrectService.get();
	}

	public void setIssue_Sheet_IncorrectService(SimpleStringProperty issue_Sheet_IncorrectService) {
		Issue_Sheet_IncorrectService = issue_Sheet_IncorrectService;
	}
	
}
