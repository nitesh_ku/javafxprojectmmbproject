package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class UnSavedBookingDataTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty awbno;

	public UnSavedBookingDataTableBean(int slno, String awbno)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbno=new SimpleStringProperty(awbno);
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getAwbno() {
		return awbno.get();
	}
	public void setAwbno(SimpleStringProperty awbno) {
		this.awbno = awbno;
	}
	
}
