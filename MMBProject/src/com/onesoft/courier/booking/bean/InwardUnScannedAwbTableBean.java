package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class InwardUnScannedAwbTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty masterAwb;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty client;
	private SimpleIntegerProperty zipcode;
	private SimpleStringProperty destination;
	private SimpleIntegerProperty totalPcs;
	private SimpleDoubleProperty totalWeight;
	private SimpleStringProperty mode_service;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty actualWeight;
	private SimpleDoubleProperty dimensionLenght;
	private SimpleDoubleProperty dimensionWidth;
	private SimpleDoubleProperty dimensionHeight;
	private SimpleStringProperty dimension;
	private SimpleStringProperty subAwbNo;


	
	
	public InwardUnScannedAwbTableBean(int slno,String masterAwb,String awb,String client,int zipcode,String destination, 
						int totalPcs, double totalWeight)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.masterAwb=new SimpleStringProperty(masterAwb);
		this.awbNo=new SimpleStringProperty(awb);
		this.client=new SimpleStringProperty(client);
		this.zipcode=new SimpleIntegerProperty(zipcode);
		this.destination=new SimpleStringProperty(destination);
		this.totalPcs=new SimpleIntegerProperty(totalPcs);
		this.totalWeight=new SimpleDoubleProperty(totalWeight);
		
	}
	
	
	public String getAwbNo() {
		return awbNo.get();
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public String getClient() {
		return client.get();
	}
	public void setClient(SimpleStringProperty client) {
		this.client = client;
	}
	public int getZipcode() {
		return zipcode.get();
	}
	public void setZipcode(SimpleIntegerProperty zipcode) {
		this.zipcode = zipcode;
	}
	public String getDestination() {
		return destination.get();
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public int getTotalPcs() {
		return totalPcs.get();
	}
	public void setTotalPcs(SimpleIntegerProperty totalPcs) {
		this.totalPcs = totalPcs;
	}
	public double getTotalWeight() {
		return totalWeight.get();
	}
	public void setTotalWeight(SimpleDoubleProperty totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getMode_service() {
		return mode_service.get();
	}
	public void setMode_service(SimpleStringProperty mode_service) {
		this.mode_service = mode_service;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getActualWeight() {
		return actualWeight.get();
	}
	public void setActualWeight(SimpleDoubleProperty actualWeight) {
		this.actualWeight = actualWeight;
	}
	public double getDimensionLenght() {
		return dimensionLenght.get();
	}
	public void setDimensionLenght(SimpleDoubleProperty dimensionLenght) {
		this.dimensionLenght = dimensionLenght;
	}
	public double getDimensionWidth() {
		return dimensionWidth.get();
	}
	public void setDimensionWidth(SimpleDoubleProperty dimensionWidth) {
		this.dimensionWidth = dimensionWidth;
	}
	public double getDimensionHeight() {
		return dimensionHeight.get();
	}
	public void setDimensionHeight(SimpleDoubleProperty dimensionHeight) {
		this.dimensionHeight = dimensionHeight;
	}
	public String getSubAwbNo() {
		return subAwbNo.get();
	}
	public void setSubAwbNo(SimpleStringProperty subAwbNo) {
		this.subAwbNo = subAwbNo;
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getMasterAwb() {
		return masterAwb.get();
	}
	public void setMasterAwb(SimpleStringProperty masterAwb) {
		this.masterAwb = masterAwb;
	}
	public String getDimension() {
		return dimension.get();
	}
	public void setDimension(SimpleStringProperty dimension) {
		this.dimension = dimension;
	}
	
}
