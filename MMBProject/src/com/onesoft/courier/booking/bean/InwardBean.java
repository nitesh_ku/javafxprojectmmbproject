package com.onesoft.courier.booking.bean;

public class InwardBean {
	
	private int slno;
	private String masterAwb;
	private String awbNo;
	private String client;
	private int zipcode;
	private String destination;
	private int totalPcs;
	private double totalWeight;
	private String mode_service;
	private int pcs;
	private double actualWeight;
	private double volWeight;
	private double billingWeight;
	private double dimensionLenght;
	private double dimensionWidth;
	private double dimensionHeight;
	private String subAwbNo;
	private String mps;
	private String mpstType;
	private String dimension;
	
	private String zoneCode;
	private String travelStatus;
	
	private String weight_prefix;
	private String weightValue;
	private double weightCalculateFromDimensions;
	private double divisionAmount;

	
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getTotalPcs() {
		return totalPcs;
	}
	public void setTotalPcs(int totalPcs) {
		this.totalPcs = totalPcs;
	}
	public double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getMode_service() {
		return mode_service;
	}
	public void setMode_service(String mode_service) {
		this.mode_service = mode_service;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}
	public double getDimensionLenght() {
		return dimensionLenght;
	}
	public void setDimensionLenght(double dimensionLenght) {
		this.dimensionLenght = dimensionLenght;
	}
	public double getDimensionWidth() {
		return dimensionWidth;
	}
	public void setDimensionWidth(double dimensionWidth) {
		this.dimensionWidth = dimensionWidth;
	}
	public double getDimensionHeight() {
		return dimensionHeight;
	}
	public void setDimensionHeight(double dimensionHeight) {
		this.dimensionHeight = dimensionHeight;
	}
	public String getSubAwbNo() {
		return subAwbNo;
	}
	public void setSubAwbNo(String subAwbNo) {
		this.subAwbNo = subAwbNo;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	public String getMasterAwb() {
		return masterAwb;
	}
	public void setMasterAwb(String masterAwb) {
		this.masterAwb = masterAwb;
	}
	public String getMps() {
		return mps;
	}
	public void setMps(String mps) {
		this.mps = mps;
	}
	public String getMpstType() {
		return mpstType;
	}
	public void setMpstType(String mpstType) {
		this.mpstType = mpstType;
	}
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getWeightValue() {
		return weightValue;
	}
	public void setWeightValue(String weightValue) {
		this.weightValue = weightValue;
	}
	public String getWeight_prefix() {
		return weight_prefix;
	}
	public void setWeight_prefix(String weight_prefix) {
		this.weight_prefix = weight_prefix;
	}
	public String getTravelStatus() {
		return travelStatus;
	}
	public void setTravelStatus(String travelStatus) {
		this.travelStatus = travelStatus;
	}
	public double getWeightCalculateFromDimensions() {
		return weightCalculateFromDimensions;
	}
	public void setWeightCalculateFromDimensions(double weightCalculateFromDimensions) {
		this.weightCalculateFromDimensions = weightCalculateFromDimensions;
	}
	public double getDivisionAmount() {
		return divisionAmount;
	}
	public void setDivisionAmount(double divisionAmount) {
		this.divisionAmount = divisionAmount;
	}
	public double getBillingWeight() {
		return billingWeight;
	}
	public void setBillingWeight(double billingWeight) {
		this.billingWeight = billingWeight;
	}
	public double getVolWeight() {
		return volWeight;
	}
	public void setVolWeight(double volWeight) {
		this.volWeight = volWeight;
	}
	public String getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	
}
