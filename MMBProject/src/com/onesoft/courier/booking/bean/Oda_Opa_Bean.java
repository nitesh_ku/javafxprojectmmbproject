package com.onesoft.courier.booking.bean;

public class Oda_Opa_Bean {
	
	private String pincode_Origin_Destination;
	private String type_ODA_OPA;
	private String type_org_desti;
	private String client;
	private String service;
	

	public String getType_ODA_OPA() {
		return type_ODA_OPA;
	}
	public void setType_ODA_OPA(String type_ODA_OPA) {
		this.type_ODA_OPA = type_ODA_OPA;
	}
	public String getPincode_Origin_Destination() {
		return pincode_Origin_Destination;
	}
	public void setPincode_Origin_Destination(String pincode_Origin_Destination) {
		this.pincode_Origin_Destination = pincode_Origin_Destination;
	}
	public String getType_org_desti() {
		return type_org_desti;
	}
	public void setType_org_desti(String type_org_desti) {
		this.type_org_desti = type_org_desti;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	

}
