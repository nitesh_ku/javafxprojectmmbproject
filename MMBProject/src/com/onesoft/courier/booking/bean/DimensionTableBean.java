package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class DimensionTableBean {

	public SimpleIntegerProperty db_serialNo;
	
	public SimpleIntegerProperty pcs;
	private SimpleDoubleProperty length;
	private SimpleDoubleProperty height;
	private SimpleDoubleProperty width;
	private SimpleStringProperty awbno;
	private SimpleStringProperty sub_Awbno;
	private SimpleDoubleProperty weight;
	
	
	
	public DimensionTableBean(int db_slno,String awbno,String sub_awbno, int pcs,double lenght,double height, double width,double weight)
	{
		this.awbno=new SimpleStringProperty(awbno);
		this.sub_Awbno=new SimpleStringProperty(sub_awbno);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.length=new SimpleDoubleProperty(lenght);
		this.height=new SimpleDoubleProperty(height);
		this.width=new SimpleDoubleProperty(width);
		this.weight=new SimpleDoubleProperty(weight);
		this.db_serialNo=new SimpleIntegerProperty(db_slno);
				
	}
	
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public void setLength(SimpleDoubleProperty length) {
		this.length = length;
	}
	public void setHeight(SimpleDoubleProperty height) {
		this.height = height;
	}
	public void setWidth(SimpleDoubleProperty width) {
		this.width = width;
	}
	public void setAwbno(SimpleStringProperty awbno) {
		this.awbno = awbno;
	}
	public void setWeight(SimpleDoubleProperty weight) {
		this.weight = weight;
	}
	public int getPcs() {
		return pcs.get();
	}
	public double getLength() {
		return length.get();
	}
	public double getHeight() {
		return height.get();
	}
	public double getWidth() {
		return width.get();
	}
	public String getAwbno() {
		return awbno.get();
	}
	public double getWeight() {
		return weight.get();
	}
	
	
	public int getDb_serialNo() {
		return db_serialNo.get();
	}
	public void setDb_serialNo(SimpleIntegerProperty db_serialNo) {
		this.db_serialNo = db_serialNo;
	}

	public String getSub_Awbno() {
		return sub_Awbno.get();
	}

	public void setSub_Awbno(SimpleStringProperty sub_Awbno) {
		this.sub_Awbno = sub_Awbno;
	}

	
	
	
}


