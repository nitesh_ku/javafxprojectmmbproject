package com.onesoft.courier.booking.bean;

public class DimensionBean {
	
	
	private int db_serialNo;
	private int pcs;
	private double length;
	private double height;
	private double width;
	private String awbno;
	private String sub_AwbNo;
	private double weight;
	
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public String getAwbno() {
		return awbno;
	}
	public void setAwbno(String awbno) {
		this.awbno = awbno;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getDb_serialNo() {
		return db_serialNo;
	}
	public void setDb_serialNo(int db_serialNo) {
		this.db_serialNo = db_serialNo;
	}
	public String getSub_AwbNo() {
		return sub_AwbNo;
	}
	public void setSub_AwbNo(String sub_AwbNo) {
		this.sub_AwbNo = sub_AwbNo;
	}
	

}
