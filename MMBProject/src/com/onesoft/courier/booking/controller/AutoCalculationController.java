package com.onesoft.courier.booking.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.AutoCalculationBean;
import com.onesoft.courier.booking.bean.AutoCalculationTableBean;
import com.onesoft.courier.booking.bean.DataEntryBean;
import com.onesoft.courier.booking.bean.DimensionBean;
import com.onesoft.courier.booking.bean.Oda_Opa_Bean;
import com.onesoft.courier.booking.bean.VAS_CalculationBean;
import com.onesoft.courier.calculation.NewCalculateWeight;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadGST_Rates;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.invoice.utils.InvoiceUtils;
import com.onesoft.courier.master.pincode.bean.UploadPincodeBean;
import com.onesoft.courier.master.vas.bean.ClientVASBean;
import com.onesoft.courier.ratemaster.bean.ClientRateZoneBean;
import com.onesoft.courier.sql.queries.MasterSQL_AutoCalculation_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_DataEntry_Utils;
import com.sun.javafx.binding.SelectBinding.AsBoolean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class AutoCalculationController implements Initializable
{
	private ObservableList<AutoCalculationTableBean> tabledata_ChangeEntry=FXCollections.observableArrayList();
	
	Task<Void> task;
	private ObservableList<String> comboBoxMonthItems=FXCollections.observableArrayList(MasterSQL_AutoCalculation_Utils.LIST_MONTHS);
	private ObservableList<String> comboBoxYearItems=FXCollections.observableArrayList(MasterSQL_AutoCalculation_Utils.LIST_YEAR);
	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	
	private List<ClientVASBean> list_ClientVAS=new ArrayList<>();
	private List<AutoCalculationBean> list_ShowDetailsData=new ArrayList<>();
	private List<ClientRateZoneBean> list_ClientsRate=new ArrayList<>();
	private List<String> list_clients=new ArrayList<>();
	private Set<String> set_NetworkItems=new HashSet<>();
	private Set<String> set_ServiceItems=new HashSet<>();
	private List<DataEntryBean> list_AddedVAS_on_Awbno=new ArrayList<>();
	public static List<Oda_Opa_Bean> List_ODA_OPA_FromPincode=new ArrayList<>();
	public Set<String> set_ClientFromFilteredList=new HashSet<>();
	public List<String> list_AwbFromFilteredList=new ArrayList<>();
	
	
	
	public List<DimensionBean> list_getDimensionsBeforeReCalculation=new ArrayList<>();
	
	
	private List<AutoCalculationBean> list_ChangedData=new ArrayList<>();
	private List<AutoCalculationBean> list_UnChangedData=new ArrayList<>();
	
	private List<UploadPincodeBean> list_PincodeWithZone_air_surface=new ArrayList<>();
	
	double totalVAS_amount=0;
	double opa_amount=0;
	double oda_amount=0;
	double docketCharge=0;
	double oss_min_amt=0;
	double oss_per_kg_Amt=0;
	double oss_VAS_Calculated_Amt=0;
	double oss_kg_limit=0;
	double oss_cm_limit=0;
	
	boolean fovStatus=false;
	boolean fuel_Excluding_FOV_status=false;
	
	double fuel_rate_global=0;
	
	boolean isDataFound_ForShowDetails=true;
	
	public double cgst_amount=0;
	public double sgst_amount=0;
	public double igst_amount=0;
	public double cgst_rate=0;
	public double sgst_rate=0;
	public double igst_rate=0;
	public String igst_name=null;
	public String cgst_name=null;
	public String sgst_name=null;
	
	
	//private List<String> list_Clients=new ArrayList<>();
	private Set<String> set_services=new HashSet<>();
	private List<LoadServiceWithNetworkBean> list_serviceWithNetwork=new ArrayList<>();
	//private List<String> list_Zone=new ArrayList<>();
	//private List<String> list_City=new ArrayList<>();
	
	private ClientRateZoneBean globalRateBean=new ClientRateZoneBean();
	
	public double oss_kg_limit_global=0;
	public double oss_cm_limit_global=0;
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	boolean isReverse_Additional=false;
	boolean isReverse_KG=false;
	boolean isDataAvailable=false;
	
	@FXML
	private RadioButton rdBtnDays;
	
	@FXML
	private RadioButton rdBtnMonth;
	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private TextField txtClient;
	
	@FXML
	private ComboBox<String> comboBoxMonth;
	
	@FXML
	private ComboBox<String> comboBoxYear;
	
	/*@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxService;*/
	
	@FXML
	private Button btnShowDetails;
	
	@FXML
	private Button btnAutoCalculate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Label lab_Before_BasicAmt;
	
	@FXML
	private Label lab_Before_VasTotalAmt;
	
	@FXML
	private Label lab_Before_Fuel;
	
	@FXML
	private Label lab_Before_CGST;
	
	@FXML
	private Label lab_Before_SGST;
	
	@FXML
	private Label lab_Before_IGST;
	
	@FXML
	private Label lab_Before_Total;
	
	@FXML
	private Label lab_After_BasicAmt;
	
	@FXML
	private Label lab_After_VasTotalAmt;
	
	@FXML
	private Label lab_After_Fuel;
	
	@FXML
	private Label lab_After_CGST;
	
	@FXML
	private Label lab_After_SGST;
	
	@FXML
	private Label lab_After_IGST;
	
	@FXML
	private Label lab_After_Total;
	
	
	@FXML
	private TableView<AutoCalculationTableBean> tableChngEntries;

	@FXML
	private TableColumn<AutoCalculationTableBean, Integer> tabCol_chngEntry_slno;

	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_Mstr_Awb_no;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_BookingDate;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_ForwardAccount;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_ClientCode;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_BranchCode;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_NetworkCode;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, String> tabCol_chngEntry_ServiceCode;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Integer> tabCol_chngEntry_PCS;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_BillingWeight;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_BasicAmt;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_FuelAmt;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_InsuranceAmt;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_VAS_Total;

	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_DocketCharge;
	
	@FXML
	private TableColumn<AutoCalculationTableBean, Double> tabCol_chngEntry_GSTAmt;

	
// ***************************************************************************************	
	
	public void setToDateViaFromDate()
	{
		LocalDate local_toDate=dpkFromDate.getValue().plusDays(60);
		LocalDate local_CurrentDate=LocalDate.now();
		
		Date toDate=Date.valueOf(local_toDate);
		Date currentDate=Date.valueOf(local_CurrentDate);
		
		if(toDate.after(currentDate))
		{
			dpkToDate.setValue(local_CurrentDate);
		}
		else if(toDate.before(currentDate))
		{
			dpkToDate.setValue(dpkFromDate.getValue().plusDays(60));
		}
		else 
		{
			dpkToDate.setValue(dpkFromDate.getValue().plusDays(60));
		}
	}
	

// ***************************************************************************************	
	
	public void setToDateViaToDate()
	{
		LocalDate local_FromDate=dpkFromDate.getValue().plusDays(60);
		LocalDate local_toDate=dpkToDate.getValue();
		Date fromDate=Date.valueOf(local_FromDate);
		Date toDate=Date.valueOf(local_toDate);
		
		if(toDate.after(fromDate))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Date Range Alert");
			alert.setHeaderText("Please select correct with in 60 days");
			alert.showAndWait();
			dpkToDate.setValue(local_FromDate);
		}
		
	}

// ***************************************************************************************
	
	public void selectDaysOrMonth()
	{
		if(rdBtnMonth.isSelected()==true)
		{
			comboBoxMonth.setDisable(false);
			comboBoxYear.setDisable(false);
			dpkFromDate.setDisable(true);
			dpkToDate.setDisable(true);
			dpkFromDate.getEditor().clear();
			dpkToDate.getEditor().clear();
		}
		else
		{
			comboBoxMonth.setDisable(true);
			comboBoxYear.setDisable(true);
			dpkFromDate.setDisable(false);
			dpkToDate.setDisable(false);
			comboBoxMonth.getSelectionModel().clearSelection();
			comboBoxYear.getSelectionModel().clearSelection();
		}
	}
	
	
// =============================================================================================

	public void loadClients() throws SQLException 
	{
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			list_clients.add(bean.getClientName() + " | " + bean.getClientCode());

		}
		TextFields.bindAutoCompletion(txtClient, list_clients);
	}	
	
	
// ******************************************************************************

	/*public void loadNetworkWithName() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();
		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			set_NetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}
		
		for(String network:set_NetworkItems)
		{
			comboBoxNetworkItems.add(network);
		}
		
		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}*/	


// ******************************************************************************

	/*public void loadServicesWithName() throws SQLException 
	{
		new LoadServiceGroups().loadServicesWithName();
		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME) 
		{
			set_ServiceItems.add(bean.getServiceName()+" | "+bean.getServiceCode());
		}
		
		for(String service:set_ServiceItems)
		{
			comboBoxServiceItems.add(service);
		}
		comboBoxService.setItems(comboBoxServiceItems);

	}*/
	
	/*public void loadNetworks() throws SQLException
	{
		new LoadNetworks().loadAllNetworksWithName();
		
		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			comboBoxNetworkItems.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}

		comboBoxNetwork.setItems(comboBoxNetworkItems);
	}	*/

	
// ******************************************************************************
	
	public void loadServices() throws SQLException
	{
		new LoadServiceGroups().loadServicesWithName();
		
		for(LoadServiceWithNetworkBean  service:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
		{
			LoadServiceWithNetworkBean snBean=new LoadServiceWithNetworkBean();
			
			snBean.setServiceCode(service.getServiceCode());
			snBean.setNetworkCode(service.getNetworkCode());
			snBean.setServiceName(service.getServiceName());
			
			list_serviceWithNetwork.add(snBean);
			set_services.add(service.getServiceCode());
		}
		
		/*for(String srvcCode: set_services)
		{
			comboBoxServiceItems.add(srvcCode);
		}
		comboBoxService.setItems(comboBoxServiceItems);*/
		
		//TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
	}
	
	
/*	public void setServiceViaNetwork()
	{
		if (comboBoxNetwork.getValue() != null) 
		{
			set_services.clear();
			comboBoxServiceItems.clear();
		
			String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		
			for(LoadServiceWithNetworkBean servicecode: list_serviceWithNetwork)
			{
				if(networkCode[1].equals(servicecode.getNetworkCode()))
				{
					set_services.add(servicecode.getServiceName()+" | "+servicecode.getServiceCode());
				}
			}
			
			for(String srvcCode: set_services)
			{
				comboBoxServiceItems.add(srvcCode);
			}
			comboBoxService.setItems(comboBoxServiceItems);
		}
	}*/
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		if (n.getId().equals("rdBtnMonth")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnDays.requestFocus();
			}
		}

		else if (n.getId().equals("rdBtnDays")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				if(rdBtnMonth.isSelected()==true)
				{
					comboBoxMonth.requestFocus();
				}
				else
				{
					dpkFromDate.requestFocus();
				}
			}
		}

		else if (n.getId().equals("comboBoxMonth")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxYear.requestFocus();
			}
		}
		
		else if (e.getCode().equals(KeyCode.ENTER) && dpkFromDate.isFocused()==true) {
				dpkToDate.requestFocus();
			}
		

		else if (n.getId().equals("dpkFromDate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				dpkToDate.requestFocus();
			}
		}


		else if (n.getId().equals("comboBoxYear")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtClient.requestFocus();
			}
		}

		else if (n.getId().equals("dpkToDate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtClient.requestFocus();
			}
		}

		else if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				clearLabels();
				btnShowDetails.requestFocus();
			}
		}
		
	/*	else if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxNetwork.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				comboBoxService.requestFocus();
			}
		}

		else if (n.getId().equals("comboBoxService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnShowDetails.requestFocus();
			}
		}*/

		else if (n.getId().equals("btnShowDetails")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				//getDataBeforeReCalculation();
				setValidation();
				//progressBarForShowDetails();
				btnAutoCalculate.requestFocus();
			}
		}

		else if (n.getId().equals("btnAutoCalculate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				re_Calculation();
			}
		}
	}
	
	
// *******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		//double test=Double.valueOf(txtWeightUpto.getText());


	
		if(rdBtnMonth.isSelected()==true)
		{
			if (comboBoxMonth.getValue() == null || comboBoxMonth.getValue().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Month field is Empty!");
				alert.showAndWait();
				comboBoxMonth.requestFocus();
			}
			else if (comboBoxYear.getValue() == null || comboBoxYear.getValue().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("Year field is Empty!");
				alert.showAndWait();
				comboBoxYear.requestFocus();
			}
			else if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Client");
				alert.showAndWait();
				txtClient.requestFocus();
			}
			else
			{
				progressBarForShowDetails();
			}
			
		}
		
		else if(rdBtnDays.isSelected()==true)
		{
			if (dpkFromDate.getValue() == null) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("From Date field is Empty!");
				alert.showAndWait();
				dpkFromDate.requestFocus();
			}
			else if (dpkToDate.getValue() == null)
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("To Date field is Empty!");
				alert.showAndWait();
				dpkToDate.requestFocus();
			}
			else if (txtClient.getText() == null || txtClient.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Client");
				alert.showAndWait();
				txtClient.requestFocus();
			}
			
			else
			{
				progressBarForShowDetails();
			}
			
		}
		
		
			
			
		

		



		/*else if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not select Excel File");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}

		else
		{
			progressBarTest();
		}*/

	}		
			

// ***************************************************************************************	
	
	public void progressBarForShowDetails() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Processing.......");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);

		taskUpdateStage.setScene(new Scene(updatePane));
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				getDataBeforeReCalculation();
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			public void handle(WorkerStateEvent t) {
				taskUpdateStage.close();
				if(isDataFound_ForShowDetails==true)
				{
					showBeforeReCalculationAmt();					
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Show Detail Alert");
					alert.setHeaderText(null);
					alert.setContentText("Data Not Found");
					alert.showAndWait();
				}
		
			}
		});

		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();

	}
	
// ***************************************************************************************
	
	public void getCompelteRateDataFromDB(String clientCode) throws SQLException
	{
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		
		try 
		{
			sql = "select client,network,service,basic_range,add_slab,add_range,kg,zone_code,rate_type,da,db,dc,dd,de,df,dg,dh,di,dj,dk,dl,dm,"
					+ "dn,do1,dp,dq,dr,ds,dt,rate_type,additional_reverse,kg_reverse from clientrate_zone,clientrate_slab where client=clientrate2client "
					//+ "and network=clientrate2network and service=clientrate2service and client=? and network=? and service=?";
					+ "and network=clientrate2network and service=clientrate2service and client=?";
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, clientCode);
			//preparedStmt.setString(2, networkCode);
		//	preparedStmt.setString(3, serviceCode);

			System.out.println("Rate SQL: "+preparedStmt);
			
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				ClientRateZoneBean clRateBean=new ClientRateZoneBean();
				
				clRateBean.setClient(rs.getString("client"));
				clRateBean.setNetwork(rs.getString("network"));
				clRateBean.setService(rs.getString("service"));
				clRateBean.setZone_code(rs.getString("zone_code"));
				clRateBean.setType(rs.getString("rate_type"));
				clRateBean.setBasic_range(rs.getDouble("basic_range"));
				clRateBean.setAdd_slab(rs.getDouble("add_slab"));
				clRateBean.setAdd_range(rs.getDouble("add_range"));
				clRateBean.setKg(rs.getString("kg"));
				clRateBean.setDa(rs.getString("da"));
				clRateBean.setDb(rs.getString("db"));
				clRateBean.setDc(rs.getString("dc"));
				clRateBean.setDd(rs.getString("dd"));
				clRateBean.setDe(rs.getString("de"));
				clRateBean.setDf(rs.getString("df"));
				clRateBean.setDg(rs.getString("dg"));
				clRateBean.setDh(rs.getString("dh"));
				clRateBean.setDi(rs.getString("di"));
				clRateBean.setDj(rs.getString("dj"));
				clRateBean.setDk(rs.getString("dk"));
				clRateBean.setDl(rs.getString("dl"));
				clRateBean.setDm(rs.getString("dm"));
				clRateBean.setDn(rs.getString("dn"));
				clRateBean.setDo1(rs.getString("do1"));
				clRateBean.setDp(rs.getString("dp"));
				clRateBean.setDq(rs.getString("dq"));
				clRateBean.setDr(rs.getString("dr"));
				clRateBean.setDs(rs.getString("ds"));
				clRateBean.setDt(rs.getString("dt"));
				clRateBean.setAdditional_reverse(rs.getString("additional_reverse"));
				clRateBean.setKg_reverse(rs.getString("kg_reverse"));
				
				list_ClientsRate.add(clRateBean);
				
			}
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// ***************************************************************************************	
	
	public String getMinimumClientRates(String clientCode,String networkCode,String serviceCode,String zoneCode,double billWeight)
	{
		String[] zone_origin_destination=zoneCode.replaceAll("\\s+","").split("\\|");
		
		String minRate=null;
		String add_rate=null;
		String kg_rate=null;
		
		
		globalRateBean.setDa("DA");
		globalRateBean.setDb("DB");
		globalRateBean.setDc("DC");
		globalRateBean.setDd("DD");
		globalRateBean.setDe("DE");
		globalRateBean.setDf("DF");
		globalRateBean.setDg("DG");
		globalRateBean.setDh("DH");
		globalRateBean.setDi("DI");
		globalRateBean.setDj("DJ");
		globalRateBean.setDk("DK");
		globalRateBean.setDl("DL");
		globalRateBean.setDm("DM");
		globalRateBean.setDn("DN");
		globalRateBean.setDo1("DO");
		globalRateBean.setDp("DP");
		globalRateBean.setDq("DQ");
		globalRateBean.setDr("DR");
		globalRateBean.setDs("DS");
		globalRateBean.setDt("DT");
		globalRateBean.setAdd_rate_colmn("Additional");
		globalRateBean.setKg_rate_colmn("KG");
		
		System.out.println("list_ClientsRate size: >>>>"+list_ClientsRate.size());
		
		for(ClientRateZoneBean clrBean: list_ClientsRate)
		{
			if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
					&& serviceCode.equals(clrBean.getService()) && zone_origin_destination[0].equals(clrBean.getZone_code()))
			{
				if(zone_origin_destination[1].equals(globalRateBean.getDa()))
				{
					minRate=clrBean.getDa();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDb()))
				{
					minRate=clrBean.getDb();					
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDc()))
				{
					minRate=clrBean.getDc();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDd()))
				{
					minRate=clrBean.getDd();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDe()))
				{
					minRate=clrBean.getDe();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDf()))
				{
					minRate=clrBean.getDf();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDg()))
				{
					minRate=clrBean.getDg();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDh()))
				{
					minRate=clrBean.getDh();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDi()))
				{
					minRate=clrBean.getDi();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDj()))
				{
					minRate=clrBean.getDj();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDk()))
				{
					minRate=clrBean.getDk();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDl()))
				{
					minRate=clrBean.getDl();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDm()))
				{
					minRate=clrBean.getDm();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDn()))
				{
					minRate=clrBean.getDn();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDo1()))
				{
					minRate=clrBean.getDo1();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDp()))
				{
					minRate=clrBean.getDp();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDq()))
				{
					minRate=clrBean.getDq();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDr()))
				{
					minRate=clrBean.getDr();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDs()))
				{
					minRate=clrBean.getDs();
				}
				else if(zone_origin_destination[1].equals(globalRateBean.getDt()))
				{
					minRate=clrBean.getDt();
				}
				
				break;
			}
		}
		return minRate;
	}
	
	
	public String getAdditionaRate(String clientCode,String networkCode,String serviceCode,String zoneCode,double billWeight)
	{
		System.out.println("===========================Additional Rate method running.. client :: "+clientCode+" | network :: "+networkCode+" | service :: "+serviceCode+" | zone :: "+zoneCode+" | weight :: "+billWeight);
		String[] zone_origin_destination=zoneCode.replaceAll("\\s+","").split("\\|");
		
		String add_rate=null;
		
		
		for(ClientRateZoneBean clrBean: list_ClientsRate)
		{
			//System.err.println("Additional Rate method running.. client :: "+clrBean.getClient()+" | network :: "+clrBean.getNetwork()+" | service :: "+clrBean.getService()+" | type :: "+clrBean.getType()+" | weight :: "+billWeight);
			
			if(isReverse_Additional==true)
			{
				if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
						&& serviceCode.equals(clrBean.getService()) && clrBean.getType().equals("Basic") && zone_origin_destination[1].equals(clrBean.getZone_code()))
				{
					add_rate=clrBean.getAdditional_reverse();
					isReverse_Additional=false;
					break;
				}
			}
			else
			{
				if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
						&& serviceCode.equals(clrBean.getService()) && clrBean.getType().equals("Additional"))
				{
					//System.out.println("Additional Rate If running....  "+zone_origin_destination[0]);
					if(zone_origin_destination[0].equals(globalRateBean.getDa()))
					{
						add_rate=clrBean.getDa();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDb()))
					{
						add_rate=clrBean.getDb();					
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDc()))
					{
						add_rate=clrBean.getDc();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDd()))
					{
						add_rate=clrBean.getDd();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDe()))
					{
						add_rate=clrBean.getDe();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDf()))
					{
						add_rate=clrBean.getDf();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDg()))
					{
						add_rate=clrBean.getDg();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDh()))
					{
						add_rate=clrBean.getDh();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDi()))
					{
						add_rate=clrBean.getDi();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDj()))
					{
						add_rate=clrBean.getDj();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDk()))
					{
						add_rate=clrBean.getDk();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDl()))
					{
						add_rate=clrBean.getDl();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDm()))
					{
						add_rate=clrBean.getDm();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDn()))
					{
						add_rate=clrBean.getDn();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDo1()))
					{
						add_rate=clrBean.getDo1();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDp()))
					{
						add_rate=clrBean.getDp();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDq()))
					{
						add_rate=clrBean.getDq();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDr()))
					{
						add_rate=clrBean.getDr();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDs()))
					{
						add_rate=clrBean.getDs();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDt()))
					{
						add_rate=clrBean.getDt();
					}
					
					break;
				}
			}
		}
		
		if(add_rate==null)
		{
		add_rate="0";	
		}
		
		System.out.println("Additional From Method... "+add_rate);
		return add_rate;
	}
	
	public String getKGRate(String clientCode,String networkCode,String serviceCode,String zoneCode,double billWeight)
	{
		String[] zone_origin_destination=zoneCode.replaceAll("\\s+","").split("\\|");
		
		String kg_rate=null;
		
		for(ClientRateZoneBean clrBean: list_ClientsRate)
		{
			if(isReverse_KG==true)
			{
				if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
						&& serviceCode.equals(clrBean.getService()) && clrBean.getType().equals("Basic") && zone_origin_destination[1].equals(clrBean.getZone_code()))
				{
					kg_rate=clrBean.getKg_reverse();
					isReverse_KG=false;
					break;
				}
			}
			else
			{
			
				if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
						&& serviceCode.equals(clrBean.getService()) && clrBean.getType().equals("KG"))
				{
					//System.out.println("Kg Rate If running...."+zone_origin_destination[0]);
					if(zone_origin_destination[0].equals(globalRateBean.getDa()))
					{
						kg_rate=clrBean.getDa();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDb()))
					{
						kg_rate=clrBean.getDb();					
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDc()))
					{
						kg_rate=clrBean.getDc();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDd()))
					{
						kg_rate=clrBean.getDd();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDe()))
					{
						kg_rate=clrBean.getDe();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDf()))
					{
						kg_rate=clrBean.getDf();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDg()))
					{
						kg_rate=clrBean.getDg();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDh()))
					{
						kg_rate=clrBean.getDh();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDi()))
					{
						kg_rate=clrBean.getDi();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDj()))
					{
						kg_rate=clrBean.getDj();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDk()))
					{
						kg_rate=clrBean.getDk();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDl()))
					{
						kg_rate=clrBean.getDl();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDm()))
					{
						kg_rate=clrBean.getDm();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDn()))
					{
						kg_rate=clrBean.getDn();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDo1()))
					{
						kg_rate=clrBean.getDo1();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDp()))
					{
						kg_rate=clrBean.getDp();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDq()))
					{
						kg_rate=clrBean.getDq();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDr()))
					{
						kg_rate=clrBean.getDr();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDs()))
					{
						kg_rate=clrBean.getDs();
					}
					else if(zone_origin_destination[0].equals(globalRateBean.getDt()))
					{
						kg_rate=clrBean.getDt();
					}
					
					break;
				}
			}
		}
		
		if(kg_rate==null)
		{
			kg_rate="0";	
		}
		System.out.println("KG from Method... "+kg_rate);
		return kg_rate;
		
	}
	
	
	public String getKG_Status(String clientCode,String networkCode,String serviceCode,String zoneCode,double billWeight)
	{
		//String[] zone_origin_destination=zoneCode.replaceAll("\\s+","").split("\\|");
		
		String kg_status=null;
		
		for(ClientRateZoneBean clrBean: list_ClientsRate)
		{
			if(clientCode.equals(clrBean.getClient()) && networkCode.equals(clrBean.getNetwork()) 
					&& serviceCode.equals(clrBean.getService()))
			{
				kg_status=clrBean.getKg();
				break;
			}
		}
		
		System.out.println("KG Status from Method... "+kg_status);
		return kg_status;
		
		
	}
	
	
// ***************************************************************************************
	
/*	public void getPincodeWithZone() throws SQLException
	{
		if(list_PincodeWithZone_air_surface.size()==0)
		{
			list_PincodeWithZone_air_surface.clear();
			ResultSet rs = null;
			Statement stmt = null;
			String sql = null;
			DBconnection dbcon = new DBconnection();
			Connection con = dbcon.ConnectDB();
				
			try 
			{
				stmt = con.createStatement();
				sql = "select pincode,zone_air,zone_surface from pincode_master";
				rs = stmt.executeQuery(sql);
	
				while (rs.next()) 
				{
					UploadPincodeBean upBean=new UploadPincodeBean();
					
					upBean.setPincode(rs.getString("pincode"));
					upBean.setZone_air(rs.getString("zone_air"));
					upBean.setZone_surface(rs.getString("zone_surface"));
					
					list_PincodeWithZone_air_surface.add(upBean);
				}
			}
			catch (Exception e) 
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally 
			{
				dbcon.disconnect(null, stmt, rs, con);
			}
		}
		
	}*/
	

// ***************************************************************************************	

	public String getZoneFromPincodes(String origin,String destination,String serviceCode) throws SQLException
	{
		
		System.err.println("OR >>>>>>>>>> "+origin+" | DSTI >>>>>>>>>>>>> "+destination+" | Service code >>> "+serviceCode);
		
		String originZone=null;
		String destiZone=null;
		String getServiceMode=null;
		
		String origin_destination_zone=null;
		
		new LoadServiceGroups().loadServicesFromServiceType();
		for(LoadServiceWithNetworkBean bean:LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
		{
			if(bean.getServiceCode().equals(serviceCode))
			{
				getServiceMode=bean.getServiceMode();
				System.out.println("Service Name: "+bean.getServiceCode()+" | Mode: "+bean.getServiceMode());
				break;
			}
		}
		
		
		
		
		/*for(UploadPincodeBean upBean: list_PincodeWithZone_air_surface)
		{*/
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(origin.equals(pinBean.getPincode()))
			{
				if(getServiceMode.equals("AR"))
				{
					originZone=pinBean.getZone_air();
				}
				else if(getServiceMode.equals("SF"))
				{
					originZone=pinBean.getZone_surface();
				}
				System.out.println("Origin Service Mode >>> "+originZone);
			}
			
			if(destination.equals(pinBean.getPincode()))
			{
				if(getServiceMode.equals("AR"))
				{
					destiZone=pinBean.getZone_air();
				}
				else if(getServiceMode.equals("SF"))
				{
					destiZone=pinBean.getZone_surface();
				}
				
				System.out.println("Destination Service Mode >>> "+destiZone);
			}
		
			
			
			if(originZone!=null && destiZone!=null)
			{
				origin_destination_zone=originZone+" | "+destiZone;
				System.out.println("Origin Zone: " +origin+" "+originZone+" | Desitination Zone: " +destination+" "+destiZone);
				break;
			}
		}
		return origin_destination_zone;
	}
	
	
// ***************************************************************************************	

	public void clearLabels()
	{
		lab_Before_BasicAmt.setText("--");
		lab_Before_VasTotalAmt.setText("--");
		lab_Before_Fuel.setText("--");
		lab_Before_CGST.setText("--");
		lab_Before_SGST.setText("--");
		lab_Before_IGST.setText("--");
		lab_Before_Total.setText("--");
		
		lab_After_BasicAmt.setText("--");
		lab_After_VasTotalAmt.setText("--");
		lab_After_Fuel.setText("--");
		lab_After_CGST.setText("--");
		lab_After_SGST.setText("--");
		lab_After_IGST.setText("--");
		lab_After_Total.setText("--");
	}
	
// ***************************************************************************************
	
	public void getDataBeforeReCalculation() throws SQLException
	{
		list_ShowDetailsData.clear();
		list_ClientsRate.clear();

		
		clearLabels();
		
		String[] monthNo=null;
		String year=null;
		
		Date fromDate=null;
		Date todate=null;
		String dateRangeSQL=null;
		String zone_Origin_Destination=null;
		
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		
		int slno = 1;
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		//String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		//String[] serviceCode=comboBoxService.getValue().replaceAll("\\s+","").split("\\|");
		
		//dateRangeSQL="dailybookingtransaction2clientdailybookingtransaction2networkdailybookingtransaction2service";
		
		if(rdBtnMonth.isSelected()==true)
		{
			monthNo=comboBoxMonth.getValue().replaceAll("\\s+","").split("\\|");
			year=comboBoxYear.getValue();
			
			dateRangeSQL="to_char(booking_date,'MM,YYYY')='"+monthNo[1]+","+year+"'";
		}
		else
		{
			LocalDate local_FromDate=dpkFromDate.getValue();
			LocalDate local_ToDate=dpkToDate.getValue();
			
			fromDate=Date.valueOf(local_FromDate);
			todate=Date.valueOf(local_ToDate);
			
			dateRangeSQL="booking_date between '"+ fromDate+"' AND '"+todate+"'";
		}
		
		
		try 
		{

			//stmt = con.createStatement();
			sql = "select master_awbno,booking_date,forwarder_account,dailybookingtransaction2branch,dailybookingtransaction2client,origin,"
					+ "zipcode,dailybookingtransaction2network,dailybookingtransaction2service,packets,billing_weight,"
					+ "insurance_amount,vas_amount,amount,docket_charge,fuel_rate,fuel,vas_total,tax_name1,tax_rate1,tax_amount1,"
					+ "tax_name2,tax_rate2,tax_amount2,tax_name3,tax_rate3,tax_amount3,total from dailybookingtransaction where "
					//+ "dailybookingtransaction2client=? and dailybookingtransaction2network=? and dailybookingtransaction2service=? and travel_status='dataentry' and mps_type='T' and spot_rate='F' and "+dateRangeSQL;
					+ "dailybookingtransaction2client=? and travel_status='dataentry' and mps_type='T' and spot_rate='F' and invoice_number is null and "+dateRangeSQL;
			
			
			//rs = stmt.executeQuery(sql);
			
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, clientCode[1]);
			//preparedStmt.setString(2, networkCode[1]);
			//preparedStmt.setString(3, serviceCode[1]);

		
			System.out.println("SQL  Query: >> "+preparedStmt);
			
			rs = preparedStmt.executeQuery();
			
			if(!rs.next())
			{
				isDataAvailable=false;
			}
			else
			{
				isDataAvailable=true;
				do{
					
					AutoCalculationBean acBean=new AutoCalculationBean();
					
					acBean.setSlno(slno);
					acBean.setMasterAwbNo(rs.getString("master_awbno"));
					acBean.setBookingDate(date.format(rs.getDate("booking_date")));
					acBean.setForwarderAccount(rs.getString("forwarder_account"));
					acBean.setBranchCode(rs.getString("dailybookingtransaction2branch"));
					acBean.setClientCode(rs.getString("dailybookingtransaction2client"));
					acBean.setOriginPincode(rs.getString("origin"));
					acBean.setDestinationPincode(rs.getString("zipcode"));
					acBean.setNetworkCode(rs.getString("dailybookingtransaction2network"));
					acBean.setServiceCode(rs.getString("dailybookingtransaction2service"));
					acBean.setPcs(rs.getInt("packets"));
					acBean.setBillWeight(rs.getDouble("billing_weight"));
					acBean.setInsuranceAmount(rs.getDouble("insurance_amount"));
					acBean.setVasAmount(rs.getDouble("vas_amount"));
					acBean.setBasicAmount(rs.getDouble("amount"));
					acBean.setDocketCharges(rs.getDouble("docket_charge"));
					acBean.setFuelRate(rs.getDouble("fuel_rate"));
					acBean.setFuelAmount(rs.getDouble("fuel"));
					acBean.setVasTotal(rs.getDouble("vas_total"));
					acBean.setTax_name1(rs.getString("tax_name1"));
					acBean.setTax_amount1(rs.getDouble("tax_amount1"));
					acBean.setTax_rate1(rs.getDouble("tax_rate1"));
					acBean.setTax_name2(rs.getString("tax_name2"));
					acBean.setTax_amount2(rs.getDouble("tax_amount2"));
					acBean.setTax_rate2(rs.getDouble("tax_rate2"));
					acBean.setTax_name3(rs.getString("tax_name3"));
					acBean.setTax_amount3(rs.getDouble("tax_amount3"));
					acBean.setTax_rate3(rs.getDouble("tax_rate3"));
					acBean.setTotal(rs.getDouble("total"));
					
					acBean.setGstAmt(Double.valueOf(df.format(acBean.getTax_amount1()+acBean.getTax_amount2()+acBean.getTax_amount3())));
					
					list_ShowDetailsData.add(acBean);
					
					//loadClientVASData(acBean.getMasterAwbNo());
					
	
					slno++;
				}while (rs.next());
				
				
			}

		
			
			if(list_ShowDetailsData.isEmpty()==false)
			{
				
				
					getVAS_Data_BeforeReCalculation();
				
				//showBeforeReCalculationAmt();
				loadChangeEntryTable();
				System.out.println("Zone Origin | Destination >> "+zone_Origin_Destination);
				
				loadDimensionDataViaAWBno();
				loadClientVASData();
				//System.out.println("Show data list size: "+list_ShowDetailsData.size());
			//	//System.out.println("Dimension list size:  "+list_getDimensionsBeforeReCalculation.size());
			//	System.out.println("Added VAS list size: "+list_AddedVAS_on_Awbno.size());
				isDataFound_ForShowDetails=true;
			}
			else
			{
				
				clearLabels();
				tabledata_ChangeEntry.clear();
				isDataFound_ForShowDetails=false;
			}
			
		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
		
	}

// ****************************************************************	

	public void loadClientVASData() throws SQLException
	{
		
		/*if(isClientVAS_Updated==true)
			{*/
		list_ClientVAS.clear();
		
		for(String clientCode: set_ClientFromFilteredList)
		{
		
		ResultSet rs = null;

		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		try 
		{
			sql = "select client_cd,service_cd,cod_min,cod_per,insur_own_min,insur_own_per,insur_carr_min,"
					+ "insur_carr_per,oss_min,oss_perkg,docket_chrgs,oda_min,oda_perkg,opa_min,opa_perkg,adv_fee_min,adv_fee_per,"
					+ "delivery_on_invoice,fuel_rate,hfd_min,hfd_per_pcs_kg,hfd_type,hfd_free_floor,topay,addres_correction_chrgs,"
					+ "hold_at_office_min,hold_at_office_perkg,green_tax,reverse_pickup_min,reverse_pickup_slab,tdd_min,tdd_add,"
					+ "tdd_slab,critical_min,critical_add,critical_slab,oss_kg_limit,oss_cm_limit,fov,fuel_excluding_fov from client_vas where client_cd=?";
			
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,clientCode);
			
		//	System.out.println("ClientVAS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();

			while (rs.next()) 
			{
				ClientVASBean cvBean=new ClientVASBean();
				
				cvBean.setClient(rs.getString("client_cd"));
				cvBean.setService(rs.getString("service_cd"));
				cvBean.setCod_Min(rs.getDouble("cod_min"));
				cvBean.setCod_Percentage(rs.getDouble("cod_per"));
				cvBean.setInsurance_Owner_Min(rs.getDouble("insur_own_min"));
				cvBean.setInsurance_Owner_Percentage(rs.getDouble("insur_own_per"));
				cvBean.setInsurance_Carrier_Min(rs.getDouble("insur_carr_min"));
				cvBean.setInsurance_Carrier_Percentage(rs.getDouble("insur_carr_per"));
				cvBean.setOverSizeShpmnt_Min(rs.getDouble("oss_min"));
				cvBean.setOverSizeShpmnt_Kg(rs.getDouble("oss_perkg"));
				cvBean.setDocketCharge_Min(rs.getDouble("docket_chrgs"));
				cvBean.setOda_Min(rs.getDouble("oda_min"));
				cvBean.setOda_Kg(rs.getDouble("oda_perkg"));
				cvBean.setOpa_Min(rs.getDouble("opa_min"));
				cvBean.setOpa_Kg(rs.getDouble("opa_perkg"));
				cvBean.setAdvancementFee_Min(rs.getDouble("adv_fee_min"));
				cvBean.setAdvancementFee_Percentage(rs.getDouble("adv_fee_per"));
				cvBean.setDeliveryOnInvoice(rs.getDouble("delivery_on_invoice"));
				cvBean.setFuelSurcharge(rs.getDouble("fuel_rate"));
				cvBean.setHdf_Charges_Min(rs.getDouble("hfd_min"));
				cvBean.setHdf_Charges_PerPcs(rs.getDouble("hfd_per_pcs_kg"));
				cvBean.setHfd_type_pcs_kg(rs.getString("hfd_type"));
				cvBean.setHfd_free_flr(rs.getInt("hfd_free_floor"));
				cvBean.setToPay(rs.getDouble("topay"));
				cvBean.setAddressCorrectionCharges(rs.getDouble("addres_correction_chrgs"));
				cvBean.setHoldAtOffice_Min(rs.getDouble("hold_at_office_min"));
				cvBean.setHoldAtOffice_Kg(rs.getDouble("hold_at_office_perkg"));
				cvBean.setGreenTax(rs.getDouble("green_tax"));
				cvBean.setReversePickup_Min(rs.getDouble("reverse_pickup_min"));
				cvBean.setReversePickup_slab(rs.getDouble("reverse_pickup_slab"));
				cvBean.settDD_Rate(rs.getDouble("tdd_min"));
				cvBean.settDD_Additional(rs.getDouble("tdd_add"));
				cvBean.settDD_Slab(rs.getDouble("tdd_slab"));
				cvBean.setCriticalSrvc_Rate(rs.getDouble("critical_min"));
				cvBean.setCriticalSrvc_Additional(rs.getDouble("critical_add"));
				cvBean.setCriticalSrvc_Slab(rs.getDouble("critical_slab"));
				cvBean.setOverSizeShpmnt_kg_Limit(rs.getDouble("oss_kg_limit"));
				cvBean.setOverSizeShpmnt_cm_Limit(rs.getDouble("oss_cm_limit"));
				cvBean.setFov_Status(rs.getString("fov"));
				cvBean.setFov_Fuel_Excluding(rs.getString("fuel_excluding_fov"));

				oss_kg_limit_global=cvBean.getOverSizeShpmnt_kg_Limit();
				oss_cm_limit_global=cvBean.getOverSizeShpmnt_cm_Limit();
				
				//System.out.println("Client: "+cvBean.getClient()+" | Service: "+ cvBean.getService()+" *************** >> OSS KG >> "+oss_kg_limit_global+ "         OSS CM >> " +oss_cm_limit_global);
				
				

				list_ClientVAS.add(cvBean);
			}

			
			
			//CLIENT=clientCode[1];
			//SERVICE=txtService.getText();

			/*System.err.println("Client VAS Fresh loaded from DB for Client :"+clientCode[1]+" and Service: "+txtService.getText());
			System.out.println("COD Min: "+cvBean.getCod_Min());
			System.out.println("ODA: "+cvBean.getOda_Min());
			System.out.println("ODA kg: "+cvBean.getOda_Kg());
			System.out.println("OSS: "+cvBean.getOverSizeShpmnt_Min());
			System.out.println("OPA: "+cvBean.getOpa_Min());
			System.out.println("OPA kg: "+cvBean.getOpa_Kg());
			System.out.println("Carrier: "+cvBean.getInsurance_Carrier_Min());
			System.out.println("Owner: "+cvBean.getInsurance_Owner_Min());
			System.out.println("OCT Fee: "+cvBean.getAdvancementFee_Min());
			System.out.println("Hold At: "+cvBean.getHoldAtOffice_Min());
			System.out.println("HFD: "+cvBean.getHdf_Charges_Min());
			System.out.println("Green Tax: "+cvBean.getGreenTax());
			System.out.println("Address Correction: "+cvBean.getAddressCorrectionCharges());
			System.out.println("To Pay: "+cvBean.getToPay());
			System.out.println("Docket: "+cvBean.getDocketCharge_Min());
			System.out.println("DOI: "+cvBean.getDeliveryOnInvoice());
			System.out.println("TDD: "+cvBean.gettDD_Rate());
			System.out.println("Critical: "+cvBean.getCriticalSrvc_Rate());
			System.out.println("Fule: "+cvBean.getFuelSurcharge());
			System.out.println("HFD type: "+cvBean.getHfd_type_pcs_kg());
			System.out.println("HFD floor: "+cvBean.getHfd_free_flr());
			System.out.println("HFD per pcs kg: "+cvBean.getHdf_Charges_PerPcs());
			System.out.println("OSS Kg Limit: "+cvBean.getOverSizeShpmnt_kg_Limit());
			System.out.println("OSS cm Limit: "+cvBean.getOverSizeShpmnt_cm_Limit());
			System.out.println("FOV Status: "+cvBean.getFov_Status());*/
			
			isDataFound_ForShowDetails=true;

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		}
		
		System.out.println("Client VAS size >?>?>?>?>?>?>?>?>?>?>?>?>?>? "+list_ClientVAS.size());
		//}

	}


	
// ***************************************************************************************	
	
	public void getVAS_Data_BeforeReCalculation() throws SQLException
	{
		double resultVAS=0;

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;
		DataEntryBean deBean = new DataEntryBean();
		try 
		{
			for(AutoCalculationBean acBean:list_ShowDetailsData)
			{
			
			String sql ="select master_awb_no,vas_cd,vas_amount,vas_total_amount,delivery_on_invoice_amount,cod_amount,"
					+ "hold_at_office_amount,adv_fee_amount,hfd_amount,oss_amount,topay_amount,green_tax_amount,reverse_pickup_amount,tdd_amount,"
					+ "critical_amount,addrs_crr_amount,vas_name,oda_amount,opa_amount,hfd_floor_number from daily_booking_vas where master_awb_no=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, acBean.getMasterAwbNo());

			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				//tabledata_VAS.clear();
			} 
			else 
			{
				do 
				{
					deBean.setMasterAwbNo(rs.getString("master_awb_no"));
					deBean.setAddOnExpense_VAS(rs.getString("vas_cd"));
					deBean.setExpenseAmount_VAS(rs.getString("vas_amount"));
					deBean.setVasName(rs.getString("vas_name"));
					deBean.setExpenseAmountTotal_VAS(rs.getDouble("vas_total_amount"));
					deBean.setDoi_VAS(rs.getDouble("delivery_on_invoice_amount"));
					deBean.setHoldAtOffice_VAS(rs.getDouble("hold_at_office_amount"));
					deBean.setOct_Fee_VAS(rs.getDouble("adv_fee_amount"));
					deBean.setHfd_Charges_VAS(rs.getDouble("hfd_amount"));
					deBean.setGreenTax_VAS(rs.getDouble("green_tax_amount"));
					deBean.setOss_VAS(rs.getDouble("oss_amount"));
					deBean.setToPay_VAS(rs.getDouble("topay_amount"));
					deBean.setReversePickup_VAS(rs.getDouble("reverse_pickup_amount"));
					deBean.setTdd_VAS(rs.getDouble("tdd_amount"));
					deBean.setCriticalService_VAS(rs.getDouble("critical_amount"));
					deBean.setAddressCorrection_VAS(rs.getDouble("addrs_crr_amount"));
					deBean.setOda_VAS(rs.getDouble("oda_amount"));
					deBean.setOpa_VAS(rs.getDouble("opa_amount"));
					deBean.setCod_amount_vas(rs.getDouble("cod_amount"));
					deBean.setHfd_floor_number(rs.getInt("hfd_floor_number"));
					
					list_AddedVAS_on_Awbno.add(deBean);
				} 
				while (rs.next());
			}
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ***************************************************************************************

	public void getODA_OPA_ViaPincodes(String client, String service,String originPincode, String destinationPincode,double billingWeight) throws SQLException
	{
	
		String branchCity=null;
		String originCity=null;
		
		for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
		{
			if(CommonVariable.USER_BRANCH_PINCODE.equals(pinBean.getPincode()))
			{
				branchCity=pinBean.getCity_name();
				break;
			}
		}
		
		
		List_ODA_OPA_FromPincode.clear();
		
		
		
		VAS_CalculationBean vcBean=new VAS_CalculationBean();
		
		new LoadPincodeForAll().loadPincode();
		
		//System.out.println("Destination pincode :: "+destinationPincode+" | Origin Pincode :: "+originPincode+" | Pincode list size ::::: "+LoadPincodeForAll.list_Load_Pincode_from_Common.size());
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(destinationPincode.equals(pinBean.getPincode()))
			{
				//System.out.println("ODA OPA Destination Running.....");
				
				Oda_Opa_Bean od_op_Bean=new Oda_Opa_Bean();
				od_op_Bean.setType_org_desti("destination");
				
				od_op_Bean.setPincode_Origin_Destination(pinBean.getPincode());
				od_op_Bean.setType_ODA_OPA(pinBean.getOda_Opa_Regular());
				List_ODA_OPA_FromPincode.add(od_op_Bean);
				break;
			}
		}
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(originPincode.equals(pinBean.getPincode()))
			{
				originCity=pinBean.getCity_name();
				//System.out.println("ODA OPA Origin Running.....");
				Oda_Opa_Bean od_op_Bean=new Oda_Opa_Bean();
				od_op_Bean.setType_org_desti("origin");
				od_op_Bean.setPincode_Origin_Destination(pinBean.getPincode());
				od_op_Bean.setType_ODA_OPA(pinBean.getOda_Opa_Regular());
				List_ODA_OPA_FromPincode.add(od_op_Bean);
				break;
			}
		}
		
		/*String client1=null;
		String service1=null;
		for(ClientVASBean vasBean:list_ClientVAS)
		{
			
		}*/
		
		//System.out.println("branch City :: "+branchCity+" ============================================= Origin City :: "+originCity);
		
		if(originCity.equals(branchCity))
		{
			isReverse_Additional=false;
			isReverse_KG=false;
			//System.out.println(" >>>>>>>>>>>>>>>>>>>>>  not reverse");
		}
		else
		{
			isReverse_Additional=true;
			isReverse_KG=true;
			//System.out.println(" >>>>>>>>>>>>>>>>>>>>> reverse");
		}
		
		
		/*for(Oda_Opa_Bean bean:List_ODA_OPA_FromPincode)
		{
			System.out.println("pincode: >> "+bean.getPincode_Origin_Destination()+" | OPA/ODA value: >> "+bean.getType_ODA_OPA()+" | Type: >> "+bean.getType_org_desti());
			
		}*/
		
		for(Oda_Opa_Bean bean: List_ODA_OPA_FromPincode)
		{
			//System.out.println("VAS loop running..." +billingWeight);
			
			if(bean.getType_org_desti().equals("origin") && !bean.getType_ODA_OPA().equals("Regular"))
			{
				//System.out.println("Check Origin");
				for(ClientVASBean vasBean:list_ClientVAS)
				{
					//System.out.println("if >>> loop running...");
					//System.out.println("vasBean: ODA: "+vasBean.getOda_Min());
					vcBean.setOda_Min_Amt(vasBean.getOda_Min());
					//System.out.println("vcBean: ODA: "+vcBean.getOda_Min_Amt());
					vcBean.setOda_Kg_Amt(vasBean.getOda_Kg()*billingWeight);
					//oda_min=vasBean.getOda_Min();
					//oda_kg=vasBean.getOda_Kg();
					break;
				}
			}
			
			else if(bean.getType_org_desti().equals("destination") && !bean.getType_ODA_OPA().equals("Regular"))
			{
				//System.out.println("Check Destination >> "+bean.getType_org_desti()+" | "+bean.getType_ODA_OPA());
				
				for(ClientVASBean vasBean:list_ClientVAS)
				{
					if(client.equals(vasBean.getClient()) && service.equals(vasBean.getService()))
					{
					//System.err.println("^^^^^^^^^ "+vasBean.getOpa_Min());
					//System.out.println("else >>> loop running...");
					//System.out.println("vasBean: OpA: "+vasBean.getOpa_Min());
					
					vcBean.setOpa_Min_Amt(vasBean.getOpa_Min());
					
					//System.out.println("vcBean: OpA: "+vcBean.getOpa_Min_Amt());
					
					vcBean.setOpa_Kg_Amt(vasBean.getOpa_Kg()*billingWeight);
					break;
					}
					
					//opa_min=vasBean.getOpa_Min();
					//opa_kg=vasBean.getOpa_Kg();
				}
			}
		}
		
		
			
		
		/*for(Oda_Opa_Bean bean: List_ODA_OPA_FromPincode)
		{
			
			if(bean.getType_org_desti().equals("destination") && !bean.getType_ODA_OPA().equals("Regular"))
			{
				System.out.println("Check Destination >> "+list_ClientVAS.size());
				
				for(ClientVASBean vasBean:list_ClientVAS)
				{
					System.err.println("^^^^^^^^^ "+vasBean.getOpa_Min());
					System.out.println("else >>> loop running...");
					System.out.println("vasBean: OpA: "+vasBean.getOpa_Min());
					vcBean.setOpa_Min_Amt(vasBean.getOpa_Min());
					System.out.println("vcBean: OpA: "+vcBean.getOpa_Min_Amt());
					vcBean.setOpa_Kg_Amt(vasBean.getOpa_Kg()*billingWeight);
					break;
					
					//opa_min=vasBean.getOpa_Min();
					//opa_kg=vasBean.getOpa_Kg();
				}
			}
		}*/
		
		System.out.println("Origin ODA_min >> "+vcBean.getOda_Min_Amt()+" | KG amt >> "+vcBean.getOda_Kg_Amt());
		System.out.println("Destination OPA_min >> "+vcBean.getOpa_Min_Amt()+" | KG amt >> "+vcBean.getOpa_Kg_Amt());
		
		
		//for()
	//	s
		
		if(vcBean.getOda_Min_Amt()>vcBean.getOda_Kg_Amt())
		{
			oda_amount=vcBean.getOda_Min_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOda_Min_Amt();
		}
		else
		{
			oda_amount=vcBean.getOda_Kg_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOda_Kg_Amt();
		}
		
		if(vcBean.getOpa_Min_Amt()>vcBean.getOpa_Kg_Amt())
		{
			opa_amount=vcBean.getOpa_Min_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOpa_Min_Amt();
		}
		else
		{
			opa_amount=vcBean.getOpa_Kg_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOpa_Kg_Amt();
		}
		
		System.err.println("OPA: "+opa_amount+" | oda: "+oda_amount);
		
	}

// ***************************************************************************************	

	
	public boolean checkClientAndBranchStateForGST(String branchCode,String clientCode)
	{
		
		String branchPincode=null;
		String clientPincode=null;
		
		String branchState=null;
		String clientState=null;
		
		boolean isSameState=false;
		
		// ********** Getting Branch Pincode via Branch Code **************
		
		for(LoadBranchBean branchBean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{	
			if(branchCode.equals(branchBean.getBranchCode()))
			{
				branchPincode=branchBean.getBranchPincode();
				break;
			}
		}
		
		// ********** Getting Client Pincode via Client Code **************
		
		for(LoadClientBean clBean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			if(clientCode.equals(clBean.getClientCode()))
			{
				clientPincode=clBean.getPincode();
				break;
			}
		}
		
		// ********** Getting Branch State Code via Branch Pincode **************
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pinBean.getPincode().equals(branchPincode))
			{
				branchState=pinBean.getState_code();
				break;
			}
		}
		
		// ********** Getting Client State Code via Client Pincode **************
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pinBean.getPincode().equals(clientPincode))
			{
				clientState=pinBean.getState_code();
				break;
			}
		}
		
		
		System.err.println("Client State >> "+clientState );
		System.err.println("Branch State >> "+branchState );
		
		if(clientState.equals(branchState))
		{
			System.out.println("CGST SGST applicable");
			isSameState=true;
		}
		else
		{
			System.out.println("IGST applicable");
			isSameState=false;
		}
		
		return isSameState;
		
	}
	
// ***************************************************************************************	

	public void getDocketCharge(String client,String service,double billingWeight)
	{
		for(ClientVASBean clBean:list_ClientVAS)
		{
			if(clBean.getClient().equals(client) && clBean.getService().equals(service))
			{
				docketCharge=0;
				docketCharge=clBean.getDocketCharge_Min();
				oss_min_amt=clBean.getOverSizeShpmnt_Min();
				fuel_rate_global=clBean.getFuelSurcharge();
				oss_per_kg_Amt=(clBean.getOverSizeShpmnt_Kg()*billingWeight);
				oss_kg_limit=clBean.getOverSizeShpmnt_kg_Limit();
				oss_cm_limit=clBean.getOverSizeShpmnt_cm_Limit();
				
				if(clBean.getFov_Status().equals("T"))
				{
					fovStatus=true;
				}
				else
				{
					fovStatus=false;
				}
				
				if(clBean.getFov_Fuel_Excluding().equals("T"))
				{
					fuel_Excluding_FOV_status=true;
				}
				else
				{
					fuel_Excluding_FOV_status=false;
				}
				
				break;
			}
		}
	}

	
// ***************************************************************************************	
	
	public void getOSS_Amt(String awb,double billingWeight)
	{
		
		System.err.println("**************************** Awb No: "+awb+" | GetOSS method >>>>>>>>> "+oss_kg_limit_global);
		
		if(oss_kg_limit>0)
		{
			
			//System.out.println("Awb No: "+awb+" | Inside OSS global >>>>>>>>> "+oss_kg_limit_global+"     | OSS without global >> "+oss_kg_limit);
			
			if(billingWeight>oss_kg_limit)
			{
				if(oss_min_amt>oss_per_kg_Amt)
				{
					oss_VAS_Calculated_Amt=oss_min_amt;
				}
				else
				{
					oss_VAS_Calculated_Amt=oss_per_kg_Amt;
				}
			}
			else
			{
				if(oss_cm_limit>0)
				{
					for(DimensionBean diBean: list_getDimensionsBeforeReCalculation)
					{
						if(diBean.getAwbno().equals(awb))
						{
							if(diBean.getLength()>oss_cm_limit || diBean.getWidth()>oss_cm_limit || diBean.getWidth()>oss_cm_limit)
							{
								if(oss_min_amt>oss_per_kg_Amt)
								{
									oss_VAS_Calculated_Amt=oss_min_amt;
								}
								else
								{
									oss_VAS_Calculated_Amt=oss_per_kg_Amt;
								}
								break;
							}
						}
					}
				}
			}
		}
		else
		{
			oss_VAS_Calculated_Amt=0;
		}
	}
	
	
// ***************************************************************************************	
	
	/*public void loadVASData() throws SQLException
	{
		if(isClientVAS_Updated==true)
		{
		list_ClientVAS.clear();
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		//String[] networkCode=txtNetwork.getText().replaceAll("\\s+","").split("\\|");
		
		ClientVASBean cvBean=new ClientVASBean();
		
		cvBean.setClient(clientCode[1]);
		//cvBean.setNetwork(networkCode[1]);
		cvBean.setService(txtService.getText());
		//>>
		
		ResultSet rs = null;
		
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;


		try 
		{
			
			sql = MasterSQL_DataEntry_Utils.CLIENTVAS_IN_DATAENTRY_RETRIEVE_SQL;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,cvBean.getClient());
			preparedStmt.setString(2,cvBean.getService());
			
			//System.out.println("ClientVAS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
				cvBean.setCod_Min(rs.getDouble("cod_min"));
				cvBean.setCod_Percentage(rs.getDouble("cod_per"));
				cvBean.setInsurance_Owner_Min(rs.getDouble("insur_own_min"));
				cvBean.setInsurance_Owner_Percentage(rs.getDouble("insur_own_per"));
				cvBean.setInsurance_Carrier_Min(rs.getDouble("insur_carr_min"));
				cvBean.setInsurance_Carrier_Percentage(rs.getDouble("insur_carr_per"));
				cvBean.setOverSizeShpmnt_Min(rs.getDouble("oss_min"));
				cvBean.setOverSizeShpmnt_Kg(rs.getDouble("oss_perkg"));
				cvBean.setDocketCharge_Min(rs.getDouble("docket_chrgs"));
				cvBean.setOda_Min(rs.getDouble("oda_min"));
				cvBean.setOda_Kg(rs.getDouble("oda_perkg"));
				cvBean.setOpa_Min(rs.getDouble("opa_min"));
				cvBean.setOpa_Kg(rs.getDouble("opa_perkg"));
				cvBean.setAdvancementFee_Min(rs.getDouble("adv_fee_min"));
				cvBean.setAdvancementFee_Percentage(rs.getDouble("adv_fee_per"));
				cvBean.setDeliveryOnInvoice(rs.getDouble("delivery_on_invoice"));
				cvBean.setFuelSurcharge(rs.getDouble("fuel_rate"));
				cvBean.setHdf_Charges_Min(rs.getDouble("hfd_min"));
				cvBean.setHdf_Charges_PerPcs(rs.getDouble("hfd_per_pcs_kg"));
				cvBean.setHfd_type_pcs_kg(rs.getString("hfd_type"));
				cvBean.setHfd_free_flr(rs.getInt("hfd_free_floor"));
				cvBean.setToPay(rs.getDouble("topay"));
				cvBean.setAddressCorrectionCharges(rs.getDouble("addres_correction_chrgs"));
				cvBean.setHoldAtOffice_Min(rs.getDouble("hold_at_office_min"));
				cvBean.setHoldAtOffice_Kg(rs.getDouble("hold_at_office_perkg"));
				cvBean.setGreenTax(rs.getDouble("green_tax"));
				cvBean.setReversePickup_Min(rs.getDouble("reverse_pickup_min"));
				cvBean.setReversePickup_slab(rs.getDouble("reverse_pickup_slab"));
				cvBean.settDD_Rate(rs.getDouble("tdd_min"));
				cvBean.settDD_Additional(rs.getDouble("tdd_add"));
				cvBean.settDD_Slab(rs.getDouble("tdd_slab"));
				cvBean.setCriticalSrvc_Rate(rs.getDouble("critical_min"));
				cvBean.setCriticalSrvc_Additional(rs.getDouble("critical_add"));
				cvBean.setCriticalSrvc_Slab(rs.getDouble("critical_slab"));
				cvBean.setOverSizeShpmnt_kg_Limit(rs.getDouble("oss_kg_limit"));
				cvBean.setOverSizeShpmnt_cm_Limit(rs.getDouble("oss_cm_limit"));
				cvBean.setFov_Status(rs.getString("fov"));
				
				oss_kg_limit_global=cvBean.getOverSizeShpmnt_kg_Limit();
				oss_cm_limit_global=cvBean.getOverSizeShpmnt_cm_Limit();
				
				if(cvBean.getFov_Status().equals("T"))
				{
					fov_status=true;
				}
				else
				{
					fov_status=false;
				}
				
				txtDocketCharges.setText(df.format(cvBean.getDocketCharge_Min()));
				
				
				list_ClientVAS.add(cvBean);
			}
			
			CLIENT=clientCode[1];
			SERVICE=txtService.getText();
			
			System.err.println("Client VAS Fresh loaded from DB for Client :"+clientCode[1]+" and Service: "+txtService.getText());
			System.out.println("COD Min: "+cvBean.getCod_Min());
			System.out.println("ODA: "+cvBean.getOda_Min());
			System.out.println("ODA kg: "+cvBean.getOda_Kg());
			System.out.println("OSS: "+cvBean.getOverSizeShpmnt_Min());
			System.out.println("OPA: "+cvBean.getOpa_Min());
			System.out.println("OPA kg: "+cvBean.getOpa_Kg());
			System.out.println("Carrier: "+cvBean.getInsurance_Carrier_Min());
			System.out.println("Owner: "+cvBean.getInsurance_Owner_Min());
			System.out.println("OCT Fee: "+cvBean.getAdvancementFee_Min());
			System.out.println("Hold At: "+cvBean.getHoldAtOffice_Min());
			System.out.println("HFD: "+cvBean.getHdf_Charges_Min());
			System.out.println("Green Tax: "+cvBean.getGreenTax());
			System.out.println("Address Correction: "+cvBean.getAddressCorrectionCharges());
			System.out.println("To Pay: "+cvBean.getToPay());
			System.out.println("Docket: "+cvBean.getDocketCharge_Min());
			System.out.println("DOI: "+cvBean.getDeliveryOnInvoice());
			System.out.println("TDD: "+cvBean.gettDD_Rate());
			System.out.println("Critical: "+cvBean.getCriticalSrvc_Rate());
			System.out.println("Fule: "+cvBean.getFuelSurcharge());
			System.out.println("HFD type: "+cvBean.getHfd_type_pcs_kg());
			System.out.println("HFD floor: "+cvBean.getHfd_free_flr());
			System.out.println("HFD per pcs kg: "+cvBean.getHdf_Charges_PerPcs());
			System.out.println("OSS Kg Limit: "+cvBean.getOverSizeShpmnt_kg_Limit());
			System.out.println("OSS cm Limit: "+cvBean.getOverSizeShpmnt_cm_Limit());
			System.out.println("FOV Status: "+cvBean.getFov_Status());
			insuranceAmount_carrier_global=cvBean.getInsurance_Carrier_Min();
			insuranceAmount_owner_global=cvBean.getInsurance_Owner_Min();

			isClientVAS_Updated=false;
			
			if(Double.valueOf(txtInsuranceAmount.getText())==0)
			{
				loadInsuranceRateAndAmount();
			}
			
		}
		
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		//}
		
	}*/
	
	
// ***************************************************************************************
	
	public double getVAS_Total_Except_OSS_OPA_ODA(String awbNo)
	{
		double vas_sum_except_oss_oda_opa=0;
		for(DataEntryBean dBean: list_AddedVAS_on_Awbno)
		{
			if(dBean.getMasterAwbNo().equals(awbNo))
			{
				vas_sum_except_oss_oda_opa=dBean.getOct_Fee_VAS()+dBean.getDoi_VAS()+dBean.getHfd_Charges_VAS()+dBean.getToPay_VAS()+
						dBean.getAddressCorrection_VAS()+dBean.getHoldAtOffice_VAS()+dBean.getGreenTax_VAS()+
						dBean.getReversePickup_VAS()+dBean.getTdd_VAS()+dBean.getCriticalService_VAS()+dBean.getExpenseAmountTotal_VAS()+dBean.getCod_amount_vas();
				
				System.err.println(awbNo+" | OCT fee: "+dBean.getOct_Fee_VAS()+" | DOI: "+dBean.getDoi_VAS()+" | HFD: "+dBean.getHfd_Charges_VAS()+
						" | To Pay: "+dBean.getToPay_VAS()+" | Add. Corrt.: "+dBean.getAddressCorrection_VAS()+" | Hold at Ofc: "+dBean.getHoldAtOffice_VAS());
				System.err.println(awbNo+" | Green Tax: "+dBean.getGreenTax_VAS()+" | Reverse: "+dBean.getReversePickup_VAS()+" | TDD: "+dBean.getTdd_VAS()+" | Crit.: "+dBean.getCriticalService_VAS()+
						" | Other VAS: "+dBean.getOtherVasTotal()+" | COD: "+dBean.getCod_amount_vas());
				System.err.println("TOTAL VAS: "+vas_sum_except_oss_oda_opa);
				
				break;
			}
		}
		
		return vas_sum_except_oss_oda_opa;
		
	}
	
	
// ***************************************************************************************	
	
	public void re_Calculation() throws SQLException
	{
		String zone_Origin_Destination=null;
		String minRate=null;
		String addRate=null;
		String kgRate=null;
		String kgStatus=null;
		double finalBasicAmt=0;
		double gstFinalAmt=0;
		double vas_sum_except_oss_oda_opa=0;
		boolean isSameState=false;
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		//String[] networkCode=comboBoxNetwork.getValue().replaceAll("\\s+","").split("\\|");
		//String[] serviceCode=comboBoxService.getValue().replaceAll("\\s+","").split("\\|");
		
		
		if(isDataAvailable==true)
		{
			System.out.println(" +++++++++++++++++++++++++++ 1 >>>>>>>>>>");
			getCompelteRateDataFromDB(clientCode[1]);
			//isDataAvailable=false;
		}
		
		
		int count=0;
		
		for(AutoCalculationBean acBean: list_ShowDetailsData)
		{
			count++;
			System.out.println(" +++++++++++++++++++++++++++ 2 >>>>>>>>>>");
			
			//System.err.println("Master Awb: "+acBean.getMasterAwbNo()+" | Client: "+acBean.getClientCode()+" | Service: "+acBean.getServiceCode()+" | Weight: "+acBean.getBillWeight()+" | Network: "+acBean.getNetworkCode());
			
			zone_Origin_Destination=getZoneFromPincodes(acBean.getOriginPincode(), acBean.getDestinationPincode(), acBean.getServiceCode());
			System.out.println("origin Destination >>> "+zone_Origin_Destination);
			if(zone_Origin_Destination!=null)
			{
			String[] zone_org_desti=zone_Origin_Destination.replaceAll("\\s+","").split("\\|");
			
			
			//getODA_OPA_ViaPincodes(zone_org_desti[0], zone_org_desti[1],acBean.getBillWeight());
			getODA_OPA_ViaPincodes(acBean.getClientCode(),acBean.getServiceCode(),acBean.getOriginPincode(), acBean.getDestinationPincode(),acBean.getBillWeight());
			getDocketCharge(acBean.getClientCode(),acBean.getServiceCode(),acBean.getBillWeight());
			getOSS_Amt(acBean.getMasterAwbNo(), acBean.getBillWeight());
			
			System.out.println("origin destination >>>>>>>> "+ zone_Origin_Destination);
			minRate=getMinimumClientRates(acBean.getClientCode(), acBean.getNetworkCode(), acBean.getServiceCode(), zone_Origin_Destination, acBean.getBillWeight());
			addRate=getAdditionaRate(acBean.getClientCode(), acBean.getNetworkCode(), acBean.getServiceCode(), zone_Origin_Destination, acBean.getBillWeight());
			kgRate=getKGRate(acBean.getClientCode(), acBean.getNetworkCode(), acBean.getServiceCode(), zone_Origin_Destination, acBean.getBillWeight());
			kgStatus=getKG_Status(acBean.getClientCode(), acBean.getNetworkCode(), acBean.getServiceCode(), zone_Origin_Destination, acBean.getBillWeight());
			
			System.out.println("Baisc Rate: "+minRate+" | Add Rate: "+addRate+" | kgRate: "+kgRate);
			
			finalBasicAmt=getFinalAmountOnBillingWeight(acBean.getClientCode(), acBean.getNetworkCode(), acBean.getServiceCode(),minRate,addRate,kgRate,kgStatus,acBean.getBillWeight());
			System.out.println("Final Basic Amount: "+finalBasicAmt);
			
			vas_sum_except_oss_oda_opa=getVAS_Total_Except_OSS_OPA_ODA(acBean.getMasterAwbNo());
			
			System.err.println("Vas Before >>> 1 > "+vas_sum_except_oss_oda_opa);
			
			
			/*for(DataEntryBean dBean: list_AddedVAS_on_Awbno)
			{
				if(dBean.getMasterAwbNo().equals(acBean.getMasterAwbNo()))
				{
					vas_sum_except_oss_oda_opa=dBean.getOct_Fee_VAS()+dBean.getDoi_VAS()+dBean.getHfd_Charges_VAS()+dBean.getToPay_VAS()+
							dBean.getAddressCorrection_VAS()+dBean.getHoldAtOffice_VAS()+dBean.getGreenTax_VAS()+
							dBean.getReversePickup_VAS()+dBean.getTdd_VAS()+dBean.getCriticalService_VAS()+dBean.getOtherVasTotal()+dBean.getCod_amount_vas();
				}
			}*/
			
			
			double taxableValue=0;
			AutoCalculationBean ac_changedBean=new AutoCalculationBean();
			
			ac_changedBean.setMasterAwbNo(acBean.getMasterAwbNo());
			ac_changedBean.setBasicAmount(finalBasicAmt);
			ac_changedBean.setZoneCode(zone_org_desti[1]);
			ac_changedBean.setDocketCharges(docketCharge);
			ac_changedBean.setOpa_VAS(opa_amount);
			ac_changedBean.setOda_VAS(oda_amount);
			ac_changedBean.setOss_VAS(oss_VAS_Calculated_Amt);
			ac_changedBean.setInsuranceAmount(acBean.getInsuranceAmount());
			//System.err.println("Vas Total Before Calculate>>> 2> "+acBean.getVasTotal() +" | OSS: "+oss_VAS_Calculated_Amt+" | oda: "+oda_amount+" | opa: "+opa_amount+" excepVAS: "+vas_sum_except_oss_oda_opa);
			//System.err.println("Vas Before >>> 2> "+vas_sum_except_oss_oda_opa);
			ac_changedBean.setVasTotal(opa_amount+oda_amount+oss_VAS_Calculated_Amt+vas_sum_except_oss_oda_opa);
			
			System.out.println();
			
			/*System.out.println("OSS Calculated AMt: "+oss_VAS_Calculated_Amt);
			System.out.println("VAS Total AMt: "+ac_changedBean.getVasTotal());
			System.err.println("Vas Before >>> 3> "+vas_sum_except_oss_oda_opa);
			System.err.println("Vas Before >>> 4> "+ac_changedBean.getVasAmount());
			System.out.println("Basic: >> "+ac_changedBean.getBasicAmount());
			System.out.println("Docket: >> "+ac_changedBean.getDocketCharges());
			System.out.println("OPA: >> "+ac_changedBean.getOpa_VAS());
			System.out.println("ODA: >> "+ac_changedBean.getOda_VAS());
			System.out.println("OSS: >> "+ac_changedBean.getOss_VAS());
			System.out.println("VAS total: >> "+(ac_changedBean.getVasTotal()+docketCharge));
			System.out.println("Insurance: >>"+acBean.getInsuranceAmount());*/
			
			double amountToGetFuel=0;
			
			if(fovStatus==false && fuel_Excluding_FOV_status==false)
			{
				amountToGetFuel=finalBasicAmt;
			}
			else if(fovStatus==false && fuel_Excluding_FOV_status==true)
			{
				amountToGetFuel=finalBasicAmt+docketCharge+ac_changedBean.getVasTotal();
			}
			else if(fovStatus==true && fuel_Excluding_FOV_status==false)
			{
				amountToGetFuel=finalBasicAmt+acBean.getInsuranceAmount();
			}
			else if(fovStatus==true && fuel_Excluding_FOV_status==true)
			{
				amountToGetFuel=finalBasicAmt+docketCharge+ac_changedBean.getVasTotal()+acBean.getInsuranceAmount();
			}

			ac_changedBean.setFuelAmount((amountToGetFuel*fuel_rate_global)/100);
			
			/*if(fovStatus==true)
			{
				
				//amountToGetFuel=finalBasicAmt+docketCharge+opa_amount+oda_amount+oss_VAS_Calculated_Amt
					//	+ac_changedBean.getVasTotal()+acBean.getInsuranceAmount();
				amountToGetFuel=finalBasicAmt+docketCharge+ac_changedBean.getVasTotal()+acBean.getInsuranceAmount();
				System.out.println("Fuel on Amount from if >>>>> "+amountToGetFuel);
				ac_changedBean.setFuelAmount((amountToGetFuel*fuel_rate_global)/100);
			}
			else
			{
				//amountToGetFuel=finalBasicAmt+docketCharge+opa_amount+oda_amount+oss_VAS_Calculated_Amt
					//	+ac_changedBean.getVasTotal();
				amountToGetFuel=finalBasicAmt+docketCharge+ac_changedBean.getVasTotal();
				System.out.println("Fuel on Amount from else >>>>> "+amountToGetFuel);
				ac_changedBean.setFuelAmount((amountToGetFuel*fuel_rate_global)/100);
			}*/
			
			System.out.println("Re calculated fuel: >>>>>>>>>>> "+ac_changedBean.getFuelAmount() +" | Fuel Rate : "+fuel_rate_global);
			
			//ac_changedBean.setFuelRate((finalBasicAmt*acBean.getFuelRate())/100);
			
			isSameState=checkClientAndBranchStateForGST(acBean.getBranchCode(), acBean.getClientCode());
			
			ac_changedBean.setTax_name1(LoadGST_Rates.cgst_Name);
			ac_changedBean.setTax_name2(LoadGST_Rates.sgst_Name);
			ac_changedBean.setTax_name3(LoadGST_Rates.igst_Name);
			
			if(isSameState==true)
			{
				ac_changedBean.setTax_rate1(LoadGST_Rates.cgst_rate_fix);
				ac_changedBean.setTax_rate2(LoadGST_Rates.sgst_rate_fix);
				ac_changedBean.setTax_rate3(0);
				
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
								+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
					//taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
				ac_changedBean.setTax_amount1((taxableValue*ac_changedBean.getTax_rate1())/100);
				ac_changedBean.setTax_amount2((taxableValue*ac_changedBean.getTax_rate2())/100);
				ac_changedBean.setTax_amount3(0);
				
				//System.out.println("else >>> Taxable Value: "+taxableValue+" | CGST RATE: "+ac_changedBean.getTax_rate1()+" | CGST TAX Amt: "+ac_changedBean.getTax_amount1());
				//System.out.println("else >>> Taxable Value: "+taxableValue+" | SGST RATE: "+ac_changedBean.getTax_rate2()+" | SGST TAX Amt: "+ac_changedBean.getTax_amount2());
				
			}
			else
			{
				ac_changedBean.setTax_rate1(0);
				ac_changedBean.setTax_rate2(0);
				ac_changedBean.setTax_rate3(LoadGST_Rates.igst_rate_fix);
				
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
								+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
					//taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
				
				ac_changedBean.setTax_amount1(0);
				ac_changedBean.setTax_amount2(0);
				ac_changedBean.setTax_amount3((taxableValue*ac_changedBean.getTax_rate3())/100);
				
				System.out.println("if>>> Taxable Value: "+taxableValue+" | IGST RATE: "+ac_changedBean.getTax_rate3()+" | IGST TAX Amt: "+ac_changedBean.getTax_amount3());
				
				
			}
			
			
			/*if(acBean.getTax_rate1()!=0)
			{
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
								+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
				
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
				+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
				
				ac_changedBean.setTax_amount1((taxableValue*acBean.getTax_rate1())/100);
				System.out.println("if>>> Taxable Value: "+taxableValue+" | CGST RATE: "+acBean.getTax_rate1()+" | CGST TAX Amt: "+ac_changedBean.getTax_amount1());
			}
			else
			{
				ac_changedBean.setTax_amount1(acBean.getTax_amount1());
				System.out.println("else>>> Taxable Value: "+taxableValue+" | CGST RATE: "+acBean.getTax_rate1()+" | CGST TAX Amt: "+ac_changedBean.getTax_amount1());
			}
			
			if(acBean.getTax_rate2()!=0)
			{
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
								+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
				ac_changedBean.setTax_amount2((taxableValue*acBean.getTax_rate2())/100);
				System.out.println("if>>> Taxable Value: "+taxableValue+" | SGST RATE: "+acBean.getTax_rate2()+" | SGST TAX Amt: "+ac_changedBean.getTax_amount2());
			}
			else
			{
				ac_changedBean.setTax_amount2(acBean.getTax_amount2());
				System.out.println("else>>> Taxable Value: "+taxableValue+" | SGST RATE: "+acBean.getTax_rate2()+" | SGST TAX Amt: "+ac_changedBean.getTax_amount2());
			}
			
			if(acBean.getTax_rate3()!=0)
			{
				taxableValue=ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+acBean.getInsuranceAmount()
								+ac_changedBean.getVasTotal()+ac_changedBean.getFuelAmount();
				//taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
				ac_changedBean.setTax_amount3((taxableValue*acBean.getTax_rate3())/100);
				System.out.println("if>>> Taxable Value: "+taxableValue+" | IGST RATE: "+acBean.getTax_rate3()+" | IGST TAX Amt: "+ac_changedBean.getTax_amount3());
			}
			else
			{
				ac_changedBean.setTax_amount3(acBean.getTax_amount3());
				System.out.println("else>>> Taxable Value: "+taxableValue+" | IGST RATE: "+acBean.getTax_rate3()+" | IGST TAX Amt: "+ac_changedBean.getTax_amount3());
			}*/
				
			gstFinalAmt=ac_changedBean.getTax_amount1()+ac_changedBean.getTax_amount2()+ac_changedBean.getTax_amount3();
			
			ac_changedBean.setTotal(ac_changedBean.getBasicAmount()+ac_changedBean.getDocketCharges()+ac_changedBean.getOpa_VAS()
			+ac_changedBean.getOda_VAS()+ac_changedBean.getOss_VAS()+acBean.getInsuranceAmount()+ac_changedBean.getFuelAmount()+gstFinalAmt+vas_sum_except_oss_oda_opa);
			
			
			oss_VAS_Calculated_Amt=0;
			System.out.println("OSS Reset Value: "+oss_VAS_Calculated_Amt);
				list_ChangedData.add(ac_changedBean);
		
			
			/*if(finalBasicAmt!=acBean.getBasicAmount() || oda_amount!=acBean.getOda_VAS() || 
					opa_amount!=acBean.getOpa_VAS() || docketCharge!=acBean.getDocketCharges() || 
					oss_VAS_Calculated_Amt!=acBean.getOss_VAS())
			{
				double taxableValue=0;
				AutoCalculationBean ac_changedBean=new AutoCalculationBean();
				
				ac_changedBean.setMasterAwbNo(acBean.getMasterAwbNo());
				ac_changedBean.setBasicAmount(finalBasicAmt);
				ac_changedBean.setFuelRate((finalBasicAmt*acBean.getFuelRate())/100);
				
				if(acBean.getTax_rate1()!=0)
				{
					taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
					ac_changedBean.setTax_amount1((taxableValue*acBean.getTax_rate1())/100);
				}
				else
				{
					ac_changedBean.setTax_amount1(acBean.getTax_amount1());
				}
				
				if(acBean.getTax_rate2()!=0)
				{
					taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
					ac_changedBean.setTax_amount2((taxableValue*acBean.getTax_rate2())/100);
				}
				else
				{
					ac_changedBean.setTax_amount2(acBean.getTax_amount2());
				}
				
				if(acBean.getTax_rate3()!=0)
				{
					taxableValue=acBean.getInsuranceAmount()+acBean.getVasTotal()+ac_changedBean.getFuelAmount()+ac_changedBean.getBasicAmount()+acBean.getDocketCharges();
					ac_changedBean.setTax_amount3((taxableValue*acBean.getTax_rate3())/100);
				}
				else
				{
					ac_changedBean.setTax_amount3(acBean.getTax_amount3());
				}
					
				gstFinalAmt=ac_changedBean.getTax_amount1()+ac_changedBean.getTax_amount2()+ac_changedBean.getTax_amount3();
				
				ac_changedBean.setTotal(ac_changedBean.getBasicAmount()+ac_changedBean.getFuelAmount()+gstFinalAmt+acBean.getDocketCharges()+acBean.getInsuranceAmount());
				
				list_ChangedData.add(ac_changedBean);
			}
			else
			{
				AutoCalculationBean ac_changedBean=new AutoCalculationBean();
				ac_changedBean.setMasterAwbNo(acBean.getMasterAwbNo());
				ac_changedBean.setBookingDate(acBean.getBookingDate());
				ac_changedBean.setForwarderAccount(acBean.getForwarderAccount());
				ac_changedBean.setBranchCode(acBean.getBranchCode());
				ac_changedBean.setClientCode(acBean.getClientCode());
				ac_changedBean.setOriginPincode(acBean.getOriginPincode());
				ac_changedBean.setDestinationPincode(acBean.getDestinationPincode());
				ac_changedBean.setNetworkCode(acBean.getNetworkCode());
				ac_changedBean.setServiceCode(acBean.getServiceCode());
				ac_changedBean.setPcs(acBean.getPcs());
				ac_changedBean.setBillWeight(acBean.getBillWeight());
				ac_changedBean.setInsuranceAmount(acBean.getInsuranceAmount());
				ac_changedBean.setVasAmount(acBean.getVasAmount());
				ac_changedBean.setBasicAmount(acBean.getBasicAmount());
				ac_changedBean.setDocketCharges(acBean.getDocketCharges());
				ac_changedBean.setFuelRate(acBean.getFuelRate());
				ac_changedBean.setFuelAmount(acBean.getFuelAmount());
				ac_changedBean.setVasTotal(acBean.getVasTotal());
				ac_changedBean.setTax_name1(acBean.getTax_name1());
				ac_changedBean.setTax_amount1(acBean.getTax_amount1());
				ac_changedBean.setTax_rate1(acBean.getTax_rate1());
				ac_changedBean.setTax_name2(acBean.getTax_name2());
				ac_changedBean.setTax_amount2(acBean.getTax_amount2());
				ac_changedBean.setTax_rate2(acBean.getTax_rate2());
				ac_changedBean.setTax_name3(acBean.getTax_name3());
				ac_changedBean.setTax_amount3(acBean.getTax_amount3());
				ac_changedBean.setTax_rate3(acBean.getTax_rate3());
				ac_changedBean.setTotal(acBean.getTotal());
				list_UnChangedData.add(ac_changedBean);
			}*/
			}
			
		}
		
		double vs=0;
		for(AutoCalculationBean ac:list_ChangedData)
		{
			vs=vs+ac.getVasTotal()+ac.getDocketCharges()+ac.getInsuranceAmount();
		}
		
		System.out.println("VS >>>>>>>>>>>>> "+vs);
		
		 showAfterReCalculationAmt();
		
		isDataAvailable=false;
	}
	
	
	public double getFinalAmountOnBillingWeight(String clientCode,String network,String service,String rates,String addRate,String kgRate,String kgStatus,double billingWeight)
	{
		double finalAmt=0;
		for(ClientRateZoneBean clrBean: list_ClientsRate)
		{
			if(clientCode.equals(clrBean.getClient()) && network.equals(clrBean.getNetwork()) && service.equals(clrBean.getService()))
			{
			System.out.println(" MMMMMMMMMMMMMMMMMMMM >>>>>>>>>>>>> Add slab >> "+clrBean.getAdd_slab()+" | Add Range >> "+clrBean.getAdd_range());
			finalAmt=new NewCalculateWeight().calculateRateViaListOfRates(rates,addRate,kgRate,kgStatus, billingWeight, clrBean.getBasic_range(), clrBean.getAdd_slab(), clrBean.getAdd_range());
			System.out.println("final Amount: >>>>>>>>> "+finalAmt);
			break;
			}
		}
		return finalAmt;
	}

// ***************************************************************************************
	
	public void loadChangeEntryTable()
	{
		tabledata_ChangeEntry.clear();
		System.out.println("Table Loaded.... >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		for(AutoCalculationBean acBean: list_ShowDetailsData)
		{
			tabledata_ChangeEntry.add(new AutoCalculationTableBean(acBean.getSlno(), acBean.getMasterAwbNo(), acBean.getBookingDate(),
					acBean.getForwarderAccount(), acBean.getClientCode(), acBean.getBranchCode(), acBean.getNetworkCode(), acBean.getServiceCode(),
					acBean.getPcs(), acBean.getBillWeight(), acBean.getBasicAmount(), acBean.getFuelAmount(), acBean.getInsuranceAmount(), acBean.getVasTotal(), acBean.getGstAmt(), acBean.getDocketCharges()));
		}
		
		tableChngEntries.setItems(tabledata_ChangeEntry);
		
	}
	
// ***************************************************************************************
	
	public void loadDimensionDataViaAWBno() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			for(AutoCalculationTableBean aclBean: tabledata_ChangeEntry)
			{
				set_ClientFromFilteredList.add(aclBean.getClientCode());
				list_AwbFromFilteredList.add(aclBean.getMasterAwbNo());
				
			String sql = "select slno,awbno,sub_awbno,pcs,weight,height,length,width from dimensions where awbno=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,aclBean.getMasterAwbNo());

			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{

			} 
			else 
			{
				do 
				{
					DimensionBean diBean = new DimensionBean();

					diBean.setDb_serialNo(rs.getInt("slno"));
					diBean.setPcs(rs.getInt("pcs"));
					diBean.setAwbno(rs.getString("awbno"));
					diBean.setSub_AwbNo(rs.getString("sub_awbno"));
					diBean.setLength(rs.getDouble("length"));
					diBean.setWidth(rs.getDouble("width"));
					diBean.setHeight(rs.getDouble("height"));
					diBean.setWeight(rs.getDouble("weight"));

					list_getDimensionsBeforeReCalculation.add(diBean);
				} 
				while (rs.next());
			}
			
			
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}

	
// ***************************************************************************************	
	
	public void showBeforeReCalculationAmt()
	{
		
		double basicAmt=0;
		double vasTotal=0;
		double fuel=0;
		double cgst=0;
		double sgst=0;
		double igst=0;
		double total=0;
		
		for(AutoCalculationBean acBean: list_ShowDetailsData)
		{
			basicAmt=basicAmt+acBean.getBasicAmount();
			vasTotal=vasTotal+acBean.getVasTotal()+acBean.getDocketCharges()+acBean.getInsuranceAmount();
			fuel=fuel+acBean.getFuelAmount();
			cgst=cgst+acBean.getTax_amount1();
			sgst=sgst+acBean.getTax_amount2();
			igst=igst+acBean.getTax_amount3();
			total=total+acBean.getTotal();
		}
		
		lab_Before_BasicAmt.setText(df.format(basicAmt));
		lab_Before_VasTotalAmt.setText(df.format(vasTotal));
		lab_Before_Fuel.setText(df.format(fuel));
		lab_Before_CGST.setText(df.format(cgst));
		lab_Before_SGST.setText(df.format(sgst));
		lab_Before_IGST.setText(df.format(igst));
		lab_Before_Total.setText(df.format(total));

		
		btnShowDetails.setDisable(true);
		btnAutoCalculate.setDisable(false);
	}
	
// ***************************************************************************************	
	
	public void showAfterReCalculationAmt() throws SQLException
	{
		double basicAmt=0;
		double fuel=0;
		double cgst=0;
		double sgst=0;
		double igst=0;
		double total=0;
		double vasTotal=0;
		
		System.err.println("After Calculation >>>>>>>>>>>>" +vasTotal);
		double docket=0;
		double onlyvas=0;
		double insurance=0;
		
		for(AutoCalculationBean acBean: list_ChangedData)
		{
			docket=docket+acBean.getDocketCharges();
			onlyvas=onlyvas+acBean.getVasTotal();
			insurance=insurance+acBean.getInsuranceAmount();
			
			//double vs=acBean.getVasTotal()+acBean.getDocketCharges()+acBean.getInsuranceAmount();
			basicAmt=basicAmt+acBean.getBasicAmount();
			vasTotal=vasTotal+acBean.getVasTotal()+acBean.getDocketCharges()+acBean.getInsuranceAmount();
			fuel=fuel+acBean.getFuelAmount();
			cgst=cgst+acBean.getTax_amount1();
			sgst=sgst+acBean.getTax_amount2();
			igst=igst+acBean.getTax_amount3();
			total=total+acBean.getTotal();
			
			//System.out.println("AWB no: "+acBean.getMasterAwbNo()+" | Basic: "+acBean.getBasicAmount()+" | VAS: "+vs+" | Fuel: "+acBean.getFuelAmount()+" | CGST: "+acBean.getTax_amount1()+" | SGST: "+acBean.getTax_amount2()+" | IGST: "+acBean.getTax_amount3());
		}
		
		System.out.println("Total Vas :: "+vasTotal+" | Only VAS :: "+onlyvas+" | Docket :: "+docket+" | Insurance :: "+insurance);
	/*	for(AutoCalculationBean acBean: list_UnChangedData)
		{
			basicAmt=basicAmt+acBean.getBasicAmount();
			fuel=fuel+acBean.getFuelAmount();
			cgst=cgst+acBean.getTax_amount1();
			sgst=sgst+acBean.getTax_amount2();
			igst=igst+acBean.getTax_amount3();
			total=total+acBean.getTotal();
		}*/

		
		lab_After_BasicAmt.setText(df.format(basicAmt));
		lab_After_VasTotalAmt.setText(df.format(vasTotal));
		lab_After_Fuel.setText(df.format(fuel));
		lab_After_CGST.setText(df.format(cgst));
		lab_After_SGST.setText(df.format(sgst));
		lab_After_IGST.setText(df.format(igst));
		lab_After_Total.setText(df.format(total));
		
		
		update_Re_CalculatedData();

		list_ChangedData.clear();

		/*totalVAS_amount=0;
		opa_amount=0;
		oda_amount=0;
		docketCharge=0;
		oss_min_amt=0;
		oss_per_kg_Amt=0;
		oss_VAS_Calculated_Amt=0;
		oss_kg_limit=0;
		oss_cm_limit=0;
		
		fovStatus=false;
		fuel_Excluding_FOV_status=false;
		
		fuel_rate_global=0;
		
		isDataFound_ForShowDetails=true;
		
		cgst_amount=0;
		sgst_amount=0;
		igst_amount=0;
		cgst_rate=0;
		sgst_rate=0;
		igst_rate=0;
		igst_name=null;
		cgst_name=null;
		sgst_name=null;*/
		
		btnShowDetails.setDisable(false);
		btnAutoCalculate.setDisable(true);
		
		txtClient.requestFocus();
		
		
	}

// ***************************************************************************************	
	
	public void update_Re_CalculatedData() throws SQLException
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		//DataEntryBean deBean = new DataEntryBean();
			
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int batchCount=0;
		
		if(list_ChangedData.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}

		try 
		{
			int	count=0;
			String query = "update dailybookingtransaction SET insurance_amount=?,amount=?,docket_charge=?,fuel=?,vas_total=?,"
					+ "tax_amount1=?,tax_amount2=?,tax_amount3=?,tax_name1=?,tax_name2=?,tax_name3=?,tax_rate1=?,tax_rate2=?,"
					+ "tax_rate3=?,total=?,zone_code=? where air_way_bill_number=? and mps_type='T'";
				
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);

			for(AutoCalculationBean acBean: list_ChangedData)
			{
				count++;
				preparedStmt.setDouble(1, acBean.getInsuranceAmount());
				preparedStmt.setDouble(2, acBean.getBasicAmount());
				preparedStmt.setDouble(3, acBean.getDocketCharges());
				preparedStmt.setDouble(4, acBean.getFuelAmount());
				preparedStmt.setDouble(5, acBean.getVasTotal());
				preparedStmt.setDouble(6, acBean.getTax_amount1());
				preparedStmt.setDouble(7, acBean.getTax_amount2());
				preparedStmt.setDouble(8, acBean.getTax_amount3());
				preparedStmt.setString(9, acBean.getTax_name1());
				preparedStmt.setString(10, acBean.getTax_name2());
				preparedStmt.setString(11, acBean.getTax_name3());
				preparedStmt.setDouble(12, acBean.getTax_rate1());
				preparedStmt.setDouble(13, acBean.getTax_rate2());
				preparedStmt.setDouble(14, acBean.getTax_rate3());
				preparedStmt.setDouble(15, acBean.getTotal());
				preparedStmt.setString(16, acBean.getZoneCode());
				preparedStmt.setString(17, acBean.getMasterAwbNo());

				preparedStmt.addBatch();
				
				batchCount++;
				if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, batchCount);
				}
			}

			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows updated in DailyBookingTransaction Table: "+ result.length);
			con.commit();

			updateRe_Calculated_VAS_Date();

			
			if(count==result.length)
			{
				alert.setContentText("Re-Calculated Data successfully saved...");
				alert.showAndWait();
			}
			
			list_ChangedData.clear();
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
	
// ***************************************************************************************
	
	public void updateRe_Calculated_VAS_Date() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_ChangedData.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}

		try 
		{
			String query = "update daily_booking_vas SET oss_amount=?,oda_amount=?,opa_amount=? where master_awb_no=?";
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(AutoCalculationBean acBean: list_ChangedData)
			{
				preparedStmt.setDouble(1, acBean.getOss_VAS());
				preparedStmt.setDouble(2, acBean.getOda_VAS());
				preparedStmt.setDouble(3, acBean.getOpa_VAS());
				preparedStmt.setString(4, acBean.getMasterAwbNo());
	
				preparedStmt.addBatch();
				
				count++;
				if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
				}
			}
			
			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows updated in daily_booking_vas Table: "+ result.length);
			con.commit();
			
			//System.err.println("From Update 2 >>>> Awb No: >> "+awbNo+" | DB Status >>"+status);

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
	
	
	
	
// ***************************************************************************************
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		comboBoxMonth.setItems(comboBoxMonthItems);
		comboBoxYear.setItems(comboBoxYearItems);
		
		rdBtnMonth.setSelected(true);
		comboBoxMonth.setDisable(false);
		comboBoxYear.setDisable(false);
		dpkFromDate.setDisable(true);
		dpkToDate.setDisable(true);
		btnAutoCalculate.setDisable(true);
		
		
		tabCol_chngEntry_slno.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Integer>("slno"));
		tabCol_chngEntry_Mstr_Awb_no.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("masterAwbNo"));
		tabCol_chngEntry_BookingDate.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("bookingDate"));
		tabCol_chngEntry_ForwardAccount.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("forwarderAccount"));
		tabCol_chngEntry_ClientCode.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("clientCode"));
		tabCol_chngEntry_BranchCode.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("branchCode"));
		tabCol_chngEntry_NetworkCode.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("networkCode"));
		tabCol_chngEntry_ServiceCode.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,String>("serviceCode"));
		tabCol_chngEntry_PCS.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Integer>("pcs"));
		tabCol_chngEntry_BillingWeight.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("billWeight"));
		tabCol_chngEntry_BasicAmt.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("basicAmount"));
		tabCol_chngEntry_FuelAmt.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("fuelAmount"));
		tabCol_chngEntry_InsuranceAmt.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("InsuranceAmount"));
		tabCol_chngEntry_VAS_Total.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("vasTotal"));
		tabCol_chngEntry_GSTAmt.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("gstAmount"));
		tabCol_chngEntry_DocketCharge.setCellValueFactory(new PropertyValueFactory<AutoCalculationTableBean,Double>("docketCharges"));
		
		try {
			loadClients();
			/*loadNetworks();
			loadServices();*/
			//getPincodeWithZone();
			System.out.println("Pincode list size: "+list_PincodeWithZone_air_surface.size());
			
			//loadNetworkWithName();
			//loadServicesWithName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
