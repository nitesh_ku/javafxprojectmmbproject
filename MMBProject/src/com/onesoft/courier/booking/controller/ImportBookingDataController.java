package com.onesoft.courier.booking.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.DataEntryBean;
import com.onesoft.courier.booking.bean.DimensionBean;
import com.onesoft.courier.booking.bean.InwardScannedAwbTableBean;
import com.onesoft.courier.booking.bean.SavedBookingDataTableBean;
import com.onesoft.courier.booking.bean.UnSavedBookingDataTableBean;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.DateValidator;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.sql.queries.MasterSQL_ImportBookingData_Excel_Utils;

import groovy.sql.Sql;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;

public class ImportBookingDataController implements Initializable {
	
	private ObservableList<SavedBookingDataTableBean> tabledata_SavedBookingData=FXCollections.observableArrayList();
	private ObservableList<UnSavedBookingDataTableBean> tabledata_UnSavedBookingData=FXCollections.observableArrayList();
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	
	int[] updateResult;
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy",Locale.US);
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	private List<DataEntryBean> list_BookingRecords=new ArrayList<>();
	private List<String> list_FormatMisMatch=new ArrayList<>();
	private Set<String> set_CheckDuplicateAwbNo=new HashSet<>();
	
	private List<DimensionBean> list_AwbNoWithAvailableDimensions =new ArrayList<>();
	
	private Set<String> set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking=new HashSet();
	private Set<String> set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update=new HashSet();
	
	private List<DataEntryBean> list_AwbNoToStore=new ArrayList<>();
	private List<String> list_AwbNoToUpdate=new ArrayList<>();
	private List<String> list_AwbnoNotExistInDB=new ArrayList<>();
	private List<String> list_AwbnoNotExistInDB_for_ConsignerConsignee=new ArrayList<>();
	
	int numberOfEmptySheets=0;
	int totalSheets=0;
	
	@FXML
	private Hyperlink hyperlinkCloudInward;
	
	@FXML
	private Hyperlink hyperlinkOutWard;
	
	@FXML
	private Hyperlink hyperlinkDataEntry;
	
	@FXML
	private Hyperlink hyperlinkInward;

	@FXML
	private TextField txtBrowseExcel;
	
	@FXML
	private Button btnBrowse;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private RadioButton rdBtnAdd;
	
	@FXML
	private RadioButton rdBtnUpdate;

// ***************************************	
	
	@FXML
	private Pagination pagination_Saved;
	
	@FXML
	private Pagination pagination_UnSaved;
	
	@FXML
	private Button btnExportToExcel_Saved;
	
	@FXML
	private Button btnExportToExcel_UnSaved;
	
// ***************************************

	@FXML
	private TableView<SavedBookingDataTableBean> tableSavedBookingData;

	@FXML
	private TableColumn<SavedBookingDataTableBean, Integer> savedBooking_tabColumn_slno;

	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_Awb;
	
	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_ForwarderNo;
	
	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_BookingDate;
	
	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_Service;
	
	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_Network;
	
	@FXML
	private TableColumn<SavedBookingDataTableBean, String> savedBooking_tabColumn_ForwarderAccount;
	

// ***************************************

	@FXML
	private TableView<UnSavedBookingDataTableBean> tableUnSavedBookingData;

	@FXML
	private TableColumn<UnSavedBookingDataTableBean, Integer> unSavedBooking_tabColumn_slno;

	@FXML
	private TableColumn<UnSavedBookingDataTableBean, String> unSavedBooking_tabColumn_Awb;	
	
	
// ******************************************************************************	
	
	Main m=new Main();
	
// ******************************************************************************

	public void showOutwardPage() throws IOException
	{
		m.showOutward();
	}
			
// ******************************************************************************

	public void showCloudInward() throws IOException
	{
		m.showCloudInward();		
	}
		
// ******************************************************************************

	public void showDataEntry() throws IOException
	{
		m.showDataEntry();
	}

// ******************************************************************************

	public void showInward() throws IOException
	{
		m.showInward();
	}	
	
// ******************************************************************************
	
	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
			//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));
		
    	selectedFile = fc.showOpenDialog(null);
    	
    	if(selectedFile != null)
    	{
    		a=5*1024*1024;
    		size=selectedFile.length();	
    		
    	   	if(size<a)
    	   	{
    	   		txtBrowseExcel.getText();
    	   		txtBrowseExcel.setText(selectedFile.getAbsolutePath());
    	   		btnSave.requestFocus();
    	   	}
    	   	else
    	   	{
    	   		txtBrowseExcel.clear();
    	   	}
    	}
    	else
    	{
    		size=0;
    	}
	}	
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException 
	{
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("rdBtnAdd")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				rdBtnUpdate.requestFocus();
			}
		}

		else if (n.getId().equals("rdBtnUpdate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnBrowse.requestFocus();
			}
		}

		else if (n.getId().equals("btnBrowse")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				fileAttachment();
				btnSave.requestFocus();
			}
		}


		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}

	}	
	
// ******************************************************************************	
	
	public void setValidation() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		
		if (txtBrowseExcel.getText() == null || txtBrowseExcel.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please select the file");
			alert.showAndWait();
			btnBrowse.requestFocus();
		}
		else
		{
			progressBarTest();
		}

	}		
			
	
// ******************************************************************************	

	public void progressBarTest() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Processing.......");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);

		taskUpdateStage.setScene(new Scene(updatePane));
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				save();

				int max = 1000;
				for (int i = 1; i <= max; i++) 
				{
					// Thread.sleep(100);
					/*if (BirtReportExportCon.FLAG == true) {
	                                   break;
	                            }*/
				}
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			public void handle(WorkerStateEvent t) {
				taskUpdateStage.close();
				
				if(tabledata_UnSavedBookingData.isEmpty()==false)
				{
					pagination_UnSaved.setPageCount((tabledata_UnSavedBookingData.size() / rowsPerPage() + 1));
					pagination_UnSaved.setCurrentPageIndex(0);
					pagination_UnSaved.setPageFactory((Integer pageIndex) -> createPage_UnSavedBookingData(pageIndex));
					
					pagination_UnSaved.setVisible(true);
					btnExportToExcel_UnSaved.setVisible(true);
				}
				else
				{
					pagination_UnSaved.setVisible(false);
					btnExportToExcel_UnSaved.setVisible(false);
				}
				
				if(tabledata_SavedBookingData.isEmpty()==false)
				{
					pagination_Saved.setPageCount((tabledata_SavedBookingData.size() / rowsPerPage() + 1));
					pagination_Saved.setCurrentPageIndex(0);
					pagination_Saved.setPageFactory((Integer pageIndex) -> createPage_SavedBookingData(pageIndex));
					
					pagination_Saved.setVisible(true);
					btnExportToExcel_Saved.setVisible(true);
				}
				else
				{
					pagination_Saved.setVisible(false);
					btnExportToExcel_Saved.setVisible(false);
				}
				
				if (numberOfEmptySheets==totalSheets)
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty File");
					//alert.setTitle("Empty Field Validation");
					alert.setContentText("Selected Excel File Is Empty...");
					alert.showAndWait();
					numberOfEmptySheets=0;
					totalSheets=0;

				}
				else
				{
					
					//checkUpdateCounts(updateResult);
					
					if(tabledata_SavedBookingData.isEmpty()==true)
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Task Complete");
						//alert.setTitle("Empty Field Validation");
						alert.setContentText("Incomplete Enteries...\nNo Data Added/Modified");
						alert.showAndWait();
					}
					else if(tabledata_SavedBookingData.isEmpty()==false && tabledata_UnSavedBookingData.isEmpty()==false)
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Task Complete");
						//alert.setTitle("Empty Field Validation");
						alert.setContentText("Booking Data Successfully Uploaded...\nExcept Some Enteries...!! Please Check");
						alert.showAndWait();
					}
					else if(tabledata_SavedBookingData.isEmpty()==false && tabledata_UnSavedBookingData.isEmpty()==true)
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Task Complete");
						//alert.setTitle("Empty Field Validation");
						alert.setContentText("Booking Data Successfully Uploaded...");
						alert.showAndWait();
					}
					
					numberOfEmptySheets=0;
					totalSheets=0;
				}
				
			}
		});

		
		

		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();

	}
	
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
			//importExcelFile(selectedFile);
	}
		
// ******************************************************************************	
		
	public void importNewExcel(File selectedFile) throws IOException
	{
		int index=0;
		list_BookingRecords.clear();
	
		try
		{
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			System.out.println("File Name: "+fileName);

			//System.out.println("input stream : "+ fileInputStream.toString());
			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			
			totalSheets=workbook.getNumberOfSheets();
			
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
				
				HSSFRow row;
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);
						
						Iterator<Cell> cellIterator = row.cellIterator();
						DataEntryBean deBean=new DataEntryBean();
						
						try
						{
							
							if(!String.valueOf((long)row.getCell(0).getNumericCellValue()).equals("0"))
							{
								
								if(row.getCell(0).getCellType()==Cell.CELL_TYPE_NUMERIC)
								{
									deBean.setMasterAwbNo(String.valueOf((long)row.getCell(0).getNumericCellValue()));
								}
								else
								{
									deBean.setMasterAwbNo(row.getCell(0).getStringCellValue());
								}
	
								if(row.getCell(1).getCellType()==Cell.CELL_TYPE_NUMERIC)
								{
									deBean.setForwarderNo(String.valueOf((long)row.getCell(1).getNumericCellValue()));
								}
								else
								{
									deBean.setForwarderNo(row.getCell(1).toString());
								}
	
								if(row.getCell(2).toString().isEmpty()==true || row.getCell(2)==null)
								{
									deBean.setBookingDate(null);
								}
								else
								{
									deBean.setBookingDate(row.getCell(2).toString());
								}
	
								if(row.getCell(3).getCellType()==Cell.CELL_TYPE_NUMERIC)
								{
									deBean.setClient(String.valueOf((long)row.getCell(3).getNumericCellValue()));
								}
								else
								{
									deBean.setClient(row.getCell(3).toString());
								}
	
								if(row.getCell(4).toString().isEmpty()==true || row.getCell(4)==null)
								{
									deBean.setDestination(null);
								}
								else
								{
									deBean.setDestination(row.getCell(4).toString());
								}
	
								if(row.getCell(5).toString().isEmpty()==true || row.getCell(5)==null)
								{
									deBean.setConsigNEE_Name(null);
								}
								else
								{
									deBean.setConsigNEE_Name(row.getCell(5).toString());
								}
	
								if(row.getCell(9).toString().isEmpty()==true || row.getCell(9)==null)
								{
									deBean.setOriginPincode(null);
								}
								else
								{
									deBean.setOriginPincode(row.getCell(9).toString().substring(0, 6));
								}
	
								index++;
	
								if(row.getCell(10).toString().isEmpty()==true || row.getCell(10)==null)
								{	
									deBean.setPincode(null);
								}
								else
								{
									deBean.setPincode(row.getCell(10).toString().substring(0, 6));
								}
	
								if(row.getCell(12).toString().isEmpty()==true || row.getCell(12)==null)
								{
									deBean.setBillWeight(0);	
								}
								else
								{
									deBean.setBillWeight(Double.valueOf(row.getCell(12).getNumericCellValue()));
								}
	
								if(row.getCell(13).toString().isEmpty()==true || row.getCell(13)==null)
								{
									deBean.setActualWeight(0);
								}
								else
								{
									deBean.setActualWeight(Double.valueOf(row.getCell(13).getNumericCellValue()));
								}
	
	
								if(row.getCell(14).toString().isEmpty()==true || row.getCell(14)==null)
								{
									deBean.setDox_nondox(null);
								}
								else
								{
									deBean.setDox_nondox(row.getCell(14).getStringCellValue());
								}
	
								if(row.getCell(15).toString().isEmpty()==true || row.getCell(15)==null)
								{
									deBean.setPcs(0);
								}
								else
								{
									deBean.setPcs((int) row.getCell(15).getNumericCellValue());
								}
	
								/*	if(row.getCell(16)==null)
									{
										System.out.println(" get service: if >> "+row.getCell(16));
										deBean.setService(null);
									}
									else
									{*/
								
								
								if(row.getCell(16).toString().isEmpty()==true || row.getCell(16)==null)
								{
									deBean.setService(null);
								}
								else
								{
									deBean.setService(row.getCell(16).getStringCellValue());
								}
								
								//deBean.setService(row.getCell(16).getStringCellValue());
								//System.out.println("AWB no :: "+deBean.getMasterAwbNo()+" | Service ::::::::::::::::::::::::::::::::::::::: "+deBean.getService());
								
								/*if(deBean.getService()==null)
								{
									
									break;
								}*/
								//	System.out.println(" get  >> service: else >> "+deBean.getService());
								//}
								if(row.getCell(17).toString().isEmpty()==true || row.getCell(17)==null)
								{
									deBean.setNetwork(null);
								}
								else
								{
									deBean.setNetwork(row.getCell(17).getStringCellValue());
								}
	
								if(row.getCell(18).toString().isEmpty()==true || row.getCell(18)==null)
								{
									deBean.setInvoiceAmount(0);
								}
								else
								{
									deBean.setInvoiceAmount(Double.valueOf(row.getCell(18).getNumericCellValue()));
								}
	
								if(row.getCell(19).toString().isEmpty()==true || row.getCell(19)==null)
								{
									deBean.setForwarderAccount(null);
								}
								else
								{
									if(row.getCell(19).getCellType()==Cell.CELL_TYPE_NUMERIC)
									{
										deBean.setForwarderAccount(String.valueOf((long)row.getCell(19).getNumericCellValue()));
									}
									else
									{
										deBean.setForwarderAccount(row.getCell(19).toString());
									}	
								}
	
								//System.out.println("Dimension >>>   >"+String.valueOf(row.getCell(20))+"<");
								
								if(String.valueOf(row.getCell(20)).isEmpty()==true || row.getCell(20)==null)
								{
									deBean.setLength_dimension(0);
								}
								else
								{
									deBean.setLength_dimension(row.getCell(20).getNumericCellValue());
								}
	
								if(String.valueOf(row.getCell(21)).isEmpty()==true || row.getCell(21)==null)
								{
									deBean.setWidth_dimension(0);
								}
								else
								{
									deBean.setWidth_dimension(row.getCell(21).getNumericCellValue());
								}
	
								if(String.valueOf(row.getCell(22)).isEmpty()==true || row.getCell(22)==null)
								{
									deBean.setHeight_dimension(0);
								}
								else
								{
									deBean.setHeight_dimension(row.getCell(22).getNumericCellValue());
								}
	
								//System.out.println("D1 :: >"+row.getCell(20)+"< | D2 :: >"+row.getCell(21)+"< | D3 :: >"+row.getCell(22));
								//System.out.println("D1 :: "+deBean.getLength_dimension()+" | D2 :: "+deBean.getWidth_dimension()+" | D3 :: "+deBean.getHeight_dimension());
	
								/*
								deBean.setLength_dimension(row.getCell(20).getNumericCellValue());
								deBean.setWidth_dimension(row.getCell(21).getNumericCellValue());
								deBean.setHeight_dimension(row.getCell(22).getNumericCellValue());
	
								System.out.println("D1 :: >"+row.getCell(20)+"< | D2 :: >"+row.getCell(21)+"< | D3 :: >"+row.getCell(22));
								System.out.println("D1 :: "+row.getCell(20).getNumericCellValue()+" | D2 :: "+row.getCell(21).getNumericCellValue()+" | D3 :: "+row.getCell(22).getNumericCellValue());
	
								deBean.setLength_dimension(Double.valueOf(row.getCell(20).toString()));
								deBean.setWidth_dimension(Double.valueOf(row.getCell(21).toString()));
								deBean.setHeight_dimension(Double.valueOf(row.getCell(22).toString()));*/
	
								deBean.setDimension(deBean.getLength_dimension()+"*"+deBean.getWidth_dimension()+"*"+deBean.getHeight_dimension());
								
								if(!set_CheckDuplicateAwbNo.contains(deBean.getMasterAwbNo()))
								{
									set_CheckDuplicateAwbNo.add(deBean.getMasterAwbNo());
									list_BookingRecords.add(deBean);	
								}
								else
								{
									set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(deBean.getMasterAwbNo());
								}
								
							}
						}
						catch(Exception e)
						{
							list_FormatMisMatch.add(String.valueOf(row.getCell(0)));
						}
					}
				}
				else
				{
					numberOfEmptySheets++;
				}
			}
			
			System.out.println("Sheet count >> "+totalSheets);
			System.out.println("Empty Sheet count >> "+numberOfEmptySheets);
			
		//	System.out.println(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_BookingRecords.size());
			//System.out.println(" format mismatch list+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_FormatMisMatch.size());
			
			/*for(String num:list_FormatMisMatch)
			{
				System.out.println("Mis Match >> "+num);
			}
			*/
			if(rdBtnAdd.isSelected()==true)
			{
				saveBookingData();
			}
			else
			{
				
				updateBookingData();
			}

			//saveBookingData();
		//	checkClientRateSlabDetails();
			//saveClientRateSlabDetails();
			fileInputStream.close();
		}

		catch(final Exception e)
		{
			e.printStackTrace();
		}

	}
	
// **********************************************************************************	
	
	public void checkAwbNoForInsert() throws SQLException
	{
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;
		
		try 
		{
			for(DataEntryBean deBean:list_AwbNoToStore)
			{
				list_AwbnoNotExistInDB.add(deBean.getMasterAwbNo());
				
				/*String sql = MasterSQL_ImportBookingData_Excel_Utils.CHECK_AWBNO_FOR_INSERT;
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,deBean.getMasterAwbNo());
			
				rs = preparedStmt.executeQuery();
				
				if (!rs.next()) 
				{
					list_AwbnoNotExistInDB.add(deBean.getMasterAwbNo());
				} 
				else
				{
					set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(deBean.getMasterAwbNo());
				}*/
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}
	
	
// **********************************************************************************	
	
	public void checkAwbNoForInsertInConsignorDetails() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		ResultSet rs = null;
		PreparedStatement preparedStmt = null;
		
		try 
		{
			for(DataEntryBean deBean:list_AwbNoToStore)
			{
				String sql = MasterSQL_ImportBookingData_Excel_Utils.CHECK_AWBNO_FOR_INSERT_IN_CONSIGNORDETAIL;
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,deBean.getMasterAwbNo());
				
				rs = preparedStmt.executeQuery();
				
				if (!rs.next()) 
				{
					list_AwbnoNotExistInDB_for_ConsignerConsignee.add(deBean.getMasterAwbNo());
				}
				else
				{
					set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.add(deBean.getMasterAwbNo());
				}
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}

	}
	

	
// **********************************************************************************	
	
	public void saveBookingData() throws SQLException
	{
		int batchSize=50;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		PreparedStatement preparedStmt = null;
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_BookingRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		try 
		{
			checkService_NetworkIsAvailable();
			
			checkAwbNoForInsert();
		
			String query = MasterSQL_ImportBookingData_Excel_Utils.INSERT_SQL_BOOKING_DATA;

			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(DataEntryBean deBean: list_BookingRecords)
			{
				if(list_AwbnoNotExistInDB.contains(deBean.getMasterAwbNo()))
				{
					//System.err.println("from insert Booking Date >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+deBean.getBookingDate());
					
					String df1=DateValidator.formatDate1(deBean.getBookingDate());
					if(df1==null)
					df1 =DateValidator.formatDate3(deBean.getBookingDate());
					
					//System.out.println("Date >>>>>>>>>>>>> "+df1);
					
					/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					LocalDate localDate = LocalDate.parse(deBean.getBookingDate(), formatter);*/
					
					preparedStmt.setString(1, deBean.getMasterAwbNo());
					preparedStmt.setString(2, deBean.getMasterAwbNo());
					preparedStmt.setString(3, deBean.getForwarderNo());
					preparedStmt.setString(4, "T");
					preparedStmt.setString(5, "T");
					//preparedStmt.setDate(6, Date.valueOf("2017-12-05"));
					preparedStmt.setDate(6, Date.valueOf(df1));
					preparedStmt.setString(7, deBean.getClient());
					preparedStmt.setString(8, deBean.getOriginPincode());
					preparedStmt.setString(9, deBean.getDestination());
					preparedStmt.setInt(10, Integer.valueOf(deBean.getPincode()));
					preparedStmt.setString(11, deBean.getNetwork());
					preparedStmt.setString(12, deBean.getService());
					
					/*if(LoadNetworks.SET_LOAD_NETWORK_WITH_NAME.size()==0)
					{
						new LoadNetworks().loadAllNetworksWithName();
					}
					for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
					{
						if(deBean.getNetwork().equals(bean.getNetworkName()))
						{
							preparedStmt.setString(11, bean.getNetworkCode());
							break;
						}
					}

					if(LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size()==0)
					{
						new LoadServiceGroups().loadServicesFromServiceType();
					}
					for(LoadServiceWithNetworkBean srvcBean: LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
					{
						if(deBean.getService().equals(srvcBean.getServiceName()))
						{
							preparedStmt.setString(12, srvcBean.getServiceCode());
							break;
						}
					}*/

					preparedStmt.setString(13, deBean.getDox_nondox());
					preparedStmt.setDouble(14, deBean.getPcs());
					preparedStmt.setString(15, deBean.getDimension());
					preparedStmt.setDouble(16, deBean.getActualWeight());
					preparedStmt.setDouble(17, deBean.getBillWeight());
					preparedStmt.setDouble(18, deBean.getInvoiceAmount());
					preparedStmt.setString(19, "dataentry");
					preparedStmt.setString(20, "MMB");
					preparedStmt.setString(21, "1");
					preparedStmt.setString(22, deBean.getForwarderAccount());
					preparedStmt.setString(23, CommonVariable.USER_BRANCHCODE);
					preparedStmt.setString(24, "F");
					
					preparedStmt.addBatch();
					
					count++;
					
					if(count % batchSize == 0)
					{
						System.out.println(" Batch slot updated........................");
						preparedStmt.executeBatch();
						con.commit();	
					}
					
				/*	if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}*/
					
					list_AwbnoNotExistInDB_for_ConsignerConsignee.add(deBean.getMasterAwbNo());
					
					

					if(deBean.getLength_dimension()!=0 && deBean.getWidth_dimension()!=0 && deBean.getHeight_dimension()!=0)
					{

						DimensionBean diBean=new DimensionBean();
						diBean.setAwbno(deBean.getMasterAwbNo());
						diBean.setPcs(deBean.getPcs());
						diBean.setLength(deBean.getLength_dimension());
						diBean.setWidth(deBean.getWidth_dimension());
						diBean.setHeight(deBean.getHeight_dimension());

						list_AwbNoWithAvailableDimensions.add(diBean);
					}
				}
				else
				{
					set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(deBean.getMasterAwbNo());
				}
			}
			
			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows inserted in DailyBookingTransaction Table: "+ result.length);
			con.commit();
			
			//checkAwbNoForInsertInConsignorDetails();
			
			
			saveConsigneeConsignorDetails();
			saveDimensionDetails();
			
			System.out.println("Daily Booking list size: "+set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.size());
			System.out.println("Consignor list size: "+set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.size());
			
		/*	for(String awb:set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking)
			{
				System.out.println("AWB from dailybooking: >> "+awb);
			}

			for(String awb:set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update)
			{
				System.out.println("AWB from Consignor: >> "+awb);
			}*/
			
			loadSavedBookingDataTable();
			loadUnSavedBookingDataTable();
				
			list_AwbnoNotExistInDB.clear();
			list_AwbnoNotExistInDB_for_ConsignerConsignee.clear();
			list_AwbNoToStore.clear();
			list_BookingRecords.clear();
			list_AwbNoWithAvailableDimensions.clear();
			list_AwbNoToUpdate.clear();
				
		} 
		catch (SQLException e) 
		{
			con.rollback();
		//	SQLException se = null;
			
			if(e!=null)
			{
			e.getNextException();
			}
			System.out.println(e);
			e.printStackTrace();
		} 
		
		
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}
	

// ****************************************************************
	
	public void checkService_NetworkIsAvailable() throws SQLException
	{
		
		for(DataEntryBean deBean:list_BookingRecords)
		{
			int result=0;
			
			boolean isServiceExist=false;
			boolean isNetworkExist=false;
			boolean isForwarderCodeExist=false;
			boolean isClientCodeExist=false;
			boolean isPincodeContainsAllNumeric=false;
			boolean isOriginPincodeContainsAllNumeric=false;
			boolean isBookingDateAvaliable=false;
			
			
			
			if(LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size()==0)
			{
				new LoadServiceGroups().loadServicesFromServiceType();
			}
			
			if(deBean.getService()!=null)
			{
				for(LoadServiceWithNetworkBean srvcBean: LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
				{
					//System.out.println("Services 1 >>> "+srvcBean.getServiceCode());
					//System.out.println("Services 2 >>> "+deBean.getService());
					if(deBean.getService().equals(srvcBean.getServiceCode()))
					{
						isServiceExist=true;
						break;
						
					}
					else
					{
						isServiceExist=false;
					}
				}
			}
			
			
				
			if(LoadNetworks.SET_LOAD_NETWORK_WITH_NAME.size()==0)
			{
				new LoadNetworks().loadAllNetworksWithName();
			}
			
			if(deBean.getNetwork()!=null)
			{
				for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
				{
					if(deBean.getNetwork().equals(bean.getNetworkCode()))
					{
						isNetworkExist=true;
						break;
						
					}
					else
					{
						isNetworkExist=false;
					}
				}
			}
			
			
			
			if(deBean.getBookingDate()==null)
			{
				isBookingDateAvaliable=false;
			}
			else
			{
				isBookingDateAvaliable=true;
			}
			
			
			if(LoadForwarderDetails.LIST_FORWARDER_DETAILS.size()==0)
			{
				new LoadForwarderDetails().loadForwarderCodeWithName();
			}
			
			
			if(deBean.getForwarderAccount()!=null)
			{
				for(LoadForwarderDetailsBean bean:LoadForwarderDetails.LIST_FORWARDER_DETAILS)
				{
					if(deBean.getForwarderAccount().equals(bean.getForwarderCode()))
					{
						isForwarderCodeExist=true;
						break;
						
					}
					else
					{
						isForwarderCodeExist=false;
					}
				}
			}
			
			
			if(LoadClients.SET_LOAD_CLIENTWITHNAME.size()==0)
			{	
				new LoadClients().loadClientWithName();
			}
			
			for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
			{
				if(deBean.getClient().equals(bean.getClientCode()))
				{
					isClientCodeExist=true;
					break;
					
				}
				else
				{
					isClientCodeExist=false;
				}
			}
			
			Pattern pattern = Pattern.compile("[0-9]+");
		 	Matcher matchePincode;
		 	Matcher matcheOriginPincode;
			
		 	//System.out.println("Origin pincode :::: "+deBean.getOriginPincode() +" | Awb No: "+deBean.getAwbNo());
		 	//System.out.println("Destination pincode :::: "+deBean.getPincode() +" | Awb No: "+deBean.getAwbNo());
		 	
			if(deBean.getOriginPincode()!=null)
			{
				//Pattern pattern = Pattern.compile("[0-9]+");
			 	//matchePincode = pattern.matcher(deBean.getPincode());
			 	matcheOriginPincode = pattern.matcher(deBean.getOriginPincode());
			
			 	if(matcheOriginPincode.matches())
				{
					isOriginPincodeContainsAllNumeric=true;
				}
				else
				{
					isOriginPincodeContainsAllNumeric=false;
				}
			  
			}
			  	
			if(deBean.getPincode()!=null)
			{
				matchePincode = pattern.matcher(deBean.getPincode());
				
				if(matchePincode.matches())
				{
				  	isPincodeContainsAllNumeric=true;
				}
				else
				{
					isPincodeContainsAllNumeric=false;
				}	
			 	
			}
			/*else
			{
				
			}*/
			
			/*if(isNetworkExist==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | network: >> "+deBean.getNetwork());
			}
			
			if(isServiceExist==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | Service: >> "+deBean.getService());
			}
			
			if(isForwarderCodeExist==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | Forwarder A/c: >> "+deBean.getForwarderAccount());
			}
			
			if(isClientCodeExist==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | Client Code: >> "+deBean.getClient());
			}
			
			if(isPincodeContainsAllNumeric==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | Pincode Code: >> "+deBean.getPincode());
			}
			
			if(isOriginPincodeContainsAllNumeric==false)
			{
				System.out.println("Master >> "+deBean.getMasterAwbNo()+" | Origin Pincode Code: >> "+deBean.getOriginPincode());
			}*/
			

			if(isServiceExist==true && isNetworkExist==true && isForwarderCodeExist==true && isClientCodeExist==true && isPincodeContainsAllNumeric==true && isOriginPincodeContainsAllNumeric==true && isBookingDateAvaliable==true)
			{
				result=1;
			}
			else
			{
				result=0;
			}
			
			//System.out.println("Result >>>>>>>>>> "+result);
			
			if(result==1)
			{
				
				
				list_AwbNoToStore.add(deBean);
				list_AwbNoToUpdate.add(deBean.getMasterAwbNo());
			}
			else
			{
				set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(deBean.getMasterAwbNo());
			}
		}
		
	}

	
// ****************************************************************	
	
	public void saveConsigneeConsignorDetails() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		int batchSize=50;
		
		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int count=0;
		
		try 
		{
			String query = MasterSQL_ImportBookingData_Excel_Utils.INSERT_SQL_CONSIGNEE_CONSIGNOR_DETAILS;
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			if(list_BookingRecords.size()>200)
			{
				isBulkDataAvailable=true;
			}
			else
			{
				isBulkDataAvailable=false;
			}
			
			for(DataEntryBean deBean:list_BookingRecords)
			{
				if(list_AwbnoNotExistInDB_for_ConsignerConsignee.contains(deBean.getMasterAwbNo()))
				{
					preparedStmt.setString(1, deBean.getMasterAwbNo());
					preparedStmt.setString(2, deBean.getConsigNEE_Name());
					preparedStmt.addBatch();
					
					count++;
					
					
					if(count % batchSize == 0)
					{
						System.out.println("Batch slot updated........................");
						preparedStmt.executeBatch();
						con.commit();	
					}
					/*if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}*/
					
				}
				else
				{
					set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.add(deBean.getMasterAwbNo());
				}
			}
			
			 int[] result = preparedStmt.executeBatch();
			 
			 System.out.println("The number of rows inserted in Consignor Table: "+ result.length);
			 con.commit();
		
		} 

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}	

	
// ****************************************************************	
	
	public void saveDimensionDetails() throws SQLException 
	{
		int batchSize=50;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_AwbNoWithAvailableDimensions.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		
		try 
		{		
			String query = "insert into dimensions(awbno,pcs,weight,length,height,width,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
			
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(DimensionBean diBean: list_AwbNoWithAvailableDimensions)
			{	
				double weight=0;
				weight=Double.valueOf(df.format((diBean.getLength()*diBean.getWidth()*diBean.getHeight())/2000));
					
				preparedStmt.setString(1, diBean.getAwbno());
				preparedStmt.setInt(2, diBean.getPcs());
				preparedStmt.setDouble(3, weight);
				preparedStmt.setDouble(4, diBean.getWidth());
				preparedStmt.setDouble(5, diBean.getLength());
				preparedStmt.setDouble(6, diBean.getHeight());
				
				preparedStmt.addBatch();
				
				count++;
				
				if(count % batchSize == 0)
				{
					System.out.println("Batch slot updated........................");
					preparedStmt.executeBatch();
					con.commit();	
				}
				
				/*if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
				}*/
				
			}
			
			int[] result = preparedStmt.executeBatch();
			 
			 System.out.println("The number of rows inserted in Dimension Table: "+ result.length);
			 con.commit();
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
// ****************************************************************		
	
	public void updateDimensionDetails() throws SQLException 
	{
		int batchSize=50;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_AwbNoWithAvailableDimensions.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		 
		try 
		{		
			String query = "update dimensions set pcs=?,weight=?,length=?,height=?,width=?,lastmodifieddate=CURRENT_TIMESTAMP "
							+ "where awbno=?";
			
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(DimensionBean diBean: list_AwbNoWithAvailableDimensions)
			{	
				double weight=0;
				weight=Double.valueOf(df.format((diBean.getLength()*diBean.getWidth()*diBean.getHeight())/2000));
					
				preparedStmt.setInt(1, diBean.getPcs());
				preparedStmt.setDouble(2, weight);
				preparedStmt.setDouble(3, diBean.getWidth());
				preparedStmt.setDouble(4, diBean.getLength());
				preparedStmt.setDouble(5, diBean.getHeight());
				preparedStmt.setString(6, diBean.getAwbno());
				
				preparedStmt.addBatch();
				
				count++;
				
				if(count % batchSize == 0)
				{
					System.out.println("Batch slot updated........................");
					preparedStmt.executeBatch();
					con.commit();	
				}
				/*if(isBulkDataAvailable==true)
				{
					new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
				}*/
				
			}
			
			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows updated in Dimension Table: "+ result.length);
			con.commit();
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
	
// ****************************************************************	
	
	public void updateBookingData() throws SQLException
	{
		
		int batchSize=50;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_BookingRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
	
		try 
		{
			checkService_NetworkIsAvailable();
			
			String query = MasterSQL_ImportBookingData_Excel_Utils.UPDATE_SQL_BOOKING_DATA;
			
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(DataEntryBean deBean:list_BookingRecords)
			{	
				if(list_AwbNoToUpdate.contains(deBean.getMasterAwbNo()))
				{	
					
				//	System.err.println(" Booking Date >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+deBean.getBookingDate());
					/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					LocalDate localDate = LocalDate.parse(deBean.getBookingDate(), formatter);*/
					
					String df1=DateValidator.formatDate1(deBean.getBookingDate());
					if(df1==null)
						df1 =DateValidator.formatDate3(deBean.getBookingDate());
			      
					preparedStmt.setString(1, deBean.getForwarderNo());
					preparedStmt.setDate(2,Date.valueOf(df1));
					preparedStmt.setString(3, deBean.getClient());
					preparedStmt.setString(4, deBean.getOriginPincode());
					preparedStmt.setString(5, deBean.getDestination());
					preparedStmt.setInt(6, Integer.valueOf(deBean.getPincode()));
					preparedStmt.setString(7, deBean.getNetwork());
					preparedStmt.setString(8, deBean.getService());
					
					/*if(LoadNetworks.SET_LOAD_NETWORK_WITH_NAME.size()==0)
					{
						new LoadNetworks().loadAllNetworksWithName();
					}
					for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
					{
						if(deBean.getNetwork().equals(bean.getNetworkName()))
						{
							preparedStmt.setString(7, bean.getNetworkCode());
							break;
						}
					}
					
					if(LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size()==0)
					{
						new LoadServiceGroups().loadServicesFromServiceType();
					}
					for(LoadServiceWithNetworkBean srvcBean: LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
					{
						if(deBean.getService().equals(srvcBean.getServiceName()))
						{
							preparedStmt.setString(8, srvcBean.getServiceCode());
							break;
						}
					}*/
					
					preparedStmt.setString(9, deBean.getDox_nondox());
					preparedStmt.setDouble(10, deBean.getPcs());
					preparedStmt.setString(11, deBean.getDimension());
					preparedStmt.setDouble(12, deBean.getActualWeight());
					preparedStmt.setDouble(13, deBean.getBillWeight());
					preparedStmt.setDouble(14, deBean.getInvoiceAmount());
					preparedStmt.setString(15, deBean.getForwarderAccount());
					preparedStmt.setString(16, CommonVariable.USER_BRANCHCODE);
					preparedStmt.setString(17, deBean.getMasterAwbNo());
					
					preparedStmt.addBatch();
					
					count++;
					
					if(count % batchSize == 0)
					{
						System.out.println("Update Method Batch slot updated........................");
						preparedStmt.executeBatch();
						con.commit();	
					}
					/*if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}*/
					
					if(deBean.getLength_dimension()!=0 && deBean.getWidth_dimension()!=0 && deBean.getHeight_dimension()!=0)
					{
						//System.out.println("Awb No: "+deBean.getMasterAwbNo()+" >> Dimensions: L :: "+deBean.getLength_dimension()+" | Wd :: "+deBean.getWidth_dimension()+" | H :: "+deBean.getHeight_dimension());

						DimensionBean diBean=new DimensionBean();
						diBean.setAwbno(deBean.getMasterAwbNo());
						diBean.setPcs(deBean.getPcs());
						diBean.setLength(deBean.getLength_dimension());
						diBean.setWidth(deBean.getWidth_dimension());
						diBean.setHeight(deBean.getHeight_dimension());

						list_AwbNoWithAvailableDimensions.add(diBean);
					}

				}
				else
					
				{
					set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.add(deBean.getMasterAwbNo());
				}
			}
			
			
			//int[] result = preparedStmt.executeBatch();
			updateResult = preparedStmt.executeBatch();
			System.out.println("The number of rows updated in table dailybookingtransaction: "+ updateResult.length);
			con.commit();
			
			
			System.out.println(preparedStmt.getMetaData());
			
			checkUpdateCounts(updateResult);

			updateConsigneeConsignorDetails();
			updateDimensionDetails();
			System.out.println("Size >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+list_AwbNoToUpdate.size());
			
			System.out.println("Daily Booking list size: "+set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.size());
			System.out.println("Consignor list size: "+set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.size());
			
		/*	for(String awb:set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking)
			{
				System.out.println("AWB from dailybooking: >> "+awb);
			}

			for(String awb:set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update)
			{
				System.out.println("AWB from Consignor: >> "+awb);
			}*/

			loadSavedBookingDataTable();
			loadUnSavedBookingDataTable();
			
			list_AwbnoNotExistInDB.clear();
			list_AwbnoNotExistInDB_for_ConsignerConsignee.clear();
			list_AwbNoToStore.clear();
			list_BookingRecords.clear();
			list_AwbNoWithAvailableDimensions.clear();
			list_AwbNoToUpdate.clear();
			
		} 
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
	}

	
	  public  void checkUpdateCounts(int[] updateCounts) {
		
		  int update_add_rows=0;
		  
		    for (int i = 0; i < updateCounts.length; i++) {
		      if (updateCounts[i] >= 0) {
		        // Successfully executed; the number represents number of affected rows
		        System.out.println("OK: >> 1 << updateCount=" + updateCounts[i]);
		        update_add_rows=update_add_rows+updateCounts[i];
		      } else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
		        // Successfully executed; number of affected rows not available
		        System.out.println("OK: >> 2 << updateCount=Statement.SUCCESS_NO_INFO");
		      } else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
		        System.out.println(">> 3 <<  updateCount=Statement.EXECUTE_FAILED");
		      }
		    }
		  }
	
	
// ****************************************************************	
	
	public void updateConsigneeConsignorDetails() throws SQLException
	{
		int batchSize=50;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		boolean isBulkDataAvailable=false;
		int count=0;
		
		if(list_BookingRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
	
		
		try 
		{
			String query =MasterSQL_ImportBookingData_Excel_Utils.UPDATE_SQL_CONSIGNEE_CONSIGNOR_DETAILS;
			preparedStmt = con.prepareStatement(query);
			con.setAutoCommit(false);
			
			for(DataEntryBean deBean:list_BookingRecords)
			{
				if(list_AwbNoToUpdate.contains(deBean.getMasterAwbNo()))
				{
					preparedStmt.setString(1, deBean.getConsigNEE_Name());
					preparedStmt.setString(2, deBean.getMasterAwbNo());
					preparedStmt.addBatch();
					
					count++;
					
					if(count % batchSize == 0)
					{
						System.out.println("Update Method Batch slot updated........................");
						preparedStmt.executeBatch();
						con.commit();	
					}
					
				/*	if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_PrepStmt(preparedStmt, con, count);
					}*/
					
					
				}
				else
				{
					set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.add(deBean.getMasterAwbNo());
				}
			}	
			
			int[] result = preparedStmt.executeBatch();
			System.out.println("The number of rows Updated in consignor table: "+ result.length);
			con.commit();

		} 

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}	

// ****************************************************************	
	
	public void selectInsertOrUpdate() throws SQLException
	{
		if(rdBtnAdd.isSelected()==true)
		{
			set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.clear();
			set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.clear();
			btnSave.setText("Save");
		}
		else
		{
			set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.clear();
			set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.clear();
			
			btnSave.setText("Update");
		}
		
	}

// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	
	public int rowsPerPage() {
		return 50;
	}

	
	public GridPane createPage_UnSavedBookingData(int pageIndex) 
	{
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_UnSavedBookingData.size() % rowsPerPage();

		if (displace >= 0) 
		{
			lastIndex = tabledata_UnSavedBookingData.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) 
		{
			if (lastIndex == pageIndex) 
			{
				tableUnSavedBookingData.setItems(FXCollections.observableArrayList(tabledata_UnSavedBookingData
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} 
			else 
			{
				tableUnSavedBookingData.setItems(FXCollections.observableArrayList(tabledata_UnSavedBookingData
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}

	
	public GridPane createPage_SavedBookingData(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_SavedBookingData.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_SavedBookingData.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) 
		{
			if (lastIndex == pageIndex) 
			{
				tableSavedBookingData.setItems(FXCollections.observableArrayList(
						tabledata_SavedBookingData.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} 
			else 
			{
				tableSavedBookingData.setItems(FXCollections.observableArrayList(tabledata_SavedBookingData
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}	
		
	
	
	
// *************************************************************************

	/*public void loadSavedBookingDataTable() throws SQLException {

		tabledata_SavedBookingData.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		

		int slno = 1;
		try {


			stmt = con.createStatement();
			sql = MasterSQL_ImportBookingData_Excel_Utils.LOAD_TABLE_SAVED_BOOKING_DATA;
		
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				DataEntryBean deBean = new DataEntryBean();
				deBean.setSlno(slno);
				deBean.setAwbNo(rs.getString("master_awbno"));
				deBean.setService(rs.getString("dailybookingtransaction2service"));
				deBean.setNetwork(rs.getString("dailybookingtransaction2network"));
				deBean.setForwarderNo(rs.getString("forwarder_number"));
				deBean.setBookingDate(date.format(rs.getDate("booking_date")));
				deBean.setForwarderAccount(rs.getString("forwarder_account"));
				

				tabledata_SavedBookingData.add(new SavedBookingDataTableBean(slno, deBean.getAwbNo(), deBean.getForwarderNo(), 
						deBean.getBookingDate(), deBean.getService(), deBean.getNetwork(), deBean.getForwarderAccount()));

				slno++;
			}

			tableSavedBookingData.setItems(tabledata_SavedBookingData);

			
			//pagination_UnForwarded.setPageCount((tabledata_UnForwarded.size() / rowsPerPage() + 1));
			//pagination_UnForwarded.setCurrentPageIndex(0);
			//pagination_UnForwarded.setPageFactory((Integer pageIndex) -> createPage_UnForwarded(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}*/

// *************************************************************************	
	
	public void loadSavedBookingDataTable() throws SQLException 
	{
		tabledata_SavedBookingData.clear();
		
		System.out.println("Booking Record >> "+list_BookingRecords.size());
		
		int slno = 1;
		
		if(rdBtnAdd.isSelected()==true)
		{
			System.out.println("Insert >>>>>>>>>>>>>>");
			for(DataEntryBean deBean: list_BookingRecords)
			{
				if(list_AwbnoNotExistInDB.contains(deBean.getMasterAwbNo()))
				{
					tabledata_SavedBookingData.add(new SavedBookingDataTableBean(slno, deBean.getMasterAwbNo(), deBean.getForwarderNo(), 
							deBean.getBookingDate(), deBean.getService(), deBean.getNetwork(), deBean.getForwarderAccount()));
	
					slno++;
				}
			}
		}
		
		else
		{
			System.out.println("Update  <<==>>>>>>>>>>>>>>" +list_AwbNoToUpdate.size());
			for(DataEntryBean deBean: list_BookingRecords)
			{
				if(list_AwbNoToUpdate.contains(deBean.getMasterAwbNo()))
				{
					tabledata_SavedBookingData.add(new SavedBookingDataTableBean(slno, deBean.getMasterAwbNo(), deBean.getForwarderNo(), 
							deBean.getBookingDate(), deBean.getService(), deBean.getNetwork(), deBean.getForwarderAccount()));
	
					slno++;
				}
			}
		}
		
		tableSavedBookingData.setItems(tabledata_SavedBookingData);
		
	
	}

	
	
// *************************************************************************

	public void loadUnSavedBookingDataTable() throws SQLException {

		tabledata_UnSavedBookingData.clear();
		
		int slno=1;

		for(String awb:set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking)
		{
			tabledata_UnSavedBookingData.add(new UnSavedBookingDataTableBean(slno, awb));
			slno++;
		}
		
		tableUnSavedBookingData.setItems(tabledata_UnSavedBookingData);

		
		set_ConsignorDetail_UnsavedAwbNoAwbNo_ForInsert_AND_Update.clear();
		set_UnsavedAwbNoWhileInsert_AND_Update_InDailyBooking.clear();
		
	}
	
	
// ========================================================================================

	@FXML
	public void exportToExcel_SavedAwb() throws SQLException, IOException 
	{

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Scanned Awb");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Awb No");
		rowhead.createCell(2).setCellValue("Forwarder No");
		rowhead.createCell(3).setCellValue("Booking Date");
		rowhead.createCell(4).setCellValue("Service");
		rowhead.createCell(5).setCellValue("Network");
		rowhead.createCell(6).setCellValue("Forwarder Account");
		


		int i = 1;

		HSSFRow row;

		for (SavedBookingDataTableBean savedDataBean : tabledata_SavedBookingData) {
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(savedDataBean.getSlno());
			row.createCell(1).setCellValue(savedDataBean.getAwbno());
			row.createCell(2).setCellValue(savedDataBean.getForwarderNo());
			row.createCell(3).setCellValue(savedDataBean.getBookingDate());
			row.createCell(4).setCellValue(savedDataBean.getServiceCode());
			row.createCell(5).setCellValue(savedDataBean.getNetworkCode());
			row.createCell(6).setCellValue(savedDataBean.getForwarderAccount());

			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		// System.out.println("file chooser: "+file.getAbsoluteFile());

		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}	
	
	
// ========================================================================================

	@FXML
	public void exportToExcel_UnSavedAwb() throws SQLException, IOException 
	{

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Un Saved Awb No's");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Awb No");

		int i = 1;

		HSSFRow row;

		for (UnSavedBookingDataTableBean unSavedBookingBean : tabledata_UnSavedBookingData) 
		{
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(unSavedBookingBean.getSlno());
			row.createCell(1).setCellValue(unSavedBookingBean.getAwbno());
			
			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
	
		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}	

	
// ******************************************************************************	

	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		// TODO Auto-generated method stub
		
		pagination_Saved.setVisible(false);
		pagination_UnSaved.setVisible(false);
		btnExportToExcel_Saved.setVisible(false);
		btnExportToExcel_UnSaved.setVisible(false);
		
		savedBooking_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,Integer>("slno"));
		savedBooking_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("awbno"));
		savedBooking_tabColumn_ForwarderNo.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("forwarderNo"));
		savedBooking_tabColumn_BookingDate.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("bookingDate"));
		savedBooking_tabColumn_Service.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("serviceCode"));
		savedBooking_tabColumn_Network.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("networkCode"));
		savedBooking_tabColumn_ForwarderAccount.setCellValueFactory(new PropertyValueFactory<SavedBookingDataTableBean,String>("forwarderAccount"));
		
		
		unSavedBooking_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<UnSavedBookingDataTableBean,Integer>("slno"));
		unSavedBooking_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<UnSavedBookingDataTableBean,String>("awbno"));
		
		
	/*	try {
			loadSavedBookingDataTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}	

}
