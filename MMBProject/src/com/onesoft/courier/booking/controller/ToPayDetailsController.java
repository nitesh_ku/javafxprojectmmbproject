package com.onesoft.courier.booking.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.ToPayBean;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadCountry;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadState;
import com.onesoft.courier.common.LoadToPayData;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.forwarder.bean.ForwarderBean;
import com.onesoft.courier.master.client.bean.ClientBean;
import com.onesoft.courier.sql.queries.MasterSQL_Client_Utils;
import com.onesoft.courier.sql.queries.MasterSQL_ToPay_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ToPayDetailsController implements Initializable{
	
	private List<String> list_City=new ArrayList<>();
	private List<String> list_ToPayNames=new ArrayList<>();
	
	private ObservableList<String> comboBoxStateItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxCountryItems=FXCollections.observableArrayList();
	
	boolean isToPayAlreadyExist=false;
	
	/*@FXML
	private RadioButton rdBtnCreateNew;
	
	@FXML
	private RadioButton rdBtnExisting;
	
	@FXML
	private TextField txtToPayCode;*/
	
	@FXML
	private TextField txtToPayName;
	
	@FXML
	private TextField txtToPayPhone;
	
	@FXML
	private TextField txtToPayAddress;
	
	@FXML
	private TextField txtToPayPincode;
	
	@FXML
	private TextField txtToPayCity;
	
	@FXML
	private TextField txtToPayGSTIN;
	
	@FXML
	private TextField txtToPayEmail;
	
	@FXML
	private ComboBox<String> comboBoxState;
	
	@FXML
	private ComboBox<String> comboBoxCountry;
	
	@FXML
	private Button btnAddUpdate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Label labToPayCode;
	

// *******************************************************************************

	public void loadState() throws SQLException 
	{
		new LoadState().loadStateWithName();

		for (LoadCityBean bean : LoadState.SET_LOAD_STATEWITHNAME) 
		{
			comboBoxStateItems.add(bean.getStateCode());
		}
		comboBoxState.setItems(comboBoxStateItems);
			// TextFields.bindAutoCompletion(txtState, list_State);
	}

// *******************************************************************************

	public void loadCountry() throws SQLException {
		new LoadCountry().loadCountryWithName();

		for (LoadCityBean bean : LoadCountry.SET_LOAD_COUNTRYWITHNAME) 
		{
			comboBoxCountryItems.add(bean.getCountryCode());
		}
		comboBoxCountry.setItems(comboBoxCountryItems);
	}

// ******************************************************************************
	
	public void loadToPayNames() throws SQLException 
	{
		list_ToPayNames.clear();
		
		for (ToPayBean bean: LoadToPayData.LIST_GETTOPAYDATA) 
		{	
			list_ToPayNames.add(bean.getName());

		}
		TextFields.bindAutoCompletion(txtToPayName, list_ToPayNames);

	}
	
// ******************************************************************************

		public void loadCityFromPincode() throws SQLException 
		{
			new LoadCity().loadCityWithNameFromPinCodeMaster();;

			for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
			{
				list_City.add(bean.getCityName());
			
			}

			//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
			//TextFields.bindAutoCompletion(txtOrigin, list_Origin);
			TextFields.bindAutoCompletion(txtToPayCity, list_City);

		}	
		
		public void setCityFromPcode() throws SQLException 
		{

			if (!txtToPayPincode.getText().equals("")) 
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);

				boolean checkZipcodeStatus = false;
				LoadCityWithZipcodeBean destinationBean = new LoadCityWithZipcodeBean();

				new LoadCity().loadZipcodeCityDetails();
				
				for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
				{
					//System.out.println("Pincode: "+bean.getZipcode());
					
					if (Long.valueOf(txtToPayPincode.getText()) == bean.getZipcode())
					{
						checkZipcodeStatus = true;
						destinationBean.setCityName(bean.getCityName());
						break;
					} 
					else
					{
						checkZipcodeStatus = false;
					}
				}

				if (checkZipcodeStatus == true) 
				{
					txtToPayCity.setText(destinationBean.getCityName());
					txtToPayCity.requestFocus();
				}
				else 
				{
					txtToPayCity.clear();
					alert.setTitle("Zipcode City Alert");
					alert.setContentText("Please enter correct zipcode");
					alert.showAndWait();
					txtToPayPincode.requestFocus();
				}
			}
			else 
			{
				txtToPayCity.requestFocus();
			}
		}
		
// *************************************************************************************

		public void setStateAndCountry() 
		{
			if (txtToPayCity.getText() == null || txtToPayCity.getText().isEmpty()) 
			{

			}
			else 
			{
				//String[] citycode = txtCity.getText().replaceAll("\\s+", "").split("\\|");

				for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
				{
					if (txtToPayCity.getText().equals(bean.getCityName())) 
					{
						comboBoxState.setValue(bean.getStateCode());
						break;
					}
				}
				
				comboBoxCountry.setValue("IN");

				/*for (LoadCityBean countryBean : LoadState.SET_LOAD_STATEWITHNAME)
				{
					if (comboBoxState.getValue().equals(countryBean.getStateCode())) 
					{
						comboBoxCountry.setValue(countryBean.getCountryCode());
						break;
					}
				}*/

			}

		}	
	
// ******************************************************************************
		
		
		
	public String autoGenerateSeriesForToPay() throws SQLException
	{
		String toPayCodeForFirstTime="TPY/520001";
		//String toPayCodeForFirstTime="520001";
		
		String newCode=checkToPayCode();
		
		if(newCode!=null)
		{
			System.out.println("new code >>>>>>>>>>>>>>  "+newCode);
			toPayCodeForFirstTime=newCode;
		}
		return toPayCodeForFirstTime;
		
	}
	

// ******************************************************************************
	
	
	
	public String checkToPayCode() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		ToPayBean tpbean=new ToPayBean();
		
		String toPayCode=null;
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code,client_type from clientmaster where client_type='ToPay' order by code DESC limit 1";
			rs=st.executeQuery(sql);
	
			
			
			if(!rs.next())
			{
				
				toPayCode=null;
			}
			else
			{
				//toPayCode=rs.getString("code");
				
				if(areAllNumericWayTwo(rs.getString("code"))==true)
				{
					tpbean.setNewCode(String.valueOf(Long.valueOf(rs.getString("code"))+1));
					toPayCode=tpbean.getNewCode();
				}
				else
				{
					String[] topay=rs.getString("code").replaceAll("\\s+","").split("\\/");
					tpbean.setCode_start(topay[0]);
					tpbean.setCode_end(Long.valueOf(topay[1]));
					tpbean.setNewCode(tpbean.getCode_start()+"/"+(tpbean.getCode_end()+1));
					toPayCode=tpbean.getNewCode();
				}
				
				/*inbean.setInvoice_start(rs.getString("invoice_start_no"));
				inbean.setInvoice_end(rs.getLong("invoice_end_no"));
				inbean.setInvoice_number(inbean.getInvoice_start()+(inbean.getInvoice_end()+1));
				invcBean.setInvoice_number(inbean.getInvoice_number());*/
				//txtInvoiceNo.setText(inbean.getInvoice_number());
				//txtInvoiceNo.setEditable(false);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return toPayCode;
	}
	
	  public static boolean areAllNumericWayTwo(String id){
	        Pattern p = Pattern.compile("[0-9]+");
	        Matcher m = p.matcher(id);
	        boolean b = m.matches();
	        return b;
	    }
	
	/*public void getLastToPayCodeFromDB()
	{
		ToPayBean tpBean=new ToPayBean();
		
		tpBean.setClientCode(txtClientCode.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = "select code,client_type from clientmaster where client_type='ToPay' order by code DESC limit 1";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, tpBean.getClientCode());
			
			System.out.println("SQL Query: " + preparedStmt);
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				txtClientCode.setEditable(true);
			} 
			else 
			{
				txtClientCode.setEditable(false);
				btnUpdate.setDisable(false);
				btnSave.setDisable(true);
				
				do 
				{
					tpBean.setClientCode(rs.getString("code"));
					tpBean.setClientName(rs.getString("name"));
					
		
				} while (rs.next());
				
				
			}
		} 

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}*/
		
	
// ******************************************************************************		
		
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

	
		if(n.getId().equals("txtToPayName"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				loadToPayDetails();
				txtToPayPhone.requestFocus();
			}
		}

		else if(n.getId().equals("txtToPayPhone"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtToPayAddress.requestFocus();
			}

		}

		else if(n.getId().equals("txtToPayAddress"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{

				txtToPayPincode.requestFocus();
			}
		}

		else if(n.getId().equals("txtToPayPincode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtToPayCity.requestFocus();
			}
		}

		else if(n.getId().equals("txtToPayCity"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxState.requestFocus();
			}
		}

		else if(n.getId().equals("comboBoxState"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxCountry.requestFocus();
			}
		}

		else if(n.getId().equals("comboBoxCountry"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtToPayGSTIN.requestFocus();
			}
		}

		else if(n.getId().equals("txtToPayGSTIN"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtToPayEmail.requestFocus();
			}
		}

		else if(n.getId().equals("txtToPayEmail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{

				btnAddUpdate.requestFocus();
				//chkBoxReverse.requestFocus();
			}
		}

		else if(n.getId().equals("btnAddUpdate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{

				//System.err.println("Ok >>>>>>>>>>>>>>> ");
				setValidation();
				//chkBoxReverse.requestFocus();
			}
		}

	/*	else if(n.getId().equals("comboBox_Air_DimensionFormula"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{

				if(comboBox_Air_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
				{
					if(Double.valueOf(txtAir.getText())>10)
					{
						alert.setTitle("Incorrect Value");
						alert.setContentText("Air value should be not more than 10, In case of Division");
						alert.showAndWait();
						txtAir.requestFocus();
						txtAir.setText("10");
					}
					else
					{
						txtSurface.requestFocus();	
					}
				}
				else
				{
					txtSurface.requestFocus();	
				}
			}
		}*/

		
	/*	else if(n.getId().equals("comboBox_Surface_DimensionFormula"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(comboBox_Surface_DimensionFormula.getValue().equals(MasterSQL_Client_Utils.CFT))
				{
					if(Double.valueOf(txtSurface.getText())>10)
					{
						alert.setTitle("Empty Field Validation");
						alert.setContentText("Surface value should be not more than 10, In case of Division");
						alert.showAndWait();
						txtSurface.requestFocus();
						txtSurface.setText("10");
					}
					else
					{
						txtDiscount_Percentage.requestFocus();
					}
				}
				else
				{
					txtDiscount_Percentage.requestFocus();
				}
			}
		}*/


	}			
		
	
	
// *******************************************************************************	
			
	public void setValidation() throws SQLException 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

	/*	if(rdBtnExisting.isSelected()==true)
		{
			if (txtToPayCode.getText() == null || txtToPayCode.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Code");
				alert.showAndWait();
				txtToPayCode.requestFocus();
			} 
		}*/
		
	/*	if (txtToPayCode.getText() == null || txtToPayCode.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Code");
			alert.showAndWait();
			txtToPayCode.requestFocus();
		} */

		if (txtToPayName.getText() == null || txtToPayName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtToPayName.requestFocus();
		}

		else if(txtToPayAddress.getText() == null || txtToPayAddress.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Address field is Empty!");
			alert.showAndWait();
			txtToPayAddress.requestFocus();
		}

		else if(txtToPayCity.getText() == null || txtToPayCity.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("City field is Empty!");
			alert.showAndWait();
			txtToPayCity.requestFocus();
		}
		
		else if(txtToPayPincode.getText() == null || txtToPayPincode.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Pincode field is Empty!");
			alert.showAndWait();
			txtToPayPincode.requestFocus();
		}

		else if(comboBoxState.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("State field is Empty!");
			alert.showAndWait();
			comboBoxState.requestFocus();
		}

		else if(comboBoxCountry.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Country field is Empty!");
			alert.showAndWait();
			comboBoxCountry.requestFocus();
		}
		
		else 
		{
			
			if(isToPayAlreadyExist==false)
			{
				System.out.println("..... Save running... .. To Pay exist status ::  "+isToPayAlreadyExist);	
				saveToPayDetails();
			}
			else
			{
				System.out.println("..... Update running... .. To Pay exist status ::  "+isToPayAlreadyExist);
				updateToPayDetails();
				isToPayAlreadyExist=false;
			}
				
		loadToPayNames();
		txtToPayName.requestFocus();
			
		}
	}	
	
// *************************************************************************

	public void saveToPayDetails() throws SQLException 
	{

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		ToPayBean tpBean=new ToPayBean();

		tpBean.setCode(autoGenerateSeriesForToPay());
		
		
		tpBean.setName(txtToPayName.getText());
		tpBean.setEmail(txtToPayEmail.getText());
		tpBean.setAddress(txtToPayAddress.getText());
		tpBean.setCity(txtToPayCity.getText());
		tpBean.setState(comboBoxState.getValue());
		tpBean.setCountry(comboBoxCountry.getValue());
		tpBean.setGstin(txtToPayGSTIN.getText());
		tpBean.setPhone(txtToPayPhone.getText());
		tpBean.setPincode(txtToPayPincode.getText());
		tpBean.setClientType("ToPay");
		tpBean.setBranchCode(CommonVariable.USER_BRANCHCODE);
		
		System.out.println("To Pay Code number: "+tpBean.getCode());


		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try 
		{
			System.out.println("phone value From try: "+tpBean.getPhone());
			String query = MasterSQL_ToPay_Utils.INSERT_SQL_SAVE_TOPAY;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, tpBean.getCode());
			preparedStmt.setString(2, tpBean.getName());
			preparedStmt.setString(3, tpBean.getBranchCode());
			preparedStmt.setString(4, tpBean.getAddress());
			preparedStmt.setString(5, tpBean.getGstin());
			preparedStmt.setString(6, tpBean.getEmail());
			preparedStmt.setString(7, tpBean.getPincode());
			preparedStmt.setString(8, tpBean.getCity());
			preparedStmt.setString(9, "MMB");
			preparedStmt.setString(10, "1");
			preparedStmt.setString(11, tpBean.getClientType());
			preparedStmt.setString(12, tpBean.getPhone());
			
			
		
			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				alert.setContentText("To Pay details successfully saved...");
				alert.showAndWait();
				LoadToPayData.LIST_GETTOPAYDATA.add(tpBean);
				for(ToPayBean bean:LoadToPayData.LIST_GETTOPAYDATA)
				{
					System.err.println("Name :: "+bean.getName()+" | Branch :: "+bean.getBranchCode()+" | Ph :: "+bean.getPhone()+" | Address :: "+bean.getAddress()+
								" | Pin :: "+bean.getPincode()+" | City :: "+bean.getCity()+" | GSTIN :: "+bean.getGstin()+" | Email :: "+bean.getEmail()+" | Type :: "+bean.getClientType());
				}
				reset();
			} else {
				alert.setContentText("To Pay details is not saved \nPlease try again...!");
				alert.showAndWait();
			}
		
			System.err.println("New List ::::::::::::::::::::::::");
			
			

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
// *************************************************************************

	

// *************************************************************************

	public void loadToPayDetails()
	{
		for(ToPayBean bean: LoadToPayData.LIST_GETTOPAYDATA)
		{
			if(txtToPayName.getText().equals(bean.getName()))
			{
				txtToPayPhone.setText(bean.getPhone());
				txtToPayAddress.setText(bean.getAddress());
				txtToPayCity.setText(bean.getCity());
				txtToPayGSTIN.setText(bean.getGstin());
				txtToPayPincode.setText(bean.getPincode());
				txtToPayEmail.setText(bean.getEmail());
				comboBoxCountry.setValue("IN");
				comboBoxState.setValue(getStateFromPincode(bean.getPincode()));
				isToPayAlreadyExist=true;
				break;
			}
			else
			{
				isToPayAlreadyExist=false;
			}
		}
		
		if(isToPayAlreadyExist==false)
		{
			txtToPayPhone.clear();
			txtToPayAddress.clear();
			txtToPayCity.clear();
			txtToPayGSTIN.clear();
			txtToPayPincode.clear();
			txtToPayEmail.clear();
			comboBoxCountry.getSelectionModel().clearSelection();
			comboBoxState.getSelectionModel().clearSelection();
		}
			
	}
	
// *************************************************************************
	
	public String getStateFromPincode(String pincode)
	{
		String stateCode=null;
		for(LoadPincodeBean pinBean: LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(pincode.equals(pinBean.getPincode()))
			{
				stateCode=pinBean.getState_code();
				break;
			}
		}
		return stateCode;
	}


// *************************************************************************
	
	public void updateToPayDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		int index=0;

		ToPayBean tpBean=new ToPayBean();
	
		tpBean.setName(txtToPayName.getText());
		tpBean.setPhone(txtToPayPhone.getText());
		tpBean.setAddress(txtToPayAddress.getText());
		tpBean.setPincode(txtToPayPincode.getText());
		tpBean.setCity(txtToPayCity.getText());
		tpBean.setGstin(txtToPayGSTIN.getText());
		tpBean.setEmail(txtToPayEmail.getText());
		tpBean.setClientType("ToPay");
		tpBean.setBranchCode(CommonVariable.USER_BRANCHCODE);
		
	
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try 
		{
			System.out.println("phone value From try: "+tpBean.getPhone());
			String query = MasterSQL_ToPay_Utils.UPDATE_TOPAY_DATA_SQL;
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, tpBean.getBranchCode());
			preparedStmt.setString(2, tpBean.getPhone());
			preparedStmt.setString(3, tpBean.getAddress());
			preparedStmt.setString(4, tpBean.getPincode());
			preparedStmt.setString(5, tpBean.getCity());
			preparedStmt.setString(6, tpBean.getGstin());
			preparedStmt.setString(7, tpBean.getEmail());
			preparedStmt.setString(8, tpBean.getName());
			preparedStmt.setString(9, tpBean.getClientType());
			
			System.out.println("Update Query :: "+preparedStmt);
			
			int status = preparedStmt.executeUpdate();

			if (status == 1) {
				alert.setContentText("To Pay details successfully Updated...");
				alert.showAndWait();
				
				for(ToPayBean bean:LoadToPayData.LIST_GETTOPAYDATA)
				{
					if(tpBean.getName().equals(bean.getName()))
					{
						LoadToPayData.LIST_GETTOPAYDATA.remove(index);
						index=0;
						break;
					}
					index++;
				}
				
				LoadToPayData.LIST_GETTOPAYDATA.add(tpBean);
				
				reset();
			} else {
				alert.setContentText("To Pay details is not Updated \nPlease try again...!");
				alert.showAndWait();
			}
			
			isToPayAlreadyExist=false;
			
			for(ToPayBean bean:LoadToPayData.LIST_GETTOPAYDATA)
			{
				System.err.println("Name :: "+bean.getName()+" | Branch :: "+bean.getBranchCode()+" | Ph :: "+bean.getPhone()+" | Address :: "+bean.getAddress()+
							" | Pin :: "+bean.getPincode()+" | City :: "+bean.getCity()+" | GSTIN :: "+bean.getGstin()+" | Email :: "+bean.getEmail()+" | Type :: "+bean.getClientType());
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
// *************************************************************************	
	
	
	public void reset()
	{
		txtToPayName.clear();
		txtToPayPhone.clear();
		txtToPayAddress.clear();
		txtToPayCity.clear();
		txtToPayGSTIN.clear();
		txtToPayPincode.clear();
		txtToPayEmail.clear();
		comboBoxCountry.getSelectionModel().clearSelection();
		comboBoxState.getSelectionModel().clearSelection();
	}
	
	
// *************************************************************************
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		
		try {
			new LoadToPayData().getToPayDataFromDB();
			loadCityFromPincode();
			loadState();
			loadCountry();
			loadToPayNames();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
