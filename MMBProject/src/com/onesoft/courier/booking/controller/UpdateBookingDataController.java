package com.onesoft.courier.booking.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.w3c.dom.ls.LSInput;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.UpdateBookingDataTableBean;
import com.onesoft.courier.booking.bean.UpdateBookingDataBean;
import com.onesoft.courier.common.BatchExecutor;
import com.onesoft.courier.common.DateValidator;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;

public class UpdateBookingDataController implements Initializable{
	
	private ObservableList<UpdateBookingDataTableBean> tabledata_UpdatedAwbData=FXCollections.observableArrayList();
	private ObservableList<UpdateBookingDataTableBean> tabledata_SheetIssueData=FXCollections.observableArrayList();
	private ObservableList<UpdateBookingDataTableBean> tabledata_WrongServiceIssueData=FXCollections.observableArrayList();
	
	File selectedFile;
	Task<FileInputStream> sheetRead;
	Task<Void> task;
	
	private Set<UpdateBookingDataBean> set_UnsavedAwbNoWhile_Update_InDailyBooking=new HashSet();
	private List<UpdateBookingDataBean> list_UpdateRecords=new ArrayList<>();
	private List<UpdateBookingDataBean> list_FormatMisMatch=new ArrayList<>();
	private List<String> list_AwbNoToUpdate=new ArrayList<>();
	
	boolean isExcelSheetIsBlank=false;
	int numberOfEmptySheets=0;
	int totalSheets=0;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private TextField txtBrowseExcelFile;

	@FXML
	private CheckBox checkBoxDate;
	
	@FXML
	private CheckBox checkBoxMode_Service;
	
	@FXML
	private CheckBox checkBoxActualWt;
	
	@FXML
	private CheckBox checkBoxChargableBillingWt;
	
	@FXML
	private CheckBox checkBoxDestiPincode;
	
	@FXML
	private CheckBox checkBoxPCS_Box;
	
	@FXML
	private CheckBox checkBoxReferenceNo;
	
	@FXML
	private Button btnBrowseFile;
	
	@FXML
	private Button btnUpload;
	
	@FXML
	private Button btnReset;
	
// ***********************************
	
	@FXML
	private TableView<UpdateBookingDataTableBean> tableUpdatedAwb;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, Integer> updatedAwb_tabCol_slno;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_MasterAwb;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_Branch;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_Client;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_BookingDate;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_Service;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, Double> updatedAwb_tabCol_ActualWt;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, Double> updatedAwb_tabCol_BillingWt;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_Pincode;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, Integer> updatedAwb_tabCol_PCS;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> updatedAwb_tabCol_RefNo;
	
// ***********************************
	
	@FXML
	private TableView<UpdateBookingDataTableBean> tableSheetIssue;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, Integer> sheetIssue_TabCol_slno;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> sheetIssue_TabCol_MasterAwb;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> sheetIssue_TabCol_Remark;
	
	
// ***********************************
	
	@FXML
	private TableView<UpdateBookingDataTableBean> tableIncorrectService;
	
	@FXML
	private TableColumn<UpdateBookingDataTableBean, Integer> incrrctSrvc_TabCol_slno;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> incrrctSrvc_TabCol_MasterAwb;

	@FXML
	private TableColumn<UpdateBookingDataTableBean, String> incrrctSrvc_TabCol_Remark;	
	
// ******************************************************************************

	public void fileAttachment()
	{
		FileChooser fc = new FileChooser();
		long a=0;
		long size=0;
		//--------- Set validation for text and image files ------------
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls", "*.xlsx"));

		selectedFile = fc.showOpenDialog(null);

		if(selectedFile != null)
		{
			a=5*1024*1024;
			size=selectedFile.length();	

			if(size<a)
			{
				txtBrowseExcelFile.getText();
				txtBrowseExcelFile.setText(selectedFile.getAbsolutePath());
				btnUpload.requestFocus();
			}
			else
			{
				txtBrowseExcelFile.clear();
			}
		}
		else
		{
			size=0;
		}
	}	

		
// ******************************************************************************	

	public void progressBarTest() 
	{
		Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
		ProgressBar progressBar = new ProgressBar();

		progressBar.setMinWidth(400);
		progressBar.setVisible(true);

		VBox updatePane = new VBox();
		updatePane.setPadding(new Insets(35));
		updatePane.setSpacing(3.0d);
		Label lbl = new Label("Processing.......");
		lbl.setFont(Font.font("Amble CN", FontWeight.BOLD, 24));
		updatePane.getChildren().add(lbl);
		updatePane.getChildren().addAll(progressBar);

		taskUpdateStage.setScene(new Scene(updatePane));
		taskUpdateStage.initModality(Modality.APPLICATION_MODAL);
		taskUpdateStage.show();

		task = new Task<Void>() 
		{
			public Void call() throws Exception 
			{
				save();

				int max = 1000;
				for (int i = 1; i <= max; i++) 
				{
					// Thread.sleep(100);
					/*if (BirtReportExportCon.FLAG == true) {
		                                   break;
		                            }*/
				}
				return null;
			}
		};


		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			public void handle(WorkerStateEvent t) {
				taskUpdateStage.close();

				if(list_UpdateRecords.isEmpty()==false)
				{
					pagination.setVisible(true);
					pagination.setPageCount((tabledata_UpdatedAwbData.size() / rowsPerPage() + 1));
					pagination.setCurrentPageIndex(0);
					pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
				}
				else
				{
					pagination.setVisible(false);
				}

				
				list_UpdateRecords.clear();
				
				/*	try {
						//loadSavedBookingDataTable();
						//loadUnSavedBookingDataTable();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					if (numberOfEmptySheets==totalSheets)
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Empty File");
						//alert.setTitle("Empty Field Validation");
						alert.setContentText("Selected Excel File Is Empty...");
						alert.showAndWait();
						numberOfEmptySheets=0;
						totalSheets=0;

					}
					else
					{
						if(tabledata_UpdatedAwbData.isEmpty()==true)
						{
							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Task Complete");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Incomplete Enteries...\nNo Data Modified");
							alert.showAndWait();
						}
						else if(tabledata_UpdatedAwbData.isEmpty()==false && tabledata_SheetIssueData.isEmpty()==false ||
								tabledata_WrongServiceIssueData.isEmpty()==false)
						{
							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Task Complete");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Rates successfully uploaded...\nExcept Some Enteries...!! Please Check");
							alert.showAndWait();
						}
						else if(tabledata_UpdatedAwbData.isEmpty()==false && tabledata_SheetIssueData.isEmpty()==true ||
								tabledata_WrongServiceIssueData.isEmpty()==true)
						{
							Alert alert = new Alert(AlertType.INFORMATION);
							alert.setTitle("Task Complete");
							//alert.setTitle("Empty Field Validation");
							alert.setContentText("Rates successfully uploaded...");
							alert.showAndWait();
						}
						
						
						numberOfEmptySheets=0;
						totalSheets=0;
					}
			}
		});


		progressBar.progressProperty().bind(task.progressProperty());
		new Thread(task).start();
	}
	
// ******************************************************************************

	public void save() throws IOException
	{
		importNewExcel(selectedFile);
	}		
		
		
// ******************************************************************************	
		
	public void importNewExcel(File selectedFile) throws IOException
	{
		int index=0;
		list_UpdateRecords.clear();

		try
		{
			FileInputStream fileInputStream=new FileInputStream(selectedFile);
			String fileName=selectedFile.getName().replace(".xls", "").trim();
			System.out.println("File Name: "+fileName);

			Workbook workbook=new HSSFWorkbook(fileInputStream); 
			
			totalSheets=workbook.getNumberOfSheets();
			
			for(int i=0;i<workbook.getNumberOfSheets();i++)
			{
				Sheet Sheet = workbook.getSheetAt(i);
				String sheetName=Sheet.getSheetName();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

				HSSFRow row;
				
				if(Sheet.getLastRowNum()!=0)
				{
					for(int k=1;k<=Sheet.getLastRowNum();k++)
					{
						row=(HSSFRow) Sheet.getRow(k);

						Iterator<Cell> cellIterator = row.cellIterator();
						UpdateBookingDataBean updateBean=new UpdateBookingDataBean();

						try
						{
							if(!String.valueOf((long)row.getCell(0).getNumericCellValue()).equals("0"))
							{
								if(row.getCell(0).getCellType()==Cell.CELL_TYPE_NUMERIC)
								{
									updateBean.setMasterAwbNo(String.valueOf((long)row.getCell(0).getNumericCellValue()));
								}
								else
								{
									updateBean.setMasterAwbNo(row.getCell(0).getStringCellValue());
								}

								if(row.getCell(1).toString().isEmpty()==true || row.getCell(1)==null)
								{
									updateBean.setBookingDate(null);
								}
								else
								{
									updateBean.setBookingDate(row.getCell(1).toString());
								}

								updateBean.setMode_Service(row.getCell(2).getStringCellValue());
								
								if(row.getCell(3).toString().isEmpty()==true || row.getCell(3)==null)
								{
									updateBean.setActualWt(0);
								}
								else
								{
									updateBean.setActualWt(Double.valueOf(row.getCell(3).getNumericCellValue()));
								}
								
								if(row.getCell(4).toString().isEmpty()==true || row.getCell(4)==null)
								{
									updateBean.setChargableBillingWt(0);	
								}
								else
								{
									updateBean.setChargableBillingWt(Double.valueOf(row.getCell(4).getNumericCellValue()));
								}

								if(row.getCell(5).toString().isEmpty()==true || row.getCell(5)==null)
								{	
									updateBean.setDestinationPincode(null);
								}
								else
								{
									updateBean.setDestinationPincode(row.getCell(5).toString().substring(0, 6));
								}
								
								if(row.getCell(6).toString().isEmpty()==true || row.getCell(6)==null)
								{
									updateBean.setPcs(0);
								}
								else
								{
									updateBean.setPcs((int) row.getCell(6).getNumericCellValue());
								}

								updateBean.setRefNo(row.getCell(7).getStringCellValue());
							
								list_UpdateRecords.add(updateBean);
							}
						}
						catch(Exception e)
						{
							UpdateBookingDataBean bean=new UpdateBookingDataBean();
							bean.setMasterAwbNo(String.valueOf(row.getCell(0)));
							bean.setIssue_InSheet("Sheet row blank/incorrect format issue");
							list_FormatMisMatch.add(bean);
						}
					}
				}
				else
				{
					numberOfEmptySheets++;
				}
			}

			System.out.println("Sheet count >> "+totalSheets);
			System.out.println("Empty Sheet count >> "+numberOfEmptySheets);
			
			System.out.println(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_UpdateRecords.size());
			System.out.println(" format mismatch list+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " +list_FormatMisMatch.size());
			
			for(UpdateBookingDataBean upBean:list_UpdateRecords)
			{
				System.out.println("Awb :: "+upBean.getMasterAwbNo()+" | Date :: "+upBean.getBookingDate()+" | Mode :: "+upBean.getMode_Service()+" | Act Wt :: "+upBean.getActualWt()+" | Wt :: "+upBean.getChargableBillingWt()+" | Pincode :: "+upBean.getDestinationPincode()+" | PCS :: "+upBean.getPcs()+" | Ref :: "+upBean.getRefNo());
			}
			
			/*for(String num:list_FormatMisMatch)
			{
				System.out.println("Mis Match >> "+num);
			}*/

			fileInputStream.close();
			
			updateBookingData();
		}

		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
/*	public void createUpdateQuery()
	{
		String bookingDate="";
		String mode_Service="";
		String actualWt="";
		String billingWt="";
		String destinationPincode="";
		String pcs="";
		String refNo="";
		String sql=null;
		
		for(UpdateBookingDataBean upBean:list_UpdateRecords)
		{
			if(checkBoxDate.isSelected()==true)
			{
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
				LocalDate localDate = LocalDate.parse(upBean.getBookingDate(), formatter);
				
				bookingDate=" booking_date='"+Date.valueOf(localDate)+"' and";
			}

			if(checkBoxMode_Service.isSelected()==true)
			{
				mode_Service=" dailybookingtransaction2service='"+upBean.getMode_Service()+"' and";
			}
			
			if(checkBoxActualWt.isSelected()==true)
			{
				actualWt=" actual_weight="+upBean.getActualWt()+" and";
			}
			
			if(checkBoxChargableBillingWt.isSelected()==true)
			{
				billingWt=" billing_weight="+upBean.getChargableBillingWt()+" and";
			}
			
			if(checkBoxDestiPincode.isSelected()==true)
			{
				destinationPincode=" zipcode="+upBean.getDestinationPincode()+" and";
			}
			
			if(checkBoxPCS_Box.isSelected()==true)
			{
				pcs=" packets="+upBean.getPcs()+" and";
			}
			
			if(checkBoxReferenceNo.isSelected()==true)
			{
				refNo=" remarks_comments='"+upBean.getRefNo()+"' and";
			}
			
			sql="Update dailybookingtransaction SET"+bookingDate+""+mode_Service+""+actualWt+""+billingWt+""+destinationPincode+""+pcs+""+refNo+" where master_awbno='"+upBean.getMasterAwbNo()+"'";
			
			System.out.println("Filtered SQL >> "+sql.replace("and where", "where"));
		}
	}*/
	
	public void updateBookingData() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		Statement stmt = null;
		
		String bookingDate="";
		String mode_Service="";
		String actualWt="";
		String billingWt="";
		String destinationPincode="";
		String pcs="";
		String refNo="";
		String sql=null;
	
		boolean isBulkDataAvailable=false;
		int count=0;
		if(list_UpdateRecords.size()>200)
		{
			isBulkDataAvailable=true;
		}
		else
		{
			isBulkDataAvailable=false;
		}
		
		try 
		{
			if(checkBoxMode_Service.isSelected()==true)
			{
				checkService_NetworkIsAvailable();
			}
			
			stmt=con.createStatement();
			con.setAutoCommit(false);
			
			for(UpdateBookingDataBean upBean:list_UpdateRecords)
			{	
				if(checkBoxMode_Service.isSelected()==true)
				{
					System.err.println(" ===================== if >>>>>>>>>>");
					
					if(list_AwbNoToUpdate.contains(upBean.getMasterAwbNo()))
					{	
						if(checkBoxDate.isSelected()==true)
						{
							/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
							LocalDate localDate = LocalDate.parse(upBean.getBookingDate(), formatter);*/
							
							String df1=DateValidator.formatDate1(upBean.getBookingDate());
							if(df1==null)
								df1 =DateValidator.formatDate3(upBean.getBookingDate());
							
							bookingDate=" booking_date='"+df1+"' ,";
						}
	
						if(checkBoxMode_Service.isSelected()==true)
						{
							mode_Service=" dailybookingtransaction2service='"+upBean.getMode_Service()+"' ,";
						}
						
						if(checkBoxActualWt.isSelected()==true)
						{
							actualWt=" actual_weight="+upBean.getActualWt()+" ,";
						}
						
						if(checkBoxChargableBillingWt.isSelected()==true)
						{
							billingWt=" billing_weight="+upBean.getChargableBillingWt()+" ,";
						}
						
						if(checkBoxDestiPincode.isSelected()==true)
						{
							destinationPincode=" zipcode="+upBean.getDestinationPincode()+" ,";
						}
						
						if(checkBoxPCS_Box.isSelected()==true)
						{
							pcs=" packets="+upBean.getPcs()+" ,";
						}
						
						if(checkBoxReferenceNo.isSelected()==true)
						{
							refNo=" remarks_comments='"+upBean.getRefNo()+"' ,";
						}
						
						sql="Update dailybookingtransaction SET"+bookingDate+""+mode_Service+""+actualWt+""+billingWt+""+destinationPincode+""+pcs+""+refNo+" where master_awbno='"+upBean.getMasterAwbNo()+"'";
						
						System.out.println("Filtered SQL >> "+sql.replace("and where", "where"));
						
						stmt.addBatch(sql.replace(", where", "where"));
						
						count++;
						if(isBulkDataAvailable==true)
						{
							new BatchExecutor().batchExecuteForBulkData_Statement(stmt, con, count);
						}
	
					}
					else
					{
						UpdateBookingDataBean bean=new UpdateBookingDataBean();
						bean.setMasterAwbNo(upBean.getMasterAwbNo());
						bean.setIssue_ServiceMisMatch("Wrong Service");
						set_UnsavedAwbNoWhile_Update_InDailyBooking.add(bean);
					}
				}
				else
				{
					System.err.println(" ===================== else >>>>>>>>>> >>>>>>>>>>>>>>>>>>>>>>>");
					
					if(checkBoxDate.isSelected()==true)
					{
					/*	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
						LocalDate localDate = LocalDate.parse(upBean.getBookingDate(), formatter);*/
						
						String df1=DateValidator.formatDate1(upBean.getBookingDate());
						if(df1==null)
							df1 =DateValidator.formatDate3(upBean.getBookingDate());
						
						bookingDate=" booking_date='"+df1+"' ,";
					}

					if(checkBoxMode_Service.isSelected()==true)
					{
						mode_Service=" dailybookingtransaction2service='"+upBean.getMode_Service()+"' ,";
					}
					
					if(checkBoxActualWt.isSelected()==true)
					{
						actualWt=" actual_weight="+upBean.getActualWt()+" ,";
					}
					
					if(checkBoxChargableBillingWt.isSelected()==true)
					{
						billingWt=" billing_weight="+upBean.getChargableBillingWt()+" ,";
					}
					
					if(checkBoxDestiPincode.isSelected()==true)
					{
						destinationPincode=" zipcode="+upBean.getDestinationPincode()+" ,";
					}
					
					if(checkBoxPCS_Box.isSelected()==true)
					{
						pcs=" packets="+upBean.getPcs()+" ,";
					}
					
					if(checkBoxReferenceNo.isSelected()==true)
					{
						refNo=" remarks_comments='"+upBean.getRefNo()+"' ,";
					}
					
					sql="Update dailybookingtransaction SET"+bookingDate+""+mode_Service+""+actualWt+""+billingWt+""+destinationPincode+""+pcs+""+refNo+" where master_awbno='"+upBean.getMasterAwbNo()+"'";
					
					System.out.println("Filtered SQL >> "+sql.replace(", where", "where"));
					stmt.addBatch(sql.replace(", where", "where"));
					
					count++;
					if(isBulkDataAvailable==true)
					{
						new BatchExecutor().batchExecuteForBulkData_Statement(stmt, con, count);
					}
				}
				
				//stmt.addBatch(sql.replace(", where", "where"));
			}
			
			int[] result = stmt.executeBatch();
			System.out.println("The number of rows Updated: "+ result.length);
			con.commit();

			loadUpdateAwbTable();
			
			
			list_FormatMisMatch.clear();
			set_UnsavedAwbNoWhile_Update_InDailyBooking.clear();
			list_AwbNoToUpdate.clear();
			
			/*txtBrowseExcelFile.clear();
			checkBoxActualWt.setSelected(false);
			checkBoxChargableBillingWt.setSelected(false);
			checkBoxDate.setSelected(false);
			checkBoxDestiPincode.setSelected(false);
			checkBoxMode_Service.setSelected(false);
			checkBoxPCS_Box.setSelected(false);
			checkBoxReferenceNo.setSelected(false);
			
			btnBrowseFile.requestFocus();*/
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		
		finally 
		{
			dbcon.disconnect(null, stmt, null, con);
		}	
	}
	
// ****************************************************************
	
	public void checkService_NetworkIsAvailable() throws SQLException
	{

		for(UpdateBookingDataBean upBean:list_UpdateRecords)
		{
			int result=0;

			boolean isServiceExist=false;
			boolean isPincodeContainsAllNumeric=false;
			
			Pattern pattern = Pattern.compile("[0-9]+");
			Matcher matchePincode = pattern.matcher(upBean.getDestinationPincode());
			
			if(LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE.size()==0)
			{
				new LoadServiceGroups().loadServicesFromServiceType();
			}

			for(LoadServiceWithNetworkBean srvcBean: LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
			{
				if(upBean.getMode_Service().equals(srvcBean.getServiceCode()))
				{
					isServiceExist=true;
					break;
				}
				else
				{
					isServiceExist=false;
				}
			}
			
			if(matchePincode.matches())
			{
				isPincodeContainsAllNumeric=true;
			}
			else
			{
				isPincodeContainsAllNumeric=false;
			}

			if(isServiceExist==false)
			{
				System.out.println("Master >> "+upBean.getMasterAwbNo()+" | Service: >> "+upBean.getMode_Service());
			}

			if(isPincodeContainsAllNumeric==false)
			{
				System.out.println("Master >> "+upBean.getMasterAwbNo()+" | Pincode Code: >> "+upBean.getDestinationPincode());
			}


			if(isServiceExist==true && isPincodeContainsAllNumeric==true)
			{
				result=1;
			}
			else
			{
				result=0;
			}

			if(result==1)
			{
				list_AwbNoToUpdate.add(upBean.getMasterAwbNo());
			}
		/*	else
			{
				set_UnsavedAwbNoWhile_Update_InDailyBooking.add(upBean.getMasterAwbNo());
			}*/
		}
	}	
	
	
	public void loadUpdateAwbTable()
	{
		tabledata_UpdatedAwbData.clear();
		tabledata_WrongServiceIssueData.clear();
		tabledata_SheetIssueData.clear();
		
		int slno=1;
		
		for(UpdateBookingDataBean upBean:list_UpdateRecords)
		{	
			if(checkBoxMode_Service.isSelected()==true)
			{
				if(list_AwbNoToUpdate.contains(upBean.getMasterAwbNo()))
				{
					 UpdateBookingDataBean upDataBean=new UpdateBookingDataBean();
					 
					 upDataBean.setSlno(slno);
					 upDataBean.setMasterAwbNo(upBean.getMasterAwbNo());
					 upDataBean.setBookingDate(upBean.getBookingDate());
					 upDataBean.setMode_Service(upBean.getMode_Service());
					 upDataBean.setActualWt(upBean.getActualWt());
					 upDataBean.setChargableBillingWt(upBean.getChargableBillingWt());
					 upDataBean.setPcs(upBean.getPcs());
					 upDataBean.setDestinationPincode(upBean.getDestinationPincode());
					 upDataBean.setRefNo(upBean.getRefNo());
					 
					 tabledata_UpdatedAwbData.add(new UpdateBookingDataTableBean(upDataBean.getSlno(), upDataBean.getMasterAwbNo(), 
							 upDataBean.getBookingDate(), upDataBean.getMode_Service(), upDataBean.getActualWt(), 
							 upDataBean.getChargableBillingWt(), upDataBean.getDestinationPincode(), upDataBean.getPcs(), upDataBean.getRefNo()));
					 
					 slno++;
					 
				}
			}
			else
			{
				 UpdateBookingDataBean upDataBean=new UpdateBookingDataBean();
				 
				 upDataBean.setSlno(slno);
				 upDataBean.setMasterAwbNo(upBean.getMasterAwbNo());
				 upDataBean.setBookingDate(upBean.getBookingDate());
				 upDataBean.setMode_Service(upBean.getMode_Service());
				 upDataBean.setActualWt(upBean.getActualWt());
				 upDataBean.setChargableBillingWt(upBean.getChargableBillingWt());
				 upDataBean.setPcs(upBean.getPcs());
				 upDataBean.setDestinationPincode(upBean.getDestinationPincode());
				 upDataBean.setRefNo(upBean.getRefNo());
				 
				 tabledata_UpdatedAwbData.add(new UpdateBookingDataTableBean(upDataBean.getSlno(), upDataBean.getMasterAwbNo(), 
						 upDataBean.getBookingDate(), upDataBean.getMode_Service(), upDataBean.getActualWt(), 
						 upDataBean.getChargableBillingWt(), upDataBean.getDestinationPincode(), upDataBean.getPcs(), upDataBean.getRefNo()));
				 
				 slno++;
				
			}
		}
		
		tableUpdatedAwb.setItems(tabledata_UpdatedAwbData);
		
		
		/*if(list_UpdateRecords.isEmpty()==false)
		{
			pagination.setVisible(true);
			pagination.setPageCount((tabledata_UpdatedAwbData.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
		}
		else
		{
			pagination.setVisible(false);
		}*/
		slno=1;
		
		for(UpdateBookingDataBean bean:set_UnsavedAwbNoWhile_Update_InDailyBooking)
		{
			 UpdateBookingDataBean upDataBean=new UpdateBookingDataBean();
			 
			 upDataBean.setSlno(slno);
			 upDataBean.setMasterAwbNo(bean.getMasterAwbNo());
			 upDataBean.setIssue_ServiceMisMatch(bean.getIssue_ServiceMisMatch());
			 tabledata_WrongServiceIssueData.add(new UpdateBookingDataTableBean(slno, upDataBean.getMasterAwbNo(), upDataBean.getIssue_ServiceMisMatch()));
			 slno++;
		}
		
		tableIncorrectService.setItems(tabledata_WrongServiceIssueData);
		
		slno=1;
		
		for(UpdateBookingDataBean bean:list_FormatMisMatch)
		{
			 UpdateBookingDataBean upDataBean=new UpdateBookingDataBean();
			 
			 upDataBean.setSlno(slno);
			 upDataBean.setMasterAwbNo(bean.getMasterAwbNo());
			 upDataBean.setIssue_InSheet(bean.getIssue_InSheet());
			 

			 tabledata_SheetIssueData.add(new UpdateBookingDataTableBean(slno, upDataBean.getMasterAwbNo(), upDataBean.getIssue_InSheet()));
			 slno++;
		}
		
		tableSheetIssue.setItems(tabledata_SheetIssueData);
		
	}
	
	
// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
	return 10;
	}

	public GridPane createPage(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_UpdatedAwbData.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_UpdatedAwbData.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableUpdatedAwb.setItems(FXCollections.observableArrayList(tabledata_UpdatedAwbData
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} 
			else 
			{
				tableUpdatedAwb.setItems(FXCollections.observableArrayList(tabledata_UpdatedAwbData
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}
		
			
				
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		// TODO Auto-generated method stub
		
		pagination.setVisible(false);
		
		sheetIssue_TabCol_slno.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Integer>("slno"));
		sheetIssue_TabCol_MasterAwb.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("masterAwbNo"));
		sheetIssue_TabCol_Remark.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("Issue_Sheet_IncorrectService"));
		
		
		incrrctSrvc_TabCol_slno.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Integer>("slno"));
		incrrctSrvc_TabCol_MasterAwb.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("masterAwbNo"));
		incrrctSrvc_TabCol_Remark.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("Issue_Sheet_IncorrectService"));
		
		updatedAwb_tabCol_slno.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Integer>("slno"));
		updatedAwb_tabCol_MasterAwb.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("masterAwbNo"));
		updatedAwb_tabCol_BookingDate.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("bookingDate"));
	
		updatedAwb_tabCol_slno.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Integer>("slno"));
		updatedAwb_tabCol_MasterAwb.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("masterAwbNo"));
		updatedAwb_tabCol_BookingDate.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("bookingDate"));
		updatedAwb_tabCol_ActualWt.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Double>("actualWt"));
		updatedAwb_tabCol_BillingWt.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Double>("chargableBillingWt"));
		updatedAwb_tabCol_PCS.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,Integer>("pcs"));
		updatedAwb_tabCol_Service.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("mode_Service"));
		updatedAwb_tabCol_Pincode.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("destinationPincode"));
		updatedAwb_tabCol_RefNo.setCellValueFactory(new PropertyValueFactory<UpdateBookingDataTableBean,String>("refNo"));
		
	}
	
}
