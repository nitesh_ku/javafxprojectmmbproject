package com.onesoft.courier.booking.controller;


import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.jar.Manifest;

import org.controlsfx.control.textfield.TextFields;


import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.ManifestTableBean;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;


public class ManifestController implements Initializable{
	
	

	
	ObservableList<String> list_masterAwbNo=FXCollections.observableArrayList();
	
	ObservableList<String> list_ManifestNo=FXCollections.observableArrayList();

	ObservableList<ManifestTableBean> list_RootItem=FXCollections.observableArrayList();
	ObservableList<ManifestTableBean> list_ChildItem=FXCollections.observableArrayList();
	
	ObservableList<ManifestTableBean> list_RootItemD=FXCollections.observableArrayList();
	ObservableList<ManifestTableBean> list_ChildItemD=FXCollections.observableArrayList();
	
	ObservableList<ManifestTableBean>  list_RootItemManf=FXCollections.observableArrayList();
	ObservableList<ManifestTableBean> list_childItemManf=FXCollections.observableArrayList();
	
	ObservableList<ManifestTableBean>  mainList_RootItemManf=FXCollections.observableArrayList();
	ObservableList<ManifestTableBean>  mainList_childItemManf=FXCollections.observableArrayList();
	
	ObservableList<ManifestTableBean>  list_RemoveRootItem=FXCollections.observableArrayList();
	ObservableList<ManifestTableBean>  list_RemoveChildItem=FXCollections.observableArrayList();
	
	
	
	private  List<String> list_Network=new ArrayList<>();
    private List<String> list_Pincode=new ArrayList<>();
    private List<String> list_Forwarder=new ArrayList<>();
    
	String pattern="dd/MM/yyyy";
	DateFormat dateformate=new SimpleDateFormat(pattern);
	
	
	
	String pattern1="dd-MM-yyyy";
	DateFormat dateFormat=new SimpleDateFormat(pattern1);
	
	TreeItem<ManifestTableBean> dummyRootD=new TreeItem<>();
	TreeItem<ManifestTableBean>  rootD=new TreeItem<>();
	TreeItem<ManifestTableBean> childD;
	
	
	TreeItem<ManifestTableBean> dummyRootM=new TreeItem<>();
	TreeItem<ManifestTableBean>  rootM;
	TreeItem<ManifestTableBean> childM;
	
	
	@FXML
	private GridPane gridGenrateManifest;
	
	@FXML
	private GridPane gridManifestButton;
	
	@FXML
	private Button btnSave;
	
	@FXML
	private Button btnreset;
	
	
	
	@FXML
	private DatePicker dpkManfDate;
	
	@FXML
	private TextField txtManifestNo;
	
	@FXML
	private DatePicker dpkDateFrom;
	
	@FXML
	private DatePicker dpkDateTo;
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtNetwork;
	
	@FXML
	private TextField txtForwarder;
	
	@FXML
	private TextField txtRemark;
	
	
	@FXML
	private TextField dtxtAwbNo;
	
	@FXML
	private  Label  dlblTotalPcs;
	
	@FXML
	private TextField mtxtAwbNo;
	
	@FXML
	private Label mlblTotalPcs;
	
	@FXML
	private Button btnSubmit;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Button btnAddClient;
	
	@FXML
	private Button btnRemoveClient;

	
	@FXML
	private ImageView imgSearchIconDataTable;
	
	@FXML
	private TextField txtSearchDataTable;
	
	@FXML
	private ImageView imgSearchIconManifestTable;
	
	@FXML
	private TextField txtSearchManfTable;
	
	
	@FXML
	private TreeTableView<ManifestTableBean> tableData;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Integer> dColumnSrNo;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String> dColumnMasterAwbNo;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Date>  dColumnDate;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String> dColumnClient;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  dColumnNetwork;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  dColumnService;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  dColumnDestination;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Double>  dColumnWeight;
	
	
	
	
	@FXML
	private  TreeTableView<ManifestTableBean>  tableManifest;
	
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Integer> mColumnSrNo;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String> mColumnMasterAwbNo;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Date>  mColumnDate;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String> mColumnClient;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  mColumnNetwork;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  mColumnService;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, String>  mColumnDestination;
	
	@FXML
	private TreeTableColumn<ManifestTableBean, Double>  mColumnWeight;


//========Load pin code =======================================================	
	public void loadPincode() throws SQLException{
		
		new LoadPincodeForAll().loadPincode();
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common){
			
			
			list_Pincode.add( pinBean.getPincode()+" | "+pinBean.getCity_name());
			
		}
		TextFields.bindAutoCompletion(txtDestination, list_Pincode)	;
	}
	
//=========Load network ==================================================================
	
	public void loadNetwork() throws SQLException{
		
		new LoadNetworks().loadAllNetworksWithName();
		
		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME){
	            list_Network.add(bean.getNetworkName()+" | "+ bean.getNetworkCode());
		}
		TextFields.bindAutoCompletion(txtNetwork, list_Network);
	}
	
	
//========Load forwarder ==================================================================
	public void loadForwarder() throws SQLException{
		
		new LoadForwarderDetails().loadForwarderCodeWithName();
		for(LoadForwarderDetailsBean fdBean:LoadForwarderDetails.LIST_FORWARDER_DETAILS){
			
			list_Forwarder.add(fdBean.getForwarderCode()+" | "+fdBean.getForwarderName());
		}
		TextFields.bindAutoCompletion(txtForwarder, list_Forwarder);
	}
	
	
	
//===================Method useEnterAsTabkey ================================================
	
	public void useEnterAsTabkey(KeyEvent ev) throws SQLException{
		

		
		 if(ev.getCode().equals(KeyCode.ENTER) && dpkDateFrom.isFocused()){
		
			dpkDateTo.requestFocus();
		}
		else if(ev.getCode().equals(KeyCode.ENTER) && dpkDateTo.isFocused()){
	
			txtDestination.requestFocus();
		}
		else if(ev.getCode().equals(KeyCode.ENTER) && txtDestination.isFocused()){
			
			txtNetwork.requestFocus();
		}
		else if(ev.getCode().equals(KeyCode.ENTER) && txtNetwork.isFocused()){
			
			txtForwarder.requestFocus();
		}
        else if(ev.getCode().equals(KeyCode.ENTER) && txtForwarder.isFocused()){
        	
			btnSubmit.requestFocus();
		}
        
        else if(ev.getCode().equals(KeyCode.ENTER) && btnSubmit.isFocused()){
        	
        	setValidationOnFilter();
        	dpkDateFrom.requestFocus();
			
		}
          else if(ev.getCode().equals(KeyCode.ENTER) && dpkManfDate.isFocused()){
				
				txtManifestNo.requestFocus();
			}
			else if(ev.getCode().equals(KeyCode.ENTER) && txtManifestNo.isFocused()){
				
				txtRemark.requestFocus();
			}
			else if(ev.getCode().equals(KeyCode.ENTER) && txtRemark.isFocused()){
				
				btnSave.requestFocus();
				
			}
			else if(ev.getCode().equals(KeyCode.ENTER) && btnSave.isFocused()){
				saveVallidation();
				
			}
        else if(ev.getCode().equals(KeyCode.ENTER) && dtxtAwbNo.isFocused()){
        
        	
        	setValidationOnDLabel();
        
        
        }
   
        else if(ev.getCode().equals(KeyCode.ENTER) && mtxtAwbNo.isFocused()){
            
        	
        	setValidatiOnMlabel();
        
        }
	}
	
//===================Set validation on save button =========================================	
	
	public void saveVallidation() throws SQLException{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		if(txtManifestNo.getText().isEmpty() || txtManifestNo.getText()==null){
			
			alert.setTitle("Manifest no");
			alert.setContentText("Manifest no text field empty");
			alert.showAndWait();
			txtManifestNo.requestFocus();
			
		}
		else{
			
			genrateManfNo();
		
		}
	}
	
	
//======================Set validation method ====================================================
	public void setValidationOnFilter() throws SQLException{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		if(dpkDateFrom.getEditor().getText().isEmpty() || dpkDateFrom.getEditor().getText()==null){
			alert.setTitle("Validation");
			alert.setContentText("Please insert date from..");
			alert.showAndWait();
			dpkDateFrom.requestFocus();
		}
		else if(dpkDateTo.getEditor().getText().isEmpty() || dpkDateTo.getEditor().getText()==null){
			alert.setTitle("Validation");
			alert.setContentText("Please insert date to..");
			alert.showAndWait();
			dpkDateTo.requestFocus();
		}
		else{
			loadDataForManifest();
			loadDataTable();
        	submitReset();
        	if(!(dummyRootD.getChildren().size()==0)){
			gridGenrateManifest.setDisable(false);
        	gridManifestButton.setDisable(false);
        	dpkManfDate.requestFocus();
        	}
        	else{
        		gridGenrateManifest.setDisable(true);
            	gridManifestButton.setDisable(true);
            	dpkDateFrom.requestFocus();
        	}
		}
	}
	
	
	
	
//=========================Method reset ==========================================================
	
	public void reset(){
		
	
		txtManifestNo.clear();
		
		dpkDateFrom.setValue(null);
		
		dpkDateTo.setValue(null);
		
		txtDestination.clear();
		
		txtNetwork.clear();
		
		txtForwarder.clear();
		
		txtRemark.clear();
	    
		dtxtAwbNo.clear();
		
		mtxtAwbNo.clear();
		
		txtSearchDataTable.clear();
		txtSearchManfTable.clear();
		
		dlblTotalPcs.setText(null);
		mlblTotalPcs.setText(null);
		
		
		dummyRootD.getChildren().clear();
		
		dummyRootM.getChildren().clear();
		gridGenrateManifest.setDisable(true);
    	gridManifestButton.setDisable(true);
    	dpkDateFrom.requestFocus();
		
	}
	
	public void submitReset(){
	
		
		dpkDateFrom.setValue(null);
		
		dpkDateTo.setValue(null);
		
		txtDestination.clear();
		
		txtNetwork.clear();
		
		txtForwarder.clear();
		
		txtRemark.clear();
		
	}
	
//================Button add and remove design method =============================================
	
	File mainFileDown = new File("src/icon/down_arrow.png");
	File mainFileUP = new File("src/icon/up_arrow.png");
	
	Image imageUp = new Image(mainFileUP.toURI().toString());
	Image imageDown = new Image(mainFileDown.toURI().toString());
	
	public void setAddRemoveGraphics()
	{
		File hoverFileUp = new File("src/icon/Hover_up.png");
		File hoverFileDown = new File("src/icon/Hover_down.png");
		Image hoverImageDown = new Image(hoverFileDown.toURI().toString());
		Image hoverImageUp = new Image(hoverFileUp.toURI().toString());
		
		btnAddClient.setGraphic( new ImageView(hoverImageDown));
		btnRemoveClient.setGraphic( new ImageView(hoverImageUp));
		
	}
	
	
	public void addClientIcon()
	{
		btnAddClient.setGraphic( new ImageView(imageDown));
	}
	
	public void removeClientIcon()
	{
		btnRemoveClient.setGraphic( new ImageView(imageUp));
		
	}
	

//=========================For manifest get all data from daily booking	transaction table ========
	
	public void loadDataForManifest() throws SQLException{
		
		list_masterAwbNo.clear();
		
		mainList_RootItemManf.clear();
		
		mainList_childItemManf.clear();
		
		list_RootItemManf.clear();
		list_childItemManf.clear();
		
		
		list_RootItemD.clear();
    	list_ChildItemD.clear();
		
		list_RootItem.clear();
		list_ChildItem.clear();
		
		
		dummyRootD.getChildren().clear();
		String network="";
		String forwarder="";
		String destination="";
		String[] parts ;
		if(! txtNetwork.getText().isEmpty() ){
		 parts=txtNetwork.getText().replaceAll("\\s+", "").split("\\|");
		 String part1 = parts[1];
		
		 network=" and dailybookingtransaction2network='"+part1.trim()+"'";
		}
		if( ! txtForwarder.getText().isEmpty()){
		 parts=txtForwarder.getText().replaceAll("\\s+", "").split("\\|");
		 String part1 = parts[0];
		 forwarder=" and forwarder_account ='"+part1.trim()+"'";
		}
		if(! txtDestination.getText().isEmpty()){
		 parts=txtDestination.getText().replaceAll("\\s+", "").split("\\|");
		 String part1 = parts[0];
		 destination=" and zipcode= "+part1.trim(); 
		}
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement pst=null;
		ResultSet rs=null;
		try{
			String query="select master_awbno,air_way_bill_number,booking_date,"
						+ "dailybookingtransaction2client,dailybookingtransaction2network,"
						+ "dailybookingtransaction2service,forwarder_account,"
						+"zipcode,billing_weight ,mps,mps_type,packets from dailybookingtransaction where"
						+ " booking_date between '"+dpkDateFrom.getValue()+"' and '"+dpkDateTo.getValue()+"'"
						+ "and mps is not null and mps_type ='T'  "
						+" "+network+" "+forwarder+" "+destination+" and manifest_no is null  order by booking_date DESC";
					
			int srNo=0;
			
			pst=con.prepareStatement(query);
			
			System.out.println(query);
			
			rs=pst.executeQuery();
			
			while(rs.next()){
				srNo++;
				
				ManifestTableBean manfTableBean=new ManifestTableBean();
				manfTableBean.setSrNo(String.valueOf(srNo));
				manfTableBean.setMasterAwbNo(rs.getString("master_awbno"));
				manfTableBean.setAwbNo(rs.getString("air_way_bill_number"));
				manfTableBean.setDate(dateFormat.format(rs.getDate("booking_date")));
				manfTableBean.setClient(rs.getString("dailybookingtransaction2client"));
				manfTableBean.setNetwork(rs.getString("dailybookingtransaction2network"));
				
				 manfTableBean.setService(rs.getString("dailybookingtransaction2service"));
				 manfTableBean.setDestination(rs.getInt("zipcode"));
				 manfTableBean.setWeight(rs.getDouble("billing_weight"));
				 manfTableBean.setPackets(rs.getString("packets"));
				list_RootItem.add( manfTableBean);
				
			}
		
			String sql="select master_awbno,air_way_bill_number,booking_date,dailybookingtransaction2client,dailybookingtransaction2network,"
					+ "dailybookingtransaction2service,forwarder_account,"
					+"zipcode,billing_weight ,mps,mps_type,packets from dailybookingtransaction where booking_date between '"+dpkDateFrom.getValue()+"' and '"+dpkDateTo.getValue()+"'"
							+ "and mps ='T' and mps_type ='F' and manifest_no is null  ";
			
             pst=con.prepareStatement(sql);
			
			 rs=pst.executeQuery();
			
			while(rs.next()){
				
				srNo++;
				ManifestTableBean manfTableBean=new ManifestTableBean();
				
				 manfTableBean.setSrNo(String.valueOf(srNo));
				 manfTableBean.setMasterAwbNo(rs.getString("master_awbno"));
				 manfTableBean.setAwbNo(rs.getString("air_way_bill_number"));
				 manfTableBean.setDate(dateFormat.format(rs.getDate("booking_date")));
				 manfTableBean.setClient(rs.getString("dailybookingtransaction2client"));
				 manfTableBean.setNetwork(rs.getString("dailybookingtransaction2network"));
				
				 manfTableBean.setService(rs.getString("dailybookingtransaction2service"));
				 manfTableBean.setDestination(rs.getInt("zipcode"));
				 manfTableBean.setWeight(rs.getDouble("billing_weight"));
				 manfTableBean.setPackets(rs.getString("packets"));
				System.out.println(rs.getString("packets"));
				 
				 list_ChildItem.add( manfTableBean);
				
			}
			
		
			
			list_RootItemD.addAll(list_RootItem);
		    list_ChildItemD.addAll(list_ChildItem);
			
			System.out.println("Main root list size:"+list_RootItem.size());
			
			System.out.println("Main child list size:"+list_ChildItem.size());
		
	
			
			

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		finally {
			dbcon.disconnect(pst, null, rs, con);
		}
		
	}
	

//****************** ALL METHOD OF DATA TABLE ****************************************************	
	
	
//=====================Load data table ==============================================================================
	
    public void loadDataTable(){
    	
    	String childSymbole="->";
    
    
	    
	    for(ManifestTableBean rootItem:list_RootItemD){
	    	
	    	dummyRootD.getChildren().add(rootD=new TreeItem<>(rootItem));
              for(ManifestTableBean childItem:list_ChildItemD ){
				
				if(childItem.getMasterAwbNo().contains(rootItem.getMasterAwbNo())){
					
				ManifestTableBean manfTableBean=new ManifestTableBean();
				manfTableBean.setSrNo(childSymbole);
				manfTableBean.setMasterAwbNo(childItem.getAwbNo());
			    // manfTableBean.setAwbNo(.getAwbNo());
				
				manfTableBean.setDate(childItem.getDate());
				manfTableBean.setClient(childItem.getClient());
				manfTableBean.setNetwork(childItem.getNetwork());
				manfTableBean.setService(childItem.getService());
				manfTableBean.setDestination(childItem.getDestination());
				manfTableBean.setWeight(childItem.getWeight());
				rootD.getChildren().add(childD=new TreeItem<ManifestTableBean>(manfTableBean));
				
				
				}
	     	}
	    	
	    }
	    
	    setCssDataTable();
	    
	  
    }

	

//===========CSS Setting for data table ========================================================
	
	
	public void setCssDataTable(){
		
		 PseudoClass leaf = PseudoClass.getPseudoClass("leaf");
		 tableData.getStyleClass().add("tree-row-cell");
		 tableData.setRowFactory(tv -> {
	          TreeTableRow<ManifestTableBean> cell=new TreeTableRow<>();
	          cell.itemProperty().addListener((obs, oldValue, newValue) -> {
	        	
	        	  if (newValue == null) {
	                  cell.setText("");
	              } else {
	                  cell.setText(newValue.toString());
	              }
	          });
	      
	          cell.treeItemProperty().addListener((obs, oldTreeItem, newTreeItem) -> 
	          
	          cell.pseudoClassStateChanged(leaf, newTreeItem != null && newTreeItem.isLeaf()));
	         
	          return cell ;
	          
	      });
		
		 tableData.getStylesheets().add(getClass().getResource("/com/onesoft/courier/common/manifest_treetable_style.css").toExternalForm());
		
	}
	
	

	
//===============Validation method for data table AWB no text field  ===============================================================================
	
	public void setValidationOnDLabel(){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		String packets=showPacketsOnDlabel();
		if(packets!=null){
			dlblTotalPcs.setText(packets);
			addClientIcon();
        	btnAddClient.requestFocus();
        	
		}
		
		else{
			alert.setTitle("MasterAWB NO.Validation");
			alert.setContentText("Master Awb No not exists...");
			alert.showAndWait();
			dtxtAwbNo.requestFocus();
	
			
		}
	
	
		
	}
	
	
//============= Tree item check in data table method  ===========================================================
	
	public boolean checkRootDataTable(){
		if(!dtxtAwbNo.getText().isEmpty()){
		
      for(TreeItem<ManifestTableBean> manfRoot:dummyRootD.getChildren()){
			
			if(manfRoot.getValue().getMasterAwbNo().contains(dtxtAwbNo.getText())){
				return true;
				
			}
		}
     
      }
		return false;
	}
	
	public boolean checkChildDatatable(){
		if(! dtxtAwbNo.getText().isEmpty()){

			
			for(TreeItem<ManifestTableBean> dummy:dummyRootD.getChildren()){
				
			
				if(!dummy.isLeaf()){
					
			for(TreeItem<ManifestTableBean> manfRoot:dummy.getChildren())
			{
				
				if(manfRoot.getValue().getMasterAwbNo().contains(dtxtAwbNo.getText()))
				{
				
					return true;
				}
			
			}
		}
				
				
			}
			
		}
		
		
		return false;
		
		
	}
	

	
	
	
//=============Method for show packets according to AWB no on data table packets label ======================================	
	
	
	
	public String showPacketsOnDlabel(){
		
		String packets=null;
		if(checkRootDataTable()==true){
		for(ManifestTableBean manfRootItem:list_RootItem){
			
		
			if(manfRootItem.getMasterAwbNo().equals(dtxtAwbNo.getText())){
		
				    packets=manfRootItem.getPackets();
				    System.out.println(packets);
					}
			
			
		}
		}
		
		if(checkChildDatatable()==true){
		  for(ManifestTableBean manfChildItem:list_ChildItem){
			
			if(manfChildItem.getAwbNo().equals(dtxtAwbNo.getText())){
				
			         packets=manfChildItem.getPackets();
					
			         System.out.println(packets);	
			}
		}
		}
		 
		return packets;
		
	}
	
	
//================Method for add tree item in data table from manifest table ======================================================	
	
	
	
	public void addItemInDataTable(){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
	
		String masterAwbNo=null;
		
		if(! mtxtAwbNo.getText().isEmpty()){
			
			masterAwbNo=mtxtAwbNo.getText();
			
			for(ManifestTableBean manfRoot:mainList_RootItemManf){
				
				if(manfRoot.getMasterAwbNo().equals(masterAwbNo)){
					
					//System.out.println("Test 1 root manf "+manfRoot.getMasterAwbNo());
					
					list_RootItemManf.remove(manfRoot);
					if(! list_RemoveRootItem.contains(manfRoot)){
					
						list_RemoveRootItem.add(manfRoot);
					}
					
					 if(! list_RootItemD.contains(manfRoot)){
					   
						list_RootItemD.add(manfRoot);
					}
					for(ManifestTableBean manfChild:mainList_childItemManf){
						
						if(manfChild.getMasterAwbNo().equals(manfRoot.getMasterAwbNo())){
					
						    list_masterAwbNo.remove(manfChild.getAwbNo());
						    
						   
							list_childItemManf.remove(manfChild);
							
							
							if(!list_RemoveChildItem.contains(manfChild))
							{
							  list_RemoveChildItem.add(manfChild);
							}
						  
						   
						    if(! list_ChildItemD.contains(manfChild)){
						    
						        list_ChildItemD.add(manfChild);
						    }
						}
					}
					
				}
				
				//System.out.println("After remove item masterAwbno list size ++++ :"+list_masterAwbNo.size());
				
				//System.out.println("After remove item root list size :"+list_RootItemManf.size());
				
				//System.out.println("After remove item child list size :"+list_childItemManf.size());
			
			}
			
          
           
			for(ManifestTableBean manfChild: mainList_childItemManf){
				
				/*for(ManifestTableBean manfRoot:list_RootItemManf){
					
				if(! manfRoot.getMasterAwbNo().equals(manfChild.getMasterAwbNo())){
					
				if(manfChild.getAwbNo().equals(masterAwbNo)){
					
					    list_childItemManf.remove(manfChild);
					    
					    if(!list_RemoveChildItem.contains(manfChild)){
						  
					    	list_RemoveChildItem.add(manfChild);
						}
					    
					    System.out.println("After remove item child list size :"+list_childItemManf.size());
					
					    if(! list_ChildItemD.contains(manfChild)){
					    	
					    list_ChildItemD.add(manfChild);
					    
					    }
				}
				}
			
			
				}*/
				
				
				
				//if(list_RootItemManf.size()==0){
				
				if(checkChildManifesttable()==false){
				
					if(manfChild.getAwbNo().equals(masterAwbNo)){
						
					    list_childItemManf.remove(manfChild);
					    
					    if(!list_RemoveChildItem.contains(manfChild)){
						  
					    	list_RemoveChildItem.add(manfChild);
						}
					    
					    //System.out.println("After remove item child list size :"+list_childItemManf.size());
					
					    if(! list_ChildItemD.contains(manfChild)){
					    	
					    list_ChildItemD.add(manfChild);
					   
					    }
				}	
					
				}
			
			}
			
		
		
			if(checkChildManifesttable()==true){
				
					alert.setTitle("Check item");
					
					alert.setContentText("Please enter master awb no ! ");
					
					alert.showAndWait();
					
					mtxtAwbNo.requestFocus();
			
			}
		
			System.out.println("After remove item masterAwbno list size -- :"+list_masterAwbNo.size());
		
			for(ManifestTableBean root:list_RemoveRootItem){
				
				if( mainList_RootItemManf.contains(root)){
					
					mainList_RootItemManf.remove(root);
				   
					//System.out.println("After remove item main root list list size>>> :"+mainList_RootItemManf.size());
				}
			}
			
			for(ManifestTableBean child:list_RemoveChildItem){
				
				if( mainList_childItemManf.contains(child)){
					
					mainList_childItemManf.remove(child);
				   
					//System.out.println("After remove item main child list list size>>> :"+mainList_childItemManf.size());
				}
			}
			
			
			dummyRootD.getChildren().clear();
			
			loadDataTable();

	        dummyRootM.getChildren().clear();
			
			loadManifestTable();
			
			
		
		}	
			
			mtxtAwbNo.clear();
			mlblTotalPcs.setText("");
			
			
			
	}
	
	
//******************************* ALL METHOD OF DATA TABLE END *******************************************************************	
	
	


	
	
	
//****************************** ALL METHOD OF MANIFEST TABLE ************************************************************	
	


//================= Method for add tree item in manifest table from data table =============================================================
	
	
	public void addDataInManifestTable(){
	
		String masterAwbNo=null;
	   
	
		if(! dtxtAwbNo.getText().isEmpty()){
			masterAwbNo=dtxtAwbNo.getText();
			
			
			for(ManifestTableBean manfRoot:list_RootItem){
				if(manfRoot.getMasterAwbNo().equals(masterAwbNo)){
					
					list_RootItemD.remove(manfRoot);
					
					if(! list_RootItemManf.contains(manfRoot)){
						
					    mainList_RootItemManf.add(manfRoot);
					}
					for(ManifestTableBean manfChild:list_ChildItem){
						if(manfChild.getMasterAwbNo().equals(manfRoot.getMasterAwbNo())){
						   
						    list_ChildItemD.remove(manfChild);
						    if(! list_childItemManf.contains(manfChild)){
						    
						        mainList_childItemManf.add(manfChild);
						    }
						}
					}
				}
			}
			
			
		
				for(ManifestTableBean manfChild: list_ChildItem){
					if(manfChild.getAwbNo().equals(masterAwbNo)){
						
						    list_ChildItemD.remove(manfChild);
						    if(! list_childItemManf.contains(manfChild)){
						   
						    	mainList_childItemManf.add(manfChild);}
					}
				}
				
		
			
				
				for(ManifestTableBean root:mainList_RootItemManf){
					
					if(! list_RootItemManf.contains(root)){
						
						
						
						list_RootItemManf.add(root);
					}
				}
			
				for(ManifestTableBean child:mainList_childItemManf)
				{
					
				
					if(! list_childItemManf.contains(child))
					{
						System.out.println("Master awb no "+child.getAwbNo());	
						list_childItemManf.add(child);
					}
				}
				
	System.out.println("Main root list size >>>>>>>>>"+mainList_RootItemManf.size() );
	System.out.println("Main child list size >>>>>>>>"+mainList_childItemManf.size());
			
			dummyRootD.getChildren().clear();
			
			loadDataTable();
			
		   
			loadManifestTable();
				
			
		
			
		}
	
		setCssManifestTable();
		dtxtAwbNo.clear();
    	dlblTotalPcs.setText("");
    	System.out.println("Root list manifest table size >>> : "+list_RootItemManf.size());
    	System.out.println("Child list manifest table size >>> : "+list_childItemManf.size());
		
		System.out.println("Root list data table size >>> : "+list_RootItemD.size());
		System.out.println("Child list data table size >>> : "+list_ChildItemD.size());
		
		
	}

//================ Method for data load in manifest table ===========================================================    
	
	public void loadManifestTable(){
		
		 dummyRootM.getChildren().clear();
		    int	count=0;
		    int srNo=0;
 		String childSymbole="->";

 		String awbNo=null;
 		
			for(ManifestTableBean manfRoot:list_RootItemManf){
				srNo++;
				ManifestTableBean manfTableBean=new ManifestTableBean();
				
				manfTableBean.setSrNo(String.valueOf(srNo));
				manfTableBean.setMasterAwbNo(manfRoot.getMasterAwbNo());
				manfTableBean.setDate(manfRoot.getDate());
				manfTableBean.setClient(manfRoot.getClient());
				manfTableBean.setNetwork(manfRoot.getNetwork());
				manfTableBean.setService(manfRoot.getService());
				manfTableBean.setDestination(manfRoot.getDestination());
				manfTableBean.setWeight(manfRoot.getWeight());
				dummyRootM.getChildren().add(rootM=new TreeItem<ManifestTableBean>(manfTableBean));
				for(ManifestTableBean manfChild:list_childItemManf){
			       
					if(manfChild.getMasterAwbNo().equals(manfRoot.getMasterAwbNo())){
						
						awbNo=manfChild.getAwbNo();
						if(! list_masterAwbNo.contains(manfChild.getAwbNo())){
					    list_masterAwbNo.add(awbNo);
						}
						
						ManifestTableBean manfTableBeanChild=new ManifestTableBean();
						
						manfTableBeanChild.setSrNo(childSymbole);
						manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
						manfTableBeanChild.setDate(manfChild.getDate());
						manfTableBeanChild.setClient(manfChild.getClient());
						manfTableBeanChild.setNetwork(manfChild.getNetwork());
						manfTableBeanChild.setService(manfChild.getService());
						manfTableBeanChild.setDestination(manfChild.getDestination());
						manfTableBeanChild.setWeight(manfChild.getWeight());
						rootM.getChildren().add(childM=new TreeItem<ManifestTableBean>(manfTableBeanChild));
						
					}else{
						count++;
						System.out.println("Loop excution count "+count +manfChild.getMasterAwbNo());
						
						
						
					}
			
					
				}
		
			}
		
			
			if(count > 0){
			for(ManifestTableBean manfChild:list_childItemManf){
			
				
				
				
				if(! list_masterAwbNo.contains(manfChild.getAwbNo())){
					  
					   srNo++;
				        
						
						
						ManifestTableBean manfTableBeanChild=new ManifestTableBean();
						
						manfTableBeanChild.setSrNo(String.valueOf(srNo));
						manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
						manfTableBeanChild.setDate(manfChild.getDate());
						manfTableBeanChild.setClient(manfChild.getClient());
						manfTableBeanChild.setNetwork(manfChild.getNetwork());
						manfTableBeanChild.setService(manfChild.getService());
						manfTableBeanChild.setDestination(manfChild.getDestination());
						manfTableBeanChild.setWeight(manfChild.getWeight());
						dummyRootM.getChildren().add(rootM=new TreeItem<ManifestTableBean>(manfTableBeanChild));
				}else{
					System.out.println("Testing awbNo");
				}
			}
			}
			
			
			System.out.println("List master awb no size --> :"+list_masterAwbNo.size());
				if(list_RootItemManf.size()==0){
					
				for(ManifestTableBean manfChild:list_childItemManf){
					
				        srNo++;
						System.out.println("Test 1 condition");
						ManifestTableBean manfTableBeanChild=new ManifestTableBean();
						
						manfTableBeanChild.setSrNo(String.valueOf(srNo));
						manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
						manfTableBeanChild.setDate(manfChild.getDate());
						manfTableBeanChild.setClient(manfChild.getClient());
						manfTableBeanChild.setNetwork(manfChild.getNetwork());
						manfTableBeanChild.setService(manfChild.getService());
						manfTableBeanChild.setDestination(manfChild.getDestination());
						manfTableBeanChild.setWeight(manfChild.getWeight());
						dummyRootM.getChildren().add(rootM=new TreeItem<ManifestTableBean>(manfTableBeanChild));
						
					
					}
					
				}
				
		
		
	}

	
	
//======================= Method for Set CSS of manifest table ================================================================
	
	public void setCssManifestTable(){
		
		
		 PseudoClass leaf = PseudoClass.getPseudoClass("leaf");
		 tableManifest.getStyleClass().add("tree-row-cell");
		 tableManifest.setRowFactory(tv -> {
	          TreeTableRow<ManifestTableBean> cell=new TreeTableRow<>();
	          cell.itemProperty().addListener((obs, oldValue, newValue) -> {
	        	
	        	  if (newValue == null) {
	                  cell.setText("");
	              } else {
	                  cell.setText(newValue.toString());
	              }
	          });
	      
	          cell.treeItemProperty().addListener((obs, oldTreeItem, newTreeItem) -> 
	          
	          cell.pseudoClassStateChanged(leaf, newTreeItem != null && newTreeItem.isLeaf()));
	         
	          return cell ;
	          
	      });
		
		 tableManifest.getStylesheets().add(getClass().getResource("/com/onesoft/courier/common/manifest_treetable_style.css").toExternalForm());
		
		
		
	}
	
	
	
	
	
	

//================= Validation set on manifest text AWB no  =============================================================
	
public void setValidatiOnMlabel(){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		String packets=showPacketsOnMlabel();
		if(packets!=null){
			mlblTotalPcs.setText(packets);
			removeClientIcon();
        	btnRemoveClient.requestFocus();
		}
		
		else{
			alert.setTitle("MasterAWB NO.Validation");
			alert.setContentText("Master Awb No not exists...");
			alert.showAndWait();
			mlblTotalPcs.setText("");
			mtxtAwbNo.requestFocus();
	
			
		}
	
	
		
	}
	
	
//===================Check data in manifest table ============================================================
	
	public boolean checkRootManifestTable(){
		
		if(!mtxtAwbNo.getText().isEmpty()){
		
      for(TreeItem<ManifestTableBean> manfRoot:dummyRootM.getChildren()){
			
			if(manfRoot.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
				
				return true;
				
			}
		}
     
      }
		return false;
	}
	
	
	public boolean checkChildManifesttable(){
		
		if(! mtxtAwbNo.getText().isEmpty()){

			
			for(TreeItem<ManifestTableBean> dummy:dummyRootM.getChildren()){
				
			
			if(!dummy.isLeaf()){
				
				
			for(TreeItem<ManifestTableBean> manfRoot:dummy.getChildren()){
				
				if(manfRoot.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
				
					return true;
				   }
			
			     }
		      }		
			}
		}

		return false;
		
		
	}
	
	
//========================= Packets value set on manifest table label ======================================================	
	
public String showPacketsOnMlabel(){
		
		String packets=null;
		if(checkRootManifestTable()==true){
		for(ManifestTableBean manfRootItem:list_RootItem){
			
		
			if(manfRootItem.getMasterAwbNo().equals(mtxtAwbNo.getText())){
		
				    packets=manfRootItem.getPackets();
				    System.out.println(packets);
					}else{
						
						 for(ManifestTableBean manfChildItem:list_ChildItem){
								
								if(manfChildItem.getAwbNo().equals(mtxtAwbNo.getText())){
									
								         packets=manfChildItem.getPackets();
									
								}
							}
					}
			
			
		}
		}
		
		if(checkChildManifesttable()==true){
		  for(ManifestTableBean manfChildItem:list_ChildItem){
			
			if(manfChildItem.getAwbNo().equals(mtxtAwbNo.getText())){
				
			         packets=manfChildItem.getPackets();
					
			         System.out.println(packets);	
			}
		}
		}
		 
		return packets;
		
	}
	

//************************** ALL METHOD OF MANIFEST TABLE CLOSE **************************************************************





	
/*
//============Check root item in manifest table =====================================================	
	
	public boolean checkRootInManifestTable(){
		
		if(!mtxtAwbNo.getText().isEmpty()){
			
			for(TreeItem<ManifestTableBean> root:dummyRootM.getChildren()){
				if(! root.isLeaf()){
				if(root.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
					System.out.println( "true");
					return true;
					
				}
				}
				if(root.isLeaf()){
					if(root.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
						System.out.println( "true");
						return true;
						
					}	
				}
			}
		}
		return false;
		
	}
	
//====================Check child item in manifest table =========================================
	
	public boolean checkChildInManifestTable(){
		
		if(!mtxtAwbNo.getText().isEmpty()){
			
			for(TreeItem<ManifestTableBean> root:rootM.getChildren()){
				if( root.isLeaf()){
				if(root.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
					
					return true;
					
				}
				}
			}
		}
		return false;
		
	}
	
	
	
	public boolean rootCheckDataTable(){
		
		for(TreeItem<ManifestTableBean> root:dummyRootD.getChildren()){
			if(! root.isLeaf()){
			if(root.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
				System.out.println("Root master awb no ====> "+root.getValue().getMasterAwbNo());
				return false;
				
				
			}
		  if(! root.getValue().getMasterAwbNo().contains(mtxtAwbNo.getText())){
				
				System.out.println("Root master awb no not avialable");
				return true;
				
			}
		 
		}
			 if(root.getValue().getMasterAwbNo()==null){
				return true;  
			  } 	
		}
		
		
		return false;
	}
	


	


//==========Check root value according to child value for child set from manifest table  ==========================================	
	
	public boolean checkChildInDataTable(){
		
		String masterAwbNo=null;
		for(ManifestTableBean manfChild:list_ChildItemD){
			
			if(manfChild.getAwbNo().contains(mtxtAwbNo.getText())){
				
				System.out.println(manfChild.getMasterAwbNo());
				masterAwbNo=manfChild.getMasterAwbNo();
			}
		}
		for(TreeItem<ManifestTableBean> root:dummyRootD.getChildren()){
			
			if(! root.isLeaf()){
				
				if(root.getValue().getMasterAwbNo().contains(masterAwbNo)){
				return true;
				}
				}
				
			
		}
		return false;
		
	}*/


	

	
	
	
	

	 
	
//==========Manifest no generate method ========================================================================================
	
	public void genrateManfNo() throws SQLException{
		
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement pst=null;
		ResultSet rs=null;
		Statement stmt=null;
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		try{
			
			
			if(! txtManifestNo.getText().isEmpty())
			{
				if(dummyRootM.getChildren().size()==0){
				
				alert.setTitle("Check manifest data");
				alert.setContentText("Dose not exist manifest data");
				alert.showAndWait();
				
				}
				else{
					
			 String sql="Select manifest_no from dailybookingtransaction where manifest_no='"+txtManifestNo.getText() +"' and air_way_bill_number is not null";
			 boolean status = false;
			 stmt=con.createStatement();
			
			 rs=stmt.executeQuery(sql);
			 if(rs.next()){
				 status=true;
			 }
			if(status){
				
				alert.setTitle("Manifest No");
				alert.setContentText("Manifest no exist");
				alert.showAndWait();
				txtManifestNo.requestFocus();
			}
			else{
			  
				list_ManifestNo.clear();
				
				String query="Update dailybookingtransaction  set  manifest_no=? where air_way_bill_number=?";
			    
				pst=con.prepareStatement(query);
			
		        String manifestNo=txtManifestNo.getText();
		        
				for(ManifestTableBean manfRoot:list_RootItemManf){
					
					list_ManifestNo.add(manfRoot.getMasterAwbNo());
					
				}
				for(ManifestTableBean manfChild:list_childItemManf){
					
					list_ManifestNo.add(manfChild.getAwbNo());
					
				}
				for(String awbNo:list_ManifestNo){
					
					pst.setString(1, manifestNo);
					pst.setString(2, awbNo);
					
					pst.addBatch();
				}
		      
			
			     int[]    batch =	pst.executeBatch();
			 	
			     System.out.println(batch.length);
		    
			   if(batch.length > 0){
			    
				alert.setTitle("Manifest No");
				
				alert.setContentText("Generated manifest no ="+txtManifestNo.getText());
				
				alert.showAndWait();
				
				txtManifestNo.clear();
				
				dummyRootM.getChildren().clear();
				
				dummyRootD.getChildren().clear();
				
				gridGenrateManifest.setDisable(true);
				
	        	gridManifestButton.setDisable(true);
	        	
	        	dpkDateFrom.requestFocus();
	        	
			     }
			     else{
			    	
			    		alert.setTitle("Manifest No");
						
			    		alert.setContentText("Manifest no not genrate \nPlease try again...! ");
						
			    		alert.showAndWait();
						
						txtManifestNo.requestFocus();
				
			     }
			}
			}
			}
		}catch(Exception e){
			
			e.printStackTrace();
			
		}
		finally {
			dbcon.disconnect(pst, stmt, rs, con);
		}
		
	}
	
	


	
//============= Method for search implement on data table  ========================================================
	
	public void filterDataTable(String filter){
		
		
		   if (filter.isEmpty()) {
			    
		       tableData.setRoot(dummyRootD); 
		        tableData.setShowRoot(false);
		      
		    }
		   else{
			   
			   TreeItem<ManifestTableBean> filteredDummyRoot =new TreeItem<>();
			   TreeItem<ManifestTableBean> filteredRoot;
			   TreeItem<ManifestTableBean> filteredChild;
			     
		        tableData.setRoot(filteredDummyRoot); 
		        FilteredList<ManifestTableBean> filteredRootData=new  FilteredList<>(list_RootItemD,e -> true);
		        filteredRootData.setPredicate((Predicate<? super ManifestTableBean>) user ->{
		  		   if(filter==null || filter.isEmpty())
		  			{
		  				return true;
		  			}
		  		   String lowerCaseFilter=filter.toLowerCase();
		  			if(String.valueOf(user.getSrNo()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getMasterAwbNo().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getDate().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getClient().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getNetwork()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getService().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getDestination()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getWeight()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			
		  		   return false;
		  	   });
		        SortedList<ManifestTableBean> sortedRootData=new  SortedList<>(filteredRootData);
		        
		        System.out.println("Sorted root list size"+sortedRootData.size());
		        
		        FilteredList<ManifestTableBean> filteredChildData=new  FilteredList<>(list_ChildItemD,e -> true);
		        
		  
		        
		        filteredChildData.setPredicate((Predicate<? super ManifestTableBean>) user ->{
		  		   if(filter==null || filter.isEmpty())
		  			{
		  				return true;
		  			}
		  		   String lowerCaseFilter=filter.toLowerCase();
		  			if(String.valueOf(user.getSrNo()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getMasterAwbNo().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getAwbNo().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getDate().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getClient().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getNetwork()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getService().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getDestination()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getWeight()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			
		  		   return false;
		  	   });
		        
		        
		
		        SortedList<ManifestTableBean> sortedChildData=new  SortedList<>(filteredChildData);
		        
		        System.out.println("Sorted child list size"+sortedChildData.size());
		        
		        
		        String childSymbole="->";
		        int count=0;
		        
		        ObservableList<String> unMatchedAwbNo=FXCollections.observableArrayList();
		        
		        ObservableList<String> unMatchedMasterAwbNo=FXCollections.observableArrayList();
		        
		        unMatchedMasterAwbNo.clear();
		        
		        unMatchedAwbNo.clear();
		        
			    if(sortedRootData.size()!=0){
			    	
			    	//System.out.println("Test 1 condition");
			    	
			    	
			    for(ManifestTableBean rootItem:sortedRootData){
			    	
			    	filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<>(rootItem));
			    	
		              for(ManifestTableBean childItem:sortedChildData ){
						
		            	 
		            	  
						if(childItem.getMasterAwbNo().contains(rootItem.getMasterAwbNo())){
							
					     	ManifestTableBean manfTableBean=new ManifestTableBean();
					     	
					     	  if(! unMatchedMasterAwbNo.contains(childItem.getMasterAwbNo()) ){
									
									unMatchedMasterAwbNo.add(childItem.getMasterAwbNo());
									
									
								}
					     	
						    manfTableBean.setSrNo(childSymbole);
						    manfTableBean.setMasterAwbNo(childItem.getAwbNo());
					        // manfTableBean.setAwbNo(.getAwbNo());
						
						    manfTableBean.setDate(childItem.getDate());
						    manfTableBean.setClient(childItem.getClient());
						    manfTableBean.setNetwork(childItem.getNetwork());
						    manfTableBean.setService(childItem.getService());
						    manfTableBean.setDestination(childItem.getDestination());
						    manfTableBean.setWeight(childItem.getWeight());
					        
						    filteredRoot.getChildren().add(filteredChild=new TreeItem<ManifestTableBean>(manfTableBean));
						
						
						}
						else {
							 count++;
							 if(! unMatchedAwbNo.contains(childItem.getMasterAwbNo())){
							 
								  unMatchedAwbNo.add(childItem.getMasterAwbNo());
							
							 }
						}
			     	}
			    	
			    }
			    
			    if(count >0){
			    	//System.out.println("Test 2 condition ");
			    	
			    	//System.out.println(unMatchedAwbNo.size());
			    	
			    	for(String masterAwb:unMatchedAwbNo){
			    		
			    	for(ManifestTableBean manfRoot:list_RootItemD){
			    		
					     if(masterAwb.equals(manfRoot.getMasterAwbNo())){
						 
					    	 if(! unMatchedMasterAwbNo.contains(manfRoot.getMasterAwbNo())){
					    		 
					    	 filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<ManifestTableBean>(manfRoot));
					         
					    	 for(ManifestTableBean childItem:sortedChildData ){
						
									if(childItem.getMasterAwbNo().contains(manfRoot.getMasterAwbNo())){
									
								     	ManifestTableBean manfTableBean=new ManifestTableBean();
									    manfTableBean.setSrNo(childSymbole);
									    manfTableBean.setMasterAwbNo(childItem.getAwbNo());
								        // manfTableBean.setAwbNo(.getAwbNo());
									
									    manfTableBean.setDate(childItem.getDate());
									    manfTableBean.setClient(childItem.getClient());
									    manfTableBean.setNetwork(childItem.getNetwork());
									    manfTableBean.setService(childItem.getService());
									    manfTableBean.setDestination(childItem.getDestination());
									    manfTableBean.setWeight(childItem.getWeight());
								        
									    filteredRoot.getChildren().add(filteredChild=new TreeItem<ManifestTableBean>(manfTableBean));
									
									
									}
					            }
					    
					    	 }
					      }
							
						}
			    	}
			    }
			     
			    }
			    
			    if(sortedRootData.size()==0){
			    	
			    	System.out.println("Test 3 condition ");
			        for(ManifestTableBean childItem:sortedChildData ){
			        	for(ManifestTableBean rootItem:list_RootItem){
			        		if(childItem.getMasterAwbNo().equals(rootItem.getMasterAwbNo())){
			        		
			        	    filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<>(rootItem));
			        	      
			        	  	ManifestTableBean manfTableBean=new ManifestTableBean();
							manfTableBean.setSrNo(childSymbole);
							manfTableBean.setMasterAwbNo(childItem.getAwbNo());
						    //manfTableBean.setAwbNo(manfBean.getAwbNo());
							manfTableBean.setDate(childItem.getDate());
							manfTableBean.setClient(childItem.getClient());
							manfTableBean.setNetwork(childItem.getNetwork());
							manfTableBean.setService(childItem.getService());
							manfTableBean.setDestination(childItem.getDestination());
							manfTableBean.setWeight(childItem.getWeight());
			        	      
			        	      filteredRoot.getChildren().add(filteredChild=new TreeItem<>(manfTableBean));
			        		}
			        	}	
			        }
			    }
			    setCssDataTable();
			    
		        
		   }
	}
	

//====================== Method for search data in manifest table =====================================
	public void filterManifestTable(String filter){
		
		
		 if (filter.isEmpty()) {
			    
		       tableManifest.setRoot(dummyRootM); 
		        tableManifest.setShowRoot(false);
		      
		    }
		 else{
			  String ss1="->";
			   TreeItem<ManifestTableBean> filteredDummyRoot =new TreeItem<>();
			   TreeItem<ManifestTableBean> filteredRoot;
			   TreeItem<ManifestTableBean> filteredChild;
			   System.out.println(list_RootItemManf.size());
			   System.out.println(list_childItemManf.size());
			     
		        tableManifest.setRoot(filteredDummyRoot);
		        
		        FilteredList<ManifestTableBean> filteredRootData=new  FilteredList<>(list_RootItemManf,e -> true);
		        filteredRootData.setPredicate((Predicate<? super ManifestTableBean>) user ->{
		  		   if(filter==null || filter.isEmpty())
		  			{
		  				return true;
		  			}
		  		   String lowerCaseFilter=filter.toLowerCase();
		  			if(String.valueOf(user.getSrNo()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getMasterAwbNo().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getDate().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getClient().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getNetwork()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getService().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getDestination()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getWeight()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			
		  		   return false;
		  	   });
		        SortedList<ManifestTableBean> sortedRootData=new  SortedList<>(filteredRootData);
		        
		        System.out.println("Sorted root list size"+sortedRootData.size());
		        
		        FilteredList<ManifestTableBean> filteredChildData=new  FilteredList<>(list_childItemManf,e -> true);
		        
		       
		        
		        filteredChildData.setPredicate((Predicate<? super ManifestTableBean>) user ->{
		  		   if(filter==null || filter.isEmpty())
		  			{
		  				return true;
		  			}
		  		   String lowerCaseFilter=filter.toLowerCase();
		  			if(String.valueOf(user.getSrNo()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getMasterAwbNo().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getAwbNo()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getDate().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getClient().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getNetwork()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(user.getService().toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getDestination()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			if(String.valueOf(user.getWeight()).toLowerCase().contains(lowerCaseFilter)) {
		  				return true;
		  			}
		  			
		  		   return false;
		  	   });
		        
		        
		
		        SortedList<ManifestTableBean> sortedChildData=new  SortedList<>(filteredChildData);
		        
		        System.out.println("Sorted child list size"+sortedChildData.size());
		        
		 
		 
		 
		 
			  int	count=0;
			  int srNo=0;
	 		    
			  String childSymbole="->";

	 		  ObservableList<String> unMatchedAwbNo=FXCollections.observableArrayList();
		        
	 		  ObservableList<String> unMatchedMasterAwbNo=FXCollections.observableArrayList();
	 		    unMatchedMasterAwbNo.clear();
		        unMatchedAwbNo.clear();
		        
		        if(sortedRootData.size() !=0){
	 		
				for(ManifestTableBean manfRoot:sortedRootData){
					srNo++;
					ManifestTableBean manfTableBean=new ManifestTableBean();
					
					manfTableBean.setSrNo(String.valueOf(srNo));
					manfTableBean.setMasterAwbNo(manfRoot.getMasterAwbNo());
					manfTableBean.setDate(manfRoot.getDate());
					manfTableBean.setClient(manfRoot.getClient());
					manfTableBean.setNetwork(manfRoot.getNetwork());
					manfTableBean.setService(manfRoot.getService());
					manfTableBean.setDestination(manfRoot.getDestination());
					manfTableBean.setWeight(manfRoot.getWeight());
					filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<ManifestTableBean>(manfTableBean));
					for(ManifestTableBean manfChild:sortedChildData){
				       
						if(manfChild.getMasterAwbNo().equals(manfRoot.getMasterAwbNo())){
						
							ManifestTableBean manfTableBeanChild=new ManifestTableBean();
						
	                        if(! unMatchedAwbNo.contains(manfChild.getAwbNo()) ){
								
								unMatchedAwbNo.addAll(manfChild.getAwbNo(),manfChild.getMasterAwbNo());
								
								
							}
	                       
							manfTableBeanChild.setSrNo(childSymbole);
							manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
							manfTableBeanChild.setDate(manfChild.getDate());
							manfTableBeanChild.setClient(manfChild.getClient());
							manfTableBeanChild.setNetwork(manfChild.getNetwork());
							manfTableBeanChild.setService(manfChild.getService());
							manfTableBeanChild.setDestination(manfChild.getDestination());
							manfTableBeanChild.setWeight(manfChild.getWeight());
						    filteredRoot.getChildren().add(filteredChild=new TreeItem<ManifestTableBean>(manfTableBeanChild));
							
						}else{
							count++;
							 if(! unMatchedMasterAwbNo.contains(manfChild.getMasterAwbNo())){
		                        	
		                        	unMatchedMasterAwbNo.add(manfChild.getMasterAwbNo());
		                        	
		                        }

						}
				
						
					}
			
				}
		        }
		        
		       
				
	      if(count > 0){
		
	         int checkParent=0;
			
			 for(ManifestTableBean manfRoot:list_RootItemManf){
				
				if(unMatchedMasterAwbNo.contains(manfRoot.getMasterAwbNo())){
				
				if(! unMatchedAwbNo.contains(manfRoot.getMasterAwbNo())){
					
					srNo++;
					
					ManifestTableBean manfTableBean=new ManifestTableBean();
					
					manfTableBean.setSrNo(String.valueOf(srNo));
					
					manfTableBean.setMasterAwbNo(manfRoot.getMasterAwbNo());
					
					manfTableBean.setDate(manfRoot.getDate());
					
					manfTableBean.setClient(manfRoot.getClient());
					
					manfTableBean.setNetwork(manfRoot.getNetwork());
					
					manfTableBean.setService(manfRoot.getService());
					
					manfTableBean.setDestination(manfRoot.getDestination());
					
					manfTableBean.setWeight(manfRoot.getWeight());
					
					filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<ManifestTableBean>(manfTableBean));
					
					
					for(ManifestTableBean manfChild1:sortedChildData){
						
						if(! unMatchedAwbNo.contains(manfChild1.getAwbNo())){
							
						if(manfChild1.getMasterAwbNo().equals(manfRoot.getMasterAwbNo())){
							
							ManifestTableBean manfTableBeanChild=new ManifestTableBean();
							
							manfTableBeanChild.setSrNo(childSymbole);
							
							manfTableBeanChild.setMasterAwbNo(manfChild1.getAwbNo());
							
							manfTableBeanChild.setDate(manfChild1.getDate());
							
							manfTableBeanChild.setClient(manfChild1.getClient());
							
							manfTableBeanChild.setNetwork(manfChild1.getNetwork());
							
							manfTableBeanChild.setService(manfChild1.getService());
							
							manfTableBeanChild.setDestination(manfChild1.getDestination());
							
							manfTableBeanChild.setWeight(manfChild1.getWeight());
							
							filteredRoot.getChildren().add(filteredChild=new TreeItem<ManifestTableBean>(manfTableBeanChild));
					
							
						}
						
					}
						
				}
					}
					
			}
				 if(! unMatchedMasterAwbNo.contains(manfRoot.getMasterAwbNo())){
					 checkParent++;
				
				}
		}
			
		if(checkParent >0){
				
				for(ManifestTableBean manfChild:sortedChildData){
					if(! unMatchedAwbNo.contains(manfChild.getAwbNo())){
						  
						  srNo++;
						    
						  ManifestTableBean manfTableBeanChild=new ManifestTableBean();
							
						  manfTableBeanChild.setSrNo(String.valueOf(srNo));
						  
						  manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
						  
						  manfTableBeanChild.setDate(manfChild.getDate());
						  
						  manfTableBeanChild.setClient(manfChild.getClient());
							
						  manfTableBeanChild.setNetwork(manfChild.getNetwork());
							
						  manfTableBeanChild.setService(manfChild.getService());
							
						  manfTableBeanChild.setDestination(manfChild.getDestination());
							
						  manfTableBeanChild.setWeight(manfChild.getWeight());
							
						  filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<ManifestTableBean>(manfTableBeanChild));
					}
				}
				
			}
					
			
				  
		}
				
				
			
		if(sortedRootData.size()==0){
					
				int checkParent=0;
						
			    for(ManifestTableBean childItem:sortedChildData ){
			        	
			        	for(ManifestTableBean rootItem:list_RootItemManf){
			        		
			        		if(childItem.getMasterAwbNo().equals(rootItem.getMasterAwbNo())){
			        			
			        		
			        		
			        	    filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<>(rootItem));
			        	      
			        	  	ManifestTableBean manfTableBean=new ManifestTableBean();
			        	  	
							manfTableBean.setSrNo(childSymbole);
							
							manfTableBean.setMasterAwbNo(childItem.getAwbNo());
							
						    //manfTableBean.setAwbNo(manfBean.getAwbNo());
							manfTableBean.setDate(childItem.getDate());
							
							manfTableBean.setClient(childItem.getClient());
							
							manfTableBean.setNetwork(childItem.getNetwork());
							
							manfTableBean.setService(childItem.getService());
							
							manfTableBean.setDestination(childItem.getDestination());
							
							manfTableBean.setWeight(childItem.getWeight());
			        	      
			        	      filteredRoot.getChildren().add(filteredChild=new TreeItem<>(manfTableBean));
			        		}
			        		else{
			        			checkParent++;
			        		}
			        		
			        	}	
			        }
			        
			   if(checkParent >0){
				   
			        	for(ManifestTableBean manfChild:sortedChildData){
						
								   srNo++;
								    
									ManifestTableBean manfTableBeanChild=new ManifestTableBean();
									
									manfTableBeanChild.setSrNo(String.valueOf(srNo));
									
									manfTableBeanChild.setMasterAwbNo(manfChild.getAwbNo());
									
									manfTableBeanChild.setDate(manfChild.getDate());
									
									manfTableBeanChild.setClient(manfChild.getClient());
									
									manfTableBeanChild.setNetwork(manfChild.getNetwork());
									
									manfTableBeanChild.setService(manfChild.getService());
									
									manfTableBeanChild.setDestination(manfChild.getDestination());
									
									manfTableBeanChild.setWeight(manfChild.getWeight());
									
									filteredDummyRoot.getChildren().add(filteredRoot=new TreeItem<ManifestTableBean>(manfTableBeanChild));
							}
						
			            }
						
		}
		 
		
		
		 
		 }
		
	}
	

	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		gridGenrateManifest.setDisable(true);
		gridManifestButton.setDisable(true);
		
		
		setAddRemoveGraphics();
		
		imgSearchIconDataTable.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIconManifestTable.setImage(new Image("/icon/searchicon_blue.png"));
	   
		dpkManfDate.setValue(LocalDate.now());
		dpkDateFrom.setEditable(false);
		dpkDateTo.setEditable(false);
		dpkManfDate.setEditable(false);
	
		tableData.setRoot(dummyRootD);
		dummyRootD.setExpanded(true);
		tableData.setShowRoot(false);
		
		tableManifest.setRoot(dummyRootM);
		dummyRootM.setExpanded(true);
		tableManifest.setShowRoot(false);
		
//==========Data table set cell value factory =================================	
		
		dColumnSrNo.setCellValueFactory(
				new TreeItemPropertyValueFactory<>("srNo"));
		dColumnMasterAwbNo.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("masterAwbNo"));
		
		dColumnDate.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("date"));
		dColumnClient.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("client"));
		dColumnNetwork.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("network"));
		dColumnService.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("service"));
		dColumnDestination.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("destination"));
		dColumnWeight.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("weight"));
		
		
//===========Manifest table set cell value factory =======================		
		
		mColumnSrNo.setCellValueFactory(
				new TreeItemPropertyValueFactory<>("srNo"));
		mColumnMasterAwbNo.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("masterAwbNo"));
		
		mColumnDate.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("date"));
		mColumnClient.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("client"));
		mColumnNetwork.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("network"));
		mColumnService.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("service"));
		mColumnDestination.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("destination"));
		mColumnWeight.setCellValueFactory(
				 new TreeItemPropertyValueFactory<>("weight"));
		
		

		
		try {
			loadPincode();
			loadNetwork();
			loadForwarder();
			//loadDataForManifest();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
 
		txtSearchDataTable.textProperty().addListener(((observable, oldValue, newValue)-> {filterDataTable(newValue);}));
	
		txtSearchManfTable.textProperty().addListener(((observable, oldValue, newValue)-> {filterManifestTable(newValue);}));
		
		
	
	}
	
	
}
