package com.onesoft.courier.booking.controller;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.DimensionBean;
import com.onesoft.courier.booking.bean.DimensionTableBean;
import com.onesoft.courier.booking.bean.InwardBean;
import com.onesoft.courier.booking.bean.InwardScannedAwbTableBean;
import com.onesoft.courier.booking.bean.InwardUnScannedAwbTableBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class InwardPageController implements Initializable {
	
	private List<String> list_clients=new ArrayList<>();
	private List<String> list_Service_Mode=new ArrayList<>();
	private List<InwardBean> list_SubAwbNo=new ArrayList<>();
	
	
	private List<InwardBean> list_Mps_With_Main=new ArrayList<>();
	private List<InwardBean> list_Mps_With_Sub=new ArrayList<>();
	
	private List<DimensionBean> LIST_SAVEDIMENSION_DATA=new ArrayList<>();
	
	private ObservableList<InwardScannedAwbTableBean> tabledata_ScannedAwb=FXCollections.observableArrayList();
	private ObservableList<InwardUnScannedAwbTableBean> tabledata_UnScannedAwb=FXCollections.observableArrayList();
	private ObservableList<DimensionTableBean> tabledata_Dimension=FXCollections.observableArrayList();
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	int totalpcs=0;
	int countPcsForValidation=0;
	double totalweight=0;
	int dimensionPcsCount=0;
	
	@FXML
	private ImageView imgSearchIcon_Scanned;
	
	@FXML
	private ImageView imgSearchIcon_UnScanned;

	@FXML
	private Hyperlink hyperlinkCloudInward;
	
	@FXML
	private Hyperlink hyperlinkOutWard;
	
	@FXML
	private Hyperlink hyperlinkDataEntry;
	
	@FXML
	private TextField txtAwbNo;
	
	@FXML
	private TextField txtSubAwbNo;
	
	@FXML
	private TextField txtClient;
	
	@FXML
	private TextField txtZipcode;
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtTotalPcs;
	
	@FXML
	private TextField txtUnScannedTotalPcs;
	
	@FXML
	private TextField txtTotalWeight;
	
	@FXML
	private TextField txtMode;
	
	@FXML
	private TextField txtDimensionPCS;
	
	@FXML
	private TextField txtActualWeight;
	
	@FXML
	private TextField txtVolWeight;

	@FXML
	private TextField txtBillWeight;
	
	@FXML
	private TextField txtDivisionValue;
	
	@FXML
	private TextField txtDimension_Length;
	
	@FXML
	private TextField txtDimension_Width;
	
	@FXML
	private TextField txtDimension_Height;
	
	
	@FXML
	private RadioButton rdBtnMPS;
	
	@FXML
	private RadioButton rdBtnSUB;
	
	
	/*@FXML
	private Label labSubAwbNo;*/
	
	
	@FXML
	private Button btnSubmit;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Button btnExportToExcel_Scanned;
	
	@FXML
	private Button btnExportToExcel_UnScanned;
	
// ===============================================
	
	@FXML
	private Pagination pagination_Scanned;
	
	@FXML
	private Pagination pagination_UnScanned;
	

// ***************************************

	@FXML
	private TableView<InwardScannedAwbTableBean> tableScannedAwb;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, Integer> scan_tabColumn_slno;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_MasterAwb;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_Awb;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_Client;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_Destination;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, Integer> scan_tabColumn_Zipcode;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, Integer> scan_tabColumn_PCS;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, Double> scan_tabColumn_TotalWeight;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_Mode;
	
	@FXML
	private TableColumn<InwardScannedAwbTableBean, String> scan_tabColumn_Dimension;
	
	
// ***************************************
	
	@FXML
	private TableView<InwardUnScannedAwbTableBean> tableUnScannedAwb;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, Integer> unScan_tabColumn_slno;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, String> unScan_tabColumn_MasterAwb;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, String> unScan_tabColumn_Awb;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, String> unScan_tabColumn_Client;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, String> unScan_tabColumn_Destination;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, Integer> unScan_tabColumn_Zipcode;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, Integer> unScan_tabColumn_PCS;
	
	@FXML
	private TableColumn<InwardUnScannedAwbTableBean, Double> unScan_tabColumn_TotalWeight;
	
	
// ***************************************
	
	@FXML
	private TableView<DimensionTableBean> tableDimension;
	
	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_AwbNo;
	
	@FXML
	private TableColumn<DimensionTableBean, Integer> dim_tabColumn_pcs;
	
	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Lenght;
	
	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Weight;
	
	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Width;

	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Height;	
	
	
	Main m=new Main();
	
// ******************************************************************************

	public void showOutwardPage() throws IOException
	{
		m.showOutward();
	}
		
// ******************************************************************************

	public void showCloudInward() throws IOException
	{
		m.showCloudInward();		
	}
	
// ******************************************************************************
	
	public void showDataEntry() throws IOException
	{
		m.showDataEntry();
	}	
	
	
	
// ******************************************************************************
	
	public void loadClients() throws SQLException
	{
		new LoadClients().loadClientWithName();
		
		for(LoadClientBean bean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			list_clients.add(bean.getClientName()+" | "+bean.getClientCode());
		}
		TextFields.bindAutoCompletion(txtClient, list_clients);
	}
	

// ******************************************************************************
	
	public void loadModeServices() throws SQLException
	{
		new LoadServiceGroups().loadServiceGroup();
		
		for(String  service:LoadServiceGroups.SET_LOAD_SERVICES_FOR_ALL)
		{
			list_Service_Mode.add(service);
		}
		TextFields.bindAutoCompletion(txtMode, list_Service_Mode);
	}	
	

// ******************************************************************************	
	
	public void getAwbDetails() throws SQLException
	{
		tabledata_Dimension.clear();
		
		List<InwardBean> list_getAwbDetailsInwardData=new ArrayList<>();
		
		if(txtAwbNo.getText()==null || txtAwbNo.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Validation Alert");
			alert.setHeaderText(null);
			alert.setContentText("You did not enter a Awb No.");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}
		else
		{
			boolean checkAwbStatus=true;
		
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			
			InwardBean inBean=new InwardBean();
			PreparedStatement preparedStmt=null;
			
			txtDimension_Height.clear();
			txtDimension_Length.clear();
			txtDimension_Width.clear();
			txtActualWeight.clear();
			txtDimensionPCS.clear();
			
				
			inBean.setAwbNo(txtAwbNo.getText());
			
			try
			{	
				//String sql = "select dailybookingtransaction2client,dailybookingtransaction2city,dailybookingtransaction2service,zipcode,billing_weight,packets from dailybookingtransaction where air_way_bill_number=?";
				
				if(rdBtnMPS.isSelected()==true)
				{
				String sql ="select dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2city,dbt.dailybookingtransaction2service,"
						+ "dbt.zipcode,dbt.billing_weight,dbt.packets,cw.weight_prefix,cw.weight,cw.dimension_lwh,cw.division_amount from dailybookingtransaction as dbt,"
						+ "consignment_weight as cw where dbt.air_way_bill_number=cw.awb_no and dbt.air_way_bill_number=? and mps_type=? ";
						
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,inBean.getAwbNo());
				preparedStmt.setString(2,"T");
				
				System.out.println(preparedStmt);
				rs = preparedStmt.executeQuery();
				}
			
				else
				{
					String sql ="select dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2city,dbt.dailybookingtransaction2service,"
							+ "dbt.zipcode,dbt.billing_weight,dbt.packets,cw.weight_prefix,cw.weight,cw.dimension_lwh,cw.division_amount from dailybookingtransaction as dbt,"
							+ "consignment_weight as cw where dbt.air_way_bill_number=cw.awb_no and dbt.master_awbno=? and mps_type=? and mps=?";
							
					preparedStmt = con.prepareStatement(sql);
					preparedStmt.setString(1,inBean.getAwbNo());
					preparedStmt.setString(2,"T");
					preparedStmt.setString(3,"T");
					//preparedStmt.setString(4,"app");
					System.out.println(preparedStmt);
					rs = preparedStmt.executeQuery();
				}
				
				if(!rs.next())
				{
					checkAwbStatus=false;
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Incorrect Awb Alert");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the correct Awb No!!");
					alert.showAndWait();
					txtAwbNo.requestFocus();
					
					reset();
				}
				else
				{
					checkAwbStatus=true;
					do
					{
						InwardBean bean=new InwardBean();
						
						String[] weightFromWeightTable=rs.getString("weight").trim().split("\\,");
						
						bean.setClient(rs.getString("dailybookingtransaction2client"));
						bean.setTotalPcs(rs.getInt("packets"));
						bean.setDestination(rs.getString("dailybookingtransaction2city"));
						bean.setMode_service(rs.getString("dailybookingtransaction2Service"));
						bean.setZipcode(rs.getInt("zipcode"));
						bean.setTotalWeight(rs.getDouble("billing_weight"));
						bean.setDimension(rs.getString("dimension_lwh"));
						bean.setDivisionAmount(rs.getDouble("division_amount"));
						
						if(weightFromWeightTable.length>1)
						{
							bean.setWeightValue(weightFromWeightTable[weightFromWeightTable.length-1]);
						}
						else
						{
							bean.setWeightValue(weightFromWeightTable[0]);
						}
						
						bean.setWeight_prefix(rs.getString("weight_prefix"));
						
						list_getAwbDetailsInwardData.add(bean);
						
					}
					while(rs.next());
						
				}
					
					
				if(checkAwbStatus==true)
				{
					for(InwardBean bean: list_getAwbDetailsInwardData)
					{
						txtClient.setText(bean.getClient());
						txtTotalPcs.setText(String.valueOf(bean.getTotalPcs()));
						txtDestination.setText(bean.getDestination());
						txtZipcode.setText(String.valueOf(bean.getZipcode()));
						
						txtMode.setText(bean.getMode_service());
						break;
					}
						
					for(InwardBean bean: list_getAwbDetailsInwardData)
					{
						if(bean.getWeight_prefix().equals("app"))
						{
							txtTotalWeight.setText(String.valueOf(bean.getWeightValue()));
							txtActualWeight.setText(String.valueOf(bean.getWeightValue()));
							
							txtVolWeight.setText("0");
							txtBillWeight.setText("0");
						}
					}
				}
					
				
				int tablePcsCount=0;
				
				if(tabledata_Dimension.size()>0)
				{
					for(DimensionTableBean bean: tabledata_Dimension)
					{
						tablePcsCount=tablePcsCount+bean.getPcs();
					}
						
					if(tablePcsCount<Integer.valueOf(txtDimensionPCS.getText()))
					{
						txtDimensionPCS.setDisable(false);
						txtDimension_Length.setDisable(false);
						txtDimension_Height.setDisable(false);
						txtDimension_Width.setDisable(false);
					}
					else
					{
						txtDimensionPCS.setDisable(true);
						txtDimension_Length.setDisable(true);
						txtDimension_Height.setDisable(true);
						txtDimension_Width.setDisable(true);
					}
				
				}
				else
				{
					txtDimensionPCS.setDisable(false);
					txtDimension_Length.setDisable(false);
					txtDimension_Height.setDisable(false);
					txtDimension_Width.setDisable(false);
					
				}
					
				if(rdBtnSUB.isSelected()==true)
				{
					load_MPS_With_SubAwbNo_In_UnScannedTable(txtAwbNo.getText());
					if(!txtTotalPcs.getText().equals(""))
					{
						getUnScannedPcsViaMasterAwbNo();
						txtDimensionPCS.setEditable(false);
						txtDimension_Length.setEditable(true);
						txtDimension_Height.setEditable(true);
						txtDimension_Width.setEditable(true);
						
					}
					else
					{
						txtDimensionPCS.setEditable(false);
						txtDimension_Length.setEditable(false);
						txtDimension_Height.setEditable(false);
						txtDimension_Width.setEditable(false);
					}
						
				}
				else
				{
					loadOnlyMPS_Main_Data_In_UnScannedTable();
					txtDimensionPCS.clear();
					txtDimensionPCS.setEditable(true);
					
					if(!txtTotalPcs.getText().equals(""))
					{
						getUnScannedPcsViaMasterAwbNo();
						
						txtDimensionPCS.setEditable(true);
						txtDimension_Length.setEditable(true);
						txtDimension_Height.setEditable(true);
						txtDimension_Width.setEditable(true);
					}
					else
					{
						txtDimensionPCS.setEditable(false);
						txtDimension_Length.setEditable(false);
						txtDimension_Height.setEditable(false);
						txtDimension_Width.setEditable(false);
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
		
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
	}	
	
	public void getUnScannedPcsViaMasterAwbNo() throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		int rowCount=0;
		
		try
		{	
			String sql ="select air_way_bill_number from dailybookingtransaction where travel_status='app' and master_awbno=? order by dailybookingtransactionid";
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,txtAwbNo.getText());
			System.out.println(preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					rowCount++;
				}
				while(rs.next());
				
			}
			
			System.out.println("Total rows: "+rowCount );
			
			if(rowCount==Integer.valueOf(txtTotalPcs.getText()))
			{
				txtUnScannedTotalPcs.setText(txtTotalPcs.getText());
			}
			else
			{
				System.out.println("RowCount: "+rowCount);
				txtUnScannedTotalPcs.setText(String.valueOf(rowCount));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}

	}

// ============================================================================

	public void getSubAwbNoViaMasterAwb(String masterAwb) throws SQLException
	{

		list_SubAwbNo.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;

		PreparedStatement preparedStmt=null;


		try
		{	
			String sql ="select air_way_bill_number, mps, mps_type from dailybookingtransaction where master_awbno=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,masterAwb);
			System.out.println(preparedStmt);
			rs = preparedStmt.executeQuery();

			if(!rs.next())
			{

			}
			else
			{
				do
				{
					InwardBean bean=new InwardBean();
					bean.setSubAwbNo(rs.getString("air_way_bill_number"));
					bean.setMps(rs.getString("mps"));
					bean.setMpstType(rs.getString("mps_type"));
					list_SubAwbNo.add(bean);

				}
				while(rs.next());

			}
			
			/*for(InwardBean inBean: list_SubAwbNo)
			{
				if(inBean.getMpstType().equals("T"))
				{
				txtSubAwbNo.setText(inBean.getSubAwbNo());
				txtDimensionPCS.setText("1");
				txtDimensionPCS.setEditable(false);
				break;
				}
			}*/
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}

	}

// ============================================================================	
	public void calculateWeightFromDimension()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		InwardBean inBean=new InwardBean();
		
		/*if(chkDimension.isSelected()==true)
		{
		
			if(txtDimension_Length.getText()==null || txtDimension_Length.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Lenght (L)");
				alert.showAndWait();
				txtDimension_Length.requestFocus();
			}
			else if(txtDimension_Width.getText()==null || txtDimension_Width.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Width (W)");
				alert.showAndWait();
				txtDimension_Width.requestFocus();
			}
			else if(txtDimension_Height.getText()==null || txtDimension_Height.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Height (H)");
				alert.showAndWait();
				txtDimension_Height.requestFocus();
			}
			else
			{
				inBean.setDimensionLenght(Double.valueOf(txtDimension_Length.getText()));
				inBean.setDimensionWidth(Double.valueOf(txtDimension_Width.getText()));	
				inBean.setDimensionHeight(Double.valueOf(txtDimension_Height.getText()));
				
				
				if(txtDivisionValue.getText() == null || txtDivisionValue.getText().isEmpty() || txtDivisionValue.getText().equals("0"))
				{
					txtDivisionValue.setText("4500.0");
				}
				
				System.out.println("L: "+txtDimension_Length.getText()+", W: "+txtDimension_Width.getText()+", H: "+txtDimension_Height.getText()+", Division Amt: "+txtDivisionValue.getText());
				
				inBean.setWeightCalculateFromDimensions((inBean.getDimensionLenght()*inBean.getDimensionWidth()*inBean.getDimensionHeight())/Double.valueOf(txtDivisionValue.getText()));
				
				System.out.println("Calculated weight: "+inBean.getWeightCalculateFromDimensions());
				txtActualWeight.setText(df.format(inBean.getWeightCalculateFromDimensions()));
				txtActualWeight.requestFocus();
			}
		}*/
	}
	
// *************************************************************************
	
	public void scanAwbNo() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		getZoneFromPincodes(txtZipcode.getText(), txtMode.getText());
		
		InwardBean inBean=new InwardBean();
		
		inBean.setAwbNo(txtAwbNo.getText());
		inBean.setMode_service(txtMode.getText());
		inBean.setBillingWeight(Double.valueOf(txtBillWeight.getText()));
		inBean.setActualWeight(Double.valueOf(txtActualWeight.getText()));
		inBean.setVolWeight(Double.valueOf(txtVolWeight.getText()));
		inBean.setTravelStatus("inward");
		inBean.setZoneCode(getZoneFromPincodes(txtZipcode.getText(), txtMode.getText()));
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		PreparedStatement preparedStmt=null;
			
		try
		{	
			if(rdBtnMPS.isSelected()==true)
			{
				String query = "update dailybookingtransaction set zone_code=?,billing_weight=?,travel_status=?,actual_weight=?,volumne_weight=? where air_way_bill_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, inBean.getZoneCode());
				preparedStmt.setDouble(2, inBean.getBillingWeight());
				preparedStmt.setString(3, inBean.getTravelStatus());
				preparedStmt.setDouble(4, inBean.getActualWeight());
				preparedStmt.setDouble(5, inBean.getVolWeight());
				preparedStmt.setString(6, inBean.getAwbNo());
				
				preparedStmt.executeUpdate();
				
				saveDimensionDetails();
				updateMainMPSweightForSubAwbNo();
				
				txtAwbNo.requestFocus();
				
			}
		
			else
			{
				if(tabledata_Dimension.size()>0)
				{
					for(DimensionTableBean bean: tabledata_Dimension)
					{
						String query = "update dailybookingtransaction set zone_code=?,billing_weight=?,travel_status=?,actual_weight=?,volumne_weight=? where air_way_bill_number=?";
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, inBean.getZoneCode());
						preparedStmt.setDouble(2, bean.getWeight());
						preparedStmt.setString(3, inBean.getTravelStatus());
						preparedStmt.setDouble(4, inBean.getActualWeight());
						preparedStmt.setDouble(5, inBean.getVolWeight());
						preparedStmt.setString(6, bean.getAwbno());
						preparedStmt.executeUpdate();
				
						txtAwbNo.requestFocus();
					}
					saveDimensionDetails();
					updateMainMPSweightForSubAwbNo();
				}
					
				else
				{	
					alert.setContentText("Please enter Dimension for atlest 1 Sub Awb no");
					alert.showAndWait();
					txtDimensionPCS.requestFocus();
				}
			}
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	

// *************************************************************************
	
	public void updateMainMPSweightForSubAwbNo() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		InwardBean inBean=new InwardBean();
		
		inBean.setAwbNo(txtAwbNo.getText());
		inBean.setMode_service(txtMode.getText());
		inBean.setBillingWeight(Double.valueOf(txtBillWeight.getText()));
		inBean.setTravelStatus("inward");
		Main m=new Main();
		
		try
		{
			if(rdBtnMPS.isSelected()==true)
			{
				String query = "update dailybookingtransaction set travel_status=? where master_awbno=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, inBean.getTravelStatus());
				preparedStmt.setString(2, inBean.getAwbNo());
				
				int status=preparedStmt.executeUpdate();
				
				System.out.println("status value: "+status);
				
				if(status>0)
				{
					alert.setContentText("Awb no successfully scanned");
					alert.showAndWait();
				}
				else
				{
					alert.setContentText("from if >> Awb Scanning not completed \nPlease try again...!");
					alert.showAndWait();
				}
				
				reset();
				m.showInward();
				txtAwbNo.requestFocus();
			}
			
			else
			{
				
				String query = "update dailybookingtransaction set billing_weight=? where air_way_bill_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setDouble(1, inBean.getBillingWeight());
				preparedStmt.setString(2, inBean.getAwbNo());
				
				int status=preparedStmt.executeUpdate();
				
				if(status>0)
				{
					alert.setContentText("Awb No successfully scanned Sub Entries...");
					alert.showAndWait();

				}
				else
				{
					alert.setContentText("from else >> Awb Scanning not completed \nPlease try again...!");
					alert.showAndWait();
				}
				
				reset();
				m.showInward();
				txtAwbNo.requestFocus();
			}
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

	
// *************************************************************************
	
	public void saveDimensionDetails() throws SQLException 
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		Main m=new Main();

		try 
		{
			if(rdBtnMPS.isSelected()==true)
			{
				if(tabledata_Dimension.size()>0)
				{
					for(DimensionTableBean diBean: tabledata_Dimension)
					{
						String query = "insert into dimensions(awbno,pcs,weight,length,height,width,indate,create_date,lastmodifieddate)"
								+ " values(?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
						
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, txtAwbNo.getText());
						preparedStmt.setInt(2, diBean.getPcs());
						preparedStmt.setDouble(3, diBean.getWeight());
						preparedStmt.setDouble(4, diBean.getWidth());
						preparedStmt.setDouble(5, diBean.getLength());
						preparedStmt.setDouble(6, diBean.getHeight());
						
						preparedStmt.executeUpdate();

					}
	
					m.showInward();
				}
			}
			else
			{
				if(tabledata_Dimension.size()>0)
				{
					for(DimensionTableBean diBean: tabledata_Dimension)
					{
						String query = "insert into dimensions(awbno,sub_awbno,pcs,weight,length,height,width,indate,create_date,lastmodifieddate)"
								+ " values(?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
						
						preparedStmt = con.prepareStatement(query);
						
						preparedStmt.setString(1, txtAwbNo.getText());
						preparedStmt.setString(2, diBean.getAwbno());
						preparedStmt.setInt(3, diBean.getPcs());
						preparedStmt.setDouble(4, diBean.getWeight());
						preparedStmt.setDouble(5, diBean.getWidth());
						preparedStmt.setDouble(6, diBean.getLength());
						preparedStmt.setDouble(7, diBean.getHeight());

						preparedStmt.executeUpdate();
					}
	
					m.showInward();
				}
			}
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
		
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	
// *************************************************************************	
	/*
	public void checkWeightPrefix(String awbNo,String weight,String prefix) throws SQLException
	{
		boolean checkWeightStatus=true;
		
		System.out.println("Check weight prefix............ running...");
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		InwardBean inBean=new InwardBean();
		PreparedStatement preparedStmt=null;
		
		
			try
			{	
				String sql = "select awb_no,weight_prefix,weight from consignment_weight where awb_no=? and weight_prefix=?";
				preparedStmt = con.prepareStatement(sql);
				preparedStmt.setString(1,awbNo);
				preparedStmt.setString(2,prefix);
				
				rs = preparedStmt.executeQuery();
			
				if(!rs.next())
				{
					checkWeightStatus=false;
					
					insertWeightWithPrefix(awbNo, weight, prefix,checkWeightStatus);
					
				}
				else
				{
					checkWeightStatus=true;
					do{
						inBean.setWeight_prefix(rs.getString("weight_prefix"));
						inBean.setWeightValue(rs.getString("weight"));
						
					}while(rs.next());
					
					
					insertWeightWithPrefix(awbNo, inBean.getWeightValue(), inBean.getWeight_prefix(),checkWeightStatus);
				}
				
				
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	
	}*/
	
// *************************************************************************

	/*public void insertWeightWithPrefix(String awbNo,String weight,String prefix,boolean weightStatus) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		System.out.println("Insert update weight");
		
		InwardBean inBean=new InwardBean();
		
		inBean.setAwbNo(awbNo);
		inBean.setWeightValue(weight);
		inBean.setWeight_prefix(prefix);
		
		
		if(chkDimension.isSelected()==true)
		{
			inBean.setDimensionLenght(Double.valueOf(txtDimension_Length.getText()));
			inBean.setDimensionWidth(Double.valueOf(txtDimension_Width.getText()));	
			inBean.setDimensionHeight(Double.valueOf(txtDimension_Height.getText()));	
			
			inBean.setDimension(inBean.getDimensionLenght()+"*"+inBean.getDimensionWidth()+"*"+inBean.getDimensionHeight());
			inBean.setDivisionAmount(Double.valueOf(txtDivisionValue.getText()));
			
		}
		
				
		try
		{	

			if(weightStatus==false)
			{
				System.out.println("Insert running... >>>>>>>");
				query = "insert into consignment_weight(awb_no,weight_prefix,weight,create_date,lastmodifieddate,dimension_lwh,division_amount) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?)";
				preparedStmt = con.prepareStatement(query);

				preparedStmt.setString(1, inBean.getAwbNo());
				preparedStmt.setString(2, inBean.getWeight_prefix());
				preparedStmt.setString(3, inBean.getWeightValue());
				preparedStmt.setString(4, inBean.getDimension());
				preparedStmt.setDouble(5, inBean.getDivisionAmount());
				
				preparedStmt.executeUpdate();

			}
			else
			{
				System.out.println("Update running... >>>>>>> prefix: "+prefix);
				query = "update consignment_weight set weight=?,lastmodifieddate=CURRENT_TIMESTAMP, dimension_lwh=?, division_amount=? where awb_no=? and weight_prefix=?";
				preparedStmt = con.prepareStatement(query);

				preparedStmt.setString(1, inBean.getWeightValue()+","+txtActualWeight.getText());
				preparedStmt.setString(2, inBean.getDimension());
				preparedStmt.setDouble(3, inBean.getDivisionAmount());
				preparedStmt.setString(4, inBean.getAwbNo());
				preparedStmt.setString(5, inBean.getWeight_prefix());
			
				preparedStmt.executeUpdate();

			}
			

		}

		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	*/
	
	
// ***************************************************************************************	

	public String getZoneFromPincodes(String destination,String serviceCode) throws SQLException
	{

			System.err.println(" | DSTI >>>>>>>>>>>>> "+destination+" | Service code >>> "+serviceCode);

			String originZone=null;
			String destiZone=null;
			String getServiceMode=null;

			String destination_zone=null;

			new LoadServiceGroups().loadServicesFromServiceType();
			for(LoadServiceWithNetworkBean bean:LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
			{
				if(bean.getServiceCode().equals(serviceCode))
				{
					getServiceMode=bean.getServiceMode();
					System.out.println("Service Name: "+bean.getServiceCode()+" | Mode: "+bean.getServiceMode());
					break;
				}
			}




			/*for(UploadPincodeBean upBean: list_PincodeWithZone_air_surface)
				{*/
			for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
			{
				if(destination.equals(pinBean.getPincode()))
				{
					if(getServiceMode.equals("AR"))
					{
						destiZone=pinBean.getZone_air();
					}
					else if(getServiceMode.equals("SF"))
					{
						destiZone=pinBean.getZone_surface();
					}

					System.out.println("Destination Service Mode >>> "+destiZone);
				}



				if(destiZone!=null)
				{
					destination_zone=destiZone;
					System.out.println("Desitination Zone: " +destination+" "+destiZone);
					break;
				}
			}
			return destination_zone;
	}		
	
	
// *************************************************************************
	

	public void loadScannedInwardTable() throws SQLException
	{
		tabledata_ScannedAwb.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		InwardBean inBean=new InwardBean();
		
		int slno=1;
		
		try
		{
			stmt=con.createStatement();
			sql="select dailybookingtransactionid, master_awbno,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2service,"
					+ "dailybookingtransaction2city,zipcode,packets,billing_weight,dimension from dailybookingtransaction where "
					+ "travel_status='inward' and mps IS NOT NUll order by dailybookingtransactionid DESC";
	
			System.out.println("SQL: "+sql);
			rs=stmt.executeQuery(sql);
				
			while(rs.next())
			{
				inBean.setSlno(slno);
				inBean.setMasterAwb(rs.getString("master_awbno"));
				inBean.setAwbNo(rs.getString("air_way_bill_number"));
				inBean.setClient(rs.getString("dailybookingtransaction2client"));
				inBean.setDestination(rs.getString("dailybookingtransaction2city"));
				inBean.setZipcode(rs.getInt("zipcode"));
				inBean.setTotalPcs(rs.getInt("packets"));
				inBean.setTotalWeight(rs.getDouble("billing_weight"));
				inBean.setMode_service(rs.getString("dailybookingtransaction2service"));
				inBean.setDimension(rs.getString("dimension"));
				
				tabledata_ScannedAwb.add(new InwardScannedAwbTableBean(inBean.getSlno(), inBean.getMasterAwb(), inBean.getAwbNo(), inBean.getClient(),
						inBean.getZipcode(), inBean.getDestination(), inBean.getTotalPcs(), inBean.getTotalWeight(),
						inBean.getMode_service(), inBean.getDimension()));
					
				slno++;
			}
			
			tableScannedAwb.setItems(tabledata_ScannedAwb);
				
			pagination_Scanned.setPageCount((tabledata_ScannedAwb.size() / rowsPerPage() + 1));
			pagination_Scanned.setCurrentPageIndex(0);
			pagination_Scanned.setPageFactory((Integer pageIndex) -> createPage_ScannedAwb(pageIndex));
				
		}
		
		catch (Exception e)
		{
			System.out.println(e);
		}
	
		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	
	
	
// *************************************************************************	

	public void loadUnScannedInwardTable() throws SQLException
	{
		
		System.err.println("Clear 1 running....");
		tabledata_UnScannedAwb.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		int slno=1;
		
		try
		{
		
			stmt=con.createStatement();
			
			if(rdBtnSUB.isSelected()==true)
			{
				sql="select dailybookingtransactionid, master_awbno,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2city,"
						+ "mps,mps_type,zipcode,packets,billing_weight from dailybookingtransaction where master_awbno='"+txtAwbNo.getText()+"' and travel_status='app' "
						+ "and mps IS NOT NUll order by dailybookingtransactionid DESC";
			}
			else
			{
			sql="select dailybookingtransactionid, master_awbno,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2city,"
					+ "mps,mps_type,zipcode,packets,billing_weight from dailybookingtransaction where travel_status='app' "
					+ "and mps IS NOT NUll order by dailybookingtransactionid DESC";
			}
	
			System.out.println("SQL: "+sql);
			rs=stmt.executeQuery(sql);
					
			while(rs.next())
			{
				InwardBean inBean=new InwardBean();
				
				inBean.setSlno(slno);
				inBean.setMps(rs.getString("mps"));
				inBean.setMpstType(rs.getString("mps_type"));
				inBean.setMasterAwb(rs.getString("master_awbno"));
				inBean.setAwbNo(rs.getString("air_way_bill_number"));
				inBean.setClient(rs.getString("dailybookingtransaction2client"));
				inBean.setDestination(rs.getString("dailybookingtransaction2city"));
				inBean.setZipcode(rs.getInt("zipcode"));
				inBean.setTotalPcs(rs.getInt("packets"));
				inBean.setTotalWeight(rs.getDouble("billing_weight"));
						
						
				if(inBean.getMpstType().equals("T"))
				{
					list_Mps_With_Main.add(inBean);
				}
				
				list_Mps_With_Sub.add(inBean);
				
			}

			loadOnlyMPS_Main_Data_In_UnScannedTable();
		}
			
		catch (Exception e)
		{
			System.out.println(e);
		}

		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}		
	
	
// *************************************************************************	
	
	public void loadOnlyMPS_Main_Data_In_UnScannedTable()
	{
		tabledata_UnScannedAwb.clear();
		
		int slno=1;
		for(InwardBean inBean: list_Mps_With_Main)
		{
			tabledata_UnScannedAwb.add(new InwardUnScannedAwbTableBean(slno, inBean.getMasterAwb(), inBean.getAwbNo(), inBean.getClient(),
					inBean.getZipcode(), inBean.getDestination(), inBean.getTotalPcs(), inBean.getTotalWeight()));
					
				slno++;
		}
		tableUnScannedAwb.setItems(tabledata_UnScannedAwb);
		
		pagination_UnScanned.setPageCount((tabledata_UnScannedAwb.size() / rowsPerPage() + 1));
		pagination_UnScanned.setCurrentPageIndex(0);
		pagination_UnScanned.setPageFactory((Integer pageIndex) -> createPage_UnScannedAwb(pageIndex));

	}

	
// *************************************************************************	
	
	public void load_MPS_With_SubAwbNo_In_UnScannedTable(String masterAwbNo)
	{
		tabledata_UnScannedAwb.clear();
		list_SubAwbNo.clear();
		
		
		System.out.println("MPS sub list size: "+list_Mps_With_Sub.size());
		
		int slno=1;
		for(InwardBean inBean: list_Mps_With_Sub)
		{
			if(inBean.getMasterAwb().equals(masterAwbNo))
			{
		
				InwardBean bean=new InwardBean();
				bean.setSubAwbNo(inBean.getAwbNo());
				bean.setMps(inBean.getMps());
				bean.setMpstType(inBean.getMpstType());
				list_SubAwbNo.add(bean);
				
				System.out.println("Master Awb: "+inBean.getMasterAwb()+" | Awb: "+inBean.getAwbNo());
				tabledata_UnScannedAwb.add(new InwardUnScannedAwbTableBean(slno, inBean.getMasterAwb(), inBean.getAwbNo(), inBean.getClient(),
				inBean.getZipcode(), inBean.getDestination(), inBean.getTotalPcs(), inBean.getTotalWeight()));
					
			slno++;
			}
			
		}
		tableUnScannedAwb.setItems(tabledata_UnScannedAwb);
		
		boolean findSubAwbNo=false;
		for(InwardBean inBean: list_SubAwbNo)
		{
			if(inBean.getMpstType().equals("T"))
			{
			txtSubAwbNo.setText(inBean.getSubAwbNo());
			txtDimensionPCS.setText("1");
			txtDimensionPCS.setEditable(false);
			findSubAwbNo=true;
			break;
			}
			else
			{
				findSubAwbNo=false;
			}
		}
		
		if(findSubAwbNo==false)
		{
			if(list_SubAwbNo.size()>0)
			{
				txtSubAwbNo.setText(list_SubAwbNo.get(0).getSubAwbNo());
				txtDimensionPCS.setText("1");
				txtDimensionPCS.setEditable(false);
			}
		}
		
		pagination_UnScanned.setPageCount((tabledata_UnScannedAwb.size() / rowsPerPage() + 1));
		pagination_UnScanned.setCurrentPageIndex(0);
		pagination_UnScanned.setPageFactory((Integer pageIndex) -> createPage_UnScannedAwb(pageIndex));
		
	}

	
// *************************************************************************
	
	public void checkSubAwbNo()
	{
		boolean checkSubAwbNoStatus=true;
		
		for(InwardBean inBean: list_SubAwbNo)
		{
			if(txtSubAwbNo.getText().equals(inBean.getSubAwbNo()))
			{
				checkSubAwbNoStatus=true;
				break;
			}
			else
			{
				checkSubAwbNoStatus=false;
			}
		}
		
		if(checkSubAwbNoStatus==false)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);
			alert.setTitle("Alert");
			alert.setContentText("Entered Sub Awb No does not exist..");
			alert.showAndWait();
			checkSubAwbNoStatus=true;
			txtSubAwbNo.requestFocus();
		}
		else
		{
			txtDimensionPCS.requestFocus();
		}
		
	}
	

// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 10;
	}

	public GridPane createPage_UnScannedAwb(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_UnScannedAwb.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_UnScannedAwb.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableUnScannedAwb.setItems(FXCollections.observableArrayList(tabledata_UnScannedAwb
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableUnScannedAwb.setItems(FXCollections.observableArrayList(tabledata_UnScannedAwb
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

	public GridPane createPage_ScannedAwb(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_ScannedAwb.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_ScannedAwb.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableScannedAwb.setItems(FXCollections.observableArrayList(
						tabledata_ScannedAwb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableScannedAwb.setItems(FXCollections.observableArrayList(tabledata_ScannedAwb
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}	
	
		
// ========================================================================================

	@FXML
	public void exportToExcel_ScannedAwb() throws SQLException, IOException {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Scanned Awb");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Master Awb No");
		rowhead.createCell(2).setCellValue("Awb No");
		
		rowhead.createCell(3).setCellValue("Client");
		rowhead.createCell(4).setCellValue("Destination");
		rowhead.createCell(5).setCellValue("Zipcode");
		
		rowhead.createCell(6).setCellValue("PCS");
		rowhead.createCell(7).setCellValue("Total Weight");
		rowhead.createCell(8).setCellValue("Mode/Service");
		rowhead.createCell(9).setCellValue("Dimension");
		

		int i = 2;

		HSSFRow row;

		for (InwardScannedAwbTableBean scannedBean : tabledata_ScannedAwb) {
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(scannedBean.getSlno());
			row.createCell(1).setCellValue(scannedBean.getMasterAwb());
			row.createCell(2).setCellValue(scannedBean.getAwbNo());

			row.createCell(3).setCellValue(scannedBean.getClient());
			row.createCell(4).setCellValue(scannedBean.getDestination());
			row.createCell(5).setCellValue(scannedBean.getZipcode());

			row.createCell(6).setCellValue(scannedBean.getTotalPcs());
			row.createCell(7).setCellValue(scannedBean.getTotalWeight());
			row.createCell(8).setCellValue(scannedBean.getMode_service());
			row.createCell(9).setCellValue(scannedBean.getDimension());

			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		// System.out.println("file chooser: "+file.getAbsoluteFile());

		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}
		
		
// ========================================================================================

	@FXML
	public void exportToExcel_UnScannedAwb() throws SQLException, IOException {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Un-Scanned Awb");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Master Awb No");
		rowhead.createCell(2).setCellValue("Awb No");
		
		rowhead.createCell(3).setCellValue("Client");
		rowhead.createCell(4).setCellValue("Destination");
		rowhead.createCell(5).setCellValue("Zipcode");
		
		rowhead.createCell(6).setCellValue("PCS");
		rowhead.createCell(7).setCellValue("Total Weight");
		

		int i = 2;

		HSSFRow row;

		for (InwardUnScannedAwbTableBean unScannedBean : tabledata_UnScannedAwb) {
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(unScannedBean.getSlno());
			row.createCell(1).setCellValue(unScannedBean.getMasterAwb());
			row.createCell(2).setCellValue(unScannedBean.getAwbNo());

			row.createCell(3).setCellValue(unScannedBean.getClient());
			row.createCell(4).setCellValue(unScannedBean.getDestination());
			row.createCell(5).setCellValue(unScannedBean.getZipcode());

			row.createCell(6).setCellValue(unScannedBean.getTotalPcs());
			row.createCell(7).setCellValue(unScannedBean.getTotalWeight());
			
			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());

		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}
	
// **************************************************************************	
	
	public void checkPCSCount()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if(!txtDimensionPCS.getText().equals(""))
		{
			if(tabledata_Dimension.size()==0)
			{
				countPcsForValidation=Integer.valueOf(txtDimensionPCS.getText());
				System.out.println("Table size is zero: value from text box: "+countPcsForValidation);
			}
			else
			{
				countPcsForValidation=0;
				for(DimensionTableBean bean: tabledata_Dimension)
				{
					countPcsForValidation=countPcsForValidation+bean.getPcs();
				}
				
				countPcsForValidation=countPcsForValidation+Integer.valueOf(txtDimensionPCS.getText());
				System.out.println(" value from Table size: "+countPcsForValidation);
			}
		/*dimensionPcsCount=Integer.valueOf(txtDimensionPCS.getText());
		countPcsForValidation=countPcsForValidation+dimensionPcsCount;
*/	
		if(countPcsForValidation > Integer.valueOf(txtUnScannedTotalPcs.getText()))
		{
			//System.out.println("value greater :"+countPcsForValidation);
			countPcsForValidation=countPcsForValidation-dimensionPcsCount;
			alert.setTitle("Alert");
			alert.setContentText("Dimension PCS not more than Total PCS");
			alert.showAndWait();
			
			txtDimensionPCS.requestFocus();
		}
		else
		{
			//System.out.println("value smaller :"+countPcsForValidation);
			txtDimension_Length.requestFocus();
		}
		}
		
		else
		{
			txtDimension_Length.requestFocus();
		}
	}
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws Exception {
		Node n = (Node) e.getSource();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if (n.getId().equals("txtAwbNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDimensionPCS.requestFocus();
			}
			
		} 
		
		else if (n.getId().equals("txtVolWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtActualWeight.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				txtDimensionPCS.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtDimensionPCS")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				checkPCSCount();
					
			}
			else if(e.getCode().equals(KeyCode.ESCAPE))
			{
				if(rdBtnSUB.isSelected()==true)
				{
					txtDimension_Height.clear();
					txtDimension_Length.clear();
					txtDimension_Width.clear();
					
					
					double weight=0;
					
					for(DimensionTableBean bean: tabledata_Dimension)
					{
						weight=weight+bean.getWeight();
					}
					
					txtVolWeight.setText(df.format(weight));
					txtVolWeight.requestFocus();
				}
				else
				{
				txtDimension_Height.clear();
				txtDimension_Length.clear();
				txtDimension_Width.clear();
				txtDimensionPCS.clear();
				
				double weight=0;
				
				for(DimensionTableBean bean: tabledata_Dimension)
				{
					weight=weight+bean.getWeight();
				}
				
				txtVolWeight.setText(df.format(weight));
				txtVolWeight.requestFocus();
				}
			}
		}
		
		else if (n.getId().equals("txtDimension_Length")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDimension_Width.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtDimension_Width")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDimension_Height.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtDimension_Height")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				
				if(rdBtnSUB.isSelected()==true)
				{
					if(!txtDimension_Width.getText().equals("") &&
						!txtDimension_Height.getText().equals("") &&
						!txtDimension_Length.getText().equals(""))
					{
						saveDimensionDataToList();
					}
					else
					{
						
						if(!txtDimension_Width.getText().equals("") ||
								!txtDimension_Height.getText().equals("") ||
								!txtDimension_Length.getText().equals(""))
						{
							if(txtDimension_Length.getText().equals(""))
							{
								alert.setTitle("Empty Field Validation");
								alert.setContentText("You did not enter a Length");
								alert.showAndWait();
								txtDimension_Length.requestFocus();
							}
							else if(txtDimension_Width.getText().equals(""))
							{
								alert.setTitle("Empty Field Validation");
								alert.setContentText("You did not enter a Width");
								alert.showAndWait();
								txtDimension_Width.requestFocus();
							}
							else if(txtDimension_Height.getText().equals(""))
							{
								alert.setTitle("Empty Field Validation");
								alert.setContentText("You did not enter a Height");
								alert.showAndWait();
								txtDimension_Height.requestFocus();
							}
						}
						else
						{
						
						
							txtDimension_Height.clear();
							txtDimension_Length.clear();
							txtDimension_Width.clear();
							
							double weight=0;
							
							for(DimensionTableBean bean: tabledata_Dimension)
							{
								weight=weight+bean.getWeight();
							}
							
							txtVolWeight.setText(df.format(weight));
							
							txtVolWeight.requestFocus();
						}
						
					}
				}
				else
				{
				if(!txtDimensionPCS.getText().equals("") &&
						!txtDimension_Width.getText().equals("") &&
						!txtDimension_Height.getText().equals("") &&
						!txtDimension_Length.getText().equals(""))
				{
				saveDimensionDataToList();
				}
				else
				{
					if(!txtDimensionPCS.getText().equals(""))
					{
						if(txtDimension_Length.getText().equals(""))
						{
							alert.setTitle("Empty Field Validation");
							alert.setContentText("You did not enter a Length");
							alert.showAndWait();
							txtDimension_Length.requestFocus();
						}
						else if(txtDimension_Width.getText().equals(""))
						{
							alert.setTitle("Empty Field Validation");
							alert.setContentText("You did not enter a Width");
							alert.showAndWait();
							txtDimension_Width.requestFocus();
						}
						else if(txtDimension_Height.getText().equals(""))
						{
							alert.setTitle("Empty Field Validation");
							alert.setContentText("You did not enter a Height");
							alert.showAndWait();
							txtDimension_Height.requestFocus();
						}
					}
					else
					{
						txtDimension_Height.clear();
						txtDimension_Length.clear();
						txtDimension_Width.clear();
						
						double weight=0;
						
						for(DimensionTableBean bean: tabledata_Dimension)
						{
							weight=weight+bean.getWeight();
						}
						
						txtVolWeight.setText(df.format(weight));
						
						txtVolWeight.requestFocus();
					}
					
				}
				}
			
				//txtDimensionPCS.requestFocus();
				
			}
		}
		
		else if (n.getId().equals("txtActualWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(Double.valueOf(txtVolWeight.getText())>=Double.valueOf(txtActualWeight.getText()))
				{
					txtBillWeight.setText(df.format(Double.valueOf(txtVolWeight.getText())));
				}
				else
				{
					txtBillWeight.setText(df.format(Double.valueOf(txtActualWeight.getText())));
				}
				
				txtBillWeight.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtBillWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnSubmit.requestFocus();
			}
		}
		else if (n.getId().equals("btnSubmit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				if(!txtUnScannedTotalPcs.getText().equals("0"))
				{
					setValidation();
				}
				else
				{
					alert.setTitle("Empty Field Validation");
					alert.setContentText("Entered Awb No. is already Scanned...");
					alert.showAndWait();
					reset();
				}
				System.out.println("Set validation running....>>>>>>>>>>>");
				
			}
		}
			
		
	}
	
	
// *************** Method to set Validation *******************	
	
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemailid.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    boolean checkmail=matcher.matches();*/
		
		
		if(txtAwbNo.getText()==null || txtAwbNo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Awb No.");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}
		else if(txtMode.getText()==null || txtMode.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Service");
			alert.showAndWait();
			txtMode.requestFocus();
		}
		/*else if(chkDimension.isSelected()==true)
		{
			if(txtDimension_Length.getText()==null || txtDimension_Length.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Lenght (L)");
				alert.showAndWait();
				txtDimension_Length.requestFocus();
			}
			else if(txtDimension_Width.getText()==null || txtDimension_Width.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Width (W)");
				alert.showAndWait();
				txtDimension_Width.requestFocus();
			}
			else if(txtDimension_Height.getText()==null || txtDimension_Height.getText().isEmpty())
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Height (H)");
				alert.showAndWait();
				txtDimension_Height.requestFocus();
			}
			else
			{
				scanAwbNo();	
			}
			
		}*/
		
		else
		{
			scanAwbNo();
			
		}
	}	
	
	
	
// ******************************************************************************
	

	public void reset()
	{
		txtAwbNo.clear();
		txtSubAwbNo.clear();
		txtActualWeight.clear();
		txtClient.clear();
		txtDestination.clear();
		
		
		txtMode.clear();
		txtDimensionPCS.clear();
		txtTotalPcs.clear();
		txtTotalWeight.clear();
		txtZipcode.clear();
		
		txtDimension_Height.clear();
		txtDimension_Length.clear();
		txtDimension_Width.clear();
		
		

	}
	
	
// ******************************************************************************
	
	public void checkMPSandSUB()
	{
		if(rdBtnMPS.isSelected()==false)
		{
			txtSubAwbNo.setDisable(false);
			reset();
		}
		else
		{
			loadOnlyMPS_Main_Data_In_UnScannedTable();
			txtSubAwbNo.setDisable(true);
			reset();
		}
	}
	
	
// ****************************************************************
	
	public void saveDimensionDataToList() throws Exception
	{
		if(rdBtnSUB.isSelected()==true)
		{
			boolean checkSubAwbNoStatus=true;
			
			for(InwardBean inBean: list_SubAwbNo)
			{
				if(txtSubAwbNo.getText().equals(inBean.getSubAwbNo()))
				{
					checkSubAwbNoStatus=true;
					break;
				}
				else
				{
					checkSubAwbNoStatus=false;
				}
			}
			
			
			if(checkSubAwbNoStatus==true)
			{
				if(!txtDimensionPCS.getText().equals(""))
				{
					txtDimensionPCS.requestFocus();
			
					DimensionBean diBean = new DimensionBean();
					
					diBean.setAwbno(txtSubAwbNo.getText());
					diBean.setPcs(Integer.valueOf(txtDimensionPCS.getText()));
					diBean.setLength(Double.valueOf(txtDimension_Length.getText()));
					diBean.setWidth(Double.valueOf(txtDimension_Width.getText()));
					diBean.setHeight(Double.valueOf(txtDimension_Height.getText()));
					diBean.setWeight((Double.valueOf(txtDimension_Height.getText()) * Double.valueOf(txtDimension_Length.getText())
							* Double.valueOf(txtDimension_Width.getText())) / 2000);
					
					
		
					LIST_SAVEDIMENSION_DATA.add(diBean);
			
					totalpcs=totalpcs+diBean.getPcs();
					totalweight=totalweight+diBean.getWeight();
					
					tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(),null, diBean.getPcs(), 
							diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));
					
					tableDimension.setItems(tabledata_Dimension);
					
					
					int listIndex=0;
					for(InwardBean bean: list_SubAwbNo)
					{
						if(diBean.getAwbno().equals(bean.getSubAwbNo()))
						{
							list_SubAwbNo.remove(listIndex);
							break;
						}
						listIndex++;
					}
		
					tabledata_UnScannedAwb.clear();
					
					int slno=1;
					for(InwardBean subbean: list_SubAwbNo)
					{
						for(InwardBean inBean: list_Mps_With_Sub)
						{
							if(subbean.getSubAwbNo().equals(inBean.getAwbNo()))
							{
								tabledata_UnScannedAwb.add(new InwardUnScannedAwbTableBean(slno, inBean.getMasterAwb(), inBean.getAwbNo(), inBean.getClient(),
										inBean.getZipcode(), inBean.getDestination(), inBean.getTotalPcs(), inBean.getTotalWeight()));
								
								slno++;
							}
						}
					}
					tableUnScannedAwb.setItems(tabledata_UnScannedAwb);
					
					for(InwardBean bean: list_SubAwbNo)
					{
						txtSubAwbNo.setText(bean.getSubAwbNo());
						break;
					}
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setHeaderText(null);
					alert.setTitle("Alert");
					alert.setContentText("Sub Awb No is not available to add Dimension\n please enter cone");
					alert.showAndWait();
				}
			}
		
			else
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle("Alert");
				alert.setContentText("Entered Sub Awb No does not exist..");
				alert.showAndWait();
				checkSubAwbNoStatus=true;
				txtSubAwbNo.requestFocus();
			}
		}

		else
		{
			DimensionBean diBean = new DimensionBean();

			diBean.setAwbno(txtAwbNo.getText());
			diBean.setPcs(Integer.valueOf(txtDimensionPCS.getText()));
			diBean.setLength(Double.valueOf(txtDimension_Length.getText()));
			diBean.setWidth(Double.valueOf(txtDimension_Width.getText()));
			diBean.setHeight(Double.valueOf(txtDimension_Height.getText()));
			diBean.setWeight((Double.valueOf(txtDimension_Height.getText()) * Double.valueOf(txtDimension_Length.getText())
					* Double.valueOf(txtDimension_Width.getText())) / 2000);
		
			LIST_SAVEDIMENSION_DATA.add(diBean);
			
			totalpcs=totalpcs+diBean.getPcs();
			totalweight=totalweight+diBean.getWeight();
			
			tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(),null, diBean.getPcs(), 
					diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));

			tableDimension.setItems(tabledata_Dimension);
		}
		
		txtDimension_Height.clear();
		txtDimension_Length.clear();
		txtDimension_Width.clear();
		
		if (Integer.valueOf(txtUnScannedTotalPcs.getText()) == totalpcs) 
		{
			txtDimension_Height.clear();
			txtDimension_Length.clear();
			txtDimension_Width.clear();
			txtDimensionPCS.clear();
			
			txtVolWeight.setText(df.format(totalweight));
			setEnableDisable();
			txtVolWeight.requestFocus();
		}
		else
		{
			txtDimensionPCS.requestFocus();
		}
	}	
	
	
// ******************************************************************************	
	
	public void setEnableDisable()
	{
		int count=0;
		
		for(DimensionTableBean bean: tabledata_Dimension)
		{
			count=count+bean.getPcs();
		}
		
		if(count<=Integer.valueOf(txtUnScannedTotalPcs.getText()))
		{
			txtDimensionPCS.setDisable(true);
			txtDimension_Height.setDisable(true);
			txtDimension_Length.setDisable(true);
			txtDimension_Width.setDisable(true);
		}
		else
		{
			txtDimensionPCS.setDisable(false);
			txtDimension_Height.setDisable(false);
			txtDimension_Length.setDisable(false);
			txtDimension_Width.setDisable(false);
			
		}
	}
	
	
// ******************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		txtClient.setEditable(false);
		txtTotalPcs.setEditable(false);
		txtDestination.setEditable(false);
		txtTotalWeight.setEditable(false);
		txtZipcode.setEditable(false);
		txtMode.setEditable(false);
		
		txtUnScannedTotalPcs.setEditable(false);
			
		txtDimensionPCS.setEditable(false);
		txtDimension_Length.setEditable(false);
		txtDimension_Height.setEditable(false);
		txtDimension_Width.setEditable(false);
		
		txtSubAwbNo.setDisable(true);
		
		imgSearchIcon_Scanned.setImage(new Image("/icon/searchicon_blue.png"));
		imgSearchIcon_UnScanned.setImage(new Image("/icon/searchicon_blue.png"));
		
		scan_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,Integer>("slno"));
		scan_tabColumn_MasterAwb.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("masterAwb"));
		scan_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("awbNo"));
		scan_tabColumn_Client.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("client"));
		scan_tabColumn_Destination.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("destination"));
		scan_tabColumn_Zipcode.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,Integer>("zipcode"));
		scan_tabColumn_PCS.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,Integer>("totalPcs"));
		scan_tabColumn_TotalWeight.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,Double>("totalWeight"));
		scan_tabColumn_Mode.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("mode_service"));
		scan_tabColumn_Dimension.setCellValueFactory(new PropertyValueFactory<InwardScannedAwbTableBean,String>("dimension"));
		/*tableCol_Rate.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("rate"));
		tableCol_Amount.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("amount"));*/
		
		
		
		unScan_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,Integer>("slno"));
		unScan_tabColumn_MasterAwb.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,String>("masterAwb"));
		unScan_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,String>("awbNo"));
		unScan_tabColumn_Client.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,String>("client"));
		unScan_tabColumn_Destination.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,String>("destination"));
		unScan_tabColumn_Zipcode.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,Integer>("zipcode"));
		unScan_tabColumn_PCS.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,Integer>("totalPcs"));
		unScan_tabColumn_TotalWeight.setCellValueFactory(new PropertyValueFactory<InwardUnScannedAwbTableBean,Double>("totalWeight"));
		
		
		dim_tabColumn_AwbNo.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("awbno"));
		dim_tabColumn_Height.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("height"));
		dim_tabColumn_pcs.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,Integer>("pcs"));
		dim_tabColumn_Lenght.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("length"));
		dim_tabColumn_Weight.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("weight"));
		dim_tabColumn_Width.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("width"));
		
		
		//test();
		
		try {
			loadClients();
			loadModeServices();
			loadScannedInwardTable();
			loadUnScannedInwardTable();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	

}
