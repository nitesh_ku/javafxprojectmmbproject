package com.onesoft.courier.booking.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.booking.bean.CloudInwardTableBean;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.CloudInwardBean;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.common.bean.LoadClientBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;



public class CloudInwardController implements Initializable{
	
	private List<String> list_clients=new ArrayList<>();
	private Set<String> set_service=new HashSet<>();
	private List<CloudInwardBean> list_inward=new ArrayList<>();
	private List<CloudInwardBean> list_unsavedSubAwb=new ArrayList<>();
	
	public String branchCode=null;
	
	private ObservableList<CloudInwardTableBean> tabledata_Awb=FXCollections.observableArrayList();
	
	
	private int submitCount=0;
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	Pattern pattern = Pattern.compile("\\d+");		
	
	@FXML
	private Hyperlink hyperlinkInward;
	
	@FXML
	private Hyperlink hyperlinkOutWard;
	
	@FXML
	private Hyperlink hyperlinkDataEntry;
	
	@FXML
	private Label labSubAwbNo;
	
	@FXML
	private TextField txtClient;
	
	@FXML
	private TextField txtService;
	
	@FXML
	private TextField txtZipcode;
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtPCS;
	
	@FXML
	private TextField txtWeight;
	
	@FXML
	private TextField txtAwbNo;
	
	@FXML
	private TextField txtSubAwbNo;
	
	@FXML
	private Button btnSubmit;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Pagination pagination_AwbInward;
	
	@FXML
	private ImageView imgSearchIcon_AwbInward;
	
	// ===============================================	
	
	@FXML
	private TableView<CloudInwardTableBean> tableAwbInward;

	@FXML
	private TableColumn<CloudInwardTableBean, Integer> awbInward_tableCol_Slno;

	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_MasterAwbNo;
	
	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_AwbNo;

	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_BookingDate;

	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_Client;

	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_Destination;

	@FXML
	private TableColumn<CloudInwardTableBean, Integer> awbInward_tableCol_Zipcode;

	@FXML
	private TableColumn<CloudInwardTableBean, Integer> awbInward_tableCol_Pcs;

	@FXML
	private TableColumn<CloudInwardTableBean, Double> awbInward_tableCol_Weight;
	
	@FXML
	private TableColumn<CloudInwardTableBean, String> awbInward_tableCol_InwardFrom;
	
	
	Main m=new Main();
	
// ******************************************************************************

	public void showOutwardPage() throws IOException
	{
		m.showOutward();
	}

	// ******************************************************************************

	public void showDataEntryPage() throws IOException
	{
		m.showDataEntry();		
	}

	// ******************************************************************************

	public void showInwardPage() throws IOException
	{
		m.showInward();
	}		
		
	
// =============================================================================================

	public void loadClients() throws SQLException 
	{
		new LoadClients().loadClientWithName();

		for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
		{
			
			//System.out.println("Branch From client : "+bean.getBranchcode()+" | Client: "+bean.getClientCode());
			
			list_clients.add(bean.getClientName() + " | " + bean.getClientCode());

		}
		TextFields.bindAutoCompletion(txtClient, list_clients);

	}
	
	
// =============================================================================================

	public void loadServicesWithName() throws SQLException 
	{
		new LoadServiceGroups().loadServicesWithName();

		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME) 
		{
			set_service.add(bean.getServiceName() + " | " + bean.getServiceCode());

		}
		TextFields.bindAutoCompletion(txtService, set_service);

	}		

	
// ============== Method the move Cursor using Entry Key =================

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		if (n.getId().equals("txtClient")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				txtService.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtService")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				txtZipcode.requestFocus();
			}
		} 
		
		else if (n.getId().equals("txtZipcode")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				getDestinationFromZipcode();
			//	txtDestination.requestFocus();
			}
		} 
		
		else if (n.getId().equals("txtDestination")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				txtPCS.requestFocus();
			}
		}

		else if (n.getId().equals("txtPCS")) 
		{
			if (e.getCode().equals(KeyCode.ENTER))
			{
				if (!txtPCS.getText().equals("")) 
				{
					checkPcsCount();
				}
				txtWeight.requestFocus();
			}
		}

		else if (n.getId().equals("txtWeight")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				txtAwbNo.requestFocus();
			}
		}

		else if (n.getId().equals("txtAwbNo")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				if (txtSubAwbNo.isDisable() == true) 
				{
					btnSubmit.requestFocus();
					CheckAwbNoExistance(txtAwbNo.getText());
				} else {
					txtSubAwbNo.requestFocus();
					CheckAwbNoExistance(txtAwbNo.getText());
				}
			}
		}

		else if (n.getId().equals("txtSubAwbNo")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				btnSubmit.requestFocus();
			}
		}

		else if (n.getId().equals("btnSubmit")) 
		{
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				setValidation();
			}
		}
	}	
	

// =============================================================================================

	public void setValidation() throws SQLException {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		/*
		 if(comboBoxClient.getValue()==null || comboBoxClient.getValue().isEmpty()) 
		 {
		  alert.setTitle("Empty Field Validation"); 
		  alert.setContentText("Client field is Empty!"); 
		  alert.showAndWait();
		  comboBoxClient.requestFocus();
		  
		  }
		 */

		if (txtClient.getText() == null || txtClient.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Client field is Empty!");
			alert.showAndWait();
			txtClient.requestFocus();

		}

		else if (txtService.getText() == null || txtService.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Service field is Empty!");
			alert.showAndWait();
			txtService.requestFocus();

		}
		
		else if (txtZipcode.getText() == null || txtZipcode.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Zipcode field is Empty!");
			alert.showAndWait();
			txtZipcode.requestFocus();

		}

		else if (txtDestination.getText() == null || txtDestination.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Destination field is Empty!");
			alert.showAndWait();
			txtDestination.requestFocus();

		}

		else if (txtPCS.getText() != null && txtPCS.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("PCS field is Empty!");
			alert.showAndWait();
			txtPCS.requestFocus();
		}

		else if (txtWeight.getText() != null && txtWeight.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Weight field is Empty!");
			alert.showAndWait();
			txtWeight.requestFocus();
		}

		else if (txtAwbNo.getText() != null && txtAwbNo.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Awb No. field is Empty!");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}

		else if (txtSubAwbNo.isDisable() == false && txtSubAwbNo.getText() != null && txtSubAwbNo.getText().isEmpty()) {
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Sub Awb No. field is Empty!");
			alert.showAndWait();
			txtSubAwbNo.requestFocus();
		}

		else if (txtSubAwbNo.isDisable() == false && txtAwbNo.isDisable() == false) {
			if (!txtAwbNo.getText().equals(txtSubAwbNo.getText())) {
				alert.setTitle("Miss match Validation");
				alert.setContentText("Awb No. and Sub Awb No. should be same for first time...!!");
				alert.showAndWait();
				txtSubAwbNo.setText(txtAwbNo.getText());
				txtSubAwbNo.requestFocus();
			}
			else 
			{
				saveCloudInwardData();
			}

		}
		else 
		{
			saveCloudInwardData();
		}
	}
	
//=============================================================================================

	@FXML
	public void checkPcsCount()
	{
		if(Integer.valueOf(txtPCS.getText())>1)
		{
			labSubAwbNo.setDisable(false);
			txtSubAwbNo.setDisable(false);
		}
		else
		{
			labSubAwbNo.setDisable(true);
			txtSubAwbNo.setDisable(true);
		}
	}	
		
// =============================================================================================

	public void getDestinationFromZipcode() 
	{

		if (!txtZipcode.getText().equals("")) 
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);

			boolean checkZipcodeStatus = false;
			LoadCityWithZipcodeBean destinationBean = new LoadCityWithZipcodeBean();

			for (LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common) 
			{
				if (txtZipcode.getText().equals(pinBean.getPincode()))
				{
					checkZipcodeStatus = true;
					destinationBean.setCityName(pinBean.getCity_name());
					break;
				} 
				else
				{
					checkZipcodeStatus = false;
				}
			}

			if (checkZipcodeStatus == true) 
			{
				txtDestination.setText(destinationBean.getCityName());
				txtDestination.requestFocus();
			}
			else 
			{
				txtDestination.clear();
				alert.setTitle("Zipcode City Alert");
				alert.setContentText("Please enter correct zipcode");
				alert.showAndWait();
				txtZipcode.requestFocus();
			}
		}
		else 
		{
			txtDestination.requestFocus();
		}
	}

// ================== Method to check AWB No. existance =====================================		
	
	public boolean CheckAwbNoExistance(String awbno) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
	
		
		Statement st=null;
		ResultSet rs=null;
		
		boolean awbStatus=false;
		
		try
		{	
			st=con.createStatement();
			String sql="select * from dailybookingtransaction where air_way_bill_number='"+awbno+"'";
			rs=st.executeQuery(sql);
			System.out.println("Sql: " +sql);
			
			if(rs.next())
			{
				awbStatus=true;
				System.out.println("Awbno: exist: "+ txtAwbNo.getText());
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Awb No. Alert");
				alert.setHeaderText(null);
				alert.setContentText("Entered Awb No. is already exist");
				alert.showAndWait();
				txtSubAwbNo.clear();
				txtAwbNo.requestFocus();
			}
			else
			{
				if(txtSubAwbNo.isDisable()==false)
				{
				txtSubAwbNo.setText(txtAwbNo.getText());
				}
				System.out.println("Awbno: "+ txtAwbNo.getText());
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return awbStatus;
	}
	
	
	// ================== Method to check AWB No. existance =====================================		
	
		public boolean CheckSubAwbNoExistance(String subAwbno) throws SQLException
		{
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
		
			
			Statement st=null;
			ResultSet rs=null;
			
			boolean subAwbStatus=false;
			
			try
			{	
				st=con.createStatement();
				String sql="select * from dailybookingtransaction where air_way_bill_number='"+subAwbno+"'";
				rs=st.executeQuery(sql);
				System.out.println("Sql: " +sql);
				
				if(rs.next())
				{
					subAwbStatus=true;
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Sub Awb No. Alert");
					alert.setHeaderText(null);
					alert.setContentText("Entered Sub Awb No. is already exist");
					alert.showAndWait();
					//txtSubAwbNo.clear();
					txtSubAwbNo.requestFocus();
				}
				else
				{
					subAwbStatus=false;
					
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
			return subAwbStatus;
		}
			

	
// =============================================================================================
	
	public void saveCloudInwardData() throws SQLException
	{
		boolean awbExistanceStatusInDB=false;
		boolean subAwbExistanceStatusInDB=false;
		boolean checkSubAwbNoInCacheList=false;
		Matcher matcher = pattern.matcher(txtSubAwbNo.getText());
		
		if(submitCount==0)				// Check Awb No if PCS is greater than 1
		{
			awbExistanceStatusInDB=CheckAwbNoExistance(txtAwbNo.getText());
		}
		
		if(awbExistanceStatusInDB==false)
		{
			submitCount++;				// read the submit button count
		
			String[] clientSplitValue=txtClient.getText().replaceAll("\\s+","").split("\\|");
			String[] serviceSplitValue=txtService.getText().replaceAll("\\s+","").split("\\|");
		
			CloudInwardBean inwardBean=new CloudInwardBean();
			
			System.out.println("Service from cloud >>>>>>>> : "+serviceSplitValue[1]);
			
			inwardBean.setClientCode(clientSplitValue[1]);
			
			for (LoadClientBean bean : LoadClients.SET_LOAD_CLIENTWITHNAME) 
			{
				//System.out.println("Branch >>  "+bean.getBranchcode()+" | Client: "+bean.getClientCode()+" | selected Client: "+clientSplitValue[1]);
				if(clientSplitValue[1].equals(bean.getClientCode()))
				{
					//System.out.println("Branch >>  "+branchCode);
					branchCode=bean.getBranchcode();
					break;
				}

			}
			
			inwardBean.setService_Mode(serviceSplitValue[1]);
			inwardBean.setZipcode(Integer.valueOf(txtZipcode.getText()));
			inwardBean.setDestination(txtDestination.getText());
			inwardBean.setPcs(Integer.valueOf(txtPCS.getText()));
			inwardBean.setWeight(Double.valueOf(txtWeight.getText()));
			//inwardBean.setOrigin(LoadOrigin.BRANCH_PINCODE);
			inwardBean.setOrigin(CommonVariable.USER_BRANCH_PINCODE);
			inwardBean.setBranchCode(branchCode);
			
			
			if(txtSubAwbNo.isDisable()==false)				// condition for: check Sub Awb textfield is disable or not
			{	
				subAwbExistanceStatusInDB=CheckSubAwbNoExistance(txtSubAwbNo.getText());
				
				if(subAwbExistanceStatusInDB==false)		// Condition for: Is Sub Awb No already added in the cache list or not
				{
					for(CloudInwardBean bean:list_inward)
					{
						if(bean.getAwbNo().equals(txtSubAwbNo.getText()))
						{
							checkSubAwbNoInCacheList=true;
							break;
						}
					}
					
					if(checkSubAwbNoInCacheList==false)
					{
						inwardBean.setMasterAwbNo(txtAwbNo.getText());
						inwardBean.setAwbNo(txtSubAwbNo.getText());
						
						
						inwardBean.setMps("T");
						
						if(list_inward.size()==0) // condition for: check main or sub MPS
						{
							inwardBean.setMps_type("T");
						}
						else
						{
							inwardBean.setMps_type("F");
						}
						
						if(matcher.matches())		// check entered Sub Awb no is alphnumeric or not
						{
							inwardBean.setSubAwbNo(txtSubAwbNo.getText());
							
						}
						else
						{
							inwardBean.setSubAwbNo(txtSubAwbNo.getText());
							inwardBean.setSubAwb_Prefix(inwardBean.getSubAwbNo().replaceAll("[^A-Za-z]+", "").toUpperCase());
							inwardBean.setSubAwb_suffix(Long.parseLong(inwardBean.getSubAwbNo().replaceAll("\\D+","")));
						}
						
						if(Integer.valueOf(txtPCS.getText()) >1 && submitCount>=1)		// if PCS greater than 1, then disable all the fields except sub awb field after first click on submit
						{	
							if(matcher.matches())
							{	
								txtSubAwbNo.setText(String.valueOf(Long.valueOf(inwardBean.getSubAwbNo())+1));
							}
							else
							{
								txtSubAwbNo.setText(inwardBean.getSubAwb_Prefix()+(inwardBean.getSubAwb_suffix()+1));
							}
						
							txtClient.setDisable(true);
							txtAwbNo.setDisable(true);
							txtService.setDisable(true);
							txtDestination.setDisable(true);
							txtPCS.setDisable(true);
							txtWeight.setDisable(true);
							txtZipcode.setDisable(true);
							txtSubAwbNo.requestFocus();
							
						}
						
						list_inward.add(inwardBean);
					}
					else
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Sub Awb No. Alert");
						alert.setHeaderText(null);
						alert.setContentText("Entered Sub Awb No. is already Added to list");
						alert.showAndWait();
						//txtSubAwbNo.clear();
						txtSubAwbNo.requestFocus();
					}
				}
				else
				{
					if(matcher.matches())		// check entered Sub Awb no is alphnumeric or not
					{
						inwardBean.setSubAwbNo(txtSubAwbNo.getText());
					}
					else
					{
						inwardBean.setSubAwbNo(txtSubAwbNo.getText());
						inwardBean.setSubAwb_Prefix(inwardBean.getSubAwbNo().replaceAll("[^A-Za-z]+", "").toUpperCase());
						inwardBean.setSubAwb_suffix(Long.parseLong(inwardBean.getSubAwbNo().replaceAll("\\D+","")));
					}
				}
				
			}
			else
			{
				inwardBean.setAwbNo(txtAwbNo.getText());
				inwardBean.setMasterAwbNo(txtAwbNo.getText());		
				
				inwardBean.setMps("F");
				inwardBean.setMps_type("T");
				list_inward.add(inwardBean);
			}
			
			
			
			if(Integer.valueOf(txtPCS.getText())==list_inward.size())
			{
				
				txtClient.setDisable(false);
				txtService.setDisable(false);
				txtAwbNo.setDisable(false);
				txtDestination.setDisable(false);
				txtPCS.setDisable(false);
				txtSubAwbNo.setDisable(false);
				txtWeight.setDisable(false);
				txtZipcode.setDisable(false);
				txtSubAwbNo.setDisable(true);
				
				txtClient.requestFocus();
				
				
				for(CloudInwardBean clBean:list_inward)
				{
					System.out.println("Client: "+clBean.getClientCode()+" >> Master AwbNo: "+clBean.getMasterAwbNo()+" >> Destination: "+clBean.getDestination()+
							">> PCS: "+clBean.getPcs()+" >> Awb: "+clBean.getAwbNo()+" >> MPS: "+clBean.getMps()+" >> MPS TYPE: "+clBean.getMps_type());
				}
				
				submitCount=0;
				saveMasterAndAwbToUsingInward();
			}
		}
	}
	
	
// =============================================================================================	
	
	public void saveMasterAndAwbToUsingInward() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		int pcsCount=0;
		
		try
		{	
			for(CloudInwardBean clBean:list_inward)
			{
				System.out.println("From saveMaster  >>>  service: "+clBean.getService_Mode());
				
				pcsCount++;
				
				query = "insert into dailybookingtransaction(booking_date,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2city,"
						+ "packets,billing_weight,zipcode,create_date,lastmodifieddate,user_name,master_awbno,mps,mps_type,inward_form,"
						+ "travel_status,dailybookingtransaction2service,origin,dailybookingtransaction2branch,spot_rate) "
						+ "values(CURRENT_DATE,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?)";
				preparedStmt = con.prepareStatement(query);
			
			
				preparedStmt.setString(1, clBean.getAwbNo());
				preparedStmt.setString(2, clBean.getClientCode().trim());
				preparedStmt.setString(3, clBean.getDestination());
				
				if(pcsCount==1)
				{
					preparedStmt.setInt(4, clBean.getPcs());	
				}
				else
				{
					preparedStmt.setInt(4, 1);
				}
				
				preparedStmt.setDouble(5, clBean.getWeight());
				preparedStmt.setLong(6, clBean.getZipcode());
				preparedStmt.setString(7, clBean.getLoggedInUser());
				preparedStmt.setString(8, clBean.getMasterAwbNo());
				preparedStmt.setString(9, clBean.getMps());
				preparedStmt.setString(10, clBean.getMps_type());
				preparedStmt.setString(11, "software");
				preparedStmt.setString(12, "app");
				preparedStmt.setString(13, clBean.getService_Mode());
				preparedStmt.setString(14, clBean.getOrigin());
				preparedStmt.setString(15, clBean.getBranchCode());
				preparedStmt.setString(16, "F");
				
				
				System.out.println("PRP SQL: "+preparedStmt);
				
			
				preparedStmt.executeUpdate();
				
			 	insertWeightWithPrefix(clBean.getAwbNo(), String.valueOf(clBean.getWeight()), "app");
			}
		}
		catch(Exception e)
		{
				System.out.println(e);
				e.printStackTrace();
		}	
		finally
		{	
			list_inward.clear();
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Submission Alert");
			alert.setHeaderText(null);
			alert.setContentText("Inward submission process successfully completed");
			alert.showAndWait();
			loadAwbInwardTable();
			
			reset();
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}
	
	
	
// ======================== Method used for Pickup App web services ==========================	
	
	public void insertWeightWithPrefix(String awbNo,String weight,String prefix) throws SQLException
	{
		//String dataSavedInDBTStatus="false";

		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;

		try
		{	
			query = "insert into consignment_weight(awb_no,weight_prefix,weight,create_date,lastmodifieddate) values(?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";
			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, awbNo);
			preparedStmt.setString(2, prefix);
			preparedStmt.setString(3, weight);

			int i=	preparedStmt.executeUpdate();
		}

		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}

		//return dataSavedInDBTStatus;
	}		

		
// =============================================================================================
	
	public void reset()
	{
		txtClient.clear();
		txtZipcode.clear();
		txtDestination.clear();
		txtPCS.clear();
		txtWeight.clear();
		txtAwbNo.clear();
		txtSubAwbNo.clear();
		txtService.clear();
		
	}
	
// ================= Method to Load Un-allocated table data ==================

	public void loadAwbInwardTable() throws SQLException {
		tabledata_Awb.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		CloudInwardBean clBean = new CloudInwardBean();

		int slno = 1;
		try {

			stmt = con.createStatement();
			sql = "select master_awbno,air_way_bill_number,booking_date,dailybookingtransaction2client, dailybookingtransaction2city,zipcode,"
					+ "packets,billing_weight,inward_form from dailybookingtransaction where travel_status='app' and mps IS NOT NULL "
					+ "and mps_type IS NOT NULL order by dailybookingtransactionid DESC";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				clBean.setSlno(slno);
				clBean.setAwbNo(rs.getString("air_way_bill_number"));
				clBean.setBookingDate(date.format(rs.getDate("booking_date")));
				clBean.setClientCode(rs.getString("dailybookingtransaction2client"));
				clBean.setDestination(rs.getString("dailybookingtransaction2city"));
				clBean.setZipcode(rs.getInt("zipcode"));
				clBean.setPcs(rs.getInt("packets"));
				clBean.setWeight(rs.getDouble("billing_weight"));
				clBean.setMasterAwbNo(rs.getString("master_awbno"));
				clBean.setInwardFrom(rs.getString("inward_form"));

				tabledata_Awb.add(new CloudInwardTableBean(clBean.getSlno(), clBean.getClientCode(),
						clBean.getDestination(), clBean.getAwbNo(), clBean.getBookingDate(), clBean.getMasterAwbNo(),
						clBean.getZipcode(), clBean.getPcs(), clBean.getWeight(),clBean.getInwardFrom()));

				slno++;
			}

			tableAwbInward.setItems(tabledata_Awb);

			pagination_AwbInward.setPageCount((tabledata_Awb.size() / rowsPerPage() + 1));
			pagination_AwbInward.setCurrentPageIndex(0);
			pagination_AwbInward.setPageFactory((Integer pageIndex) -> createPage_AwbInward(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}		
	
	
// =========================== Code for paginatation ==============================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 20;
	}

	public GridPane createPage_AwbInward(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_Awb.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_Awb.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableAwbInward.setItems(FXCollections.observableArrayList(
						tabledata_Awb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableAwbInward.setItems(FXCollections.observableArrayList(
						tabledata_Awb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;
	}	
	
// =============================================================================================
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		labSubAwbNo.setDisable(true);
		txtSubAwbNo.setDisable(true);
		txtDestination.setEditable(false);
		
		LoadCity loadCity=new  LoadCity();
		
		
		imgSearchIcon_AwbInward.setImage(new Image("/icon/searchicon_blue.png"));
		
		awbInward_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,Integer>("slno"));
		awbInward_tableCol_AwbNo.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("awbNo"));
		awbInward_tableCol_BookingDate.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("bookingDate"));
		awbInward_tableCol_Client.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("clientCode"));
		awbInward_tableCol_Destination.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("destination"));
		awbInward_tableCol_Zipcode.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,Integer>("zipcode"));
		awbInward_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,Integer>("pcs"));
		awbInward_tableCol_Weight.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,Double>("weight"));
		awbInward_tableCol_MasterAwbNo.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("masterAwbNo"));
		awbInward_tableCol_InwardFrom.setCellValueFactory(new PropertyValueFactory<CloudInwardTableBean,String>("inwardFrom"));
				
		
		try {
		
			
			loadServicesWithName();
			loadClients();
			loadAwbInwardTable();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
