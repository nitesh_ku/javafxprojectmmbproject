package com.onesoft.courier.booking.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import com.onesoft.courier.booking.bean.DataEntryBean;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class SelectedVASItemPreviewController implements Initializable{
	
	//private ObservableList<String> listview_SelectedVASItems;
	private ObservableList<String> listview_SelectedVASItems=FXCollections.observableArrayList();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	@FXML
	private ListView<String> listview_selectedVAS;
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, IOException {
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("listview_selectedVAS")) {
			if (e.getCode().equals(KeyCode.ESCAPE)) {
				Main.selectedVASITemStage.hide();
				
			}
		}

	}
	
	
	public void loadSelectedVASItems()
	{
		listview_SelectedVASItems.clear();
		
		for(DataEntryBean dBean: DataEntryController.list_All_Amount_and_selectedVasItems)
		{
			
			System.out.println("Basic Amt >> "+dBean.getBasicAmount());
			System.out.println("Fuel Amt >> "+dBean.getFuelAmount());
			System.out.println("Invoice Amt >> "+dBean.getInvoiceAmount());
			System.out.println("Insurance Amt >> "+dBean.getInsuranceAmount());
			System.out.println("DOI VAS Amt >> "+dBean.getDoi_VAS());
			System.out.println("Hold at ofc Amt >> "+dBean.getHoldAtOffice_VAS());
			System.out.println("OCT Fee Amt >> "+dBean.getOct_Fee_VAS());
			System.out.println("OSS VAS Amt >> "+dBean.getOss_VAS());
			System.out.println("Green Tax VAS Amt >> "+dBean.getGreenTax_VAS());
			System.out.println("Reverse Pickup Amt >> "+dBean.getReversePickup_VAS());
			System.out.println("COD Amt >> "+dBean.getCod_amount_vas());
			System.out.println("Add. Corrct. Amt >> "+dBean.getAddressCorrection_VAS());
			System.out.println("To Pay Amt >> "+dBean.getToPay_VAS());
			System.out.println("TDD Amt >> "+dBean.getTdd_VAS());
			System.out.println("Cirtical Service Amt >> "+dBean.getCriticalService_VAS());
			System.out.println("HFD Charges Amt >> "+dBean.getHfd_Charges_VAS());
			System.out.println("HFD Floor Amt >> "+dBean.getHfd_floor_number());
			System.out.println("Other VAS Amt >> "+dBean.getOtherVasTotal());
			System.out.println("Taxable Amt >> "+dBean.getTaxableValueAmount());
			System.out.println("CGST Amt >> "+dBean.getTax_amount1());
			System.out.println("SGST Amt >> "+dBean.getTax_amount2());
			System.out.println("IGST Amt >> "+dBean.getTax_amount3());
			System.out.println("GST Total Amt >> "+dBean.getGst());
			System.out.println("Grand Total Amt >> "+dBean.getTotal());
		}
		
		
		for(DataEntryBean dBean: DataEntryController.list_All_Amount_and_selectedVasItems)
		{
			
			listview_SelectedVASItems.add("---------- FREIGHT CHARGES ----------");
			
				listview_SelectedVASItems.add("Amount            : "+df.format(dBean.getBasicAmount()));
			
			
			listview_SelectedVASItems.add("\n");
			listview_SelectedVASItems.add("---------- FOV CHARGES ----------");
			
				if(dBean.getInvoiceAmount()>0)
				{
					listview_SelectedVASItems.add("Insurance Value   : "+df.format(dBean.getInvoiceAmount()));
				}
				
				if(dBean.getInsuranceAmount()>0)
				{
					listview_SelectedVASItems.add("Insurance Amt @"+df.format(dBean.getInsuranceRate())+"  : "+df.format(dBean.getInsuranceAmount()));
				}
			
			listview_SelectedVASItems.add("\n");
			listview_SelectedVASItems.add("---------- VAS CHARGES ----------");
			
			
				if(dBean.getDocketCharges()>0)
				{
					listview_SelectedVASItems.add("Docket Charges Amt: "+df.format(dBean.getDocketCharges()));
				}
				
				if(dBean.getOda_VAS()>0)
				{
					listview_SelectedVASItems.add("ODA Amt           : "+df.format(dBean.getOda_VAS()));
				}
				
				if(dBean.getOpa_VAS()>0)
				{
					listview_SelectedVASItems.add("OPA Amt           : "+df.format(dBean.getOpa_VAS()));
				}
				
				if(dBean.getDoi_VAS()>0)
				{
					listview_SelectedVASItems.add("DOI VAS Amt       : "+df.format(dBean.getDoi_VAS()));
				}
				
				if(dBean.getHoldAtOffice_VAS()>0)
				{
					listview_SelectedVASItems.add("Hold at Office Amt: "+df.format(dBean.getHoldAtOffice_VAS()));
				}
				
				if(dBean.getOct_Fee_VAS()>0)
				{
					listview_SelectedVASItems.add("OCT Fee Amt       : "+df.format(dBean.getOct_Fee_VAS()));
				}
				
				if(dBean.getOss_VAS()>0)
				{
					listview_SelectedVASItems.add("OSS VAS Amt       : "+df.format(dBean.getOss_VAS()));
				}
				
				if(dBean.getGreenTax_VAS()>0)
				{
					listview_SelectedVASItems.add("Green Tax VAS Amt : "+df.format(dBean.getGreenTax_VAS()));
				}
				
				if(dBean.getReversePickup_VAS()>0)
				{
					listview_SelectedVASItems.add("Reverse Pickup Amt: "+df.format(dBean.getReversePickup_VAS()));
				}
				
				if(dBean.getCod_amount_vas()>0)
				{
					listview_SelectedVASItems.add("COD Amt           : "+df.format(dBean.getCod_amount_vas()));
				}
				
				if(dBean.getAddressCorrection_VAS()>0)
				{
					listview_SelectedVASItems.add("Addr. Corrct. Amt : "+df.format(dBean.getAddressCorrection_VAS()));
				}
				
				if(dBean.getToPay_VAS()>0)
				{
					listview_SelectedVASItems.add("To Pay Amt        : "+df.format(dBean.getToPay_VAS()));
				}
				
				if(dBean.getTdd_VAS()>0)
				{
					listview_SelectedVASItems.add("TDD Amt           : "+df.format(dBean.getTdd_VAS()));
				}
				
				if(dBean.getCriticalService_VAS()>0)
				{
					listview_SelectedVASItems.add("Criti. Service Amt: "+df.format(dBean.getCriticalService_VAS()));
				}
				
				if(dBean.getHfd_Charges_VAS()>0)
				{
					listview_SelectedVASItems.add("HFD Charges       : "+df.format(dBean.getHfd_Charges_VAS()));
					listview_SelectedVASItems.add("HFD No. of Floors : "+dBean.getHfd_floor_number());
				}
				
				if(dBean.getOtherVasTotal()>0)
				{
					listview_SelectedVASItems.add("Other VAS Amt     : "+df.format(dBean.getOtherVasTotal()));
				}
				
					listview_SelectedVASItems.add("\n");
					listview_SelectedVASItems.add("---------- FOV CHARGES ----------");
				
				if(dBean.getFuelAmount()>0)
				{
					listview_SelectedVASItems.add("Fuel Amt          : "+df.format(dBean.getFuelAmount()));
				}
			
			
			listview_SelectedVASItems.add("\n");
			listview_SelectedVASItems.add("---------- FINAL AMOUNT AND GST ----------");
			
				listview_SelectedVASItems.add("Taxable Amt       : "+df.format(dBean.getTaxableValueAmount()));
				listview_SelectedVASItems.add("CGST              : "+df.format(dBean.getTax_amount1()));
				listview_SelectedVASItems.add("SGST              : "+df.format(dBean.getTax_amount2()));
				listview_SelectedVASItems.add("IGST              : "+df.format(dBean.getTax_amount3()));
				listview_SelectedVASItems.add("GST Total         : "+df.format(dBean.getGst()));
				listview_SelectedVASItems.add("Grand Total       : "+df.format(dBean.getTotal()));
		
		}
		
		listview_selectedVAS.setItems(listview_SelectedVASItems);
		//DataEntryController.list_All_Amount_and_selectedVasItems.clear();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		loadSelectedVASItems();
		
	}
	
	

}
