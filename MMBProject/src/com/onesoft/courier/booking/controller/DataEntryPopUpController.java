package com.onesoft.courier.booking.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.DataEntryBean;
import com.onesoft.courier.booking.bean.DimensionBean;
import com.onesoft.courier.booking.bean.DimensionTableBean;
import com.onesoft.courier.booking.bean.Oda_Opa_Bean;
import com.onesoft.courier.booking.bean.VAS_CalculationBean;
import com.onesoft.courier.calculation.NewCalculateWeight;
import com.onesoft.courier.calculation.TaxCalculator;
import com.onesoft.courier.calculation.bean.TaxCalculatorBean;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadForwarderDetails;
import com.onesoft.courier.common.LoadPincodeForAll;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadVASItems;
import com.onesoft.courier.common.bean.LoadCityBean;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.common.bean.LoadForwarderDetailsBean;
import com.onesoft.courier.common.bean.LoadPincodeBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.common.bean.LoadVAS_Item_Bean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.master.vas.bean.ClientVASBean;
import com.onesoft.courier.master.vas.bean.VAS_Table_Bean;
import com.onesoft.courier.sql.queries.MasterSQL_DataEntry_Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class DataEntryPopUpController implements Initializable{
	
	public static String awbno=null;
	
	public static String fromZone=null;
	public static String toZone=null;
	
	
	public static String CLIENT=null;
	public static String SERVICE=null;
	public static String CLIENT_STATIC_CITY_NAME="NEW DELHI";
	public static String CLIENT_STATIC_STATE_CODE="DL";
	
	public static String CLIENT_DB_CITY_NAME=null;
	public static String CLIENT_DB_STATE_CODE=null;
	
	
	double totalVAS_amount=0;
	double opa_amount=0;
	double oda_amount=0;
	int slno_Vas_Table=0;
	double fuelRate=0;
	
	public static int TotalPCS=0;
	public static double volWeightFromDimension=0.0;
	
	public static double Dimension__totalweight=0.0;
	public static double Dimension__finalweight=0.0;
	public static int Dimension_totalPcs=0;
	
	public static List<DimensionBean> LIST_SAVEDIMENSION_DATA=new ArrayList<>();
	
	
	private ObservableList<DimensionTableBean> tabledata_Dimension=FXCollections.observableArrayList();
	private ObservableList<VAS_Table_Bean> tabledata_VAS=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList("Dox","Non-Dox");
	private ObservableList<String> comboBoxInsuranceItems=FXCollections.observableArrayList("Carrier","Owner");
	
	private ObservableList<String> comboBoxConsigNOR_StateItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxConsigNOR_CountryItems=FXCollections.observableArrayList("IN");
	
	//private ObservableList<String> comboBoxConsigNEE_StateItems=FXCollections.observableArrayList();
	//private ObservableList<String> comboBoxConsigNEE_CountryItems=FXCollections.observableArrayList("IN");
	
	private List<String> list_City=new ArrayList<>();
	private List<String> list_ForwarderCodewithName=new ArrayList<>();
	private List<String> list_Origin=new ArrayList<>();
	private List<String> list_Destination=new ArrayList<>();
	private List<String> list_VAS=new ArrayList<>();
//	public static HashMap<String, String> hashmap_ODA_OPA_FromPincode=new HashMap<>();
	public static List<Oda_Opa_Bean> List_ODA_OPA_FromPincode=new ArrayList<>();
	private Set<String> list_State=new HashSet<>();
	public static List<TaxCalculatorBean> LIST_GST_TAXES=new ArrayList<>();
	private List<ClientVASBean> list_ClientVAS=new ArrayList<>();
	private List<ClientVASBean> list_SelectedVAS=new ArrayList<>();
	
	private List<String> list_All_amounts=new ArrayList<>();
	
	
	/*
	public double doi_global;
	public double holdAtOffice_global;
	public double oct_Fee_global;
	public double hfd_charge_global;
	public double green_Tax_global;
	public double reverse_Pickup_global;
	public double tdd_global;
	public double critical_Service_global;
	public double address_Correction_global;
	
	*/
	
//	public DataEntryBean deBean_global=new DataEntryBean();
	
	public DataEntryBean deBean_global;//=new DataEntryBean();
	public static List<DataEntryBean> list_All_Amount_and_selectedVasItems=new ArrayList<>();

	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	int consignorConsigneeSaveStatus=0;
	
	
	double volWeight=0;
	
	boolean checkConsignorConsigneeExistanceStatus=false;
	boolean isDataAvailable=false;
	
	@FXML
	private Hyperlink hyperlinkInward;
	
	@FXML
	private Hyperlink hyperlinkOutWard;
	
	@FXML
	private Hyperlink hyperlinkCloudInward;
	
	/*@FXML
	private ImageView imgSearchIcon;*/
	
	/*@FXML
	private CheckBox checkBoxDimension;*/
	
	/*@FXML
	private Pagination pagination;*/
	
	
// ****************************************************************
	
	@FXML
	private TextField txtAwbNo;

	@FXML
	private TextField txtBookingDate;
	
	@FXML
	private TextField txtOrigin; 
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtNetwork;
	
	@FXML
	private TextField txtService;
	
	@FXML
	private TextField txt_Consignor_Phone;
	
	@FXML
	private TextField txt_Consignor_Name;
	
	@FXML
	private TextField txt_Consignor_Address;
	
	@FXML
	private TextField txt_Consignor_ContactPerson;
	
	@FXML
	private TextField txt_Consignor_Zipcode;
	
	@FXML
	private TextField txt_Consignor_City;
	
	@FXML
	private TextField txt_Consignor_Email;
	
	@FXML
	private TextField txt_Consignee_Phone;
	
	@FXML
	private TextField txt_Consignee_Name;
	
	@FXML
	private TextField txt_Consignee_Address;
	
/*	@FXML
	private TextField txt_Consignee_ContactPerson;*/
	
	@FXML
	private TextField txt_Consignee_Zipcode;
	
	/*@FXML
	private TextField txt_Consignee_City;*/
	
	@FXML
	private TextField txt_Consignee_Email;
	
	/*
	@FXML
	private TextField txt_Consignee_Remarks;*/
	 
	@FXML
	private TextField txtPcs;
	
	@FXML
	private TextField txtActualWeight;
	
	@FXML
	private TextField txtVolumeWeight;
	
	@FXML
	private TextField txtBillWeight;
	
	@FXML
	private TextField txtInvoiceNo;
	
	@FXML
	private TextField txtInvoiceAmount;
	
	@FXML
	private TextField txtInsuranceRate;
	
	@FXML
	private TextField txtInsuranceAmount;

	@FXML
	private TextField txtBasicAmount;
	
	@FXML
	private TextField txtFuelAmount;
	
	@FXML
	private TextField txtTotalVASAmount;

	@FXML
	private TextField txtClient;
	
	@FXML
	private TextField txtForwarderNo;
	
	@FXML
	private TextField txtForwarderService;
	
	@FXML
	private TextField txtDocketCharges;
	
	@FXML
	private TextField txtDimension_Length;
	
	@FXML
	private TextField txtDimension_Width;
	
	@FXML
	private TextField txtDimension_Height;
	
	@FXML
	private TextField txtAddOnExpense;
	
	@FXML
	private TextField txtExpenseAmount;
	
	@FXML
	private TextField txtTaxableValue;

	@FXML
	private TextField txtHFD_Floors;
	
	@FXML
	private TextField txtForwarderAccount;
	
	@FXML
	private TextField txtGSTAmt;
	
	@FXML
	private TextField txtTotal;
	
	@FXML
	private TextField txtPCS_To_addDimensions;
	
	
// ****************************************************************
	/*@FXML
	private Button btnVASPreview;*/
	
	@FXML
	private CheckBox checkBoxDOI;
	
	@FXML
	private CheckBox checkBoxHoldAtOffice;
	
	@FXML
	private CheckBox checkBoxOCT_Fee;
	
	@FXML
	private CheckBox checkBoxHFDCharges;
	
	@FXML
	private CheckBox checkBoxGreenTax;
	
	@FXML
	private CheckBox checkBoxCOD;
	
	@FXML
	private CheckBox checkBoxFOD;
	
	@FXML
	private CheckBox checkBoxAddressCorrection;
	
	@FXML
	private CheckBox checkBoxReversePickup;
	
	@FXML
	private CheckBox checkBoxToPay;
	
	@FXML
	private CheckBox checkBoxOSS;
	
	@FXML
	private CheckBox checkBoxHFD_Charges;
	
	@FXML
	private RadioButton rdBtnTDD;
	
	@FXML
	private RadioButton rdBtnCrit;
	
	
// ****************************************************************

	@FXML
	private ComboBox<String> combBox_Consignor_State;
	
/*	@FXML
	private ComboBox<String> combBox_Consignee_State;*/
	
	@FXML
	private ComboBox<String> combBox_Consignor_Country;
	
	/*@FXML
	private ComboBox<String> combBox_Consignee_Country;*/
	
	@FXML
	private ComboBox<String> combBox_Type;
	
	@FXML
	private ComboBox<String> combBox_Insurance;
	

// ****************************************************************	

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnClose;
	
	/*@FXML
	private Button btnExportToExcel;*/
	
	
	
// ***************************************

	@FXML
	private TableView<DimensionTableBean> tableDimension;

	@FXML
	private TableColumn<DimensionTableBean, Integer> dim_tabColumn_pcs;

	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Lenght;

	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Weight;

	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Width;

	@FXML
	private TableColumn<DimensionTableBean, String> dim_tabColumn_Height;	


// ***************************************

	@FXML
	private TableView<VAS_Table_Bean> tableVAS;

	@FXML
	private TableColumn<VAS_Table_Bean, Integer> vas_tabColumn_slno;

	@FXML
	private TableColumn<VAS_Table_Bean, String> vas_tabColumn_vasCode;
	
	@FXML
	private TableColumn<VAS_Table_Bean, String> vas_tabColumn_vasName;

	@FXML
	private TableColumn<VAS_Table_Bean, Double> vas_tabColumn_vasAmount;	

	
	Main m=new Main();
		


// ****************************************************************	
	@FXML
	public void saveDataEntry() throws SQLException
	{
		
		if(!txtAwbNo.getText().equals(""))
		{
			saveConsigneeConsignorDetails();
			//showConsigneeConsignorDetails();
			//checkConsigNORDetails();
			//checkConsigNEEDetails();
		updateConsignorConsigneeDetailsInDailyBookingTransaction();
		}
		
	}
	
	
	
// ******************************************************************************

/*	public void loadCity() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_City.add(bean.getCityName());
		}

	//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		TextFields.bindAutoCompletion(txt_Consignor_City, list_City);

	}	*/
	
	
// ******************************************************************************

	public void loadCityFromPincode() throws SQLException 
	{
		new LoadCity().loadCityWithNameFromPinCodeMaster();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
		{
			list_City.add(bean.getCityName());

		}

		//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		//TextFields.bindAutoCompletion(txtOrigin, list_Origin);
		TextFields.bindAutoCompletion(txt_Consignor_City, list_City);

	}
	
// ******************************************************************************

	public void loadForwarder() throws SQLException 
	{
		new LoadForwarderDetails().loadForwarderCodeWithName();

		for (LoadForwarderDetailsBean bean : LoadForwarderDetails.LIST_FORWARDER_DETAILS) 
		{
			list_ForwarderCodewithName.add(bean.getForwarderCode()+" | "+bean.getForwarderName());

		}

		//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		//TextFields.bindAutoCompletion(txtOrigin, list_Origin);
		TextFields.bindAutoCompletion(txtForwarderAccount, list_ForwarderCodewithName);

	}
	
// ******************************************************************************

	public void loadOrigin_and_Destination() throws SQLException 
	{
		new LoadCity().loadZipcodeCityDetails();

		for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
		{
			list_Origin.add(bean.getZipcode()+" | "+bean.getCityName());
			list_Destination.add(bean.getZipcode()+" | "+bean.getCityName());
		}

		//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		TextFields.bindAutoCompletion(txtOrigin, list_Origin);
		TextFields.bindAutoCompletion(txtDestination, list_Destination);

	}	
		
		
// ******************************************************************************

/*	public void loadDestination() throws SQLException 
	{
		new LoadCity().loadCityWithNameFromPinCodeMaster();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
		{
			list_Destination.add(bean.getCityName());
		}
		//	TextFields.bindAutoCompletion(txt_Consignee_City, list_City);
		TextFields.bindAutoCompletion(txtDestination, list_Destination);
	}		
*/
	
// ******************************************************************************

	public void loadAddOnExpense() throws SQLException 
	{
		new LoadVASItems().loadVASWithName();

		for (LoadVAS_Item_Bean bean : LoadVASItems.SET_LOAD_VAS_WITH_NAME) 
		{		
			list_VAS.add(bean.getVasName()+" | "+bean.getVasCode());
		}

		TextFields.bindAutoCompletion(txtAddOnExpense, list_VAS);
		

	}		

// ******************************************************************************
	
	public void loadState() throws SQLException 
	{
		new LoadCity().loadCityWithName();

		for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME) 
		{
			list_State.add(bean.getStateCode());
			
		}
		
		for(String state:list_State)
		{
			comboBoxConsigNOR_StateItems.add(state);
			//comboBoxConsigNEE_StateItems.add(state);
		}

		combBox_Consignor_State.setItems(comboBoxConsigNOR_StateItems);
		//combBox_Consignee_State.setItems(comboBoxConsigNEE_StateItems);
	}
	


	
// ******************************************************************************


	/*public void autoFillConsigNORStateViaCity() throws SQLException 
	{

		if (txt_Consignor_City.getText() == null || txt_Consignor_City.getText().isEmpty()) 
		{

		}
		else if (!txt_Consignor_City.getText().equals("") || !txt_Consignor_City.getText().isEmpty()) 
		{
			for (LoadCityBean cityBean : LoadCity.SET_LOAD_CITYWITHNAME) 
			{
				if (txt_Consignor_City.getText().equals(cityBean.getCityName())) 
				{
					combBox_Consignor_State.setValue(cityBean.getStateCode());
					combBox_Consignor_Country.setValue("IN");
					break;
				}
			}
		} 
		else 
		{
			combBox_Consignor_State.getSelectionModel().clearSelection();
			combBox_Consignor_Country.getSelectionModel().clearSelection();
		}
	}*/
	
	public void setStateAndCountry() 
	{
		if (txt_Consignor_City.getText() == null || txt_Consignor_City.getText().isEmpty()) 
		{

		}
		else 
		{
			//String[] citycode = txtCity.getText().replaceAll("\\s+", "").split("\\|");

			for (LoadCityBean bean : LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER) 
			{
				if (txt_Consignor_City.getText().equals(bean.getCityName())) 
				{
					combBox_Consignor_State.setValue(bean.getStateCode());
					break;
				}
			}
			
			combBox_Consignor_Country.setValue("IN");

			/*for (LoadCityBean countryBean : LoadState.SET_LOAD_STATEWITHNAME)
			{
				if (comboBoxState.getValue().equals(countryBean.getStateCode())) 
				{
					comboBoxCountry.setValue(countryBean.getCountryCode());
					break;
				}
			}*/

		}

	}
	
	
// ******************************************************************************

	/*public void autoFillConsigNEEStateViaCity() throws SQLException 
	{
		if (txt_Consignee_City.getText() == null || txt_Consignee_City.getText().isEmpty()) 
		{

		} 
		else if (!txt_Consignee_City.getText().equals("") || !txt_Consignee_City.getText().isEmpty()) 
		{
			for (LoadCityBean cityBean : LoadCity.SET_LOAD_CITYWITHNAME) 
			{
				if (txt_Consignee_City.getText().equals(cityBean.getCityName())) 
				{
					combBox_Consignee_State.setValue(cityBean.getStateCode());
					combBox_Consignee_Country.setValue("IN");
					break;
				}
			}
		} 
		else 
		{
			combBox_Consignee_State.getSelectionModel().clearSelection();
			combBox_Consignee_Country.getSelectionModel().clearSelection();
		}
	}	*/
	
	
// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, IOException {
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("txtAwbNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Phone.requestFocus();
				
			}
		}

		else if (n.getId().equals("txtClient")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtOrigin.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtBookingDate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtClient.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtOrigin")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDestination.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtDestination")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtNetwork.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtService.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtForwarderService.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtForwarderService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtForwarderNo.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtForwarderNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtForwarderAccount.requestFocus();
			}
		}
		
		
		else if (n.getId().equals("txtForwarderAccount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_Phone.requestFocus();
			}
		}
		
		else if (n.getId().equals("txt_Consignor_Phone")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_Name.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignor_Name")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_Address.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignor_Address")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_ContactPerson.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignor_ContactPerson")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_Zipcode.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignor_Zipcode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_City.requestFocus();
			}
		}

		
		else if (n.getId().equals("txt_Consignor_City")) 
		{ 
			if(e.getCode().equals(KeyCode.ENTER)) { 
				 combBox_Consignor_State.requestFocus(); 
			}
		}
		 

		else if (n.getId().equals("combBox_Consignor_State")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				combBox_Consignor_Country.requestFocus();
			}
		}

		else if (n.getId().equals("combBox_Consignor_Country")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignor_Email.requestFocus();
			}
		}
		
		else if (n.getId().equals("txt_Consignor_Email")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Phone.requestFocus();
			}
		}
		
		else if (n.getId().equals("txt_Consignee_Phone")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getClientVASDetails();
				if(Double.valueOf(txtInsuranceAmount.getText())==0)
				{
					loadInsuranceRateAndAmount();
				}
				txt_Consignee_Name.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignee_Name")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Address.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignee_Address")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Email.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignee_Email")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Zipcode.requestFocus();
			}
		}

	/*	else if (n.getId().equals("txt_Consignee_Zipcode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_City.requestFocus();
			}
		}

		
		else if (n.getId().equals("txt_Consignee_City")) 
		{ 
			if(e.getCode().equals(KeyCode.ENTER)) { 
				 combBox_Consignee_State.requestFocus(); 
			}
		}
		 

		/*else if (n.getId().equals("combBox_Consignee_State")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				combBox_Consignee_Country.requestFocus();
			}
		}

		else if (n.getId().equals("combBox_Consignee_Country")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Email.requestFocus();
			}
		}

		else if (n.getId().equals("txt_Consignee_Email")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txt_Consignee_Remarks.requestFocus();
			}
		}*/

		else if (n.getId().equals("txt_Consignee_Zipcode")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				combBox_Type.requestFocus();
			}
		}

		else if (n.getId().equals("combBox_Type")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				txtPcs.requestFocus();
			}
		}

		else if (n.getId().equals("txtPcs")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtDocketCharges.requestFocus();
			}
		}

		else if (n.getId().equals("txtDocketCharges")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				/*for(DimensionTableBean diBean:tabledata_Dimension)
				{
					volWeightFromDimension=volWeightFromDimension+diBean.getWeight();
				}
				
				if(volWeightFromDimension>Double.valueOf(txtVolumeWeight.getText()))
				{
					volWeightFromDimension=volWeightFromDimension+Double.valueOf(txtVolumeWeight.getText());
					
				}
				*/
				
				txtActualWeight.requestFocus();
			}
		}
		
		
		
		else if (n.getId().equals("txtActualWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
		
				txtVolumeWeight.requestFocus();
			}
		}

		else if (n.getId().equals("txtVolumeWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
			//	System.out.println("State value from User enter method: "+volWeightFromDimension);
				
		
				
				
				
				/*	volWeight=volWeightFromDimension;
				
					volWeightFromDimension=0.0;
					System.out.println("Set focus method running... Final Weight: "+df.format(volWeight));


					if(volWeight==0)
					{
						txtVolumeWeight.setText("0");
					}
					else
					{
						txtVolumeWeight.setText(df.format(Math.abs(volWeight)));
					}*/
					
						if(Double.valueOf(txtActualWeight.getText())!=0 || Double.valueOf(txtVolumeWeight.getText())!=0)
						{
							if(Double.valueOf(txtActualWeight.getText())>Double.valueOf(txtVolumeWeight.getText()))
							{
								txtBillWeight.setText(txtActualWeight.getText());
								txtBillWeight.requestFocus();
							}
							else
							{
								txtBillWeight.setText(txtVolumeWeight.getText());
								txtBillWeight.requestFocus();
							}
						}
						else
						{
							txtBillWeight.requestFocus();
						}
				}
				
			
			//}
		}

		else if (n.getId().equals("txtBillWeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {

				if(txtAwbNo.getText().equals(""))
				{
					combBox_Insurance.requestFocus();
				}
				else
				{
					String[] originPincode=txtOrigin.getText().replaceAll("\\s+","").split("\\|");
					String[] destinationPincode=txtDestination.getText().replaceAll("\\s+","").split("\\|");
					
					getZoneViaCityCode(originPincode[0], destinationPincode[0]);
				}
				//calculateFinalAmountForWeight();
				
				
			}
		}

		else if (n.getId().equals("combBox_Insurance")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInvoiceNo.requestFocus();
			}
		}

		else if (n.getId().equals("txtInvoiceNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInvoiceAmount.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtInsuranceRate")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtInvoiceNo.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtInvoiceAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getInsuranceAmount();
				txtInsuranceAmount.requestFocus();
			}
		}

		else if (n.getId().equals("txtInsuranceAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAddOnExpense.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtAddOnExpense")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtExpenseAmount.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtExpenseAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtBasicAmount.requestFocus();
				//getFuelAmount();
			//	txtExpenseAmount.requestFocus();
				
			}
		}
		
		else if (n.getId().equals("txtBasicAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getFuelAmount();
				txtFuelAmount.requestFocus();
				
			}
		}
		
		else if (n.getId().equals("txtFuelAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getTaxableValue();
				showGST_and_TotalAmt();
				txtTotalVASAmount.requestFocus();
			}
		}
		
		
		else if (n.getId().equals("txtTotalVASAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtTaxableValue.requestFocus();
			}
			
		}
		
		else if (n.getId().equals("txtTaxableValue")) {
			if (e.getCode().equals(KeyCode.ENTER)) 
			{
				btnSave.requestFocus();
			}
			else if(e.getCode().equals(KeyCode.F5))
			{
				showSelectedVAS();
			}
		}
		
		/*else if (n.getId().equals("txtInsuranceAmount")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnSave.requestFocus();
				
			}
		}*/
		
		else if (n.getId().equals("btnSave")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				saveDataEntry();
				
			}
		}

		
	}	
	
	
	
	
	
	

// *************** Method to set Validation *******************

	public void setValidation() throws SQLException {

		
		
		/*Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);

		if (txtAwbNo.getText() == null || txtAwbNo.getText().isEmpty()) 
		{
			alert.setTitle("Empty Awb No. Validation");
			alert.setContentText("You did not enter a AWB No.");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}
		else if (txtName.getText() == null || txtName.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Name");
			alert.showAndWait();
			txtName.requestFocus();
		}
		else 
		{
			
			if(btnSubmit.isDisable()==true)
			{
				updateForwarderDetails();	
			}
			else
			{
				saveForwarderDetails();
			}
		}*/
	}		
	
// ****************************************************************	

	public void getDetailsViaAwbNo() throws SQLException
	{
		
		deBean_global=new DataEntryBean();
		boolean isAwbNoNotExist=false;
		
		String setAwbNo=OutwardPageController.awbNoForDataEntryPopup;
		System.out.println("Set AWB NO:  >>>>>>>>>>>>>>>>>>>>> "+setAwbNo);
		
		
		
		//txtAwbNo.setText(setAwbNo);
		
		
			loadDimensionTableViaAWBno();
			
			LIST_SAVEDIMENSION_DATA.clear();
			Dimension__finalweight=0;
			Dimension__totalweight=0;
			Dimension_totalPcs=0;
			awbno=null;
			TotalPCS=0;
			volWeightFromDimension=0;
			
			
			
			//checkBoxDimension.setDisable(false);
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		DataEntryBean deBean=new DataEntryBean();
		PreparedStatement preparedStmt = null;

		//awbno=txtAwbNo.getText();
		deBean.setAwbNo(setAwbNo);
	//	deBean.setTravelStatus("outward");

		try {
			String sql = "select dbt.travel_status as travelstatus,dbt.dailybookingtransaction2client as client,dbt.dailybookingtransactionid as id,"
					+ "dbt.booking_date as bookingdate,dbt.origin as origin,dbt.dailybookingtransaction2city as destination,dbt.forwarder_number as for_number,"
					+ "dbt.dailybookingtransaction2forwarder_service as for_service,dbt.zipcode as zipcode,dbt.dailybookingtransaction2service as service,"
					+ "dbt.dailybookingtransaction2network as network,dbt.billing_weight as billweight,dbt.packets as pcs,dbt.volumne_weight as volweight,"
					+ " dbt.actual_weight as actweight,dbt.consignor_phone as cosignorPhone,dbt.consignee_phone as cosigneePhone,dbt.dox_nondox as dnd,dbt.forwarder_account,"
					+ "dbt.amount as basic_amount, dbt.fuel as fuel_amount,cm.code as clientcode, cm.name as clientname,cm.contactperson as contactperson,"
					+ "cm.emailid as email,cm.pincode as cm_pincode,cm.city as cm_city,dbt.insurance_invoice_no as insrnc_inv_no,dbt.insurance_rate as insrnc_rate,"
					+ "dbt.docket_charge as dkcharg,dbt.insurance_type as insrnc_type,dbt.insurance_value as insrnc_value,dbt.insurance_amount as insrnc_amt,"
					+ "cm.clientmaster2address as address,vm.name as networkname,dbt.tax_amount1 as cgst, dbt.tax_amount2 as sgst,dbt.tax_amount3 as igst,dbt.total as total from dailybookingtransaction as dbt,clientmaster as cm, vendormaster as vm "
					+ "where dbt.dailybookingtransaction2client=cm.code and dbt.dailybookingtransaction2network=vm.code and dbt.air_way_bill_number=? and mps_type='T'";
			
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, deBean.getAwbNo());
			//preparedStmt.setString(2, deBean.getTravelStatus());
			System.out.println("Details Sql: "+preparedStmt);
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				isAwbNoNotExist=true;
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Incorrect Awb Alert");
				alert.setHeaderText(null);
				alert.setContentText("Please enter the correct Awb No!!");
				alert.showAndWait();
				txtAwbNo.requestFocus();
			} 
			else 
			{
				isAwbNoNotExist=false;
				
				do 
				{
				
					isDataAvailable=true;
					
					//System.out.println("Zipcode: "+rs.getString("zipcode")+" | Bill wt: "+rs.getDouble("billweight")+" | Vol wt: "+rs.getDouble("volweight")+" | Actual wt: "+rs.getDouble("actweight"));
					
					deBean.setBookingDate(date.format(rs.getDate("bookingdate")));
					deBean.setClient(rs.getString("clientname")+" | "+rs.getString("client"));
					deBean.setOrigin(rs.getString("origin"));
					deBean.setDestination(rs.getString("destination"));
					deBean.setService(rs.getString("service"));
					deBean.setNetwork(rs.getString("networkname")+" | "+rs.getString("network"));
					deBean.setBillWeight(rs.getDouble("billweight"));
					deBean.setForwarderNo(rs.getString("for_number"));
					deBean.setForwarderService(rs.getString("for_service"));
					deBean.setConsigNEE_Zipcode(rs.getString("zipcode"));
					deBean.setPcs(rs.getInt("pcs"));
					deBean.setDocketCharges(rs.getDouble("dkcharg"));
					deBean.setVolumeWeight(rs.getDouble("volweight"));
					deBean.setActualWeight(rs.getDouble("actweight"));
					deBean.setType(rs.getString("dnd"));
					CLIENT_DB_CITY_NAME=rs.getString("cm_city");
					//CLIENT_DB_STATE_CODE=rs.getString("cm_statcode");
					deBean.setPincode(rs.getString("cm_pincode"));
					deBean.setCity(rs.getString("cm_city"));					
					deBean.setBasicAmount(rs.getDouble("basic_amount"));
					deBean.setFuelAmount(rs.getDouble("fuel_amount"));
					deBean.setConsigNOR_Name(rs.getString("clientname"));
					deBean.setConsigNOR_Address(rs.getString("address"));
					deBean.setConsigNOR_ContactPerson(rs.getString("contactperson"));
					deBean.setConsigNOR_Email(rs.getString("email"));
					deBean.setConsigNEE_Phone(rs.getString("cosigneePhone"));
					deBean.setConsigNOR_Phone(rs.getString("cosignorPhone"));
					deBean.setTravelStatus(rs.getString("travelstatus"));
					deBean.setInsurance(rs.getString("insrnc_type"));
					deBean.setInsuranceAmount(rs.getDouble("insrnc_amt"));
					deBean.setInsuranceRate(rs.getDouble("insrnc_rate"));
					deBean.setInvoiceAmount(rs.getDouble("insrnc_value"));
					deBean.setInvoiceNo(rs.getString("insrnc_inv_no"));
					deBean.setForwarderAccount(rs.getString("forwarder_account"));
					deBean.setTax_amount1(rs.getDouble("CGST"));
					deBean.setTax_amount2(rs.getDouble("SGST"));
					deBean.setTax_amount3(rs.getDouble("IGST"));
					deBean.setGst(deBean.getTax_amount1()+deBean.getTax_amount2()+deBean.getTax_amount3());
					deBean.setTotal(rs.getDouble("total"));
					
					tabledata_VAS.clear();
					getVAS_DataFromDB_viaAWBNo();
					

				} while (rs.next());
				
				
				TotalPCS=deBean.getPcs();
				//txtBookingDate.setText(deBean.getBookingDate());
				
				txtClient.setText(deBean.getClient());
				if(deBean.getOrigin()!=null)
				{
					for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
					{
						if(deBean.getOrigin().equals(String.valueOf(bean.getZipcode())))
						{
							txtOrigin.setText(bean.getZipcode()+" | "+bean.getCityName());
							break;
						}
					}
				}
				txtDestination.setText(deBean.getConsigNEE_Zipcode()+" | "+deBean.getDestination());
				txtService.setText(deBean.getService());
				txtNetwork.setText(deBean.getNetwork());
				txtForwarderNo.setText(deBean.getForwarderNo());
				txtForwarderService.setText(deBean.getForwarderService());
				txtBillWeight.setText(String.valueOf(deBean.getBillWeight()));
				txtPcs.setText(String.valueOf(deBean.getPcs()));
				txtVolumeWeight.setText(String.valueOf(deBean.getVolumeWeight()));
				txtActualWeight.setText(String.valueOf(deBean.getActualWeight()));
				txt_Consignee_Zipcode.setText(deBean.getConsigNEE_Zipcode());
				txtBasicAmount.setText(String.valueOf(deBean.getBasicAmount()));
				System.out.println("Fuel AMt   >>>> "+deBean.getFuelAmount());
				txtFuelAmount.setText(String.valueOf(deBean.getFuelAmount()));
				txtInsuranceAmount.setText(String.valueOf(deBean.getInsuranceAmount()));
				txtInsuranceRate.setText(String.valueOf(deBean.getInsuranceRate()));
				txtInvoiceAmount.setText(String.valueOf(deBean.getInvoiceAmount()));
				txtDocketCharges.setText(String.valueOf(deBean.getDocketCharges()));
				txtGSTAmt.setText(df.format(deBean.getGst()));
				txtTotal.setText(df.format(deBean.getTotal()));
				
				boolean isForwarderAccountAvailable=false;
				
				for(LoadForwarderDetailsBean lfBean: LoadForwarderDetails.LIST_FORWARDER_DETAILS)
				{
					if(lfBean.getForwarderCode().equals(deBean.getForwarderAccount()))
					{
						isForwarderAccountAvailable=true;
						txtForwarderAccount.setText(lfBean.getForwarderCode()+" | "+lfBean.getForwarderName());
						break;
					}
					else
					{
						isForwarderAccountAvailable=false;
					}
				}
					
				
				if(isForwarderAccountAvailable==false)
				{
					txtForwarderAccount.setText(deBean.getForwarderAccount());
				}
				
				
				if(deBean.getInvoiceNo()==null)
				{
					txtInvoiceNo.clear();
				}
				else
				{
					txtInvoiceNo.setText(String.valueOf(deBean.getInvoiceNo()));	
				}
				
				if(deBean.getInsurance()==null)
				{
					combBox_Insurance.setValue("Carrier");
				}
				else
				{
					combBox_Insurance.setValue(String.valueOf(deBean.getInsurance()));
				}
				
				
				
				//System.out.println("Tavel Status >>>>>>>>>>> "+deBean.getTravelStatus());
			
				if(!deBean.getTravelStatus().equals("dataentry"))
				{
					txt_Consignee_Name.clear();
					txt_Consignee_Address.clear();
					txt_Consignee_Email.clear();
					txt_Consignee_Phone.clear();
					txt_Consignee_Email.clear();
				}
				else
				{
					
					txt_Consignee_Phone.setText(deBean.getConsigNEE_Phone());
				}
				
				
				if(!deBean.getTravelStatus().equals("dataentry"))
				{
					
					txt_Consignor_Name.clear();
					txt_Consignor_Address.clear();
					txt_Consignor_ContactPerson.clear();
					txt_Consignor_Email.clear();
					txt_Consignor_Phone.clear();
					txt_Consignor_City.clear();
					txt_Consignor_Zipcode.clear();
					combBox_Consignor_Country.getSelectionModel().clearSelection();
					combBox_Consignor_State.getSelectionModel().clearSelection();
					
					txt_Consignor_Name.setText(deBean.getConsigNOR_Name());
					txt_Consignor_Address.setText(deBean.getConsigNOR_Address());
					txt_Consignor_ContactPerson.setText(deBean.getConsigNOR_ContactPerson());
					txt_Consignor_Email.setText(deBean.getConsigNOR_Email());
					txt_Consignor_Zipcode.setText(deBean.getPincode());
					txt_Consignor_City.setText(deBean.getCity());
				}
				else
				{
					txt_Consignor_Phone.setText(deBean.getConsigNOR_Phone());
				}
				
				
				/*if(deBean.getConsigNOR_Phone()==null)
				{
					
					txt_Consignor_Name.clear();
					txt_Consignor_Address.clear();
					txt_Consignor_ContactPerson.clear();
					txt_Consignor_Email.clear();
					txt_Consignor_Phone.clear();
					txt_Consignor_City.clear();
					txt_Consignor_Zipcode.clear();
					combBox_Consignor_Country.getSelectionModel().clearSelection();
					combBox_Consignor_State.getSelectionModel().clearSelection();
					
					
					txt_Consignor_Name.setText(deBean.getConsigNOR_Name());
					txt_Consignor_Address.setText(deBean.getConsigNOR_Address());
					txt_Consignor_ContactPerson.setText(deBean.getConsigNOR_ContactPerson());
					txt_Consignor_Email.setText(deBean.getConsigNOR_Email());
				}
				else
				{
					txt_Consignor_Phone.setText(deBean.getConsigNOR_Phone());
				}*/
				
				
				txtInsuranceRate.setText(String.valueOf(deBean.getInsuranceRate()));
				txtInsuranceAmount.setText(String.valueOf(deBean.getInsuranceAmount()));
				txtInvoiceAmount.setText(String.valueOf(deBean.getInvoiceAmount()));
			}
			
			//getODA_OPA_ViaPincodes(deBean.getPincode(), deBean.getConsigNEE_Zipcode());
			
			if(isDataAvailable==true)
			{ 
				getODA_OPA_ViaPincodes(deBean.getPincode(), deBean.getConsigNEE_Zipcode());
				//getClientVASDetails();
				isDataAvailable=false;
						
				if(deBean.getTravelStatus().equals("dataentry"))
				{
					showConsigneeConsignorDetails();
					//showConsignorDetails();
					//showConsigneeDetails();
				}
			}
			
			//txtTotalVASAmount.clear();
			
			
		//	getExtraVAS_Amount();
			
			if(isAwbNoNotExist==false)
			{
			if(tabledata_VAS.size()>0)
			{
				for(VAS_Table_Bean vsBean: tabledata_VAS)
				{
					totalVAS_amount=totalVAS_amount+vsBean.getVasAmount();
				}
			}
			
			
			txtTotalVASAmount.setText(String.valueOf(totalVAS_amount+Double.valueOf(txtInsuranceAmount.getText())+Double.valueOf(txtDocketCharges.getText())));
			
			System.out.println("Get 1 finalweihgt amt >> "+txtBasicAmount.getText());
			System.out.println("Get 2 fuel amt>> "+txtFuelAmount.getText());
			System.out.println("Get 3 >> total VAS "+txtTotalVASAmount.getText());
			System.out.println("Get 4 >> VAS: >> "+totalVAS_amount);
			System.out.println("Get 5 >> Ins amt >>"+txtInsuranceAmount.getText());
			
			System.out.println(">> "+txtBasicAmount.getText());
			System.out.println(">> "+txtFuelAmount.getText());
			System.out.println(">> "+txtTotalVASAmount.getText());
			//System.out.println(">> "+String.valueOf(Double.valueOf(txtBasicAmount.getText())+Double.valueOf(txtFuelAmount.getText())+Double.valueOf(txtTotalVASAmount.getText())));
			
			/*if(txtFuelAmount.getText().equals(""))
			{
				
			}
			else
			{
				
			}*/
			
			txtTaxableValue.setText(String.valueOf(Double.valueOf(txtBasicAmount.getText())+Double.valueOf(txtFuelAmount.getText())+Double.valueOf(txtTotalVASAmount.getText())));
			
			totalVAS_amount=0;
			}
						
		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}
	

	public void setCityFromPcode() throws SQLException 
	{

		if (!txt_Consignor_Zipcode.getText().equals("")) 
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(null);

			boolean checkZipcodeStatus = false;
			LoadCityWithZipcodeBean destinationBean = new LoadCityWithZipcodeBean();

			new LoadCity().loadZipcodeCityDetails();
			
			for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
			{
				//System.out.println("Pincode: "+bean.getZipcode());
				
				if (Long.valueOf(txt_Consignor_Zipcode.getText()) == bean.getZipcode())
				{
					checkZipcodeStatus = true;
					destinationBean.setCityName(bean.getCityName());
					break;
				} 
				else
				{
					checkZipcodeStatus = false;
				}
			}

			if (checkZipcodeStatus == true) 
			{
				txt_Consignor_City.setText(destinationBean.getCityName());
				txt_Consignor_City.requestFocus();
			}
			else 
			{
				txt_Consignor_City.clear();
				alert.setTitle("Zipcode City Alert");
				alert.setContentText("Please enter correct zipcode");
				alert.showAndWait();
				txt_Consignor_Zipcode.requestFocus();
			}
		}
		else 
		{
			txt_Consignor_City.requestFocus();
		}
	}
	
// *************************************************************************
	
/*	public void getODA_OPA_ViaPincodes(String originPincode, String destinationPincode) throws SQLException
	{
		List_ODA_OPA_FromPincode.clear();
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		
		//System.out.println("Origin: "+originPincode);
		//System.out.println("Destination: "+destinationPincode);
		
		PreparedStatement preparedStmt = null;
		
		try 
		{
			String sql = "select pincode,city_name,oda_opa from pincode_master where pincode=? OR pincode=?";
				
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, originPincode);
			preparedStmt.setString(2, destinationPincode);
			System.out.println("Origin Destination: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				
			} 
			else 
			{
				do 
				{
					Oda_Opa_Bean od_op_Bean=new Oda_Opa_Bean();
					
					if(originPincode.equals(rs.getString("pincode")))
					{
						od_op_Bean.setType_org_desti("origin");
					}
					else
					{
						od_op_Bean.setType_org_desti("destination");
					}
					
					od_op_Bean.setPincode_Origin_Destination(rs.getString("pincode"));
					od_op_Bean.setType_ODA_OPA(rs.getString("oda_opa"));
					
					List_ODA_OPA_FromPincode.add(od_op_Bean);
				} 
				while (rs.next());
			}
			
			for(Oda_Opa_Bean bean:List_ODA_OPA_FromPincode)
			{
				System.out.println("pincode: >> "+bean.getPincode_Origin_Destination()+" | OPA/ODA value: >> "+bean.getType_ODA_OPA()+" | Type: >> "+bean.getType_org_desti());
				
			}
			
			
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}*/
	
	
	public void getODA_OPA_ViaPincodes(String originPincode, String destinationPincode) throws SQLException
	{
		List_ODA_OPA_FromPincode.clear();
		
		new LoadPincodeForAll().loadPincode();
		

		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(destinationPincode.equals(pinBean.getPincode()))
			{
				Oda_Opa_Bean od_op_Bean=new Oda_Opa_Bean();
				od_op_Bean.setType_org_desti("destination");
				od_op_Bean.setPincode_Origin_Destination(pinBean.getPincode());
				od_op_Bean.setType_ODA_OPA(pinBean.getOda_Opa_Regular());
				List_ODA_OPA_FromPincode.add(od_op_Bean);
				break;
			}
		}
		
		for(LoadPincodeBean pinBean:LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(originPincode.equals(pinBean.getPincode()))
			{
				Oda_Opa_Bean od_op_Bean=new Oda_Opa_Bean();
				od_op_Bean.setType_org_desti("origin");
				od_op_Bean.setPincode_Origin_Destination(pinBean.getPincode());
				od_op_Bean.setType_ODA_OPA(pinBean.getOda_Opa_Regular());
				List_ODA_OPA_FromPincode.add(od_op_Bean);
				break;
			}
		}
		
		
		for(Oda_Opa_Bean bean:List_ODA_OPA_FromPincode)
		{
			System.out.println("pincode: >> "+bean.getPincode_Origin_Destination()+" | OPA/ODA value: >> "+bean.getType_ODA_OPA()+" | Type: >> "+bean.getType_org_desti());
			
		}
		
	}

// *************************************************************************	
	
	public void getGST_rate() throws SQLException
	{
		if(LIST_GST_TAXES.size()==0)
		{
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
				 	
			try
			{		
				st=con.createStatement();
				String sql="select taxname1,taxrate1,taxname2,taxrate2,taxname3,taxrate3 from expensestaxtype";
				rs=st.executeQuery(sql);
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						TaxCalculatorBean txBean=new TaxCalculatorBean();
						txBean.setTaxname1(rs.getString("taxname1"));
						txBean.setTaxname2(rs.getString("taxname2"));
						txBean.setTaxname3(rs.getString("taxname3"));
						txBean.setTaxrate1(rs.getDouble("taxrate1"));
						txBean.setTaxrate2(rs.getDouble("taxrate2"));
						txBean.setTaxrate3(rs.getDouble("taxrate3"));
						
						LIST_GST_TAXES.add(txBean);
					}while(rs.next());
				}
			}
			
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
	}
	
// *************************************************************************

	public void updateConsignorConsigneeDetailsInDailyBookingTransaction() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		
		String[] originPincode=txtOrigin.getText().replaceAll("\\s+","").split("\\|");
		
		DataEntryBean deBean = new DataEntryBean();

		deBean.setConsigNEE_Phone(txt_Consignee_Phone.getText());
		deBean.setConsigNOR_Phone(txt_Consignor_Phone.getText());
		if(combBox_Type.getValue().equals("Dox"))
		{
			deBean.setType("D");	
		}
		else
		{
			deBean.setType("N");
		}
		deBean.setPcs(Integer.valueOf(txtPcs.getText()));
		deBean.setActualWeight(Double.valueOf(txtActualWeight.getText()));
		deBean.setVolumeWeight(Double.valueOf(txtVolumeWeight.getText()));
		deBean.setBillWeight(Double.valueOf(txtBillWeight.getText()));
		deBean.setInsurance(combBox_Insurance.getValue());
		//deBean.setOrigin(txtOrigin.getText());
		deBean.setOrigin(originPincode[0]);
		deBean.setInsuranceRate(Double.valueOf(txtInsuranceRate.getText()));
		deBean.setInvoiceNo(txtInvoiceNo.getText());
		deBean.setDocketCharges(Double.valueOf(txtDocketCharges.getText()));
		deBean.setInvoiceAmount(Double.valueOf(txtInvoiceAmount.getText()));
		deBean.setInsuranceAmount(Double.valueOf(txtInsuranceAmount.getText()));
		deBean.setAwbNo(txtAwbNo.getText());
		deBean.setTravelStatus("dataentry");
		deBean.setFuelRate(fuelRate);
		deBean.setFuelAmount(Double.valueOf(txtFuelAmount.getText()));
		deBean.setBasicAmount(Double.valueOf(txtBasicAmount.getText()));
		deBean.setVasTotal(Double.valueOf(txtTotalVASAmount.getText())-(deBean.getInsuranceAmount()+deBean.getDocketCharges()));
		
		if(txtForwarderAccount.getText().contains("|"))
		{
			String[] forwarderAccount=txtForwarderAccount.getText().replaceAll("\\s+","").split("\\|");
			deBean.setForwarderAccount(forwarderAccount[0]);
		}
		else
		{
			deBean.setForwarderAccount(txtForwarderAccount.getText());
		}
		//deBean.setVasTotal(Double.valueOf(txtTotalVASAmount.getText()));
		//deBean.setTotal(Double.valueOf(txtTaxableValue.getText()));
		
		//getGST_rate();
		
		new LoadCity().loadZipcodeCityDetails();
		
		for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
		{
			
			if (CLIENT_DB_CITY_NAME.equals(bean.getCityName()))
			{
				//System.out.println("");
				CLIENT_DB_STATE_CODE=bean.getState_code();
				break;
			}
			else
			{
				CLIENT_DB_STATE_CODE=null;
			}
		}
		
			
			/*for(TaxCalculatorBean txBean:LIST_GST_TAXES)
			{
				deBean.setTax_name1(txBean.getTaxname1());
				deBean.setTax_name2(txBean.getTaxname2());
				deBean.setTax_name3(txBean.getTaxname3());
				
				TaxCalculator tc=new TaxCalculator();
				if(CLIENT_STATIC_STATE_CODE.equals(CLIENT_DB_STATE_CODE))
				{
					//System.out.println("Form if >>> Static City: "+CLIENT_STATIC_CITY_NAME+" | DB CITY: "+CLIENT_DB_CITY_NAME);
					tc.calculateGST(Double.valueOf(txtTaxableValue.getText()), txBean.getTaxrate1(), txBean.getTaxrate2(),0);

					deBean.setTax_rate1(txBean.getTaxrate1());
					deBean.setTax_rate2(txBean.getTaxrate2());
					deBean.setTax_rate3(0);
					deBean.setTax_amount1(TaxCalculator.taxAmount1);
					deBean.setTax_amount2(TaxCalculator.taxAmount2);
					deBean.setTax_amount3(0);
					break;
				}
				else
				{
					//System.out.println("Form else >>> Static City: "+CLIENT_STATIC_CITY_NAME+" | DB CITY: "+CLIENT_DB_CITY_NAME);
					tc.calculateGST(Double.valueOf(txtTaxableValue.getText()), 0, 0,txBean.getTaxrate3());
					deBean.setTax_rate1(0);
					deBean.setTax_rate2(0);
					deBean.setTax_rate3(txBean.getTaxrate3());
					deBean.setTax_amount1(0);
					deBean.setTax_amount2(0);
					deBean.setTax_amount3(TaxCalculator.taxAmount3);
					break;
				}
			}*/
		
			//System.out.println("Taxable value before GST: "+txtTaxableValue.getText());
			
			deBean.setTotal(Double.valueOf(txtTaxableValue.getText())+deBean_global.getTax_amount1()+deBean_global.getTax_amount2()+deBean_global.getTax_amount3());
		
			//System.out.println("Taxable value after GST: "+deBean.getTotal());
			
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = "update dailybookingtransaction SET consignor_phone=?,consignee_phone=?,dox_nondox=?,packets=?,actual_weight=?,"
					+ "volumne_weight=?,billing_weight=?,insurance_type=?,insurance_rate=?,insurance_invoice_no=?,"
					+ "insurance_value=?,insurance_amount=?,travel_status=?,docket_charge=?,origin=?,amount=?,fuel_rate=?,fuel=?,total=?,"
					+ "tax_rate1=?,tax_rate2=?,tax_rate3=?,tax_name1=?,tax_name2=?,tax_name3=?,tax_amount1=?,tax_amount2=?,tax_amount3=?,"
					+ "forwarder_account=?,vas_total=? where air_way_bill_number=?";
			
			preparedStmt = con.prepareStatement(query);

			
			preparedStmt.setString(1, deBean.getConsigNOR_Phone());
			preparedStmt.setString(2, deBean.getConsigNEE_Phone());
			preparedStmt.setString(3, deBean.getType());
			preparedStmt.setInt(4, deBean.getPcs());
			preparedStmt.setDouble(5, deBean.getActualWeight());
			preparedStmt.setDouble(6, deBean.getVolumeWeight());
			preparedStmt.setDouble(7, deBean.getBillWeight());
			preparedStmt.setString(8, deBean.getInsurance());
			preparedStmt.setDouble(9, deBean.getInsuranceRate());
			preparedStmt.setString(10, deBean.getInvoiceNo());
			preparedStmt.setDouble(11, deBean.getInvoiceAmount());
			preparedStmt.setDouble(12, deBean.getInsuranceAmount());
			preparedStmt.setString(13, deBean.getTravelStatus());
			preparedStmt.setDouble(14, deBean.getDocketCharges());
			preparedStmt.setString(15, deBean.getOrigin());
			preparedStmt.setDouble(16, deBean.getBasicAmount());
			preparedStmt.setDouble(17, deBean.getFuelRate());
			preparedStmt.setDouble(18, deBean.getFuelAmount());
			preparedStmt.setDouble(19, deBean.getTotal());
			preparedStmt.setDouble(20, deBean_global.getTax_rate1());
			preparedStmt.setDouble(21, deBean_global.getTax_rate2());
			preparedStmt.setDouble(22, deBean_global.getTax_rate3());
			preparedStmt.setString(23, deBean_global.getTax_name1());
			preparedStmt.setString(24, deBean_global.getTax_name2());
			preparedStmt.setString(25, deBean_global.getTax_name3());
			preparedStmt.setDouble(26, deBean_global.getTax_amount1());
			preparedStmt.setDouble(27, deBean_global.getTax_amount2());
			preparedStmt.setDouble(28, deBean_global.getTax_amount3());
			preparedStmt.setString(29, deBean.getForwarderAccount());
			preparedStmt.setDouble(30, deBean.getVasTotal());
			preparedStmt.setString(31, deBean.getAwbNo());
		

			int status = preparedStmt.executeUpdate();

			if (status == 1 && consignorConsigneeSaveStatus==1) 
			{
			
				
				getExtraVAS_Amount();
				/*if(tabledata_VAS.size()>0)
				{*/
					checkAwbNo_InDailyBookingVAS();
				//}
				//
				alert.setContentText("Entry successfully saved...");
				alert.showAndWait();
				reset();
			} 
			else if(consignorConsigneeSaveStatus==0)
			{
				alert.setContentText("Consignor Consignee details not saved... \nPlease Check!! ");
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Something went wrong...! \n please try again");
				alert.showAndWait();
			}

			//loadTable();
			saveDimensionDetails();

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}	
		
	}	
	
	
	public void saveDimensionDetails() throws SQLException 
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		 
		
		
		 
		/*totalweight = totalweight + ((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText()) / 2000));*/
	
		/*DimensionBean diBean = new DimensionBean();

		diBean.setAwbno(DataEntryController.awbno);
		diBean.setPcs(Integer.valueOf(txtDimensionPCS.getText()));
		diBean.setLength(Double.valueOf(txtLength.getText()));
		diBean.setWidth(Double.valueOf(txtWidth.getText()));
		diBean.setHeight(Double.valueOf(txtHeight.getText()));
		diBean.setWeight((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText())) / 2000);
		System.out.println("total+ " + (diBean.getHeight() * diBean.getWidth() * diBean.getHeight()) + ", weight: "
				+ diBean.getWeight());*/

		

		try 
		{
			for(DimensionBean diBean:LIST_SAVEDIMENSION_DATA)
		
		{
			
			String query = "insert into dimensions(awbno,pcs,weight,length,height,width,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";

			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, diBean.getAwbno());
			preparedStmt.setInt(2, diBean.getPcs());
			preparedStmt.setDouble(3, diBean.getWeight());
			preparedStmt.setDouble(4, diBean.getWidth());
			preparedStmt.setDouble(5, diBean.getLength());
			preparedStmt.setDouble(6, diBean.getHeight());

			int consignorSaveStatus = preparedStmt.executeUpdate();
		}
			
			//loadDimensionTableFromDB();

		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			LIST_SAVEDIMENSION_DATA.clear();
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
	

// ****************************************************************	
	
	public void saveConsigneeConsignorDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Consignee Consignor Status alert");
		alert.setHeaderText(null);

		DataEntryBean deBean=new DataEntryBean();

		deBean.setAwbNo(txtAwbNo.getText());
		deBean.setConsigNOR_Phone(txt_Consignor_Phone.getText());
		deBean.setConsigNOR_Name(txt_Consignor_Name.getText());
		deBean.setConsigNOR_Address(txt_Consignor_Address.getText());
		deBean.setConsigNOR_ContactPerson(txt_Consignor_ContactPerson.getText());
		deBean.setConsigNOR_Zipcode(txt_Consignor_Zipcode.getText());
		deBean.setConsigNOR_City(txt_Consignor_City.getText());
		deBean.setConsigNOR_State(combBox_Consignor_State.getValue());
		deBean.setConsigNOR_Country(combBox_Consignor_Country.getValue());
		deBean.setConsigNOR_Email(txt_Consignor_Email.getText());
		
		deBean.setConsigNEE_Name(txt_Consignee_Name.getText());
		deBean.setConsigNEE_Address(txt_Consignee_Address.getText());
		deBean.setConsigNEE_Email(txt_Consignee_Email.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		showConsigneeConsignorDetails();
		
		try 
		{
			if(checkConsignorConsigneeExistanceStatus==false)
			{
				String query = "insert into consignor_consignee_details(name,address,contact_person,zipcode,city,state,country,email,cnee_name,cnee_address,cnee_email,awb_no,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
			
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setString(1, deBean.getConsigNOR_Name());
				preparedStmt.setString(2, deBean.getConsigNOR_Address());
				preparedStmt.setString(3, deBean.getConsigNOR_ContactPerson());
				preparedStmt.setString(4, deBean.getConsigNOR_Zipcode());
				preparedStmt.setString(5, deBean.getConsigNOR_City());
				preparedStmt.setString(6, deBean.getConsigNOR_State());
				preparedStmt.setString(7, deBean.getConsigNOR_Country());
				preparedStmt.setString(8, deBean.getConsigNOR_Email());
				preparedStmt.setString(9, deBean.getConsigNEE_Name());
				preparedStmt.setString(10, deBean.getConsigNEE_Address());
				preparedStmt.setString(11, deBean.getConsigNEE_Email());
				preparedStmt.setString(12, deBean.getAwbNo());
	
				consignorConsigneeSaveStatus=preparedStmt.executeUpdate();
				System.out.println("ExecuteUpdate Status from if :"+consignorConsigneeSaveStatus);
			}
			else
			{
				String query = "update consignor_consignee_details SET name=?,address=?,contact_person=?,zipcode=?,city=?,state=?,"
							+ "country=?,email=?,cnee_name=?,cnee_address=?,cnee_email=?,lastmodifieddate=CURRENT_TIMESTAMP "
							+ "where awb_no=?";
				
				preparedStmt = con.prepareStatement(query);

				preparedStmt.setString(1, deBean.getConsigNOR_Name());
				preparedStmt.setString(2, deBean.getConsigNOR_Address());
				preparedStmt.setString(3, deBean.getConsigNOR_ContactPerson());
				preparedStmt.setString(4, deBean.getConsigNOR_Zipcode());
				preparedStmt.setString(5, deBean.getConsigNOR_City());
				preparedStmt.setString(6, deBean.getConsigNOR_State());
				preparedStmt.setString(7, deBean.getConsigNOR_Country());
				preparedStmt.setString(8, deBean.getConsigNOR_Email());
				preparedStmt.setString(9, deBean.getConsigNEE_Name());
				preparedStmt.setString(10, deBean.getConsigNEE_Address());
				preparedStmt.setString(11, deBean.getConsigNEE_Email());
				preparedStmt.setString(12, deBean.getAwbNo());

				consignorConsigneeSaveStatus=preparedStmt.executeUpdate();
				System.out.println("ExecuteUpdate Status from else : "+consignorConsigneeSaveStatus);

				checkConsignorConsigneeExistanceStatus=false;
			}
		} 
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

// ****************************************************************	

	public void showConsigneeConsignorDetails() throws SQLException
	{
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		
		DataEntryBean deBean=new DataEntryBean();
		PreparedStatement preparedStmt = null;
		
		deBean.setAwbNo(txtAwbNo.getText());
		
		try 
		{
			String sql = "select name,address,contact_person,zipcode,city,state,country,email,remarks,cnee_name,cnee_address,cnee_email from consignor_consignee_details where awb_no=?";
				
			preparedStmt = con.prepareStatement(sql);
			
			preparedStmt.setString(1, deBean.getAwbNo());
			
			//System.out.println("nor Sql: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				checkConsignorConsigneeExistanceStatus=false;
				txt_Consignor_Name.requestFocus();
			} 
			else 
			{
				do 
				{
					checkConsignorConsigneeExistanceStatus=true;
					
					deBean.setConsigNOR_Name(rs.getString("name"));
					deBean.setConsigNOR_Address(rs.getString("address"));
					deBean.setConsigNOR_ContactPerson(rs.getString("contact_person"));
					deBean.setConsigNOR_Zipcode(rs.getString("zipcode"));
					deBean.setConsigNOR_City(rs.getString("city"));
					deBean.setConsigNOR_State(rs.getString("state"));
					deBean.setConsigNOR_Country(rs.getString("country"));
					//deBean.setConsigNOR_Remarks(rs.getString("remarks"));
					deBean.setConsigNOR_Email(rs.getString("email"));
					deBean.setConsigNEE_Name(rs.getString("cnee_name"));
					deBean.setConsigNEE_Address(rs.getString("cnee_address"));
					deBean.setConsigNEE_Email(rs.getString("cnee_email"));
					
				} 
				while (rs.next());
				
				txt_Consignor_Name.setText(deBean.getConsigNOR_Name());
				txt_Consignor_Address.setText(deBean.getConsigNOR_Address());
				txt_Consignor_ContactPerson.setText(deBean.getConsigNOR_ContactPerson());
				txt_Consignor_Zipcode.setText(String.valueOf(deBean.getConsigNOR_Zipcode()));
				txt_Consignor_City.setText(deBean.getConsigNOR_City());
				combBox_Consignor_State.setValue(deBean.getConsigNOR_State());
				combBox_Consignor_Country.setValue(deBean.getConsigNOR_Country());
				//txt_Consignor_Remarks.setText(deBean.getConsigNEE_Remarks());
				txt_Consignor_Email.setText(deBean.getConsigNOR_Email());
				
				txt_Consignee_Name.setText(deBean.getConsigNEE_Name());
				txt_Consignee_Address.setText(deBean.getConsigNEE_Address());
				txt_Consignee_Email.setText(deBean.getConsigNEE_Email());
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ****************************************************************
	
	/*public void checkConsigNORDetails() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;

		try
		{	
			st=con.createStatement();
			String sql="select * from consignor_consignee_details where phone_no='"+txt_Consignor_Phone.getText()+"' and type='cnor'";
			
			rs=st.executeQuery(sql);
		
			if(!rs.next())
			{
				consignorExistanceStatus=false;
			}
			else
			{
				do{
					consignorExistanceStatus=true;
					break;
				}while(rs.next());
				
			}
			
		//saveConsigNORDetails();
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
	}*/
	
	
// ****************************************************************

	/*public void checkConsigNEEDetails() throws SQLException 
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		try 
		{
			st = con.createStatement();
			String sql = "select * from consignor_consignee_details where phone_no='" + txt_Consignee_Phone.getText()+ "' and type='cnee'";

			rs = st.executeQuery(sql);

			if (!rs.next()) 
			{
				consigneeExistanceStatus = false;
			} 
			else 
			{
				do 
				{
					consigneeExistanceStatus = true;
					break;
				} while (rs.next());

			}
			
			//saveConsigNEEDetails();

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{

			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
	
// ****************************************************************
	
	/*public void saveConsigNORDetails() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);

		DataEntryBean deBean=new DataEntryBean();

		deBean.setConsigNOR_Phone(txt_Consignor_Phone.getText());
		deBean.setConsignor_Consignee_Type("cnor");
		deBean.setConsigNOR_Name(txt_Consignor_Name.getText());
		deBean.setConsigNOR_Address(txt_Consignor_Address.getText());
		deBean.setConsigNOR_ContactPerson(txt_Consignor_ContactPerson.getText());
		//deBean.setConsigNOR_Zipcode(Integer.valueOf(txt_Consignor_Zipcode.getText()));
		deBean.setConsigNOR_Zipcode(txt_Consignor_Zipcode.getText());
		deBean.setConsigNOR_City(txt_Consignor_City.getText());
		deBean.setConsigNOR_State(combBox_Consignor_State.getValue());
		deBean.setConsigNOR_Country(combBox_Consignor_Country.getValue());
		deBean.setConsigNOR_Email(txt_Consignor_Email.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try {
			
			if(consignorExistanceStatus==false)
			{
				String query = "insert into consignor_consignee_details(phone_no,type,name,address,contact_person,zipcode,city,state,country,email,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
			
				preparedStmt = con.prepareStatement(query);

				preparedStmt.setString(1, String.valueOf(deBean.getConsigNOR_Phone()));
				preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
				preparedStmt.setString(3, deBean.getConsigNOR_Name());
				preparedStmt.setString(4, deBean.getConsigNOR_Address());
				preparedStmt.setString(5, deBean.getConsigNOR_ContactPerson());
				//preparedStmt.setInt(6, deBean.getConsigNOR_Zipcode());
				preparedStmt.setString(6, deBean.getConsigNOR_Zipcode());
				preparedStmt.setString(7, deBean.getConsigNOR_City());
				preparedStmt.setString(8, deBean.getConsigNOR_State());
				preparedStmt.setString(9, deBean.getConsigNOR_Country());
				preparedStmt.setString(10, deBean.getConsigNOR_Email());
	
			
				consignorSaveStatus = preparedStmt.executeUpdate();
			}
			else
			{
					String query = "update consignor_consignee_details SET name=?,address=?,contact_person=?,zipcode=?,city=?,state=?,"
							+ "country=?,email=?,indate=CURRENT_DATE,create_date=CURRENT_DATE,lastmodifieddate=CURRENT_TIMESTAMP "
							+ "where phone_no=? and type=?";
				
					preparedStmt = con.prepareStatement(query);
				
					preparedStmt.setString(1, deBean.getConsigNOR_Name());
					preparedStmt.setString(2, deBean.getConsigNOR_Address());
					preparedStmt.setString(3, deBean.getConsigNOR_ContactPerson());
					//preparedStmt.setInt(4, deBean.getConsigNOR_Zipcode());
					preparedStmt.setString(4, deBean.getConsigNOR_Zipcode());
					preparedStmt.setString(5, deBean.getConsigNOR_City());
					preparedStmt.setString(6, deBean.getConsigNOR_State());
					preparedStmt.setString(7, deBean.getConsigNOR_Country());
					preparedStmt.setString(8, deBean.getConsigNOR_Email());
					preparedStmt.setString(9, String.valueOf(deBean.getConsigNOR_Phone()));
					preparedStmt.setString(10, deBean.getConsignor_Consignee_Type());
		
					consignorSaveStatus = preparedStmt.executeUpdate();
			}

						
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}*/
	

// ****************************************************************	
	
	/*public void saveConsigNEEDetails() throws SQLException
	{
		
		DataEntryBean deBean=new DataEntryBean();

		deBean.setConsigNEE_Phone(txt_Consignee_Phone.getText());
		deBean.setConsignor_Consignee_Type("cnee");
		deBean.setConsigNEE_Name(txt_Consignee_Name.getText());
		deBean.setConsigNEE_Address(txt_Consignee_Address.getText());
		deBean.setConsigNEE_ContactPerson(txt_Consignee_ContactPerson.getText());
		deBean.setConsigNEE_Zipcode(txt_Consignee_Zipcode.getText());
		//deBean.setConsigNEE_Zipcode(Integer.valueOf(txt_Consignee_Zipcode.getText()));
		deBean.setConsigNEE_City(txt_Consignee_City.getText());
		deBean.setConsigNEE_State(combBox_Consignee_State.getValue());
		deBean.setConsigNEE_Country(combBox_Consignee_Country.getValue());
		deBean.setConsigNEE_Email(txt_Consignee_Email.getText());
		deBean.setConsigNEE_Remarks(txt_Consignee_Remarks.getText());
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		
		try 
		{
			
			if(consigneeExistanceStatus==false)
			{
				String query = "insert into consignor_consignee_details(phone_no,type,name,address,contact_person,zipcode,city,state,country,email,remarks,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";
			
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, String.valueOf(deBean.getConsigNEE_Phone()));
				preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
				preparedStmt.setString(3, deBean.getConsigNEE_Name());
				preparedStmt.setString(4, deBean.getConsigNEE_Address());
				preparedStmt.setString(5, deBean.getConsigNEE_ContactPerson());
				//preparedStmt.setInt(6, deBean.getConsigNEE_Zipcode());
				preparedStmt.setString(6, deBean.getConsigNEE_Zipcode());
				preparedStmt.setString(7, deBean.getConsigNEE_City());
				preparedStmt.setString(8, deBean.getConsigNEE_State());
				preparedStmt.setString(9, deBean.getConsigNEE_Country());
				preparedStmt.setString(10, deBean.getConsigNEE_Email());
				preparedStmt.setString(11, deBean.getConsigNEE_Remarks());

				consigneeSaveStatus = preparedStmt.executeUpdate();

			}
			else
			{
				String query = "update consignor_consignee_details SET name=?,address=?,contact_person=?,zipcode=?,city=?,state=?,"
						+ "country=?,email=?,remarks=?,indate=CURRENT_DATE,create_date=CURRENT_DATE,lastmodifieddate=CURRENT_TIMESTAMP "
						+ "where phone_no=? and type=?";
			
				preparedStmt = con.prepareStatement(query);
			
				preparedStmt.setString(1, deBean.getConsigNEE_Name());
				preparedStmt.setString(2, deBean.getConsigNEE_Address());
				preparedStmt.setString(3, deBean.getConsigNEE_ContactPerson());
				//preparedStmt.setInt(4, deBean.getConsigNEE_Zipcode());
				preparedStmt.setString(4, deBean.getConsigNEE_Zipcode());
				preparedStmt.setString(5, deBean.getConsigNEE_City());
				preparedStmt.setString(6, deBean.getConsigNEE_State());
				preparedStmt.setString(7, deBean.getConsigNEE_Country());
				preparedStmt.setString(8, deBean.getConsigNEE_Email());
				preparedStmt.setString(9, deBean.getConsigNEE_Remarks());
				preparedStmt.setString(10, String.valueOf(deBean.getConsigNEE_Phone()));
				preparedStmt.setString(11, deBean.getConsignor_Consignee_Type());
	
				consigneeSaveStatus = preparedStmt.executeUpdate();
		}

			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}*/

// ****************************************************************

	/*@FXML
	public void showConsigneeDetails() throws SQLException
	{
		//if(txt_Consignee_Phone.getText()!=null)
		//{
		//	if(!txt_Consignee_Phone.getText().equals(""))
		//	{
			
				DBconnection dbcon = new DBconnection();
				Connection con = dbcon.ConnectDB();
				
				Statement st = null;
				ResultSet rs = null;
				
				DataEntryBean deBean=new DataEntryBean();
				PreparedStatement preparedStmt = null;
				
				deBean.setConsigNEE_Phone(txt_Consignee_Phone.getText());
				deBean.setConsignor_Consignee_Type("cnee");

				try 
				{
					String sql = "select name,address,contact_person,zipcode,city,state,country,email,remarks from consignor_consignee_details where phone_no=? and type=?";
					
					preparedStmt = con.prepareStatement(sql);
					
					if(txt_Consignee_Phone.getText().equals(""))
					{
						preparedStmt.setString(1, "");
						preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
					}
					else
					{
						preparedStmt.setString(1, deBean.getConsigNEE_Phone());
						preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
					}
					System.out.println("Sql: "+preparedStmt);
					rs = preparedStmt.executeQuery();
					
					if (!rs.next()) 
					{
						txt_Consignee_Name.requestFocus();
					} 
					else 
					{
						do 
						{
							deBean.setConsigNEE_Name(rs.getString("name"));
							deBean.setConsigNEE_Address(rs.getString("address"));
							deBean.setConsigNEE_ContactPerson(rs.getString("contact_person"));
							deBean.setConsigNEE_Zipcode(rs.getString("zipcode"));
							deBean.setConsigNEE_City(rs.getString("city"));
							deBean.setConsigNEE_State(rs.getString("state"));
							deBean.setConsigNEE_Country(rs.getString("country"));
							deBean.setConsigNEE_Remarks(rs.getString("remarks"));
							deBean.setConsigNEE_Email(rs.getString("email"));
							
							
						} 
						while (rs.next());
						
						txt_Consignee_Name.setText(deBean.getConsigNEE_Name());
						txt_Consignee_Address.setText(deBean.getConsigNEE_Address());
						txt_Consignee_ContactPerson.setText(deBean.getConsigNEE_ContactPerson());
						txt_Consignee_Zipcode.setText(String.valueOf(deBean.getConsigNEE_Zipcode()));
						txt_Consignee_City.setText(deBean.getConsigNEE_City());
						combBox_Consignee_State.setValue(deBean.getConsigNEE_State());
						combBox_Consignee_Country.setValue(deBean.getConsigNEE_Country());
						txt_Consignee_Remarks.setText(deBean.getConsigNEE_Remarks());
						txt_Consignee_Email.setText(deBean.getConsigNEE_Email());
					}
				}
				
				catch (Exception e) 
				{
					System.out.println(e);
					e.printStackTrace();
				}
				
				finally 
				{
					dbcon.disconnect(null, st, rs, con);
				}
		//	}
		//}
	}*/
	


// ****************************************************************

/*	@FXML
	public void showConsignorDetails() throws SQLException
	{
		//if(txt_Consignor_Phone.getText()!=null)
	//	{
	//		if(!txt_Consignor_Phone.getText().equals(""))
		//	{
				
				System.out.println("Nor running...");
				
				DBconnection dbcon = new DBconnection();
				Connection con = dbcon.ConnectDB();
				
				Statement st = null;
				ResultSet rs = null;
				
				DataEntryBean deBean=new DataEntryBean();
				PreparedStatement preparedStmt = null;
				
				
				
				deBean.setConsigNOR_Phone(txt_Consignor_Phone.getText());
				deBean.setConsignor_Consignee_Type("cnor");

				try 
				{
					String sql = "select name,address,contact_person,zipcode,city,state,country,email,remarks from consignor_consignee_details where phone_no=? and type=?";
						
					preparedStmt = con.prepareStatement(sql);
					
					if(txt_Consignor_Phone.getText().equals(""))
					{
						preparedStmt.setString(1, "");
						preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
					}
					else
					{
						preparedStmt.setString(1, deBean.getConsigNOR_Phone());
						preparedStmt.setString(2, deBean.getConsignor_Consignee_Type());
					}
					System.out.println("nor Sql: "+preparedStmt);
					rs = preparedStmt.executeQuery();
					
					if (!rs.next()) 
					{
						txt_Consignor_Name.requestFocus();
					} 
					else 
					{
						do 
						{
							deBean.setConsigNOR_Name(rs.getString("name"));
							deBean.setConsigNOR_Address(rs.getString("address"));
							deBean.setConsigNOR_ContactPerson(rs.getString("contact_person"));
							deBean.setConsigNOR_Zipcode(rs.getString("zipcode"));
							deBean.setConsigNOR_City(rs.getString("city"));
							deBean.setConsigNOR_State(rs.getString("state"));
							deBean.setConsigNOR_Country(rs.getString("country"));
							//deBean.setConsigNOR_Remarks(rs.getString("remarks"));
							deBean.setConsigNOR_Email(rs.getString("email"));
							
							
						} 
						while (rs.next());
						
						txt_Consignor_Name.setText(deBean.getConsigNOR_Name());
						txt_Consignor_Address.setText(deBean.getConsigNOR_Address());
						txt_Consignor_ContactPerson.setText(deBean.getConsigNOR_ContactPerson());
						txt_Consignor_Zipcode.setText(String.valueOf(deBean.getConsigNOR_Zipcode()));
						txt_Consignor_City.setText(deBean.getConsigNOR_City());
						combBox_Consignor_State.setValue(deBean.getConsigNOR_State());
						combBox_Consignor_Country.setValue(deBean.getConsigNOR_Country());
						//txt_Consignor_Remarks.setText(deBean.getConsigNEE_Remarks());
						txt_Consignor_Email.setText(deBean.getConsigNOR_Email());
					}
				}
				
				catch (Exception e) 
				{
					System.out.println(e);
					e.printStackTrace();
				}
				
				finally 
				{
					dbcon.disconnect(null, st, rs, con);
				}
		//	}
		//}
	}	*/

// ****************************************************************
	

	@FXML
	public void goDimensionForm() throws IOException
	{
		
		/*if(checkBoxDimension.isSelected()==true)
		{
		Main m=new Main();
		m.showDimensionForm();
		
			txtVolumeWeight.requestFocus();	
		}
		*/
		
		//}
	}


// ****************************************************************
	
	/*public void loadTable() throws SQLException
	{
		tabledata_DataEntry.clear();
		
		//tableForwarder.refresh();
		
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		DataEntryBean deBean=new DataEntryBean();  
				
		int checkRow=0;
		
		int slno = 1;
		try {

			stmt = con.createStatement();
			sql = "select dbt.air_way_bill_number as awb,dbt.booking_date as bookingdate,dbt.origin as origin ,dbt.dailybookingtransaction2city as destination ,dbt.dailybookingtransaction2network as network,"
					+ "dbt.dailybookingtransaction2service as service, dbt.dox_nondox as dnd, dbt.packets as pcs, dbt.volumne_weight as v_weight, dbt.actual_weight as a_weight, "
					+ "dbt.billing_weight as b_weight,ccd.name as name,ccd.type as consignType from dailybookingtransaction as dbt, consignor_consignee_details as ccd "
					+ "where ccd.phone_no=dbt.consignor_phone OR ccd.phone_no=dbt.consignee_phone and dbt.mps_type='T'";
		
			System.out.println("SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				
				checkRow++;

				if(rs.getString("consignType").equals("cnor"))
				{
					//storeConsigNORName=rs.getString("name");
					deBean.setConsigNOR_Name(rs.getString("name"));
					
				}
				else if(rs.getString("consignType").equals("cnee"))
				{
					deBean.setConsigNEE_Name(rs.getString("name"));
					//storeConsigNEEName=rs.getString("name");
				}
				
				deBean.setSlno(slno);
				deBean.setAwbNo(rs.getString("awb"));;
				deBean.setBookingDate(date.format(rs.getDate("bookingdate")));
				deBean.setOrigin(rs.getString("origin"));
				deBean.setDestination(rs.getString("destination"));
				deBean.setNetwork(rs.getString("network"));
				deBean.setService(rs.getString("service"));
				deBean.setType(rs.getString("dnd"));
				deBean.setPcs(rs.getInt("pcs"));
				deBean.setVolumeWeight(rs.getDouble("v_weight"));
				deBean.setActualWeight(rs.getDouble("a_weight"));
				deBean.setActualWeight(rs.getDouble("b_weight"));
				
				
				//deBean.setConsigNEE_Name(rs.getString("name"));
				//deBean.setConsigNOR_Name(rs.getString("name"));
				
				if(checkRow==2){
					
					slno++;
				tabledata_DataEntry.add(new DataEntryTableBean(deBean.getSlno(), deBean.getAwbNo(), deBean.getBookingDate(), deBean.getOrigin(),
						deBean.getDestination(), deBean.getNetwork(), deBean.getService(), deBean.getConsigNOR_Name(), deBean.getConsigNEE_Name(),
						deBean.getType(), deBean.getActualWeight(), deBean.getVolumeWeight(), deBean.getBillWeight(), deBean.getPcs()));
				checkRow=0;
				
				}
			}

			tableView_DataEntry.setItems(tabledata_DataEntry);
		}

		catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}*/
	

// ****************************************************************	
	
	public void getCities() throws SQLException
	{
		
	}
	
	public void getZoneViaCityCode(String source,String destination) throws SQLException
	{
		//select (select zonedetail2zonetype as source from zonedetail where zone2city='DEL'),(select zonedetail2zonetype as destination from zonedetail where zone2city='KNJ')
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		String getServiceMode=null;
		
		boolean isSourceAvailable=false;
		boolean isDestinationAvailable=false;
		
		
		for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
		{
			if(source.equals(String.valueOf(bean.getZipcode())))
			{
				isSourceAvailable=true;
				break;
			}
			else
			{
				isSourceAvailable=false;
			}
		}
		
		
		for (LoadCityWithZipcodeBean bean : LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE) 
		{
			if(destination.equals(String.valueOf(bean.getZipcode())))
			{
				isDestinationAvailable=true;
				break;
			}
			else
			{
				isDestinationAvailable=false;
			}
		}
		
		
		/*for(LoadCityBean src:LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{
		
			if(src.getCityName().equals(source))
			{
				isSourceAvailable=true;
				break;
			}
			else
			{
				isSourceAvailable=false;
			}
		}
		
		for(LoadCityBean desti:LoadCity.SET_LOAD_CITYWITHNAME_FROM_PINCODEMASTER)
		{
			if(desti.getCityName().equals(destination))
			{
				isDestinationAvailable=true;
				break;
			}
			else
			{
				isDestinationAvailable=false;
			}
		}*/
		
		
		if(isSourceAvailable==true && isDestinationAvailable==true)
		{
		
		/*DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		String sql=null;
		
		
		PreparedStatement preparedStmt = null;*/
		String subQuery_serviceMode=null;
		
		for(LoadServiceWithNetworkBean bean:LoadServiceGroups.LIST_LOAD_SERVICES_FROM_SERVICETYPE)
		{
			if(bean.getServiceCode().equals(txtService.getText()))
			{
				getServiceMode=bean.getServiceMode();
				System.out.println("Service Name: "+bean.getServiceCode()+" | Mode: "+bean.getServiceMode());
				break;
			}
		}
		
		
		if(getServiceMode.equals("AR"))
		{
			subQuery_serviceMode="zone_air";
		}
		else if(getServiceMode.equals("SF"))
		{
			subQuery_serviceMode="zone_surface";
		}
		
		
		for(LoadPincodeBean pinBean: LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(source.equals(pinBean.getPincode()))
			{
				if(getServiceMode.equals("AR"))
				{
					fromZone=pinBean.getZone_air();
					break;
				}
				else if(getServiceMode.equals("SF"))
				{
					fromZone=pinBean.getZone_surface();
					break;
				}
			}
		}
		
		
		for(LoadPincodeBean pinBean: LoadPincodeForAll.list_Load_Pincode_from_Common)
		{
			if(destination.equals(pinBean.getPincode()))
			{
				if(getServiceMode.equals("AR"))
				{
					toZone=pinBean.getZone_air();
					break;
				}
				else if(getServiceMode.equals("SF"))
				{
					toZone=pinBean.getZone_surface();
					break;
				}
			}
		}
		
		
		System.out.println("Source zone >> : "+fromZone);
		System.out.println("Destination zone >> : "+toZone);
		
		calculateFinalAmountForWeight();
		
		/*try 
		{
			//sql = "select (select zone_code as source from pincode_master where city_name=? limit 1),(select zone_code as destination from pincode_master where city_name=? limit 1)";
				
			sql = "select (select "+subQuery_serviceMode+" as source from pincode_master where pincode=? limit 1),(select "+subQuery_serviceMode+" as destination from pincode_master where pincode=? limit 1)";
			
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, source);
			preparedStmt.setString(2, destination);
			System.out.println("Zone Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				
			} 
			else 
			{
				do 
				{
					fromZone=rs.getString("source");
					toZone=rs.getString("destination");
				} 
				while (rs.next());
			}
			
			System.out.println("Source zone: "+fromZone);
			System.out.println("Destination zone: "+toZone);
			
			calculateFinalAmountForWeight();
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
		}
		else if(isSourceAvailable==false)
		{
			
			alert.setTitle("Origin city is incorrect");
			alert.setContentText("Please enter the correct city in Origin");
			alert.showAndWait();
			txtOrigin.requestFocus();
		}
		else if(isDestinationAvailable==false)
		{
			alert.setTitle("Destination city is incorrect");
			alert.setContentText("Please enter the correct city in Destination");
			alert.showAndWait();
			txtDestination.requestFocus();
		}
	}
	
// ****************************************************************	

	public void calculateFinalAmountForWeight() throws NumberFormatException, SQLException
	{
		//CalculateWeight cw=new CalculateWeight();
		
		//System.out.println("Type: "+combBox_Type.getValue());
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		String[] networkCode=txtNetwork.getText().replaceAll("\\s+","").split("\\|");
		
		//double finalAmount = cw.getClientRateDetails(clientCode[1], networkCode[1], txtService.getText(), fromZone, toZone, combBox_Type.getValue(), Double.valueOf(txtBillWeight.getText()));
		
		NewCalculateWeight calWt=new NewCalculateWeight();
		
		double finalAmount=	calWt.getNewClientRateDetails(clientCode[1], networkCode[1], txtService.getText(), fromZone,toZone,Double.valueOf(txtBillWeight.getText()));
		
		txtBasicAmount.setText(df.format(finalAmount));
		combBox_Insurance.requestFocus();
		
	}
	

// ****************************************************************

	public void getFuelAmount()
	{
		
		double oda_min=0;
		double oda_kg=0;
		
		double opa_min=0;
		double opa_kg=0;
		double billingWeight=Double.valueOf(txtBillWeight.getText());
		
		totalVAS_amount=0;
		double basicAmount=0;
		
		VAS_CalculationBean vcBean=new VAS_CalculationBean();
		
		basicAmount=Double.valueOf(txtBasicAmount.getText());
		for(ClientVASBean cvBean:list_ClientVAS)
		{
			fuelRate=cvBean.getFuelSurcharge();
		}
		
		TaxCalculator taxCalc=new TaxCalculator();
		//txtFuelAmount.setText(df.format(taxCalc.calculateFuelAmount(basicAmount,fuelRate)));
		getExtraVAS_Amount();
		
		
		
		if(tabledata_VAS.size()>0)
		{
			for(VAS_Table_Bean vsBean: tabledata_VAS)
			{
				totalVAS_amount=totalVAS_amount+vsBean.getVasAmount();
			}
		}
		
		
		for(Oda_Opa_Bean bean: List_ODA_OPA_FromPincode)
		{
			System.out.println("VAS loop running..." +billingWeight);
			
			if(bean.getType_org_desti().equals("origin") && !bean.getType_ODA_OPA().equals("Regular"))
			{
				System.out.println("Check Origin");
				for(ClientVASBean vasBean:list_ClientVAS)
				{
					vcBean.setOda_Min_Amt(vasBean.getOda_Min());
					vcBean.setOda_Kg_Amt(vasBean.getOda_Kg()*billingWeight);
					//oda_min=vasBean.getOda_Min();
					//oda_kg=vasBean.getOda_Kg();
				}
			}
			else if(bean.getType_org_desti().equals("destination") && !bean.getType_ODA_OPA().equals("Regular"))
			{
				System.out.println("Check Destination");
				
				for(ClientVASBean vasBean:list_ClientVAS)
				{
					vcBean.setOpa_Min_Amt(vasBean.getOpa_Min());
					vcBean.setOpa_Kg_Amt(vasBean.getOpa_Kg()*billingWeight);
					
					//opa_min=vasBean.getOpa_Min();
					//opa_kg=vasBean.getOpa_Kg();
				}
			}
		}
		
		System.out.println("Origin ODA_min >> "+vcBean.getOda_Min_Amt()+" | KG amt >> "+vcBean.getOda_Kg_Amt());
		System.out.println("Destination OPA_min >> "+vcBean.getOpa_Min_Amt()+" | KG amt >> "+vcBean.getOpa_Kg_Amt());
		
		if(vcBean.getOda_Min_Amt()>vcBean.getOda_Kg_Amt())
		{
			oda_amount=vcBean.getOda_Min_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOda_Min_Amt();
		}
		else
		{
			oda_amount=vcBean.getOda_Kg_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOda_Kg_Amt();
		}
		
		if(vcBean.getOpa_Min_Amt()>vcBean.getOpa_Kg_Amt())
		{
			opa_amount=vcBean.getOpa_Min_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOpa_Min_Amt();
		}
		else
		{
			opa_amount=vcBean.getOpa_Kg_Amt();
			totalVAS_amount=totalVAS_amount+vcBean.getOpa_Kg_Amt();
		}
		
		
		txtTotalVASAmount.setText(df.format(totalVAS_amount+Double.valueOf(txtInsuranceAmount.getText())));
		System.out.println("VAS Amount: "+totalVAS_amount);
		System.out.println("Total VAS: >> "+(totalVAS_amount+Double.valueOf(txtInsuranceAmount.getText())));
		totalVAS_amount=0;
	}
	
// ****************************************************************

	public void getTaxableValue() throws SQLException
	{
		txtTaxableValue.setText(df.format(Double.valueOf(txtBasicAmount.getText()) + 
								Double.valueOf(txtFuelAmount.getText()) + 
								Double.valueOf(txtTotalVASAmount.getText())));
		
		
		getGST_rate();
		
		for(TaxCalculatorBean txBean:LIST_GST_TAXES)
		{
			deBean_global.setTax_name1(txBean.getTaxname1());
			deBean_global.setTax_name2(txBean.getTaxname2());
			deBean_global.setTax_name3(txBean.getTaxname3());
			
			TaxCalculator tc=new TaxCalculator();
			if(CLIENT_STATIC_STATE_CODE.equals(CLIENT_DB_STATE_CODE))
			{
				//System.out.println("Form if >>> Static City: "+CLIENT_STATIC_CITY_NAME+" | DB CITY: "+CLIENT_DB_CITY_NAME);
				tc.calculateGST(Double.valueOf(txtTaxableValue.getText()), txBean.getTaxrate1(), txBean.getTaxrate2(),0);

				deBean_global.setTax_rate1(txBean.getTaxrate1());
				deBean_global.setTax_rate2(txBean.getTaxrate2());
				deBean_global.setTax_rate3(0);
				deBean_global.setTax_amount1(TaxCalculator.taxAmount1);
				deBean_global.setTax_amount2(TaxCalculator.taxAmount2);
				deBean_global.setTax_amount3(0);
				break;
			}
			else
			{
				//System.out.println("Form else >>> Static City: "+CLIENT_STATIC_CITY_NAME+" | DB CITY: "+CLIENT_DB_CITY_NAME);
				tc.calculateGST(Double.valueOf(txtTaxableValue.getText()), 0, 0,txBean.getTaxrate3());
				deBean_global.setTax_rate1(0);
				deBean_global.setTax_rate2(0);
				deBean_global.setTax_rate3(txBean.getTaxrate3());
				deBean_global.setTax_amount1(0);
				deBean_global.setTax_amount2(0);
				deBean_global.setTax_amount3(TaxCalculator.taxAmount3);
				break;
			}
		}
		
		double otherVAS_total=0;
		
		list_All_Amount_and_selectedVasItems.clear();
		
		DataEntryBean deBean=new DataEntryBean();
		
		deBean.setBasicAmount(Double.valueOf(txtBasicAmount.getText()));
		deBean.setFuelAmount(Double.valueOf(txtFuelAmount.getText()));
		deBean.setInvoiceAmount(Double.valueOf(txtInvoiceAmount.getText()));
		deBean.setInsuranceAmount(Double.valueOf(txtInsuranceAmount.getText()));
		deBean.setDocketCharges(Double.valueOf(txtDocketCharges.getText()));
		deBean.setOda_VAS(oda_amount);
		deBean.setOpa_VAS(opa_amount);
		deBean.setDoi_VAS(deBean_global.getDoi_VAS());
		deBean.setHoldAtOffice_VAS(deBean_global.getHoldAtOffice_VAS());
		deBean.setOct_Fee_VAS(deBean_global.getOct_Fee_VAS());
		deBean.setOss_VAS(deBean_global.getOss_VAS());
		deBean.setGreenTax_VAS(deBean_global.getGreenTax_VAS());
		deBean.setReversePickup_VAS(deBean_global.getReversePickup_VAS());
		deBean.setCod_amount_vas(deBean_global.getCod_amount_vas());
		deBean.setAddressCorrection_VAS(deBean_global.getAddressCorrection_VAS());
		deBean.setToPay_VAS(deBean_global.getToPay_VAS());
		deBean.setTdd_VAS(deBean_global.getTdd_VAS());
		deBean.setCriticalService_VAS(deBean_global.getCriticalService_VAS());
		deBean.setHfd_Charges_VAS(deBean_global.getHfd_Charges_VAS());
		deBean.setHfd_floor_number(deBean_global.getHfd_floor_number());
		
		if(tabledata_VAS.size()>0)
		{
			for(VAS_Table_Bean otherVAS_Bean: tabledata_VAS)
			{
				otherVAS_total=otherVAS_total+otherVAS_Bean.getVasAmount();
			}
			
			deBean.setOtherVasTotal(otherVAS_total);
		}
		else
		{
			deBean.setOtherVasTotal(otherVAS_total);
		}
		
		deBean.setTax_amount1(deBean_global.getTax_amount1());
		deBean.setTax_amount2(deBean_global.getTax_amount2());
		deBean.setTax_amount3(deBean_global.getTax_amount3());
		deBean.setGst(deBean.getTax_amount1()+deBean.getTax_amount2()+deBean.getTax_amount3());
		deBean.setTaxableValueAmount(Double.valueOf(txtTaxableValue.getText()));
		deBean.setTotal(deBean.getTaxableValueAmount()+deBean.getGst());
		
		list_All_Amount_and_selectedVasItems.add(deBean);
		
		
	}

	
// ****************************************************************
	
	public void showGST_and_TotalAmt()
	{
		for(DataEntryBean deBean: list_All_Amount_and_selectedVasItems)
		{
			txtGSTAmt.setText(df.format(deBean.getGst()));
			txtTotal.setText(df.format(deBean.getTotal()));
			
		}
	}

// ****************************************************************
	
	/*	public void loadOriginViaBranch() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		
		PreparedStatement preparedStmt = null;
		
		try 
		{
			String sql = "select bd.brndetail2address as addresscode, ad.city as city from brndetail as bd, address as ad where bd.brndetail2address=ad.addressid and bd.code=?";
				
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, HomeController.userBranch);
			
			System.out.println("Origin Sql: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				
			} 
			else 
			{
				do 
				{
					txtOrigin.setText(rs.getString("city"));
				} 
				while (rs.next());
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}*/

// ****************************************************************	
	
	public void getClientVASDetails() throws SQLException
	{
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		
		if(CLIENT==null && SERVICE==null)
		{
		
			loadVASData();
			
		}
		else if(CLIENT.equals(clientCode[1]) && SERVICE.equals(txtService.getText()))
		{
			//System.out.println("Not Running :: same value/////");
		}
		else
		{
			loadVASData();
		
		}
	}
	
// ****************************************************************	
	
	public void loadVASData() throws SQLException
	{
		list_ClientVAS.clear();
		
		String[] clientCode=txtClient.getText().replaceAll("\\s+","").split("\\|");
		//String[] networkCode=txtNetwork.getText().replaceAll("\\s+","").split("\\|");
		
		ClientVASBean cvBean=new ClientVASBean();
		
		cvBean.setClient(clientCode[1]);
		//cvBean.setNetwork(networkCode[1]);
		cvBean.setService(txtService.getText());
		//>>
		
		ResultSet rs = null;
		
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;


		try 
		{
			
			sql = MasterSQL_DataEntry_Utils.CLIENTVAS_IN_DATAENTRY_RETRIEVE_SQL;

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,cvBean.getClient());
			preparedStmt.setString(2,cvBean.getService());
			
			//System.out.println("ClientVAS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
					
				cvBean.setCod_Min(rs.getDouble("cod_min"));
				cvBean.setCod_Percentage(rs.getDouble("cod_per"));
				cvBean.setInsurance_Owner_Min(rs.getDouble("insur_own_min"));
				cvBean.setInsurance_Owner_Percentage(rs.getDouble("insur_own_per"));
				cvBean.setInsurance_Carrier_Min(rs.getDouble("insur_carr_min"));
				cvBean.setInsurance_Carrier_Percentage(rs.getDouble("insur_carr_per"));
				cvBean.setOverSizeShpmnt_Min(rs.getDouble("oss_min"));
				cvBean.setOverSizeShpmnt_Kg(rs.getDouble("oss_perkg"));
				cvBean.setDocketCharge_Min(rs.getDouble("docket_chrgs"));
				cvBean.setOda_Min(rs.getDouble("oda_min"));
				cvBean.setOda_Kg(rs.getDouble("oda_perkg"));
				cvBean.setOpa_Min(rs.getDouble("opa_min"));
				cvBean.setOpa_Kg(rs.getDouble("opa_perkg"));
				cvBean.setAdvancementFee_Min(rs.getDouble("adv_fee_min"));
				cvBean.setAdvancementFee_Percentage(rs.getDouble("adv_fee_per"));
				cvBean.setDeliveryOnInvoice(rs.getDouble("delivery_on_invoice"));
				cvBean.setFuelSurcharge(rs.getDouble("fuel_rate"));
				cvBean.setHdf_Charges_Min(rs.getDouble("hfd_min"));
				cvBean.setHdf_Charges_PerPcs(rs.getDouble("hfd_per_pcs_kg"));
				cvBean.setHfd_type_pcs_kg(rs.getString("hfd_type"));
				cvBean.setHfd_free_flr(rs.getInt("hfd_free_floor"));
				cvBean.setToPay(rs.getDouble("topay"));
				cvBean.setAddressCorrectionCharges(rs.getDouble("addres_correction_chrgs"));
				cvBean.setHoldAtOffice_Min(rs.getDouble("hold_at_office_min"));
				cvBean.setHoldAtOffice_Kg(rs.getDouble("hold_at_office_perkg"));
				cvBean.setGreenTax(rs.getDouble("green_tax"));
				cvBean.setReversePickup_Min(rs.getDouble("reverse_pickup_min"));
				cvBean.setReversePickup_slab(rs.getDouble("reverse_pickup_slab"));
				cvBean.settDD_Rate(rs.getDouble("tdd_min"));
				cvBean.settDD_Additional(rs.getDouble("tdd_add"));
				cvBean.settDD_Slab(rs.getDouble("tdd_slab"));
				cvBean.setCriticalSrvc_Rate(rs.getDouble("critical_min"));
				cvBean.setCriticalSrvc_Additional(rs.getDouble("critical_add"));
				cvBean.setCriticalSrvc_Slab(rs.getDouble("critical_slab"));
				
				
				list_ClientVAS.add(cvBean);
			}
			
			CLIENT=clientCode[1];
			SERVICE=txtService.getText();
			
			System.err.println("Client VAS Fresh loaded from DB for Client :"+clientCode[1]+" and Service: "+txtService.getText());
			System.out.println("COD Min: "+cvBean.getCod_Min());
			System.out.println("ODA: "+cvBean.getOda_Min());
			System.out.println("ODA kg: "+cvBean.getOda_Kg());
			System.out.println("OSS: "+cvBean.getOverSizeShpmnt_Min());
			System.out.println("OPA: "+cvBean.getOpa_Min());
			System.out.println("OPA kg: "+cvBean.getOpa_Kg());
			System.out.println("Carrier: "+cvBean.getInsurance_Carrier_Min());
			System.out.println("Owner: "+cvBean.getInsurance_Owner_Min());
			System.out.println("OCT Fee: "+cvBean.getAdvancementFee_Min());
			System.out.println("Hold At: "+cvBean.getHoldAtOffice_Min());
			System.out.println("HFD: "+cvBean.getHdf_Charges_Min());
			System.out.println("Green Tax: "+cvBean.getGreenTax());
			System.out.println("Address Correction: "+cvBean.getAddressCorrectionCharges());
			System.out.println("To Pay: "+cvBean.getToPay());
			System.out.println("Docket: "+cvBean.getDocketCharge_Min());
			System.out.println("DOI: "+cvBean.getDeliveryOnInvoice());
			System.out.println("TDD: "+cvBean.gettDD_Rate());
			System.out.println("Critical: "+cvBean.getCriticalSrvc_Rate());
			System.out.println("Fule: "+cvBean.getFuelSurcharge());
			System.out.println("HFD type: "+cvBean.getHfd_type_pcs_kg());
			System.out.println("HFD floor: "+cvBean.getHfd_free_flr());
			System.out.println("HFD per pcs kg: "+cvBean.getHdf_Charges_PerPcs());

			
			
		}
		
		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	

// ****************************************************************	

	public void getExtraVAS_Amount()
	{
		VAS_CalculationBean vcBean=new VAS_CalculationBean();
		NewCalculateWeight newCalWt=new NewCalculateWeight();
		
		double billingWeight=Double.valueOf(txtBillWeight.getText());
		
		
		for(ClientVASBean cvBean:list_ClientVAS)
		{
			
			System.err.println("Client VAS After selection >>>>>>>>>>> :");
			System.out.println("COD Min: "+cvBean.getCod_Min());
			System.out.println("ODA: "+cvBean.getOda_Min());
			System.out.println("ODA kg: "+cvBean.getOda_Kg());
			System.out.println("OSS: "+cvBean.getOverSizeShpmnt_Min());
			System.out.println("OPA: "+cvBean.getOpa_Min());
			System.out.println("OPA kg: "+cvBean.getOpa_Kg());
			System.out.println("Carrier: "+cvBean.getInsurance_Carrier_Min());
			System.out.println("Owner: "+cvBean.getInsurance_Owner_Min());
			System.out.println("OCT Fee: "+cvBean.getAdvancementFee_Min());
			System.out.println("Hold At: "+cvBean.getHoldAtOffice_Min());
			System.out.println("HFD: "+cvBean.getHdf_Charges_Min());
			System.out.println("Green Tax: "+cvBean.getGreenTax());
			System.out.println("Address Correction: "+cvBean.getAddressCorrectionCharges());
			System.out.println("To Pay: "+cvBean.getToPay());
			System.out.println("Docket: "+cvBean.getDocketCharge_Min());
			System.out.println("DOI: "+cvBean.getDeliveryOnInvoice());
			System.out.println("TDD: "+cvBean.gettDD_Rate());
			System.out.println("Critical: "+cvBean.getCriticalSrvc_Rate());
			System.out.println("Fule: "+cvBean.getFuelSurcharge());
			System.out.println("HFD type: "+cvBean.getHfd_type_pcs_kg());
			System.out.println("HFD floor: "+cvBean.getHfd_free_flr());
			System.out.println("HFD per pcs kg: "+cvBean.getHdf_Charges_PerPcs());
			
			//list_selectedVasItems.clear();
			if(checkBoxDOI.isSelected()==true)
			{
				deBean_global.setDoi_VAS(cvBean.getDeliveryOnInvoice());
				System.out.println("DOI: Yes >> ======= " +deBean_global.getDoi_VAS());
			}
			else
			{
				deBean_global.setDoi_VAS(0);
				System.out.println("DOI: No >> ======= " +deBean_global.getDoi_VAS());
			}
			
			if(checkBoxCOD.isSelected()==true)
			{
				double codAmt=0;
				
				if(!txtInsuranceRate.equals(""))
				{
					codAmt=(Double.valueOf(txtInvoiceAmount.getText())*cvBean.getCod_Percentage())/100;
					
					if(codAmt>cvBean.getCod_Min())
					{
						deBean_global.setCod_amount_vas(codAmt);
					}
					else
					{
						deBean_global.setCod_amount_vas(cvBean.getCod_Min());
					}
					
					deBean_global.getCod_amount_vas();
					
				}
				else
				{
					deBean_global.setCod_amount_vas(0);
				}
				
				//deBean_global.setDoi_VAS(cvBean.getDeliveryOnInvoice());
				System.out.println("COD Amt: Yes >> ======= " +deBean_global.getCod_amount_vas());
			}
			else
			{
				deBean_global.setCod_amount_vas(0);
				System.out.println("COD Amt: No >> ======= " +deBean_global.getCod_amount_vas());
			}
			
			if(checkBoxHoldAtOffice.isSelected()==true)
			{
				vcBean.setHoldAtOffice_Min_Amt(cvBean.getHoldAtOffice_Min());
				vcBean.setHoldAtOffice_Kg_Amt(cvBean.getHoldAtOffice_Kg()*billingWeight);
				
				if(vcBean.getHoldAtOffice_Min_Amt()>vcBean.getHoldAtOffice_Kg_Amt())
				{
					deBean_global.setHoldAtOffice_VAS(vcBean.getHoldAtOffice_Min_Amt());
				}
				else
				{
					deBean_global.setHoldAtOffice_VAS(vcBean.getHoldAtOffice_Kg_Amt());
				}
				
				//deBean_global.setHoldAtOffice_VAS(cvBean.getHoldAtOffice_Min());
				System.out.println("Hold at ofc: Yes >> ======= "+deBean_global.getHoldAtOffice_VAS());
			}
			else
			{
				deBean_global.setHoldAtOffice_VAS(0);
				System.out.println("Hold at ofc: No >> ======= "+deBean_global.getHoldAtOffice_VAS());
			}
			
			if(checkBoxOCT_Fee.isSelected()==true)
			{
				vcBean.setAdvancementFee_Min_Amt(cvBean.getAdvancementFee_Min());
				vcBean.setAdvancementFee_Percentage_Amt(cvBean.getAdvancementFee_Percentage()*billingWeight);
				
				if(vcBean.getAdvancementFee_Min_Amt()>vcBean.getAdvancementFee_Percentage_Amt())
				{
					deBean_global.setOct_Fee_VAS(vcBean.getAdvancementFee_Min_Amt());
				}
				else
				{
					deBean_global.setOct_Fee_VAS(vcBean.getAdvancementFee_Percentage_Amt());
				}
				
				//deBean_global.setOct_Fee_VAS(cvBean.getAdvancementFee_Min());
				System.out.println("OCT Fee: Yes >> ======= "+deBean_global.getOct_Fee_VAS());
			}
			else
			{
				deBean_global.setOct_Fee_VAS(0);
				System.out.println("OCT Fee: No >> ======= "+deBean_global.getOct_Fee_VAS());
			}
			
			if(checkBoxHFDCharges.isSelected()==true)
			{
				//txtHFD_Floors.setDisable(true);
				//txtHFD_Floors.setText("3");
				
				if(Integer.valueOf(txtHFD_Floors.getText())>cvBean.getHfd_free_flr())
				{

					deBean_global.setHfd_floor_number(Integer.valueOf(txtHFD_Floors.getText()));
					vcBean.setHdf_Charges_Min_Amt(cvBean.getHdf_Charges_Min());
					//vcBean.setHdf_Charges_PerPcs_Amt(cvBean.getHdf_Charges_PerPcs());
					
					if(cvBean.getHfd_type_pcs_kg().equals("per kg"))
					{
						vcBean.setHdf_Charges_PerPcs_Amt(cvBean.getHdf_Charges_PerPcs()*billingWeight);
						if(vcBean.getHdf_Charges_Min_Amt()>vcBean.getHdf_Charges_PerPcs_Amt())
						{
							
							deBean_global.setHfd_Charges_VAS(vcBean.getHdf_Charges_Min_Amt());
						}
						else
						{
							deBean_global.setHfd_Charges_VAS(vcBean.getHdf_Charges_PerPcs_Amt());
						}
						
					}
					else
					{
						vcBean.setHdf_Charges_PerPcs_Amt(cvBean.getHdf_Charges_PerPcs()*Integer.valueOf(txtPcs.getText()));
						if(vcBean.getHdf_Charges_Min_Amt()>vcBean.getHdf_Charges_PerPcs_Amt())
						{
							deBean_global.setHfd_Charges_VAS(vcBean.getHdf_Charges_Min_Amt());
						}
						else
						{
							deBean_global.setHfd_Charges_VAS(vcBean.getHdf_Charges_PerPcs_Amt());
						}
					}
				}
				else
				{
					deBean_global.setHfd_floor_number(cvBean.getHfd_free_flr());
					deBean_global.setHfd_Charges_VAS(0);
				}
				
				//deBean_global.setHfd_Charges_VAS(cvBean.getHdf_Charges_Min());
				System.out.println("HFD: Yes >> ======= "+deBean_global.getHfd_Charges_VAS());
			}
			else
			{
				//txtHFD_Floors.clear();
				//txtHFD_Floors.setDisable(true);
				deBean_global.setHfd_floor_number(0);
				deBean_global.setHfd_Charges_VAS(0);
				System.out.println("HFD: No >> ======= "+deBean_global.getHfd_Charges_VAS());
			}
			
			if(checkBoxGreenTax.isSelected()==true)
			{
				deBean_global.setGreenTax_VAS(cvBean.getGreenTax());
				System.out.println("Green Tax:  Yes >> ======= "+deBean_global.getGreenTax_VAS());
			}
			else
			{
				deBean_global.setGreenTax_VAS(0);
				System.out.println("Green Tax:  No >> ======= "+deBean_global.getGreenTax_VAS());
			}
			
			if(checkBoxReversePickup.isSelected()==true)
			{
				
				deBean_global.setReversePickup_VAS(cvBean.getReversePickup_Min());
				System.out.println("Rvrs Pickup: Yes >> ======= "+deBean_global.getReversePickup_VAS());
			}
			else
			{
				deBean_global.setReversePickup_VAS(0);
				System.out.println("Rvrs Pickup: No >> ======= "+deBean_global.getReversePickup_VAS());
			}
			
			if(rdBtnTDD.isSelected()==true)
			{
				if(cvBean.gettDD_Slab()!=0)
				{
				double tdd=newCalWt.getTDD(cvBean.gettDD_Rate(), cvBean.gettDD_Additional(), cvBean.gettDD_Slab(), billingWeight);
				deBean_global.setTdd_VAS(tdd);
				System.out.println("TDD: Yes >> ======= "+deBean_global.getTdd_VAS());
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("TDD slab Alert");
					alert.setHeaderText(null);
					alert.setContentText("Slab for TDD is not defined...");
					alert.showAndWait();
					deBean_global.setTdd_VAS(0);
				}
			}
			else
			{
				deBean_global.setTdd_VAS(0);
			}
		
			
			if(rdBtnCrit.isSelected()==true)
			{
				if(cvBean.getCriticalSrvc_Slab()!=0)
				{
					double crit=newCalWt.getCritical(cvBean.getCriticalSrvc_Rate(), cvBean.getCriticalSrvc_Additional(), cvBean.getCriticalSrvc_Slab(), billingWeight);
					deBean_global.setCriticalService_VAS(crit);
					System.out.println("Crit. Serivce: Yes >> ======= "+deBean_global.getCriticalService_VAS());
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("TDD slab Alert");
					alert.setHeaderText(null);
					alert.setContentText("Slab for Critical Service is not defined...");
					alert.showAndWait();
					deBean_global.setCriticalService_VAS(0);
				}
			}
			else
			{
				deBean_global.setCriticalService_VAS(0);
			}
			
			if(checkBoxAddressCorrection.isSelected())
			{
				deBean_global.setAddressCorrection_VAS(cvBean.getAddressCorrectionCharges());
				System.out.println("Add. Corrt. Yes >> ======= "+deBean_global.getAddressCorrection_VAS());
			}
			else
			{
				deBean_global.setAddressCorrection_VAS(0);
				System.out.println("Add. Corrt. No >> ======= "+deBean_global.getAddressCorrection_VAS());
			}
			
			
			if(checkBoxToPay.isSelected())
			{
				deBean_global.setToPay_VAS(cvBean.getToPay());
				System.out.println("To Pay Yes >> ======= "+deBean_global.getToPay_VAS());
			}
			else
			{
				deBean_global.setToPay_VAS(0);
				System.out.println("To Pay No >> ======= "+deBean_global.getToPay_VAS());
			}
			
			if(checkBoxOSS.isSelected())
			{
				vcBean.setOverSizeShpmnt_Min_Amt(cvBean.getOverSizeShpmnt_Min());
				vcBean.setOverSizeShpmnt_Kg_Amt(cvBean.getOverSizeShpmnt_Kg()*billingWeight);
				
				if(vcBean.getOverSizeShpmnt_Min_Amt()>vcBean.getOverSizeShpmnt_Kg_Amt())
				{
					deBean_global.setOss_VAS(vcBean.getOverSizeShpmnt_Min_Amt());
				}
				else
				{
					deBean_global.setOss_VAS(vcBean.getOverSizeShpmnt_Kg_Amt());
				}
				
				//deBean_global.setOss_VAS(cvBean.getOverSizeShpmnt_Min());
				System.out.println("To OSS Yes >> ======= "+deBean_global.getOss_VAS());
			}
			else
			{
				deBean_global.setOss_VAS(0);
				System.out.println("To OSS No >> ======= "+deBean_global.getOss_VAS());
			}
			
			//list_selectedVasItems.add(deBean_global);
			
			break;
		}
		
		
		totalVAS_amount=deBean_global.getDoi_VAS()+deBean_global.getHoldAtOffice_VAS()+deBean_global.getOct_Fee_VAS()+deBean_global.getToPay_VAS()+deBean_global.getCod_amount_vas()+
							deBean_global.getHfd_Charges_VAS()+deBean_global.getGreenTax_VAS()+deBean_global.getReversePickup_VAS()+deBean_global.getOss_VAS()+
								deBean_global.getTdd_VAS()+deBean_global.getCriticalService_VAS()+deBean_global.getAddressCorrection_VAS()+Double.valueOf(txtDocketCharges.getText());
		
		System.out.println("VAS from get >>  "+totalVAS_amount);
		

}

// ****************************************************************
	
	public void loadInsuranceRateAndAmount()
	{
		System.out.println("load Insurance Running... ");
		
		
		for(ClientVASBean cvBean: list_ClientVAS)
		{
			if(combBox_Insurance.getValue().equals("Owner"))
			{
				txtInsuranceRate.setText(String.valueOf(cvBean.getInsurance_Owner_Percentage()));
				txtInsuranceAmount.setText(String.valueOf(cvBean.getInsurance_Owner_Min()));
			}
			else if(combBox_Insurance.getValue().equals("Carrier"))
			{
				txtInsuranceRate.setText(String.valueOf(cvBean.getInsurance_Carrier_Percentage()));
				txtInsuranceAmount.setText(String.valueOf(cvBean.getInsurance_Carrier_Min()));
			}
			
			break;
		}
		
	}
	
	
// ******************************************************************
	
	
	public void getInsuranceAmount()
	{
		double insRateAmount=0;
		
		if(!txtInsuranceRate.equals(""))
		{
			insRateAmount=(Double.valueOf(txtInvoiceAmount.getText())*Double.valueOf(txtInsuranceRate.getText()))/100;
			
			if(insRateAmount>Double.valueOf(txtInsuranceAmount.getText()))
			{
				txtInsuranceAmount.setText(df.format(insRateAmount));
			}
			
		}
	}
	
// ****************************************************************
	
	public void loadDimensionTableViaAWBno() throws SQLException
	{
		
		tabledata_Dimension.clear();
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		
		PreparedStatement preparedStmt = null;
		
		try 
		{
			String sql = "select pcs,weight,height,length,width from dimensions where awbno=?";
				
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, OutwardPageController.awbNoForDataEntryPopup);
			
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				
			} 
			else 
			{
				do 
				{
					DimensionBean diBean = new DimensionBean();
					
					diBean.setPcs(rs.getInt("pcs"));
					diBean.setLength(rs.getDouble("length"));
					diBean.setWidth(rs.getDouble("width"));
					diBean.setHeight(rs.getDouble("height"));
					diBean.setWeight(rs.getDouble("weight"));
		
							
					tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(), null,diBean.getPcs(), 
							diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));
					
				} 
				while (rs.next());
				
				tableDimension.setItems(tabledata_Dimension);
				
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}

	
// ****************************************************************		
	
	public void addDimensions()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if(Integer.valueOf(txtPCS_To_addDimensions.getText())>Integer.valueOf(txtPcs.getText()))
		{
			alert.setTitle("Alert");
			alert.setContentText("Dimension PCS not more than Total PCS");
			alert.showAndWait();
		}
		
	}
	
// ****************************************************************	

	public void addVAS_InTable()
	{
		if(!txtAddOnExpense.getText().equals("") && !txtExpenseAmount.getText().equals(""))
		{
			slno_Vas_Table++;
			
			String[] vasCode=txtAddOnExpense.getText().replaceAll("\\s+","").split("\\|");
			
			LoadVAS_Item_Bean vasBean=new LoadVAS_Item_Bean();
			
			vasBean.setVasCode(vasCode[1]);
			vasBean.setVasName(vasCode[0]);
			vasBean.setVasAmount(Double.valueOf(txtExpenseAmount.getText()));
			
			tabledata_VAS.add(new VAS_Table_Bean(slno_Vas_Table, vasBean.getVasCode(), vasBean.getVasName(),vasBean.getVasAmount()));
			
			tableVAS.setItems(tabledata_VAS);
		}
	}
	


// ****************************************************************
	
	public void checkAwbNo_InDailyBookingVAS() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;

		PreparedStatement preparedStmt = null;
		
		boolean isAwbNoExist=true;

		try 
		{
			String sql = "select master_awb_no from daily_booking_vas where master_awb_no=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, txtAwbNo.getText());

			//System.out.println("VAS sql: "+ preparedStmt);
			
			rs = preparedStmt.executeQuery();

			if (!rs.next()) 
			{
				//System.out.println("VAS insert>>>>>>");
				isAwbNoExist=false;
				// insert
				saveDataToDailyBookingVAS_Table(isAwbNoExist);
				
			} 
			else 
			{
				//System.out.println("VAS update>>>>>>");
				isAwbNoExist=true;
				// update
				saveDataToDailyBookingVAS_Table(isAwbNoExist);
				
			}
		}

		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
// ****************************************************************

	public void saveDataToDailyBookingVAS_Table(boolean isAwbNoExist) throws SQLException
	{
		DataEntryBean deBean=new DataEntryBean();

		deBean.setAwbNo(txtAwbNo.getText());
		deBean.setHfd_floor_number(deBean_global.getHfd_floor_number());
		deBean.setDoi_VAS(deBean_global.getDoi_VAS());
		deBean.setHoldAtOffice_VAS(deBean_global.getHoldAtOffice_VAS());
		deBean.setOct_Fee_VAS(deBean_global.getOct_Fee_VAS());
		deBean.setHfd_Charges_VAS(deBean_global.getHfd_Charges_VAS());
		deBean.setGreenTax_VAS(deBean_global.getGreenTax_VAS());
		deBean.setReversePickup_VAS(deBean_global.getReversePickup_VAS());
		deBean.setTdd_VAS(deBean_global.getTdd_VAS());
		deBean.setCriticalService_VAS(deBean_global.getCriticalService_VAS());
		deBean.setAddressCorrection_VAS(deBean_global.getAddressCorrection_VAS());
		deBean.setOss_VAS(deBean_global.getOss_VAS());
		deBean.setToPay_VAS(deBean_global.getToPay_VAS());
		deBean.setCod_amount_vas(deBean_global.getCod_amount_vas());
		deBean.setOda_VAS(oda_amount);
		deBean.setOpa_VAS(opa_amount);
		
		String vascode=null;
		String vasname=null;
		String vasAmt=null;
		double vasAmtTotal=0;
		
		
		if(tabledata_VAS.size()>0)
		{
		if(tabledata_VAS.size()==1)
		{
			System.out.println("VAS data size 1 running...");
			for(VAS_Table_Bean vsBean:tabledata_VAS)
			{
				vascode=vsBean.getVasCode();
				vasname=vsBean.getVasName();
				vasAmt=String.valueOf(vsBean.getVasAmount());
				vasAmtTotal=vsBean.getVasAmount();
						break;
			}
			
			deBean.setAddOnExpense_VAS(vascode);
			deBean.setExpenseAmount_VAS(vasAmt);
			deBean.setVasName(vasname);
			deBean.setExpenseAmountTotal_VAS(vasAmtTotal);
		}
		
		else if(tabledata_VAS.size()>1)
		{
			System.out.println("VAS data size > 1 running...");
			
			for(VAS_Table_Bean vsBean:tabledata_VAS)
			{
				vasAmtTotal=vasAmtTotal+vsBean.getVasAmount();
				vasname=vasname+","+vsBean.getVasName();
				vascode=vascode+","+vsBean.getVasCode();
				vasAmt=vasAmt+","+vsBean.getVasAmount();
			}
			
			deBean.setAddOnExpense_VAS(vascode.substring(5));
			deBean.setExpenseAmount_VAS(vasAmt.substring(5));
			deBean.setVasName(vasname.substring(5));
			deBean.setExpenseAmountTotal_VAS(vasAmtTotal);
		}
		//System.out.println("VAS table size: "+tabledata_VAS.size());
		
		
		}
		else
		{
			deBean.setAddOnExpense_VAS("");
			deBean.setExpenseAmount_VAS("");
			deBean.setVasName("");
			deBean.setExpenseAmountTotal_VAS(0);
		}
			
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;
		String query=null;
		
		try 
		{
			if(isAwbNoExist==false)
			{
				query = "insert into daily_booking_vas(master_awb_no,vas_cd,vas_amount,vas_total_amount,delivery_on_invoice_amount,"
							+ "hold_at_office_amount,adv_fee_amount,hfd_amount,green_tax_amount,reverse_pickup_amount,tdd_amount,"
							+ "critical_amount,addrs_crr_amount,vas_name,topay_amount,cod_amount,oss_amount,oda_amount,opa_amount,hfd_floor_number,create_date,lastmodifieddate) "
							+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";
			
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, deBean.getAwbNo());
				preparedStmt.setString(2, deBean.getAddOnExpense_VAS());
				preparedStmt.setString(3, deBean.getExpenseAmount_VAS());
				preparedStmt.setDouble(4, deBean.getExpenseAmountTotal_VAS());
				preparedStmt.setDouble(5, deBean.getDoi_VAS());
				preparedStmt.setDouble(6, deBean.getHoldAtOffice_VAS());
				preparedStmt.setDouble(7, deBean.getOct_Fee_VAS());
				preparedStmt.setDouble(8, deBean.getHfd_Charges_VAS());
				preparedStmt.setDouble(9, deBean.getGreenTax_VAS());
				preparedStmt.setDouble(10, deBean.getReversePickup_VAS());
				preparedStmt.setDouble(11, deBean.getTdd_VAS());
				preparedStmt.setDouble(12, deBean.getCriticalService_VAS());
				preparedStmt.setDouble(13, deBean.getAddressCorrection_VAS());
				preparedStmt.setString(14, deBean.getVasName());
				preparedStmt.setDouble(15, deBean.getToPay_VAS());
				preparedStmt.setDouble(16, deBean.getCod_amount_vas());
				preparedStmt.setDouble(17, deBean.getOss_VAS());
				preparedStmt.setDouble(18, deBean.getOda_VAS());
				preparedStmt.setDouble(19, deBean.getOpa_VAS());
				preparedStmt.setInt(20, deBean.getHfd_floor_number());

				preparedStmt.executeUpdate();
			}
			else if(isAwbNoExist==true)
			{
				
				query = "update daily_booking_vas set vas_cd=?,vas_amount=?,vas_total_amount=?,delivery_on_invoice_amount=?,"
						+ "hold_at_office_amount=?,adv_fee_amount=?,hfd_amount=?,green_tax_amount=?,reverse_pickup_amount=?,tdd_amount=?,"
						+ "critical_amount=?,addrs_crr_amount=?,vas_name=?,topay_amount=?,cod_amount=?,oss_amount=?,oda_amount=?,"
						+ "opa_amount=?,hfd_floor_number=?,lastmodifieddate=CURRENT_TIMESTAMP where master_awb_no=?";
						
		
			preparedStmt = con.prepareStatement(query);
			
			
			preparedStmt.setString(1, deBean.getAddOnExpense_VAS());
			preparedStmt.setString(2, deBean.getExpenseAmount_VAS());
			preparedStmt.setDouble(3, deBean.getExpenseAmountTotal_VAS());
			preparedStmt.setDouble(4, deBean.getDoi_VAS());
			preparedStmt.setDouble(5, deBean.getHoldAtOffice_VAS());
			preparedStmt.setDouble(6, deBean.getOct_Fee_VAS());
			preparedStmt.setDouble(7, deBean.getHfd_Charges_VAS());
			preparedStmt.setDouble(8, deBean.getGreenTax_VAS());
			preparedStmt.setDouble(9, deBean.getReversePickup_VAS());
			preparedStmt.setDouble(10, deBean.getTdd_VAS());
			preparedStmt.setDouble(11, deBean.getCriticalService_VAS());
			preparedStmt.setDouble(12, deBean.getAddressCorrection_VAS());
			preparedStmt.setString(13, deBean.getVasName());
			preparedStmt.setDouble(14, deBean.getToPay_VAS());
			preparedStmt.setDouble(15, deBean.getCod_amount_vas());
			preparedStmt.setDouble(16, deBean.getOss_VAS());
			preparedStmt.setDouble(17, deBean.getOda_VAS());
			preparedStmt.setDouble(18, deBean.getOpa_VAS());
			preparedStmt.setInt(19, deBean.getHfd_floor_number());
			preparedStmt.setString(20, deBean.getAwbNo());

			preparedStmt.executeUpdate();
			}

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

	
// ****************************************************************	

	public void getVAS_DataFromDB_viaAWBNo() throws SQLException
	{
		double resultVAS=0;
		
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Statement st = null;
		ResultSet rs = null;
		
		PreparedStatement preparedStmt = null;
		DataEntryBean deBean = new DataEntryBean();
		try 
		{
			String sql ="select master_awb_no,vas_cd,vas_amount,vas_total_amount,delivery_on_invoice_amount,cod_amount,"
					+ "hold_at_office_amount,adv_fee_amount,hfd_amount,oss_amount,topay_amount,green_tax_amount,reverse_pickup_amount,tdd_amount,"
					+ "critical_amount,addrs_crr_amount,vas_name,oda_amount,opa_amount,hfd_floor_number from daily_booking_vas where master_awb_no=?";
				
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1, OutwardPageController.awbNoForDataEntryPopup);
			
			rs = preparedStmt.executeQuery();
			
			if (!rs.next()) 
			{
				tabledata_VAS.clear();
			} 
			else 
			{
				do 
				{
					deBean.setAddOnExpense_VAS(rs.getString("vas_cd"));
					deBean.setExpenseAmount_VAS(rs.getString("vas_amount"));
					deBean.setVasName(rs.getString("vas_name"));
					deBean.setExpenseAmountTotal_VAS(rs.getDouble("vas_total_amount"));
					deBean.setDoi_VAS(rs.getDouble("delivery_on_invoice_amount"));
					deBean.setHoldAtOffice_VAS(rs.getDouble("hold_at_office_amount"));
					deBean.setOct_Fee_VAS(rs.getDouble("adv_fee_amount"));
					deBean.setHfd_Charges_VAS(rs.getDouble("hfd_amount"));
					deBean.setGreenTax_VAS(rs.getDouble("green_tax_amount"));
					deBean.setOss_VAS(rs.getDouble("oss_amount"));
					deBean.setToPay_VAS(rs.getDouble("topay_amount"));
					deBean.setReversePickup_VAS(rs.getDouble("reverse_pickup_amount"));
					deBean.setTdd_VAS(rs.getDouble("tdd_amount"));
					deBean.setCriticalService_VAS(rs.getDouble("critical_amount"));
					deBean.setAddressCorrection_VAS(rs.getDouble("addrs_crr_amount"));
					deBean.setOda_VAS(rs.getDouble("oda_amount"));
					deBean.setOpa_VAS(rs.getDouble("opa_amount"));
					deBean.setCod_amount_vas(rs.getDouble("cod_amount"));
					deBean.setHfd_floor_number(rs.getInt("hfd_floor_number"));
		
					/*tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(), diBean.getPcs(), 
							diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));*/
					
				} 
				while (rs.next());
				
				
				if(deBean.getDoi_VAS()>0)
				{
					checkBoxDOI.setSelected(true);
					resultVAS=resultVAS+deBean.getDoi_VAS();
				}
				else
				{
					resultVAS=resultVAS+deBean.getDoi_VAS();
					checkBoxDOI.setSelected(false);
				}
				
				if(deBean.getHoldAtOffice_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getHoldAtOffice_VAS();
					checkBoxHoldAtOffice.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getHoldAtOffice_VAS();
					checkBoxHoldAtOffice.setSelected(false);
				}
				
				if(deBean.getOct_Fee_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getOct_Fee_VAS();
					checkBoxOCT_Fee.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getOct_Fee_VAS();
					checkBoxOCT_Fee.setSelected(false);
				}
					
				if(deBean.getOss_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getOss_VAS();
					checkBoxOSS.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getOss_VAS();
					checkBoxOSS.setSelected(false);
				}
				
				if(deBean.getHfd_Charges_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getHfd_Charges_VAS();
					checkBoxHFDCharges.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getHfd_Charges_VAS();
					checkBoxHFDCharges.setSelected(false);
				}
				
				if(deBean.getGreenTax_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getGreenTax_VAS();
					checkBoxGreenTax.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getGreenTax_VAS();
					checkBoxGreenTax.setSelected(false);
				}
				
				if(deBean.getReversePickup_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getReversePickup_VAS();
					checkBoxReversePickup.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getReversePickup_VAS();
					checkBoxReversePickup.setSelected(false);
				}
				
				if(deBean.getTdd_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getTdd_VAS();
					rdBtnTDD.setSelected(true);
				}
				else if(deBean.getCriticalService_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getCriticalService_VAS();
					rdBtnCrit.setSelected(true);
				}

				if(deBean.getCod_amount_vas()>0)
				{
					resultVAS=resultVAS+deBean.getCod_amount_vas();
					checkBoxCOD.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getCod_amount_vas();
					checkBoxCOD.setSelected(false);
				}
				
				if(deBean.getToPay_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getToPay_VAS();
					checkBoxToPay.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getToPay_VAS();
					checkBoxToPay.setSelected(false);
				}
					
				if(deBean.getAddressCorrection_VAS()>0)
				{
					resultVAS=resultVAS+deBean.getAddressCorrection_VAS();
					checkBoxAddressCorrection.setSelected(true);
				}
				else
				{
					resultVAS=resultVAS+deBean.getAddressCorrection_VAS();
					checkBoxAddressCorrection.setSelected(false);
				}
				
				
				if(checkBoxHFDCharges.isSelected()==true)
				{
					txtHFD_Floors.setDisable(false);
					txtHFD_Floors.setText(String.valueOf(deBean.getHfd_floor_number()));
				}
				else
				{
					txtHFD_Floors.setDisable(true);
					txtHFD_Floors.setText("0");
				}
					
				resultVAS=resultVAS+deBean.getOda_VAS();
				resultVAS=resultVAS+deBean.getOpa_VAS();
				
				if(!deBean.getAddOnExpense_VAS().equals(""))
				{
				String[] vasCode=deBean.getAddOnExpense_VAS().replaceAll("\\s+","").split("\\,");
				String[] vasAmt=deBean.getExpenseAmount_VAS().replaceAll("\\s+","").split("\\,");
				String[] vasName=deBean.getVasName().replaceAll("\\s+","").split("\\,");
				
				int count=0;
				int slno=1;
				

				for(String code: vasCode)
				{
					
					tabledata_VAS.add(new VAS_Table_Bean(slno, code, vasName[count], Double.valueOf(vasAmt[count])));
					count++;
					slno++;
				}
				
				tableVAS.setItems(tabledata_VAS);
				}
				
				System.out.println("Vas Result: "+resultVAS);
				totalVAS_amount=resultVAS;
				//tableDimension.setItems(tabledata_Dimension);
				
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ****************************************************************
	
	/*	public double showActualWeight() throws SQLException
		{
			
			//tableForwarder.refresh();
			
			ResultSet rs = null;
			Statement stmt = null;
			String sql = null;
			DBconnection dbcon = new DBconnection();
			Connection con = dbcon.ConnectDB();
				
			double actualWeight=0.0;
			
			try {

				stmt = con.createStatement();
				sql = "select sum(weight) as weight from dimensions where awbno='"+txtAwbNo.getText()+"'";
			
				System.out.println("SQL: " + sql);
				rs = stmt.executeQuery(sql);

				while (rs.next()) {
					actualWeight=rs.getDouble("weight");
					}
			}

			catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}

			finally {
				dbcon.disconnect(null, stmt, rs, con);
			}
			return actualWeight;
		}	
	*/
	
	
	
	
// ****************************************************************	

	public void reset()
	{
		txtAwbNo.clear();
		txtClient.clear();
		txtDestination.clear();
		txtService.clear();
		//txtOrigin.clear();
		txtNetwork.clear();
		txtBookingDate.clear();
		txtForwarderNo.clear();
		txtForwarderService.clear();
		txtInvoiceNo.clear();
		//combBox_Insurance.getSelectionModel().clearSelection();
		
		
		
		txt_Consignor_Name.clear();
		txt_Consignor_Address.clear();
		txt_Consignor_ContactPerson.clear();
		txt_Consignor_Email.clear();
		txt_Consignor_Phone.clear();
		txt_Consignor_City.clear();
		txt_Consignor_Zipcode.clear();
		combBox_Consignor_Country.getSelectionModel().clearSelection();
		combBox_Consignor_State.getSelectionModel().clearSelection();
		
		txt_Consignee_Name.clear();
		txt_Consignee_Address.clear();
		//txt_Consignee_ContactPerson.clear();
		txt_Consignee_Email.clear();
		txt_Consignee_Phone.clear();
		//txt_Consignee_City.clear();
		txt_Consignee_Zipcode.clear();
		txt_Consignee_Email.clear();
		//txt_Consignee_Remarks.clear();
		//combBox_Consignee_Country.getSelectionModel().clearSelection();
		//combBox_Consignee_State.getSelectionModel().clearSelection();
		
		///checkBoxDimension.setSelected(false);
		//checkBoxDimension.setDisable(true);
		combBox_Type.setValue("Dox");
				
		txtPcs.setText("0");
		txtActualWeight.setText("0");
		txtVolumeWeight.setText("0");
		txtBillWeight.setText("0");
		txtBasicAmount.setText("0");
		txtInsuranceRate.setText("0");
		txtInvoiceAmount.setText("0");
		txtInsuranceAmount.setText("0");
		txtDocketCharges.setText("0");
		
		txtFuelAmount.setText("0");
		txtBasicAmount.setText("0");
		txtTaxableValue.setText("0");
		txtTotalVASAmount.setText("0");
		txtGSTAmt.setText("0");
		txtTotal.setText("0");
		
		checkBoxDOI.setSelected(false);		
		checkBoxHoldAtOffice.setSelected(false);
		checkBoxOCT_Fee.setSelected(false);
		checkBoxOSS.setSelected(false);
		checkBoxGreenTax.setSelected(false);
		checkBoxReversePickup.setSelected(false);
		checkBoxCOD.setSelected(false);
		checkBoxFOD.setSelected(false);
		checkBoxAddressCorrection.setSelected(false);		
		checkBoxToPay.setSelected(false);
		checkBoxHFDCharges.setSelected(false);
		txtHFD_Floors.clear();
		txtHFD_Floors.setDisable(true);
		
		rdBtnTDD.setSelected(false);
		rdBtnCrit.setSelected(false);
		
		txtForwarderAccount.clear();
		
		tabledata_VAS.clear();
		tabledata_Dimension.clear();
		
		txtAddOnExpense.clear();
		txtExpenseAmount.clear();
		
		
	}
	
// ****************************************************************

	public void isFloorTextBoxDisable()
	{
		if(checkBoxHFDCharges.isSelected()==true)
		{
			txtHFD_Floors.setDisable(false);
			txtHFD_Floors.setText("3");
		}
		else
		{
			txtHFD_Floors.setDisable(true);
			txtHFD_Floors.clear();
		}
	}

// ****************************************************************	

	public void showSelectedVAS() throws IOException
	{
		Main m=new Main();
		m.showSelectedVASItems();
	}
	

// ****************************************************************
	
	public void loadDetails() throws SQLException
	{
		System.out.println("From Data entry >>>>>>>>>>>>>>>>>>>>>> "+OutwardPageController.awbNoForDataEntryPopup);
		DataEntryPopUpController de=new DataEntryPopUpController();
		//de.txtAwbNo.setText();
		//txtAwbNo.setText(OutwardPageController.awbNoForDataEntryPopup);
		getDetailsViaAwbNo();
	}
	
	
// ****************************************************************
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
	//	txtAwbNo.setText(OutwardPageController.awbNoForDataEntryPopup);
		
		System.out.println("AWB no from Outward >>>>>>>>>>> "+OutwardPageController.awbNoForDataEntryPopup);
		/*try {
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		txtBookingDate.setEditable(false);
		
		combBox_Type.setValue("Dox");
		combBox_Type.setItems(comboBoxTypeItems);
		combBox_Insurance.setItems(comboBoxInsuranceItems);
		//checkBoxDimension.setDisable(true);
		
		//txtService.setEditable(false);
		
		txtHFD_Floors.setDisable(true);
		
		//combBox_Consignee_Country.setItems(comboBoxConsigNEE_CountryItems);
		combBox_Consignor_Country.setItems(comboBoxConsigNOR_CountryItems);
		
		//imgSearchIcon.setImage(new Image("/icon/searchicon_blue.png"));
		//txtOrigin.setText(LoadOrigin.ORIGIN);
		
		LIST_SAVEDIMENSION_DATA.clear();
		Dimension__finalweight=0;
		Dimension__totalweight=0;
		Dimension_totalPcs=0;
		awbno=null;
		TotalPCS=0;
		volWeightFromDimension=0;
		
		txtPcs.setText("0");
		txtActualWeight.setText("0");
		txtVolumeWeight.setText("0");
		txtBillWeight.setText("0");
		txtBasicAmount.setText("0");
		txtInsuranceRate.setText("0");
		txtInvoiceAmount.setText("0");
		txtInsuranceAmount.setText("0");
		txtDocketCharges.setText("0");
		txtDimension_Length.setText("0");
		txtDimension_Height.setText("0");
		txtDimension_Width.setText("0");
		txtGSTAmt.setText("0");
		txtTotal.setText("0");
		txtTaxableValue.setText("0");
		txtFuelAmount.setText("0");
		txtTotalVASAmount.setText("0");
		
		
		txtTaxableValue.setEditable(false);
		txtGSTAmt.setEditable(false);
		txtTotal.setEditable(false);
		
		dim_tabColumn_Height.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("height"));
		dim_tabColumn_pcs.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,Integer>("pcs"));
		dim_tabColumn_Lenght.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("length"));
		dim_tabColumn_Weight.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("weight"));
		dim_tabColumn_Width.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("width"));
		
		
		vas_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,Integer>("slno"));
		vas_tabColumn_vasCode.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,String>("vasCode"));
		vas_tabColumn_vasName.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,String>("vasName"));
		vas_tabColumn_vasAmount.setCellValueFactory(new PropertyValueFactory<VAS_Table_Bean,Double>("vasAmount"));
		
		
		CLIENT=null;
		SERVICE=null;
		
	
		try 
		{
			loadCityFromPincode();
			loadState();
			loadOrigin_and_Destination();
			new LoadServiceGroups().loadServicesFromServiceType();
			loadForwarder();
			//loadDestination();
			loadAddOnExpense();
			//loadTable();
			//loadOriginViaBranch();
			
			getDetailsViaAwbNo();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

}
