package com.onesoft.courier.booking.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.controlsfx.control.textfield.TextFields;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.OutwardBean;
import com.onesoft.courier.booking.bean.OutwardForwardedTableBean;
import com.onesoft.courier.booking.bean.OutwardUnForwardedTableBean;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.bean.LoadNetworkBean;
import com.onesoft.courier.common.bean.LoadServiceWithNetworkBean;
import com.onesoft.courier.main.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class OutwardPageController implements Initializable {

	private List<String> list_Networks=new ArrayList<>();
	private Set<String> set_ForwarderService=new HashSet<>();
	
	private ObservableList<OutwardForwardedTableBean> tabledata_Forwarded=FXCollections.observableArrayList();
	private ObservableList<OutwardUnForwardedTableBean> tabledata_UnForwarded=FXCollections.observableArrayList();

	
	public static String awbNoForDataEntryPopup;

	@FXML
	private Hyperlink hyperlinkInward;
	
	@FXML
	private Hyperlink hyperlinkCloudInward;
	
	@FXML
	private Hyperlink hyperlinkDataEntry;
	
// ******************************************	
	
	@FXML
	private Pagination pagination_Forwarded;
	
	@FXML
	private Pagination pagination_UnForwarded;

	
// ******************************************	
	
	@FXML
	private TextField txtNetwork;
	
	@FXML
	private TextField txtAwbNo;
	
	@FXML
	private TextField txtForwarderService;
	
	@FXML
	private TextField txtBookingService;
	
	@FXML
	private TextField txtForwardingNo;
	
// ******************************************

	@FXML
	private Button btnSubmit;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Button btnExportToExcel_Forwarded;
	
	@FXML
	private Button btnExportToExcel_UnForwarded;
	
// ******************************************
	
	@FXML
	private ImageView imgViewSearchIcon_Forwarded;
	
	@FXML
	private ImageView imgViewSearchIcon_UnForwarded;
	
	
// ***************************************

	@FXML
	private TableView<OutwardForwardedTableBean> tableForwarded;

	@FXML
	private TableColumn<OutwardForwardedTableBean, Integer> forwrd_tabColumn_slno;

	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_MasterAwb;

	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Awb;

	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_ForwardingNo;
	
	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Network;
	
	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Client;

	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Mode_service;
	
	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Forwarder_service;

	@FXML
	private TableColumn<OutwardForwardedTableBean, Integer> forwrd_tabColumn_PCS;

	@FXML
	private TableColumn<OutwardForwardedTableBean, Double> forwrd_tabColumn_Weight;
	
	@FXML
	private TableColumn<OutwardForwardedTableBean, String> forwrd_tabColumn_Destination;

	@FXML
	private TableColumn<OutwardForwardedTableBean, Integer> forwrd_tabColumn_Zipcode;


// *******************************************************************************************

	@FXML
	private TableView<OutwardUnForwardedTableBean> tableUnForwarded;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, Integer> unForwrd_tabColumn_slno;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, String> unForwrd_tabColumn_MasterAwb;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, String> unForwrd_tabColumn_Awb;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, String> unForwrd_tabColumn_Client;
	
	@FXML
	private TableColumn<OutwardUnForwardedTableBean, String> unForwrd_tabColumn_Mode_service;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, Integer> unForwrd_tabColumn_PCS;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, Double> unForwrd_tabColumn_Weight;
	
	@FXML
	private TableColumn<OutwardUnForwardedTableBean, String> unForwrd_tabColumn_Destination;

	@FXML
	private TableColumn<OutwardUnForwardedTableBean, Integer> unForwrd_tabColumn_Zipcode;
	
	Main m=new Main();
	
// ******************************************************************************

	public void showInwardPage() throws IOException
	{
		m.showInward();
	}
	
// ******************************************************************************

	public void showCloudInward() throws IOException
	{
		m.showCloudInward();		
	}
	
// ******************************************************************************
	
	public void showDataEntry() throws IOException
	{
		m.showDataEntry();
	}
	
// ******************************************************************************
	
	public void loadNetworks() throws SQLException
	{
		txtAwbNo.clear();
		new LoadNetworks().loadAllNetworksWithName();
		
		for(LoadNetworkBean bean:LoadNetworks.SET_LOAD_NETWORK_WITH_NAME)
		{
			list_Networks.add(bean.getNetworkName()+" | "+bean.getNetworkCode());
		}
		
		TextFields.bindAutoCompletion(txtNetwork, list_Networks);
	}	
	
	
// ******************************************************************************

	public void loadForwarderServicesWithName() throws SQLException 
	{
		new LoadServiceGroups().loadServicesWithName();

		for (LoadServiceWithNetworkBean bean : LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME) 
		{
			set_ForwarderService.add(bean.getServiceName()+" | "+bean.getServiceCode());

		}
		TextFields.bindAutoCompletion(txtForwarderService, set_ForwarderService);

	}		

// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException {
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("txtNetwork")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtForwarderService.requestFocus();
			}
		}
		
		else if (n.getId().equals("txtForwarderService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAwbNo.requestFocus();
			}
		}
		
		/*else if (n.getId().equals("txtBookingService")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtAwbNo.requestFocus();
			}
		}*/
		
		else if (n.getId().equals("txtAwbNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				getServiceViaAwbNo();
				txtForwardingNo.requestFocus();
			}
		}

		else if (n.getId().equals("txtForwardingNo")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				btnSubmit.requestFocus();
			}
		}

		else if (n.getId().equals("btnSubmit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				setValidation();
			}
		}


	}
	
	
// *************** Method to set Validation *******************	
	
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
			
			/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			String email=txtemailid.getText();
		    Pattern pattern = Pattern.compile(regex);
		    Matcher matcher = pattern.matcher((CharSequence) email);
		    boolean checkmail=matcher.matches();*/
			
		
		if(txtNetwork.getText()==null || txtNetwork.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Network");
			alert.showAndWait();
			txtNetwork.requestFocus();
		}
		else if(txtAwbNo.getText()==null || txtAwbNo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Awb No.");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}
		
		else if(txtForwarderService.getText()==null || txtAwbNo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Forwarder Service");
			alert.showAndWait();
			txtForwarderService.requestFocus();
		}
		
		else if(txtForwardingNo.getText()==null || txtForwardingNo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("You did not enter a Forwarding No.");
			alert.showAndWait();
			txtForwardingNo.requestFocus();
		}
		
		else
		{
			//testSaveOutward();
			checkForwarderNoExistance();
			//saveOutwardDetails();
		}
	}	
	
// *************************************************************************

	public void testSaveOutward()
	{
		System.out.println("Network: "+txtNetwork.getText());
		System.out.println("Forwarder Srvc: "+txtForwarderService);
		System.out.println("Booking Srvc: "+txtBookingService.getText());
		System.out.println("Awb No: "+txtAwbNo.getText());
		System.out.println("Forwarding No: "+txtForwardingNo.getText());
		
		if(!txtAwbNo.getText().matches("[^A-Za-z]+"))
		{
			System.out.println("Alphnumeric :"+txtAwbNo.getText()+" | Replaced value: "+txtAwbNo.getText().replaceAll("\\D+",""));
			txtAwbNo.setText(txtAwbNo.getText().replaceAll("[^A-Za-z]+", "")+String.valueOf(Integer.valueOf(txtAwbNo.getText().replaceAll("\\D+",""))+1));
			txtAwbNo.requestFocus();
			
		}
		else
		{
			
			txtAwbNo.setText(String.valueOf(Integer.valueOf(txtAwbNo.getText())+1));
			System.out.println("Only digit :"+txtAwbNo.getText());
			txtAwbNo.requestFocus();
		}
	}
	
// *************************************************************************	
	
	public void getServiceViaAwbNo() throws SQLException
	{
		OutwardBean outBean =new OutwardBean();
		outBean.setAwbNo(txtAwbNo.getText());
		
		ResultSet rs = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		try 
		{
			sql = "select dailybookingtransaction2service from dailybookingtransaction where air_way_bill_number=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,outBean.getAwbNo());
			
			System.out.println("PS Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			while (rs.next()) 
			{
				for(LoadServiceWithNetworkBean srvcBean:LoadServiceGroups.LIST_LOAD_SERVICES_WITH_NAME)
				{
					if(rs.getString("dailybookingtransaction2service").equals(srvcBean.getServiceCode()))
					{
						txtBookingService.setText(srvcBean.getServiceName()+" | "+srvcBean.getServiceCode());
						break;
					}
				}
			}
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
	
// *************************************************************************

	public void saveOutwardDetails() throws SQLException
	{
		String[] network=null;
		String[] forwarderService=null;
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Inscan Status");
		alert.setHeaderText(null);	
		
		OutwardBean outBean=new OutwardBean();
		
		network=txtNetwork.getText().replaceAll("\\s+","").split("\\|");
		
		if(txtForwarderService.getText().contains("|"))
		{
			forwarderService=txtForwarderService.getText().replaceAll("\\s+","").split("\\|");
			outBean.setForwarderService(forwarderService[1]);
		}
		else
		{
			outBean.setForwarderService(txtForwarderService.getText());
		}
		
		outBean.setAwbNo(txtAwbNo.getText());
		outBean.setNetworkCode(network[1].trim());
		//outBean.setForwarderService(txtForwarderService.getText());
		outBean.setForwardingNo(txtForwardingNo.getText());
		outBean.setTravelStatus("outward");
		
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dailybookingtransaction set travel_status=?,forwarder_number=?, dailybookingTransaction2network=?,dailybookingtransaction2forwarder_service=? where air_way_bill_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, outBean.getTravelStatus());
				preparedStmt.setString(2, outBean.getForwardingNo());
				preparedStmt.setString(3, outBean.getNetworkCode());
				preparedStmt.setString(4, outBean.getForwarderService());
				preparedStmt.setString(5, outBean.getAwbNo());
				
				int status=preparedStmt.executeUpdate();
				
				if(status==1)
				{
					alert.setContentText("Outward successfully complete...");
					alert.showAndWait();
					txtAwbNo.requestFocus();
					txtForwardingNo.clear();
				}
				else
				{
					alert.setContentText("Outward is not completed \nPlease try again...!");
					alert.showAndWait();
					txtAwbNo.requestFocus();
					
				}
				
				//reset();
				
				if(!txtAwbNo.getText().matches("[^A-Za-z]+"))
				{
					txtAwbNo.setText(txtAwbNo.getText().replaceAll("[^A-Za-z]+", "")+String.valueOf(Integer.valueOf(txtAwbNo.getText().replaceAll("\\D+",""))+1));
				}
				else
				{
					txtAwbNo.setText(String.valueOf(Integer.valueOf(txtAwbNo.getText())+1));
				}
				
				
				loadForwardedTable();
				loadUnForwardedTable();
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
	}
	
// *************************************************************************

	public void checkForwarderNoExistance() throws SQLException
	{
		boolean isForwarderNoExist=false;
		
		ResultSet rs = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt=null;

		try 
		{
			sql = "select air_way_bill_number, forwarder_number from dailybookingtransaction where forwarder_number=?";

			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setString(1,txtForwardingNo.getText());
			
			System.out.println("Forwarder Sql: "+ preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if(!rs.next())
			{
				saveOutwardDetails();
				//isForwarderNoExist=false;
			}
			else
			{	
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Fowarder No alert..");
				alert.setHeaderText(null);	
				alert.setContentText("Forwarder No is already exist \nPlease try again...!");
				alert.showAndWait();
				txtForwardingNo.requestFocus();
				//isForwarderNoExist=true;
			}
		
		}
		
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally 
		{
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	
		
	}
	
	
// *************************************************************************

	public void loadForwardedTable() throws SQLException 
	{

		tabledata_Forwarded.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		OutwardBean outBean = new OutwardBean();

		int slno = 1;
		try {

			stmt = con.createStatement();
		/*	sql = "select dailybookingtransactionid, master_awbno,air_way_bill_number,forwarder_number,dailybookingtransaction2network,dailybookingtransaction2client,dailybookingtransaction2service,"
					+ "dailybookingtransaction2forwarder_service,dailybookingtransaction2city,zipcode,packets,billing_weight,dimension from dailybookingtransaction "
					+ "where travel_status='outward' and dailybookingtransaction2service IS NOT NULL and dailybookingtransaction2network IS NOT NUll "
					+ "and mps IS NOT NULL order by dailybookingtransactionid DESC";*/
			
			sql = "select dailybookingtransactionid, master_awbno,air_way_bill_number,forwarder_number,dailybookingtransaction2network,dailybookingtransaction2client,dailybookingtransaction2service,"
					+ "dailybookingtransaction2forwarder_service,dailybookingtransaction2city,zipcode,packets,billing_weight,dimension from dailybookingtransaction "
					+ "where travel_status='outward' and dailybookingtransaction2service IS NOT NULL and dailybookingtransaction2network IS NOT NUll and mps_type='T' and booking_date > current_date - interval '3 day' order by dailybookingtransactionid DESC";
		
			System.out.println("Forwarded SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				outBean.setSlno(slno);
				outBean.setMasterAwbNo(rs.getString("master_awbno"));
				outBean.setAwbNo(rs.getString("air_way_bill_number"));
				outBean.setForwardingNo(rs.getString("forwarder_number"));
				outBean.setNetworkCode(rs.getString("dailybookingtransaction2network"));
				outBean.setClientCode(rs.getString("dailybookingtransaction2client"));
				outBean.setForwarderService(rs.getString("dailybookingtransaction2forwarder_service"));
				outBean.setDestination(rs.getString("dailybookingtransaction2city"));
				outBean.setZipcode(rs.getInt("zipcode"));
				outBean.setPcs(rs.getInt("packets"));
				outBean.setWeight(rs.getDouble("billing_weight"));
				outBean.setMode_serivce(rs.getString("dailybookingtransaction2service"));

				tabledata_Forwarded.add(new OutwardForwardedTableBean(outBean.getSlno(), outBean.getMasterAwbNo(),
						outBean.getAwbNo(), outBean.getForwardingNo(), outBean.getNetworkCode(),
						outBean.getClientCode(), outBean.getMode_serivce(),outBean.getForwarderService(), outBean.getPcs(), outBean.getWeight(),
						outBean.getDestination(), outBean.getZipcode()));

				slno++;
			}

			tableForwarded.setItems(tabledata_Forwarded);
			
			clickForwarderRow();

			pagination_Forwarded.setPageCount((tabledata_Forwarded.size() / rowsPerPage() + 1));
			pagination_Forwarded.setCurrentPageIndex(0);
			pagination_Forwarded.setPageFactory((Integer pageIndex) -> createPage_Forwarded(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
// *************************************************************************

	public void loadUnForwardedTable() throws SQLException {
		
		tabledata_UnForwarded.clear();

		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		OutwardBean outBean = new OutwardBean();

		int slno = 1;
		try {

			
			stmt = con.createStatement();
			sql = "select dailybookingtransactionid, master_awbno,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2service,"
					+ "dailybookingtransaction2city,zipcode,packets,billing_weight,dimension from dailybookingtransaction "
					+ "where travel_status='inward' and dailybookingtransaction2service IS NOT NULL and dailybookingtransaction2network IS NUll "
					+ "and mps IS NOT NULL order by dailybookingtransactionid DESC";
			
		
			/*
			 * sql=
			 * "select dailybookingtransactionid, master_awbno,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2service,"
			 * +
			 * "dailybookingtransaction2city,zipcode,packets,billing_weight,mps,mps_type,dimension from dailybookingtransaction "
			 * +
			 * "where dailybookingtransaction2service IS NOT NULL and sub_awbno_status='Single' OR sub_awbno_status='Multiple' order by dailybookingtransactionid DESC"
			 * ;
			 */
			System.out.println("Table SQL: " + sql);
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				outBean.setSlno(slno);
				outBean.setMasterAwbNo(rs.getString("master_awbno"));
				outBean.setAwbNo(rs.getString("air_way_bill_number"));
				outBean.setClientCode(rs.getString("dailybookingtransaction2client"));
				outBean.setDestination(rs.getString("dailybookingtransaction2city"));
				outBean.setZipcode(rs.getInt("zipcode"));
				outBean.setPcs(rs.getInt("packets"));
				outBean.setWeight(rs.getDouble("billing_weight"));
				outBean.setMode_serivce(rs.getString("dailybookingtransaction2service"));
				
		
				tabledata_UnForwarded.add(new OutwardUnForwardedTableBean(outBean.getSlno(),outBean.getMasterAwbNo(), outBean.getAwbNo(), outBean.getClientCode(),outBean.getMode_serivce(),
						outBean.getPcs(), outBean.getWeight(), outBean.getDestination(), outBean.getZipcode()));

				slno++;
			}

			tableUnForwarded.setItems(tabledata_UnForwarded);
			
			pagination_UnForwarded.setPageCount((tabledata_UnForwarded.size() / rowsPerPage() + 1));
			pagination_UnForwarded.setCurrentPageIndex(0);
			pagination_UnForwarded.setPageFactory((Integer pageIndex) -> createPage_UnForwarded(pageIndex));

		}

		catch (Exception e) {
			System.out.println(e);
		}

		finally {
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	
	

// ============ Code for paginatation =====================================================

	public int itemsPerPage() {
		return 1;
	}

	public int rowsPerPage() {
		return 10;
	}

	public GridPane createPage_UnForwarded(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_UnForwarded.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_UnForwarded.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableUnForwarded.setItems(FXCollections.observableArrayList(tabledata_UnForwarded
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableUnForwarded.setItems(FXCollections.observableArrayList(tabledata_UnForwarded
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}

	public GridPane createPage_Forwarded(int pageIndex) {
		int lastIndex = 0;

		GridPane pane = new GridPane();
		int displace = tabledata_Forwarded.size() % rowsPerPage();

		if (displace >= 0) {
			lastIndex = tabledata_Forwarded.size() / rowsPerPage();
		}

		int page = pageIndex * itemsPerPage();
		for (int i = page; i < page + itemsPerPage(); i++) {
			if (lastIndex == pageIndex) {
				tableForwarded.setItems(FXCollections.observableArrayList(
						tabledata_Forwarded.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
			} else {
				tableForwarded.setItems(FXCollections.observableArrayList(tabledata_Forwarded
						.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
			}
		}
		return pane;

	}
	

// ========================================================================================

	@FXML
	public void exportToExcel_Forwarded() throws SQLException, IOException {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Outward Forwarded");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Master Awb No");
		rowhead.createCell(2).setCellValue("Awb No");
		rowhead.createCell(3).setCellValue("Forwarding No");
		rowhead.createCell(4).setCellValue("Network");
		rowhead.createCell(5).setCellValue("Client");
		rowhead.createCell(6).setCellValue("Service");
		rowhead.createCell(7).setCellValue("PCS");
		rowhead.createCell(8).setCellValue("Weight");
		rowhead.createCell(9).setCellValue("Destination");
		rowhead.createCell(10).setCellValue("Zipcode");

		

		int i = 2;

		HSSFRow row;

		for (OutwardForwardedTableBean forwardedBean : tabledata_Forwarded) {
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(forwardedBean.getSlno());
			row.createCell(1).setCellValue(forwardedBean.getMasterAwbNo());
			row.createCell(2).setCellValue(forwardedBean.getAwbNo());
			row.createCell(3).setCellValue(forwardedBean.getForwardingNo());
			row.createCell(4).setCellValue(forwardedBean.getNetworkCode());
			row.createCell(5).setCellValue(forwardedBean.getClientCode());
			row.createCell(6).setCellValue(forwardedBean.getMode_serivce());
			row.createCell(7).setCellValue(forwardedBean.getPcs());
			row.createCell(8).setCellValue(forwardedBean.getWeight());
			row.createCell(9).setCellValue(forwardedBean.getDestination());
			row.createCell(10).setCellValue(forwardedBean.getZipcode());
	
			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		// System.out.println("file chooser: "+file.getAbsoluteFile());

		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}


// ========================================================================================

	@FXML
	public void exportToExcel_UnForwarded() throws SQLException, IOException {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Outward Un-Forwarded");
		HSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Master Awb No");
		rowhead.createCell(2).setCellValue("Awb No");
		rowhead.createCell(3).setCellValue("Client");
		rowhead.createCell(4).setCellValue("Service");
		rowhead.createCell(5).setCellValue("PCS");
		rowhead.createCell(6).setCellValue("Weight");
		rowhead.createCell(7).setCellValue("Destination");
		rowhead.createCell(8).setCellValue("Zipcode");

		int i = 2;

		HSSFRow row;

		for (OutwardUnForwardedTableBean unForwardedBean : tabledata_UnForwarded) {
			row = sheet.createRow((short) i);

			row.createCell(0).setCellValue(unForwardedBean.getSlno());
			row.createCell(1).setCellValue(unForwardedBean.getMasterAwbNo());
			row.createCell(2).setCellValue(unForwardedBean.getAwbNo());
			row.createCell(3).setCellValue(unForwardedBean.getClientCode());
			row.createCell(4).setCellValue(unForwardedBean.getMode_serivce());
			row.createCell(5).setCellValue(unForwardedBean.getPcs());
			row.createCell(6).setCellValue(unForwardedBean.getWeight());
			row.createCell(7).setCellValue(unForwardedBean.getDestination());
			row.createCell(8).setCellValue(unForwardedBean.getZipcode());

			i++;
		}

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files", "*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());

		workbook.write(fileOut);
		fileOut.close();

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File " + file.getName() + " successfully saved");
		alert.showAndWait();

	}	
	
	
// *************************************************************************	
	
	public void clickForwarderRow() throws Exception
	{
		
		System.out.println("Click sent row method is runnuig.... for SENTMAIL !! ");
		//System.out.println("========Loop values==C===");
		tableForwarded.setRowFactory( tv -> {
			TableRow<OutwardForwardedTableBean> row = new TableRow<>();
			//System.out.println("=====print repeat Data==   =");
			row.setOnMouseClicked(event -> {
			    	
				OutwardForwardedTableBean rowData = row.getItem();
				

				//String name1=rowData.getName();
				//String subject1=rowData.getSubject();
				
				
				try {
				
					if (event.getClickCount() == 1 && (! row.isEmpty()))
					{
						
					}
						
					if (event.getClickCount() == 2 && (! row.isEmpty()) )
					{
						//awbNoForDataEntryPopup=rowData.getAwbNo();
						//System.out.println("AWB No: "+awbNoForDataEntryPopup);
						
						Main m=new Main();
						m.showDataEntryPopupPage(rowData.getAwbNo());
						
						
						
						
					}
						
					} 
				catch (Exception e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			    });
			    return row ;
			});
			
			
			System.out.println("===Repeat=======From SENTMAIL================");
		}
	
	
// *************************************************************************

	public void reset()
	{
		txtAwbNo.clear();
		txtForwardingNo.clear();
		txtNetwork.clear();
		//txtForwarderService.clear();
	}
	
// *************************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		txtBookingService.setEditable(false);
		
		imgViewSearchIcon_Forwarded.setImage(new Image("/icon/searchicon_blue.png"));
		imgViewSearchIcon_UnForwarded.setImage(new Image("/icon/searchicon_blue.png"));
		
		
		forwrd_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,Integer>("slno"));
		forwrd_tabColumn_MasterAwb.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("masterAwbNo"));
		forwrd_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("awbNo"));
		forwrd_tabColumn_ForwardingNo.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("forwardingNo"));
		forwrd_tabColumn_Network.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("networkCode"));
		forwrd_tabColumn_Client.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("clientCode"));
		forwrd_tabColumn_Mode_service.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("mode_serivce"));
		forwrd_tabColumn_Forwarder_service.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("forwarderService"));
		forwrd_tabColumn_PCS.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,Integer>("pcs"));
		forwrd_tabColumn_Weight.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,Double>("weight"));
		forwrd_tabColumn_Destination.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,String>("destination"));
		forwrd_tabColumn_Zipcode.setCellValueFactory(new PropertyValueFactory<OutwardForwardedTableBean,Integer>("zipcode"));	
	
				
		unForwrd_tabColumn_slno.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,Integer>("slno"));
		unForwrd_tabColumn_MasterAwb.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,String>("masterAwbNo"));
		unForwrd_tabColumn_Awb.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,String>("awbNo"));
		unForwrd_tabColumn_Client.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,String>("clientCode"));
		unForwrd_tabColumn_Mode_service.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,String>("mode_serivce"));
		unForwrd_tabColumn_PCS.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,Integer>("Pcs"));
		unForwrd_tabColumn_Weight.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,Double>("weight"));
		unForwrd_tabColumn_Destination.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,String>("destination"));
		unForwrd_tabColumn_Zipcode.setCellValueFactory(new PropertyValueFactory<OutwardUnForwardedTableBean,Integer>("zipcode"));
		
		
		
		
		try {
			loadNetworks();
			loadUnForwardedTable();
			loadForwardedTable();
			loadForwarderServicesWithName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
