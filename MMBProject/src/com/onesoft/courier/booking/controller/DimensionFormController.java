package com.onesoft.courier.booking.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.booking.bean.DimensionBean;
import com.onesoft.courier.booking.bean.DimensionTableBean;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.WindowEvent;

public class DimensionFormController implements Initializable{
	
	/*double totalweight=0.0;
	double finalweight=0.0;
	int totalPcs=0;*/
	

	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	boolean checkAwbNoForDimension=false;
	
	private ObservableList<DimensionTableBean> tabledata_Dimension=FXCollections.observableArrayList();
	
	@FXML
	private TextField txtDimensionPCS;
	
	@FXML
	private TextField txtLength; 
	
	@FXML
	private TextField txtWidth;
	
	@FXML
	private TextField txtHeight;
	
	@FXML
	private TextField txtTest;
	
	@FXML
	private Button btnSubmit;
	
	
	
	
	@FXML
	private TableView<DimensionTableBean> tableDimension;
	
	@FXML
	private TableColumn<DimensionTableBean, String> tabColumn_AwbNo;
	
	@FXML
	private TableColumn<DimensionTableBean, Integer> tabColumn_pcs;
	
	@FXML
	private TableColumn<DimensionTableBean, String> tabColumn_Lenght;
	
	@FXML
	private TableColumn<DimensionTableBean, String> tabColumn_Weight;
	
	@FXML
	private TableColumn<DimensionTableBean, String> tabColumn_Width;

	@FXML
	private TableColumn<DimensionTableBean, String> tabColumn_Height;
	

// *************** Method the move Cursor using Entry Key *******************

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws Exception {
		Node n = (Node) e.getSource();

		
		if (n.getId().equals("txtDimensionPCS")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtLength.requestFocus();
				
			}
		}

		else if (n.getId().equals("txtLength")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtHeight.requestFocus();
			}
		}

		else if (n.getId().equals("txtHeight")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				txtWidth.requestFocus();
			}
		}

		else if (n.getId().equals("txtWidth")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				/*totalPcs=totalPcs+Integer.valueOf(txtDimensionPCS.getText());
				System.out.println("Total PCS From dimension: "+totalPcs );*/
				btnSubmit.requestFocus();
			}
		}

		else if (n.getId().equals("btnSubmit")) {
			if (e.getCode().equals(KeyCode.ENTER)) {
				
				int count=DataEntryController.Dimension_totalPcs;
				
				DataEntryController.Dimension_totalPcs=DataEntryController.Dimension_totalPcs+Integer.valueOf(txtDimensionPCS.getText());
				
				
				if(DataEntryController.Dimension_totalPcs<=DataEntryController.TotalPCS)
				{
					//saveDimensionDetails();
					saveDimensionDataToList();
					txtDimensionPCS.requestFocus();
				}
				else
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setHeaderText(null);
					alert.setTitle("Alert");
					alert.setContentText("Dimension PCS not more than Total PCS");
					alert.showAndWait();
					DataEntryController.Dimension_totalPcs=count;
					txtDimensionPCS.requestFocus();
				}
				
				//saveDimensionDetails();

				/*if(totalPcs==DataEntryController.TotalPCS)
				{
					for(DimensionTableBean bean:tabledata_Dimension)
					{
						finalweight=finalweight+bean.getWeight();
					}
					
					
					DataEntryController.volWeightFromDimension=finalweight;
						totalPcs=0;
					Main.dimensionStage.close();
					
				}*/
				
				
			}
		}

		
	}
	
	
	

	public void showValues()
	{
		
		//System.out.println("PCS: "+txtDimensionPCS.getText()+", L: "+txtLength.getText()+", H: "+txtHeight.getText()+", W: "+txtWidth.getText()+", weight: "+(Double.valueOf(txtHeight.getText())*Double.valueOf(txtLength.getText())*Double.valueOf(txtWidth.getText())/2000));
	/*	totalweight=totalweight+((Double.valueOf(txtHeight.getText())*Double.valueOf(txtLength.getText())*Double.valueOf(txtWidth.getText())/2000));
		finalweight=finalweight+totalweight;*/
		//System.out.println("Total PCS: "+DataEntryController.Dimension_totalPcs +", Weight: "+df.format(DataEntryController.Dimension__totalweight));
		
		
		if(DataEntryController.Dimension_totalPcs==DataEntryController.TotalPCS)
		{
			//DataEntryController.Dimension_totalPcs=0;
			
			//DataEntryController.volWeightFromDimension=finalweight;
			
			//txtWidth.setText(String.valueOf(finalweight));
			//Singleton.getInstance().setTxtField1(txtWidth);
			
			//DataEntryController.Dimension__finalweight=0;
			//DataEntryController.Dimension__totalweight=0;
			Main.dimensionStage.close();
					
		}
	}
	
	public void stop() {
	    System.out.println("Stopping: stop()");
	  }
	
	
	
// ****************************************************************
	
	public void saveDimensionDataToList() throws Exception
	{
		/*DataEntryController.Dimension__totalweight = DataEntryController.Dimension__totalweight + ((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText()) / 2000));*/
		
		
		DimensionBean diBean = new DimensionBean();

		diBean.setAwbno(DataEntryController.awbno);
		diBean.setPcs(Integer.valueOf(txtDimensionPCS.getText()));
		diBean.setLength(Double.valueOf(txtLength.getText()));
		diBean.setWidth(Double.valueOf(txtWidth.getText()));
		diBean.setHeight(Double.valueOf(txtHeight.getText()));
		diBean.setWeight((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText())) / 2000);
	//	System.out.println("total+ " + (diBean.getHeight() * diBean.getWidth() * diBean.getHeight()) + ", weight: "
	//			+ diBean.getWeight());
		
		DataEntryController.LIST_SAVEDIMENSION_DATA.add(diBean);
		tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(),null, diBean.getPcs(), 
				diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));
		
		tableDimension.setItems(tabledata_Dimension);
		
		clickDimensionTableRows();
		
		showDimension();
		
		
		
		
		double f_Wt=0.0;
		
		for(DimensionTableBean bean:tabledata_Dimension)
		{
			f_Wt=f_Wt+bean.getWeight();
		}
		
		
		
		
		DataEntryController.volWeightFromDimension = f_Wt;
		//DataEntryController.volWeightFromDimension = DataEntryController.volWeightFromDimension + DataEntryController.Dimension__totalweight;
		DataEntryController.Dimension__finalweight=f_Wt;
		
		System.out.println("WT: "+df.format(DataEntryController.Dimension__finalweight));
		
		if (DataEntryController.Dimension_totalPcs == DataEntryController.TotalPCS) {
			
			
		//	System.out.println("Finally : " + DataEntryController.Dimension__finalweight);
			//DataEntryController.Dimension_totalPcs = 0;

			//DataEntryController.Dimension__finalweight = 0;
			//DataEntryController.Dimension__totalweight = 0;
			Main.dimensionStage.close();
			
			
		}
		
	}
	
	
	
	
	public void loadDimensionTableViaList() throws Exception
	{
		//System.out.println("Save Dimension list size: "+DataEntryController.LIST_SAVEDIMENSION_DATA.size());
		
		if(DataEntryController.LIST_SAVEDIMENSION_DATA.size()!=0)
		{
			tabledata_Dimension.clear();
			
			for(DimensionBean diBean:DataEntryController.LIST_SAVEDIMENSION_DATA)
			{
				tabledata_Dimension.add(new DimensionTableBean(0,diBean.getAwbno(),null, diBean.getPcs(), 
						diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));
				
				tableDimension.setItems(tabledata_Dimension);
			}
			
			clickDimensionTableRows();
			showDimension();
		}
	}
	
	/*public void saveDimensionDetails() throws SQLException 
	{
			
		  //Alert alert = new Alert(AlertType.INFORMATION); alert.setTitle("Inscan Status");
		  //alert.setHeaderText(null);
		 
		totalweight = totalweight + ((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText()) / 2000));
		// finalweight=finalweight+totalweight;

		// System.out.println("Final weight: "+finalweight);

		DimensionBean diBean = new DimensionBean();

		diBean.setAwbno(DataEntryController.awbno);
		diBean.setPcs(Integer.valueOf(txtDimensionPCS.getText()));
		diBean.setLength(Double.valueOf(txtLength.getText()));
		diBean.setWidth(Double.valueOf(txtWidth.getText()));
		diBean.setHeight(Double.valueOf(txtHeight.getText()));
		diBean.setWeight((Double.valueOf(txtHeight.getText()) * Double.valueOf(txtLength.getText())
				* Double.valueOf(txtWidth.getText())) / 2000);
		System.out.println("total+ " + (diBean.getHeight() * diBean.getWidth() * diBean.getHeight()) + ", weight: "
				+ diBean.getWeight());

		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement preparedStmt = null;

		try 
		{
			String query = "insert into dimensions(awbno,pcs,weight,length,height,width,indate,create_date,lastmodifieddate)"
					+ " values(?,?,?,?,?,?,CURRENT_DATE,CURRENT_DATE,CURRENT_TIMESTAMP)";

			preparedStmt = con.prepareStatement(query);

			preparedStmt.setString(1, diBean.getAwbno());
			preparedStmt.setInt(2, diBean.getPcs());
			preparedStmt.setDouble(3, diBean.getWeight());
			preparedStmt.setDouble(4, diBean.getWidth());
			preparedStmt.setDouble(5, diBean.getLength());
			preparedStmt.setDouble(6, diBean.getHeight());

			int consignorSaveStatus = preparedStmt.executeUpdate();

			loadDimensionTableFromDB();

		}
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally 
		{
			if (totalPcs == DataEntryController.TotalPCS) {
				DataEntryController.volWeightFromDimension = DataEntryController.volWeightFromDimension + totalweight;
				System.out.println("Finally : " + finalweight);
				totalPcs = 0;

				finalweight = 0;
				totalweight = 0;
				Main.dimensionStage.close();
			}
			dbcon.disconnect(preparedStmt, null, null, con);
		}

	}
		*/
// ****************************************************************
		
	public void checkDimensionForEnteredAwbNo() throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		
		
		DimensionBean diBean=new DimensionBean();
		
		try 
		{
			st = con.createStatement();
			String sql = "select * from dimensions where awbno='" + DataEntryController.awbno + "'";

		//	System.out.println("Sql: "+sql);
			rs = st.executeQuery(sql);

			if (!rs.next()) 
			{
				checkAwbNoForDimension=false;
			} 
			else 
			{
				checkAwbNoForDimension=true;
			}
			loadDimensionTableFromDB();

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{

			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
// ****************************************************************	
	
	public void loadDimensionTableFromDB() throws SQLException 
	{
		//tabledata_Dimension.clear();
		
		if(checkAwbNoForDimension==true)
		{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		Statement st = null;
		ResultSet rs = null;
		
		int countpcs=0;
		DimensionBean diBean=new DimensionBean();
		
		try 
		{
			st = con.createStatement();
			String sql = "select slno,awbno,pcs,weight,length,height,width from dimensions where awbno='" + DataEntryController.awbno + "'";

			//System.out.println("Sql: "+sql);
			rs = st.executeQuery(sql);

			if (!rs.next()) 
			{
				//consignorExistanceStatus = false;
			} 
			else 
			{
				do 
				{
					diBean.setDb_serialNo(rs.getInt("slno"));
					diBean.setAwbno(rs.getString("awbno"));
					diBean.setPcs(rs.getInt("pcs"));
					diBean.setWeight(rs.getDouble("weight"));
					diBean.setWidth(rs.getDouble("width"));
					diBean.setHeight(rs.getDouble("height"));
					diBean.setLength(rs.getDouble("length"));
					
					countpcs=countpcs+diBean.getPcs();
					
					tabledata_Dimension.add(new DimensionTableBean(diBean.getDb_serialNo(),diBean.getAwbno(),null, diBean.getPcs(), 
							diBean.getLength(), diBean.getHeight(), diBean.getWidth(), diBean.getWeight()));
					}
				while (rs.next());
			}
			tableDimension.setItems(tabledata_Dimension);
			
			clickDimensionTableRows();
			
			DataEntryController.Dimension_totalPcs=countpcs;
			
			if(DataEntryController.Dimension_totalPcs==DataEntryController.TotalPCS)
			{
				txtDimensionPCS.setDisable(true);
				txtHeight.setDisable(true);
				txtWidth.setDisable(true);
				txtLength.setDisable(true);
				btnSubmit.setDisable(true);
			}
			
			showDimension();

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{

			dbcon.disconnect(null, st, rs, con);
		}
		}
	}	
	
	
// =================== Method for check clickable Dimension Table row ======================		
	
	public void clickDimensionTableRows() throws Exception 
	{
		tableDimension.setRowFactory(tv -> {
		TableRow<DimensionTableBean> row = new TableRow<>();
		row.setOnMouseClicked(event -> {
			
			/*if (event.getClickCount() == 1 && (!row.isEmpty())) {
				
			System.out.println("Single click >>>");
			
			}*/

			if (event.getClickCount() == 2 && (!row.isEmpty())) 
			{
						
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Delete Confirmation");
				alert.setHeaderText(null);
				alert.setContentText("Are you sure you want to delete the selected entry...?");
						
				ButtonType buttonTypeYes = new ButtonType("Yes");
				ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
				//ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
				alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
						
				Optional<ButtonType> result = alert.showAndWait();
				
				if (result.get() == buttonTypeYes)
				{
					tabledata_Dimension.remove(row.getIndex());
				
					if(DataEntryController.LIST_SAVEDIMENSION_DATA.size()>0)
					{
						if(row.getIndex()<DataEntryController.LIST_SAVEDIMENSION_DATA.size())
						{
							int index=row.getIndex();
							DataEntryController.LIST_SAVEDIMENSION_DATA.remove(index);
							
							setWeight();
							
							if(tabledata_Dimension.size()==0)
							{
								DataEntryController.volWeightFromDimension=0.0;
							}
							
							/*System.out.println("Final Before: "+DataEntryController.Dimension__finalweight);
							System.out.println("Vol Before: "+DataEntryController.volWeightFromDimension);
							DataEntryController.volWeightFromDimension=DataEntryController.volWeightFromDimension-DataEntryController.LIST_SAVEDIMENSION_DATA.get(index).getWeight();
							System.out.println("Vol After: "+DataEntryController.volWeightFromDimension);
							System.out.println("Final After: "+DataEntryController.Dimension__finalweight);*/
						}
					}
						
						if(row.getItem().getDb_serialNo()>0)
						{
							try 
							{
								deleteDimensionsFromDB(row.getItem().getDb_serialNo());
								setWeight();
								
								if(tabledata_Dimension.size()==0)
								{
									DataEntryController.volWeightFromDimension=0.0;
								}
							}
							catch (Exception e) 
							{
								e.printStackTrace();
							}
						}
							
						DataEntryController.Dimension_totalPcs=DataEntryController.Dimension_totalPcs-1;
						
						if(DataEntryController.Dimension_totalPcs!=DataEntryController.TotalPCS)
						{
							txtDimensionPCS.setDisable(false);
							txtHeight.setDisable(false);
							txtWidth.setDisable(false);
							txtLength.setDisable(false);
							btnSubmit.setDisable(false);
						}
							
					}
				}
			
			});
			return row;
		});

	}	

// ****************************************************************
	
	public void setWeight()
	{
		for(DimensionTableBean bean: tabledata_Dimension)
		{
			DataEntryController.volWeightFromDimension = DataEntryController.volWeightFromDimension + bean.getWeight();
		}
	}
	
// ****************************************************************
	
	public void showDimension()
	{
		
		for(DimensionTableBean bean: tabledata_Dimension)
		{
			System.out.println("DB slno: "+bean.getDb_serialNo()+" | Weight: "+bean.getWeight());
		}
	}
	
	
// ****************************************************************
	
	public void deleteDimensionsFromDB(int slno) throws SQLException
	{
		DBconnection dbcon = new DBconnection();
		Connection con = dbcon.ConnectDB();

		PreparedStatement pst=null;
		
		try 
		{
			System.out.println("Enter in Inbox Delete");
			String query = "delete from dimensions where slno=?";
			pst = con.prepareStatement(query);
			pst.setInt(1,slno);
			pst.executeUpdate();
			

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			e.printStackTrace();
		} 
		finally 
		{
			dbcon.disconnect(pst, null, null, con);
		}
	}
	
// ****************************************************************	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		
		
		
		
		tabColumn_AwbNo.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("awbno"));
		tabColumn_Height.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("height"));
		tabColumn_pcs.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,Integer>("pcs"));
		tabColumn_Lenght.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("length"));
		tabColumn_Weight.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("weight"));
		tabColumn_Width.setCellValueFactory(new PropertyValueFactory<DimensionTableBean,String>("width"));
		
	
		
		
		if(DataEntryController.Dimension_totalPcs==DataEntryController.TotalPCS)
		{
			txtDimensionPCS.setDisable(true);
			txtHeight.setDisable(true);
			txtWidth.setDisable(true);
			txtLength.setDisable(true);
			btnSubmit.setDisable(true);
		}
		
		
		try {
			loadDimensionTableViaList();
			checkDimensionForEnteredAwbNo();
			//loadDimensionTableFromDB();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

}
