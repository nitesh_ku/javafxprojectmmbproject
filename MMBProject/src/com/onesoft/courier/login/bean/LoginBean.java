package com.onesoft.courier.login.bean;

public class LoginBean {
	
	private String userid;
	private String password;
	private String username;
	private String branchCode;
	private String branchCity;
	private String branchPincode;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchCity() {
		return branchCity;
	}
	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}
	public String getBranchPincode() {
		return branchPincode;
	}
	public void setBranchPincode(String branchPincode) {
		this.branchPincode = branchPincode;
	}
	

}
