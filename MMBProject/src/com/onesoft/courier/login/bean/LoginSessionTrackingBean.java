package com.onesoft.courier.login.bean;

import java.sql.Date;
import java.sql.Time;

public class LoginSessionTrackingBean {
	
	private int slno_uid;
	
	private long hours;
	private long min;
	private long sec;
	
	private String time;
	private String[] timearray;
	private String timeduration;
	
	private String userid;
	private Date logindate;
	private Time logintime;
	private Date logoutdate;
	private Time logouttime;
	private String duration;
	private String branchcode;
	private String ipaddress;
	private String hostname;
	private String location;
	
	
	
	public long getHours() {
		return hours;
	}
	public void setHours(long hours) {
		this.hours = hours;
	}
	public long getMin() {
		return min;
	}
	public void setMin(long min) {
		this.min = min;
	}
	public long getSec() {
		return sec;
	}
	public void setSec(long sec) {
		this.sec = sec;
	}
	public int getSlno_uid() {
		return slno_uid;
	}
	public void setSlno_uid(int slno_uid) {
		this.slno_uid = slno_uid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public Date getLogindate() {
		return logindate;
	}
	public void setLogindate(Date logindate) {
		this.logindate = logindate;
	}
	public Time getLogintime() {
		return logintime;
	}
	public void setLogintime(Time logintime) {
		this.logintime = logintime;
	}
	public Date getLogoutdate() {
		return logoutdate;
	}
	public void setLogoutdate(Date logoutdate) {
		this.logoutdate = logoutdate;
	}
	public Time getLogouttime() {
		return logouttime;
	}
	public void setLogouttime(Time logouttime) {
		this.logouttime = logouttime;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String[] getTimearray() {
		return timearray;
	}
	public void setTimearray(String[] timearray) {
		this.timearray = timearray;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTimeduration() {
		return timeduration;
	}
	public void setTimeduration(String timeduration) {
		this.timeduration = timeduration;
	}
	
	
	

}
