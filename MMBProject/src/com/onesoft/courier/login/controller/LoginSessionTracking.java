package com.onesoft.courier.login.controller;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.login.bean.LoginSessionTrackingBean;
import com.onesoft.courier.main.Main;



public class LoginSessionTracking {
	
	
java.util.Date dateutil=new java.util.Date();
	
	LocalDate logindate;
	LocalTime logintime;
	
	LocalDate logoutdate;
	LocalTime logouttime;
	
	static int loginhour=0;
	static int logouthour=0;
	
	static int loginmin=0;
	static int logoutmin=0;
	
	static int loginsec=0;
	static int logoutsec=0;
	
	static int sln_uid=0;
	
	String ip;

	
	
	  
	  
	public void loginDetail(String userid,String username) throws ParseException, IOException, SQLException
	{
		logindate=logindate.now();
		logintime=logintime.now();
		
		LoginSessionTrackingBean lstbean=new LoginSessionTrackingBean();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement ps=null;
		
	
		InetAddress addr = InetAddress.getLocalHost();
	 //   String hostname = addr.getHostName();
	    
	    int nextval=saveLoginDetail();
		
	//System.out.println("next value from loginDetail(): "+nextval); 
		
	    if(username.equals("Default"))
	    {
	    	lstbean.setUserid("Default");
	    }
	    else
	    {
	    	lstbean.setUserid(userid);
	    }
		lstbean.setLogindate(Date.valueOf(logindate));
		lstbean.setLogintime(Time.valueOf(logintime));
		lstbean.setIpaddress(addr.getHostAddress());
		lstbean.setHostname(addr.getHostName());
		
	
		try
		{
			String sql="insert into trackuser(slno_uid,userid,logindate,logintime,ip_address,hostname)"
						+ "values(?,?,?,?,?,?)";
				
			ps = con.prepareStatement(sql);
				
			ps.setInt(1, nextval);
			ps.setString(2, lstbean.getUserid());
			ps.setDate(3, lstbean.getLogindate());
			ps.setTime(4, lstbean.getLogintime());
			ps.setString(5, lstbean.getIpaddress());
			ps.setString(6, lstbean.getHostname());
				
			int i=ps.executeUpdate();
			System.out.println(i+" row affected");
	
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(ps, null, null, con);
		}
	    
	  }
	
	public int saveLoginDetail() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement ps=null;
		ResultSet res = null;
		
	
        int nextVal = 0;
        try {
             ps = con.prepareStatement("select nextval('trackuser_slno_uid_seq')");
              
              res = ps.executeQuery();
              while (res.next())
              {
                    nextVal = res.getInt("nextval");
              }
        } 
        catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(ps, null, null, con);
		}
        
        //log.debug("Exit in getNextAddressId() ");
        System.out.println("next value: "+nextVal);
        sln_uid=nextVal;
        return nextVal;

	}
	
	public void logoutDetail() throws ParseException, SQLException
	{
		LoginSessionTrackingBean lstbean=new LoginSessionTrackingBean();
		logouttime=logouttime.now();
		logoutdate=logoutdate.now();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement ps=null;
		ResultSet rs = null;
		Statement st=null;
		
		try
		{
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			String sql="select slno_uid,logintime from trackuser where slno_uid="+sln_uid;
			rs=st.executeQuery(sql);
			
            while (rs.next())
            {
                 // //nextVal = rs.getInt("nextval");
            	lstbean.setSlno_uid(rs.getInt("slno_uid"));
            	lstbean.setLogintime(rs.getTime("logintime"));
            }
			rs.last();
			
			
			logintime = lstbean.getLogintime().toLocalTime();
			
			//System.out.println(logintime);
			loginhour=logintime.getHour();
		    loginmin=logintime.getMinute();
		    loginsec=logintime.getSecond();
		    
		    logouthour=logouttime.getHour();
			logoutmin=logouttime.getMinute();
			logoutsec=logouttime.getSecond();
			
			
		//	System.out.println("loginDetail: "+loginhour+" "+loginmin+" "+loginsec);
		//	System.out.println("logoutDetail: "+logouthour+" "+logoutmin+" "+logoutsec);
			
			
			lstbean.setLogoutdate(Date.valueOf(logoutdate));
			lstbean.setLogouttime(Time.valueOf(logouttime));
			lstbean.setDuration(duration());
			
			
			sql="update trackuser set logoutdate=?,logouttime=?,duration=? where slno_uid=?";
			
			ps = con.prepareStatement(sql);
			
			
			ps.setDate(1, lstbean.getLogoutdate());
			ps.setTime(2, lstbean.getLogouttime());
			ps.setString(3, lstbean.getDuration());
			ps.setInt(4, lstbean.getSlno_uid());
			

			int i=ps.executeUpdate();
			System.out.println(i+" row affected");
			
			//Main.primaryStage.setTitle("MMB Project");
			//duration();
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(ps, st, rs, con);
		}

	}
	
	
	public String duration()
	{
		
		LoginSessionTrackingBean lgbean=new LoginSessionTrackingBean();
		
		lgbean.setHours(Duration.between(logintime, logouttime).toHours());
		lgbean.setMin(Duration.between(logintime, logouttime).toMinutes());
		lgbean.setSec((Duration.between(logintime, logouttime).getSeconds())%60);
		
		/*System.out.println(lgbean.getHours());
		System.out.println("min from toMinutes: "+lgbean.getMin());
		System.out.println("Min: "+lgbean.getSec()/60);
		System.out.println("Sec: "+lgbean.getSec());*/
	
		lgbean.setTime(lgbean.getHours()+":"+lgbean.getMin()+":"+lgbean.getSec());
		lgbean.setTimearray(lgbean.getTime().split(":"));
		
		if(Integer.parseInt(lgbean.getTimearray()[0])<10)
		{
			lgbean.getTimearray()[0]="0"+lgbean.getTimearray()[0];
		}
		
		if(Integer.parseInt(lgbean.getTimearray()[1])<10)
		{
			lgbean.getTimearray()[1]="0"+lgbean.getTimearray()[1];
		}
		
		if(Integer.parseInt(lgbean.getTimearray()[2])<10)
		{
			lgbean.getTimearray()[2]="0"+lgbean.getTimearray()[2];
		}
		
		
		lgbean.setTimeduration(lgbean.getTimearray()[0]+":"+lgbean.getTimearray()[1]+":"+lgbean.getTimearray()[2]);
		
		return lgbean.getTimeduration();
		
		
		
	}
	

}
