package com.onesoft.courier.login.controller;

import java.net.InetAddress;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ResourceBundle;

import com.onesoft.courier.DB.DBconnection;
import com.onesoft.courier.common.CommonVariable;
import com.onesoft.courier.login.bean.LoginBean;
import com.onesoft.courier.main.Main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class LoginController implements Initializable{
	
	
	@FXML
	public TextField txtuserid;
	
	@FXML
	public PasswordField pwduserpassword;
	
	@FXML
	public Button btnlogin;
	
	public static String start=null;
	
	LoginSessionTracking ls=new LoginSessionTracking();
	
// *************************************************************************************
	
		@FXML
		public void useEnterAsTabKey(KeyEvent e) throws SQLException
		{
			Node n=(Node) e.getSource();
			
			
			
			if(n.getId().equals("txtuserid"))						
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					pwduserpassword.requestFocus();
				}
			}
			
			else if(n.getId().equals("pwduserpassword"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					btnlogin.requestFocus();
				}
			}
			
			else if(n.getId().equals("btnlogin"))
			{
				if(e.getCode().equals(KeyCode.ENTER))
				{
					setValidation();
				}
			}
		}		
	
// *************************************************************************************
		
	public void setValidation() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		if(!txtuserid.getText().equals("") || !pwduserpassword.getText().equals(""))
		{
			if (txtuserid.getText() == null || txtuserid.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a User ID");
				alert.showAndWait();
				txtuserid.requestFocus();
			} 
			
			else if (pwduserpassword.getText() == null || pwduserpassword.getText().isEmpty()) 
			{
				alert.setTitle("Empty Field Validation");
				alert.setContentText("You did not enter a Password");
				alert.showAndWait();
				pwduserpassword.requestFocus();
			}
			else
			{
				getdata();
			}
		}
		
		else
		{
			getdata();
		}
		/*else if(dpkDOB.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("DOB field is Empty!");
			alert.showAndWait();
			dpkDOB.requestFocus();
		}
		
		else if(dpkDOJ.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("DOJ field is Empty!");
			alert.showAndWait();
			dpkDOJ.requestFocus();
		}
		
		else 
		{
			if(txtEmployeeCode.isEditable()==true)
			{
				saveEmployeeDetails();
			}
			else
			{
				updateEmployeeRegistrationDetails();
			}
			
		}*/
	}
		
// *************************************************************************************	
		
	public void getdata() throws SQLException
	{
			
		LoginBean logbean=new LoginBean();
		
		ResultSet rs=null;
		PreparedStatement ps=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Instant start=null;//Instant.now();
		
			try
			{
				sql="select ld.userid as loginid,ld.password as pswd,ld.username as username,bd.code as branchcode,"
						+ "bd.branch_city as branchcity,bd.branch_pincode as branchpincode from logindetail as ld , brndetail as bd "
						+ "where ld.branchcode=bd.code and userid=? and password=?";
				ps = con.prepareStatement(sql);
		
				ps.setString(1, txtuserid.getText());
				ps.setString(2, pwduserpassword.getText());
				rs=ps.executeQuery();
		
				if(!rs.next())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Database Alert");
					alert.setHeaderText(null);
					alert.setContentText("Incorrect userid or password");
					alert.showAndWait();
				}
				else
				{
					logbean.setUserid(rs.getString("loginid"));
					logbean.setPassword(rs.getString("pswd"));
					logbean.setUsername(rs.getString("username"));
					logbean.setBranchCode(rs.getString("branchcode"));
					logbean.setBranchCity(rs.getString("branchcity"));
					logbean.setBranchPincode(rs.getString("branchpincode"));
					
					
					if(logbean.getUserid().equals(txtuserid.getText()) && logbean.getPassword().equals(pwduserpassword.getText()))
					{
						
						// ------------------ method to store login details ----------------
						
						//ls.loginDetail(logbean.getUserid(),logbean.getUsername());
						Main.loginStage.close();
						if(logbean.getUsername().equals("Default"))
						{
							CommonVariable.LOGGED_IN_USER="Default";
							//HomeController.loggedInUSER="Default";
						}
						else
						{
							CommonVariable.LOGGED_IN_USER=logbean.getUserid();
							//HomeController.loggedInUSER=logbean.getUserid();
						}
						
						InetAddress addr = InetAddress.getLocalHost();
						CommonVariable.USER_IP_ADDRESS=addr.getHostAddress();
						CommonVariable.USER_HOST_NAME=addr.getHostName();
						CommonVariable.USER_ID=logbean.getUserid();
						System.err.println("IP Address >> "+CommonVariable.USER_IP_ADDRESS);
						System.err.println("Host Name >> "+CommonVariable.USER_HOST_NAME);
						
						
						CommonVariable.LOGGED_IN_USER="UserID: "+logbean.getUserid()+" | Username: "+logbean.getUsername();
						System.out.println(CommonVariable.LOGGED_IN_USER);
						CommonVariable.USER_BRANCHCODE=logbean.getBranchCode();
						CommonVariable.USER_BRANCH_PINCODE=logbean.getBranchPincode();
						//HomeController.userIdAndUserName="UserID: "+logbean.getUserid()+" | Username: "+logbean.getUsername();
						//HomeController.userBranch=logbean.getBranchCode();
						//new LoadOrigin().loadOriginViaBranch();
						Main.homePage();
					}
				}	
			}
			
			catch(Exception e)
			{
			System.out.println(e);
			e.printStackTrace();
			}
			
			finally
			{
			dbcon.disconnect(ps, null, rs, con);
			System.out.println("Connection closed by Search method");
			}
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		/*CalculateWeight cw=new CalculateWeight();
		try {
			cw.getClientRateDetails();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}
	

}
